/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ShareViewController.h"
#import "UITools.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import <Rainbow/Rainbow.h>

#import "CustomSearchController.h"
#import "NSData+ImageForData.h"
#import "FileSharingService+UploadFile.h"
#import "UIRainbowGenericTableViewCell.h"
#import "File+DefaultImage.h"
#import "UIScrollView+APParallaxHeader.h"
#import "ServicesManager+CustomResourceName.h"
#import "UITextView+Placeholder.h"
#import "CustomSearchBarContainerView.h"
#import "Conversation+Private.h"
#import "UIImage+Additions.h"
#import "UIStoryboardManager.h"
#import "MBProgressHUD.h"
#import "UIView+MGBadgeView.h"
#import <HockeySDK/HockeySDK.h>

static BOOL didSetupHockeyAppSDK = NO;

@interface ShareViewController () <UITableViewDelegate, UITableViewDataSource, CustomSearchControllerDelegate, UITextViewDelegate, BITHockeyManagerDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *maskView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maskViewHeightConstraint;

@property (nonatomic, strong) NSMutableArray<Conversation *> *conversations;
@property (nonatomic, strong) NSMutableArray <Conversation*> *selectedItems;

@property (nonatomic, strong) Conversation *myRainbowSharingConversation;
@property (nonatomic, strong) NSCondition *uploadLock;
@property (nonatomic, strong) NSCondition *sendMessageLock;

@property (nonatomic, strong) ServicesManager *serviceManager;
@property (nonatomic, strong) FileSharingService *fileSharingService;
@property (nonatomic, strong) ConversationsManagerService *conversationsManagerService;

@property (nonatomic, strong) NSMutableArray <File *> *filesToShare;

@property (nonatomic, strong) UISearchResultController *searchResultController;
@property (nonatomic, strong) CustomSearchController *customSearchController;

// Header view
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *attachmentImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *attachmentImageHeigthConstraint;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIImageView *secondAttachmentView;
@property (weak, nonatomic) IBOutlet UIImageView *lastAttachmentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewLeadingConstraint;
@property (weak, nonatomic) IBOutlet UILabel *fileNameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *attachmentImageViewConstraint;

// Footer view
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerViewHeigthConstraint;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@end

@implementation ShareViewController

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void) awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndLoadingConversations:) name:kConversationsManagerDidEndLoadingConversations object:nil];
    [RainbowUserDefaults setSuiteName:@"group.com.alcatellucent.otcl"];
    
    _serviceManager = [ServicesManager sharedInstance];
    [ServicesManager setCustomResourceName:@"mobile_ios_shared_extension"];
    _conversationsManagerService = _serviceManager.conversationsManagerService;
    _fileSharingService = _serviceManager.fileSharingService;
    
    if(_serviceManager.myUser.username && _serviceManager.myUser.password){
        [_serviceManager.loginManager connect];
        
        _myRainbowSharingConversation = [Conversation new];
        _myRainbowSharingConversation.conversationId = @"fake";
        Contact *fakeContact = [[ServicesManager sharedInstance].contactsManagerService createOrUpdateRainbowContactWithJid:@"myRainbowSharing@fake.com"];
        
        [[ServicesManager sharedInstance].contactsManagerService updateRainbowContact:fakeContact withJSONDictionary:@{@"lastName":NSLocalizedString(@"My Rainbow share", nil)}];
        UIImage *myRainbowSharingImage = [[UIImage imageNamed:@"myRainbowShare"] add_tintedImageWithColor:[UITools defaultTintColor] style:ADDImageTintStyleKeepingAlpha];
        [[ServicesManager sharedInstance].contactsManagerService updateRainbowContact:fakeContact withPhotoData:UIImagePNGRepresentation(myRainbowSharingImage)];
        
        _myRainbowSharingConversation.peer = fakeContact;
        Message *fakeLastMessage = [Message new];
        fakeLastMessage.timestamp = [[NSDate date] dateByAddingHours:1];
        fakeLastMessage.peer = _myRainbowSharingConversation.peer;
        _myRainbowSharingConversation.lastMessage = fakeLastMessage;
    }
    _conversations = [NSMutableArray array];
    _selectedItems = [NSMutableArray array];
    _uploadLock = [NSCondition new];
    _sendMessageLock = [NSCondition new];
    _filesToShare = [NSMutableArray array];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidEndLoadingConversations object:nil];
    _serviceManager = nil;
    _conversationsManagerService = nil;
}

-(void) viewDidLoad {
    [super viewDidLoad];
    
    if(!didSetupHockeyAppSDK){
        [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"b926d8c830384b7899f9a2dcfd5d812b"];
        [[BITHockeyManager sharedHockeyManager] setDelegate:self];
        [[BITHockeyManager sharedHockeyManager] setDisableMetricsManager:YES];
        [[BITHockeyManager sharedHockeyManager] startManager];
        didSetupHockeyAppSDK = YES;
    }
    
    if(!_serviceManager.myUser.username || !_serviceManager.myUser.password){
        [self showAlertWithTitle:NSLocalizedString(@"Login requested", nil) message:NSLocalizedString(@"You must be logged into Rainbow to share a file", nil) completionHandler:^{
            [self didTapCancelButton:nil];
        }];
    }
    
    self.definesPresentationContext = YES;
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];
    [self.tableView setEditing:YES animated:YES];
    
    _textView.placeholder = NSLocalizedString(@"Your message", nil);
    _textView.font = [UIFont fontWithName:[UITools defaultFontName] size:_textView.font.pointSize];
    _textView.placeholderColor = [UIColor lightGrayColor];
    
    [UITools applyCustomBoldFontTo:_sendButton.titleLabel];
    [_sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _sendButton.backgroundColor = [UITools defaultTintColor];
    [_sendButton setTitle:NSLocalizedString(@"Share", nil) forState:UIControlStateNormal];
    
    _cancelButton.title = NSLocalizedString(@"Cancel", nil);
    [_cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UITools defaultTintColor], NSFontAttributeName:[UIFont fontWithName:[UITools boldFontName] size:16]} forState:UIControlStateNormal];
    
    _attachmentImageView.badgeView.badgeColor = [UITools notificationBadgeColor];
    _attachmentImageView.badgeView.outlineWidth = 0.0f;
    _attachmentImageView.badgeView.position = MGBadgePositionTopRight;
    _attachmentImageView.badgeView.font = [UIFont fontWithName:[UITools defaultFontName] size:13];
    _attachmentImageView.badgeView.bageStyle = MGBadgeStyleRoundRect;
    _attachmentImageView.badgeView.displayIfZero = NO;
    _attachmentImageView.badgeView.badgeValue = 0;
    _attachmentImageView.badgeView.horizontalOffset = 5;
    
    // Search controller
    _searchResultController = (UISearchResultController*)[[UIStoryboardManager sharedInstance].searchStoryboard instantiateViewControllerWithIdentifier:@"UISearchResultController"];
    _searchResultController.modalPresentationStyle = UIModalPresentationNone;
    _customSearchController = [[CustomSearchController alloc] initWithSearchResultsController:_searchResultController customSearchControllerDelegate:self];
    _customSearchController.handleDismissOnCancel = NO;
    _customSearchController.searchScope = SearchScopeConversations | SearchScopeRooms | SearchScopeContacts;
    
    __weak typeof(self) weakSelf = self;
    __weak typeof(_selectedItems) weakSelectedItems = _selectedItems;
    __weak typeof(_customSearchController) weakCustomSearchController = _customSearchController;
    __weak typeof(_conversations) weakConversations = _conversations;
    __weak typeof(self.tableView) weakTableView = self.tableView;
    _searchResultController.cellSelectedActionBlock = ^(id object, NSString * sectionName) {
        NSLog(@"Selected object %@" ,object);
        Peer * aPeer = (Peer *)object;
        if([object isKindOfClass:[Conversation class]]){
            aPeer = ((Conversation *)object).peer;
        }
        [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:aPeer notifyStartConversation:NO withCompletionHandler:^(Conversation *conversation, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error){
                    [weakSelf showAlertWithTitle:@"" message:[NSString stringWithFormat:NSLocalizedString(@"Error while creating conversation with %@", nil), ((Peer *)object).displayName] completionHandler:nil];
                } else {
                    NSInteger existingConversationIdx = NSNotFound;
                    if([weakConversations containsObject:conversation]){
                        existingConversationIdx = [weakConversations indexOfObject:conversation];
                        [weakConversations removeObject:conversation];
                    }
                    [weakSelectedItems addObject:conversation];
                    [weakSelectedItems sortUsingDescriptors:@[[UITools sortDescriptorByLastMessageDate]]];
                    [weakTableView beginUpdates];
                    NSInteger idx = [weakSelectedItems indexOfObject:conversation];
                    if(existingConversationIdx != NSNotFound){
                        // Move the cell
                        [weakTableView moveRowAtIndexPath:[NSIndexPath indexPathForRow:existingConversationIdx inSection:1] toIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
                    } else {
                        // insert the new cell
                        [weakTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
                    }
                    [weakTableView endUpdates];
                    [weakSelectedItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
                        [weakTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                    }];
                    [weakTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    [weakSelf showHideSendButton];
                    [weakCustomSearchController searchBarCancelButtonClicked:weakCustomSearchController.searchBar];
                }
            });
        }];
    };
    CustomSearchBarContainerView *titleView = [[CustomSearchBarContainerView alloc] initWithSearchBar:_customSearchController.searchBar];
    titleView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
    self.navigationItem.titleView = titleView;
    self.navigationController.navigationBar.barTintColor = [UITools colorFromHexa:0xF9F9F9FF];
    
    NSExtensionItem *item = self.extensionContext.inputItems.firstObject;
    for (NSItemProvider *itemProvider in item.attachments) {
        if ([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeImage]) {
            // This item can be represented as image, load it
            [itemProvider loadItemForTypeIdentifier:(NSString *)kUTTypeImage options:nil completionHandler:^(NSData *attachmentData, NSError *error) {
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"ddMMyyyy_HHmm"];
                NSString *fileName = [NSString stringWithFormat:@"IMG_%@.%@", [dateFormat stringFromDate:[NSDate date]], [attachmentData extension]];
                File *aFile = [_fileSharingService createTemporaryFileWithFileName:fileName andData:attachmentData andURL:nil];
                if (aFile != nil)
                    [_filesToShare addObject:aFile];
                [self showAttachmentView];
            }];
        } else if ([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeVideo] || [itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeMovie]) {
            NSString *type = [itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeVideo]?(NSString *)kUTTypeVideo:(NSString *)kUTTypeMovie;
            
            [itemProvider loadItemForTypeIdentifier:type options:nil completionHandler:^(NSData *attachmentData, NSError *error) {
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"ddMMyyyy_HHmm"];
                NSString *fileName = [NSString stringWithFormat:@"IMG_%@.%@", [dateFormat stringFromDate:[NSDate date]], [attachmentData extension]];
                File * aFile = [_fileSharingService createTemporaryFileWithFileName:fileName andData:attachmentData andURL:nil];
                if (aFile != nil)
                    [_filesToShare addObject:aFile];
                
                [itemProvider loadPreviewImageWithOptions:@{} completionHandler:^(id<NSSecureCoding> item, NSError * error) {
                    aFile.thumbnailData = UIImagePNGRepresentation((UIImage*)item);
                    [self showAttachmentView];
                }];
                [self showAttachmentView];
            }];
        } else if ([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeURL] || [itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeFileURL] || [itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypePDF]) {
            NSString *type = @"";
            if ([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeURL])
                type = (NSString *)kUTTypeURL;
            if([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeFileURL])
                type = (NSString *)kUTTypeFileURL;
            if([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypePDF])
                type = (NSString *)kUTTypePDF;
            
            [itemProvider loadItemForTypeIdentifier:type options:nil completionHandler:^(NSURL *url, NSError *error) {
                if([url isFileURL]){
                    // We can create a FileAttachement
                    NSData *data = [NSData dataWithContentsOfURL:url];
                    File *aFile = [_fileSharingService createTemporaryFileWithFileName:[url lastPathComponent] andData:data andURL:nil];
                    if (aFile != nil)
                        [_filesToShare addObject:aFile];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _textView.text = @"";
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _textView.text = [NSString stringWithFormat:@"%@\n",[url description]];
                    });
                }
                [self showAttachmentView];
            }];
        }
    }
    
    // Shared plain text is stored here. Content varies wildly based on app.
    NSString *sharedPlainText = [item.attributedContentText string];
    if (sharedPlainText)  {
        dispatch_async(dispatch_get_main_queue(), ^{
            _textView.text = [NSString stringWithFormat:@"%@\n",sharedPlainText];
        });
    }
    [self showAttachmentView];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self showAttachmentView];
    self.tableView.parallaxView.layer.zPosition = 1;
    [self getConversations];
    [_tableView reloadData];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [self showHideSendButton];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

-(void) getConversations {
    [_conversations removeAllObjects];
    
    if(_myRainbowSharingConversation && ![_selectedItems containsObject:_myRainbowSharingConversation]){
        [_conversations addObject:_myRainbowSharingConversation];
    }
    
    [_conversations addObjectsFromArray:_conversationsManagerService.conversations];
    [_conversations removeObjectsInArray:_selectedItems];
    [_conversations sortUsingDescriptors:@[[UITools sortDescriptorByLastMessageDate]]];
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

-(void) showHideSendButton {
    if(_selectedItems.count > 0){
        if(_footerView.hidden == YES){
            _footerViewHeigthConstraint.constant = 68;
            _footerView.hidden = NO;
        }
    } else {
        _footerViewHeigthConstraint.constant = 0;
        _footerView.hidden = YES;
    }
    if(_selectedItems.count == 1 && [_selectedItems containsObject:_myRainbowSharingConversation]){
        _textView.alpha = 0.3;
        _textView.userInteractionEnabled = NO;
    } else {
        _textView.alpha = 1;
        _textView.userInteractionEnabled = YES;
    }
}

-(void) showAttachmentView {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showAttachmentView];
        });
        return;
    }
    
    __block File *firstFile = nil;
    [_filesToShare enumerateObjectsUsingBlock:^(File * aFile, NSUInteger idx, BOOL * stop) {
        if(idx == 0){
            firstFile = aFile;
            _attachmentImageView.image = aFile.thumbnailBig;
            _attachmentImageView.tintColor = aFile.tintColor;
        } else if(idx == 1){
            _secondAttachmentView.image = aFile.thumbnailBig;
            _secondAttachmentView.tintColor = aFile.tintColor;
            _secondAttachmentView.hidden = NO;
        } else if(idx == 2){
            _lastAttachmentView.image = aFile.thumbnailBig;
            _lastAttachmentView.tintColor = aFile.tintColor;
            _lastAttachmentView.hidden = NO;
        } else {
            *stop = YES;
        }
    }];
    
    if( firstFile.hasThumbnailOnServer && (firstFile.thumbnailData || firstFile.data) ) {
        _attachmentImageHeigthConstraint.constant = 90;
        _fileNameLabel.hidden = YES;
        _attachmentImageViewConstraint.constant = 10;
    } else {
        _attachmentImageHeigthConstraint.constant = 64;
        _fileNameLabel.hidden = NO;
        _fileNameLabel.text = firstFile.fileName;
        _attachmentImageViewConstraint.constant = 0;
    }
    
    if(_filesToShare.count > 1){
        _attachmentImageView.badgeView.badgeValue = _filesToShare.count;
    }

    CGFloat height = self.navigationController.navigationBar.frame.size.height + _attachmentImageHeigthConstraint.constant;
    [self adjustAttachmentViewFrameWithHeight:height];
    
    if(_filesToShare.count == 0)
        _textViewLeadingConstraint.constant = - _attachmentImageHeigthConstraint.constant;
    else
        _textViewLeadingConstraint.constant = 10;
    
    
    [self.tableView addParallaxWithView:_headerView andHeight:height andMinHeight:height andShadow:NO];
    
}

-(void) adjustAttachmentViewFrameWithHeight:(CGFloat) heigth {
    CGRect frame = _headerView.frame;
    frame.size.height = heigth;
    frame.origin.y = -frame.size.height;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    
    _headerView.frame = frame;
}

- (BOOL)isContentValid {
    // Do validation of contentText and/or NSExtensionContext attachments here
    return YES;
}

- (void)didSelectPost {
    // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
    
    // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
    [_serviceManager.loginManager disconnect];
    [self.extensionContext completeRequestReturningItems:self.extensionContext.inputItems completionHandler:nil];
}

-(void)share {
    MBProgressHUD *uploadDataHud = nil;
    NSMutableDictionary <NSString *, NSArray*> * uploadStatusDic = [NSMutableDictionary dictionary];
    NSMutableDictionary <NSString *, NSArray*> * sendMessageStatusDic = [NSMutableDictionary dictionary];
    uploadDataHud = [UITools showHUDWithMessage:NSLocalizedString(@"Uploading file(s)", nil) forView:self.view mode:MBProgressHUDModeIndeterminate imageName:nil dismissCompletionBlock:nil];
    [uploadDataHud showAnimated:YES whileExecutingBlock:^{
        __weak __typeof__(_uploadLock) weakUploadLock = _uploadLock;
        __weak __typeof__(_sendMessageLock) weakSendMessageLock = _sendMessageLock;
        if(_filesToShare.count > 0){
            NSMutableArray<File*> *uploadedFiles = [NSMutableArray array];
            for (File *aFile in _filesToShare) {
                [[ServicesManager sharedInstance].fileSharingService uploadFile:aFile forConversation:nil completionHandler:^(File *file, NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *msg;
                        if (error) {
                            msg = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
                            if(error.code == 413){
                                msg = NSLocalizedString(@"Attachment reach allowed transfer limit", nil);
                            }
                        } else {
                            [uploadedFiles addObject:file];
                        }
                        if (!msg) {
                            msg = @"";
                        }
                        [weakUploadLock lock];
                        [uploadStatusDic setObject:@[msg] forKey:[NSString stringWithFormat:@"%@-%p",_myRainbowSharingConversation.conversationId, aFile]];
                        [weakUploadLock signal];
                        [weakUploadLock unlock];
                    });
                }];
            }
            [_uploadLock lock];
            while (uploadStatusDic.count != _filesToShare.count) {
                [_uploadLock wait];
            }
            [_uploadLock unlock];
            
            for (Conversation *conversation in _selectedItems) {
                [uploadedFiles enumerateObjectsUsingBlock:^(File * uploadedFile, NSUInteger idx, BOOL * stop) {
                    if(conversation == _myRainbowSharingConversation){
                        [sendMessageStatusDic setObject:@[@""] forKey:[NSString stringWithFormat:@"%@-%p",conversation.conversationId, uploadedFile]];
                    } else {
                        NSString *text = _textView.text;
                        if(idx == 0 && _filesToShare.count > 1){
                            [_conversationsManagerService sendMessage:text fileAttachment:nil to:conversation completionHandler:nil attachmentUploadProgressHandler:nil];
                            [NSThread sleepForTimeInterval:0.5];
                        }
                        
                        [_conversationsManagerService sendMessage:(idx == 0 && _filesToShare.count == 1)?text:nil fileAttachment:uploadedFile to:conversation completionHandler:^(Message *message, NSError *error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                NSString *msg = @"";
                                if (error) {
                                    msg = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
                                    if(error.code == 413){
                                        msg = NSLocalizedString(@"Attachment reach allowed transfer limit", nil);
                                    }
                                }
                                [weakSendMessageLock lock];
                                [sendMessageStatusDic setObject:@[msg] forKey:[NSString stringWithFormat:@"%@-%p",conversation.conversationId, uploadedFile]];
                                [weakSendMessageLock signal];
                                [weakSendMessageLock unlock];
                            });
                        } attachmentUploadProgressHandler:nil];
                    }
                }];
            }
            
            [_sendMessageLock lock];
            while (sendMessageStatusDic.count != _selectedItems.count * uploadedFiles.count) {
                [_sendMessageLock wait];
            }
            [_sendMessageLock unlock];
        } else {
            for (Conversation * conversation in _selectedItems) {
                NSString *text = _textView.text;
                [_conversationsManagerService sendMessage:text fileAttachment:nil to:conversation completionHandler:^(Message *message, NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *msg = @"";
                        if (error) {
                            msg = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
                            if(error.code == 413){
                                msg = NSLocalizedString(@"Attachment reach allowed transfer limit", nil);
                            }
                        }
                        [weakSendMessageLock lock];
                        [sendMessageStatusDic setObject:@[msg] forKey:[NSString stringWithFormat:@"%@-%lu",conversation.conversationId, (unsigned long)[text hash]]];
                        [weakSendMessageLock signal];
                        [weakSendMessageLock unlock];
                    });
                } attachmentUploadProgressHandler:nil];
            }
            [_sendMessageLock lock];
            while (sendMessageStatusDic.count != _selectedItems.count) {
                [_sendMessageLock wait];
            }
            [_sendMessageLock unlock];
        }
    } completionBlock:^{
        NSString *firstUploadErrorMsg = [[[uploadStatusDic allValues] firstObject] firstObject];
        NSString *firstSendErrorMsg = [[[sendMessageStatusDic allValues] firstObject] firstObject];
        if(firstUploadErrorMsg.length > 0 || firstSendErrorMsg.length > 0){
            NSString *msg = (firstUploadErrorMsg.length > 0)?firstUploadErrorMsg:firstSendErrorMsg;
            [self showAlertWithTitle:NSLocalizedString(@"Cannot send message", nil) message:msg completionHandler:^{
                [self didTapCancelButton:nil];
            }];
        } else {
            MBProgressHUD *uploadDoneHUD = [UITools showHUDWithMessage:NSLocalizedString(@"Sent", nil) forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Checkmark" dismissCompletionBlock:nil];
            [uploadDoneHUD showAnimated:YES whileExecutingBlock:^{
                [NSThread sleepForTimeInterval:1.5];
            } completionBlock:^{
                [_serviceManager.loginManager disconnect];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)),
                    dispatch_get_main_queue(), ^{
                    [self.extensionContext completeRequestReturningItems:self.extensionContext.inputItems completionHandler:nil];
                });
            }];
        }
    }];
}

#pragma mark - Button actions
- (IBAction)didTapSendButton:(UIButton *)sender {
    [self share];
}

- (IBAction)didTapCancelButton:(UIBarButtonItem *)sender {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0), ^{
        [_serviceManager.loginManager disconnect];
    });
    [self.extensionContext cancelRequestWithError:[NSError errorWithDomain:@"RainbowShareExtension" code:500 userInfo:nil]];
}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Selected items
    if(section == 0)
        return [_selectedItems count];
    else
        return [_conversations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    
    NSObject *object = nil;
    if(indexPath.section == 0)
        object = [_selectedItems objectAtIndex:indexPath.row];
    else
        object = [_conversations objectAtIndex:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.cellObject = (NSObject<UIRainbowGenericTableViewCellProtocol>*)object;
    cell.contentView.alpha = 1;
    cell.userInteractionEnabled = YES;
    if([object isEqual:_myRainbowSharingConversation]){
        [cell.avatarView setAsCircle:NO];
        cell.dateLabel.hidden = YES;
        if(_filesToShare.count == 0){
            cell.contentView.alpha = 0.5;
            cell.userInteractionEnabled = NO;
        }
    } else {
        [cell.avatarView setAsCircle:YES];
        cell.dateLabel.hidden = NO;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        [self tableView:tableView didDeselectRowAtIndexPath:indexPath];
    }
    
    if(indexPath.section == 1){
        Conversation *conversation = [_conversations objectAtIndex:indexPath.row];
        [_selectedItems addObject:conversation];
        [_selectedItems sortUsingDescriptors:@[[UITools sortDescriptorByLastMessageDate]]];
        [_conversations removeObject:conversation];
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:[_selectedItems indexOfObject:conversation] inSection:0];
        // move object from current section to first section
        [tableView beginUpdates];
        [tableView moveRowAtIndexPath:indexPath toIndexPath:newIndexPath];
        [tableView endUpdates];
        [self showHideSendButton];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        Conversation *conversation = [_selectedItems objectAtIndex:indexPath.row];
        [tableView beginUpdates];
        [_selectedItems removeObjectAtIndex:indexPath.row];
        [self getConversations];
        NSInteger conversationIndex = [_conversations indexOfObject:conversation];
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:conversationIndex inSection:1]];
        [tableView endUpdates];
        
        [self showHideSendButton];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 0){
        return 0;
    }
    return 44;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return nil;
    } else
        return NSLocalizedString(@"Recent conversations", nil);
}

#pragma mark - Notifications
-(void) didEndLoadingConversations:(NSNotification *) notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self getConversations];
        [_tableView reloadData];
    });
}

#pragma mark - CustomSearchBar delegate
-(void) customSearchController:(CustomSearchController * _Nonnull) customSearchController didPressCancelButton :(UISearchBar * _Nonnull ) searchBar {
    
}

-(void) willPresentSearchController:(CustomSearchController * _Nonnull) customSearchController  {
    [self.navigationItem setLeftBarButtonItems:nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.titleView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
}

-(void) willDismissSearchController:(CustomSearchController * _Nonnull) customSearchController {
    ((CustomSearchBarContainerView*)self.navigationController.navigationItem.titleView).frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
    self.navigationItem.rightBarButtonItem = _cancelButton;
}

#pragma mark - Alert message
-(void) showAlertWithTitle:(NSString *) title message:(NSString *) message completionHandler:(void(^)())completionHandler {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:(UIAlertActionStyleDefault) handler:completionHandler?completionHandler:nil];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - TextView delegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
    CGFloat height = self.navigationController.navigationBar.frame.size.height + _attachmentImageHeigthConstraint.constant + 100;
    _maskViewHeightConstraint.constant = -height;
    _maskView.hidden = NO;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    _maskView.hidden = YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location != NSNotFound ){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (IBAction)didTapMaskView:(UITapGestureRecognizer *)sender {
    [_textView resignFirstResponder];
}

#pragma mark - HockeyApp delegate
-(NSString *)userIDForHockeyManager:(BITHockeyManager *)hockeyManager componentManager:(BITHockeyBaseManager *)componentManager {
    return [ServicesManager sharedInstance].myUser.contact.jid;
}

@end
