/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NotificationService.h"
#import <Rainbow/Rainbow.h>

@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;
@property (nonatomic) BOOL hasCompletedAction;

@end

@implementation NotificationService

-(instancetype) init {
    self = [super init];
    if(self){
        [RainbowUserDefaults setSuiteName:@"group.com.alcatellucent.otcl"];
        [[LogsRecorder sharedInstance] startRecord];
        [ServicesManager sharedInstance];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConversation:) name:kConversationsManagerDidUpdateConversation object:nil];
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidUpdateConversation object:nil];
    [[LogsRecorder sharedInstance] stopRecord];
}

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    NSLog(@"[NotificationService] NotificationRequest: %@", [Tools anonymizeNotificationRequest:request.description]);
    if ([request.content.userInfo isKindOfClass:[NSDictionary class]]) {
        NSLog(@"[NotificationService] didReceiveNotificationRequest %@", [Tools anonymizeNotificationUserInfo:request.content.userInfo]);
    }
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];

    [[ServicesManager sharedInstance].notificationsManager saveNotificationWithUserInfo:self.bestAttemptContent.userInfo];
    
    while (!_hasCompletedAction) {
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.2]];
    }
    [[RainbowUserDefaults sharedInstance] setBool:YES forKey:@"shouldRefreshConversationUI"];
    self.contentHandler(self.bestAttemptContent);
}

- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    NSLog(@"[NotificationService] serviceExtensionTimeWillExpire");
    self.contentHandler(self.bestAttemptContent);
}

-(void) didUpdateConversation:(NSNotification *) notification {
    NSLog(@"[NotificationService] didUpdateConversation %@", notification);
    _hasCompletedAction = YES;
}

@end
