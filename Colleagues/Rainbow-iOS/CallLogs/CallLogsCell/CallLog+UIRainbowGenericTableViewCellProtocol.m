/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Rainbow/CallLog.h>
#import "UITools.h"

@implementation CallLog (UIRainbowGenericTableViewCellProtocol)
-(NSString *) mainLabel {
    return self.peer.displayName;
}

-(UIColor *) mainLabelTextColor {
    if([self.peer isKindOfClass:[Contact class]]){
        Contact *contact = (Contact *)self.peer;
        if( contact.calendarPresence.presence == CalendarPresenceOutOfOffice || contact.calendarPresence.automaticReply.isEnabled) {
            return [UITools defaultTintColor];
        } else if( contact.calendarPresence.presence == CalendarPresenceBusy ) {
            return [UITools defaultTintColor];
        } else {
            return [UIColor blackColor];
        }
    } else {
        return [UIColor blackColor];
    }
    return [UIColor blackColor];
}

-(NSString *) subLabel {
    Contact *contact = (Contact*)self.peer;
    
    if( contact.isPresenceSubscribed ) {
        return [UITools getPresenceText:contact];
    } else if( contact.companyName ) {
        return contact.companyName;
    } else if( !contact.isRainbowUser && contact.phoneNumbers.count > 0 ) {
        NSString *number = ((PhoneNumber *)[contact.phoneNumbers firstObject]).number;
        return (![number isEqualToString:self.peer.displayName]) ? number : nil;
    } else {
        return nil;
    }
}

-(Peer *) avatar {
    return (Peer*)self.peer;
}

-(NSArray *) observablesKeyPath {
    return @[kContactLastNameKey,kContactFirstNameKey];
}

-(NSInteger) badgeValue {
    return 0;
}

-(NSString *) cellButtonTitle {
    return nil;
}

-(UIImage *) cellButtonImage {
    return nil;
}

-(UIImage *) rightIcon {
    UIImage *icon;
    
    if( self.service == CallLogServiceConference ) {
        icon = [UIImage imageNamed:@"conference"];
    } else if( self.state == CallLogStateMissed && !self.isOutgoing) {
        icon = (self.type == CallLogTypeWebRTC) ? [UIImage imageNamed:@"incomingMissedCall"] : [UIImage imageNamed:@"incomingMissedCallPBX"];
    } else if( self.state == CallLogStateMissed && self.isOutgoing) {
        icon = (self.type == CallLogTypeWebRTC) ? [UIImage imageNamed:@"outgoingMissedCall"] : [UIImage imageNamed:@"outgoingMissedCallPBX"];
    } else if( self.state == CallLogStateAnswered && !self.isOutgoing) {
        icon = (self.type == CallLogTypeWebRTC) ? [UIImage imageNamed:@"incomingCall"] : [UIImage imageNamed:@"incomingCallPBX"];
    } else {
        icon = (self.type == CallLogTypeWebRTC) ? [UIImage imageNamed:@"outgoingCall"] : [UIImage imageNamed:@"outgoingCallPBX"];
    }
    
    return icon;
}

-(UIColor *) rightIconTintColor {
    UIColor *tintColor;
    
    if( self.service == CallLogServiceConference ) {
        tintColor = [UITools defaultTintColor];
    } else if( self.state == CallLogStateMissed && !self.isOutgoing) {
        tintColor = [UIColor redColor];
    } else if( self.state == CallLogStateMissed && self.isOutgoing) {
        tintColor = [UIColor redColor];
    } else if( self.state == CallLogStateAnswered && !self.isOutgoing) {
        tintColor = [UITools defaultTintColor];
    } else {
        tintColor = [UITools defaultTintColor];
    }
    
    return tintColor;
}
@end
