/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UITools.h"
#import "SCLAlertView.h"
#import "UICallLogsViewController.h"
#import "UIRainbowGenericTableViewCell.h"
#import "UICallLogsDetailsViewController.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "MyInfoNavigationItem.h"
#import "DZNSegmentedControl.h"
#import "FilteredSortedSectionedArray.h"
#import "Contact+Extensions.h"
#import <Rainbow/CallLog.h>
#import <Rainbow/ServicesManager.h>
#import "UIContactDetailsViewController.h"
#import "CustomNavigationController.h"
#import "UINetworkLostViewController.h"
#import "UIStoryboardManager.h"
#import "UINetworkLostViewController.h"
#import "UIGuestModeViewController.h"
#import "UIView+MGBadgeView.h"

typedef NS_ENUM(NSInteger, SelectorType) {
    SelectorTypeCallHistory = 0,
    SelectorTypeVoicemail
};

@interface DZNSegmentedControl ()
- (UIButton *)buttonAtIndex:(NSUInteger)segment;
@end

@interface UICallLogsViewController() <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, UIActionSheetDelegate, DZNSegmentedControlDelegate, UITableViewDelegate, UITableViewDataSource>
{
    BOOL isNetworkConnected;
}
@property (nonatomic) BOOL lostNetworkViewAsBeenScrolled;
@property (nonatomic) BOOL guestBannerAsBeenScrolled;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) IBOutlet UIBarButtonItem *editBarButtonItem;
@property (strong, nonatomic) IBOutlet UIToolbar *footerToolBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *deleteAllButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *deleteSelectionButton;
@property (nonatomic, strong) IBOutlet DZNSegmentedControl *tabSelector;

@property (nonatomic, strong) NSPredicate *globalFilterAll;
@property (nonatomic, strong) NSPredicate *globalFilterMissed;
@property (nonatomic, strong) NSPredicate *globalFilterNone;
@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) FilteredSortedSectionedArray<CallLog *> *callLogsArray;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;
@property (nonatomic, strong) CallLogsService *callLogsService;
@property (nonatomic, strong) TelephonyService *telephonyService;
@property (nonatomic, strong) UIButton *tabSelectorVoicemailButton;
@property (nonatomic) NSInteger voicemailCount;

@end

@implementation UICallLogsViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        isNetworkConnected = YES;
        
        _callLogsService = [ServicesManager sharedInstance].callLogsService;
        _telephonyService = [ServicesManager sharedInstance].telephonyService;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddCallLog:) name:kCallLogsServiceDidAddCallLog object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFetchCallLogs:) name:kCallLogsServiceDidFetchCallLogs object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveAllCallLogs:) name:kCallLogsServiceDidRemoveAllCallLogs object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveCallLogs:) name:kCallLogsServiceDidRemoveCallLogs object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateVoicemailEntries) name:kMyUserVoicemailCountDidUpdate object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTryToReconnect:) name:kLoginManagerTryToReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideNetworkLostView:) name:CustomNavigationControllerDidHideNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowNetworkLostView:) name:CustomNavigationControllerWillShowNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNetworkLostViewFrame:) name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowGuestModeView:) name:CustomNavigationControllerWillShowGuestModeView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideGuestModeView:) name:CustomNavigationControllerDidHideGuestModeView object:nil];
    
        
        _sectionedByName = ^NSString*(CallLog *callLog) {
            if(callLog.service == CallLogServiceConference || [callLog.peer.displayName rangeOfString:@"^[*]*$" options:NSRegularExpressionSearch].location != NSNotFound)
                return [NSString stringWithFormat:@"%@-%p",callLog.peer.displayName, callLog];
            return callLog.peer.displayName;
        };
        
        // Global filter to have all call logs
        _globalFilterAll = [NSPredicate predicateWithBlock:^BOOL(CallLog *callLog, NSDictionary<NSString *,id> * bindings) {
            if([callLog.peer.displayName length] < 1)
                return NO; // Do no display entries with no identity
            return YES;
        }];
        
        // Global filter only missed call logs
        _globalFilterMissed = [NSPredicate predicateWithBlock:^BOOL(CallLog *callLog, NSDictionary<NSString *,id> * bindings) {
            if([callLog.peer.displayName length] < 1)
                return NO; // Do no display entries with no identity
            
            NSInteger pos = [_callLogsArray indexOfObjectInHisSection:callLog];
            // We want to display the section only if the first object of this section is a missed call
            if(pos == 0)
                return callLog.state == CallLogStateMissed && !callLog.isOutgoing;
            else
                return NO;
        }];
        
        //
        _globalFilterNone = [NSPredicate predicateWithBlock:^BOOL(CallLog *callLog, NSDictionary<NSString *,id> * bindings) {
            return NO;
        }];
        
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            NSArray <CallLog*> *obj1SectionContent = [_callLogsArray objectsInSection:obj1];
            NSArray <CallLog*> *obj2SectionContent = [_callLogsArray objectsInSection:obj2];
            CallLog *firstObj1 = [obj1SectionContent firstObject];
            CallLog *firstObj2 = [obj2SectionContent firstObject];
            return [firstObj1.date compare:firstObj2.date];
        }];
        
        // All call logs
        _callLogsArray = [FilteredSortedSectionedArray new];
        
        // Default filters/sorter/.. values
        _callLogsArray.sectionNameFromObjectComputationBlock = _sectionedByName;
        _callLogsArray.globalFilteringPredicate = _globalFilterAll;
        _callLogsArray.sectionSortDescriptor = _sortSectionAsc;

        // __default__ has a special meaning : any other sections not found in the dictionary
        _callLogsArray.objectSortDescriptorForSection = @{@"__default__": @[[self sortDescriptorForCallLogsByDate]]};
    }
    
    return self;
}

-(void) dealloc {
    _callLogsService = nil;
    _telephonyService = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCallLogsServiceDidFetchCallLogs object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCallLogsServiceDidAddCallLog object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCallLogsServiceDidRemoveAllCallLogs object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCallLogsServiceDidRemoveCallLogs object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserVoicemailCountDidUpdate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerTryToReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideGuestModeView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowGuestModeView object:nil];
    
    _sortSectionAsc = nil;
    _globalFilterAll = nil;
    _globalFilterMissed = nil;
    _globalFilterNone = nil;
    _sectionedByName = nil;
    _callLogsArray = nil;
}

- (NSSortDescriptor *) sortDescriptorForCallLogsByDate {
    NSSortDescriptor *sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO comparator:^NSComparisonResult(NSDate* obj1, NSDate* obj2) {
        return [obj1 compare:obj2];
    }];
    return sortByDate;
}

#pragma mark - Rotation
-(BOOL) shouldAutorotate {
    return NO;
}

#pragma mark - Style
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void) shouldUpdateVoicemailEntries {
    if([NSThread isMainThread]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [self shouldUpdateVoicemailEntries];
        });
        return;
    }
    NSAssert(![NSThread isMainThread], @"This code must not be invoked on main thread !!!");
    
    _voicemailCount = [ServicesManager sharedInstance].myUser.voiceMailCount;
    
    if([self isViewLoaded]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update badge count
            _tabSelectorVoicemailButton.titleLabel.badgeView.badgeValue = _voicemailCount;
            [self.tableView reloadData];
            [self.view setNeedsLayout];
        });
    }
}

#pragma mark - view life cycle
- (void)viewDidLoad {
    [super viewDidLoad];

    ((MyInfoNavigationItem*)self.navigationItem).parentViewController = self;
    
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    self.title = NSLocalizedString(@"Recents", nil);
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];
    
    self.definesPresentationContext = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    // Footer edit toolbar
    _deleteAllButton.title = NSLocalizedString(@"Delete all", nil);
    [self updateDeleteSelectionButtonTitle];
    
    _deleteAllButton.tintColor = [UITools defaultTintColor];
    _deleteSelectionButton.tintColor = [UITools defaultTintColor];
    [_footerToolBar setFrame:CGRectMake(0, self.tabBarController.tabBar.frame.origin.y-44, self.tabBarController.tabBar.frame.size.width, 44)];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
    
    [_tabSelector setItems:@[NSLocalizedString(@"All", nil), NSLocalizedString(@"Missed", nil), NSLocalizedString(@"Messaging", nil)]];
    _tabSelector.delegate = self;
    _tabSelector.showsCount = NO;
    _tabSelector.autoAdjustSelectionIndicatorWidth = NO;
    _tabSelector.height = 60;
    _tabSelector.selectionIndicatorHeight = 4.0f;
    _tabSelector.bouncySelectionIndicator = YES;
    
    //
    _tabSelectorVoicemailButton = [_tabSelector buttonAtIndex:2];
    _tabSelectorVoicemailButton.titleLabel.badgeView.badgeColor = [UITools notificationBadgeColor];
    _tabSelectorVoicemailButton.titleLabel.badgeView.outlineWidth = 2.0;
    _tabSelectorVoicemailButton.titleLabel.badgeView.position = MGBadgePositionTopRight;
    _tabSelectorVoicemailButton.titleLabel.badgeView.textColor = [UIColor whiteColor];
    _tabSelectorVoicemailButton.titleLabel.badgeView.font = [UIFont fontWithName:[UITools defaultFontName] size:11];
    _tabSelectorVoicemailButton.titleLabel.badgeView.displayIfZero = NO;
    _tabSelectorVoicemailButton.titleLabel.badgeView.bageStyle = MGBadgeStyleRoundRect;
    
    
    [_tabSelector addTarget:self action:@selector(didFilterButtonValueChanged:) forControlEvents:UIControlEventValueChanged];
    CGRect frame = _tabSelector.frame;
    frame.size.width = self.view.frame.size.width;
    _tabSelector.frame = frame;
    
    
    [self displayGoodButtonForSelectorType:_tabSelector.selectedSegmentIndex];
    UIEdgeInsets inset = self.tableView.contentInset;
    inset.bottom += _tabSelector.height;
    self.tableView.contentInset = inset;
    self.tableView.scrollIndicatorInsets = inset;
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_callLogsService markAllCallLogsAsRead];
    
    CGSize b1textSize = [_tabSelectorVoicemailButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:_tabSelectorVoicemailButton.titleLabel.font}];
    if(b1textSize.width > _tabSelectorVoicemailButton.frame.size.width )
        // Truncated text
        _tabSelectorVoicemailButton.titleLabel.badgeView.horizontalOffset = -10;
    else
        _tabSelectorVoicemailButton.titleLabel.badgeView.horizontalOffset = 0;

    
    [self shouldUpdateVoicemailEntries];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    BOOL isDisplayingNetworkView = ((CustomNavigationController *)self.navigationController).isDisplayingNetworkLostView;
    if(isDisplayingNetworkView  && !_lostNetworkViewAsBeenScrolled){
        [self didLostConnection:nil];
        [self willShowNetworkLostView:nil];
    }
    
    BOOL isGuestBannerView = ((CustomNavigationController *)self.navigationController).isDisplayingGuestModeView;
    if(isGuestBannerView  && !_guestBannerAsBeenScrolled){
        [self willShowGuestModeView:nil];
    }
}


-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(self.tableView.isEditing)
        [self didTapBarButtonItem:_editBarButtonItem];
    
    if( _lostNetworkViewAsBeenScrolled){
        [self didHideNetworkLostView:nil];
    }
    if(_guestBannerAsBeenScrolled){
        [self didHideGuestModeView:nil];
    }
}



-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
   // [super didLostConnection:notification];
   
    if([self isViewLoaded]){
        _editBarButtonItem.enabled = NO;
        
    }
}

-(void) didTryToReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didTryToReconnect:notification];
        });
        return;
    }
    
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    //[super didReconnect:notification];
    
    if([self isViewLoaded]){
        _editBarButtonItem.enabled = YES;
       
    }
}

-(void) didChangeNetworkLostViewFrame:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
}

-(void) willShowNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant += 30;
        _lostNetworkViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            isNetworkConnected = NO;
        }];
    }
}

-(void) didHideNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant -= kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            isNetworkConnected = YES;
        }];
    }
}

-(void) didHideGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _guestBannerAsBeenScrolled){
        self.topConstraint.constant -= kGuestModeHeight;
        _guestBannerAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:nil];
    }
}

-(void) willShowGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_guestBannerAsBeenScrolled){
        self.topConstraint.constant += kGuestModeHeight;
        _guestBannerAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            isNetworkConnected = NO;
        }];
    }
}

-(void) displayGoodButtonForSelectorType:(SelectorType) selectorType {
    NSMutableArray <UIBarButtonItem*> *buttons = [NSMutableArray array];
    [buttons addObject:_editBarButtonItem];
    ((MyInfoNavigationItem*)self.navigationItem).customRightItems = buttons;
}

- (IBAction)didTapBarButtonItem:(UIBarButtonItem *)sender {
    [self.tableView setEditing:!self.tableView.isEditing animated:YES];
    if(self.tableView.isEditing){
        [sender setImage:[UIImage imageNamed:@"Checkmark_status"]];
        UIWindow *window = [UIApplication sharedApplication].windows[0];
        [window addSubview:_footerToolBar];
        [self updateDeleteSelectionButtonTitle];
    } else {
        [sender setImage:[UIImage imageNamed:@"Modify"]];
        [_footerToolBar removeFromSuperview];
    }
}

#pragma mark - table view delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_callLogsArray sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_callLogsArray sections] objectAtIndex:section];
    if([[_callLogsArray objectsInSection:key] count] > 0)
        return 1;
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_callLogsArray sections] objectAtIndex:indexPath.section];
    CallLog *callLog = [[_callLogsArray objectsInSection:key] objectAtIndex:indexPath.row];
    
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];

    cell.cellObject = (NSObject<UIRainbowGenericTableViewCellProtocol>*)callLog;
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    return cell;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {    
    //Perform a segue.
    NSString *key = [[_callLogsArray sections] objectAtIndex:indexPath.section];
    
    UICallLogsDetailsViewController *callLogDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"callLogDetailsViewID"];
    NSArray<CallLog*> *callLogs = [_callLogsArray objectsInSection:key];
    callLogDetailViewController.callLogs = callLogs;
    [self.navigationController pushViewController:callLogDetailViewController animated:YES];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_callLogsArray sections] objectAtIndex:indexPath.section];
    CallLog *callLog = [[_callLogsArray objectsInSection:key] objectAtIndex:indexPath.row];
    
    UITableViewRowAction *remove = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title: NSLocalizedString(@"Delete", nil) handler: ^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [_callLogsService deleteCallLogWithPeer:(Contact*)callLog.peer];
        [self removeCallLog:callLog];
       
    }];

    return @[remove];
}

#pragma mark - DZNEmptyDataSet
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    if(_tabSelector.selectedSegmentIndex == 2)
        return [UIImage imageNamed:@"voicemail"];
    
    return [UIImage imageNamed:@"emptyCallLog"];
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView {
    if(_tabSelector.selectedSegmentIndex == 2)
        return [UIColor lightGrayColor];

    return [UITools defaultTintColor];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    if(_tabSelector.selectedSegmentIndex == 2 && [[ServicesManager sharedInstance].myUser.voiceMailNumber length] > 0)
        return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Call your voicemail", nil) attributes:attributes];
    
    else
        return nil;
}

-(UIImage *) buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    return [UITools imageWithColor:[UITools defaultTintColor] size:CGSizeMake(1, 40)];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"You have no recent events.", nil);
    if(_tabSelector.selectedSegmentIndex == 1)
        text = NSLocalizedString(@"No missed events", nil);
    else if(_tabSelector.selectedSegmentIndex == 2) {
        if([[ServicesManager sharedInstance].myUser.voiceMailNumber length] > 0) {
            _voicemailCount = [ServicesManager sharedInstance].myUser.voiceMailCount;
            if(_voicemailCount == 1)
                text = NSLocalizedString(@"You have a new voice message", nil);
            else if(_voicemailCount > 1)
                text = [NSString stringWithFormat:NSLocalizedString(@"You have %ld new voice messages", nil), _voicemailCount];
            else
                text = NSLocalizedString(@"You have no new voice message", nil);
        } else {
            text = NSLocalizedString(@"Sorry but you have no voicemail associated with your account", nil);
        }
    }
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    CGFloat offset = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
    offset += CGRectGetHeight(self.navigationController.navigationBar.frame);
    offset += CGRectGetHeight(_tabSelector.frame);
    return -offset;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 0.0f;
}

- (void)emptyDataSetDidTapButton:(UIScrollView *)scrollView {
    if(_tabSelector.selectedSegmentIndex == 2) {
        NSString *voiceMailNumber = [ServicesManager sharedInstance].myUser.voiceMailNumber;
        if([voiceMailNumber length] > 0) {
            PhoneNumber *phone = [TelephonyService getPhoneNumberFromString:[ServicesManager sharedInstance].myUser.voiceMailNumber];
            
            // Make call to voicemail
            [UITools makeCallToPhoneNumber:phone inController:self];
        }
    }
}

#pragma mark - Alert Action
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(!tableView.isEditing && isNetworkConnected) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        UIAlertController *callActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        NSString *key = [[_callLogsArray sections] objectAtIndex:indexPath.section];
        CallLog *callLog = [[_callLogsArray objectsInSection:key] objectAtIndex:indexPath.row];
        Contact *contact = (Contact*)callLog.peer;
        
        // Do not create call actions for anonymous
        if([contact.displayName rangeOfString:@"^[*]*$" options:NSRegularExpressionSearch].location != NSNotFound)
            return;
        
        // Rainbow User
        // Video call
        if( contact.canChatWith && contact.canCallInWebRTCVideo ) {
            UIAlertAction* videoCall = [UIAlertAction actionWithTitle:NSLocalizedString(@"RTC video call", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [self makeCallTo:contact features:(RTCCallFeatureAudio|RTCCallFeatureLocalVideo)];
            }];
            [videoCall setValue:[[UIImage imageNamed:@"video"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
            [callActionSheet addAction:videoCall];
        }

        // Audio call
        if( contact.canChatWith && contact.canCallInWebRTC) {
            UIAlertAction* audioCall = [UIAlertAction actionWithTitle:NSLocalizedString(@"RTC audio call", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [self makeCallTo:contact features:(RTCCallFeatureAudio|RTCCallFeatureAudio)];
            }];
            [audioCall setValue:[[UIImage imageNamed:@"headset"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
            [callActionSheet addAction:audioCall];
        }
        
        
        // Contact (caller or callee) phone numbers
        [contact.phoneNumbers enumerateObjectsUsingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
            NSString *label = aPhoneNumber.label;
            if (aPhoneNumber.deviceType == PhoneNumberDeviceTypeMobile){
                switch (aPhoneNumber.type) {
                    case PhoneNumberTypeHome:
                        label = NSLocalizedString(@"Personal Mobile", nil);
                        break;
                    case PhoneNumberTypeWork:
                        label = NSLocalizedString(@"Professional Mobile", nil);
                        break;
                    case PhoneNumberTypeOther:
                        label = NSLocalizedString(@"Professional Mobile", nil);
                        break;
                    default:
                        label = NSLocalizedString(@"Cell", nil);
                        break;
                }
            } else if (aPhoneNumber.deviceType == PhoneNumberDeviceTypeLandline){
                switch (aPhoneNumber.type) {
                    case PhoneNumberTypeHome:
                        label = NSLocalizedString(@"Personal", nil);
                        break;
                    case PhoneNumberTypeWork:
                        label = NSLocalizedString(@"Professional", nil);
                        break;
                    default:
                        label = NSLocalizedString(@"Work", nil);
                        break;
                }
            } else {
                label = NSLocalizedString(aPhoneNumber.label, nil);
            }
            
            UIAlertAction *callAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@ : %@",label, aPhoneNumber.number] style:UIAlertActionStyleDefault handler: ^(UIAlertAction * action) {
                [UITools makeCallToPhoneNumber:aPhoneNumber inController:self];
            }];
            
            [callActionSheet addAction:callAction];
        }];
        
        // Present menu only if at least one item has been added
        if( callActionSheet.actions.count > 0 ) {
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
            [callActionSheet addAction:cancel];
            
            // show the menu.
            [callActionSheet.view setTintColor:[UITools defaultTintColor]];
            // Workaround for iOS issue not showing immediatelly the alertcontroller
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:callActionSheet animated:YES completion:nil];
            });
        }
    }
    else {
        [self updateDeleteSelectionButtonTitle];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self updateDeleteSelectionButtonTitle];
}

-(void) makeCallTo:(Contact *) contact features:(RTCCallFeatureFlags) features {
    if([ServicesManager sharedInstance].rtcService.microphoneAccessGranted){
        [[ServicesManager sharedInstance].rtcService beginNewOutgoingCallWithPeer:contact withFeatures:features];
    } else {
        [UITools showMicrophoneBlockedPopupInController:self];
    }
}

- (IBAction)didFilterButtonValueChanged:(UISegmentedControl *)sender {
    if(sender.selectedSegmentIndex == 0) {
        _callLogsArray.globalFilteringPredicate = _globalFilterAll;
    }
    else if(sender.selectedSegmentIndex == 1) {
        _callLogsArray.globalFilteringPredicate = _globalFilterMissed;
    }
    else {
        _callLogsArray.globalFilteringPredicate = _globalFilterNone;
    }
    
    if([self isViewLoaded]) {
        [self.tableView reloadData];
        [self updateDeleteSelectionButtonTitle];
    }
}

#pragma mark - local data model
-(void) insertCallLog:(CallLog *) callLog {
    if (![_callLogsArray containsObject:callLog]) {
        [_callLogsArray addObject:callLog];
    }
    
    if([self isViewLoaded])
        [self.tableView reloadData];
}

-(void) removeCallLog:(CallLog *) callLog {
    if ([_callLogsArray containsObject:callLog]) {
         [_callLogsArray removeObject:callLog];
    }
   
    if([self isViewLoaded])
        [self.tableView reloadData];
}

#pragma mark - CallLog service notifications

-(void) didAddCallLog:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddCallLog:notification];
        });
        return;
    }
    
    CallLog *callLog = (CallLog *)notification.object;
    [self insertCallLog:callLog];
}

-(void) didFetchCallLogs:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didFetchCallLogs:notification];
        });
        return;
    }
    
    NSArray<CallLog*> *callLogs = _callLogsService.callLogs;
    [callLogs enumerateObjectsUsingBlock:^(CallLog * callLog, NSUInteger idx, BOOL * stop) {
        [self insertCallLog:callLog];
    }];
}
    
-(void) didRemoveAllCallLogs:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveAllCallLogs:notification];
        });
        return;
    }
    [_callLogsArray removeAllObjects];
    [_callLogsArray reloadData];
    
    if([self isViewLoaded]) {
        [self.tableView reloadData];
        
        if(self.tableView.isEditing) {
            [self updateDeleteSelectionButtonTitle];
            [self didTapBarButtonItem:_editBarButtonItem];
        }
    }
}

-(void) didRemoveCallLogs:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveCallLogs:notification];
        });
        return;
    }
    
    NSArray <CallLog *> *calllogs = (NSArray *) notification.object;
    NSLog(@"did remove callLogs %@", calllogs);
    [calllogs enumerateObjectsUsingBlock:^(CallLog * aCallLog, NSUInteger idx, BOOL * stop) {
        [self removeCallLog:aCallLog];
    }];
    
    [_callLogsArray reloadData];
    if([self isViewLoaded]){
        if(self.tableView.isEditing) {
            [self updateDeleteSelectionButtonTitle];
            
            if(_callLogsArray.count == 0) {
                [self didTapBarButtonItem:_editBarButtonItem];
            }
        }
        
        [self.tableView reloadData];
    }
}

-(void) updateDeleteSelectionButtonTitle {
    // Update the delete button's title, based on how many items are selected
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    
    BOOL noItemsAreSelected = selectedRows.count == 0;
    
    NSString *titleFormatString = NSLocalizedString(@"Delete (%d)", nil);
    _deleteSelectionButton.title = [NSString stringWithFormat:titleFormatString, selectedRows.count];
    _deleteSelectionButton.enabled = !noItemsAreSelected;
    _deleteAllButton.enabled = !(_callLogsArray.count == 0);

}

- (IBAction)didTapDeleteAllButton:(UIBarButtonItem *)sender {
    // Show a confirmation popup.
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    alert.customViewColor = [UITools defaultTintColor]; // button color and top icon background
    alert.iconTintColor = [UITools defaultBackgroundColor]; // top icon font color
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = YES;
    alert.labelTitle.numberOfLines = 2;
    alert.labelTitle.lineBreakMode = NSLineBreakByWordWrapping;
    alert.labelTitle.textAlignment = NSTextAlignmentCenter;

    [alert addButton:NSLocalizedString(@"Yes", nil) actionBlock:^(void) {
        [_callLogsService deleteAllCallLogs];
        [_callLogsArray removeAllObjects];
      
        if([self isViewLoaded]) {
            [self.tableView reloadData];
  
        }
  
    }];

    [alert showInfo:self
              title:NSLocalizedString(@"Delete all", nil)
           subTitle:NSLocalizedString(@"Whole call history will be suppressed", nil)
   closeButtonTitle:NSLocalizedString(@"No", nil)
           duration:0.0f];
    
}

- (IBAction)didTapDeleteSelectedButtonItem:(UIBarButtonItem *)sender {
    
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    
    [selectedRows enumerateObjectsUsingBlock:^(NSIndexPath * indexPath, NSUInteger idx, BOOL * stop) {
        
        NSString *key = [[_callLogsArray sections] objectAtIndex:indexPath.section];
        if ( [_callLogsArray objectsInSection:key].count > indexPath.row) {
            CallLog *callLog = [[_callLogsArray objectsInSection:key] objectAtIndex:indexPath.row];
            Contact *contact = (Contact*)callLog.peer;
            
            [_callLogsService deleteCallLogWithPeer:contact];
            [self removeCallLog:callLog];
        }
    
    }];
}

#pragma mark - search bar controller notifications
- (void) presentContactDetailController:(Contact*) contact{
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}


-(void) scrollToTop {
    [self.tableView setContentOffset:CGPointMake(0.0f, -self.tableView.contentInset.top) animated:YES];
}
@end
