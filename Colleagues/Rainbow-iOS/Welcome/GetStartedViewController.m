/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE (i love beer) International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "WelcomePageCommonViewController.h"
#import "UITools.h"
#import <Rainbow/defines.h>
#import "UINotificationManager.h"
#import <Rainbow/ServicesManager.h>
#import <Photos/Photos.h>
#import "GoogleAnalytics.h"

@interface GetStartedViewController : WelcomePageCommonViewController
@end

@interface GetStartedViewController () <UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *presentationLabel2;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (strong, nonatomic) NSMutableArray<id <AuthorizeRequester>> *delegates;
@property (strong, nonatomic) IBOutlet UIView *requestsStatusView;
@property (strong, nonatomic) IBOutlet UICollectionView *requestsStatusCollectionView;
@property (nonatomic) BOOL allRequestsDone;
@end

@implementation GetStartedViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _allRequestsDone = NO;
        _delegates = [NSMutableArray new];
        [_delegates addObject:[PushNotificationRequester new] ];
        [_delegates addObject:[ContactAccessRequester new] ];
        [_delegates addObject:[MicrophoneAccessRequester new] ];
        [_delegates addObject:[CameraAccessRequester new] ];
        [_delegates addObject:[PhotoLibraryRequester new] ];
        [_delegates addObject:[[AnalyticsRequester alloc] initWithController:self] ];
    }
    return self;
}

-(void) viewDidLoad {
    [super viewDidLoad];
    
    [UITools applyCustomFontTo:_presentationLabel2];
    self.logo.hidden = NO;
    _requestsStatusView.hidden = YES;
    
    self.presentationLabel.text = [NSLocalizedString(@"Get Started", nil) uppercaseString];
    self.presentationLabel.textColor = [UITools defaultTintColor];
    _presentationLabel2.text = NSLocalizedString(@"To make audio and video calls, give access to your microphone, camera and contacts. Also give access to your photos to share them in your conversations.", nil);
    [UITools applyThinCustomFontTo:_bottomLabel];
    _bottomLabel.text = NSLocalizedString(@"These authorizations can be changed at any time in Rainbow settings", nil);
    [self.continueButton setTitle:[NSLocalizedString(@"Give access", nil) uppercaseString] forState:UIControlStateNormal];
    self.continueButton.enabled = YES;
    self.continueButton.alpha = 1.0f;
    
}

- (IBAction)continueButtonTapped:(UIButton *)sender {
    if(_allRequestsDone)
        [self.delegate nextPage];
    else
        [self requestAuthorizationAtIndex:0];
}

-(void) requestAuthorizationAtIndex:(NSInteger) index {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self requestAuthorizationAtIndex:index];
        });
        return;
    }

    [_requestsStatusCollectionView reloadData];
    
    if(index>=_delegates.count) {
        _allRequestsDone = YES;
        self.logo.hidden = YES;
        _requestsStatusView.hidden = NO;
        
        [self.continueButton setTitle:[NSLocalizedString(@"Continue", nil) uppercaseString] forState:UIControlStateNormal];
    }
    
    else {
        id<AuthorizeRequester> delegate = [_delegates objectAtIndex:index];
        
        if([delegate needUserRequest])
            [delegate requestWithCompletionHandler:^(BOOL succeed) {
                NSLog(@"requestWithCompletionHandler %@", [delegate class]);
                [self requestAuthorizationAtIndex:index+1];
            }];
        else
            [self requestAuthorizationAtIndex:index+1];
    }
}

-(BOOL) shouldDisplay {
    if(self.delegate.errorDuringWizardSteps)
        return NO;
    
    BOOL itSould = NO;
    
    for (id<AuthorizeRequester> delegate in _delegates) {
        if([delegate needUserRequest]) {
            itSould = YES;
            break;
        }
    }
    
    return itSould;
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    static NSString *identifier = @"requestStateViewCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    id<AuthorizeRequester> obj = [_delegates objectAtIndex: indexPath.row];
    
    UIButton *button = (UIButton *)[[[cell contentView] subviews] firstObject];
    [button setImage:[UIImage imageNamed:@"AcceptInvitation"] forState:UIControlStateSelected];
    [button setImage:[UIImage imageNamed:@"DeclineInvitation"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"help"] forState:UIControlStateDisabled];
    
    button.selected = NO;
    button.enabled = NO;
    
    if(obj.status == AuthorizeRequesterAccepted) {
        button.selected = YES;
        button.enabled = YES;
    } else if(obj.status == AuthorizeRequesterNotAccepted) {
        button.enabled = YES;
    }
        
    [button setTitle:obj.title forState:UIControlStateNormal];
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _delegates.count;
}

- (void)traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection {
}

- (void)preferredContentSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
}

@end


#pragma mark - Remote push notifications

@implementation PushNotificationRequester

-(instancetype) init {
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRegisterForRemoteWithDeviceToken:) name:UIApplicationDidRegisterForRemoteNotificationWithDeviceToken object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailToRegisterForRemoteNotificationsWithError:) name:UIApplicationDidFailToRegisterForRemoteNotificationsWithError object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRegisterUserNotificationSettings:) name:UIApplicationDidRegisterUserNotificationSettings object:nil];
        _granted = NO;
    }
    return self;
}

-(NSString *)title {
    return NSLocalizedString(@"Notifications", nil);
}
-(AuthorizeRequesterStatus) status {
    if([self needUserRequest])
        return AuthorizeRequesterUnknown;
    return _granted ? AuthorizeRequesterAccepted : AuthorizeRequesterNotAccepted;
    //return [UIApplication sharedApplication].isRegisteredForRemoteNotifications ? AuthorizeRequesterAccepted : AuthorizeRequesterNotAccepted;
}

-(BOOL) needUserRequest {
    return (![UIApplication sharedApplication].isRegisteredForRemoteNotifications && ![[NSUserDefaults standardUserDefaults] boolForKey:kRemotePushNotificationsWizardPageSeen]);
}
-(void) requestWithCompletionHandler:(void (^)(BOOL))completionHandler {
    BOOL alreadyRegistered = [[NSUserDefaults standardUserDefaults] boolForKey:kRemotePushNotificationsWizardPageSeen];
    
    if(![UIApplication sharedApplication].isRegisteredForRemoteNotifications && !alreadyRegistered){
        // Flag as seen this request to not resquest again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kRemotePushNotificationsWizardPageSeen];
        
    #if TARGET_IPHONE_SIMULATOR
        completionHandler(NO);
        NSLog(@"GetStarted - registerForUserNotificationsSettings not needed (simulator)");
    #else
        _completionHandler = completionHandler;
        [[ServicesManager sharedInstance].notificationsManager registerForUserNotificationsSettingsWithCompletionHandler:^(BOOL granted, NSError * _Nullable error) {
            _granted = granted;
            _completionHandler(granted);
        }];
    #endif
    }
}

-(void) didRegisterUserNotificationSettings:(NSNotification *) notification {
    UIUserNotificationSettings *notificationSettings = (UIUserNotificationSettings*)notification.object;
    if(notificationSettings.types == UIUserNotificationTypeNone){
        NSLog(@"GetStarted - didRegisterUserNotificationSettings UIUserNotificationTypeNone");
        if(_completionHandler)
            _completionHandler(NO);
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}
-(void) didRegisterForRemoteWithDeviceToken:(NSNotification *) notification {
    NSString *deviceToken = (NSString *) notification.object;
    NSLog(@"GetStarted - didRegisterForRemoteNotificationsWithDeviceToken %@",deviceToken);
}
-(void) didFailToRegisterForRemoteNotificationsWithError:(NSNotification *) notification {
    NSError *err = (NSError *) notification.object;
    NSLog(@"GetStarted - didFailToRegisterForRemoteNotificationsWithError %@",[err description]);
    if(_completionHandler)
        _completionHandler(NO);
}

- (void)dealloc {
    _completionHandler = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidRegisterForRemoteNotificationWithDeviceToken object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidFailToRegisterForRemoteNotificationsWithError object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidRegisterUserNotificationSettings object:nil];
}
@end

#pragma mark - Contact access

@implementation ContactAccessRequester

-(instancetype) init {
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localAccessGranted:) name:kContactsManagerServiceLocalAccessGrantedNotification object:nil];
    }
    return self;
}

-(NSString *)title {
    return NSLocalizedString(@"Contacts", nil);
}
-(AuthorizeRequesterStatus) status {
    if([self needUserRequest])
        return AuthorizeRequesterUnknown;
    
    return [ServicesManager sharedInstance].contactsManagerService.localDeviceAccessGranted ? AuthorizeRequesterAccepted : AuthorizeRequesterNotAccepted;
}

- (BOOL)needUserRequest {
    return ![ServicesManager sharedInstance].contactsManagerService.localDeviceAccessAlreadyRequired;
}

- (void)requestWithCompletionHandler:(AuthorizeRequesterCompletionHandler)completionHandler {
    NSLog(@"GetStarted - requestAddressBookAccess");
    _completionHandler = completionHandler;
    [[ServicesManager sharedInstance].contactsManagerService requestAddressBookAccess];
}

-(void) localAccessGranted:(NSNotification *)notification {
    if(_completionHandler)
        _completionHandler( [ServicesManager sharedInstance].contactsManagerService.localDeviceAccessGranted );
}

- (void)dealloc {
    _completionHandler = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceLocalAccessGrantedNotification object:nil];
}
@end

#pragma mark - Microphone access

@implementation MicrophoneAccessRequester

-(instancetype) init {
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(microphoneAccessChanged:) name:kRTCServiceDidAllowMicrophoneNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(microphoneAccessChanged:) name:kRTCServiceDidRefuseMicrophoneNotification object:nil];
    }
    return self;
}

-(NSString *)title {
    return NSLocalizedString(@"Microphone", nil);
}
-(AuthorizeRequesterStatus) status {
    if([self needUserRequest])
        return AuthorizeRequesterUnknown;
    
    return [ServicesManager sharedInstance].rtcService.microphoneAccessGranted ? AuthorizeRequesterAccepted : AuthorizeRequesterNotAccepted;
}

- (BOOL)needUserRequest {
    return ![ServicesManager sharedInstance].rtcService.microphoneAccessAlreadyDetermined;
}

- (void)requestWithCompletionHandler:(AuthorizeRequesterCompletionHandler)completionHandler {
    _completionHandler = completionHandler;
    [[ServicesManager sharedInstance].rtcService requestMicrophoneAccess];
}

-(void)microphoneAccessChanged:(NSNotification *) notification {
    if(_completionHandler)
        _completionHandler([ServicesManager sharedInstance].rtcService.microphoneAccessGranted);
}

-(void) dealloc {
    _completionHandler = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidRefuseMicrophoneNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidAllowMicrophoneNotification object:nil];
}

@end

#pragma mark - Camera access

@implementation CameraAccessRequester {
    BOOL granted;
}

-(instancetype) init {
    self = [super init];
    if(self){
        granted = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusAuthorized;
    }
    return self;
}

-(NSString *)title {
    return NSLocalizedString(@"Camera", nil);
}
-(AuthorizeRequesterStatus) status {
    if([self needUserRequest])
        return AuthorizeRequesterUnknown;
    return granted ? AuthorizeRequesterAccepted : AuthorizeRequesterNotAccepted;
}

- (BOOL)needUserRequest {
    return [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined;
}

- (void)requestWithCompletionHandler:(AuthorizeRequesterCompletionHandler)completionHandler {
    _completionHandler = completionHandler;
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL isGranted) {
        granted = isGranted;
        if(_completionHandler)
            _completionHandler(isGranted);
    }];
}

-(void) dealloc {
    _completionHandler = nil;
}

@end

#pragma mark - Photo Library access

@implementation PhotoLibraryRequester {
    BOOL granted;
}


-(instancetype) init {
    self = [super init];
    if(self){
        granted = [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized;
    }
    return self;
}

-(NSString *)title {
    return NSLocalizedString(@"Access photos", nil);
}
-(AuthorizeRequesterStatus) status {
    if([self needUserRequest])
        return AuthorizeRequesterUnknown;
    return [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized ? AuthorizeRequesterAccepted : AuthorizeRequesterNotAccepted;
}

- (BOOL)needUserRequest {
    return [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusNotDetermined;
}

- (void)requestWithCompletionHandler:(AuthorizeRequesterCompletionHandler)completionHandler {
    _completionHandler = completionHandler;
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if(_completionHandler)
            _completionHandler(status == PHAuthorizationStatusAuthorized);
    }];
}

-(void) dealloc {
    _completionHandler = nil;
}

@end

#pragma mark - Analytics approval

@implementation AnalyticsRequester {
    WelcomePageCommonViewController *ctrl;
}

-(instancetype) initWithController:(WelcomePageCommonViewController*) controller {
    if (self = [super init]) {
        ctrl = controller;
    }
    return self;
}

-(NSString *)title {
    return NSLocalizedString(@"Usage data", nil);
}
-(AuthorizeRequesterStatus) status {
    if([self needUserRequest])
        return AuthorizeRequesterUnknown;
    return [GoogleAnalytics sharedInstance].authorizationGranted ? AuthorizeRequesterAccepted : AuthorizeRequesterNotAccepted;
}

- (BOOL)needUserRequest {
    return ![GoogleAnalytics sharedInstance].authorizationAlreadyRequested;
}

- (void)requestWithCompletionHandler:(AuthorizeRequesterCompletionHandler)completionHandler {
    _completionHandler = completionHandler;

    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *message = NSLocalizedString(@"Help us improve the product by providing anonymous product usage data.", nil);
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Usage data", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Don't allow", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [GoogleAnalytics sharedInstance].authorizationAlreadyRequested = YES;
            [GoogleAnalytics sharedInstance].authorizationGranted = NO;
            completionHandler(NO);
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Allow", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [GoogleAnalytics sharedInstance].authorizationAlreadyRequested = YES;
            [GoogleAnalytics sharedInstance].authorizationGranted = YES;
            completionHandler(YES);
        }]];
        
        [ctrl presentViewController:alert animated:YES completion:nil];
    });
}

-(void) dealloc {
    _completionHandler = nil;
}

@end
