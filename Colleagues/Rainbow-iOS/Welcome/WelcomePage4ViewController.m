/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "WelcomePageCommonViewController.h"
#import "UIAvatarView.h"
#import "UITools.h"
#import "UIDevice-Hardware.h"
#import <Rainbow/ServicesManager.h>
#import "UIImageView+Letters.h"
#import <Photos/Photos.h>
#import "SCLAlertView.h"
#import "SCLAlertView+Rotation.h"

@interface WelcomePage4ViewController : WelcomePageCommonViewController
@end

@interface WelcomePage4ViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *takeAPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *chooseAPictureButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (nonatomic, strong) UIImage *imageSelected;
@property (nonatomic, strong) NSMutableDictionary *userInfos;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarViewWidthConstraint;
@end

@implementation WelcomePage4ViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    CGRect f = _avatarView.frame;
    _avatarView.layer.cornerRadius = f.size.width/2.0;
    _avatarView.image = [UIImage imageNamed:@"ContactTabBar"];
    _avatarView.layer.masksToBounds = YES;
    
    _takeAPhotoButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    _chooseAPictureButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [UITools applyCustomFontTo:_takeAPhotoButton.titleLabel];
    [UITools applyCustomFontTo:_chooseAPictureButton.titleLabel];
    [_takeAPhotoButton setBackgroundColor:[UITools colorFromHexa:0x34B233FF]];
    [_chooseAPictureButton setBackgroundColor:[UITools colorFromHexa:0x34B233FF]];
    [_takeAPhotoButton setTitle:NSLocalizedString(@"Take Photo", nil) forState:UIControlStateNormal];
    [_chooseAPictureButton setTitle:NSLocalizedString(@"Choose From Library", nil) forState:UIControlStateNormal];
    
    self.continueButton.enabled = YES;
    self.continueButton.alpha = 1.0f;
    _userInfos = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"userInfos"]];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(_imageSelected){
        _avatarView.image = _imageSelected;
        [self.continueButton setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateNormal];
    } else {
        NSString *firstName = nil;
        if(_userInfos[@"firstname"])
            firstName = _userInfos[@"firstname"];
        
        NSString *lastName = nil;
        if(_userInfos[@"lastname"])
            lastName = _userInfos[@"lastname"];

        NSString *fullName = [self getFullNameForFirstName:firstName andLastName:lastName];
        UIColor *backgroundColor = [UITools colorForString:fullName];
        [_avatarView setImageWithString:fullName color:backgroundColor circular:YES fontName:[UITools defaultFontName]];
    }
    
    _takeAPhotoButton.layer.cornerRadius = 8.0f;
    _chooseAPictureButton.layer.cornerRadius = 8.0f;
    
    if(IS_IPHONE_4_OR_LESS){
        CGFloat size = _avatarView.frame.size.width * 0.7;
        _avatarViewHeightConstraint.constant = size;
        _avatarViewWidthConstraint.constant = size;
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (IBAction)takeAPhotoTapped:(UIButton *)sender {
    if([[UIDevice currentDevice].modelName containsString:@"Simulator"]){
        [self showErrorAlertWithMessage:NSLocalizedString(@"No camera found", nil)];
    } else {
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
    }
}

- (IBAction)chooseAPictureTapped:(UIButton *)sender {
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)source {
    if(source == UIImagePickerControllerSourceTypeCamera){
        switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo]) {
            case AVAuthorizationStatusNotDetermined:{
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if(granted){
                        [self showImagePickerForSource:source];
                    } else {
                        [self showAccessNotGrantedPopup];
                    }
                }];
                break;
            }
            case AVAuthorizationStatusAuthorized:{
                [self showImagePickerForSource:source];
                break;
            }
            default:{
                [self showAccessNotGrantedPopup];
                break;
            }
        }
    } else {
        switch ([PHPhotoLibrary authorizationStatus]) {
            case PHAuthorizationStatusNotDetermined:{
                [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                    if(status == PHAuthorizationStatusAuthorized){
                        [self showImagePickerForSource:source];
                    } else {
                        [self showAccessNotGrantedPopup];
                    }
                }];
                break;
            }
            case PHAuthorizationStatusAuthorized:{
                [self showImagePickerForSource:source];
                break;
            }
            default:
                [self showAccessNotGrantedPopup];
                break;
        }
    }
}

-(void) showImagePickerForSource:(UIImagePickerControllerSourceType)source {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showImagePickerForSource:source];
        });
        return;
    }
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    if (source == UIImagePickerControllerSourceTypePhotoLibrary && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    else if(source == UIImagePickerControllerSourceTypePhotoLibrary)
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    else if (source == UIImagePickerControllerSourceTypeCamera)
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    if(source == UIImagePickerControllerSourceTypeCamera){
        // Don't use our custom overlay, it create pictures not squarred..
        // We disable editing because overlay block it
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
            imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    }
    [self presentViewController: imagePickerController animated: YES completion: nil];
}

-(void) showAccessNotGrantedPopup {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showAccessNotGrantedPopup];
        });
        return;
    }
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    [alert addButton:NSLocalizedString(@"Go to Settings", nil) actionBlock:^(void) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    alert.customViewColor = [UITools defaultTintColor];
    alert.iconTintColor = [UITools defaultBackgroundColor];
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = YES;
    
    [alert showInfo:self
              title:NSLocalizedString(@"Access photos", nil)
           subTitle:NSLocalizedString(@"Please allow access to photos and camera from your device settings", nil)
   closeButtonTitle:NSLocalizedString(@"Close", nil)
           duration:0.0f];
}

#pragma mark UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *finalImage = nil;
    if(!picker.allowsEditing){
        finalImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    } else {
        // We use the edited image
        finalImage = [info objectForKey:UIImagePickerControllerEditedImage];
    }
    _imageSelected = [UITools imageWithImage:finalImage scaledToSize:CGSizeMake(100, 100)];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(UIView *) overlayView {
    UIView *overlayView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 40.0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-50)];
    overlayView.userInteractionEnabled = NO;
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height-50;
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    int position = IS_WIDESCREEN ? 124-40 : 80;
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    UIBezierPath *path2 = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(0.0f, position, screenWidth, screenWidth)];
    [path2 setUsesEvenOddFillRule:YES];
    [circleLayer setPath:[path2 CGPath]];
    [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, screenWidth, screenHeight-72) cornerRadius:0];
    [path appendPath:path2];
    [path setUsesEvenOddFillRule:YES];
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = path.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [UIColor blackColor].CGColor;
    fillLayer.opacity = 0.4;
    [overlayView.layer addSublayer:fillLayer];
    
    return overlayView;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // This is only for the choose picture view
    if ([navigationController.viewControllers count] == 3) {
        CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
        CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
        int position = IS_WIDESCREEN ? 124 : 80;
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        UIBezierPath *path2 = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0.0f, position, screenWidth, screenWidth)];
        [path2 setUsesEvenOddFillRule:YES];
        [circleLayer setPath:[path2 CGPath]];
        [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, screenWidth, screenHeight-72) cornerRadius:0];
        [path appendPath:path2];
        [path setUsesEvenOddFillRule:YES];
        CAShapeLayer *fillLayer = [CAShapeLayer layer];
        fillLayer.path = path.CGPath;
        fillLayer.fillRule = kCAFillRuleEvenOdd;
        fillLayer.fillColor = [UIColor blackColor].CGColor;
        fillLayer.opacity = 0.4;
        [viewController.view.layer addSublayer:fillLayer];
    }
}

- (IBAction)continueButtonTapped:(UIButton *)sender {
    [self showHUD];

    if(_imageSelected)
        [_userInfos setObject:_imageSelected forKey:@"photo"];
    
    // Add current user time zone
    NSTimeZone *timezone = [NSTimeZone localTimeZone];
    [_userInfos setObject:timezone.name forKey:@"timezone"];
    
    // Add current user language
    NSString *languageCode = [[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode];
    [_userInfos setObject:languageCode forKey:@"language"];
    
    [_userInfos setObject:@YES forKey:@"isInitialized"];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[ServicesManager sharedInstance].contactsManagerService updateUserWithFields:_userInfos withCompletionBlock:^(NSError *error) {
            if(error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showErrorAlertWithMessageAndContinue:NSLocalizedString(@"Error while updating user info", nil)];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self hideHUD];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userInfos"];
                    [self.delegate nextPage];
                });
            }
        }];
    });
}

-(void) showErrorAlertWithMessageAndContinue:(NSString *) message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userInfos"];
        [self.delegate nextPage];
    }]];
     [self presentViewController:alert animated:YES completion:nil];
}


- (void)didTapInBackground:(UITapGestureRecognizer *)sender {
    // Nothing to do here
}

-(NSString *) getFullNameForFirstName:(NSString *) firstName andLastName:(NSString *) lastName {
    NSMutableString *format = [[NSMutableString alloc] init];
    NSInteger nbParams = 0;
    if(firstName){
        [format appendString:@"%@"];
        nbParams++;
    }
    if(firstName && lastName)
        [format appendString:@" "];
    if(lastName){
        [format appendString:@"%@"];
        nbParams++;
    }
    
    if(nbParams == 1)
        if(firstName)
            return [NSString stringWithFormat:format, [firstName capitalizedString]];
        else
            return [NSString stringWithFormat:format, [lastName uppercaseString]];
        else
            return [NSString stringWithFormat:format, [firstName capitalizedString], [lastName uppercaseString]];
}

// Should display the avatar view ?
-(BOOL) shouldDisplay {
    if(self.delegate.errorDuringWizardSteps)
        return NO;
    if ([ServicesManager sharedInstance].myUser.isGuest){
        return NO;
    }
    
    return ![ServicesManager sharedInstance].myUser.isInitialized;
}

@end
