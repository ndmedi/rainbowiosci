/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "WelcomePageCommonViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import "MBProgressHUD.h"
#import "UILabel+Clickable.h"

#define kTokenLength 6

@interface WelcomePage10ViewController : WelcomePageCommonViewController
@end

@interface WelcomePage10ViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *presentationLabel2;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIImageView *passwordImageView;
@property (weak, nonatomic) IBOutlet UIImageView *codeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *emailAddressImageView;
@property (weak, nonatomic) IBOutlet UILabel *tosLabel;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@end

@implementation WelcomePage10ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UITools applyCustomFontTo:_presentationLabel2];
    _tosLabel.hidden = YES;
    if(self.invitationInformations){
        
        NSString *emailAddress = self.invitationInformations[@"loginEmail"];
        
        self.logo.image = [UIImage imageNamed:@"Invitation"];
        
        self.logoWidthConstraint.constant *= 1.5;
        self.logoHeightConstraint.constant *= 1.5;
        self.presentationLabelTopConstraint.constant = 0;
        
        self.presentationLabel.text = NSLocalizedString(@"We have detected that you are trying to connect to Rainbow using a new medium.", nil);
        self.presentationLabel.numberOfLines = 5;
        
        _presentationLabel2.text = [NSString stringWithFormat:NSLocalizedString(@"For security reasons, an activation code has been sent to your email address %@.", nil), emailAddress];
        _codeTextField.placeholder = NSLocalizedString(@"Activation code with 6 digits", nil);
        
        NSString *tosText = NSLocalizedString(@"By continuing, you agree:\n• The Terms of Service\n• Privacy Policy", nil);
        NSMutableAttributedString *tosTextAttributed = [[NSMutableAttributedString alloc] initWithString:tosText];
        
        NSString *tosLinkText = NSLocalizedString(@"The Terms of Service", nil);
        NSString *privacyLinkText = NSLocalizedString(@"Privacy Policy", nil);
        
        NSRange tosLinkRange = [tosText rangeOfString:tosLinkText options:NSNumericSearch];
        NSRange privacyLinkRange = [tosText rangeOfString:privacyLinkText options:NSNumericSearch];
        
        NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionaryWithDictionary:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:12] ,  NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle), NSLinkAttributeName: [NSURL URLWithString:@"https://www.openrainbow.com/terms-service/"], NSForegroundColorAttributeName: [UITools defaultTintColor]}];
        
        [tosTextAttributed setAttributes:linkAttributes range:tosLinkRange];
        
        [linkAttributes setObject:[NSURL URLWithString:@"http://enterprise.alcatel-lucent.com/?content=AboutUs&page=Privacy"] forKey:NSLinkAttributeName];
        
        [tosTextAttributed setAttributes:linkAttributes range:privacyLinkRange];
        
        _tosLabel.attributedText = tosTextAttributed;
        _tosLabel.userInteractionEnabled = YES;
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnTosLabel:)];
        [_tosLabel addGestureRecognizer:_tapGesture];
        _tosLabel.hidden = NO;
        
        linkAttributes = nil;
        tosTextAttributed = nil;
        
    }
    
    [_codeTextField setTintColor:[UITools defaultTintColor]];
    [UITools applyCustomFontToTextField:_codeTextField];
}

- (void)handleTapOnTosLabel:(UITapGestureRecognizer *)tapGesture {
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    [_tosLabel openTappedLinkAtLocation:locationOfTouchInLabel];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _codeTextField.leftView = _codeImageView;
    _codeTextField.leftViewMode = UITextFieldViewModeAlways;
    [_codeTextField layoutIfNeeded];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if([self shouldDisplay]){
        NSString *emailAddress = self.invitationInformations[@"loginEmail"];
        [[ServicesManager sharedInstance].loginManager sendResetPasswordEmailWithLoginEmail:emailAddress completionHandler:nil];
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_tosLabel removeGestureRecognizer:_tapGesture];
    _tapGesture = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)textFieldValueChanged:(UITextField *)sender {
    [self checkContinueButtonState];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if([textField isEqual:_codeTextField]){
        if (!string.length)
            return YES;
        
        if (textField.keyboardType == UIKeyboardTypeNumberPad) {
            if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound){
                return NO;
            }
        }
        
        NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (updatedText.length > kTokenLength) {
            return NO;
        }
        
        [self checkContinueButtonState];
        
        return YES;
    } else {
        return YES;
    }
}

-(void) checkContinueButtonState {
    BOOL isEnable = NO;
    if((_codeTextField.text.length >= 4 && _codeTextField.text.length <= kTokenLength)){
        isEnable = YES;
    }
    
    if(isEnable){
        self.continueButton.enabled = YES;
        self.continueButton.alpha = 1.0f;
    } else {
        self.continueButton.enabled = NO;
        self.continueButton.alpha = 0.5f;
    }
}

- (IBAction)continueButtonTapped:(UIButton *)sender {
    [self showHUD];
    [_codeTextField resignFirstResponder];
    
    NSString *emailEntered = self.invitationInformations[@"loginEmail"];
    NSString *temporaryCode = _codeTextField.text;
    NSString *password = [UITools generateUniquePassword];
    
    [[ServicesManager sharedInstance].loginManager sendResetPasswordWithLoginEmail:emailEntered password:password temporaryCode:temporaryCode completionHandler:^(NSDictionary *jsonResponse, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(error){
                [self hideHUD];
                [self checkErrorInJsonResponse:jsonResponse];
            } else {
                [self.delegate gotAnErrorDuringWizardSteps:nil];
                self.hud.mode = MBProgressHUDModeCustomView;
                self.hud.labelText = NSLocalizedString(@"Password changed", nil);
                self.hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                [self.hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:nil];
                
                // Everything ok, saving given username and password
                [[ServicesManager sharedInstance].loginManager setUsername:emailEntered andPassword:password];
                // Now login the user
                [[ServicesManager sharedInstance].loginManager connect];
                // We got to the next page because the user have perhaps already done the wizard so he can stay locked here.
                [self.delegate cancelWizard];
            }
        });
    }];
}

-(void) checkErrorInJsonResponse:(NSDictionary *) jsonResponse {
    if(jsonResponse){
        NSNumber *errorCode = (NSNumber*)jsonResponse[@"errorCode"];
        if(errorCode.integerValue == 400){
            // Check if the error is regarding token length
            BOOL tokenLengthError = NO;
            NSArray *errorDetails = jsonResponse[@"errorDetails"];
            for (NSDictionary *errorDetail in errorDetails) {
                if([errorDetail[@"param"] isEqualToString:@"temporaryToken"]){
                    tokenLengthError = YES;
                    break;
                }
            }
            if(tokenLengthError){
                [self showErrorAlertWithTitle:NSLocalizedString(@"Token length invalid", nil) andMessage:NSLocalizedString(@"The given token is not valid, please check if you have the latest application version", nil) exitOnError:NO];
            } else {
                [self showErrorAlertWithTitle:NSLocalizedString(@"Invalid password",nil) andMessage:NSLocalizedString(@"Password must be more than 8 characters and contain \n•1 lowercase letter \n•1 uppercase letter \n•1 number \n•1 special charater",nil) exitOnError:NO];
            }
        } else if(errorCode.integerValue == 401) {
            NSNumber *detailedCode = (NSNumber *)jsonResponse[@"errorDetailsCode"];
            if(detailedCode.integerValue == 401202){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Unauthorized access", nil) exitOnError:YES];
            } else if(detailedCode.integerValue == 401510){
                [self showErrorAlertWithMessage:NSLocalizedString(@"User not found", nil) exitOnError:YES];
            } else if(detailedCode.integerValue == 401520){
                [self showErrorAlertWithMessage:NSLocalizedString(@"User not activated", nil) exitOnError:YES];
            } else if(detailedCode.integerValue == 401521){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Company not activated", nil) exitOnError:YES];
            } else {
                [self showErrorAlertWithMessage:NSLocalizedString(@"An unknown error occured", nil) exitOnError:YES];
            }
        } else if(errorCode.integerValue == 403) {
            NSNumber *detailedCode = (NSNumber *)jsonResponse[@"errorDetailsCode"];
            if(detailedCode.integerValue == 403000){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Access denied, user don't have required role", nil) exitOnError:YES];
            } else if (detailedCode.integerValue == 403100){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Access denied, wrong user", nil) exitOnError:YES];
            } else if (detailedCode.integerValue == 403500){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Email is black-listed, could not register user", nil) exitOnError:YES];
            } else {
                [self showErrorAlertWithMessage:NSLocalizedString(@"An unknown error occured", nil) exitOnError:YES];
            }
        } else if(errorCode.integerValue == 404) {
            [self showErrorAlertWithMessage:NSLocalizedString(@"User does not exist.", nil) exitOnError:YES];
        } else if(errorCode.integerValue == 409) {
            [self showErrorAlertWithMessage:NSLocalizedString(@"A user with this email already exist in Rainbow system", nil) exitOnError:YES];
        } else {
            [self showErrorAlertWithMessage:NSLocalizedString(@"An unknown error occured", nil) exitOnError:YES];
        }
    }
}

-(void) showErrorAlertWithTitle:(NSString *) title andMessage:(NSString *)message exitOnError:(BOOL) exit {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        if(exit){
            [[ServicesManager sharedInstance].loginManager setUsername:nil andPassword:nil];
            [self.delegate cancelWizard];
        }
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) showErrorAlertWithMessage:(NSString *) message exitOnError:(BOOL) exit{
    [self showErrorAlertWithTitle:NSLocalizedString(@"Error", nil) andMessage:message exitOnError:exit];
}

- (void)didTapInBackground:(UITapGestureRecognizer *)sender {
    if(_codeTextField.isFirstResponder)
        [_codeTextField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(self.continueButton.enabled)
        [self continueButtonTapped:self.continueButton];
    return YES;
}

-(BOOL) shouldDisplay {
    return self.isGuestMode && self.invitationInformations && self.delegate.errorDuringWizardSteps ? YES : NO;
}


@end

