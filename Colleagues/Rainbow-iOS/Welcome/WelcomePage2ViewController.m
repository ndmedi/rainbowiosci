/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "WelcomePageCommonViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import "MBProgressHUD.h"
#import "UILabel+Clickable.h"
#import "ACFloatingTextField.h"

#define kTokenLength 6

@interface WelcomePage2ViewController : WelcomePageCommonViewController
@end

@interface WelcomePage2ViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *presentationLabel2;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *codeTextField;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *tosLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeightContraint;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@end

@implementation WelcomePage2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UITools applyCustomFontTo:_presentationLabel2];
    _tosLabel.hidden = YES;
    if(self.invitationInformations){
        self.logo.image = [UIImage imageNamed:@"Invitation"];
        
        self.logoWidthConstraint.constant *= 1.5;
        self.logoHeightConstraint.constant *= 1.5;
        self.presentationLabelTopConstraint.constant = 0;
        NSString *companyName = nil;
        BOOL companyInvitation = NO;
        if([self.invitationInformations objectForKey:@"companyName"]){
            self.logo.image = [UIImage imageNamed:@"InvitationCompany"];
            companyName = self.invitationInformations[@"companyName"];
            companyInvitation = YES;
        } else
            companyName = @"Rainbow";
        
        NSString *welcomeMessage = @"";
        
        if(self.invitationInformations.count == 1){
            // SMS Invitation
            welcomeMessage = [NSString stringWithFormat:NSLocalizedString(@"Join\n%@", nil), companyName];
        } else {
            welcomeMessage = [NSString stringWithFormat:NSLocalizedString(@"Join\n%@\n\nYour login is\n%@", nil), companyName, self.invitationInformations[@"loginEmail"]];
        }
        
        NSRange welcomeMessageJoinRange = [welcomeMessage rangeOfString:NSLocalizedString(@"Join", nil) options:NSNumericSearch];
        NSRange welcomeMessageCompanyNameRange = [welcomeMessage rangeOfString:companyInvitation?companyName:@"Rainbow" options:NSNumericSearch];
        
        NSMutableAttributedString *welcomeMessageAttributedText = [[NSMutableAttributedString alloc] initWithString:welcomeMessage];
        
        NSMutableDictionary *welcomeMessageAttributes = [NSMutableDictionary dictionaryWithDictionary:@{NSFontAttributeName:[UIFont fontWithName:[UITools boldFontName] size:30], NSForegroundColorAttributeName: [UIColor blackColor]}];
        
        [welcomeMessageAttributedText setAttributes:welcomeMessageAttributes range:welcomeMessageJoinRange];
        
        [welcomeMessageAttributes setObject:[UITools defaultTintColor] forKey:NSForegroundColorAttributeName];
        
        [welcomeMessageAttributedText setAttributes:welcomeMessageAttributes range:welcomeMessageCompanyNameRange];
        
        self.presentationLabel.attributedText = welcomeMessageAttributedText;
        self.presentationLabel.numberOfLines = 5;
        _passwordTextField.placeholder = NSLocalizedString(@"Password", nil);
        if(self.invitationInformations.count == 1){
            _presentationLabel2.text = NSLocalizedString(@"Enter an email address, choose a password and start your Rainbow experience", nil);
            _codeTextField.hidden = NO;
            _codeTextField.placeholder = NSLocalizedString(@"Email address", nil);
            _codeTextField.keyboardType = UIKeyboardTypeEmailAddress;
            _passwordTextField.placeholder = NSLocalizedString(@"Password", nil);
        } else {
            _presentationLabel2.text = NSLocalizedString(@"Choose a password and start your Rainbow experience", nil);
            _codeTextField.hidden = YES;
            _passwordTextField.placeholder = NSLocalizedString(@"Password", nil);
        }
        
        welcomeMessageAttributedText = nil;
        welcomeMessageAttributes = nil;
        
        NSString *tosText = NSLocalizedString(@"By continuing, you agree:\n• The Terms of Service\n• Privacy Policy", nil);
        NSMutableAttributedString *tosTextAttributed = [[NSMutableAttributedString alloc] initWithString:tosText];
        
        NSString *tosLinkText = NSLocalizedString(@"The Terms of Service", nil);
        NSString *privacyLinkText = NSLocalizedString(@"Privacy Policy", nil);
        
        NSRange tosLinkRange = [tosText rangeOfString:tosLinkText options:NSNumericSearch];
        NSRange privacyLinkRange = [tosText rangeOfString:privacyLinkText options:NSNumericSearch];
        
        NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionaryWithDictionary:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:12] ,  NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle), NSLinkAttributeName: [NSURL URLWithString:@"https://www.openrainbow.com/terms-service/"], NSForegroundColorAttributeName: [UITools defaultTintColor]}];
        
        [tosTextAttributed setAttributes:linkAttributes range:tosLinkRange];
        
        [linkAttributes setObject:[NSURL URLWithString:@"http://enterprise.alcatel-lucent.com/?content=AboutUs&page=Privacy"] forKey:NSLinkAttributeName];
        
        [tosTextAttributed setAttributes:linkAttributes range:privacyLinkRange];
        
        _tosLabel.attributedText = tosTextAttributed;
        _tosLabel.userInteractionEnabled = YES;
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnTosLabel:)];
        [_tosLabel addGestureRecognizer:_tapGesture];
        _tosLabel.hidden = NO;
        
        linkAttributes = nil;
        tosTextAttributed = nil;
        
    } else {
        // Guest user start finalizing the account
        if ([ServicesManager sharedInstance].myUser.isGuest) {
            _codeTextField.hidden = YES;
            self.presentationLabel.text = NSLocalizedString(@"Choose a password and start your Rainbow experience", nil);
            _presentationLabel2.text = @"";
            _passwordTextField.placeholder = NSLocalizedString(@"Password", nil);
        }else {
            _codeTextField.hidden = NO;
            self.presentationLabel.text = NSLocalizedString(@"Please check your mailbox!", nil);
            _presentationLabel2.text = NSLocalizedString(@"We have sent you an e-mail with a 6-digit verification code", nil);
            _codeTextField.placeholder = NSLocalizedString(@"Verification code", nil);
            _passwordTextField.placeholder = NSLocalizedString(@"New password", nil);
        }
    }
    
    // Adjust container view depending on the tos label visibility
    _containerViewHeightContraint.constant = (_tosLabel.isHidden) ? 210 : 260; // todo: calculate diff automatically
}

- (void)handleTapOnTosLabel:(UITapGestureRecognizer *)tapGesture {
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    [_tosLabel openTappedLinkAtLocation:locationOfTouchInLabel];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _codeTextField.borderStyle = UITextBorderStyleNone;
    _codeTextField.lineColor = [UITools foregroundGrayColor];
    _codeTextField.selectedLineColor = [UITools defaultTintColor];
    _codeTextField.placeHolderColor = [UITools foregroundGrayColor];
    _codeTextField.selectedPlaceHolderColor = [UITools defaultTintColor];
    [_codeTextField setTintColor:[UITools defaultTintColor]];
    _passwordTextField.borderStyle = UITextBorderStyleNone;
    _passwordTextField.lineColor = [UITools foregroundGrayColor];
    _passwordTextField.selectedLineColor = [UITools defaultTintColor];
    _passwordTextField.placeHolderColor = [UITools foregroundGrayColor];
    _passwordTextField.selectedPlaceHolderColor = [UITools defaultTintColor];
    [_passwordTextField setTintColor:[UITools defaultTintColor]];
    [UITools applyCustomFontToTextField:_codeTextField];
    [UITools applyCustomFontToTextField:_passwordTextField];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_tosLabel removeGestureRecognizer:_tapGesture];
    _tapGesture = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)textFieldValueChanged:(UITextField *)sender {
    [self checkContinueButtonState];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(self.invitationInformations.count == 1){
        [self checkContinueButtonState];
        return YES;
    }
    
    if([textField isEqual:_codeTextField]){
        if (!string.length)
            return YES;
        
        if (textField.keyboardType == UIKeyboardTypeNumberPad) {
            if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound){
                return NO;
            }
        }
        
        NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (updatedText.length > kTokenLength) {
            return NO;
        }

        [self checkContinueButtonState];
        
        return YES;
    } else {
        return YES;
    }
}

-(void) checkContinueButtonState {
    BOOL isEnable = NO;
    if(self.invitationInformations.count == 1){
        if(_codeTextField.text.length > 0 && _passwordTextField.text.length > 0)
            isEnable = YES;
    } else {
        if(self.invitationInformations || [ServicesManager sharedInstance].myUser.isGuest){
            if(_passwordTextField.text.length > 0){
                isEnable = YES;
            }
        } else {
            if((_codeTextField.text.length >= 4 && _codeTextField.text.length <= kTokenLength) && _passwordTextField.text.length > 0){
                isEnable = YES;
            }
        }
    }
    
    if(isEnable){
        self.continueButton.enabled = YES;
        self.continueButton.alpha = 1.0f;
    } else {
        self.continueButton.enabled = NO;
        self.continueButton.alpha = 0.5f;
    }
}

- (IBAction)continueButtonTapped:(UIButton *)sender {
    [self showHUD];
    [_codeTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    
    NSString *emailEntered = [ServicesManager sharedInstance].myUser.username;
    NSString *temporaryCode = _codeTextField.text;
    NSString *password = _passwordTextField.text;
    
    if(self.lostPasswordMode){
        [[ServicesManager sharedInstance].loginManager sendResetPasswordWithLoginEmail:emailEntered password:password temporaryCode:temporaryCode completionHandler:^(NSDictionary *jsonResponse, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error){
                    [self hideHUD];
                    [self checkErrorInJsonResponse:jsonResponse];
                } else {
                    self.hud.mode = MBProgressHUDModeCustomView;
                    self.hud.labelText = NSLocalizedString(@"Password changed", nil);
                    self.hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [self.hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:nil];
                    
                    // Everything ok, saving given username and password
                    [[ServicesManager sharedInstance].loginManager setUsername:emailEntered andPassword:password];
                    // Now login the user
                    [[ServicesManager sharedInstance].loginManager connect];
                    
                    // We don't go to the next page, this will be done by didLogin notification into CustomTabBarController
                }
            });
        }];
    } else if(self.invitationInformations){
        if([self.invitationInformations objectForKey:@"invitationID"]){
            NSString *loginEmail = self.invitationInformations[@"loginEmail"];
            if(self.invitationInformations.count == 1)
                loginEmail = _codeTextField.text;
            [[ServicesManager sharedInstance].loginManager sendSelfRegisterRequestWithLoginEmail:loginEmail password:password invitationId:self.invitationInformations[@"invitationID"] completionHandler:^(NSDictionary *jsonResponse, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(error){
                        [self hideHUD];
                        [self checkErrorInJsonResponse:jsonResponse];
                    } else {
                        self.hud.mode = MBProgressHUDModeCustomView;
                        self.hud.labelText = NSLocalizedString(@"Account created", nil);
                        self.hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                        [self.hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:nil];
                        
                        // Everything ok, saving given username and password
                        [[ServicesManager sharedInstance].loginManager setUsername:loginEmail andPassword:password];
                        // Now login the user
                        [[ServicesManager sharedInstance].loginManager connect];
                        
                        // We don't go to the next page, this will be done by didLogin notification into CustomTabBarController
                    }
                });
            }];
        } else if ([self.invitationInformations objectForKey:@"joinCompanyID"]){
            __weak __typeof__ (self) weakSelf = self;
            [[ServicesManager sharedInstance].loginManager sendSelfRegisterRequestWithLoginEmail:self.invitationInformations[@"loginEmail"] password:password joinCompanyInvitationId:self.invitationInformations[@"joinCompanyID"]  completionHandler:^(NSDictionary *jsonResponse, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(error){
                        [weakSelf hideHUD];
                        [weakSelf checkErrorInJsonResponse:jsonResponse];
                    } else {
                        weakSelf.hud.mode = MBProgressHUDModeCustomView;
                        weakSelf.hud.labelText = NSLocalizedString(@"Account created", nil);
                        weakSelf.hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                        [weakSelf.hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:nil];
                        
                        // Everything ok, saving given username and password
                        [[ServicesManager sharedInstance].loginManager setUsername:self.invitationInformations[@"loginEmail"] andPassword:password];
                        // Now login the user
                        [[ServicesManager sharedInstance].loginManager connect];
                        
                        // We don't go to the next page, this will be done by didLogin notification into CustomTabBarController
                    }
                });
            }];
        } else {
            NSLog(@"Did not found info in invitation");
        }
    } else if ([ServicesManager sharedInstance].myUser.isGuest){
        
        NSString *savedPassword = [ServicesManager sharedInstance].myUser.password;
        
        NSMutableDictionary *givenInfo = [NSMutableDictionary dictionary];
        [givenInfo setObject:@YES forKey:@"forceGuestInitialize"];

        [[ServicesManager sharedInstance].contactsManagerService updateUserWithFields:givenInfo withCompletionBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error){
                    [self hideHUD];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userInfos"];
                } else {
                    // Send request to server
                    [[ServicesManager sharedInstance].loginManager sendChangePassword:savedPassword newPassword:password completionHandler:^(NSDictionary *jsonResponse, NSError *error) {
                        if(error) {
                            // Error to treat
                            NSLog(@"Change password error");
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self hideHUD];
                                [self checkErrorInJsonResponse:jsonResponse];
                            });
                        } else {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                NSLog(@"Change password success");
                                self.hud.mode = MBProgressHUDModeCustomView;
                                self.hud.labelText = NSLocalizedString(@"Saved", nil);
                                self.hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                                [self.hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:nil];
                            });
                            [[ServicesManager sharedInstance].loginManager setUsername: [ServicesManager sharedInstance].myUser.username andPassword:password];
                            [[ServicesManager sharedInstance].loginManager connect];
                            // We don't go to the next page, this will be done by didLogin notification into CustomTabBarController
                        }
                        
                    }];
                }
            });
        }];
    }else {
        [[ServicesManager sharedInstance].loginManager sendSelfRegisterRequestWithLoginEmail:emailEntered password:password temporaryCode:temporaryCode completionHandler:^(NSDictionary *jsonResponse, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error){
                    [self hideHUD];
                    [self checkErrorInJsonResponse:jsonResponse];
                } else {
                    self.hud.mode = MBProgressHUDModeCustomView;
                    self.hud.labelText = NSLocalizedString(@"Account created", nil);
                    self.hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [self.hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:nil];
                    
                    // Everything ok, saving given username and password
                    [[ServicesManager sharedInstance].loginManager setUsername:emailEntered andPassword:password];
                    // Now login the user
                    [[ServicesManager sharedInstance].loginManager connect];
                    
                    // We don't go to the next page, this will be done by didLogin notification into CustomTabBarController
                }
            });
        }];
    }
}

-(void) checkErrorInJsonResponse:(NSDictionary *) jsonResponse {
    if(jsonResponse){
        NSNumber *errorCode = (NSNumber*)jsonResponse[@"errorCode"];
        if(errorCode.integerValue == 400){
            // Check if the error is regarding token length
            BOOL tokenLengthError = NO;
            NSArray *errorDetails = jsonResponse[@"errorDetails"];
            for (NSDictionary *errorDetail in errorDetails) {
                if([errorDetail[@"param"] isEqualToString:@"temporaryToken"]){
                    tokenLengthError = YES;
                    break;
                }
            }
            if(tokenLengthError){
                [self showErrorAlertWithTitle:NSLocalizedString(@"Token length invalid", nil) andMessage:NSLocalizedString(@"The given token is not valid, please check if you have the latest application version", nil) exitOnError:NO];
            } else {
                [self showErrorAlertWithTitle:NSLocalizedString(@"Invalid password",nil) andMessage:NSLocalizedString(@"Password must be more than 8 characters and contain \n•1 lowercase letter \n•1 uppercase letter \n•1 number \n•1 special charater",nil) exitOnError:NO];
            }
        } else if(errorCode.integerValue == 401) {
            NSNumber *detailedCode = (NSNumber *)jsonResponse[@"errorDetailsCode"];
            if(detailedCode.integerValue == 401202){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Unauthorized access", nil) exitOnError:YES];
            } else if(detailedCode.integerValue == 401510){
                [self showErrorAlertWithMessage:NSLocalizedString(@"User not found", nil) exitOnError:YES];
            } else if(detailedCode.integerValue == 401520){
                [self showErrorAlertWithMessage:NSLocalizedString(@"User not activated", nil) exitOnError:YES];
            } else if(detailedCode.integerValue == 401521){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Company not activated", nil) exitOnError:YES];
            } else if(detailedCode.integerValue == 401600){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Invalid or expired code", nil) exitOnError:NO];
            } else {
                [self showErrorAlertWithMessage:NSLocalizedString(@"An unknown error occured", nil) exitOnError:YES];
            }
        } else if(errorCode.integerValue == 403) {
            NSNumber *detailedCode = (NSNumber *)jsonResponse[@"errorDetailsCode"];
            if(detailedCode.integerValue == 403000){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Access denied, user don't have required role", nil) exitOnError:YES];
            } else if (detailedCode.integerValue == 403100){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Access denied, wrong user", nil) exitOnError:YES];
            } else if (detailedCode.integerValue == 403500){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Email is black-listed, could not register user", nil) exitOnError:YES];
            } else {
                [self showErrorAlertWithMessage:NSLocalizedString(@"An unknown error occured", nil) exitOnError:YES];
            }
        } else if(errorCode.integerValue == 404) {
            [self showErrorAlertWithMessage:NSLocalizedString(@"User does not exist.", nil) exitOnError:YES];
        } else if(errorCode.integerValue == 409) {
            [self showErrorAlertWithMessage:NSLocalizedString(@"A user with this email already exist in Rainbow system", nil) exitOnError:YES];
        } else {
            [self showErrorAlertWithMessage:NSLocalizedString(@"An unknown error occured", nil) exitOnError:YES];
        }
    }
}

-(void) showErrorAlertWithTitle:(NSString *) title andMessage:(NSString *)message exitOnError:(BOOL) exit {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        if(exit){
            [self.delegate cancelWizard];
        }
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) showErrorAlertWithMessage:(NSString *) message exitOnError:(BOOL) exit{
    [self showErrorAlertWithTitle:NSLocalizedString(@"Error", nil) andMessage:message exitOnError:exit];
}

- (void)didTapInBackground:(UITapGestureRecognizer *)sender {
    if(_codeTextField.isFirstResponder)
        [_codeTextField resignFirstResponder];
    if(_passwordTextField.isFirstResponder)
        [_passwordTextField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == _codeTextField)
        [_passwordTextField becomeFirstResponder];
    
    if(textField == _passwordTextField){
        [_passwordTextField resignFirstResponder];
        if(self.continueButton.enabled)
            [self continueButtonTapped:self.continueButton];
    }
    return YES;
}

-(BOOL) shouldDisplay {
    if(self.invitationInformations)
        return YES;
    if(self.lostPasswordMode)
        return YES;
    if(self.isGuestMode)
        return NO;
    
    return YES;
}
@end
