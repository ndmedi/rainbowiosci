/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "WelcomePageCommonViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/defines.h>
#import "UILabel+Clickable.h"
#import "ACFloatingTextField.h"

@interface WelcomePage1ViewController : WelcomePageCommonViewController
@end

@interface WelcomePage1ViewController ()
@property (weak, nonatomic) IBOutlet ACFloatingTextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIImageView *emailImageView;
@property (weak, nonatomic) IBOutlet UILabel *presentationLabel2;
@property (weak, nonatomic) IBOutlet UILabel *tosLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeightContraint;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *continueButtonTopConstraint;
@end

@implementation WelcomePage1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_emailTextField setTintColor:[UITools foregroundGrayColor]];
    _emailTextField.placeholder = NSLocalizedString(@"Email address", nil);
    [UITools applyCustomFontToTextField:_emailTextField];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _tosLabel.hidden = YES;
    if(self.lostPasswordMode){
        self.presentationLabel.text = NSLocalizedString(@"Please enter the e-mail address associated with your Rainbow account.", nil);
        _presentationLabel2.text = NSLocalizedString(@"A password reset code will be sent to you.", nil);
        _presentationLabel2.hidden = NO;
        if([[[UIDevice currentDevice] model] hasPrefix:@"iPad"]){
            self.presentationLabelTopConstraint.constant -= 50;
            _continueButtonTopConstraint.constant -= 30;
        }
    } else {
        _tosLabel.hidden = NO;
        self.presentationLabel.text = NSLocalizedString(@"Create account", nil);
        _presentationLabel2.hidden = YES;
        self.containerViewTopConstraint.constant -= _presentationLabel2.frame.size.height;
        NSString *tosText = NSLocalizedString(@"By continuing, you are indicating that you agree the Rainbow Terms of Service and Privacy Policy", nil);
        NSMutableAttributedString *tosTextAttributed = [[NSMutableAttributedString alloc] initWithString:tosText];

        NSString *tosLinkText = NSLocalizedString(@"Terms of Service", nil);
        NSString *privacyLinkText = NSLocalizedString(@"Privacy Policy", nil);

        NSRange tosLinkRange = [tosText rangeOfString:tosLinkText options:NSNumericSearch];
        NSRange privacyLinkRange = [tosText rangeOfString:privacyLinkText options:NSNumericSearch];
        
        NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionaryWithDictionary:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:13] ,  NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle), NSLinkAttributeName: [NSURL URLWithString:@"https://www.openrainbow.com/terms-service/"], NSForegroundColorAttributeName: [UITools defaultTintColor]}];
        
        [tosTextAttributed setAttributes:linkAttributes range:tosLinkRange];
        
        [linkAttributes setObject:[NSURL URLWithString:@"https://www.openrainbow.com/dataprivacy"] forKey:NSLinkAttributeName];
        
        [tosTextAttributed setAttributes:linkAttributes range:privacyLinkRange];
        
        _tosLabel.attributedText = tosTextAttributed;
        _tosLabel.userInteractionEnabled = YES;
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnTosLabel:)];
        [_tosLabel addGestureRecognizer:_tapGesture];
    }
    
    // Adjust container view depending on the tos label visibility
    _containerViewHeightContraint.constant = (_tosLabel.isHidden) ? 160 : 210; // todo: calculate diff automatically
    
    _emailTextField.borderStyle = UITextBorderStyleNone;
    _emailTextField.lineColor = [UITools foregroundGrayColor];
    _emailTextField.selectedLineColor = [UITools defaultTintColor];
    _emailTextField.placeHolderColor = [UITools foregroundGrayColor];
    _emailTextField.selectedPlaceHolderColor = [UITools defaultTintColor];

}

- (void)handleTapOnTosLabel:(UITapGestureRecognizer *)tapGesture {
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    [_tosLabel openTappedLinkAtLocation:locationOfTouchInLabel];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(!self.lostPasswordMode){
        [_tosLabel removeGestureRecognizer:_tapGesture];
        _tapGesture = nil;
    }
}

- (IBAction)emailTextFieldChanged:(UITextField *)sender {
    if(_emailTextField.text.length > 0) {
        self.continueButton.enabled = YES;
        self.continueButton.alpha = 1.0;
    } else {
        self.continueButton.enabled = NO;
        self.continueButton.alpha = 0.5;
    }
}

- (IBAction)continueButtonTapped:(UIButton *)sender {
    NSString *emailEntered = _emailTextField.text;
    [self showHUD];
    
    if(self.lostPasswordMode){
        [[ServicesManager sharedInstance].loginManager sendResetPasswordEmailWithLoginEmail:emailEntered completionHandler:^(NSDictionary *jsonResponse, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hideHUD];
                if(error){
                    if(jsonResponse){
                        NSNumber *errorCode = (NSNumber *)jsonResponse[@"errorCode"];
                        NSNumber *detailedCode = (NSNumber *)jsonResponse[@"errorDetailsCode"];
                        NSString *errorDetailsMsg = nil;
                        if([jsonResponse[@"errorDetails"] isKindOfClass:[NSArray class]]){
                            NSArray * errorDetails = jsonResponse[@"errorDetails"];
                            errorDetailsMsg = errorDetails[0][@"msg"];
                        } else {
                            errorDetailsMsg = jsonResponse[@"errorDetails"];
                        }
                        switch (errorCode.integerValue) {
                            case 400:{
                                [self showErrorAlertWithMessage:errorDetailsMsg];
                                break;
                            }
                            case 404:{
                                if(detailedCode.integerValue == 404101){
                                    [self showErrorAlertWithMessage:errorDetailsMsg];
                                } else if(detailedCode.integerValue == 404100) {
                                    [self showErrorAlertWithMessage:errorDetailsMsg];
                                } else {
                                    [self showErrorAlertWithMessage:NSLocalizedString(@"An unknown error occured", nil)];
                                }
                                break;
                            }
                            case 500:{
                                [self showErrorAlertWithMessage:error.localizedDescription];
                                break;
                            }
                            default:
                                break;
                        }
                    } else {
                        [self showErrorAlertWithMessage:error.localizedDescription];
                    }
                } else {
                    // Everything ok, go to next screen
                    [[ServicesManager sharedInstance].loginManager setUsername:emailEntered andPassword:nil];
                    [self.delegate nextPage];
                }
            });
        }];
    } else {
        [[ServicesManager sharedInstance].loginManager sendNotificationForEnrollmentTo:emailEntered completionHandler:^(NSDictionary *jsonResponse, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hideHUD];
                if(error){
                    if(jsonResponse){
                        NSNumber *errorCode = (NSNumber*)jsonResponse[@"errorCode"];
                        if(errorCode.integerValue == 409)
                            [self showErrorAlertWithMessage:NSLocalizedString(@"A user with this email already exist in Rainbow system", nil)];
                        else
                            [self showErrorAlertWithMessage:NSLocalizedString(@"An unknown error occured", nil)];
                    } else {
                        [self showErrorAlertWithMessage:error.localizedDescription];
                    }
                } else {
                    // Everything ok, go to next screen
                    [[ServicesManager sharedInstance].loginManager setUsername:emailEntered andPassword:nil];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kWizardCompleted];
                    [self.delegate nextPage];
                }
            });
        }];
    }
}

-(void) moveToNextPage {
    [self.delegate nextPage];
}

- (void)didTapInBackground:(UITapGestureRecognizer *)sender {
    if(_emailTextField.isFirstResponder)
        [_emailTextField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == _emailTextField){
        [_emailTextField resignFirstResponder];
        if(self.continueButton.enabled)
            [self continueButtonTapped:self.continueButton];
    }
    return YES;
}

-(BOOL) shouldDisplay {
    return self.isGuestMode ? NO : YES ;
}


@end
