/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "WelcomePageCommonViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/Tools.h>

@interface WelcomePageCommonViewController () <UITextFieldDelegate>
@property (nonatomic, strong) UITapGestureRecognizer *tapInBackgroundGestureReconizer;
@property (nonatomic, weak) UITextField *activeField;
@property (nonatomic, strong) UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *tosLabel;
@end

@implementation WelcomePageCommonViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        _tapInBackgroundGestureReconizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapInBackground:)];
        _tapInBackgroundGestureReconizer.numberOfTapsRequired = 1;
    }
    return self;
}

-(void) dealloc {
    _hud = nil;
    _activeField = nil;
    _tapInBackgroundGestureReconizer = nil;
    _invitationInformations = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [_topView removeFromSuperview];
    _topView = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
    _topView.backgroundColor = [UITools defaultBackgroundColor];
    
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UITools defaultBackgroundColor];
    [UITools applyCustomFontTo:_presentationLabel];

    [UITools applyCustomFontTo:_continueButton.titleLabel];
    [_continueButton setBackgroundColor:[UITools defaultTintColor]];
    [_continueButton setTitle:[NSLocalizedString(@"Continue", nil) uppercaseString] forState:UIControlStateNormal];
    _continueButton.enabled = NO;
    _continueButton.alpha = 0.5;
    _continueButton.layer.cornerRadius = 8.0f;
    
    [self.view addGestureRecognizer:_tapInBackgroundGestureReconizer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowHide:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if([[[UIDevice currentDevice] model] hasPrefix:@"iPad"]){
        _containerViewTopConstraint.constant -= 30;
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication].keyWindow addSubview:_topView];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _logo.transform = CGAffineTransformIdentity;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [_topView removeFromSuperview];
}

-(void) showHUD {
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.mode = MBProgressHUDModeIndeterminate;
    _hud.labelText = NSLocalizedString(@"", nil);
    _hud.removeFromSuperViewOnHide = NO;
    [_hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
}

-(void) hideHUD {
    [_hud hide:YES];
}

-(void) showErrorAlertWithMessage:(NSString *) message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)didTapInBackground:(UITapGestureRecognizer *)sender {
    _activeField = nil;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    _activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    _activeField = nil;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}



- (void)keyboardWillShowHide:(NSNotification *)notification {
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    NSInteger animationCurveOption = (curve << 16);

    CGRect containerViewFrame = _containerView.frame;
    NSLog(@"Container view frame intersect with keyboard ? %@", NSStringFromBOOL(CGRectIntersectsRect(containerViewFrame, keyboardRect)));
    
    UIScrollView *scrollView = (UIScrollView *) self.view;
    if(!_activeField || CGRectIntersectsRect(containerViewFrame, keyboardRect)) {
        if(self.view.frame.origin.y == 0){
            CGRect intersection = CGRectIntersection(containerViewFrame, keyboardRect);
            
            [UIView animateWithDuration:0.0 delay:0.0 options:animationCurveOption animations:^{
                BOOL up = [notification.name isEqualToString:UIKeyboardWillShowNotification];
                
                if(up){
                    if(!CGRectIsNull(intersection)){
                        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, MAX(intersection.size.height, intersection.origin.y), 0.0);
                        scrollView.contentInset = contentInsets;
                        scrollView.scrollIndicatorInsets = contentInsets;
                        
                        CGPoint scrollPoint = CGPointMake(0.0, intersection.size.height+ 20);
//                        // This is the first page and we are trying to reset the password, so we add an offset due too the second label displayed in the page
//                        if(_lostPasswordMode && self.view.tag == 0)
//                            scrollPoint.y += 30;
                        scrollView.contentOffset = scrollPoint;
                    }
                } else {
                    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
                    scrollView.contentInset = contentInsets;
                    scrollView.scrollIndicatorInsets = contentInsets;
                    scrollView.contentOffset = CGPointZero;
                }
            } completion:nil];
        } else if([notification.name isEqualToString:UIKeyboardWillHideNotification]) {
            [UIView animateWithDuration:duration delay:0.0 options:animationCurveOption animations:^{
                UIEdgeInsets contentInsets = UIEdgeInsetsZero;
                scrollView.contentInset = contentInsets;
                scrollView.scrollIndicatorInsets = contentInsets;
                scrollView.contentOffset = CGPointZero;
            } completion:nil];
        }
    }
}

-(BOOL) shouldDisplay {
    // Always return `YES` except if a sub class decide otherwise
    return YES;
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end

