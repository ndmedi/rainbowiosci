/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import <Rainbow/Server.h>
#import "WelcomePageProtocol.h"

@interface WelcomePageCommonViewController : UIViewController
@property (nonatomic) NSInteger pageTag;
@property (weak, nonatomic) IBOutlet UILabel *presentationLabel;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic, weak) id<WelcomePageProtocol> delegate;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic) BOOL lostPasswordMode;
@property (nonatomic) BOOL isGuestMode;
@property (nonatomic, strong) NSDictionary *invitationInformations;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *presentationLabelTopConstraint;
-(void) showHUD;
-(void) hideHUD;
-(void) showErrorAlertWithMessage:(NSString *) message;
-(IBAction) didTapInBackground:(UITapGestureRecognizer *)sender;
-(BOOL) shouldDisplay;
@end


typedef void (^AuthorizeRequesterCompletionHandler) (BOOL succeed);

typedef NS_ENUM(NSInteger, AuthorizeRequesterStatus) {
    AuthorizeRequesterUnknown = 0,
    AuthorizeRequesterAccepted,
    AuthorizeRequesterNotAccepted
};

@protocol AuthorizeRequester <NSObject>
-(NSString *)title;
-(AuthorizeRequesterStatus) status;
-(BOOL) needUserRequest;
-(void) requestWithCompletionHandler:(AuthorizeRequesterCompletionHandler)completionHandler;
@end

@interface PushNotificationRequester : NSObject <AuthorizeRequester>
@property (nonatomic) BOOL granted;
@property (nonatomic, copy) AuthorizeRequesterCompletionHandler completionHandler;
@end

@interface ContactAccessRequester : NSObject <AuthorizeRequester>
@property (nonatomic, copy) AuthorizeRequesterCompletionHandler completionHandler;
@end

@interface MicrophoneAccessRequester : NSObject <AuthorizeRequester>
@property (nonatomic, copy) AuthorizeRequesterCompletionHandler completionHandler;
@end

@interface CameraAccessRequester : NSObject <AuthorizeRequester>
@property (nonatomic, copy) AuthorizeRequesterCompletionHandler completionHandler;
@end

@interface PhotoLibraryRequester : NSObject <AuthorizeRequester>
@property (nonatomic, copy) AuthorizeRequesterCompletionHandler completionHandler;
@end

@interface AnalyticsRequester : NSObject <AuthorizeRequester>
-(instancetype) initWithController:(UIViewController*) controller;
@property (nonatomic, copy) AuthorizeRequesterCompletionHandler completionHandler;
@end
