/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "MyInfoNavigationItem.h"
#import "UISearchResultController.h"
#import "CustomSearchController.h"
#import "CustomNavigationController.h"
#import <Rainbow/ServicesManager.h>
#import "CustomSearchBarContainerView.h"
#import "UIStoryboardManager.h"

@interface MyInfoNavigationItem () <CustomSearchControllerDelegate>
@property (nonatomic, strong) UIAvatarView *myAvatarView;
@property (nonatomic, strong) CustomSearchController *customSearchController;
@property (nonatomic, strong) UISearchResultController*  searchResultController;
@property (nonatomic, strong) NSArray *leftItems;
@end

@implementation MyInfoNavigationItem
-(instancetype) initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if(self){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willLogin:) name:kLoginManagerWillLogin object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tabBarControllerDidSelectViewController:) name:@"tabBarControllerDidSelectViewController" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerWillLogin object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"tabBarControllerDidSelectViewController" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidFinishLaunchingNotification object:nil];
}

-(void) awakeFromNib {
    [super awakeFromNib];
    _myAvatarView = [[UIAvatarView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    _myAvatarView.asCircle = YES;
    _myAvatarView.showPresence = NO;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectMyAvatar:)];
    [_myAvatarView addGestureRecognizer:tap];
    _myAvatarView.userInteractionEnabled = YES;
    _myAvatarView.backgroundColor = [UIColor clearColor];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -10;
    
    UIBarButtonItem *positiveSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    positiveSpacer.width = 10;
    
    _leftItems = [NSArray arrayWithObjects:negativeSpacer, [[UIBarButtonItem alloc] initWithCustomView:_myAvatarView], positiveSpacer, nil];
    
    [self setLeftBarButtonItems:_leftItems animated:NO];
    
    _searchResultController = (UISearchResultController*)[[UIStoryboardManager sharedInstance].searchStoryboard instantiateViewControllerWithIdentifier:@"UISearchResultController"];
    
    // Search controller
    _customSearchController = [[CustomSearchController alloc] initWithSearchResultsController:_searchResultController customSearchControllerDelegate:self];
     _searchResultController.modalPresentationStyle = UIModalPresentationFullScreen;
    CustomSearchBarContainerView *titleView = [[CustomSearchBarContainerView alloc] initWithSearchBar:_customSearchController.searchBar];
    titleView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
    self.titleView = titleView;
}

-(void) setCustomRightItems:(NSArray<UIBarButtonItem *> *)customRightItems {
    _customRightItems = customRightItems;
    self.rightBarButtonItems = _customRightItems;
}

-(void) setParentViewController:(id)parentViewController {
    _parentViewController = parentViewController;
    _searchResultController.currentViewController = _parentViewController;
    _searchResultController.uiDelegate = _parentViewController;
}

#pragma mark - my info
-(void) didSelectMyAvatar:(UIBarButtonItem *) sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myInfoNavigationItemWillPresentMyInfos" object:nil];
    CustomNavigationController *myInfos = (CustomNavigationController*)[[UIStoryboardManager sharedInstance].myInfosStoryBoard instantiateViewControllerWithIdentifier:@"myInfosNavigationControllerID"];
    myInfos.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [_parentViewController presentViewController:myInfos animated:YES completion:nil];
}

#pragma mark - CustomSearchControllerDelegate
-(void)customSearchController:(CustomSearchController *)customSearchController didPressCancelButton:(UISearchBar *)searchBar {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myInfoNavigationItemDidDismissSearchController" object:nil];
}

-(void) willPresentSearchController:(CustomSearchController *) customSearchController {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myInfoNavigationItemWillPresentNewView" object:nil];
    [self setLeftBarButtonItems:nil];
    self.rightBarButtonItems = nil;
    ((CustomSearchBarContainerView*)self.titleView).frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
}

- (void)willDismissSearchController:(CustomSearchController *) customSearchController {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myInfoNavigationItemWillDismissSearchController" object:nil];
    [self setLeftBarButtonItems:_leftItems];
    self.rightBarButtonItems = _customRightItems;
    ((CustomSearchBarContainerView*)self.titleView).frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
}

#pragma mark - Rainbow notifications

-(void) applicationDidFinishLaunching:(NSNotification *) notification {
    _myAvatarView.peer = [ServicesManager sharedInstance].myUser.contact;
}

-(void) willLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self willLogin:notification];
        });
        return;
    }
    
    _myAvatarView.peer = [ServicesManager sharedInstance].myUser.contact;
}

-(void) didLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    
    _myAvatarView.peer = [ServicesManager sharedInstance].myUser.contact;
}

-(void) didLogout:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    _myAvatarView.peer = nil;
}

-(void) tabBarControllerDidSelectViewController:(NSNotification *) notification {
    [_customSearchController dismissSearchController];
}

-(void) focusSearchController {
    _customSearchController.active = YES;
}
@end
