/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIConferencesViewController.h"

#import <Rainbow/ServicesManager.h>
#import <Rainbow/ConferencesManagerService.h>
#import "UIInvitationConferencesTableViewCell.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "DZNSegmentedControl.h"
#import "BGTableViewRowActionWithImage.h"
#import "UIRainbowGenericTableViewCell.h"
#import "UITools.h"
#import "MyInfoNavigationItem.h"
#import "UIMessagesViewController.h"
#import "UIStoryboardManager.h"
#import "UIGenericBubbleDetailsViewController.h"
#import "CustomNavigationController.h"
#import "FilteredSortedSectionedArray.h"
#import "Room+Extensions.h"
#import "UIRainbowGenericTableViewCell.h"
#import "Room+UIRainbowGenericTableViewCellProtocol.h"
#import "UIContactDetailsViewController.h"
#import "RecentsConversationsTableViewController.h"
#import "UIViewController+Visible.h"
#import "UIGuestModeViewController.h"
#import "UINetworkLostViewController.h"

#define kConferencesTableCell @"ConferencesTableCell"
#define kInvitationConferencesTableCell @"InvitationConferencesTableCell"

#define kMeetingInvitationsSection @"Invitations"
#define kMyMeetingsSection @"My meetings"
#define kHistorySection @"History"

typedef NS_ENUM(NSInteger, SelectorType){
    SelectorTypeMyMeetings = 0,
    SelectorTypeHistory
};

@interface UIConferencesViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, DZNSegmentedControlDelegate>
@property (nonatomic, strong) ConferencesManagerService *conferencesManagerService;
@property (nonatomic, strong) ConversationsManagerService *conversationsManagerService;
@property (nonatomic, strong) RoomsService *roomsService;
@property (nonatomic, strong) IBOutlet DZNSegmentedControl *historyFilter;
@property (nonatomic, strong) NSObject *conferencesMutex;
@property (nonatomic, strong) FilteredSortedSectionedArray<Room *> *meetings;
@property (nonatomic, strong) NSPredicate *globalFilterMyMeetings;
@property (nonatomic, strong) NSPredicate *globalFilterHistory;
@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *createMeetingButton;

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic) BOOL lostNetworkViewAsBeenScrolled;
@property (nonatomic) BOOL guestBannerViewAsBeenScrolled;
@property (nonatomic) BOOL isNetworkConnected;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation UIConferencesViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        _conferencesManagerService = [ServicesManager sharedInstance].conferencesManagerService;
        _conversationsManagerService = [ServicesManager sharedInstance].conversationsManagerService;
        _roomsService = [ServicesManager sharedInstance].roomsService;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddRoom:) name:kRoomsServiceDidAddRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRoom:) name:kRoomsServiceDidRemoveRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(roomInvitationStatusChanged:) name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dumpMeetings:) name:@"dumpMeetings" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddConference:) name:kConferencesManagerDidAddConference object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConference:) name:kConferencesManagerDidUpdateConference object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveCreateConfUserActivated:) name:kContactsManagerServiceDidReceiveCreateConfUserActivated object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveCreateConfUserActivated:) name:kMyUserFeatureDidUpdate object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStartConversation:) name:kConversationsManagerDidStartConversation object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowNetworkLostView:) name:CustomNavigationControllerWillShowNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNetworkLostViewFrame:) name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowGuestModeView:) name:CustomNavigationControllerWillShowGuestModeView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideGuestModeView:) name:CustomNavigationControllerDidHideGuestModeView object:nil];
        
        _sectionedByName = ^NSString*(Room *aRoom) {
            if(_historyFilter.selectedSegmentIndex == SelectorTypeMyMeetings){
                if(aRoom.myStatusInRoom == ParticipantStatusInvited)
                    return kMeetingInvitationsSection;
            }
            return kMyMeetingsSection;
        };
        
        _globalFilterMyMeetings = [NSPredicate predicateWithBlock:^BOOL(Room *room, NSDictionary<NSString *,id> * bindings) {
            if([room.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                return NO;
            if(room.conference && room.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                if(room.conference.type == ConferenceTypeInstant){
                    if(room.myStatusInRoom == ParticipantStatusInvited || room.myStatusInRoom == ParticipantStatusAccepted){
                        return YES;
                    }
                }
                if(room.conference.type == ConferenceTypeScheduled) {
                    if(room.myStatusInRoom == ParticipantStatusInvited || room.myStatusInRoom == ParticipantStatusAccepted){
                        // When the conference is created for the room the notifcation has room.conference.end == nil
                        if(room.conference.end == nil || [room.conference.end isLaterThanDate:[NSDate date]])
                            return YES;
                    }
                }
            }
            return NO;
        }];
        
        _globalFilterHistory = [NSPredicate predicateWithBlock:^BOOL(Room *room, NSDictionary<NSString *,id> * bindings) {
            if([room.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                return NO;
            if(room.myStatusInRoom == ParticipantStatusInvited)
                return NO;
            if(room.conference && room.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                if(room.myStatusInRoom == ParticipantStatusRejected || room.myStatusInRoom == ParticipantStatusUnsubscribed || room.myStatusInRoom == ParticipantStatusDeleted){
                    return NO;
                }
                if(!room.conference.endpoint)
                    return YES;
                
                if(room.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio && room.conference.type == ConferenceTypeScheduled && [room.conference.end isEarlierThanDate:[NSDate date]]){
                    return YES;
                }
                
            }
            return NO;
        }];

        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            if([obj1 isEqualToString:kMeetingInvitationsSection])
                return NSOrderedAscending;
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
        }];
        
        _meetings = [FilteredSortedSectionedArray new];
        _meetings.sectionNameFromObjectComputationBlock = _sectionedByName;
        _meetings.globalFilteringPredicate = _globalFilterMyMeetings;
        _meetings.sectionSortDescriptor = _sortSectionAsc;
        
        _meetings.objectSortDescriptorForSection = @{@"__default__": @[[self sortDescriptorByStartDate]]};
        _conferencesMutex = [NSObject alloc];
        
        // When the room is opened in a conversation, sometimes we get the didAddRoom before this init, so we lose it, so just add initial rooms
        for (Room* room in _roomsService.rooms) {
            @synchronized (_conferencesMutex) {
                if(![_meetings containsObject:room])
                    [_meetings addObject:room];
            }
        }
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidAddRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRemoveRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidReceiveCreateConfUserActivated object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserFeatureDidUpdate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConferencesManagerDidAddConference object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConferencesManagerDidUpdateConference object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidStartConversation object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideGuestModeView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowGuestModeView object:nil];
    
    _conferencesMutex = nil;
    _conferencesManagerService = nil;
    _conversationsManagerService = nil;
    _roomsService = nil;
    _meetings = nil;
}

-(void) viewDidLoad {
    [super viewDidLoad];
    ((MyInfoNavigationItem*)self.navigationItem).parentViewController = self;
    ((MyInfoNavigationItem*)self.navigationItem).customRightItems = @[_createMeetingButton];
    
    self.definesPresentationContext = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    self.title = NSLocalizedString(@"Meetings", nil);
    _tableView.tableFooterView = [UIView new];
    [self.tableView setBackgroundColor:[UITools defaultBackgroundColor]];
    self.tableView.sectionHeaderHeight = 30;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
    
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    
    [_historyFilter setItems:@[NSLocalizedString(@"My meetings", nil), NSLocalizedString(@"History", nil)]];
    _historyFilter.delegate = self;
    _historyFilter.showsCount = NO;
    _historyFilter.autoAdjustSelectionIndicatorWidth = NO;
    _historyFilter.height = 60;
    _historyFilter.selectionIndicatorHeight = 4.0f;

    [_historyFilter addTarget:self action:@selector(historyFilterValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    CGRect frameTab = _historyFilter.frame;
    frameTab.size.width = self.view.frame.size.width;
    _historyFilter.frame = frameTab;
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, _historyFilter.height, 0);
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if([_meetings objectsInSection:kMeetingInvitationsSection].count > 0)
        _historyFilter.selectedSegmentIndex = 0;
    [self checkAndEnableCreateMeetingButton];
    [_meetings reloadData];
    [self.tableView reloadData];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)historyFilterValueChanged:(UISegmentedControl *)sender {
    
    if(sender.selectedSegmentIndex == SelectorTypeMyMeetings)
        _meetings.globalFilteringPredicate = _globalFilterMyMeetings;
    if(sender.selectedSegmentIndex == SelectorTypeHistory)
        _meetings.globalFilteringPredicate = _globalFilterHistory;
    
    [_meetings reloadData];
    [self.tableView reloadData];
}

-(NSSortDescriptor *) sortDescriptorByStartDate {
    NSSortDescriptor *sortByStartDate = [NSSortDescriptor sortDescriptorWithKey:@"conference.start" ascending:NO comparator:^NSComparisonResult(NSDate* date1, NSDate* date2) {
        return [date1 compare:date2];
    }];
    return sortByStartDate;
}

-(void) didLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    
    [self checkAndEnableCreateMeetingButton];
}

-(void) didLogout:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    
    @synchronized (_conferencesMutex) {
        [_meetings removeAllObjects];
    }
    
    [self.tableView reloadData];
    
    _createMeetingButton.enabled = NO;
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        _createMeetingButton.enabled = NO;
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    [self checkAndEnableCreateMeetingButton];
}

#pragma mark - network lost & guest mode banners
-(void) didChangeNetworkLostViewFrame:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
}

-(void) willShowNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant += kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

-(void) didHideNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant -= kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = YES;
        }];
    }
}

-(void) didHideGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _guestBannerViewAsBeenScrolled){
        self.topConstraint.constant -= kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:nil];
    }
}

-(void) willShowGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_guestBannerViewAsBeenScrolled){
        self.topConstraint.constant += kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

#pragma mark - Conversation notification
-(void) didStartConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didStartConversation:notification];
        });
        return;
    }
    
    // We don't want to open the conversation view if the application is in background, case of message send from a push notification.
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
        return;
    
    if(!self.isVisible)
        return;
    
    Conversation *theConversation = (Conversation *)notification.object;
    [RecentsConversationsTableViewController openConversationViewForConversation:theConversation inViewController:self.navigationController];
}

#pragma mark - Room events
-(void)didAddRoom:(NSNotification *)notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddRoom:notification];
        });
        return;
    }
    Room *theRoom = (Room *) notification.object;
    @synchronized (_conferencesMutex) {
        if(![_meetings containsObject:theRoom]){
            [_roomsService fetchRoomDetails:theRoom];
            [_meetings addObject:theRoom];
        }
    }
    if([self isViewLoaded])
        [self.tableView reloadData];
}


-(void) didUpdateRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateRoom:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
    
    if([changedKeys containsObject:@"name"] || [changedKeys containsObject:@"myStatusInRoom"] || [changedKeys containsObject:@"topic"] || [changedKeys containsObject:@"conference"]){
        [_meetings reloadData];
        if([self isViewLoaded])
            [self.tableView reloadData];
    }
}

-(void) didRemoveRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveRoom:notification];
        });
        return;
    }
    
    Room *theRoom = (Room *) notification.object;
    @synchronized (_conferencesMutex) {
        if([_meetings containsObject:theRoom])
            [_meetings removeObject:theRoom];
    }
    if([self isViewLoaded])
        [self.tableView reloadData];
}

-(void)roomInvitationStatusChanged: (NSNotification *)notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self roomInvitationStatusChanged:notification];
        });
        return;
    }
    if([self isViewLoaded]){
        [_meetings reloadData];
        [self.tableView reloadData];
    }
}

#pragma marl - Conferences events

-(void)didAddConference:(NSNotification *)notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddConference:notification];
        });
        return;
    }
    
    if([self isViewLoaded])
        [self.tableView reloadData];
}

-(void)didUpdateConference:(NSNotification *)notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateConference:notification];
        });
        return;
    }
    
    if([self isViewLoaded])
        [self.tableView reloadData];
}

#pragma mark - User settings event
-(void) didReceiveCreateConfUserActivated: (NSNotification *)notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReceiveCreateConfUserActivated:notification];
        });
        return;
    }
    
    [self checkAndEnableCreateMeetingButton];
}

-(void) checkAndEnableCreateMeetingButton {
    if( [self isViewLoaded] ) {
        _createMeetingButton.enabled = [ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyConference && [ServicesManager sharedInstance].myUser.isReadyToCreateConference && ![ServicesManager sharedInstance].myUser.isGuest;
        [self.tableView reloadData];
    }
}

#pragma mark - UITableviewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_meetings sections] count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_meetings sections] objectAtIndex:section];
    return [[_meetings objectsInSection:key] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionName = [[_meetings sections] objectAtIndex:section];
    if([_meetings objectsInSection:sectionName].count > 0){
        if([[_meetings sections] count] == 2 || [sectionName isEqualToString:kMeetingInvitationsSection])
            return NSLocalizedString(sectionName, nil);
    }
    return @"";
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    view.tintColor = [UITools colorFromHexa:0xF8F8F8FF];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont fontWithName:[UITools defaultFontName] size:20]];
    [header.textLabel setTextColor:[UITools colorFromHexa:0xB8B8B8FF]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_meetings sections] objectAtIndex:indexPath.section];
    Room *room = [[_meetings objectsInSection:key] objectAtIndex:indexPath.row];
    
    if (room.myStatusInRoom == ParticipantStatusInvited) {
        UIInvitationConferencesTableViewCell *cell = (UIInvitationConferencesTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kInvitationConferencesTableCell forIndexPath:indexPath];
        cell.room = room;
        return cell;
    }
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    cell.showMidLabel = YES;
    cell.cellObject = room;
    __weak Room *weakRoom = room;
    if(_historyFilter.selectedSegmentIndex == SelectorTypeHistory)
        cell.cellButtonTapHandler = nil;
    else {
        cell.cellButtonTapHandler = ^(UIButton *sender) {
            if(weakRoom.conference && weakRoom.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                if(![[ServicesManager sharedInstance].conferencesManagerService hasMyUserJoinedConference:weakRoom.conference]){
                    if(weakRoom.canJoin){
                        [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:weakRoom withCompletionHandler:^(Conversation *conversation, NSError *error) {
                            if(!error){
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"joinConference" object:weakRoom];
                                });
                            }
                        }];
                    }
                } else {
                    if(weakRoom.isMyRoom){
                        [[ServicesManager sharedInstance].conferencesManagerService disconnectParticipant:weakRoom.conference.myConferenceParticipant inConference:weakRoom.conference completionBlock:^(NSError *error) {
                            if(error){
                                NSLog(@"[UIConferencesViewController] error, %@", [error localizedDescription]);
                            } else {
                                // remove the hangup icon
                                [_meetings reloadData];
                                [self.tableView reloadData];
                            }
                        }];
                    }
                }
            }
        };
    }

    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *key = [[_meetings sections] objectAtIndex:indexPath.section];
    Room *room = [[_meetings objectsInSection:key] objectAtIndex:indexPath.row];
    if(![key isEqualToString:kInvitationConferencesTableCell])
        [_conversationsManagerService startConversationWithPeer:room withCompletionHandler:nil];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(![ServicesManager sharedInstance].loginManager.isConnected){
        return NO;
    }
    NSString *key = [[_meetings sections] objectAtIndex:indexPath.section];
    Room *room = [[_meetings objectsInSection:key] objectAtIndex:indexPath.row];

    // We must not delete instant conference we must detach the confEndpoint
    if(room.myStatusInRoom == ParticipantStatusInvited)
        return NO;
    
    if(room.conference.type == ConferenceTypeInstant)
        return NO;
    
    return YES;
}

-(void)cancelConference:(Conference *)conference {
    [_conferencesManagerService deleteConference:conference completionBlock:nil];
}

-(void) deleteActionForRoom:(Room *)room completionBlock:(void(^)(BOOL actionPerformed)) completionHandler {
    NSString *message = @"";
    
    if(room.isMyRoom)
        message = NSLocalizedString(@"Are you sure you want to permanently suppress this meeting? Participants will no longer have access to it.", nil);
    else
        message = NSLocalizedString(@"As soon as you will leave this meeting, this one will no longer be accessible. This operation cannot be canceled.", nil);
    
    UIAlertController *confirmDeleteActionSheet = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        dispatch_group_t deleteConferenceGroup = dispatch_group_create();
        dispatch_group_enter(deleteConferenceGroup);
        if(room.isMyRoom){
            [[ServicesManager sharedInstance].conferencesManagerService cancelInvitationSentToParticipants:room.participants toConference:room.conference inRoom:room completionBlock:^(NSError *error) {
                if(error){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error while deleting meeting", nil) message:NSLocalizedString(error.localizedDescription, nil) inViewController:self];
                    });
                    dispatch_group_leave(deleteConferenceGroup);
                    return;
                } else {
                    [[ServicesManager sharedInstance].roomsService detachConference:room.conference fromRoom:room completionBlock:^(NSError *error) {
                        if(error){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error while deleting meeting", nil) message:NSLocalizedString(error.localizedDescription, nil) inViewController:self];
                            });
                            dispatch_group_leave(deleteConferenceGroup);
                            return;
                        } else {
                            [[ServicesManager sharedInstance].conferencesManagerService deleteConference:room.conference completionBlock:^(NSError *error) {
                                if(error){
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error while deleting meeting", nil) message:NSLocalizedString(error.localizedDescription, nil) inViewController:self];
                                    });
                                    dispatch_group_leave(deleteConferenceGroup);
                                    return;
                                } else {
                                    [[ServicesManager sharedInstance].roomsService deleteRoom:room];
                                    Conversation *conversation = [[ServicesManager sharedInstance].conversationsManagerService getConversationWithPeerJID:room.jid];
                                    [[ServicesManager sharedInstance].conversationsManagerService stopConversation:conversation];
                                    dispatch_group_leave(deleteConferenceGroup);
                                }
                            }];
                        }
                    }];
                }
            }];
        } else {
            [[ServicesManager sharedInstance].roomsService leaveRoom:room];
            Conversation *conversation = [[ServicesManager sharedInstance].conversationsManagerService getConversationWithPeerJID:room.jid];
            [[ServicesManager sharedInstance].conversationsManagerService stopConversation:conversation];
            dispatch_group_leave(deleteConferenceGroup);
        }
        
        dispatch_group_notify(deleteConferenceGroup, dispatch_get_main_queue(), ^{
            if(completionHandler)
                completionHandler(YES);
            [_meetings reloadData];
            [self.tableView reloadData];
        });
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [confirmDeleteActionSheet addAction:ok];
    [confirmDeleteActionSheet addAction:cancel];
    // show the menu.
    [confirmDeleteActionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:confirmDeleteActionSheet animated:YES completion:nil];
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_meetings sections] objectAtIndex:indexPath.section];
    Room *room = [[_meetings objectsInSection:key] objectAtIndex:indexPath.row];
    BGTableViewRowActionWithImage *delete = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:room.isMyRoom?NSLocalizedString(@"Delete", nil):NSLocalizedString(@"Quit", nil) backgroundColor:[UITools redColor] image:room.isMyRoom?[UIImage imageNamed:@"DeleteConference"]:[UIImage imageNamed:@"ExitMeeting"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self deleteActionForRoom:room completionBlock:nil];
    }];
    
    BGTableViewRowActionWithImage *edit = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Edit", nil) backgroundColor:[UIColor lightGrayColor] image:[UIImage imageNamed:@"EditMeeting"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self editActionForRoom:room];
    }];
    
    if(room.isMyRoom && _historyFilter.selectedSegmentIndex == SelectorTypeMyMeetings)
        return @[delete, edit];
    
    return @[delete];
}

-(void) editActionForRoom:(Room *) room {
    CustomNavigationController *navCtrl = [[UIStoryboardManager sharedInstance].addAttendeesStoryBoard instantiateViewControllerWithIdentifier:@"addAttendeeViewControllerID"];
    UIGenericBubbleDetailsViewController *addAttendeeViewController = (UIGenericBubbleDetailsViewController*)[navCtrl.viewControllers firstObject];
    addAttendeeViewController.meeting = YES;
    addAttendeeViewController.roomName = room.displayName;
    addAttendeeViewController.roomTopic = room.topic;
    addAttendeeViewController.room = room;
    NSMutableArray<Contact *> *contacts = [NSMutableArray new];
    [room.participants enumerateObjectsUsingBlock:^(Participant * aParticipant, NSUInteger idx, BOOL * stop) {
        if(![aParticipant.contact isEqual:[ServicesManager sharedInstance].myUser.contact])
            [contacts addObject:aParticipant.contact];
    }];
    addAttendeeViewController.roomParticipants = contacts;
    [self presentViewController:navCtrl animated:YES completion:nil];
}

#pragma mark - DZNEmptyDataSet
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    if([ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyConference)
        return [UIImage imageNamed:@"EmptyMeetingCreate"];
    return [UIImage imageNamed:@"EmptyMeeting"];
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultTintColor];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"";
    if(_historyFilter.selectedSegmentIndex == 0) {
        if([ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyConference)
            text = NSLocalizedString(@"Schedule and invite people to a meeting.", nil);
        else
            text = NSLocalizedString(@"Meetings you have been invited to will appear here.", nil);
    } else {
        text = NSLocalizedString(@"You have no past meetings.", nil);
    }
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    if(_historyFilter.selectedSegmentIndex == 1) {
        if([ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyConference){
            NSString *text = NSLocalizedString(@"Schedule and invite people to a meeting.", nil);
            
            NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
            
            return [[NSAttributedString alloc] initWithString:text attributes:attributes];
        } else
            return nil;
    } else
        return nil;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    if(![ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyConference)
        return nil;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    if([ServicesManager sharedInstance].loginManager.isConnected) {
        if([ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyConference && [ServicesManager sharedInstance].myUser.isReadyToCreateConference)
            return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Schedule a meeting", nil) attributes:attributes];
        else
            return nil;
    }
    return nil;
}

-(UIImage *) buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    return [UITools imageWithColor:[UITools defaultTintColor] size:CGSizeMake(1, 40)];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    CGFloat offset = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
    offset += CGRectGetHeight(self.navigationController.navigationBar.frame);
    offset -= CGRectGetHeight(_historyFilter.frame);
    return -offset;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 0.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    [self didTapCreateMeeting:nil];
}

#pragma mark - Cell button action

- (IBAction)cellButtonAction:(id)sender {
}

- (IBAction)didTapCreateMeeting:(UIBarButtonItem *)sender {
    
    CustomNavigationController *navCtrl = [[UIStoryboardManager sharedInstance].addAttendeesStoryBoard instantiateViewControllerWithIdentifier:@"addAttendeeViewControllerID"];
    UIGenericBubbleDetailsViewController *addAttendeeViewController = (UIGenericBubbleDetailsViewController*)[navCtrl.viewControllers firstObject];
    addAttendeeViewController.meeting = YES;
    
    [self presentViewController:navCtrl animated:YES completion:nil];
}


-(void) dumpMeetings:(NSNotification *) notification {
    NSLog(@"DUMP Meetings : %@", [_meetings description]);
}

#pragma mark - Search delegate
- (void) presentContactDetailController:(Contact*) contact{
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void) scrollToTop {
    [self.tableView setContentOffset:CGPointMake(0.0f, -self.tableView.contentInset.top) animated:YES];
}
@end
