/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIInvitationConferencesTableViewCell.h"
#import "UITools.h"
#import "UIAvatarView.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/Room.h>
#import "Room+UIRainbowGenericTableViewCellProtocol.h"

@interface UIInvitationConferencesTableViewCell ()
@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UILabel *date;
@property (nonatomic, weak) IBOutlet UILabel *organizerLabel;
@property (nonatomic, weak) IBOutlet UILabel *organizerName;
@property (nonatomic, weak) IBOutlet UIAvatarView *avatar;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIButton *declineButton;
@end

@implementation UIInvitationConferencesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomBoldFontTo:_label];
    _label.textColor = [UITools defaultTintColor];
    [UITools applyCustomFontTo:_date];
    _date.textColor = [UITools defaultTintColor];
    [UITools applyCustomFontTo:_organizerLabel];
    _organizerLabel.textColor = [UITools foregroundGrayColor];
    [UITools applyCustomFontTo:_organizerName];
    _organizerName.textColor = [UITools foregroundGrayColor];
    _label.text = @"";
    _date.text = @"";
    _organizerLabel.text = NSLocalizedString(@"Organizer:", @"");
    _organizerName.text = @"";
    _avatar.asCircle = YES;
}

-(void)dealloc {

}

-(void) setRoom:(Room *)room {
    _room = room;
    _avatar.peer = _room;
    
    if(_room.conference.type == ConferenceTypeInstant)
        _label.text = [NSString stringWithFormat:@"%@ [%@]", _room.displayName, NSLocalizedString(@"Meet now", @"")];
    else
        _label.text = _room.displayName;
    
    NSString *dateStr = [UITools formatDateTimeForDate:_room.conference.start];
    if(_room.conference.end && dateStr){
        if([_room.conference.end isEqualToDateIgnoringTime:room.conference.start])
            dateStr = [NSString stringWithFormat:@"%@ - %@", dateStr, [UITools formatTimeForDate:_room.conference.end]];
        else
            dateStr = [NSString stringWithFormat:@"%@ - %@", dateStr, [UITools formatDateTimeForDate:_room.conference.end]];
    }
    
    _date.text = dateStr;
    
    _organizerName.text = _room.isMyRoom?NSLocalizedString(@"Me",nil):_room.conference.owner.displayName;
}


- (IBAction)acceptInvitation:(UIButton *)sender {
 
    [[ServicesManager sharedInstance].roomsService acceptInvitation:_room completionBlock:nil];
    
}

- (IBAction)declineInvitation:(UIButton *)sender {
   
    [[ServicesManager sharedInstance].roomsService declineInvitation:_room completionBlock:nil];
}

@end
