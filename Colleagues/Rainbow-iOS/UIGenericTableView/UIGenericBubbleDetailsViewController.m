/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIWithDatePickerTableViewCell.h"
#import "UIWithToggleTableViewCell.h"
#import "UIGenericBubbleDetailsViewController.h"
#import "UIGenericAddObjectWithTokenViewController.h"
#import "ACFloatingTextField.h"
#import "UITools.h"

@interface UIGenericBubbleDetailsViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightBarButton;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *nameLabel;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *subNameLabel;
@property (nonatomic, strong) NSMutableArray *expandedIndexPaths;
@property (nonatomic) BOOL isMeeting;
@property (nonatomic) BOOL isScheduledMeeting;
@property (nonatomic, strong) NSString *initialRoomName;
@property (nonatomic, strong) NSString *initialRoomTopic;
@property (nonatomic, strong) NSArray<Contact*> *initialParticipants;
@end

@implementation UIGenericBubbleDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isScheduledMeeting = YES;
    if(_isMeeting){
        self.title = NSLocalizedString(@"Schedule a meeting", nil);
        if(_room)
            self.title = NSLocalizedString(@"Edit the meeting", nil);
    } else
        self.title = NSLocalizedString( @"Create a bubble", nil);
    
    self.navigationController.navigationBar.translucent = NO;
    [self.view setBackgroundColor:[UITools defaultBackgroundColor]];
    
    [_nameLabel setPlaceholder:NSLocalizedString( (_isMeeting) ? @"Meeting name" : @"Bubble name", nil)];
    [UITools applyCustomFontToTextField:_nameLabel];
    _nameLabel.textColor = [UITools defaultTintColor];
    _nameLabel.tintColor = [UITools defaultTintColor];
    _nameLabel.lineColor = [UITools defaultTintColor];
    _nameLabel.selectedLineColor = [UITools defaultTintColor];
    _nameLabel.placeHolderColor = [UITools defaultTintColor];
    _nameLabel.selectedPlaceHolderColor = [UITools defaultTintColor];
    [_nameLabel addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [_subNameLabel setPlaceholder:NSLocalizedString( (_isMeeting) ? @"Meeting topic (Optional)" : @"Bubble topic (Optional)", nil)];
    [UITools applyCustomFontToTextField:_subNameLabel];
    _subNameLabel.textColor = [UITools defaultTintColor];
    _subNameLabel.tintColor = [UITools defaultTintColor];
    _subNameLabel.lineColor = [UITools defaultTintColor];
    _subNameLabel.selectedLineColor = [UITools defaultTintColor];
    _subNameLabel.placeHolderColor = [UITools defaultTintColor];
    _subNameLabel.selectedPlaceHolderColor = [UITools defaultTintColor];
    
    [_rightBarButton setTitle:NSLocalizedString(@"Continue", nil)];
    
    _tableView.hidden = !_isMeeting;
    _tableView.tableFooterView = [UIView new];
    _tableView.backgroundColor = [UITools defaultBackgroundColor];
    
    _expandedIndexPaths = [NSMutableArray array];
    
    // Predefined values
    if( _initialRoomName ) {
        [_nameLabel setText:_initialRoomName];
    }
    if( _initialRoomTopic ) {
        [_subNameLabel setText:_initialRoomTopic];
    }
}

-(BOOL)shouldAutorotate {
    return NO;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self doneButtonState:[self validateUIContent]];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_nameLabel becomeFirstResponder];
}

-(void) setMeeting:(BOOL)meeting {
    _isMeeting = meeting;
}

-(void) setRoomName:(NSString *)roomName {
    _initialRoomName = roomName;
}

-(void) setRoomTopic:(NSString *)roomTopic {
    _initialRoomTopic = roomTopic;
}

-(void) setRoomParticipants:(NSArray<Contact *> *)roomParticipants {
    _initialParticipants = roomParticipants;
}

-(void) doneButtonState:(BOOL) enabled {
    _rightBarButton.enabled = enabled;
}

-(BOOL) validateUIContent {
    if(_room)
        return YES;
    BOOL isValid = NO;
    if( ![self roomNameMatchAnExistingRoom] && [_nameLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length >= 3)
        return YES;
    
    return isValid;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)textFieldDidChange:(UITextField *) textField {
    [self doneButtonState:[self validateUIContent]];
    
    if(textField == self.nameLabel){
        if(!_room){
            if([self roomNameMatchAnExistingRoom] || (textField.text.length < 3 && textField.text.length > 0) || (textField.text.length > 0 && [[textField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] length] < 3)) {
                textField.textColor = [UIColor redColor];
                textField.tintColor = [UIColor redColor];
            } else {
                textField.textColor = [UITools defaultTintColor];
                textField.tintColor = [UITools defaultTintColor];
            }
        }
    }
}

-(BOOL) roomNameMatchAnExistingRoom {
    NSArray<Room*> * rooms = [[ServicesManager sharedInstance].roomsService searchMyRoomMatchName:_nameLabel.text];
    if(rooms.count > 0)
        return YES;
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if([textField isEqual:_nameLabel]){
        [_subNameLabel becomeFirstResponder];
        _subNameLabel.returnKeyType = UIReturnKeyDone;
    }
    if([textField isEqual:_subNameLabel]){
        if(_nameLabel.text.length > 0)
            [self didTapOnRightBarButtonItem:nil];
        else {
            [_nameLabel becomeFirstResponder];
            _nameLabel.returnKeyType = UIReturnKeyNext;
        }
    }
    return YES;
}

- (IBAction)didTapOnCancelButton:(UIBarButtonItem *)sender {
    [_nameLabel resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didTapOnRightBarButtonItem:(UIBarButtonItem *)sender {
    UIGenericAddObjectWithTokenViewController* controller = (UIGenericAddObjectWithTokenViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"objectWithTokenViewController"];
    controller.mainLabel = _nameLabel.text;
    controller.subLabel = _subNameLabel.text;
    controller.isMeeting = _isMeeting;
    controller.initialParticipants = _initialParticipants;
    controller.room = _room;
    
    if(_isMeeting) {
        for (NSInteger j = 0; j < [_tableView numberOfSections]; ++j)
        {
            for (NSInteger i = 0; i < [_tableView numberOfRowsInSection:j]; ++i)
            {
                UIWithDatePickerTableViewCell *cell = (UIWithDatePickerTableViewCell*) [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:j]];
                if( i == 0 ) {
                    controller.meetingStartDate = cell.datePickerDate;
                }
                if( i == 1 ) {
                    controller.meetingEndDate = [NSDate dateWithTimeInterval:cell.datePickerDuration sinceDate:controller.meetingStartDate];
                }
            }
        }
        
        NSLog( @"%@, %@", controller.meetingStartDate, controller.meetingEndDate );
    }
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    switch (indexPath.row) {
        case 0: {
            UIWithDatePickerTableViewCell *cell = (UIWithDatePickerTableViewCell *) [_tableView dequeueReusableCellWithIdentifier:@"ScheduledMeetingDate"];
            
            if (cell == nil) {
                cell = [[UIWithDatePickerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ScheduledMeetingDate"];
            }
            cell.backgroundColor = [UITools defaultBackgroundColor];        
            [cell setDatePickerMode: UIDatePickerModeDateAndTime];

            cell.expanded = NO;
            if([_expandedIndexPaths indexOfObject:indexPath] != NSNotFound)
                cell.expanded = YES;

            return cell;
            break;
        }
        case 1: {
            UIWithDatePickerTableViewCell *cell = (UIWithDatePickerTableViewCell *) [_tableView dequeueReusableCellWithIdentifier:@"ScheduledMeetingDate"];
            
            if (cell == nil) {
                cell = [[UIWithDatePickerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ScheduledMeetingDate"];
            }
            cell.backgroundColor = [UITools defaultBackgroundColor];
            [cell setDatePickerMode: UIDatePickerModeCountDownTimer];
            
            cell.expanded = NO;
            if([_expandedIndexPaths indexOfObject:indexPath] != NSNotFound)
                cell.expanded = YES;
            
            return cell;
        }
            break;
        default:
            return nil;
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([_expandedIndexPaths indexOfObject:indexPath] != NSNotFound) {
        return 230;
    } else {
        return 44;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.view endEditing:YES];
    
    switch (indexPath.row) {
        case 0:
        case 1:{
            UIWithDatePickerTableViewCell *cell = (UIWithDatePickerTableViewCell*)[_tableView cellForRowAtIndexPath:indexPath];
            
            [_tableView beginUpdates];

            NSIndexPath *idx = [_expandedIndexPaths firstObject];
            if(idx != nil && ![idx isEqual:indexPath]) {
                UIWithDatePickerTableViewCell *prevCell = (UIWithDatePickerTableViewCell*)[_tableView cellForRowAtIndexPath:idx];
                prevCell.expanded =NO;
                [_expandedIndexPaths removeObject:idx];
            }
            
            if(!cell.expanded){
                cell.expanded = YES;
                [_expandedIndexPaths addObject:indexPath];
            } else {
                cell.expanded = NO;
                [_expandedIndexPaths removeObject:indexPath];
            }
            [_tableView endUpdates];
        }
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (_isScheduledMeeting) ? 2 : 1;
}

- (IBAction)didToggledMeetingType:(UISwitch *)sender {
    _isScheduledMeeting = sender.on;
    [_tableView reloadData];
    [self.view endEditing:YES];
}

@end
