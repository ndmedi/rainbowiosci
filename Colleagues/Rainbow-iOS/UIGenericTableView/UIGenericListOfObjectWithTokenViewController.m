/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIGenericListOfObjectWithTokenViewController.h"
#import "UITools.h"
#import "UIContactDetailsViewController.h"
#import <Rainbow/Contact.h>

@implementation UIGenericListOfObjectWithTokenViewController

-(void) awakeFromNib {
    [super awakeFromNib];
    _servicesManager = [ServicesManager sharedInstance];
    _objects = [NSMutableArray array];
    
    _selectedObjects = [NSMutableArray array];
    _filteredObjects = [NSMutableArray array];
    _searchedObjects = [NSMutableArray array];
    _searchOperationQueue = [NSOperationQueue new];
    _searchOperationQueue.maxConcurrentOperationCount = 1;
    
    _searchOnServer = YES;
}

-(void) dealloc {
    _servicesManager = nil;
    [_filteredObjects removeAllObjects];
    [_selectedObjects removeAllObjects];
    [_searchedObjects removeAllObjects];
    _filteredObjects = nil;
    _selectedObjects = nil;
    _searchedObjects = nil;
    [_searchOperationQueue cancelAllOperations];
    _searchOperationQueue = nil;
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

-(void) viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
    
    _tableView.emptyDataSetSource = self;
    _tableView.emptyDataSetDelegate = self;
    
    _tokenInputView.fieldName = NSLocalizedString(@"Add :", nil);
    _tokenInputView.placeholderText = NSLocalizedString(@"Enter a name", nil);
    _tokenInputView.tintColor = [UITools defaultTintColor];
    _tokenInputView.fieldColor = [UIColor darkGrayColor];
    _tokenInputView.drawBottomBorder = YES;
    _tokenInputView.backgroundColor = [UIColor whiteColor];
    _tokenInputView.keyboardType = UIKeyboardTypeDefault;
    _tokenInputView.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _tokenInputView.delegate = self;
    
    _tableView.tableFooterView = [UIView new];
    _tableView.backgroundColor = [UITools defaultBackgroundColor];
    self.tabBarController.tabBar.translucent = NO;
    _isTyping = NO;
    
    if(_object){
        _tokenInputWithCreateRoomViewTopConstraint.constant -= _createHeaderView.frame.size.height;
        _createHeaderView.hidden = YES;
    } else {
        _tokenInputWithCreateRoomViewTopConstraint.constant = 0;
        _createHeaderView.hidden = NO;
        
        [_nameTextField setPlaceholder:NSLocalizedString(@"Bubble name", nil)];
        [UITools applyCustomFontToTextField:_nameTextField];
        _nameTextField.lineColor = [UITools defaultTintColor];
        _nameTextField.selectedLineColor = [UITools defaultTintColor];
        _nameTextField.placeHolderColor = [UITools defaultTintColor];
        _nameTextField.selectedPlaceHolderColor = [UITools defaultTintColor];
        
        [_nameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        
        [_subNameTextField setPlaceholder:NSLocalizedString(@"Bubble topic (Optional)", nil)];
        [UITools applyCustomFontToTextField:_subNameTextField];
        _subNameTextField.lineColor = [UITools defaultTintColor];
        _subNameTextField.selectedLineColor = [UITools defaultTintColor];
        _subNameTextField.placeHolderColor = [UITools defaultTintColor];
        _subNameTextField.selectedPlaceHolderColor = [UITools defaultTintColor];
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self doneButtonState:[self validateUIContent]];
}

- (void)viewDidAppear:(BOOL)animated {
    if (!_tokenInputView.editing && _object) {
        [_tokenInputView beginEditing];
    }
    if(!_object)
        [_nameTextField becomeFirstResponder];
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - TextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    [self doneButtonState:[self validateUIContent]];
}

-(void) textFieldDidChange:(UITextField *) textField {
    [self doneButtonState:[self validateUIContent]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if([textField isEqual:_nameTextField])
        [_subNameTextField becomeFirstResponder];
    if([textField isEqual:_subNameTextField])
        if (!_tokenInputView.editing)
            [_tokenInputView beginEditing];
    return YES;
}

-(void) doneButtonState:(BOOL) enabled {
    _doneButton.enabled = enabled;
}

-(BOOL) validateUIContent {
    BOOL isValid = YES;
    if(!_object){
        if(_nameTextField.text.length == 0)
            return NO;
    }
    
    if(_tokenInputView.allTokens.count == 0)
        return NO;
    
    return isValid;
}


-(MBProgressHUD *) showHUDWithMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = message;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}

#pragma mark - DZNEmptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = [self textForEmptyView];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools boldFontName] size:18.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

#pragma mark - CLTokenInputViewDelegate
- (void)tokenInputView:(CLTokenInputView *)view didChangeText:(NSString *)text {
    [_filteredObjects removeAllObjects];
    if ([text isEqualToString:@""]){
        _isTyping = NO;
        _isSearchingOnServer = NO;
        [self.tableView reloadData];
    } else {
        _isTyping = YES;
        
        NSPredicate *predicate = [self predicateForText:text];
        [_filteredObjects addObjectsFromArray:[_objects filteredArrayUsingPredicate:predicate]];
        [_searchOperationQueue cancelAllOperations];
        
        _isSearchingOnServer = YES;
        _searchDone = NO;
        [self.tableView reloadData];
        if(_searchOnServer){
            [_searchOperationQueue addOperationWithBlock:^{
                [NSThread sleepForTimeInterval:0.2];
                [[ServicesManager sharedInstance].contactsManagerService searchRemoteContactsWithPattern:text withCompletionHandler:^(NSString *searchPattern, NSArray<Contact *> *foundContacts) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_searchedObjects removeAllObjects];
                        if(foundContacts.count > 0 && [searchPattern isEqualToString:_tokenInputView.text])
                            [_searchedObjects addObjectsFromArray:foundContacts];
                        
                        _searchDone = YES;
                        [self.tableView reloadData];
                    });
                }];
            }];
        }
    }
}

- (void)tokenInputView:(CLTokenInputView *)view didAddToken:(CLToken *)token {
    [_selectedObjects addObject:token.context];
    [self doneButtonState:[self validateUIContent]];
}

- (void)tokenInputView:(CLTokenInputView *)view didRemoveToken:(CLToken *)token {
    id anObject = token.context;
    [_selectedObjects removeObject:anObject];
    if(![_objects containsObject:anObject])
        [_objects addObject:anObject];
    [self sortUI];
    [self doneButtonState:[self validateUIContent]];
    [_tableView reloadData];
}

- (CLToken *)tokenInputView:(CLTokenInputView *)view tokenForText:(NSString *)text {
    // Return nil to avoid creating a token only with the entered text
    return nil;
}

- (void)tokenInputViewDidEndEditing:(CLTokenInputView *)view {
    [self doneButtonState:[self validateUIContent]];
}

- (void)tokenInputViewDidBeginEditing:(CLTokenInputView *)view {
    [self.view removeConstraint:_tableViewTopConstraint];
    _tableViewTopConstraint = [NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    [self.view addConstraint:_tableViewTopConstraint];
    [self.view layoutIfNeeded];
    [self doneButtonState:[self validateUIContent]];
}

- (BOOL)tokenInputViewShouldReturn:(CLTokenInputView *)view {
    return NO;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(_isSearchingOnServer && _searchOnServer)
        return 2;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        if(!_isTyping)
            return [_objects count];
        else
            return [_filteredObjects count];
    } else {
        return [_searchedObjects count];
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(_isSearchingOnServer && section == 1)
        return NSLocalizedString(@"More contacts", nil);
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert(YES, @"This method must be overriden");
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
    tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools boldFontName] size:16.0f];
    tableViewHeaderFooterView.textLabel.textColor = [UITools defaultTintColor];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kRainbowGenericTableViewCellHeight;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Done/Cancel actions
- (IBAction)cancelButtonClicked:(UIBarButtonItem *)sender {
    [_tokenInputView endEditing];
    if(!_objects){
        [_nameTextField resignFirstResponder];
        [_subNameTextField resignFirstResponder];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneButtonClicked:(UIBarButtonItem *)sender {
    [_tokenInputView endEditing];
    if(!_objects){
        [_nameTextField resignFirstResponder];
        [_subNameTextField resignFirstResponder];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(nullable id)sender {
    if([identifier isEqualToString:@"seguePushContactDetailController"]) {
        CGPoint point = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
        Contact *selectedContact = nil;
        if(indexPath.section == 0){
            if(self.isTyping)
                selectedContact = [_filteredObjects objectAtIndex:indexPath.row];
            else
                selectedContact = [_objects objectAtIndex:indexPath.row];
        } else {
            selectedContact = [_searchedObjects objectAtIndex:indexPath.row];
        }
        
        if (![selectedContact.companyId isEqualToString:[ServicesManager sharedInstance].myUser.contact.companyId] && !selectedContact.isInRoster)
            return NO;
        
        return YES;
    }
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"seguePushContactDetailController"]) {
        UIContactDetailsViewController *destViewController = [segue destinationViewController];
        CGPoint point = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
        Contact *selectedContact = nil;
        if(indexPath.section == 0){
            if(self.isTyping)
                selectedContact = [_filteredObjects objectAtIndex:indexPath.row];
            else
                selectedContact = [_objects objectAtIndex:indexPath.row];
        } else {
            selectedContact = [_searchedObjects objectAtIndex:indexPath.row];
        }
        destViewController.contact = selectedContact;
        destViewController.fromView = self;
    }
}

-(NSPredicate *) predicateForText:(NSString *) text {
    NSAssert(NO, @"This method must be overwritten");
    return nil;
}

-(NSString *) textForEmptyView {
    return (!_searchDone && _searchOnServer)?NSLocalizedString(@"Searching ...",nil):NSLocalizedString(@"No contact found",nil);
}

-(void) sortUI {
    NSAssert(NO, @"This method must be overwritten");
}

@end
