/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "CLTokenInputView.h"
#import <Rainbow/ServicesManager.h>
#import "MBProgressHUD.h"
#import "UIRainbowGenericTableViewCell.h"
#import "UIPreviewingRoomAvatarView.h"

@interface UIGenericAddObjectWithTokenViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, CLTokenInputViewDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@property (nonatomic, strong) ServicesManager *servicesManager;

@property (nonatomic, strong) id object;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIPreviewingRoomAvatarView *previewingAvatarView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet CLTokenInputView *tokenInputView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tokenInputViewTopConstraint;

@property (nonatomic) BOOL isTyping;
@property (nonatomic) BOOL isSearchingOnServer;
@property (nonatomic) BOOL searchDone;

@property (nonatomic, strong) NSOperationQueue *searchOperationQueue;
@property (nonatomic) BOOL searchOnServer;

@property (nonatomic, strong) NSMutableArray<id> *objects;
@property (nonatomic, strong) NSMutableArray<id> *filteredObjects;
@property (nonatomic, strong) NSMutableArray<id> *selectedObjects;
@property (nonatomic, strong) NSMutableArray<id> *searchedObjects;

-(MBProgressHUD *) showHUDWithMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion;

@property (nonatomic, strong) Room *room;
@property (nonatomic, strong) NSString *mainLabel;
@property (nonatomic, strong) NSString *subLabel;
@property (nonatomic) BOOL isMeeting;
@property (nonatomic, strong) NSDate *meetingStartDate;
@property (nonatomic, strong) NSDate *meetingEndDate;

@property (nonatomic, strong) NSArray<Contact*> *initialParticipants;

@end
