/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "CLTokenInputView.h"
#import <Rainbow/ServicesManager.h>
#import "MBProgressHUD.h"
#import "UIRainbowGenericTableViewCell.h"

@interface UIGenericListOfObjectWithTokenViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CLTokenInputViewDelegate, UITextFieldDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@property (nonatomic, strong) ServicesManager *servicesManager;

@property (nonatomic, strong) id object;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet CLTokenInputView *tokenInputView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tokenInputWithCreateRoomViewTopConstraint;
@property (nonatomic) BOOL isTyping;
@property (nonatomic) BOOL isSearchingOnServer;
@property (nonatomic) BOOL searchDone;

@property (weak, nonatomic) IBOutlet UIView *createHeaderView;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *nameTextField;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *subNameTextField;
@property (nonatomic, strong) NSOperationQueue *searchOperationQueue;
@property (nonatomic) BOOL searchOnServer;

@property (nonatomic, strong) NSMutableArray<id> *objects;
@property (nonatomic, strong) NSMutableArray<id> *filteredObjects;
@property (nonatomic, strong) NSMutableArray<id> *selectedObjects;
@property (nonatomic, strong) NSMutableArray<id> *searchedObjects;

-(MBProgressHUD *) showHUDWithMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion;

-(IBAction)doneButtonClicked:(UIBarButtonItem *)sender;
-(BOOL) validateUIContent;
-(void) textFieldDidChange:(UITextField *) textField;
@end
