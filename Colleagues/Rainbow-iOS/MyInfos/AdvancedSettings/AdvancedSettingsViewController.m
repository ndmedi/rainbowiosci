/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "AdvancedSettingsViewController.h"
#import "OrderedOptionalSectionedContent.h"
#import "UITools.h"
#import "AdvancedSettingsSwitchTableViewCell.h"

@interface AdvancedSettingsViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property  (nonatomic, strong) OrderedOptionalSectionedContent *content;

@end

#define kSectionDisableTurns @"Use turns (ON = SSL)"
#define kSectionDumpContact @"Dump Contacts memory"
#define kSectionDumpConversation @"Dump conversations memory"
#define kSectionDumpBubbles @"Dump bubbles memory"
#define kSectionDumpMeetings @"Dump meetings memory"

@implementation AdvancedSettingsViewController

-(void) awakeFromNib {
    [super awakeFromNib];
    _content = [[OrderedOptionalSectionedContent alloc] initWithSections:@[kSectionDisableTurns, kSectionDumpContact, kSectionDumpConversation, kSectionDumpBubbles, kSectionDumpMeetings]];
    [_content addObject:[NSObject new] toSection:kSectionDisableTurns];
    [_content addObject:[NSObject new] toSection:kSectionDumpContact];
    [_content addObject:[NSObject new] toSection:kSectionDumpConversation];
    [_content addObject:[NSObject new] toSection:kSectionDumpBubbles];
    [_content addObject:[NSObject new] toSection:kSectionDumpMeetings];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_content allNotEmptySections].count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_content allNotEmptySections] objectAtIndex:indexPath.section];
    if([key isEqualToString:kSectionDisableTurns]){
        AdvancedSettingsSwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"advancedSettingsSwitchCell"];
        cell.title.text = key;
        NSNumber *value = [[NSUserDefaults standardUserDefaults] objectForKey:@"advancedSettingsDisableTurns"];
        cell.switchOnOff.on = [value boolValue];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"advancedSettingsCell"];
    cell.textLabel.text = key;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *key = [[_content allNotEmptySections] objectAtIndex:indexPath.section];
    if([key isEqualToString:kSectionDumpConversation]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dumpConversations" object:nil];
    }
    if([key isEqualToString:kSectionDumpContact]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dumpContacts" object:nil];
    }
    if([key isEqualToString:kSectionDumpBubbles]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dumpBubbles" object:nil];
    }
    if([key isEqualToString:kSectionDumpMeetings]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dumpMeetings" object:nil];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

@end
