/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "AdvancedSettingsSwitchTableViewCell.h"

@interface AdvancedSettingsSwitchTableViewCell ()

@end

@implementation AdvancedSettingsSwitchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)switchChanged:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:sender.on] forKey:@"advancedSettingsDisableTurns"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didChangeAdvancedSettings" object:nil];
}

@end
