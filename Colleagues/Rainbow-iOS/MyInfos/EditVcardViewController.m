/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "EditVcardViewController.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/ContactsManagerService.h>
#import "UITools.h"
#import "LargePhotoCell.h"
#import "MBProgressHUD.h"
#import "UIImageView+Letters.h"
#import "Contact+Extensions.h"
#import <Rainbow/OrderedDictionary.h>


@interface NSString (SpaceReplace)
-(NSString *) replaceSpaceWithNonBrSpace;
-(NSString *) replaceNonBrSpaceWithSpace;
@end

@implementation NSString (SpaceReplace)
-(NSString *) replaceSpaceWithNonBrSpace {
    return [self stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
}
-(NSString *) replaceNonBrSpaceWithSpace {
    return [self stringByReplacingOccurrencesOfString:@"\u00a0" withString:@" "];
}
@end


#define kTextFieldMaxLength 40

@interface EditVcardViewController ()

@property (nonatomic, strong) XLFormRowDescriptor * photo;
@property (nonatomic, strong) XLFormRowDescriptor * lastname;
@property (nonatomic, strong) XLFormRowDescriptor * firstname;
@property (nonatomic, strong) XLFormRowDescriptor * nickname;
@property (nonatomic, strong) XLFormRowDescriptor * jobrole;
@property (nonatomic, strong) XLFormRowDescriptor * usertitle;
@property (nonatomic, strong) XLFormRowDescriptor * professional_landphone;
@property (nonatomic, strong) XLFormRowDescriptor * professional_mobilephone;
@property (nonatomic, strong) XLFormRowDescriptor * personal_landphone;
@property (nonatomic, strong) XLFormRowDescriptor * personal_mobilephone;
@property (nonatomic, strong) XLFormRowDescriptor * personal_email;
@property (nonatomic, strong) XLFormRowDescriptor * country;
@property (nonatomic, strong) ContactsManagerService *contactsManager;
@property (nonatomic, strong) OrderedDictionary *countryList;
@end


@implementation EditVcardViewController

// Helper function to create a XLForm row
- (XLFormRowDescriptor*) createAndAddRowType:(NSString*)type withTitle:(NSString*)title toSection:(XLFormSectionDescriptor*)section {
    return [self createAndAddRowType:type withTitle:title toSection:section withDefaultValue:nil];
}

// Helper function to create a XLForm row with a default value
- (XLFormRowDescriptor*) createAndAddRowType:(NSString*)type withTitle:(NSString*)title toSection:(XLFormSectionDescriptor*)section withDefaultValue:(id)value {
    
    XLFormRowDescriptor *row = [XLFormRowDescriptor formRowDescriptorWithTag:title rowType:type title:NSLocalizedString(title, nil)];
    if (value) {
        if ([value isKindOfClass:[NSString class]]) {
            // Replace <spaces> by <non-breaking spaces>
            // This fix an issue because we have right aligned textfields and spaces are not
            // displayed if they are the last character of the string
            row.value = [value replaceSpaceWithNonBrSpace];
        } else {
            row.value = value;
        }
    }
    
    // Apply generic customization.
    [row.cellConfig setObject:[UIFont fontWithName:[UITools defaultFontName] size:15] forKey:@"textLabel.font"];
    [row.cellConfig setObject:[UIColor lightGrayColor] forKey:@"textLabel.textColor"];
    [row.cellConfig setObject:[UITools defaultTintColor] forKey:@"self.tintColor"];
    
    if ([[XLFormViewController cellClassesForRowDescriptorTypes] objectForKey:type] == [XLFormTextFieldCell class]) {
        [row.cellConfig setObject:[UIFont fontWithName:[UITools defaultFontName] size:15] forKey:@"textField.font"];
        [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
        [row.cellConfig setObject:[NSNumber numberWithFloat:0.4] forKey:XLFormTextFieldLengthPercentage];
    }
    
    if([type isEqualToString:XLFormRowDescriptorTypeSelectorPickerView]){
        [row.cellConfig setObject:[UIFont fontWithName:[UITools defaultFontName] size:15] forKey:@"detailTextLabel.font"];
        [row.cellConfig setObject:[UIColor blackColor] forKey:@"detailTextLabel.textColor"];
        [row.cellConfig setObject:[UITools defaultTintColor] forKey:@"self.tintColor"];
        
        NSMutableArray * selectorOptions = [NSMutableArray array];
        __block XLFormOptionsObject *defaultCountryValue = nil;
        [_countryList enumerateKeysAndObjectsWithIndexUsingBlock:^(NSString  *countryCode, NSString *countryName, NSUInteger idx, BOOL * stop) {
            XLFormOptionsObject *object = [XLFormOptionsObject formOptionsObjectWithValue:countryCode displayText:countryName];
            if([_contact.countryCode isEqualToString:countryCode])
                defaultCountryValue = object;
            
            [selectorOptions addObject:object];
        }];
        row.selectorOptions = selectorOptions;
        
        row.value = defaultCountryValue;
    }

    [section addFormRow:row];
    
    return row;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        if (!_contact) {
            _contact = [ServicesManager sharedInstance].myUser.contact;
        }
        
        NSString *countryListFilePath = [[NSBundle bundleForClass:[self class]] pathForResource: @"country_list" ofType: @"plist"];
        _countryList = [OrderedDictionary dictionaryWithContentsOfFile:countryListFilePath];
        
        self.tableView.delaysContentTouches = YES;
        
        // Add our Custom LargePhotoCell Row to XLForm
        [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[LargePhotoCell class] forKey:kLargePhotoCellType];
        
        XLFormDescriptor * form = [XLFormDescriptor formDescriptorWithTitle:NSLocalizedString(@"Update My Info", nil)];
        form.endEditingTableViewOnScroll = YES;
        //form.assignFirstResponderOnShow = YES;
        
        // Create the Sections
        XLFormSectionDescriptor *sectionPhoto = [XLFormSectionDescriptor formSection];
        sectionPhoto.title = NSLocalizedString(@"Picture", nil);
        [form addFormSection:sectionPhoto];
        
        XLFormSectionDescriptor *sectionNames = [XLFormSectionDescriptor formSection];
        sectionNames.title = NSLocalizedString(@"User", nil);
        [form addFormSection:sectionNames];
        
        XLFormSectionDescriptor *sectionNumbers = [XLFormSectionDescriptor formSection];
        sectionNumbers.title = NSLocalizedString(@"Phone numbers", nil);
        [form addFormSection:sectionNumbers];
        
        
        
        // Photo
        if(_contact.photoData)
            _photo = [self createAndAddRowType:kLargePhotoCellType withTitle:@"Avatar" toSection:sectionPhoto withDefaultValue:[UIImage imageWithData:_contact.photoData]];
        else {
            UIImageView *temp = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            [temp setImageWithString:_contact.displayName color:[UITools colorForString:_contact.displayName] circular:YES fontName:[UITools defaultFontName]];
            temp.contentMode = UIViewContentModeScaleAspectFit;
            
          _photo = [self createAndAddRowType:kLargePhotoCellType withTitle:@"Avatar" toSection:sectionPhoto withDefaultValue:temp.image];
        }
        
        
        // Lastname
        _lastname = [self createAndAddRowType:XLFormRowDescriptorTypeText withTitle:NSLocalizedString(@"Lastname",nil) toSection:sectionNames withDefaultValue:_contact.lastName];
        _lastname.required = YES;
        
        // Firstname
        _firstname = [self createAndAddRowType:XLFormRowDescriptorTypeText withTitle:NSLocalizedString(@"Firstname",nil) toSection:sectionNames withDefaultValue:_contact.firstName];
        _firstname.required = YES;
        
        // Nickname
        _nickname = [self createAndAddRowType:XLFormRowDescriptorTypeText withTitle:NSLocalizedString(@"Nickname",nil) toSection:sectionNames withDefaultValue:_contact.nickName];
        
        // Job Title
        _jobrole = [self createAndAddRowType:XLFormRowDescriptorTypeText withTitle:NSLocalizedString(@"Job Title",nil) toSection:sectionNames withDefaultValue:_contact.jobTitle];
        
        // User Title (Dr, PhD, Me, ..)
        _usertitle = [self createAndAddRowType:XLFormRowDescriptorTypeText withTitle:NSLocalizedString(@"Title",nil) toSection:sectionNames withDefaultValue:_contact.title];
        
        // Personal Email
        _personal_email = [self createAndAddRowType:XLFormRowDescriptorTypeEmail withTitle:NSLocalizedString(@"Personal Email",nil) toSection:sectionNames
                                   withDefaultValue:[_contact getEmailAdressOfType:EmailAddressTypeHome].address];
        
        
        // Professional Phone Number
        PhoneNumber *workPhoneNumber = [_contact getPhoneNumberOfType:PhoneNumberTypeWork withDeviceType:PhoneNumberDeviceTypeLandline];
        _professional_landphone = [self createAndAddRowType:XLFormRowDescriptorTypePhone withTitle:NSLocalizedString(@"Professional",nil) toSection:sectionNumbers
                                           withDefaultValue:workPhoneNumber.number];
        if(workPhoneNumber.isMonitored)
            _professional_landphone.disabled = @YES;
        
        // Professional Cell Number
        _professional_mobilephone = [self createAndAddRowType:XLFormRowDescriptorTypePhone withTitle:NSLocalizedString(@"Professional Mobile",nil) toSection:sectionNumbers withDefaultValue:[_contact getPhoneNumberOfType:PhoneNumberTypeWork withDeviceType:PhoneNumberDeviceTypeMobile].number];
        
        // Personal Phone Number
        _personal_landphone = [self createAndAddRowType:XLFormRowDescriptorTypePhone withTitle:NSLocalizedString(@"Personal",nil) toSection:sectionNumbers
                                       withDefaultValue:[_contact getPhoneNumberOfType:PhoneNumberTypeHome withDeviceType:PhoneNumberDeviceTypeLandline].number];
        
        // Personal Cell Number
        _personal_mobilephone = [self createAndAddRowType:XLFormRowDescriptorTypePhone withTitle:NSLocalizedString(@"Personal Mobile",nil) toSection:sectionNumbers withDefaultValue:[_contact getPhoneNumberOfType:PhoneNumberTypeHome withDeviceType:PhoneNumberDeviceTypeMobile].number];
        
        _country = [self createAndAddRowType:XLFormRowDescriptorTypeSelectorPickerView withTitle:NSLocalizedString(@"Country", nil) toSection:sectionNames];
        _country.required = YES;
        
        self.form = form;
        
        _contactsManager = [ServicesManager sharedInstance].contactsManagerService;
        
    }
    return self;
}

-(void) dealloc {
    _photo = nil;
    _lastname = nil;
    _firstname = nil;
    _nickname = nil;
    _jobrole = nil;
    _usertitle = nil;
    _professional_landphone = nil;
    _professional_mobilephone = nil;
    _personal_landphone = nil;
    _personal_mobilephone = nil;
    _personal_email = nil;
    _country = nil;
    _contactsManager = nil;
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Rotation
-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (IBAction)editVcardViewControllerDidSave:(id)sender {
    NSArray * validationErrors = [self formValidationErrors];
    if (validationErrors.count > 0){
        [self showFormValidationError:[validationErrors firstObject]];
        return;
    }
    [self.tableView endEditing:YES];
    
    // Re-Replace <non-breaking spaces> by <spaces> because we don't send nonbr space to server
    // This fix an issue because we have right aligned textfields and spaces are not
    // displayed if they are the last character of the string
    
    NSDictionary *fields = @{
                             @"photo": _photo.value ? _photo.value : @"",
                             @"lastname": _lastname.value ? [_lastname.value replaceNonBrSpaceWithSpace] : @"",
                             @"firstname": _firstname.value ? [_firstname.value replaceNonBrSpaceWithSpace] : @"",
                             @"nickname": _nickname.value ? [_nickname.value replaceNonBrSpaceWithSpace] : @"",
                             @"jobrole": _jobrole.value ? [_jobrole.value replaceNonBrSpaceWithSpace] : @"",
                             @"title": _usertitle.value ? [_usertitle.value replaceNonBrSpaceWithSpace] : @"",
                             @"personal_email": _personal_email.value ? [_personal_email.value replaceNonBrSpaceWithSpace] : @"",
                             @"professional_landphone": _professional_landphone.value && !_professional_landphone.isDisabled ? [UITools trimAndRemoveUnicodeFormatFromNumber:[_professional_landphone.value replaceNonBrSpaceWithSpace]] : @"",
                             @"professional_mobilephone": _professional_mobilephone.value ? [UITools trimAndRemoveUnicodeFormatFromNumber:[_professional_mobilephone.value replaceNonBrSpaceWithSpace]] : @"",
                             @"personal_landphone": _personal_landphone.value ? [UITools trimAndRemoveUnicodeFormatFromNumber:[_personal_landphone.value replaceNonBrSpaceWithSpace]] : @"",
                             @"personal_mobilephone": _personal_mobilephone.value ? [UITools trimAndRemoveUnicodeFormatFromNumber:[_personal_mobilephone.value replaceNonBrSpaceWithSpace]] : @"",
                             @"country" : _country.value ? ((XLFormOptionsObject *)_country.value).formValue : @""
                            };
    
    // display a loading spinner
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Saving", nil);
    hud.removeFromSuperViewOnHide = YES;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    
    // Don't do the update on main thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [_contactsManager updateUserWithFields:fields withCompletionBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error) {
                    // Hide our loading spinner
                    [hud hide:YES];
                    NSLog(@"An error while updating the vcard %@", error);
                    // Show the error message
                    NSString* message = NSLocalizedString(error.localizedDescription, nil);
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error while updating user info", nil) message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                    [alert show];
                } else {
                    // If everything is OK,
                    // Display the checkmark, wait 1 sec and go back to MyInfos
                    hud.mode = MBProgressHUDModeCustomView;
                    hud.labelText = NSLocalizedString(@"Saved", nil);
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
            });
        }];
    });
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    BOOL shouldChange = newLength <= kTextFieldMaxLength || returnKey;
    
    if (shouldChange) {
        // Change myself and replace <spaces> by <non-breaking spaces>
        // This fix an issue because we have right aligned textfields and spaces are not
        // displayed if they are the last character of the string
        // http://stackoverflow.com/a/22211018
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        textField.text = [textField.text replaceSpaceWithNonBrSpace];
    }
    
    return NO;
}

@end
