/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "MyInfosDebugViewController.h"
#import <Rainbow/Tools.h>
#import "UITools.h"
#import <Rainbow/LogsRecorder.h>
#import <MessageUI/MessageUI.h>
#import <Rainbow/ServicesManager.h>
#import "UIDevice-Hardware.h"
#import <Rainbow/UIDevice+VersionCheck.h>
#import "MFMailComposeViewController+StatusBarStyle.h"
#import "debuggerCheck.h"
#import "MBProgressHUD.h"
#import "UILabel+Clickable.h"

@interface MyInfosDebugViewController () <MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIButton *sendLogsByMailButton;
@property (weak, nonatomic) IBOutlet UIButton *showLegalsInfo;
@property (nonatomic, strong) NSURL *applicationLogUrl;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (weak, nonatomic) IBOutlet UITextView *copyrightTextView;
@property (weak, nonatomic) IBOutlet UILabel *tosDataPrivacyLabel;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@end

@implementation MyInfosDebugViewController

#pragma mark - Rotation
-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}


-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"About", nil), [Tools applicationName]];
    [UITools applyCustomFontTo:_versionLabel];
    [UITools applyCustomFontTo:_sendLogsByMailButton.titleLabel];
    [_copyrightTextView setFont:[UIFont fontWithName:[UITools defaultFontName] size:_copyrightTextView.font.pointSize]];
    [UITools applyCustomFontTo:_tosDataPrivacyLabel];
    
    _versionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Version %@", nil), [Tools applicationVersion]];
    [_sendLogsByMailButton setTitle:NSLocalizedString(@"Send logs by email", nil) forState:UIControlStateNormal];
    [_showLegalsInfo setTintColor:[UITools defaultTintColor]];
    [_sendLogsByMailButton setTintColor:[UITools defaultTintColor]];
    
    NSString *tosText = NSLocalizedString(@"Terms of service and Privacy statement", nil);
    
    NSMutableAttributedString *tosTextAttributed = [[NSMutableAttributedString alloc] initWithString:tosText];
    
    NSString *tosLinkText = NSLocalizedString(@"Terms of service", nil);
    NSString *privacyLinkText = NSLocalizedString(@"Privacy statement", nil);
    
    NSRange tosLinkRange = [tosText rangeOfString:tosLinkText options:NSNumericSearch];
    NSRange privacyLinkRange = [tosText rangeOfString:privacyLinkText options:NSNumericSearch];
    
    NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionaryWithDictionary:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:12] ,  NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle), NSLinkAttributeName: [NSURL URLWithString:@"https://www.openrainbow.com/terms-service/"], NSForegroundColorAttributeName: [UITools defaultTintColor]}];
    
    [tosTextAttributed setAttributes:linkAttributes range:tosLinkRange];
    
    [linkAttributes setObject:[NSURL URLWithString:@"https://www.openrainbow.com/dataprivacy"] forKey:NSLinkAttributeName];
    
    [tosTextAttributed setAttributes:linkAttributes range:privacyLinkRange];
    
    _tosDataPrivacyLabel.attributedText = tosTextAttributed;
    _tosDataPrivacyLabel.userInteractionEnabled = YES;
    
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnTosLabel:)];
    [_tosDataPrivacyLabel addGestureRecognizer:_tapGesture];
    
    _copyrightTextView.text = NSLocalizedString(@"Copyright ALE International. All rights reserved. ALE International and the Alcatel-Lucent Enterprise logo are registered trademarks and service marks of ALE International.\n This application is protected by copyright law and international treaties. Unauthorized reproduction or distribution of this program, or any part of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum extent possible under the law.", nil);
}

- (void)handleTapOnTosLabel:(UITapGestureRecognizer *)tapGesture {
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    [_tosDataPrivacyLabel openTappedLinkAtLocation:locationOfTouchInLabel];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_tosDataPrivacyLabel removeGestureRecognizer:_tapGesture];
    _tapGesture = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sendLogsByMailButtonClicked:(UIButton *)sender {
    if(!_hud)
        _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    _hud.mode = MBProgressHUDModeIndeterminate;
    [_hud showAnimated:YES whileExecutingBlock:^{
        [self sendLogsByMail];
    }];
    
}
-(void) sendLogsByMail {
    // If no configure mail account
    if (![MFMailComposeViewController canSendMail]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Could not send mail", nil) message:NSLocalizedString(@"No configured mail account", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alertView show];
        });
        return;
    }
    // Stop record before zipping to got the complete file
    [[LogsRecorder sharedInstance] stopRecord];
    _applicationLogUrl = [[LogsRecorder sharedInstance] zippedApplicationLogs];
    if (!_applicationLogUrl) {
        [[LogsRecorder sharedInstance] startRecord];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Could not send mail", nil) message:NSLocalizedString(@"No logs file found.", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alertView show];
        });
        return;
    }
    // restat record after zipping
    if(AmIBeingDebugged() == FALSE)
        [[LogsRecorder sharedInstance] startRecord];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd_HH:mm:ss"];
    NSString* date = [dateFormatter stringFromDate:[NSDate date]];
    
    NSString* bodyContent = [NSString stringWithFormat:@"\n\n\n%@\n------\nProduct : %@\nVersion: %@\n Date: %@\niOS version : %@\niPhone model : %@", NSLocalizedString(@"Do not modify anythings below this line", nil), [Tools applicationName], [Tools applicationVersion], date, [UIDevice currentDevice].systemVersion, [UIDevice currentDevice].modelName];
    NSString* attachedFileName = [NSString stringWithFormat:@"%@-%@-logs.txt.zip", [Tools applicationName], [Tools applicationVersion]];
    NSData* attachmentData = [NSData dataWithContentsOfFile:_applicationLogUrl.path];
    
    // Compute username
    NSString* userName = [ServicesManager sharedInstance].myUser.username;

    // Send logs
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setModalPresentationStyle:UIModalPresentationFormSheet];
    [mailComposer setSubject:[NSString stringWithFormat:@"%@ %@ %@ application logs for user %@",[Tools applicationName], [Tools applicationVersion], [Tools currentOS], userName]];
    [mailComposer setMessageBody:bodyContent isHTML:NO];
    [mailComposer addAttachmentData:attachmentData mimeType:@"application/x-gzip" fileName:attachedFileName];
    mailComposer.navigationBar.tintColor = [UIColor whiteColor];
    mailComposer.navigationBar.translucent = NO;
    [self presentViewController:mailComposer animated:YES completion:^{
        [_hud hide:YES];
    }];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if(_applicationLogUrl){
        [[NSFileManager defaultManager] removeItemAtURL:_applicationLogUrl error:nil];
        _applicationLogUrl = nil;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
