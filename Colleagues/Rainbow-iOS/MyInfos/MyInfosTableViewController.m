/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIAvatarView.h"
#import "UITools.h"
#import "MyInfosTableViewController.h"
#import "MyInfosDebugViewController.h"
#import "MBProgressHUD.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/ContactsManagerService.h>
#import <Rainbow/Tools.h>
#import "EditVcardViewController.h"
#import "Contact+Extensions.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "MyPresenceCell.h"
#import "UIScrollView+APParallaxHeader.h"
#import "UIContactDetailsViewController.h"
#import "UINotificationManager.h"
#import "GoogleAnalytics.h"
#import "UIMyFilesOnServerTableViewController.h"
#import "ChangePasswordViewController.h"
#import <Rainbow/OrderedDictionary.h>
#import "UIStoryboardManager.h"
#import "MyInfoNavigationItem.h"
#import "TelephonyOptionsViewController.h"

#define kMyPresenceCellID @"MyPresenceCellID"
#define kMyInfosCellID @"MyInfosCellID"
#define kMyOthersSettingsCellID @"MyOthersSettingsCellID"

@interface MyInfosTableViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>
@property (nonatomic, strong) Contact *contact;
@property (nonatomic, strong) LoginManager *loginManager;
@property (nonatomic, strong) ContactsManagerService *contactsManager;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIAvatarView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;
@property (weak, nonatomic) IBOutlet UILabel *intergrationLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *textualPresence;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) NSArray *supportedPresence;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButtonItem;
@property (nonatomic) BOOL interactionMustBeBlocked;
@property (nonatomic, strong) Contact *emilyBotContact;
@property (nonatomic, strong) OrderedDictionary *countryList;
@property (weak, nonatomic) IBOutlet UIImageView *calendarPresenceImage;
@property (weak, nonatomic) IBOutlet UILabel *calendarPresenceLabel;

@property (nonatomic) NSInteger sectionIndexMyFilesOnServer;
@property (nonatomic) NSInteger sectionIndexTelephonySettings;
@property (nonatomic) NSInteger sectionIndexMyOthersSettings;
@end

@implementation MyInfosTableViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateMyContact:) name:kContactsManagerServiceDidUpdateMyContact object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContactDisplaySettings:) name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddCall:) name:kTelephonyServiceDidAddCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateCall:) name:kTelephonyServiceDidUpdateCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveCall:) name:kTelephonyServiceDidRemoveCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTelephonyStatusUpdate:) name:kMyUserNomadicStatusDidUpdate object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTelephonyStatusUpdate:) name:kMyUserCallForwardDidUpdate object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTelephonyStatusUpdate:) name:kMyUserFeatureDidUpdate object:nil];

        _supportedPresence = @[[Presence presenceAvailable], [Presence presenceAway], [Presence presenceExtendedAway], [Presence presenceDoNotDisturb]];
        _interactionMustBeBlocked = NO;
        
        NSString *countryListFilePath = [[NSBundle bundleForClass:[self class]] pathForResource: @"country_list" ofType: @"plist"];
        _countryList = [OrderedDictionary dictionaryWithContentsOfFile:countryListFilePath];
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidAddCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidUpdateCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidRemoveCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserNomadicStatusDidUpdate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserCallForwardDidUpdate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserFeatureDidUpdate object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if(!_contact)
        [self setContact:[ServicesManager sharedInstance].myUser.contact];
    if(!_loginManager)
        _loginManager = [ServicesManager sharedInstance].loginManager;
    if(!_contactsManager)
        _contactsManager = [ServicesManager sharedInstance].contactsManagerService;

    _avatarView.asCircle = YES;
    _avatarView.showPresence = YES;
    _avatarView.withBorder = YES;
    _headerView.backgroundColor = [UITools defaultTintColor];
    _nameLabel.textColor = [UIColor whiteColor];
    _subLabel.textColor = [UIColor whiteColor];
    _companyName.textColor = [UIColor whiteColor];
    _textualPresence.textColor = [UIColor whiteColor];
    _calendarPresenceLabel.textColor = [UIColor whiteColor];
    
    [UITools applyCustomBoldFontTo:_nameLabel];
    [UITools applyCustomFontTo:_subLabel];
    [UITools applyCustomFontTo:_companyName];
    [UITools applyCustomFontTo:_textualPresence];
    [UITools applyCustomFontTo:_intergrationLabel];
    [UITools applyCustomFontTo:_calendarPresenceLabel];
    
    _calendarPresenceImage.tintColor = [UIColor whiteColor];
    
    self.title = @"";
    
    self.navigationController.navigationBar.translucent = NO;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    [self.tableView setBackgroundColor:[UITools defaultBackgroundColor]];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.sectionHeaderHeight = 0.0f;
    self.tableView.sectionFooterHeight = 0.0f;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.tableHeaderView = nil;
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];
    
    CGFloat height = _headerView.frame.size.height;
    [self.tableView addParallaxWithView:_headerView andHeight:height andMinHeight:height - 10 andShadow:YES];
    
    // Hide seperator between nav bar and header view
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar setBackgroundImage:[UIImage new] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [navigationBar setShadowImage:[UIImage new]];
    
    _interactionMustBeBlocked = [ServicesManager sharedInstance].rtcService.hasActiveCalls;
    
    // Add Telephony item if user has at least one of this feature activated
    [self setTelephonyItemVisible:([ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyNomadicMode || [ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyCallForward)];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _avatarView.peer = _contact;
    [[ServicesManager sharedInstance].contactsManagerService fetchCalendarAutomaticReply:_contact];
    [self populateUI];
    if(![ServicesManager sharedInstance].myUser.server.defaultServer){
        _intergrationLabel.text = [NSString stringWithFormat:@"Domain : %@", [ServicesManager sharedInstance].myUser.server.serverDisplayedName];
        _intergrationLabel.hidden = NO;
    } else {
        _intergrationLabel.hidden = YES;
    }
    [self.tableView reloadData];
    if(_loginManager.isConnected)
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    else
        [self didLostConnection:nil];
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"my infos" action:@"show" label:nil value:nil];
    _emilyBotContact = [_contactsManager getSupportBot];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _intergrationLabel.hidden = YES;
    if(!_loginManager.isConnected)
        [self didReconnect:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void) setTelephonyItemVisible:(BOOL) visible {
    // Update indexes of sections
    if(visible) {
        _sectionIndexTelephonySettings = 2;
        _sectionIndexMyFilesOnServer = 3;
        _sectionIndexMyOthersSettings = 4;
    } else {
        _sectionIndexTelephonySettings = 99; // not active
        _sectionIndexMyFilesOnServer = 2;
        _sectionIndexMyOthersSettings = 3;
    }
    
    if([self isViewLoaded]){
        [self.tableView reloadData];
    }
}

#pragma mark - Notifications
-(void) didTelephonyStatusUpdate:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didTelephonyStatusUpdate:notification];
        });
        return;
    }
    
    [self setTelephonyItemVisible:([ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyNomadicMode || [ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyCallForward)];
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        NSLog(@"Did lost network in MyInfoTab");
        [self.tableView reloadData];
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        NSLog(@"Did reconnect in MyInfoTab");
        [self.tableView reloadData];
    }
}

-(void) changeContactDisplaySettings:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self changeContactDisplaySettings:notification];
        });
        return;
    }
    if([self isViewLoaded]){
        NSLog(@"%@ Reload UI on contact display settings changed", self);
        [self.tableView reloadData];
    }
}

-(void) didUpdateMyContact:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateMyContact:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Contact *contact = [userInfo objectForKey:kContactKey];
    if([contact isEqual:[ServicesManager sharedInstance].myUser.contact]){
        [self setContact:[ServicesManager sharedInstance].myUser.contact];
        if([self isViewLoaded]){
            [self.tableView reloadData];
        }
    }
}

-(void) didAddCall:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddCall:notification];
        });
        return;
    }
        
    _interactionMustBeBlocked = YES;
    if([self isViewLoaded]){
        [self.tableView reloadData];
    }
}

-(void) didUpdateCall:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateCall:notification];
        });
        return;
    }
    
    if(!_interactionMustBeBlocked){
        _interactionMustBeBlocked = YES;
        if([self isViewLoaded]){
            [self.tableView reloadData];
        }
    }
}

-(void) didRemoveCall:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveCall:notification];
        });
        return;
    }
    
    _interactionMustBeBlocked = NO;
    if([self isViewLoaded]){
        [self.tableView reloadData];
    }
}

-(void) populateUI {
    if(_contact){
        NSMutableString *nameLabel = [NSMutableString string];
        if(_contact.title.length > 0)
            [nameLabel appendFormat:@"%@ ",_contact.title];
        
        [nameLabel appendString:_contact.displayName];
        _nameLabel.text = nameLabel;
        _subLabel.text = _contact.jobTitle;
        _companyName.text = _contact.companyName;
        _textualPresence.text = [UITools getPresenceText:_contact];
        
        if(_contact.calendarPresence.automaticReply.isEnabled){
            _calendarPresenceImage.image = [UIImage imageNamed:@"outOfOffice"];
            if( _contact.calendarPresence.automaticReply.untilDate != nil ) {
                _calendarPresenceLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Out of office until %@", nil), [UITools optimizedFormatForCalendarPresenceDate: _contact.calendarPresence.automaticReply.untilDate]];
            } else {
                _calendarPresenceLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Out of office", nil)];
            }
        } else if( _contact.calendarPresence.presence == CalendarPresenceOutOfOffice ) {
            _calendarPresenceImage.image = [UIImage imageNamed:@"outOfOffice"];
            _calendarPresenceLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Out of office until %@", nil), [UITools optimizedFormatForCalendarPresenceDate:_contact.calendarPresence.until]];
        } else {
            _calendarPresenceLabel.text = @"";
            _calendarPresenceImage.image = nil;
        }
    }
}

#pragma mark - Rotation
-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

-(void) setContact:(Contact *)contact {
    _contact = nil;
    _contact = contact;
    [self populateUI];
}

-(void) didUpdatePresence {
    if([self isViewLoaded]){
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if([ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyNomadicMode || [ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyCallForward)
        return 5; // + nomadic
    
    return 4; // one for status + one for My Infos + One for my Rainbow Share + one for settings and about
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0)
        return [_supportedPresence count];
    else if (section == 1)
        return 1;
    else if (section == _sectionIndexMyFilesOnServer)
        return 1;
    else if (section == _sectionIndexTelephonySettings)
        return 1;
    else if (section == _sectionIndexMyOthersSettings)
        return 5;
    else
        return 0;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 1 && indexPath.row == 0){
        if(cell.tag != 999){
            CGRect sepFrameTop = CGRectMake(0, 0, cell.frame.size.width , 1);
            UIView *seperatorViewTop = [[UIView alloc] initWithFrame:sepFrameTop];
            seperatorViewTop.backgroundColor = [UITools colorFromHexa:0xD5D5D5FF];
            [cell.contentView addSubview:seperatorViewTop];
            [cell bringSubviewToFront:seperatorViewTop];
            
            CGRect sepFrame = CGRectMake(0, cell.frame.size.height-1, cell.frame.size.width , 1);
            UIView *seperatorView =[[UIView alloc] initWithFrame:sepFrame];
            seperatorView.backgroundColor = [UITools colorFromHexa:0xD5D5D5FF];
            [cell.contentView addSubview:seperatorView];
            cell.tag = 999;
        }
    }
    
    if(indexPath.section == 2 && indexPath.row == 0){
        if(cell.tag != 998){
            CGRect sepFrame = CGRectMake(0, cell.frame.size.height-1, cell.frame.size.width , 1);
            UIView *seperatorView =[[UIView alloc] initWithFrame:sepFrame];
            seperatorView.backgroundColor = [UITools colorFromHexa:0xD5D5D5FF];
            [cell.contentView addSubview:seperatorView];
            cell.tag = 998;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        MyPresenceCell *cell = (MyPresenceCell*)[tableView dequeueReusableCellWithIdentifier:kMyPresenceCellID forIndexPath:indexPath];
        cell.presence = _supportedPresence[indexPath.row];
        BOOL isSamePresence = [_contact.presence isEqual:_supportedPresence[indexPath.row]];
        cell.isCheckMarkEnabled = isSamePresence;
        cell.userInteractionEnabled = _loginManager.isConnected && !_interactionMustBeBlocked;
        cell.presenceLabel.alpha = cell.userInteractionEnabled?1:0.5;
        return cell;
    } else if(indexPath.section == 1){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMyInfosCellID];
        cell.textLabel.text = NSLocalizedString(@"My profile", nil);
        cell.imageView.image = [UIImage imageNamed:@"icon_contact"];
        [cell.imageView setTintColor:[UITools defaultTintColor]];
        [UITools applyCustomFontTo:cell.textLabel];
        return cell;
    } else if (indexPath.section == _sectionIndexTelephonySettings){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMyOthersSettingsCellID];
        cell.userInteractionEnabled = YES;
        cell.textLabel.alpha = 1;
        cell.alpha = 1;
        cell.textLabel.text = NSLocalizedString(@"Telephony", nil);
        
        BOOL forwarded = NO;

        // CallForward
        if([ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyCallForward && [ServicesManager sharedInstance].myUser.callForwardStatus) {
            switch ([ServicesManager sharedInstance].myUser.callForwardStatus.type) {
                case CallForwardTypeVoicemail:
                    forwarded = YES;
                    cell.imageView.image = [UIImage imageNamed:@"forward_voicemail"];
                    break;
                    
                case CallForwardTypePhoneNumber:
                    forwarded = YES;
                    cell.imageView.image = [UIImage imageNamed:@"forward_phonenumber"];
                    break;
                    
                case CallForwardTypeNotForwarded:
                default:
                    cell.imageView.image = [UIImage imageNamed:@"forward_off"];
                    break;
            }
        }
        
        // Nomadic
        if(!forwarded) {
            MyUser *myUser = [ServicesManager sharedInstance].myUser;
            
            PhoneNumber *workMobileNumber = [myUser.contact getPhoneNumberOfType:PhoneNumberTypeWork withDeviceType:PhoneNumberDeviceTypeMobile];
            PhoneNumber *personalMobileNumber = [myUser.contact getPhoneNumberOfType:PhoneNumberTypeHome withDeviceType:PhoneNumberDeviceTypeMobile];
            NSMutableArray <PhoneNumber *> *validNumbers = [NSMutableArray array];
            if(workMobileNumber)
                [validNumbers addObject:workMobileNumber];
            if(personalMobileNumber)
                [validNumbers addObject:personalMobileNumber];
            
            if((myUser.isAllowedToUseTelephonyNomadicMode && myUser.nomadicStatus.activated && [validNumbers containsObject:myUser.nomadicStatus.destination]) || (myUser.isAllowedToUseTelephonyWebRTCtoPSTN && myUser.isNomadicModeActivated && myUser.mediaPillarStatus.activated && myUser.mediaPillarStatus.featureActivated)) {
                cell.imageView.image = [UIImage imageNamed:@"mobile_ringing"];
            } else {
                cell.imageView.image = [UIImage imageNamed:@"office_mobile_ringing_off"];
            }
        }
        
        if(!forwarded && ![ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyNomadicMode)
            cell.imageView.image = [UIImage imageNamed:@"forward_off"];
        
        [cell.imageView setTintColor:[UITools defaultTintColor]];
        [UITools applyCustomFontTo:cell.textLabel];
        return cell;
    } else if (indexPath.section == _sectionIndexMyFilesOnServer){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMyOthersSettingsCellID];
        cell.userInteractionEnabled = YES;
        cell.textLabel.alpha = 1;
        cell.alpha = 1;
        cell.textLabel.text = NSLocalizedString(@"My Rainbow share", nil);
        cell.imageView.image = [UIImage imageNamed:@"folderRainbowSmall"];
        [cell.imageView setTintColor:[UITools defaultTintColor]];
        [UITools applyCustomFontTo:cell.textLabel];
        return cell;
    } else if (indexPath.section == _sectionIndexMyOthersSettings){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMyOthersSettingsCellID];
        cell.userInteractionEnabled = YES;
        cell.textLabel.alpha = 1;
        cell.alpha = 1;
        NSString *message = nil;
        UIImage *image = nil;
        switch (indexPath.row) {
            case 0:
                message = [NSString stringWithFormat:@"%@ (%@)", _emilyBotContact.displayName,
                           NSLocalizedString(@"e-Support", nil)];
                image = [_emilyBotContact imageWithSize:CGSizeMake(25, 25)];
                break;
            case 1:
                message = NSLocalizedString(@"Help Center", nil);
                image = [UIImage imageNamed:@"help"];
                break;
            case 2:
                message = NSLocalizedString(@"Settings", nil);
                image = [UIImage imageNamed:@"settings"];
                break;
            case 3:
                message = NSLocalizedString(@"About", nil);
                image = [UIImage imageNamed:@"infoButton"];
                break;
            case 4:
                message = NSLocalizedString(@"Logout", nil);
                image = [UIImage imageNamed:@"logout"];
                cell.userInteractionEnabled = _loginManager.isConnected && !_interactionMustBeBlocked;
                cell.textLabel.alpha = cell.userInteractionEnabled?1:0.5;
            default:
                break;
        }
        cell.textLabel.text = message;
        cell.imageView.image = image;
        [cell.imageView setTintColor:[UITools defaultTintColor]];
        [UITools applyCustomFontTo:cell.textLabel];
        
        if(indexPath.row == 1){
            cell.imageView.layer.cornerRadius = 25/2;
            cell.imageView.layer.masksToBounds = YES;
        } else {
            cell.imageView.layer.cornerRadius = 0;
            cell.imageView.layer.masksToBounds = NO;
        }
        return cell;
    } else {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        MyPresenceCell *cell = (MyPresenceCell*)[tableView cellForRowAtIndexPath:indexPath];
        cell.presence = _supportedPresence[indexPath.row];
        cell.isCheckMarkEnabled = YES;
        [tableView beginUpdates];
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
        [tableView endUpdates];
        [_contactsManager changeMyPresence:_supportedPresence[indexPath.row]];
    } else if(indexPath.section == 1) {
        // Done by perform segue
    } else if (indexPath.section == _sectionIndexMyFilesOnServer){
        UIMyFilesOnServerTableViewController *myFiles = (UIMyFilesOnServerTableViewController *)[[UIStoryboardManager sharedInstance].myFilesStoryBoard instantiateViewControllerWithIdentifier:@"myFilesOnServerID"];
        myFiles.fromView = self;
        [self.navigationController pushViewController:myFiles animated:YES];

    } else if (indexPath.section == _sectionIndexTelephonySettings) {
        TelephonyOptionsViewController *pwdView = (TelephonyOptionsViewController *)[[UIStoryboardManager sharedInstance].myInfosStoryBoard instantiateViewControllerWithIdentifier:@"TelephonyOptionsView"];
        [self.navigationController pushViewController:pwdView animated:YES];
    
    } else if (indexPath.section == _sectionIndexMyOthersSettings){
        switch (indexPath.row) {
            case 0:{
                [self dismissViewControllerAnimated:YES completion:^{
                    [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:_emilyBotContact withCompletionHandler:nil];
                }];
                break;
            }
            case 1:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://support.openrainbow.com"]];
                break;
            case 2:
                // Open iOS Settings
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                break;
            case 3:
                // Do nothing handle by perform segue
                break;
            case 4:
                // Logout
                [self logout];
                break;
            default:
                break;
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if([identifier isEqualToString:@"showAboutSegueID"]){
        if([self.tableView.indexPathForSelectedRow isEqual:[NSIndexPath indexPathForRow:3 inSection:_sectionIndexMyOthersSettings]]){
            return YES;
        } else {
            return NO;
        }
    }
    
    return YES;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"changeStatusSegueID"]){
        MyInfosTableViewController *destViewController = [segue destinationViewController];
        destViewController.contact = _contact;
        destViewController.contactsManager = _contactsManager;
    }
    if([[segue identifier] isEqualToString:@"showContactDetailsSegueID"]){
        UIContactDetailsViewController *destinationController = [segue destinationViewController];
        destinationController.contact = _contact;
    }
}
- (void) logout {
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.mode = MBProgressHUDModeIndeterminate;
    _hud.labelText = NSLocalizedString(@"Disconnecting", nil);
    _hud.removeFromSuperViewOnHide = NO;
    [_hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    
#if !DEBUG
    // Reset password on manual disconnect
    // Keep this in DEBUG mode, because its useful for us, we use the same passwd for all our test accounts.
    [_loginManager setUsername:[ServicesManager sharedInstance].myUser.username andPassword:@""];
#endif
    
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"misc" action:@"logout" label:nil value:nil];
    [_loginManager performSelectorInBackground:@selector(disconnect) withObject:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - DZNEmptyDataSet
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    if(!_loginManager.isConnected)
        return [UIImage imageNamed:@"EmptyNoNetwork"];
    else
        return nil;
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView {
    if(_loginManager.isConnected)
        return [UITools defaultTintColor];
    else
        return nil;
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"";
    if(!_loginManager.isConnected)
        text = NSLocalizedString(@"You're not connected\n check your connection", nil);
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools boldFontName] size:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    return nil;
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    return NO;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    CGFloat offset = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
    offset += CGRectGetHeight(self.navigationController.navigationBar.frame);
    return -offset;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 20.0f;
}

- (void)emptyDataSetWillAppear:(UIScrollView *)scrollView {
    _headerView.hidden = YES;
}

- (void)emptyDataSetWillDisappear:(UIScrollView *)scrollView {
    _headerView.hidden = NO;
}

- (IBAction)didTapOnDoneButtonItem:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
