/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "LargePhotoCell.h"
#import <Photos/Photos.h>
#import "SCLAlertView.h"
#import "SCLAlertView+Rotation.h"
#import "UITools.h"

// Copy-Pasted from XLFormImageCell
// With custom modifications


NSString *const kLargePhotoCellType = @"largephotocell";


@interface LargePhotoCell() <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIPopoverController *popoverController;
    UIImagePickerController *imagePickerController;
    UIAlertController *alertController;
}
@end


@implementation LargePhotoCell

// Custom big height
+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor
{
    return 80;
}

- (void)configure
{
    [super configure];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Custom big image preview
    self.accessoryView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    self.editingAccessoryView = self.accessoryView;
}

- (void)update
{
    [super update];
    self.textLabel.text = self.rowDescriptor.title;
    self.imageView.image = self.rowDescriptor.value;
    // Custom corner radius on preview image
    self.imageView.layer.cornerRadius = self.imageView.frame.size.width/2.0; //8;
    self.imageView.layer.masksToBounds = YES;
}

- (void)chooseImage:(UIImage *)image
{
    self.imageView.image = image;
    self.rowDescriptor.value = image;
}

- (UIImageView *)imageView
{
    return (UIImageView *)self.accessoryView;
}

- (void)formDescriptorCellDidSelectedWithFormController:(XLFormViewController *)controller
{
    alertController = [UIAlertController alertControllerWithTitle: self.rowDescriptor.title
                                                          message: nil
                                                   preferredStyle: UIAlertControllerStyleActionSheet];
    
    [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Choose From Library", nil) style: UIAlertActionStyleDefault handler: ^(UIAlertAction * action) {
        [UITools openImagePicker:UIImagePickerControllerSourceTypePhotoLibrary delegate:self presentingViewController:self.formViewController];
    }]];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Take Photo", nil) style: UIAlertActionStyleDefault handler: ^(UIAlertAction * _Nonnull action) {
            [UITools openImagePicker:UIImagePickerControllerSourceTypeCamera delegate:self presentingViewController:self.formViewController];
        }]];
    }
    
    // Custom 'cancel' button
    [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Cancel", nil)
                                                        style: UIAlertActionStyleCancel
                                                      handler: ^(UIAlertAction * _Nonnull action) {
                                                          [alertController dismissViewControllerAnimated:YES completion:nil];
                                                      }]];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        alertController.modalPresentationStyle = UIModalPresentationPopover;
        alertController.popoverPresentationController.sourceView = self.contentView;
        alertController.popoverPresentationController.sourceRect = self.contentView.bounds;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.formViewController presentViewController:alertController animated: true completion: nil];
    });
}

#pragma mark -  UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    
    if (editedImage) {
        [self chooseImage:editedImage];
    } else {
        [self chooseImage:originalImage];
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if (popoverController && popoverController.isPopoverVisible) {
            [popoverController dismissPopoverAnimated: YES];
        }
    } else {
        [self.formViewController dismissViewControllerAnimated: YES completion: nil];
    }
}

-(UIView *) overlayView {
    UIView *overlayView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 40.0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-50)];
    overlayView.userInteractionEnabled = NO;
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height-50;
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    int position = IS_WIDESCREEN ? 124-40 : 80;
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    UIBezierPath *path2 = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(0.0f, position, screenWidth, screenWidth)];
    [path2 setUsesEvenOddFillRule:YES];
    [circleLayer setPath:[path2 CGPath]];
    [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, screenWidth, screenHeight-72) cornerRadius:0];
    [path appendPath:path2];
    [path setUsesEvenOddFillRule:YES];
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = path.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [UIColor blackColor].CGColor;
    fillLayer.opacity = 0.4;
    [overlayView.layer addSublayer:fillLayer];
    
    return overlayView;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // This is only for the choose picture view
    if ([navigationController.viewControllers count] == 3) {
        [viewController.view.layer addSublayer:[self overlayView].layer];
    }
}


@end
