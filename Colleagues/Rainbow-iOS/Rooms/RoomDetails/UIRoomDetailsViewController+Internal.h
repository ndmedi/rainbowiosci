/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIContactDetailsViewController.h"
#import <Rainbow/ServicesManager.h>
#import "UITools.h"
#import "UIRoomsDetailsAddParticipantTableViewCell.h"
#import "UIAvatarView.h"
#import "UIScrollView+APParallaxHeader.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "OrderedOptionalSectionedContent.h"

#define kAdministratorsSection @"Organizers"
#define kAttendeesSection @"Members"
#define kPendingSection @"Pending invitations"
#define kAwaySection @"Away attendees"
#define kRefusedSection @"Refused attendees"

@interface UIRoomDetailsViewController () <APParallaxViewDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIAvatarView *avatarView;
@property (weak, nonatomic) IBOutlet UIAvatarView *backgroundAvatar;
@property (nonatomic, strong) UIVisualEffectView *visualEffectView;
@property (nonatomic, strong) OrderedOptionalSectionedContent<Participant *> *participants;
-(void) showContactDetails:(Contact *) contact;
@end
