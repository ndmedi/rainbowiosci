/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIRoomDetailsViewController.h"
#import "UIContactDetailsViewController.h"
#import <Rainbow/ServicesManager.h>
#import "UITools.h"
#import "UIRoomsDetailsAddParticipantTableViewCell.h"
#import "UIGenericAddObjectWithTokenViewController.h"
#import "UIAvatarView.h"
#import "UIScrollView+APParallaxHeader.h"
#import "BGTableViewRowActionWithImage.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "OrderedOptionalSectionedContent.h"
#import "GoogleAnalytics.h"
#import "UIRainbowGenericTableViewCell.h"
#import "Participant+UIRainbowTableViewCellProtocol.h"

#import "UIRoomDetailsViewController+Internal.h"
#import <Photos/Photos.h>
#if !defined(APP_EXTENSION)
#import "SCLAlertView.h"
#import "SCLAlertView+Rotation.h"
#endif
#import "UIStoryboardManager.h"


@interface UIRoomDetailsViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *roomNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomTopicLabel;
@property (nonatomic, strong) UIAlertAction *saveAction;
@end

@implementation UIRoomDetailsViewController

-(void)awakeFromNib {
    [super awakeFromNib];
}

-(void) viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    if(_room.conference)
        self.title = NSLocalizedString(@"Meeting details", nil);
    else
        self.title = NSLocalizedString(@"Bubble details", nil);
    
    if(!_room.isAdmin) {
        self.navigationItem.rightBarButtonItem = nil;
    } else {
        // If we use a standard "self.editButtonItem", the label will go from "Edit" to "Done"
        // when we edit a cell of the tableview. Which is NOT what we want.
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Modify"] style:UIBarButtonItemStylePlain target:self action:@selector(editButtonClicked:)];
    }
    
    _headerView.clipsToBounds = NO;
    
    [self drawRoomInfos];
    
    [_headerView sendSubviewToBack:_backgroundAvatar];
    
    [self.tableView addParallaxWithView:_headerView andHeight:_headerView.frame.size.height andMinHeight:_headerView.frame.size.height andShadow:YES];
    
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    _visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    _visualEffectView.frame = self.tableView.parallaxView.bounds;;
    [_backgroundAvatar addSubview:_visualEffectView];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.tableView.parallaxView setDelegate:self];
    
    // Force the parallax view to be at the top position.
    // Without this the tableview section headers goes on top of the parallax view.
    self.tableView.parallaxView.layer.zPosition = 1;
    
    [UITools applyCustomBoldFontTo:_roomNameLabel];
    [UITools applyCustomFontTo:_roomTopicLabel];
    
    if(_room){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(invitationStatusChanged:) name:kRoomsServiceDidRoomInvitationStatusChanged object:nil],
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRoom:) name:kRoomsServiceDidRemoveRoom object:nil];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    

    if(![ServicesManager sharedInstance].loginManager.isConnected)
        [self didLostConnection:nil];
    
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(![ServicesManager sharedInstance].loginManager.isConnected)
        [self didReconnect:nil];
}

-(void) dealloc {
    _saveAction = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRemoveRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    
    [_participants removeAllObjects];
    _participants = nil;
    
    _room = nil;
}

#pragma mark - Login manager notification
-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        if(_room.isAdmin){
            UIRoomsDetailsAddParticipantTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
            cell.userInteractionEnabled = NO;
            self.navigationItem.rightBarButtonItem.enabled = NO;
        }
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        if(_room.isAdmin){
            UIRoomsDetailsAddParticipantTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
            cell.userInteractionEnabled = YES;
            self.navigationItem.rightBarButtonItem.enabled = YES;
        }
    }
}

-(void) setRoom:(Room *)room {
    _room = nil;
    _room = room;

    [_participants removeAllObjects];
    _participants = nil;
    
    _participants = [[OrderedOptionalSectionedContent alloc] initWithSections:@[kAdministratorsSection, kAttendeesSection]]; // kPendingSection, kAwaySection, kRefusedSection
    
    // Cheat for my room, we want a "Add Participant" line in the Attendees section.
    if (_room.isAdmin) {
        // UGLY : By chance, when sorted, this fake participant remains on top.
        Participant *fakeAddParticipant = [Participant new];
        [_participants addObject:fakeAddParticipant toSection:kAttendeesSection];
    }
    
    // Now we have to dispatch the _room.participants in the right sections.
    for (Participant *participant in _room.participants) {
        if (participant.privilege == ParticipantPrivilegeModerator || participant.privilege == ParticipantPrivilegeOwner) {
            [_participants addObject:participant toSection:kAdministratorsSection];
        } else {
            if(participant.status == ParticipantStatusInvited || participant.status == ParticipantStatusAccepted || participant.status == ParticipantStatusInvitedGuest)
                [_participants addObject:participant toSection:kAttendeesSection];
        }
    }
    
    // Now we can sort them
    if([ServicesManager sharedInstance].contactsManagerService.sortByFirstName) {
        [[_participants sectionForKey:kAdministratorsSection] sortUsingDescriptors:@[[self participantPrivilegeFirstNameLastNameSortDescriptor]]];
        [[_participants sectionForKey:kAttendeesSection] sortUsingDescriptors:@[[self participantStatusFirstNameLastNameSortDescriptor]]];
    } else {
        [[_participants sectionForKey:kAdministratorsSection] sortUsingDescriptors:@[[self participantPrivilegeLastNameFirstNameSortDescriptor]]];
        [[_participants sectionForKey:kAttendeesSection] sortUsingDescriptors:@[[self participantStatusLastNameFirstNameSortDescriptor]]];
    }
}

#pragma mark - sort by status
-(NSSortDescriptor *) participantStatusLastNameFirstNameSortDescriptor {
    return [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(Participant* obj1, Participant* obj2) {
        // Fake participant (Add participant button) MUST always be at the top of member list
        if(obj1.contact == nil || obj2.contact == nil)
            return NSOrderedAscending;
        
        // Compose a string based on ParticipantStatus, contact.lastName and contact.firstName to be used to sort by all these criterias at once
        // status accepted must be first (0) and others status values +1.
        // ParticipantStatusAccepted -> 0, ParticipantStatusUnknown+1, ParticipantStatusInvited+1, etc...
        NSString *obj1Str = [NSString stringWithFormat:@"%lu %@ %@", (obj1.status == ParticipantStatusAccepted) ? 0 : obj1.status+1, obj1.contact.lastName, obj1.contact.firstName];
        NSString *obj2Str = [NSString stringWithFormat:@"%lu %@ %@", (obj2.status == ParticipantStatusAccepted) ? 0 : obj2.status+1, obj2.contact.lastName, obj2.contact.firstName];
        return [obj1Str compare:obj2Str options:NSCaseInsensitiveSearch];
    }];
}

-(NSSortDescriptor *) participantStatusFirstNameLastNameSortDescriptor {
    return [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(Participant* obj1, Participant* obj2) {
        // Fake participant (Add participant button) MUST always be at the top of member list
        if(obj1.contact == nil || obj2.contact == nil)
            return NSOrderedAscending;
        
        // Compose a string based on ParticipantStatus, contact.lastName and contact.firstName to be used to sort by all these criterias at once
        // status accepted must be first (0) and others status values +1.
        // ParticipantStatusAccepted -> 0, ParticipantStatusUnknown+1, ParticipantStatusInvited+1, etc...
        NSString *obj1Str = [NSString stringWithFormat:@"%lu %@ %@", (obj1.status == ParticipantStatusAccepted) ? 0 : obj1.status+1, obj1.contact.firstName, obj2.contact.lastName ];
        NSString *obj2Str = [NSString stringWithFormat:@"%lu %@ %@", (obj2.status == ParticipantStatusAccepted) ? 0 : obj2.status+1, obj2.contact.firstName, obj2.contact.lastName ];
        return [obj1Str compare:obj2Str options:NSCaseInsensitiveSearch];
    }];
}

#pragma mark - sort by privilege
-(NSSortDescriptor *) participantPrivilegeLastNameFirstNameSortDescriptor {
    return [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(Participant* obj1, Participant* obj2) {
        // Fake participant (Add participant button) MUST always be at the top of member list
        if(obj1.contact == nil || obj2.contact == nil)
            return NSOrderedAscending;
        
        // Compose a string based on ParticipantStatus, contact.lastName and contact.firstName to be used to sort by all these criterias at once
        // status accepted must be first (0) and others status values +1.
        // ParticipantStatusAccepted -> 0, ParticipantStatusUnknown+1, ParticipantStatusInvited+1, etc...
        NSString *obj1Str = [NSString stringWithFormat:@"%lu %@ %@", (obj1.privilege == ParticipantPrivilegeOwner) ? 0 : obj1.privilege+1, obj1.contact.lastName, obj1.contact.firstName];
        NSString *obj2Str = [NSString stringWithFormat:@"%lu %@ %@", (obj2.privilege == ParticipantPrivilegeOwner) ? 0 : obj2.privilege+1, obj2.contact.lastName, obj2.contact.firstName];
        return [obj1Str compare:obj2Str options:NSCaseInsensitiveSearch];
    }];
}

-(NSSortDescriptor *) participantPrivilegeFirstNameLastNameSortDescriptor {
    return [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(Participant* obj1, Participant* obj2) {
        // Fake participant (Add participant button) MUST always be at the top of member list
        if(obj1.contact == nil || obj2.contact == nil)
            return NSOrderedAscending;
        
        // Compose a string based on ParticipantStatus, contact.lastName and contact.firstName to be used to sort by all these criterias at once
        // status accepted must be first (0) and others status values +1.
        // ParticipantStatusAccepted -> 0, ParticipantStatusUnknown+1, ParticipantStatusInvited+1, etc...
        NSString *obj1Str = [NSString stringWithFormat:@"%lu %@ %@", (obj1.privilege == ParticipantPrivilegeOwner) ? 0 : obj1.privilege+1, obj1.contact.firstName, obj2.contact.lastName ];
        NSString *obj2Str = [NSString stringWithFormat:@"%lu %@ %@", (obj2.privilege == ParticipantPrivilegeOwner) ? 0 : obj2.privilege+1, obj2.contact.firstName, obj2.contact.lastName ];
        return [obj1Str compare:obj2Str options:NSCaseInsensitiveSearch];
    }];
}


-(void) drawRoomInfos {
    if(_room){
        _avatarView.peer = _room;
        _avatarView.showPresence = NO;
        _avatarView.asCircle = YES;
        _backgroundAvatar.peer = _room;
        _backgroundAvatar.showPresence = NO;
        _backgroundAvatar.cornerRadius = 0;
        _roomNameLabel.text = _room.displayName;
        _roomTopicLabel.text = _room.topic;
    }
}


#pragma mark - orientation & style

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

-(UIInterfaceOrientationMask) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark - APParallaxViewDelegate
- (void)parallaxView:(APParallaxView *)view willChangeFrame:(CGRect)frame {
}

- (void)parallaxView:(APParallaxView *)view didChangeFrame:(CGRect)frame {
    CGRect visualEffectFrame = _visualEffectView.frame;
    visualEffectFrame.size.height = frame.size.height;
    [_visualEffectView setFrame:visualEffectFrame];
}

#pragma mark - UITableView delegate

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_participants allNotEmptySections] count];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:section];
    return [[_participants sectionForKey:key] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:section];
    NSString *sectionStr = [[_participants allNotEmptySections] objectAtIndex:section];
    NSUInteger participantCount = [[_participants sectionForKey:key] count];
    
    if([sectionStr isEqualToString:kAttendeesSection]) {
        if(participantCount > 0 && _room.isAdmin) // The addParticipant button should not be counted
            participantCount--;
        
        return [NSString stringWithFormat:@"%@ (%lu)", NSLocalizedString(sectionStr, nil), (unsigned long)participantCount];
    } else if( [sectionStr isEqualToString:kAdministratorsSection] ) {
        return (participantCount > 1) ? [NSString stringWithFormat:@"%@ (%lu)", NSLocalizedString(@"Organizers", nil), (unsigned long)participantCount] : NSLocalizedString(@"Organizer", nil);
    }
    
    return NSLocalizedString(sectionStr, nil);
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
    tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools boldFontName] size:16.0f];
    tableViewHeaderFooterView.textLabel.textColor = [UITools defaultTintColor];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 1 && indexPath.row == 0 && _room.isAdmin) {
        // "Add participant" line button
        UIRoomsDetailsAddParticipantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addParticipantCellID" forIndexPath:indexPath];
        return cell;
    } else {
        return [self cellForRowAtIndexPath:indexPath];
    }
}

-(UIRainbowGenericTableViewCell *) cellForRowAtIndexPath:(NSIndexPath *) indexPath {
    
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:indexPath.section];
    NSArray<Participant *> *content = [_participants sectionForKey:key];
    
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    
    cell.cellObject = [content objectAtIndex:indexPath.row];
    __weak typeof(cell) weakCell = cell;
    
    // Disable cell button with invited guest
    if ([content objectAtIndex:indexPath.row].status == ParticipantStatusInvitedGuest) {
        cell.cellButtonTapHandler = nil;
        cell.userInteractionEnabled = NO;
        return cell;
    } else {
        cell.cellButtonTapHandler = ^(UIButton *sender) {
            [self showContactDetails:((Participant *)weakCell.cellObject).contact];
        };
        cell.userInteractionEnabled = YES;
    }
    
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Give a specific height to the "Add Participant" line button
    if(indexPath.section == 1  && indexPath.row == 0 && _room.isAdmin)
        return 44;
    return kRainbowGenericTableViewCellHeight;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 1 && indexPath.row == 0 && _room.isAdmin) {
        // Open add participant view
    } else {
        UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        [self showContactDetails:((Participant *)cell.cellObject).contact];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    if (![cell isKindOfClass:[UIRainbowGenericTableViewCell class]]) {
        return NO;
    }
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:indexPath.section];
    NSArray<Participant *> *content = [_participants sectionForKey:key];
    Participant *participant = [content objectAtIndex:indexPath.row];
    Contact *contact = participant.contact;
    if((contact == [ServicesManager sharedInstance].myUser.contact && _room.isAdmin) || participant.privilege == ParticipantPrivilegeModerator)
        return NO;
    
    return _room.isAdmin && _room.myStatusInRoom == ParticipantStatusAccepted;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    // We can have the UIRoomsDetailsAddParticipantTableViewCell
    if (![cell isKindOfClass:[UIRainbowGenericTableViewCell class]]) {
        return @[];
    }
    
    Participant *selectedParticipant = (Participant*)cell.cellObject;
    Contact *selectedContact = selectedParticipant.contact;
    
    switch (selectedParticipant.status) {
        case ParticipantStatusInvited: {
            BGTableViewRowActionWithImage *action = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Remove", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"RemoveAttendees"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                    [[ServicesManager sharedInstance].roomsService cancelInvitationForContact:selectedContact inRoom:_room];
                });
            }];
            action.accessibilityLabel = NSLocalizedString(@"Remove", nil);
            return @[action];
        }
        case ParticipantStatusAccepted: {
            BGTableViewRowActionWithImage *action = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Remove", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"RemoveAttendees"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                    [[ServicesManager sharedInstance].roomsService removeParticipant:selectedParticipant fromRoom:_room];
                });
            }];
            action.accessibilityLabel = NSLocalizedString(@"Remove", nil);
            return @[action];
        }
        case ParticipantStatusUnsubscribed:
        case ParticipantStatusDeleted:
        case ParticipantStatusRejected: {
            BGTableViewRowActionWithImage *action = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Re-invite", nil) backgroundColor:[UIColor blueColor] image:[UIImage imageNamed:@"JoinBubble"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                    if(selectedParticipant.status == ParticipantStatusRejected || selectedParticipant.status == ParticipantStatusUnsubscribed)
                        [[ServicesManager sharedInstance].roomsService deleteParticipant:selectedParticipant fromRoom:_room];
                    [[ServicesManager sharedInstance].roomsService inviteContact:selectedContact inRoom:_room error:nil];
                });
            }];
            action.accessibilityLabel = NSLocalizedString(@"Re-invite", nil);
            return @[action];
        }
        case ParticipantStatusUnknown:
        default:
            break;
    }
    
    return @[];
}

#pragma mark - Segues
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"addParticipantsSegueID"]){
        UINavigationController *destNavController = [segue destinationViewController];
        UIGenericAddObjectWithTokenViewController *destViewController = [destNavController.viewControllers firstObject];
        destViewController.room = _room;
    }
}

- (void) editButtonClicked:(UIBarButtonItem *)sender {
    UIAlertController *editActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* changeNameAction = [UIAlertAction actionWithTitle:_room.conference?NSLocalizedString(@"Change meeting name", nil):NSLocalizedString(@"Change bubble name", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        UIAlertController *newNameAlert = [UIAlertController alertControllerWithTitle:_room.conference?NSLocalizedString(@"Enter new meeting name", nil):NSLocalizedString(@"Enter new bubble name", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        [newNameAlert addTextFieldWithConfigurationHandler:^(UITextField * textField) {
            [UITools applyCustomFontToTextField:textField];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:textField];
        }];
        _saveAction = nil;
        _saveAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSString *newName = [newNameAlert.textFields firstObject].text;
            [[ServicesManager sharedInstance].roomsService updateRoom:_room withName:newName];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
            
        }];
        _saveAction.enabled = NO;
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
            _saveAction = nil;
        }];
        
        [newNameAlert addAction:_saveAction];
        [newNameAlert addAction:cancel];
        [newNameAlert.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:newNameAlert animated:YES completion:nil];
    }];
    
    [editActionSheet addAction:changeNameAction];
    
    
    UIAlertAction* changeTopicAction = [UIAlertAction actionWithTitle:_room.conference?NSLocalizedString(@"Change meeting subject", @""):NSLocalizedString(@"Change bubble topic", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        UIAlertController *newNameAlert = [UIAlertController alertControllerWithTitle:_room.conference?NSLocalizedString(@"Enter new meeting subject", nil):NSLocalizedString(@"Enter new bubble topic", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        [newNameAlert addTextFieldWithConfigurationHandler:^(UITextField * textField) {
            [UITools applyCustomFontToTextField:textField];
        }];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSString *newTopic = [newNameAlert.textFields firstObject].text;
            [[ServicesManager sharedInstance].roomsService updateRoom:_room withTopic:newTopic];
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        
        [newNameAlert addAction:ok];
        [newNameAlert addAction:cancel];
        [newNameAlert.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:newNameAlert animated:YES completion:nil];
    }];
    
    [editActionSheet addAction:changeTopicAction];
    /*
    UIAlertAction* removeAllParticipantsAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Remove all attendees", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        NSString *message = NSLocalizedString(@"By removing all attendees, they will not be able to access to this bubble, this operation is irresversible", nil);
        
        UIAlertController *confirmDeleteActionSheet = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                for (Participant *participant in _room.participants) {
                    [[ServicesManager sharedInstance].roomsService removeContact:participant.contact fromRoom:_room];
                }
            });
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        
        [confirmDeleteActionSheet addAction:ok];
        [confirmDeleteActionSheet addAction:cancel];
        // show the menu.
        [confirmDeleteActionSheet.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:confirmDeleteActionSheet animated:YES completion:nil];
    }];
    */
    
    // check > 1 because we are a participant
    /*if(_room.participants.count > 1)
        [editActionSheet addAction:removeAllParticipantsAction];
     */
    
    UIAlertAction* changeAvatarAction = [UIAlertAction actionWithTitle:_room.conference?NSLocalizedString(@"Change meeting avatar", @""):NSLocalizedString(@"Change bubble avatar", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        UIAlertController *chooseSource = [UIAlertController alertControllerWithTitle: NSLocalizedString(@"Choose your source", nil)
                                                              message: nil
                                                       preferredStyle: UIAlertControllerStyleActionSheet];
        
        [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Choose From Library", nil) style: UIAlertActionStyleDefault handler: ^(UIAlertAction * action) {
            [UITools openImagePicker:UIImagePickerControllerSourceTypePhotoLibrary delegate:self presentingViewController:self];
        }]];
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Take Photo", nil) style: UIAlertActionStyleDefault handler: ^(UIAlertAction * _Nonnull action) {
                [UITools openImagePicker:UIImagePickerControllerSourceTypeCamera delegate:self presentingViewController:self];
            }]];
        }
        
        // Custom 'cancel' button
        [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Cancel", nil) style: UIAlertActionStyleCancel handler: ^(UIAlertAction * _Nonnull action) {
            [chooseSource dismissViewControllerAnimated:YES completion:nil];
        }]];
        [chooseSource.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:chooseSource animated:YES completion:nil];
    }];
    
    [editActionSheet addAction:changeAvatarAction];
    
    if( _room.photoData ) {
        UIAlertAction* deleteAvatarAction = [UIAlertAction actionWithTitle:_room.conference?NSLocalizedString(@"Delete meeting avatar", @""):NSLocalizedString(@"Delete bubble avatar", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            UIAlertController *deleteAvatarAlert = [UIAlertController alertControllerWithTitle:_room.conference?NSLocalizedString(@"Do you want to delete the customized meeting avatar ?", nil):NSLocalizedString(@"Do you want to delete the customized bubble avatar ?", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [[ServicesManager sharedInstance].roomsService deleteAvatar:_room];
            }];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
            
            [deleteAvatarAlert addAction:ok];
            [deleteAvatarAlert addAction:cancel];
            [deleteAvatarAlert.view setTintColor:[UITools defaultTintColor]];
            [self presentViewController:deleteAvatarAlert animated:YES completion:nil];
            
        }];
        
        [editActionSheet addAction:deleteAvatarAction];
    }
    
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [editActionSheet addAction:cancel];
    
    // show the menu.
    [editActionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:editActionSheet animated:YES completion:nil];
}

#pragma mark -  UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    NSData *imageData;
    
    if (editedImage) {
        NSLog(@"Edited image %@", editedImage);
        imageData = [NSData dataWithData:UIImagePNGRepresentation(editedImage)];
    } else {
        NSLog(@"Original image %@", originalImage);
        imageData = [NSData dataWithData:UIImagePNGRepresentation(originalImage)];
    }
    
    // display a loading spinner
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Saving", nil);
    hud.removeFromSuperViewOnHide = YES;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [[ServicesManager sharedInstance].roomsService updateAvatar:_room withPhotoData: imageData withCompletionBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error) {
                    // Hide our loading spinner
                    [hud hide:YES];
                    NSLog(@"An error occured while updating the room avatar %@", error);
                } else {
                    // If everything is OK,
                    // Display the checkmark, wait 1 sec and go back to MyInfos
                    hud.mode = MBProgressHUDModeCustomView;
                    hud.labelText = NSLocalizedString(@"Saved", nil);
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:nil];
                }
            });
        }];
    });

    [picker dismissViewControllerAnimated: YES completion: nil];
}

#pragma mark - RoomsService notifications
-(void) didUpdateRoom:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateRoom:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Room *room = [userInfo objectForKey:kRoomKey];
    if([room isEqual:_room]){
        //NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        
        [self setRoom:room];
        if([self isViewLoaded]){
            [self drawRoomInfos];
            [self.tableView reloadData];
        }
    }
}

-(void) invitationStatusChanged:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self invitationStatusChanged:notification];
        });
        return;
    }
    
    Room *room = (Room *) notification.object;
    if([room isEqual:_room]){
        if(_room.myStatusInRoom == ParticipantStatusAccepted) {
            [self drawRoomInfos];
            [self.tableView reloadData];
        } else {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

-(void) didRemoveRoom:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveRoom:notification];
        });
        return;
    }
    
    Room *room = (Room *) notification.object;
    if([room isEqual:_room]){
        // We are displaying a room that as been removed so leaving this view.
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - DZNEmptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"No attendees",nil);
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools boldFontName] size:18.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - 3D Touch actions
- (NSArray<id<UIPreviewActionItem>> *)previewActionItems {
    UIPreviewAction *archiveAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Archive", nil) style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"save" label:nil value:nil];
        [[ServicesManager sharedInstance].roomsService archiveRoom:_room];
    }];
    
    UIPreviewAction *leaveAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Quit", nil) style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"leave" label:nil value:nil];
        [[ServicesManager sharedInstance].roomsService leaveRoom:_room];
    }];
    
    UIPreviewAction *acceptAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Accept",nil) style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"accept invitation" label:nil value:nil];
       
        
        [[ServicesManager sharedInstance].roomsService acceptInvitation:_room completionBlock:^(NSError *error, BOOL success) {
            if (!success && error.code != NSURLErrorNotConnectedToInternet) {
               
                [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Cannot accept the invitation to join the bubble", nil)  inViewController:self];
            }
        }];
        
      
    }];
    
    UIPreviewAction *declineAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Decline",nil) style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"cancel" label:nil value:nil];
        
        
        [[ServicesManager sharedInstance].roomsService declineInvitation:_room completionBlock:^(NSError *error, BOOL success) {
            if (!success && error.code != NSURLErrorNotConnectedToInternet) {
               
                [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Cannot decline the invitation to join the bubble", nil)  inViewController:self];
            }
        }];
    }];
    
    if(_room.myStatusInRoom == ParticipantStatusInvited)
        return @[acceptAction, declineAction];
    else {
        if(_room.isMyRoom)
            return @[archiveAction];
        else
            return @[leaveAction];
    }
    return nil;
}

-(void) textFieldDidChange:(NSNotification *) notification {
    UITextField *textField = (UITextField *)notification.object;
    _saveAction.enabled = (textField.text.length >= 3) && ![self roomNameMatchAnExistingRoomWithName:textField.text] && ([[textField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] length] != 0);
}

-(BOOL) roomNameMatchAnExistingRoomWithName:(NSString *) name {
    NSArray<Room*> * rooms = [[ServicesManager sharedInstance].roomsService searchMyRoomMatchName:name];
    if(rooms.count > 0)
        return YES;
    
    return NO;
}

#pragma mark - show contact details
-(void) showContactDetails:(Contact *) contact {
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}
@end
