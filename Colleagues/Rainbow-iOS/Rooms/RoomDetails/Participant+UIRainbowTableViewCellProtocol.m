/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Participant+UIRainbowTableViewCellProtocol.h"
#import <Rainbow/Peer.h>
#import "UITools.h"
#import "Contact+UIRainbowGenericTableViewCellProtocol.h"

@implementation Participant (UIRainbowGenericTableViewCellProtocol)

-(NSString *) mainLabel {
    return self.contact.mainLabel;
}

-(NSString *) subLabel {
    return self.contact.subLabel;
}

-(UIColor *) mainLabelTextColor {
    return self.contact.mainLabelTextColor;
}

-(NSDate *) date {
    return self.contact.date;
}

-(NSString *) bottomRightLabel {
    return self.contact.bottomRightLabel;
}

-(Peer *) avatar {
    return self.contact.avatar;
}

-(NSArray *) observablesKeyPath {
    return self.contact.observablesKeyPath;
}

-(NSInteger) badgeValue {
    return self.contact.badgeValue;
}

-(NSString *) cellButtonTitle {
    return self.contact.cellButtonTitle;
}

-(UIImage *) cellButtonImage {
    return self.contact.cellButtonImage;
}

-(UIColor *) cellButtonImageColor {
    return self.contact.cellButtonImageColor;
}

-(UIImage *) rightIcon {
    if(self.privilege == ParticipantPrivilegeOwner)
        return [UIImage imageNamed:@"owner"];
    if(self.privilege != ParticipantPrivilegeModerator) {
        if(self.status == ParticipantStatusInvited)
            return [UIImage imageNamed:@"Participant_pending"];
        else if(self.status == ParticipantStatusAccepted)
            return [UIImage imageNamed:@"Participant_accepted"];
    }
    return nil;
}

-(UIColor *) rightIconTintColor {
    if(self.privilege == ParticipantPrivilegeOwner)
        return [UIColor blackColor];
    if(self.privilege != ParticipantPrivilegeModerator) {
        if(self.status == ParticipantStatusInvited)
            return [UIColor grayColor];
        else if(self.status == ParticipantStatusAccepted)
            return [UITools colorFromHexa:0x34B233FF];
    }
    return nil;
}

@end
