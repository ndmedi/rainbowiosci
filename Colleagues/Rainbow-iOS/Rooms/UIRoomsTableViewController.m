/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIRoomsTableViewController.h"
#import "UITools.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import <Rainbow/ServicesManager.h>
#import <Rainbow/defines.h>
#import <Rainbow/RoomsService.h>
#import "DZNSegmentedControl.h"
#import "UIRainbowGenericTableViewCell.h"
#import "Room+UIRainbowGenericTableViewCellProtocol.h"
#import "UIRoomsInvitationTableViewCell.h"
#import "UIRoomDetailsViewController.h"
#import "BGTableViewRowActionWithImage.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import "JSQSystemSoundPlayer+JSQMessages.h"
#import "UINotificationManager.h"
#import "CustomSearchController.h"
#import "UIContactDetailsViewController.h"
#import "UIGroupDetailsViewController.h"
#import "CustomNavigationController.h"
#import "GoogleAnalytics.h"
#import "MyInfoNavigationItem.h"
#import "UIStoryboardManager.h"
#import "UIGenericBubbleDetailsViewController.h"

#import "FilteredSortedSectionedArray.h"
#import "UIStoryboardManager.h"

#import "UIViewController+Visible.h"
#import "UIPagingMessagesViewController.h"
#import "RecentsConversationsTableViewController.h"
#import "UINetworkLostViewController.h"
#import "UIGuestModeViewController.h"

#define kRoomsInvitationTableViewReusableKey @"roomInvitationCellID"
#define kRoomInvitationsSection @"Invitations"
#define kRoomsSection @"Rooms"

#define dznButtonDisabledColor 0xB2B2B2FF

typedef NS_ENUM(NSInteger, SelectorType){
    SelectorTypeAll = 0,
    SelectorTypeMyRooms,
    SelectorTypeLeftRooms
};

@interface UIRoomsTableViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, DZNSegmentedControlDelegate, UIViewControllerPreviewingDelegate,
UIPopoverPresentationControllerDelegate,UISearchResultDisplayDetailControllerProtocol>
@property (nonatomic, strong) ServicesManager *serviceManager;
@property (nonatomic, strong) RoomsService *roomsService;
@property (nonatomic, strong) ConversationsManagerService *conversationManagerService;
@property (nonatomic, strong) IBOutlet DZNSegmentedControl *roomsFilter;

// This outlet must be keep in strong reference because we must increase the retainCount of the button, to avoid releasing it when displaying it and removing it from the UI depending of search bar state.
@property (nonatomic, strong) IBOutlet UIBarButtonItem *roomRightBarButtonItem;
@property (nonatomic, strong) NSMutableArray *presentedLocalNotification;
@property (nonatomic, strong) CustomSearchController *searchController;
@property (nonatomic, strong) id previewingContext;

@property (nonatomic, strong) FilteredSortedSectionedArray <Room *> *rooms;
@property (nonatomic, strong) NSObject *roomsMutex;
@property (nonatomic, strong) NSPredicate *globalFilterAll;
@property (nonatomic, strong) NSPredicate *globalFilterMyRooms;
@property (nonatomic, strong) NSPredicate *globalFilterArchived;
@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic) BOOL lostNetworkViewAsBeenScrolled;
@property (nonatomic) BOOL guestBannerViewAsBeenScrolled;
@property (nonatomic) BOOL isNetworkConnected;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@property (nonatomic) BOOL populated;
@end

@implementation UIRoomsTableViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLocalNotification:) name:kNotificationsManagerHandleNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndPopulatingMyNetwork:) name:kContactsManagerServiceDidEndPopulatingMyNetwork object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveAllRooms:) name:kRoomsServiceDidRemoveAllRooms object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRoom:) name:kRoomsServiceDidRemoveRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddRoom:) name:kRoomsServiceDidAddRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveRoomInvitation:) name:kRoomsServiceDidReceiveRoomInvitation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRoomInvitationStatusChanged:) name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dumpBubbles) name:@"dumpBubbles" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailRemoveRoom:) name:kRoomsServiceDidFailRemoveRoom object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStartConversation:) name:kConversationsManagerDidStartConversation object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowNetworkLostView:) name:CustomNavigationControllerWillShowNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNetworkLostViewFrame:) name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowGuestModeView:) name:CustomNavigationControllerWillShowGuestModeView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideGuestModeView:) name:CustomNavigationControllerDidHideGuestModeView object:nil];
        
        _roomsMutex = [NSObject new];
        _presentedLocalNotification = [NSMutableArray array];
        
        _sectionedByName = ^NSString*(Room *room) {
            if(room.conference.endpoint.mediaType != ConferenceEndPointMediaTypePSTNAudio && room.myStatusInRoom == ParticipantStatusInvited)
                return kRoomInvitationsSection;
            return kRoomsSection;
        };
        
        _globalFilterAll = [NSPredicate predicateWithBlock:^BOOL(Room *room, NSDictionary<NSString *,id> * bindings) {
            if([room.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                return NO;
            if(room.conference.endedConference && room.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio)
                return NO;
            if(room.conference.endpoint.mediaType != ConferenceEndPointMediaTypePSTNAudio){
                if(room.myStatusInRoom == ParticipantStatusAccepted || room.myStatusInRoom == ParticipantStatusInvited)
                    return YES;
            }
            return NO;
        }];
        
        _globalFilterMyRooms = [NSPredicate predicateWithBlock:^BOOL(Room *evaluatedObject, NSDictionary<NSString *,id> *  bindings) {
            if([evaluatedObject.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                return NO;
            if(evaluatedObject.conference.endedConference && evaluatedObject.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio)
                return NO;
            return evaluatedObject.conference.endpoint.mediaType != ConferenceEndPointMediaTypePSTNAudio && [evaluatedObject.creator isEqual: [ServicesManager sharedInstance].myUser.contact] && evaluatedObject.myStatusInRoom == ParticipantStatusAccepted;
        }];
        
        _globalFilterArchived = [NSPredicate predicateWithBlock:^BOOL(Room *evaluatedObject, NSDictionary<NSString *,id> *  bindings) {
            if([evaluatedObject.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                return NO;
            if(evaluatedObject.conference.endedConference && evaluatedObject.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio)
                return NO;
            return evaluatedObject.conference.endpoint.mediaType != ConferenceEndPointMediaTypePSTNAudio && evaluatedObject.myStatusInRoom == ParticipantStatusUnsubscribed;
        }];
        
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            if([obj1 isEqualToString:kRoomInvitationsSection])
                return NSOrderedAscending;
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
        }];
        
        _rooms = [FilteredSortedSectionedArray new];
        
        // Default filters/sorter/.. values
        _rooms.sectionNameFromObjectComputationBlock = _sectionedByName;
        _rooms.globalFilteringPredicate = _globalFilterAll;
        _rooms.sectionSortDescriptor = _sortSectionAsc;
        
        // __default__ has a special meaning : any other sections not found in the dictionary
        _rooms.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByDisplayName]]};
        
        _serviceManager = [ServicesManager sharedInstance];
        _roomsService = _serviceManager.roomsService;
        _conversationManagerService = _serviceManager.conversationsManagerService;
        
        // When the room is opened in a conversation, sometimes we get the didAddRoom before this init, so we lose it, so just add initial rooms
        for (Room* room in _roomsService.rooms) {
            @synchronized (_roomsMutex) {
                if(![_rooms containsObject:room]){
                    [_rooms addObject:room];
                }
            }
        }
        [_rooms reloadData];
        
        // Don't enable the add button until the roster is populated
        _populated = NO;
        _roomRightBarButtonItem.enabled = NO;
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidEndPopulatingMyNetwork object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRemoveAllRooms object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRemoveRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidAddRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidReceiveRoomInvitation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dumpBubbles" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidFailRemoveRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidStartConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideGuestModeView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowGuestModeView object:nil];

    
    [self didLogout:nil];
    
    @synchronized (_roomsMutex) {
        [_rooms removeAllObjects];
    }
    _roomsMutex = nil;
    
    [_presentedLocalNotification removeAllObjects];
    _presentedLocalNotification = nil;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(![ServicesManager sharedInstance].loginManager.isConnected)
        [self didLostConnection:nil];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(![ServicesManager sharedInstance].loginManager.isConnected)
        [self didReconnect:nil];
}

#pragma mark - Login/logout notification

-(void) didLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    
    if([self isViewLoaded])
        [self.tableView reloadData];
    
    [self checkAndEnableAddBubbleButton];
}

-(void) didLogout:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    
    @synchronized (_roomsMutex) {
        [_rooms removeAllObjects];
    }
    
    [self.tableView reloadData];
    
    self.tabBarController.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    _populated = NO;
    _roomRightBarButtonItem.enabled = NO;
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        _roomRightBarButtonItem.enabled = NO;
        [self.tableView reloadData];
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    if([self isViewLoaded]) {
        [self checkAndEnableAddBubbleButton];
        [self.tableView reloadData];
    }
    
}

-(void) didEndPopulatingMyNetwork:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didEndPopulatingMyNetwork:notification];
        });
        return;
    }
    
    NSLog(@"[UIRoomsTableViewController] didEndPopulatingMyNetwork notifications");
    _populated = YES;
    [self checkAndEnableAddBubbleButton];
}

#pragma mark - network lost & guest mode notifications
-(void) didChangeNetworkLostViewFrame:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
}

-(void) willShowNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant += kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

-(void) didHideNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant -= kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = YES;
        }];
    }
}

-(void) didHideGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _guestBannerViewAsBeenScrolled){
        self.topConstraint.constant -= kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:nil];
    }
}

-(void) willShowGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_guestBannerViewAsBeenScrolled){
        self.topConstraint.constant += kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

-(void) handleLocalNotification:(NSNotification *) notification {
    // TODO: Implement join/decline from local notification
}

#pragma mark - Conversation notifications
-(void) didStartConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didStartConversation:notification];
        });
        return;
    }
    
    // We don't want to open the conversation view if the application is in background, case of message send from a push notification.
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
        return;
    
    if(!self.isVisible)
        return;
    
    Conversation *theConversation = (Conversation *)notification.object;
    [RecentsConversationsTableViewController openConversationViewForConversation:theConversation inViewController:self.navigationController];
}

# pragma mark - Rooms service notifications
-(void) didAddRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddRoom:notification];
        });
        return;
    }
    
    Room *theRoom = (Room *) notification.object;
    @synchronized (_roomsMutex) {
        if(![_rooms containsObject:theRoom]){
            [_rooms addObject:theRoom];
            [_rooms reloadData];
        }
    }
    if([self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didUpdateRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateRoom:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];

    if([changedKeys containsObject:@"name"] || [changedKeys containsObject:@"myStatusInRoom"] || [changedKeys containsObject:@"topic"] || [changedKeys containsObject:@"users"] || [changedKeys containsObject:@"creator"]){
        [_rooms reloadData];
        if([self isViewLoaded])
            [self.tableView reloadData];
    }
}

-(void) didRemoveRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveRoom:notification];
        });
        return;
    }
    
    Room *theRoom = (Room *) notification.object;
    @synchronized (_roomsMutex) {
        if([_rooms containsObject:theRoom]){
            [_rooms removeObject:theRoom];
            [_rooms reloadData];
        }
    }
    if([self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didFailRemoveRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didFailRemoveRoom:notification];
        });
        return;
    }
    Room *theRoom = (Room *) notification.object;
    NSString *errorMessage = [NSString stringWithFormat:NSLocalizedString(@"Failed to remove %@ 's bubble",nil),[theRoom displayName]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [UITools showHUDWithMessage:errorMessage forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Failed" dismissCompletionBlock:nil];
    });
}

-(void) didRemoveAllRooms:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveAllRooms:notification];
        });
        return;
    }
    
    @synchronized (_roomsMutex) {
        [_rooms removeAllObjects];
    }
    if([self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didReceiveRoomInvitation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReceiveRoomInvitation:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        [_rooms reloadData];
        [self.tableView reloadData];
    }

    // Show notification if application in background.
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        NSLog(@"Room invitation received and application is in foreground so play a sound");
        [JSQSystemSoundPlayer jsq_playMessageReceivedAlert];
    }
}

-(void) didRoomInvitationStatusChanged:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRoomInvitationStatusChanged:notification];
        });
        return;
    }
    
    Room *room = notification.object;
    
    if(room.myStatusInRoom == ParticipantStatusAccepted){
        [UITools showHUDWithMessage:NSLocalizedString(@"Accepted", nil) forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Checkmark" dismissCompletionBlock:nil];
    }
    
    if([self isViewLoaded]){
        [_rooms reloadData];
        [self.tableView reloadData];
    }
    
    __block UILocalNotification *notifSent = nil;
    [_presentedLocalNotification enumerateObjectsUsingBlock:^(UILocalNotification *localNotification, NSUInteger idx, BOOL * stop) {
        if([[localNotification.userInfo valueForKey:@"from"] isEqualToString:room.jid]) {
            notifSent = localNotification;
            *stop = YES;
        }
    }];
    if(notifSent){
        [[UIApplication sharedApplication] cancelLocalNotification:notifSent];
        [_presentedLocalNotification removeObject:notifSent];
    }
}

#pragma mark - View life cycle

-(void) viewDidLoad {
    [super viewDidLoad];
    
    _serviceManager = [ServicesManager sharedInstance];
    _roomsService = _serviceManager.roomsService;
    _conversationManagerService = _serviceManager.conversationsManagerService;
    
    ((MyInfoNavigationItem*)self.navigationItem).parentViewController = self;
    ((MyInfoNavigationItem*)self.navigationItem).customRightItems = @[_roomRightBarButtonItem];
    
    self.definesPresentationContext = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    self.title = NSLocalizedString(@"Bubbles", nil);
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView setBackgroundColor:[UITools defaultBackgroundColor]];
    
    if ([UITools isForceTouchAvailableForTraitCollection:self.traitCollection]) {
        self.previewingContext = [self registerForPreviewingWithDelegate:self sourceView:self.view];
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
    
    [_roomsFilter setItems:@[NSLocalizedString(@"All", nil), NSLocalizedString(@"My bubbles", nil), NSLocalizedString(@"Archives", nil)]];
    _roomsFilter.delegate = self;
    _roomsFilter.showsCount = NO;
    _roomsFilter.autoAdjustSelectionIndicatorWidth = NO;
    _roomsFilter.height = 60;
    _roomsFilter.selectionIndicatorHeight = 4.0f;
    
    [_roomsFilter addTarget:self action:@selector(roomsFilterValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    CGRect frameTab = _roomsFilter.frame;
    frameTab.size.width = self.view.frame.size.width;
    _roomsFilter.frame = frameTab;
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, _roomsFilter.height, 0);
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if([_rooms objectsInSection:kRoomInvitationsSection].count > 0)
        _roomsFilter.selectedSegmentIndex = 0;
    
    [_rooms reloadData];
    [self.tableView reloadData];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)roomsFilterValueChanged:(UISegmentedControl *)sender {
    
    if(sender.selectedSegmentIndex == SelectorTypeAll)
        _rooms.globalFilteringPredicate = _globalFilterAll;
    if(sender.selectedSegmentIndex == SelectorTypeMyRooms)
        _rooms.globalFilteringPredicate = _globalFilterMyRooms;
    if(sender.selectedSegmentIndex == SelectorTypeLeftRooms)
        _rooms.globalFilteringPredicate = _globalFilterArchived;
    
    [_rooms reloadData];
    [self.tableView reloadData];
}

+(BOOL) hasAcceptedParticipantsInRoom:(Room *) room {
    __block BOOL foundAcceptedParticipant = NO;
    [room.participants enumerateObjectsUsingBlock:^(Participant * participant, NSUInteger idx, BOOL * stop) {
        if(participant.status == ParticipantStatusAccepted && ![participant.contact isEqual:room.creator]){
            foundAcceptedParticipant = YES;
            *stop = YES;
        }
    }];
    
    return foundAcceptedParticipant;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_rooms sections] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionName = [[_rooms sections] objectAtIndex:section];
    if([_rooms objectsInSection:sectionName].count > 0){
        if([[_rooms sections] count] == 2 || [sectionName isEqualToString:kRoomInvitationsSection])
            return NSLocalizedString(sectionName, nil);
    }
    return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_rooms sections] objectAtIndex:section];
    return [[_rooms objectsInSection:key] count];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
    tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools boldFontName] size:16.0f];
    tableViewHeaderFooterView.textLabel.textColor = [UITools defaultTintColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
    Room *room = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
    
    if (room.myStatusInRoom == ParticipantStatusInvited) {
        UIRoomsInvitationTableViewCell *cell = (UIRoomsInvitationTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kRoomsInvitationTableViewReusableKey forIndexPath:indexPath];
        cell.cellObject = room;
        cell.roomsService = _roomsService;
        return cell;
    }
    
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
        cell.cellObject = room;
    __weak typeof(cell) weakCell = cell;
    cell.cellButtonTapHandler = ^(UIButton *sender) {
        [self showRoomDetails:(Room*)weakCell.cellObject];
    };
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
    Room *room = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
    if (room.myStatusInRoom == ParticipantStatusInvited) {
        [self showRoomDetails:room];
        return;
    }
    
    [_conversationManagerService startConversationWithPeer:room withCompletionHandler:nil];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {    
    NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
    Room *room = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
    if (room.myStatusInRoom == ParticipantStatusInvited) {
        return NO;
    }
    
    return YES;
}

+(void)leaveAction:(Room *)room inController:(UIViewController *) controller completionHandler:(void (^ __nullable)())completionHandler {
    NSString *message = room.isMyRoom?NSLocalizedString(@"This bubble will be deleted. Participants will no longer have access to the bubble.", nil):NSLocalizedString(@"You will leave this bubble and will no longer be able to publish information.", nil);
    
    UIAlertController *confirmDeleteActionSheet = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"leave" label:nil value:nil];
            [[ServicesManager sharedInstance].roomsService leaveRoom:room];
            if(completionHandler)
                completionHandler();
        });
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [confirmDeleteActionSheet addAction:ok];
    [confirmDeleteActionSheet addAction:cancel];
    // show the menu.
    [confirmDeleteActionSheet.view setTintColor:[UITools defaultTintColor]];
    [controller presentViewController:confirmDeleteActionSheet animated:YES completion:nil];
}

+(void)archiveAction:(Room *)room inController:(UIViewController *) controller completionHandler:(void (^ __nullable)())completionHandler {
    NSString *message = room.isMyRoom?NSLocalizedString(@"This bubble will be archived for all participants, who will no longer publish anything.", nil):@"";
    
    UIAlertController *confirmDeleteActionSheet = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"archive" label:nil value:nil];
            [[ServicesManager sharedInstance].roomsService archiveRoom:room];
            if(completionHandler)
                completionHandler();
        });
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [confirmDeleteActionSheet addAction:ok];
    [confirmDeleteActionSheet addAction:cancel];
    // show the menu.
    [confirmDeleteActionSheet.view setTintColor:[UITools defaultTintColor]];
    [controller presentViewController:confirmDeleteActionSheet animated:YES completion:nil];
}

+(void)deleteAction:(Room *)room inController:(UIViewController *) controller completionHandler:(void (^ __nullable)())completionHandler {
    NSString *message = room.isMyRoom?NSLocalizedString(@"This bubble will be permanently deleted for all participants, who will no longer have access.", nil):NSLocalizedString(@"Bubble will be removed for your archiving list. You will no longer have access to the information contained in this bubble.", nil);
    
    UIAlertController *confirmDeleteActionSheet = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        if(room.isMyRoom){
            [[ServicesManager sharedInstance].roomsService deleteRoom:room];
            if(completionHandler)
                completionHandler();
        } else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                [[ServicesManager sharedInstance].roomsService deleteParticipant:[room participantFromContact:[ServicesManager sharedInstance].myUser.contact] fromRoom:room];
                if(completionHandler)
                    completionHandler();
            });
        }
        
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"delete" label:nil value:nil];

    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [confirmDeleteActionSheet addAction:ok];
    [confirmDeleteActionSheet addAction:cancel];
    // show the menu.
    [confirmDeleteActionSheet.view setTintColor:[UITools defaultTintColor]];
    [controller presentViewController:confirmDeleteActionSheet animated:YES completion:nil];
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
    Room *room = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
    // The leave button
    BGTableViewRowActionWithImage *leave = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Quit", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"LeaveBubble"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [[self class] leaveAction:room inController:self completionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.editing = NO;
            });
        }];
    }];
    
    
    // The Archive button
    BGTableViewRowActionWithImage *archive = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Archive", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"ArchiveBubble"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [[self class] archiveAction:room inController:self completionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.editing = NO;
            });
        }];
    }];
    
    // The delete button
    BGTableViewRowActionWithImage *delete = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Remove", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"DeleteBubble"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [[self class] deleteAction:room inController:self completionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.editing = NO;
            });
        }];
    }];
    
    if(_roomsFilter.selectedSegmentIndex == 0) {
        return room.isMyRoom ? @[archive] : @[leave];
    } else if(_roomsFilter.selectedSegmentIndex == 1){
        return room.isMyRoom ? @[archive]:@[];
    } else {
        return @[delete];
    }
}

#pragma mark - DZNEmptyDataSet
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"EmptyBubbles"];
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultTintColor];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSMutableString *text = [[NSMutableString alloc] init];
    
    if(_roomsFilter.selectedSegmentIndex == 2) {
        [text appendString:NSLocalizedString(@"You have no archived bubbles.", nil)];
        [text appendString:@"\n"];
    }
    [text appendString:NSLocalizedString(@"Create, share and work together with Bubbles.", nil)];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"Invite Rainbow users in your Bubbles and start team conversations.", nil);
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Create a bubble", nil) attributes:attributes];
}

-(UIImage *) buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    // Change button color for guest user
    if ([ServicesManager sharedInstance].myUser.isGuest || !_roomRightBarButtonItem.enabled) {
        return [UITools imageWithColor:[UITools colorFromHexa:dznButtonDisabledColor] size:CGSizeMake(1, 40)];
    } else {
        return [UITools imageWithColor:[UITools defaultTintColor] size:CGSizeMake(1, 40)];
    }
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    CGFloat offset = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
    offset += CGRectGetHeight(self.navigationController.navigationBar.frame);
    offset -= CGRectGetHeight(_roomsFilter.frame);
    return -offset;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 0.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    // Disable interaction if user is guest
    if ([ServicesManager sharedInstance].myUser.isGuest ||!_roomRightBarButtonItem.enabled) {
        return NO;
    }
    else
    return  YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    [self didTapOnAddButton:nil];
}
- (void) presentContactDetailController:(Contact*) contact{
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 3D touch actions
- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
    [super traitCollectionDidChange:previousTraitCollection];
    if ([UITools isForceTouchAvailableForTraitCollection:self.traitCollection]) {
        if (!self.previewingContext) {
            self.previewingContext = [self registerForPreviewingWithDelegate:self sourceView:self.view];
        }
    } else {
        if (self.previewingContext) {
            [self unregisterForPreviewingWithContext:self.previewingContext];
            self.previewingContext = nil;
        }
    }
}


- (UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location {
    
    if ([self.presentedViewController isKindOfClass:[UIRoomDetailsViewController class]]) {
        return nil;
    }
    
    NSIndexPath *idx = [self.tableView indexPathForRowAtPoint:location];
    Room *theRoom = nil;
    if(idx){
        NSString *key = [[_rooms sections] objectAtIndex:idx.section];
        theRoom = [[_rooms objectsInSection:key] objectAtIndex:idx.row];
        UITableViewCell *tableCell = [self.tableView cellForRowAtIndexPath:idx];
        
        UIRoomDetailsViewController * roomDetails = [[UIStoryboardManager sharedInstance].roomDetailsStoryBoard instantiateViewControllerWithIdentifier:@"roomDetailsViewControllerID"];
        roomDetails.room = theRoom;

        CustomNavigationController *navCtrl = [[CustomNavigationController alloc] initWithRootViewController:roomDetails];
        
        previewingContext.sourceRect = [self.view convertRect:tableCell.frame fromView:self.tableView];
        
        return navCtrl;
    }
    return nil;
}

- (void)previewingContext:(id<UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit {
    if([viewControllerToCommit isKindOfClass:[CustomNavigationController class]]){
        [self.navigationController pushViewController:((CustomNavigationController*)viewControllerToCommit).topViewController animated:YES];
    }
}

#pragma mark - show room details
-(void) showRoomDetails:(Room *) room {
    UIRoomDetailsViewController *roomDetailsViewController = [[UIStoryboardManager sharedInstance].roomDetailsStoryBoard instantiateViewControllerWithIdentifier:@"roomDetailsViewControllerID"];
    roomDetailsViewController.room = room;
    roomDetailsViewController.fromView = self;
    [self.navigationController pushViewController:roomDetailsViewController animated:YES];
}

- (IBAction)didTapOnAddButton:(UIBarButtonItem *)sender {
    
     UIGenericBubbleDetailsViewController *addAttendeeViewController = [[UIStoryboardManager sharedInstance].addAttendeesStoryBoard instantiateViewControllerWithIdentifier:@"addAttendeeViewControllerID"];
    
//    addAttendeeViewController.preferredContentSize = CGSizeMake(300, 300);
//    addAttendeeViewController.modalPresentationStyle = UIModalPresentationPopover;
//    
//    UIPopoverPresentationController *popoverCtrl = addAttendeeViewController.popoverPresentationController;
//    popoverCtrl.delegate = self;
//    popoverCtrl.sourceView = self.view;
//    [popoverCtrl setPermittedArrowDirections:UIPopoverArrowDirectionUnknown];
//    popoverCtrl.backgroundColor = [UITools defaultTintColor];
    [self presentViewController:addAttendeeViewController animated:YES completion:nil];
}

-(void) checkAndEnableAddBubbleButton {
    UIScrollView *scrollView = [[UIScrollView alloc]init];
    [self emptyDataSetShouldAllowTouch:scrollView];
    [self buttonBackgroundImageForEmptyDataSet:scrollView forState:UIControlStateNormal];
    if([ServicesManager sharedInstance].myUser.isGuest){
        _roomRightBarButtonItem.enabled = NO;
    } else if(_populated == YES) {
        _roomRightBarButtonItem.enabled = YES;
    }
}

-(void) dumpBubbles {
    NSLog(@"DUMP BUBBLES : %@", [_rooms description]);
}

-(void) scrollToTop {
    [self.tableView setContentOffset:CGPointMake(0.0f, -self.tableView.contentInset.top) animated:YES];
}
@end
