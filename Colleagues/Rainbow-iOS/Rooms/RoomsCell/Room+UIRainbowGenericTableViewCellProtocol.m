/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Room+UIRainbowGenericTableViewCellProtocol.h"
#import "UITools.h"
#import <Rainbow/Rainbow.h>
#import "Room+Extensions.h"

@implementation Room (UIRainbowGenericTableViewCellProtocol)
-(NSString *) mainLabel {
    return self.displayName;
}

-(UIFont *)mainLabelFont {
    if(self.conference && self.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio)
        return [UIFont fontWithName:[UITools boldFontName] size:16];
    return [UIFont fontWithName:[UITools defaultFontName] size:16];
}

-(UIColor *)mainLabelTextColor {
    if(self.conference && self.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio)
        return [UITools defaultTintColor];
    return [UIColor blackColor];
}

-(NSString *) subLabel {
    if(self.conference){
        if (self.conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC){
            if(self.conference.endedConference)
                return self.topic;
            return NSLocalizedString(@"Video conference", nil);
        }
        NSString *organizerName = [NSString stringWithFormat:NSLocalizedString(@"Organizer: %@", nil),self.isMyRoom?NSLocalizedString(@"Me",nil):self.creator.displayName];
        return organizerName;
    }
    return self.topic;
}

-(NSDate *) date {
    return nil;
}

-(NSString *) bottomRightLabel {
    return nil;
}

-(Peer *) avatar {
    return (Peer*)self;
}

-(NSArray *) observablesKeyPath {
    return nil;
}

-(NSInteger) badgeValue {
    return 0;
}

-(NSString *) cellButtonTitle {
    return nil;
}

-(UIImage *) cellButtonImage {
   
    if(self.conference && self.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
        if(![[ServicesManager sharedInstance].conferencesManagerService hasMyUserJoinedConference:self.conference]){
            if(self.canJoin)
                return [UIImage imageNamed:@"Phone"];
            else
                return nil;
        } else {
            if(self.isMyRoom &&
               self.conference.myConferenceParticipant &&
               self.conference.myConferenceParticipant.state != ParticipantStateDisconnected)
                return [UIImage imageNamed:@"Hangup"];
        }
        return nil;
    } else
        return [UIImage imageNamed:@"Vcard"];
}

-(UIImage *) rightIcon {
    return nil;
}

-(UIColor *) rightIconTintColor {
    return nil;
}

-(NSString *)midLabel {
    if(self.conference){
        if(self.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
            if(self.conference.type == ConferenceTypeInstant)
                return NSLocalizedString(@"Instant meeting", nil);
            if(self.conference.type == ConferenceTypeScheduled){
                NSString *dateStr = [UITools formatDateTimeForDate:self.conference.start];
                if(self.conference.end && dateStr){
                    if([self.conference.end isEqualToDateIgnoringTime:self.conference.start])
                        dateStr = [NSString stringWithFormat:@"%@ - %@", dateStr, [UITools formatTimeForDate:self.conference.end]];
                    else
                        dateStr = [NSString stringWithFormat:@"%@ - %@", dateStr, [UITools formatDateTimeForDate:self.conference.end]];
                }
                
                return dateStr;
            }
        } else if (self.conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC){
            return NSLocalizedString(@"Video conference", nil);
        }
    } else
        return nil;
    return nil;
}

-(UIColor *)midLabelTextColor {
    return [UITools defaultTintColor];
}
@end
