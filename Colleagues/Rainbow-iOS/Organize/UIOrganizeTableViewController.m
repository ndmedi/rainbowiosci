//
//  UIOrganizeTableViewController.m
//  Rainbow-iOS
//
//  Created by AsalTech on 3/13/18.
//  Copyright © 2018 ALE International. All rights reserved.
//

#import "UIOrganizeTableViewController.h"
#import "UITools.h"


@interface UIOrganizeTableViewController ()<UIGestureRecognizerDelegate>


@end

@implementation UIOrganizeTableViewController

#pragma mark - Application LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.sectionHeaderHeight = 30;
    self.tableView.tableFooterView = [UIView new];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandler:)];
    [self.tableView addGestureRecognizer:tap];

    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) // Sort Array
        return self.sortArray.count;
    if(section == 1)// Filter Array
        return self.filterArray.count;
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return 44;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0)
        return NSLocalizedString(@"Sort order", nil);
    if(section == 1)
        return NSLocalizedString(@"Filter", nil);
    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellIdentifier"];
    }

    if(indexPath.section == 0) {
        cell.textLabel.text = [self.sortArray objectAtIndex:indexPath.row];
        cell.accessoryView = nil;
        if(self.selectedSortIndex  == indexPath.row){

            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark_status"]];
            cell.accessoryView.tintColor = [UITools defaultTintColor];
        }
    } else if (indexPath.section == 1) {
        cell.textLabel.text = [self.filterArray objectAtIndex:indexPath.row];
        cell.accessoryView = nil;

        if(self.selectedFilterIndex == indexPath.row) {

            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark_status"]];
            cell.accessoryView.tintColor = [UITools defaultTintColor];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [UITools applyCustomFontTo:cell.textLabel];
    return cell;

}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {

   UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
   tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:14.0f];
   tableViewHeaderFooterView.textLabel.textColor = [UITools defaultTintColor];
   tableViewHeaderFooterView.backgroundView.backgroundColor = [UITools defaultBackgroundColor];
   return;
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section==0){

        self.selectedSortIndex = indexPath.row;

        if([self.delegate respondsToSelector:@selector(willSortBySortOrder:)])
            [self.delegate willSortBySortOrder:indexPath.row];
    }
    else{

        self.selectedFilterIndex = indexPath.row;

        if([self.delegate respondsToSelector:@selector(willFilterByFilterType:)])
            [self.delegate willFilterByFilterType:indexPath.row];
        
    }
    
    [tableView reloadData];

  if (indexPath.section == 1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if(_isSubView) {
            [self.view removeFromSuperview];        
            }
            else
            [self dismissViewControllerAnimated:YES completion:nil];

        });
    }
}

- (void)gestureHandler:(UITapGestureRecognizer *)tap
{
    CGPoint location = [tap locationInView:self.tableView];
    NSIndexPath *path = [self.tableView indexPathForRowAtPoint:location];
    if(!path)
    {

        if(_isSubView) {
            [self.view removeFromSuperview];
        }
        else
            [self dismissViewControllerAnimated:YES completion:nil];
        
        if([self.delegate respondsToSelector:@selector(didFooterPressed)])
            [self.delegate didFooterPressed];
       


    }
    else {
        [self tableView:self.tableView didSelectRowAtIndexPath:path];
    }
}


-(void) setViewHorizontalPosition:(CGFloat) horizontalPosition {
    CGRect frame = self.view.frame;
    frame.origin.y += horizontalPosition;
    self.view.frame = frame;
    NSLog(@"NEW VIEW FRAME %@", NSStringFromCGRect(self.view.frame));

}

@end
