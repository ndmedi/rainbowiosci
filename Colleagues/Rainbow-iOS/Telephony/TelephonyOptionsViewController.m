/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import <Rainbow/ServicesManager.h>
#import <Rainbow/TelephonyService.h>
#import "TelephonyOptionsViewController.h"
#import "EditVcardViewController.h"
#import "UIStoryboardManager.h"
#import "UITools.h"


@interface TelephonyOptionsViewController()
@property (weak, nonatomic) IBOutlet UIView *nomadicView;
@property (weak, nonatomic) IBOutlet UILabel *nomadicLabel;
@property (weak, nonatomic) IBOutlet UILabel *nomadicSwitchLabel;
@property (weak, nonatomic) IBOutlet UISwitch *nomadicSwitch;
@property (weak, nonatomic) IBOutlet UILabel *nomadicNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *nomadicNumberButton;
@property (weak, nonatomic) IBOutlet UITextField *nomadicNumberTextfield;
@property (weak, nonatomic) IBOutlet UILabel *nomadicDescription;
@property (weak, nonatomic) IBOutlet UIImageView *nomadicNumberArrowImage;

@property (weak, nonatomic) IBOutlet UIView *nomadicVoipView;
@property (weak, nonatomic) IBOutlet UILabel *nomadicVoipSwitchLabel;
@property (weak, nonatomic) IBOutlet UISwitch *nomadicVoipSwitch;

@property (weak, nonatomic) IBOutlet UIView *forwardView;
@property (weak, nonatomic) IBOutlet UILabel *forwardLabel;
@property (weak, nonatomic) IBOutlet UIButton *forwardButton;
@property (weak, nonatomic) IBOutlet UILabel *forwardDescription;
@property (weak, nonatomic) IBOutlet UIImageView *forwardButtonArrowImage;
@property (weak, nonatomic) IBOutlet UILabel *myMobileNumberDescription;
@property (weak, nonatomic) IBOutlet UILabel *nomadicVoipSwitchDescription;
@end

@implementation TelephonyOptionsViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateMyContact:) name:kContactsManagerServiceDidUpdateMyContact object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateNomadicStatus:) name:kMyUserNomadicStatusDidUpdate object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateCallForwardStatus:) name:kMyUserCallForwardDidUpdate object:nil];
    
    self.title = NSLocalizedString(@"Telephony", nil);
    self.navigationController.navigationBar.translucent = NO;
    
    // NOMADIC
    [UITools applyCustomFontTo:_nomadicLabel];
    [UITools applyCustomFontTo:_nomadicSwitchLabel];
    [UITools applyCustomFontTo:_nomadicNumberButton.titleLabel];
    [UITools applyCustomFontTo:_nomadicDescription];
    [UITools applyCustomFontTo:_nomadicVoipSwitchLabel];
    [UITools applyCustomFontTo:_myMobileNumberDescription];
    [UITools applyCustomFontTo:_nomadicVoipSwitchDescription];
    
    [_nomadicLabel setText:NSLocalizedString(@"Decide where to receive your phone calls", nil)];
    [_nomadicSwitchLabel setText:NSLocalizedString(@"Use my mobile phone for business calls", nil)];
    [_nomadicSwitch setOn:NO];
    [_nomadicSwitch setOnTintColor:[UITools defaultTintColor]];
    [_nomadicNumberButton setTitle:NSLocalizedString(@"My mobile number", nil) forState:UIControlStateNormal];
    [_nomadicDescription setText:NSLocalizedString(@"Make and receive business calls through your enterprise communications system (PBX). If option is deactivated, you will no longer pass through your enterprise communications system.", nil)];
    _nomadicNumberTextfield.text = @"";
    _nomadicNumberTextfield.placeholder = NSLocalizedString(@"No phone number", nil);
    _nomadicNumberArrowImage.tintColor = [UIColor blackColor];
    [_nomadicVoipSwitchLabel setText:NSLocalizedString(@"Activate VoIP for business calls", nil)];
    [_nomadicVoipSwitch setOnTintColor:[UITools defaultTintColor]];
    [_myMobileNumberDescription setText:NSLocalizedString(@"Enter your business mobile phone number (if not defined).", nil)];
    [_nomadicVoipSwitchDescription setText:NSLocalizedString(@"Make and receive business calls over the Internet (VoIP) through your enterprise communications system (PBX).", nil)];
    
    [self initializeNomadicValues];
    
    // CALL FORWARD
    [UITools applyCustomFontTo:_forwardLabel];
    [UITools applyCustomFontTo:_forwardDescription];
    [UITools applyCustomFontTo:_forwardButton.titleLabel];
    
    [_forwardLabel setText:NSLocalizedString(@"Forward your phone calls", nil)];
    [_forwardDescription setText:NSLocalizedString(@"Redirect phone calls to your voicemail or another phone number, when you are out of office or unavailable.", nil)];
    [_forwardButton setTitle:NSLocalizedString(@"Do not forward calls", nil) forState:UIControlStateNormal];
    _forwardButtonArrowImage.tintColor = [UIColor blackColor];
    
    [self initializeForwardValues];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateMyContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserNomadicStatusDidUpdate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserCallForwardDidUpdate object:nil];
}

-(void) didUpdateMyContact:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateMyContact:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Contact *contact = [userInfo objectForKey:kContactKey];
    if([contact isEqual:[ServicesManager sharedInstance].myUser.contact]){
        if([self isViewLoaded]){
            [self initializeNomadicValues];
            [self initializeForwardValues];
        }
    }
}

-(void) didUpdateNomadicStatus:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateNomadicStatus:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        [self initializeNomadicValues];
        [self initializeForwardValues];
    }
}

-(void) didUpdateCallForwardStatus:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateCallForwardStatus:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        [self initializeForwardValues];
    }
}

-(void) changeNomadicViewVisibility: (BOOL)visible {
    [_nomadicView setHidden:!visible];
}

-(void) changeNomadicVoipViewVisibility: (BOOL) visible {
    [_nomadicVoipView setHidden:!visible];
}

-(void) initializeNomadicValues {
    MyUser *myUser = [ServicesManager sharedInstance].myUser;
    // Define the VOIP for nomadic visibility
    [self changeNomadicVoipViewVisibility: myUser.mediaPillarStatus.jid && myUser.mediaPillarStatus.featureActivated];
    // Define the Nomadic options visibility
    [self changeNomadicViewVisibility: myUser.isAllowedToUseTelephonyNomadicMode && myUser.nomadicStatus.featureActivated];

    [_nomadicSwitch setOn:NO]; // init to OFF nomadic status
    [_nomadicVoipSwitch setOn:NO]; // init Voip to OFF
    _nomadicNumberTextfield.text = @"";
    
    PhoneNumber *workMobileNumber = [myUser.contact getPhoneNumberOfType:PhoneNumberTypeWork withDeviceType:PhoneNumberDeviceTypeMobile];
    PhoneNumber *personalMobileNumber = [myUser.contact getPhoneNumberOfType:PhoneNumberTypeHome withDeviceType:PhoneNumberDeviceTypeMobile];
    
    NSMutableArray <PhoneNumber *> *validNumbers = [NSMutableArray array];
    if(workMobileNumber)
        [validNumbers addObject:workMobileNumber];
    if(personalMobileNumber)
        [validNumbers addObject:personalMobileNumber];
    if(myUser.isAllowedToUseTelephonyWebRTCtoPSTN){
        // TODO: Add the mediaPillarNumber
    }
    
    if(myUser.isNomadicModeActivated && ([validNumbers containsObject:myUser.nomadicStatus.destination])){
        _nomadicSwitch.on = YES;
        PhoneNumber *pnToDisplay = [validNumbers objectAtIndex:[validNumbers indexOfObject:myUser.nomadicStatus.destination]];
        _nomadicNumberTextfield.text = pnToDisplay.number;
    }
    
    if(myUser.isAllowedToUseTelephonyWebRTCtoPSTN){
        if(myUser.isNomadicModeActivated && myUser.mediaPillarStatus.activated && myUser.mediaPillarStatus.featureActivated){
            [_nomadicSwitch setOn:YES];
            [_nomadicVoipSwitch setOn: myUser.mediaPillarStatus.activated];
        }
    }

    if(_nomadicNumberTextfield.text.length == 0){
        if(workMobileNumber)
            _nomadicNumberTextfield.text = workMobileNumber.number;
        if(!workMobileNumber && personalMobileNumber)
            _nomadicNumberTextfield.text = personalMobileNumber.number;
    }
}

- (IBAction)tappedNomadicNumberButton:(UIButton *)sender {
    EditVcardViewController *pwdView = (EditVcardViewController *)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"editVcardView"];
    [self.navigationController pushViewController:pwdView animated:YES];
}

-(void) initializeForwardValues {
    MyUser *myUser = [ServicesManager sharedInstance].myUser;
    [_forwardView setHidden: !myUser.isAllowedToUseTelephonyCallForward];
    
    if(![_forwardView isHidden]) {
        NSString *buttonLabel = NSLocalizedString(@"Do not forward calls", nil);
        
        if(myUser.callForwardStatus) {
            if(myUser.callForwardStatus.type == CallForwardTypeVoicemail)
                buttonLabel = NSLocalizedString(@"Voicemail", nil);
            if(myUser.callForwardStatus.type == CallForwardTypePhoneNumber) {
                if(myUser.callForwardStatus.destination != nil)
                    buttonLabel = myUser.callForwardStatus.destination.numberE164;
                else
                    buttonLabel = NSLocalizedString(@"Other number", nil);
            }
        }
        
        [_forwardButton setTitle:buttonLabel forState:UIControlStateNormal];
    }
}

- (IBAction)tappedChangeButton:(UIButton *)sender {
    if([_nomadicNumberTextfield.text length] == 0 && _nomadicSwitch.isOn && !_nomadicVoipSwitch.isOn)
        return;
    if([_nomadicView isHidden]) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    // display a loading spinner
    __block MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Saving", nil);
    hud.removeFromSuperViewOnHide = YES;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    
    // Server request
    if(_nomadicSwitch.isOn) {
        [[ServicesManager sharedInstance].telephonyService nomadicLoginWithPhoneString:_nomadicNumberTextfield.text withVoIP:_nomadicVoipSwitch.isOn withCompletionBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error) {
                    [hud hide:YES];
                    [self treatChangeResponseError:error];
                } else {
                    // Modification succeed. Return to previous view
                    hud.mode = MBProgressHUDModeCustomView;
                    hud.labelText = NSLocalizedString(@"Saved", nil);
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
            });
        }];
    }
    
    else {
        [[ServicesManager sharedInstance].telephonyService nomadicLogoutWithCompletionBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error) {
                    [hud hide:YES];
                    [self treatChangeResponseError:error];
                } else {
                    // Modification succeed. Return to previous view
                    hud.mode = MBProgressHUDModeCustomView;
                    hud.labelText = NSLocalizedString(@"Saved", nil);
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
            });
        }];
    }
    
}

-(void) treatChangeResponseError:(NSError *) error {
    NSString *errorTitle = NSLocalizedString(@"Error", nil);
    NSString *errorMessage = NSLocalizedString(@"An unknown error occured", nil);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:errorTitle message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)nomadicSwitchValueChanged:(UISwitch *)sender {
    if(!sender.isOn){
        _nomadicVoipSwitch.on = NO;
    }
}

- (IBAction)voipSwitchValueChanged:(UISwitch *)sender {
    if(sender.isOn && !_nomadicSwitch.isOn){
        _nomadicSwitch.on = YES;
    }
    if(!sender.isOn && _nomadicSwitch.isOn && _nomadicNumberLabel.text.length == 0){
        _nomadicSwitch.on = NO;
    }
}

#pragma mark - Rotation
-(BOOL) shouldAutorotate {
    return NO;
}

@end
