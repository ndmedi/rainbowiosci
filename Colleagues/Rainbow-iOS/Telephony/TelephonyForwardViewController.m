/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "TelephonyForwardViewController.h"
#import <Rainbow/ServicesManager.h>
#import "UITools.h"
#import <Rainbow/MyUser.h>

@interface TelephonyForwardViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *forwardOptionsTableView;
@property (nonatomic) NSIndexPath *checkedCell;
@end

@implementation TelephonyForwardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateCallForwardStatus:) name:kMyUserCallForwardDidUpdate object:nil];
    
    self.title = NSLocalizedString(@"Forward your phone calls", nil);
    self.navigationController.navigationBar.translucent = NO;

    self.forwardOptionsTableView.sectionHeaderHeight = 0.0f;
    self.forwardOptionsTableView.sectionFooterHeight = 0.0f;
    self.forwardOptionsTableView.tableFooterView = [UIView new];
    self.forwardOptionsTableView.tableHeaderView = nil;
    self.forwardOptionsTableView.backgroundColor = [UITools colorFromHexa:0xF5F5F5FF];

}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserCallForwardDidUpdate object:nil];
}

-(void) didUpdateCallForwardStatus:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateCallForwardStatus:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        [self.forwardOptionsTableView reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0)
        return ([ServicesManager sharedInstance].myUser.voiceMailNumber) ? 1 : 0;
    else if(section == 1)
        return 1;
    else if(section == 2)
        return 1;
    else
        return 0;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"forwardOptionCell"];
    NSString *label = @"";
    cell.detailTextLabel.text = @"";
    cell.accessoryType = UITableViewCellAccessoryNone;

    CallForwardType type = CallForwardTypeNotForwarded;
    if([ServicesManager sharedInstance].myUser.callForwardStatus) {
        type = [ServicesManager sharedInstance].myUser.callForwardStatus.type;
    }
    
    if(indexPath.section == 0) {
        label = NSLocalizedString(@"Voicemail", nil);
        if(type == CallForwardTypeVoicemail) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        cell.textLabel.text = label;
    }
    
    else if(indexPath.section == 1) {
        label = NSLocalizedString(@"Other phone", nil);
        if(type == CallForwardTypePhoneNumber) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
            if([ServicesManager sharedInstance].myUser.callForwardStatus && [ServicesManager sharedInstance].myUser.callForwardStatus.destination != nil) {
                cell.detailTextLabel.text = [ServicesManager sharedInstance].myUser.callForwardStatus.destination.numberE164;
            }
        }
        cell.textLabel.text = label;
    }
    
    else if(indexPath.section == 2) {
        label = NSLocalizedString(@"Do not forward calls", nil);
        if(type == CallForwardTypeNotForwarded) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        cell.textLabel.text = label;
    }
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // display a loading spinner
    __block MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Saving", nil);
    hud.removeFromSuperViewOnHide = YES;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    
    // Voicemail
    if(indexPath.section == 0) {
        [[ServicesManager sharedInstance].telephonyService forwardToVoicemailWithCompletionBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error) {
                    [hud hide:YES];
                    [self treatChangeResponseError:error];
                } else {
                    // Modification succeed. Return to previous view
                    hud.mode = MBProgressHUDModeCustomView;
                    hud.labelText = NSLocalizedString(@"Saved", nil);
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
            });
        }];
    }
    
    // Forward to a phone number
    else if(indexPath.section == 1) {
        [hud hide:NO]; // TODO: prompt for a phone number
        
        __block PhoneNumber *currentDestination;
        if([ServicesManager sharedInstance].myUser.callForwardStatus && [ServicesManager sharedInstance].myUser.callForwardStatus.destination != nil) {
            currentDestination = [ServicesManager sharedInstance].myUser.callForwardStatus.destination;
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Other phone", nil) message:@"" preferredStyle:(UIAlertControllerStyleAlert)];
        [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.keyboardType = UIKeyboardTypePhonePad;
            if(currentDestination) {
                textField.text = currentDestination.numberE164;
            }
        }];
        
        [alert addAction: [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
        
        [alert addAction: [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = NSLocalizedString(@"Saving", nil);
            hud.removeFromSuperViewOnHide = YES;
            [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
        
            UITextField *phoneTextfield = [alert.textFields firstObject];
            
            [[ServicesManager sharedInstance].telephonyService forwardToExternalPhoneString: phoneTextfield.text withCompletionBlock:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(error) {
                        [hud hide:YES];
                        [self treatChangeResponseError:error];
                    } else {
                        // Modification succeed. Return to previous view
                        hud.mode = MBProgressHUDModeCustomView;
                        hud.labelText = NSLocalizedString(@"Saved", nil);
                        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:^{
                            [self.navigationController popViewControllerAnimated:YES];
                        }];
                    }
                });
            }];
            
        }]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    // Cancel forward
    else if(indexPath.section == 2) {
        [[ServicesManager sharedInstance].telephonyService cancelForwardWithCompletionBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error) {
                    [hud hide:YES];
                    [self treatChangeResponseError:error];
                } else {
                    // Modification succeed. Return to previous view
                    hud.mode = MBProgressHUDModeCustomView;
                    hud.labelText = NSLocalizedString(@"Saved", nil);
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
            });
        }];
    }
    
    else {
        [hud hide:YES];
    }
    
}

-(void) treatChangeResponseError:(NSError *) error {
    NSString *errorTitle = NSLocalizedString(@"Forward calls", nil);
    NSString *errorMessage = NSLocalizedString(@"Call forwarding cannot be applied", nil);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:errorTitle message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Rotation
-(BOOL) shouldAutorotate {
    return NO;
}

@end
