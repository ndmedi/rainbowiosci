/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RecentsConversationsTableViewController.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/Conversation.h>
#import "UIRainbowGenericTableViewCell.h"
#import "CustomNavigationController.h"
#import "UIMessagesViewController.h"
#import "UIPagingMessagesViewController.h"
#import "BGTableViewRowActionWithImage.h"
#import <Rainbow/defines.h>
#import <Rainbow/ContactsManagerService.h>
#import <Rainbow/Message.h>
#import <Rainbow/Room.h>
#import <Rainbow/Tools.h>
#import <Rainbow/ConversationsManagerService.h>
#import "Contact+Extensions.h"
#import "CustomSearchController.h"
#import "UIViewController+TopViewController.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import "UINotificationManager.h"
#import "UIContactDetailsViewController.h"
#import "UIRoomDetailsViewController.h"
#import "UIGroupDetailsViewController.h"
#import <Rainbow/UIDevice+VersionCheck.h>
#import "UINavigationController+PreviewActions.h"
#import "MyInfoNavigationItem.h"
#import "FilteredSortedSectionedArray.h"
#import "UIStoryboardManager.h"
#import "UIRoomsTableViewController.h"
#import "UIViewController+Visible.h"
#import "UIGuestModeViewController.h"
#import "UINetworkLostViewController.h"
#import "FilteredSortedSectionedArray+ConversationsList.h"
#import "Firebase.h"
#define kActiveCallsSection @"Active calls"
#define kMyConversationsSection @"My conversations"

@interface RecentsConversationsTableViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, UIViewControllerPreviewingDelegate, UISearchResultDisplayDetailControllerProtocol>
@property (nonatomic, strong) ServicesManager *serviceManager;
@property (nonatomic, strong) ContactsManagerService *contactManagerService;
@property (nonatomic, strong) ConversationsManagerService *conversationManagerService;

@property (nonatomic, strong) NSString *lastMessageIdReceiveByPush;
@property (nonatomic, strong) id previewingContext;
@property (nonatomic) BOOL populated;

// Data model
@property (nonatomic, strong) FilteredSortedSectionedArray<Conversation *> *conversations;
@property (nonatomic, strong) NSObject *conversationsMutex;
@property (nonatomic, strong) NSPredicate *globalFilterAll;
@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (nonatomic) BOOL lostNetworkViewAsBeenScrolled;
@property (nonatomic) BOOL guestBannerViewAsBeenScrolled;
@property (nonatomic) BOOL isNetworkConnected;
@property (nonatomic) BOOL isAbleToRefreshUI;
@end

@implementation RecentsConversationsTableViewController
-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndLoadingConversations:) name:kConversationsManagerDidEndLoadingConversations object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddConversation:) name:kConversationsManagerDidAddConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveConversation:) name:kConversationsManagerDidRemoveConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveAllConversations:) name:kConversationsManagerDidRemoveAllConversations object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConversation:) name:kConversationsManagerDidUpdateConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStartConversation:) name:kConversationsManagerDidStartConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStopConversation:) name:kConversationsManagerDidStopConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNewMessageForConversation:) name:kConversationsManagerDidReceiveNewMessageForConversation object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLocalNotification:) name:kNotificationsManagerHandleNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContactDisplaySettings:) name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLocalNotification:) name:UIApplicationDidReceiveLocalNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConference:) name:kConferencesManagerDidUpdateConference object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dumpConversations) name:@"dumpConversations" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRoom:) name:kRoomsServiceDidRemoveRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowNetworkLostView:) name:CustomNavigationControllerWillShowNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNetworkLostViewFrame:) name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowGuestModeView:) name:CustomNavigationControllerWillShowGuestModeView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideGuestModeView:) name:CustomNavigationControllerDidHideGuestModeView object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isAbleToRefreshUIChanged:) name:kIsAbleToRefreshUIChanged object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endReceiveXMPPRequestsAfterResume:) name:kEndReceiveXMPPRequestsAfterResume object:nil];
        
        _sectionedByName = ^NSString*(Conversation *conversation) {
            if(conversation.hasActiveCall)
                return kActiveCallsSection;
            if([conversation.peer isKindOfClass:[Room class]]){

                Conference *conf = ((Room*)conversation.peer).conference;
                
                if ([((Room*)conversation.peer) isWebRTCConferenceRoom] && conf.myConferenceParticipant.state == ParticipantStateConnected)
                    return kActiveCallsSection;
                
               else if ([((Room*)conversation.peer) isPGIConferenceRoom] && conf.myConferenceParticipant.state == ParticipantStateConnected && conf.isActive)
                    return kActiveCallsSection;
            }
            return kMyConversationsSection;
        };
        
        _globalFilterAll = [NSPredicate predicateWithBlock:^BOOL(Conversation *conversation, NSDictionary<NSString *,id> * bindings) {
            if([conversation.peer isKindOfClass:[Room class]]){
                if([((Room *)conversation.peer).topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                    return NO;
            }
            return YES;
        }];
        
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            if([obj1 isEqualToString:kActiveCallsSection])
                return NSOrderedAscending;
            if([obj2 isEqualToString:kMyConversationsSection])
                return NSOrderedDescending;
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];

        }];
        
        _conversations = [FilteredSortedSectionedArray new];
        _conversationsMutex = [NSObject new];
        
        
        // Default filters/sorter/.. values
        _conversations.sectionNameFromObjectComputationBlock = _sectionedByName;
        _conversations.globalFilteringPredicate = _globalFilterAll;
        _conversations.sectionSortDescriptor = _sortSectionAsc;
        
        // __default__ has a special meaning : any other sections not found in the dictionary
        _conversations.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByLastMessageDate]]};
        
        _serviceManager = [ServicesManager sharedInstance];
        _contactManagerService = _serviceManager.contactsManagerService;
        _conversationManagerService = _serviceManager.conversationsManagerService;
        
        _populated = NO;
        _isAbleToRefreshUI = YES;
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidEndLoadingConversations object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidAddConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidRemoveConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidRemoveAllConversations object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidUpdateConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidStartConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidStopConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidReceiveNewMessageForConversation object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationsManagerHandleNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidReceiveLocalNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConferencesManagerDidUpdateConference object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dumpConversations" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideGuestModeView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowGuestModeView object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kIsAbleToRefreshUIChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kEndReceiveXMPPRequestsAfterResume object:nil];
    
    [self didLogout:nil];
    
    @synchronized (_conversationsMutex) {
        [_conversations removeAllObjects];
    }
    
    _conversationsMutex = nil;
    
    _contactManagerService = nil;
    _conversationManagerService = nil;
    _serviceManager = nil;
}

#pragma mark - Logout notification

-(void) didLogin:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    _serviceManager = [ServicesManager sharedInstance];
    _contactManagerService = _serviceManager.contactsManagerService;
    _conversationManagerService = _serviceManager.conversationsManagerService;
    _populated = NO; // Display only the empty message if the fetch of all conversations returns nothing (keep white screen even if the cache is empty)
    
    if (_isAbleToRefreshUI && [self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didLogout:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    
    _populated = NO;
    
    @synchronized (_conversationsMutex) {
        [_conversations removeAllObjects];
    }
    
    if (_isAbleToRefreshUI && [self isViewLoaded])
        [self.tableView reloadData];
    
    self.tabBarController.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
    }
}

-(void) didChangeNetworkLostViewFrame:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
}

-(void) willShowNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant += kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

-(void) didHideNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant -= kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = YES;
        }];
    }
}

-(void) didHideGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _guestBannerViewAsBeenScrolled){
        self.topConstraint.constant -= kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:nil];
    }
}

-(void) willShowGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_guestBannerViewAsBeenScrolled){
        self.topConstraint.constant += kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

-(void) didRemoveAllMessagesFromConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveConversation:notification];
        });
        return;
    }
    
    Conversation *theConversation = (Conversation *) notification.object;
    @synchronized (_conversationsMutex) {
        if([_conversations containsObject:theConversation]){
            [_conversationManagerService markAsReadByMeAllMessageForConversation:theConversation];
            [_conversationManagerService stopConversation:theConversation];
        }
    }
    
    if (_isAbleToRefreshUI && [self isViewLoaded])
        [self.tableView reloadData];
}

-(void) changeContactDisplaySettings:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self changeContactDisplaySettings:notification];
        });
        return;
    }
    if (_isAbleToRefreshUI && [self isViewLoaded]) {
        NSLog(@"Reload UI on contact display settings changed %@", NSStringFromBOOL(_contactManagerService.displayFirstNameFirst));
        [self.tableView reloadData];
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

-(void) viewDidLoad {
    [super viewDidLoad];
    ((MyInfoNavigationItem*)self.navigationItem).parentViewController = self;
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    self.title = NSLocalizedString(@"Conversations", nil);
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    self.tableView.sectionHeaderHeight = 30;
    self.definesPresentationContext = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    if ([UITools isForceTouchAvailableForTraitCollection:self.traitCollection]) {
        self.previewingContext = [self registerForPreviewingWithDelegate:self sourceView:self.view];
    }
    if([[ServicesManager sharedInstance].myUser.profilesName count] > 0) {
        NSString *profileNames = [[ServicesManager sharedInstance].myUser.profilesName componentsJoinedByString:@","];
        [FIRAnalytics setUserPropertyString:profileNames forName:@"rainbow_service_plan"];
    }

    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, self.tabBarController.tabBar.frame.size.height, 0);
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [FIRAnalytics setScreenName:@"conversations_screen" screenClass:@"RecentsConversationsTableViewController"];
}


-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark - Application notification

-(void) applicationWillResignActive:(NSNotification *) notification {
    if([UIDevice currentDevice].systemMajorVersion > 9.0){
        if(self.traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable){
            NSLog(@"3D touch actions are available");
            // Register the 3 latest conversations as 3D Touch quick action
            NSArray<Conversation *> *subset = nil;
            @synchronized (_conversationsMutex) {
                NSInteger size = 3;
                // TODO : Change me to use always the my conversation section
                NSArray<Conversation *> *myConversationSection = [_conversations objectsInSection:kMyConversationsSection];
                if([myConversationSection count] < 3)
                    size = [myConversationSection count];
                
                subset = [myConversationSection subarrayWithRange:NSMakeRange(0, size)];
            }
            NSMutableArray *shortcuts = [NSMutableArray array];
            [subset enumerateObjectsUsingBlock:^(Conversation * aConversation, NSUInteger idx, BOOL * stop) {
                NSString *name = aConversation.peer.displayName;
                if (aConversation.peer.jid) {
                    UIApplicationShortcutIcon *icon = [UIApplicationShortcutIcon iconWithTemplateImageName:[aConversation.peer isKindOfClass:[Contact class]]?@"chat3DTouch":@"Bubble3DTouch"];
                    [shortcuts addObject:[[UIApplicationShortcutItem alloc] initWithType:name localizedTitle:name localizedSubtitle:nil icon:icon userInfo:@{@"conversationJid":aConversation.peer.jid}]];
                }
            }];
        
            [UIApplication sharedApplication].shortcutItems = shortcuts;
        } else {
            NSLog(@"3D touch actions are NOT available");
        }
    }
}

#pragma mark - Conversations Manager notifications
-(void) didEndLoadingConversations:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didEndLoadingConversations:notification];
        });
        return;
    }
    
    _populated = YES;
    
    @synchronized(_conversationsMutex){
        [_conversations removeAllObjects];
        [_conversationManagerService.conversations enumerateObjectsUsingBlock:^(Conversation * conversation, NSUInteger idx, BOOL * stop) {
            if(![_conversations containsConversation:conversation]){
                [_conversations addObject:conversation];
            }
        }];
    }
    [_conversations reloadData];
    
    if (_isAbleToRefreshUI && [self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didAddConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddConversation:notification];
        });
        return;
    }

    Conversation *theConversation = (Conversation *) notification.object;
    @synchronized (_conversationsMutex) {
        if(![_conversations containsConversation:theConversation]){
            [_conversations addObject:theConversation];
            [_conversations reloadData];
        }
    }
    
    if (_isAbleToRefreshUI && [self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didRemoveConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveConversation:notification];
        });
        return;
    }
    
    Conversation *theConversation = (Conversation *) notification.object;
    @synchronized (_conversationsMutex) {
        if([_conversations containsConversation:theConversation]){
            [_conversations removeObject:theConversation];
            [_conversations reloadData];
        }
    }
    
    if (_isAbleToRefreshUI && [self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didRemoveAllConversations:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveAllConversations:notification];
        });
        return;
    }
    
    _populated = NO;
    
    @synchronized (_conversationsMutex) {
        [_conversations removeAllObjects];
    }
    
    if (_isAbleToRefreshUI && [self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didUpdateConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateConversation:notification];
        });
        return;
    }

    Conversation *theConversation = (Conversation *) notification.object;
    
    if(theConversation) { // DO NOT add a nil conversation to avoid crash
        @synchronized (_conversationsMutex) {
            if(![_conversations containsConversation:theConversation]){
                [_conversations addObject:theConversation];
            }
            [_conversations reloadData];
        }
    }
    if (_isAbleToRefreshUI && [self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didStartConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didStartConversation:notification];
        });
        return;
    }
    
    // We don't want to open the conversation view if the application is in background, case of message send from a push notification.
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
        return;
    
    if(!self.isVisible)
        return;
    
    Conversation *theConversation = (Conversation *)notification.object;
    if([(theConversation.peer) isKindOfClass:[Contact class]]) {
        [FIRAnalytics setUserPropertyString:((Contact *)theConversation.peer).companyName forName:@"company"];
    }
    NSInteger pos = [_conversations indexOfObjectInHisSection:theConversation];
    NSInteger section = [[_conversations sections] count] > 1 ? 1:0;
    if(pos != NSNotFound) {
        __weak __typeof__ (self) weakSelf = self;
        [UIView animateWithDuration:0.2 animations:^{
            [weakSelf.tableView moveRowAtIndexPath:[NSIndexPath indexPathForRow:pos inSection:section] toIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
        } completion:^(BOOL finished){
            if(theConversation.stringForConversationType)
            [FIRAnalytics logEventWithName:@"start_conversation"
                                parameters:@{@"conversation_type":theConversation.stringForConversationType ,@"category":@"conversations_screen"}];
            [[weakSelf class] openConversationViewForConversation:theConversation inViewController:weakSelf.navigationController];
        }];
    }
}

-(void) didStopConversation:(NSNotification *) notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_isAbleToRefreshUI && [self isViewLoaded])
            [self.tableView reloadData];
    });
  
}

#pragma mark - Conference notifications
-(void) didUpdateConference:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateConference:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kConferenceChangedAttributesKey];
    NSLog(@"DID UPDATE CONFERENCE IN CONVERSATION TAB %@", userInfo);
    if([changedKeys containsObject:@"isActive"] ||
       [changedKeys containsObject:@"scheduledStartDate"] ||
       [changedKeys containsObject:@"scheduledEndDate"]){
        [_conversations reloadData];
        
        if (_isAbleToRefreshUI && [self isViewLoaded])
            [self.tableView reloadData];
    }
}
#pragma mark - Room notification
-(void) didRemoveRoom:(NSNotification *) notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_isAbleToRefreshUI && [self isViewLoaded])
            [self.tableView reloadData];
    });
    
}

#pragma mark - XMPPService notification
-(void) isAbleToRefreshUIChanged:(NSNotification *) notification {
    _isAbleToRefreshUI = [[notification.userInfo objectForKey:@"isAbleToRefreshUI"] boolValue];
}

-(void) endReceiveXMPPRequestsAfterResume:(NSNotification *) notification {
    OTCLog(@"No more XMPP requests after resume - reload data on tableView");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

#pragma mark - Table view data source

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    view.tintColor = [UITools colorFromHexa:0xF8F8F8FF];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont fontWithName:[UITools defaultFontName] size:20]];
    [header.textLabel setTextColor:[UITools colorFromHexa:0xB8B8B8FF]];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionName = [[_conversations sections] objectAtIndex:section];
    if([_conversations objectsInSection:sectionName].count > 0){
        if([[_conversations sections] count] == 2 || [sectionName isEqualToString:kActiveCallsSection])
        return NSLocalizedString(sectionName, nil);
    }
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_conversations sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_conversations sections] objectAtIndex:section];
    return [[_conversations objectsInSection:key] count];
} 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    
    NSString *key = [[_conversations sections] objectAtIndex:indexPath.section];
    Conversation *conversation = [[_conversations objectsInSection:key] objectAtIndex:indexPath.row];
    
    cell.cellObject = (NSObject<UIRainbowGenericTableViewCellProtocol>*)conversation;
    cell.showMidLabel = YES;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *key = [[_conversations sections] objectAtIndex:indexPath.section];
    Conversation *theConversation = [[_conversations objectsInSection:key] objectAtIndex:indexPath.row];
    [_conversationManagerService sendMarkAllMessagesAsReadFromConversation:theConversation];
    
    UIPagingMessagesViewController *pagingMsgView = [[UIStoryboardManager sharedInstance].messagesStoryBoard instantiateViewControllerWithIdentifier:@"pagingMessageViewControllerID"];
    pagingMsgView.conversation = theConversation;
    UIMessagesViewController *msgView = pagingMsgView.messagesViewController;
    [[self class] prepareMessageView:msgView withConversation:theConversation];
    [pagingMsgView setViewControllers];
    [self.navigationController pushViewController:pagingMsgView animated:YES];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_conversations sections] objectAtIndex:indexPath.section];
    Conversation *theConversation = [[_conversations objectsInSection:key] objectAtIndex:indexPath.row];
    if(theConversation.hasActiveCall)
        return NO;
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_conversations sections] objectAtIndex:indexPath.section];
    Conversation *theConversation = (Conversation *)[[_conversations objectsInSection:key] objectAtIndex:indexPath.row];
    
    BGTableViewRowActionWithImage *close = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Close", nil) backgroundColor:[UITools defaultTintColor] image:[UIImage imageNamed:@"close_conversation"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [_conversationManagerService markAsReadByMeAllMessageForConversation:theConversation];
        [_conversationManagerService stopConversation:theConversation];
        [FIRAnalytics logEventWithName:@"close_conversation"
                            parameters:@{@"conversation_type":theConversation.stringForConversationType,@"category":@"conversations_screen"}];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tableView.editing = NO;
        });
    }];
    close.accessibilityLabel = NSLocalizedString(@"Close", nil);
    
    BGTableViewRowActionWithImage *leaveBubble = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Quit", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"LeaveBubble"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [UIRoomsTableViewController leaveAction:(Room *)theConversation.peer inController:self completionHandler:^{
            [FIRAnalytics logEventWithName:@"leave_bubble"
                                parameters:@{@"conversation_type":theConversation.stringForConversationType,@"category":@"conversations_screen" }];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.editing = NO;
            });
        }];
    }];
    leaveBubble.accessibilityLabel = NSLocalizedString(@"Quit", nil);
    
    BGTableViewRowActionWithImage *archiveBubble = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Archive", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"ArchiveBubble"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [UIRoomsTableViewController archiveAction:(Room *)theConversation.peer inController:self completionHandler:^{
            [FIRAnalytics logEventWithName:@"archive_bubble"
                                parameters:@{@"conversation_type":theConversation.stringForConversationType,@"category":@"conversations_screen"}];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.editing = NO;
            });
        }];
    }];
    archiveBubble.accessibilityLabel = NSLocalizedString(@"Quit", nil);
    
    BGTableViewRowActionWithImage *mute = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Silence", nil) backgroundColor:[UIColor lightGrayColor] image:[UIImage imageNamed:@"MuteConversation"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [_conversationManagerService muteConversation:theConversation];
        [FIRAnalytics logEventWithName:@"enable_notification" parameters:@{@"category":@"conversations_screen"}];

        dispatch_async(dispatch_get_main_queue(), ^{
            self.tableView.editing = NO;
        });
    }];
    mute.accessibilityLabel = NSLocalizedString(@"Silence", nil);
    
    BGTableViewRowActionWithImage *unmute = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Alert", nil) backgroundColor:[UIColor lightGrayColor] image:[UIImage imageNamed:@"UnmuteConversation"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [_conversationManagerService unmuteConversation:theConversation];
        [FIRAnalytics logEventWithName:@"disable_notification" parameters:@{@"category":@"conversations_screen"}];

        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
        self.tableView.editing = NO;
    }];
    unmute.accessibilityLabel = NSLocalizedString(@"Alert", nil);
    
    NSMutableArray *actions = [NSMutableArray array];
    if (theConversation.type == ConversationTypeRoom && ((Room*)theConversation.peer).myStatusInRoom == ParticipantStatusAccepted){
        Room *aRoom = (Room*)theConversation.peer;
        if(!aRoom.conference || (aRoom.conference && aRoom.conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC)){
            if(aRoom.isMyRoom)
                [actions addObject:archiveBubble];
            else
                [actions addObject:leaveBubble];
        }
    }
    
    [actions addObject:close];
    
    if(theConversation.isMuted)
        [actions addObject:unmute];
    else
        [actions addObject:mute];
    
    return actions;
}

-(void) didReceiveNewMessageForConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReceiveNewMessageForConversation:notification];
        });
        return;
    }
    Conversation *theConversation = (Conversation *)notification.object;
    Message *theMessage = theConversation.lastMessage;
    
    // Ignore if it's a composing message or it has a zero length or it's a message from me.
    if(theMessage.isComposing || theMessage.body.length == 0 || theMessage.isOutgoing)
        return;
    
    if([_lastMessageIdReceiveByPush isEqualToString:theMessage.messageID] || theMessage.hasBeenPresentedInPush || theMessage.isResentMessage){
        NSLog(@"We are treating the message that we already received by push, so don't play sound");
        return;
    }
    
    BOOL notificationsAreEnabled = [UINotificationManager canShowNotifications];
    
    if(!notificationsAreEnabled){
        NSLog(@"Sound notification are disabled or no notification enabled, don't play sound");
        return;
    }
    
    BOOL canPlaySound = [UINotificationManager canPlaySoundForNotification];
    
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        if(!theConversation.isMuted && theMessage.groupChatEventType == MessageGroupChatEventNone && canPlaySound && !theMessage.hasBeenPresentedInPush && theMessage.type != MessageTypeWebRTC){
            NSLog(@"Application is in foreground so play a sound");
            [JSQSystemSoundPlayer jsq_playMessageReceivedAlert];
        }
    } else if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground && !theMessage.hasBeenPresentedInPush && theMessage.type != MessageTypeWebRTC && !theConversation.isMuted && !theMessage.isResentMessage) {
        if(!theMessage.via.jid){
            NSLog(@"No jid for this message %@", theMessage);
            return;
        }
            
        // We are not active, so use a local notification instead
        NSString *body = theMessage.body;
        NSString *displayName = theMessage.via.displayName;
        NSLog(@"Will display local notification");
        NSDictionary *userInfo = nil;
        switch(theConversation.type) {
            case ConversationTypeUser: {
                userInfo = @{@"type" : @"user", @"from" : theMessage.via.jid};
                break;
            }
            case ConversationTypeRoom: {
                userInfo = @{@"type" : @"room", @"from" : theMessage.via.jid};
                body = [NSString stringWithFormat:@"%@ : %@", theMessage.peer.displayName, theMessage.body];
                break;
            }
            default:
                break;
        }
        [FIRAnalytics logEventWithName:@"recieve_new_message" parameters:@{@"category":@"conversations_screen"}];
        [UINotificationManager presentUILocalNotificationWithBody:body title:displayName category:@"im_category" userInfo:userInfo playSound:!theConversation.isMuted];
    }
}

#pragma mark - Local notification
-(void) handleLocalNotification:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        __weak __typeof__(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf handleLocalNotification:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary*)notification.object;
    NSString *category = userInfo[@"category"];
    NSString *from = userInfo[@"from"];
    if([category isEqualToString:@"im_category"]) {
        if(((NSString *)userInfo[@"roomJid"]).length > 0)
            from = userInfo[@"roomJid"];
        if(((NSString *)userInfo[@"room-jid"]).length > 0)
            from = userInfo[@"room-jid"];
        _lastMessageIdReceiveByPush = userInfo[@"messageID"];
        [self.navigationController popToRootViewControllerAnimated:YES];
        Conversation *theConversation = [_conversationManagerService getConversationWithPeerJID:from];
        [self didStartConversation:[NSNotification notificationWithName:kConversationsManagerDidStartConversation object:theConversation]];
    }
}

#pragma mark - DZNEmptyDataSet
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"EmptyRecentsConversations"];
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultTintColor];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"You have no recent conversations.",nil);
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Start new conversation", nil) attributes:attributes];
}

-(UIImage *) buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    return [UITools imageWithColor:[UITools defaultTintColor] size:CGSizeMake(1, 40)];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    CGFloat offset = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
    offset += CGRectGetHeight(self.navigationController.navigationBar.frame);
    return -offset;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 20.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return _populated;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

- (BOOL) emptyDataSetShouldAllowImageViewAnimate:(UIScrollView *)scrollView {
    return YES;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    [((MyInfoNavigationItem*)self.navigationItem) focusSearchController];
}

- (void) presentContactDetailController:(Contact*) contact{
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 3D touch actions
- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
    [super traitCollectionDidChange:previousTraitCollection];
    if ([UITools isForceTouchAvailableForTraitCollection:self.traitCollection]) {
        if (!self.previewingContext) {
            self.previewingContext = [self registerForPreviewingWithDelegate:self sourceView:self.view];
        }
    } else {
        if (self.previewingContext) {
            [self unregisterForPreviewingWithContext:self.previewingContext];
            self.previewingContext = nil;
        }
    }
}


- (UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location {
    
    if ([self.presentedViewController isKindOfClass:[UIPagingMessagesViewController class]]) {
        return nil;
    }
    
    CGPoint translatedLocation = [self.view convertPoint:location toView:self.tableView];
    
    NSIndexPath *idx = [self.tableView indexPathForRowAtPoint:translatedLocation];
    if(idx){
        UIRainbowGenericTableViewCell *tableCell = (UIRainbowGenericTableViewCell*)[self.tableView cellForRowAtIndexPath:idx];
        Conversation *theConversation = ((Conversation*)tableCell.cellObject);
        
        UIPagingMessagesViewController *pagingMsgView = [[UIStoryboardManager sharedInstance].messagesStoryBoard instantiateViewControllerWithIdentifier:@"pagingMessageViewControllerID"];
        pagingMsgView.conversation = theConversation;
        
        UIMessagesViewController *msgView = pagingMsgView.messagesViewController;
        
        [[self class] prepareMessageView:msgView withConversation:theConversation];
        [pagingMsgView setViewControllers];
        CustomNavigationController *navCtrl = [[CustomNavigationController alloc] initWithRootViewController:pagingMsgView];
        
        previewingContext.sourceRect = [self.view convertRect:tableCell.frame fromView:self.tableView];
        
        return navCtrl;
    }
    return nil;
}

- (void)previewingContext:(id<UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit {
    if([viewControllerToCommit isKindOfClass:[CustomNavigationController class]]){
        [self.navigationController pushViewController:((CustomNavigationController*)viewControllerToCommit).topViewController animated:YES];
    }
}

-(void) dumpConversations {
    NSLog(@"DUMP CONVERSATIONS : %@", [_conversations description]);
}

+(void) prepareMessageView:(UIMessagesViewController *) messageView withConversation:(Conversation *) conversation {
    messageView.conversation = conversation;
    messageView.fromView = self;
    
    // If we open a conversation with somebody which have no presence and no last-activity
    // get the last-activity of the user.
    if (conversation.type == ConversationTypeUser) {
        Contact *contact = (Contact*) conversation.peer;
        if (contact.presence.presence == ContactPresenceUnavailable &&
            contact.lastActivityDate == nil &&
            contact.isPresenceSubscribed && !contact.sentInvitation) {
            [[ServicesManager sharedInstance].contactsManagerService getLastActivityForContact:contact];
        }
    }
}

+(void) openConversationViewForConversation:(Conversation *) conversation inViewController:(UINavigationController *) navigationController {
    UIPagingMessagesViewController *pagingMsgView = [[UIStoryboardManager sharedInstance].messagesStoryBoard instantiateViewControllerWithIdentifier:@"pagingMessageViewControllerID"];
    pagingMsgView.conversation = conversation;
    [RecentsConversationsTableViewController prepareMessageView:pagingMsgView.messagesViewController withConversation:conversation];
    [pagingMsgView setViewControllers];
    [navigationController pushViewController:pagingMsgView animated:YES];
}

-(void) scrollToTop {
    NSInteger count = [[_conversationManagerService conversations] count];
    if (count > 0)
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}
@end
