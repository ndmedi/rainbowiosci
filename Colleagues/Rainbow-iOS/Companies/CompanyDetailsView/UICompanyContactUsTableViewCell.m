/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UICompanyContactUsTableViewCell.h"
#import "UITools.h"
#import <Rainbow/Rainbow.h>
#import "UIAvatarView.h"
#import "UISearchResultController.h"

@interface UICompanyContactUsTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UILabel *companyContactDisplayName;
@property (weak, nonatomic) IBOutlet UIAvatarView *companyContactAvatarView;
@property (weak, nonatomic) IBOutlet UIButton *companyContactChatButton;

@end

@implementation UICompanyContactUsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_cellTitle];
    [UITools applyCustomFontTo:_companyContactDisplayName];
    _companyContactAvatarView.asCircle = YES;
    _companyContactAvatarView.showPresence = YES;
    [UITools applyCustomFontTo:_companyContactDisplayName];
    _cellTitle.textColor = [UITools defaultTintColor];
    
    [_companyContactChatButton setTintColor:[UITools defaultTintColor]];
}

-(void) setCompany:(Company *)company {
    _company = company;
    _cellTitle.text = NSLocalizedString(@"Contact us", nil);
    _companyContactAvatarView.peer = _company.companyContact;
    _companyContactDisplayName.text = _company.companyContact.displayName;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(CGFloat) computedHeight {
    return 120;
}
- (IBAction)didTapChatButton:(UIButton *)sender {
    if([_fromView isKindOfClass:[UISearchResultController class]]){
        UISearchResultController *search = (UISearchResultController*) _fromView;
        [search.controllerDelegate dismissSearchControllerAndPerformAction:^{
            [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:_company.companyContact withCompletionHandler:nil];
        }];
    }
}

@end
