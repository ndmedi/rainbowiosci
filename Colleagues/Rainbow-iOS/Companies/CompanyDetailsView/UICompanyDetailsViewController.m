/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UICompanyDetailsViewController.h"
#import "UICompanyAvatarView.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import "MBProgressHUD.h"
#import "UIScrollView+APParallaxHeader.h"
#import "UICompanyInfosTableViewCell.h"
#import "UICompanyAboutUsTableViewCell.h"
#import "UICompanyContactUsTableViewCell.h"
#import "UICompanyJoinCompanyCell.h"
#import "OrderedOptionalSectionedContent.h"

#define kCompanyDefaultSection @"default"
#define kCompanyAboutUsSection @"aboutUs"
#define kCompanyContactUsSection @"contactUs"
#define kCompanyJoinUsSection @"joinUs"

@interface PostalAddress (private)
@property (readwrite) NSString *countryCode;
@end

@interface UICompanyDetailsViewController () <APParallaxViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarViewWidthConstraint;

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UICompanyAvatarView *backgroundAvatarView;
@property (weak, nonatomic) IBOutlet UICompanyAvatarView *avatarView;
@property (nonatomic, strong) UIVisualEffectView *visualEffectView;
@property (nonatomic, strong) OrderedOptionalSectionedContent<Company *> *sections;
@end

@implementation UICompanyDetailsViewController

-(void) awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateCompanyInvitation:) name:kCompaniesServiceDidUpdateCompanyInvitation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddCompanyInvitation:) name:kCompaniesServiceDidAddCompanyInvitation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateCompany:) name:kCompaniesServiceDidUpdateCompany object:nil];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompaniesServiceDidUpdateCompanyInvitation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompaniesServiceDidAddCompanyInvitation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompaniesServiceDidUpdateCompany object:nil];
}

-(void) didUpdateCompany:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateCompany:notification];
        });
        return;
    }
    
    _backgroundAvatarView.company = _company;
    _backgroundAvatarView.contentMode = UIViewContentModeScaleAspectFit;
    ((UICompanyAvatarView *)self.avatarView).company = _company;
    
    if(!_company.banner){
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        _visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        _visualEffectView.frame = self.tableView.parallaxView.bounds;
        [_backgroundAvatarView addSubview:_visualEffectView];
    } else {
        if(_visualEffectView.superview)
           [_visualEffectView removeFromSuperview];
    }
}

-(void) setCompany:(Company *)company {
    _company = company;
    [[ServicesManager sharedInstance].companiesService populateBannerForCompany:_company];
    
    [_sections removeAllObjects];
    _sections = nil;
    
    _sections = [[OrderedOptionalSectionedContent alloc] initWithSections:@[kCompanyDefaultSection, kCompanyAboutUsSection, kCompanyContactUsSection, kCompanyJoinUsSection]];
    
    [_sections addObject:_company toSection:kCompanyDefaultSection];
    if(_company.companyDescription.length > 0)
        [_sections addObject:_company toSection:kCompanyAboutUsSection];
    if(_company.companyContact)
        [_sections addObject:_company toSection:kCompanyContactUsSection];
    if([ServicesManager sharedInstance].myUser.isInDefaultCompany && [ServicesManager sharedInstance].myUser.isUser)
        [_sections addObject:_company toSection:kCompanyJoinUsSection];
}

-(void) viewDidLoad {
    [super viewDidLoad];
    _headerView.clipsToBounds = NO;
    _avatarView.company = _company;
    _avatarView.showPresence = NO;
    _avatarView.asCircle = _company.logoIsCircle;
    _avatarView.layer.borderWidth = 2.0f;
    _avatarView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _backgroundAvatarView.company = _company;
    _backgroundAvatarView.banner = YES;
    _backgroundAvatarView.asCircle = NO;
    _backgroundAvatarView.showPresence = NO;
    _backgroundAvatarView.cornerRadius = 0;
    
    self.title = _company.name;
    [self.tableView addParallaxWithView:_headerView andHeight:[self getHeaderViewHeight] andMinHeight:_avatarView.frame.size.height  andShadow:NO];
    
    if(!_company.banner){
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        _visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        _visualEffectView.frame = self.tableView.parallaxView.bounds;
        [_backgroundAvatarView addSubview:_visualEffectView];
    }
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.tableView.parallaxView setDelegate:self];
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = nil;
    
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];
    self.tableView.parallaxView.layer.zPosition = 1;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _backgroundAvatarView.company = _company;
    _backgroundAvatarView.contentMode = UIViewContentModeScaleAspectFit;
    ((UICompanyAvatarView *)self.avatarView).company = _company;
    
    self.title = _company.name;
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompaniesServiceDidUpdateCompanyInvitation object:nil];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark - APParallaxViewDelegate
- (void)parallaxView:(APParallaxView *)view willChangeFrame:(CGRect)frame {
}

- (void)parallaxView:(APParallaxView *)view didChangeFrame:(CGRect)frame {
    CGRect visualEffectFrame = _visualEffectView.frame;
    visualEffectFrame.size.height = frame.size.height;
    [_visualEffectView setFrame:visualEffectFrame];
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_sections allNotEmptySections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_sections allNotEmptySections] objectAtIndex:indexPath.section];
    
    if([key isEqualToString:kCompanyDefaultSection]){
        UICompanyInfosTableViewCell *cell = (UICompanyInfosTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"companyInfoID"];
        cell.company = _company;
        return cell;
    }
    if([key isEqualToString:kCompanyAboutUsSection]){
        UICompanyAboutUsTableViewCell *cell = (UICompanyAboutUsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"companyAboutUsID"];
        cell.company = _company;
        return cell;
    }
    
    if([key isEqualToString:kCompanyContactUsSection]){
        UICompanyContactUsTableViewCell *cell = (UICompanyContactUsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"companyContactUsID"];
        cell.company = _company;
        // We send the from view (the search controller) to allow to dismiss it correctly
        cell.fromView = _fromView;
        return cell;
    }
    
    if([key isEqualToString:kCompanyJoinUsSection]){
        UICompanyJoinCompanyCell *cell = (UICompanyJoinCompanyCell *)[tableView dequeueReusableCellWithIdentifier:joinCompanyCellID];
        cell.company = _company;
        cell.fromView = self;
        return cell;
    }
    
    return nil;    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_sections allNotEmptySections] objectAtIndex:indexPath.section];
    
    if([key isEqualToString:kCompanyDefaultSection]){
        UICompanyInfosTableViewCell *cell = (UICompanyInfosTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"companyInfoID"];
        cell.company = _company;
        return cell.computedHeight;
    }
    if([key isEqualToString:kCompanyAboutUsSection]){
        UICompanyAboutUsTableViewCell *cell = (UICompanyAboutUsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"companyAboutUsID"];
        cell.company = _company;
        return cell.computedHeight;
    }
    
    if([key isEqualToString:kCompanyContactUsSection]){
        UICompanyContactUsTableViewCell *cell = (UICompanyContactUsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"companyContactUsID"];
        cell.company = _company;
        return cell.computedHeight;
    }
    
    if([key isEqualToString:kCompanyJoinUsSection]){
        UICompanyJoinCompanyCell *cell = (UICompanyJoinCompanyCell *)[tableView dequeueReusableCellWithIdentifier:joinCompanyCellID];
        cell.company = _company;
        return cell.computedHeight;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

#pragma mark - Company invitation updates

-(void) didUpdateCompanyInvitation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        return;
    } else {
        [self.tableView reloadData];
    }
}

-(void) didAddCompanyInvitation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        return;
    } else {
        [self.tableView reloadData];
    }
}

#pragma mark - HeaderView Calculation
-(CGFloat) getHeaderViewHeight {
    if(_company.banner) {
        UIImage *bannerImage = [UIImage imageWithData:_company.banner];
        CGFloat calcSize = (bannerImage.size.height * [UIScreen mainScreen].bounds.size.width) / bannerImage.size.width;
        _avatarViewHeightConstraint.constant = calcSize - 30;
        _avatarViewWidthConstraint.constant  = calcSize - 30;
        return calcSize;
    }
        return 120;
}
@end
