/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UICompanyAboutUsTableViewCell.h"
#import "UITools.h"

@interface UICompanyAboutUsTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UITextView *companyDescription;

@end
@implementation UICompanyAboutUsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_cellTitle];
    [UITools applyCustomFontToTextField:_companyDescription];
    _cellTitle.textColor = [UITools defaultTintColor];
}

-(void) setCompany:(Company *)company {
    _company = company;
    _companyDescription.text = _company.companyDescription;
    _cellTitle.text = NSLocalizedString(@"About us", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(CGFloat) computedHeight {
    CGRect valueRect = [_companyDescription.text boundingRectWithSize:CGSizeMake(_companyDescription.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:_companyDescription.font.pointSize]} context:nil];
    
    return MAX(valueRect.size.height+30, 100);
}

@end
