/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UINotificationManager.h"
#import <Rainbow/ContactsManagerService.h>
#import "Contact+Extensions.h"
#import "UITools.h"
#import <Rainbow/LoginManager.h>
#import "CustomTabBarViewController.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/Tools.h>
#import <Rainbow/defines.h>

static UINotificationManager *singleton = nil;

@interface JFMinimalNotification (ExtendInterface)
- (void)setTitle:(NSString*)title withSubTitle:(NSString*)subTitle;
@property (nonatomic, assign) NSTimeInterval dismissalDelay;
@end


@interface UINotificationManager () <JFMinimalNotificationDelegate>
@property (nonatomic, strong) JFMinimalNotification* minimalNotification;
@end

@implementation UINotificationManager

+(UINotificationManager*) sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[UINotificationManager alloc] init];
    });
    return singleton;
}

-(void) setWindow:(UIWindow *)window {
    _window = window;
    [self configureNotification];
}

-(void) configureNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didInviteContact:) name:kContactsManagerServiceDidInviteContact object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailedToInviteContact:) name:kContactsManagerServiceDidFailedToInviteContact object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];

    _minimalNotification = [JFMinimalNotification notificationWithStyle:JFMinimalNotificationStyleSuccess title:@"Notification" subTitle:@"" dismissalDelay:0 touchHandler:^{
        [_minimalNotification dismiss];
    }];
    [_window.rootViewController.view addSubview:_minimalNotification];
    [UITools applyCustomFontTo:_minimalNotification.titleLabel];
    [UITools applyCustomFontTo:_minimalNotification.subTitleLabel];
    _minimalNotification.edgePadding = UIEdgeInsetsMake(15, -5, 0, -5);
    _minimalNotification.presentFromTop = YES;
    _minimalNotification.delegate = self;
    _minimalNotification.hidden = YES;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidInviteContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidFailedToInviteContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [_minimalNotification removeFromSuperview];
    _minimalNotification = nil;
    _window = nil;
}

#pragma mark - Managed notifications
-(void) didInviteContact:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didInviteContact:notification];
        });
        return;
    }
    
    Contact *invitedContact = (Contact *)notification.object;
    _minimalNotification.dismissalDelay = 1.5;
    [_minimalNotification setStyle:JFMinimalNotificationStyleSuccess animated:NO];
    [_minimalNotification setTitle:[NSString stringWithFormat:NSLocalizedString(@"Invitation sent to %@", nil), invitedContact.displayName] withSubTitle:nil];
    [_minimalNotification show];
}

-(void) didFailedToInviteContact:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didFailedToInviteContact:notification];
        });
        return;
    }
    NSDictionary *infos = (NSDictionary *) notification.object;
    Contact *invitedContact = [infos objectForKey:@"contact"];
    NSString *reason = [infos objectForKey:@"reason"];
    _minimalNotification.dismissalDelay = 2.5;
    [_minimalNotification setStyle:JFMinimalNotificationStyleError animated:NO];
    [_minimalNotification setTitle:[NSString stringWithFormat:NSLocalizedString(@"Failed to invite %@", nil), invitedContact.displayName] withSubTitle:reason];
    [_minimalNotification show];
    
}

-(void) didLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    
}

-(void) didLogout:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }

}

-(void) showNetworkErrorNotificationInViewController:(UIViewController *) viewController {
    JFMinimalNotification *networkErrorNotification = [JFMinimalNotification notificationWithStyle:JFMinimalNotificationStyleWarning title:NSLocalizedString(@"Network error", nil) subTitle:NSLocalizedString(@"Check your network connection.", nil) dismissalDelay:0];
    
    CGFloat navBarHeight = viewController.navigationController.navigationBar.bounds.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
    networkErrorNotification.edgePadding = UIEdgeInsetsMake(navBarHeight-15, 0, 0, 50);
    networkErrorNotification.presentFromTop = YES;
    networkErrorNotification.delegate = self;
    networkErrorNotification.hidden = YES;
    
    [viewController.view insertSubview:networkErrorNotification aboveSubview:viewController.navigationController.navigationBar];

    [networkErrorNotification show];
}

+(JFMinimalNotification *) notificationForNetworkConnectionError {
    JFMinimalNotification *networkErrorNotification = [JFMinimalNotification notificationWithStyle:JFMinimalNotificationStyleWarning title:NSLocalizedString(@"Network error", nil) subTitle:NSLocalizedString(@"Check your network connection.", nil) dismissalDelay:0];
    networkErrorNotification.presentFromTop = NO;
    return networkErrorNotification;
}

#pragma mark - JFMinimalNotification delegate
- (void)minimalNotificationWillShowNotification:(JFMinimalNotification*)notification {
    notification.hidden = NO;
}

- (void)minimalNotificationDidDismissNotification:(JFMinimalNotification*)notification {
    notification.hidden = YES;
    if(notification != _minimalNotification)
        [notification removeFromSuperview];
}

#pragma mark - UILocalNotification facilitor

+(UNMutableNotificationContent *) presentUILocalNotificationWithBody:(NSString *) body title:(NSString *) title category:(NSString *) category userInfo:(NSDictionary *) userInfo {
    return [UINotificationManager presentUILocalNotificationWithBody:body title:title category:category userInfo:userInfo playSound:[UINotificationManager canPlaySoundForNotification]];
}

+(UNMutableNotificationContent *) presentUILocalNotificationWithBody:(NSString *) body title:(NSString *) title category:(NSString *) category userInfo:(NSDictionary *) userInfo playSound:(BOOL) playSound {
    BOOL alreadyRegistered = [[NSUserDefaults standardUserDefaults] boolForKey:kRemotePushNotificationsWizardPageSeen];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(![UIApplication sharedApplication].isRegisteredForRemoteNotifications && !alreadyRegistered) {
            [[ServicesManager sharedInstance].notificationsManager registerForUserNotificationsSettingsWithCompletionHandler:nil];
        }
    });
    
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
    
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.title = title;
    content.body = body;
    content.userInfo = userInfo;
    content.sound = playSound ? [UNNotificationSound defaultSound] : nil;
    content.badge = @([UIApplication sharedApplication].applicationIconBadgeNumber + 1);
    content.categoryIdentifier = category;
    
    NSString *notificationId = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:notificationId
                                                                          content:content trigger:trigger];
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    @synchronized(center){
        [center removePendingNotificationRequestsWithIdentifiers:[NSArray arrayWithObjects:notificationId, nil]];
        [center addNotificationRequest:request withCompletionHandler:nil];
    }
    
    return content;
}

+(BOOL) canShowNotifications {
    BOOL notificationsAreEnabled = YES;
    
    UIUserNotificationType types = [[[UIApplication sharedApplication] currentUserNotificationSettings] types];
    
    if(types == UIUserNotificationTypeNone)
        notificationsAreEnabled = NO;
    NSLog(@"Can show notifications %@", NSStringFromBOOL(notificationsAreEnabled));
    return notificationsAreEnabled;
}

+(BOOL) canPlaySoundForNotification {
    BOOL canPlaySound = YES;
    Presence *myPresence = [ServicesManager sharedInstance].myUser.contact.presence;
    if(myPresence.presence == ContactPresenceBusy || myPresence.presence == ContactPresenceDoNotDisturb){
        NSLog(@"My Presence is Busy or DND, Don't play any sound");
        canPlaySound = NO;
    }
    NSLog(@"Can play sound for notifications %@", NSStringFromBOOL(canPlaySound));
    return canPlaySound;
}

@end
