/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "JFMinimalNotification.h"
#import <Rainbow/Conversation.h>
#import <UserNotifications/UserNotifications.h>

@interface UINotificationManager : NSObject
@property (nonatomic, strong) UIWindow *window;

+(UINotificationManager*) sharedInstance;
+(JFMinimalNotification *) notificationForNetworkConnectionError;

-(void) showNetworkErrorNotificationInViewController:(UIViewController *) viewController;

+(UNMutableNotificationContent *) presentUILocalNotificationWithBody:(NSString *) body title:(NSString *) title category:(NSString *) category userInfo:(NSDictionary *) userInfo;
+(UNMutableNotificationContent *) presentUILocalNotificationWithBody:(NSString *) body title:(NSString *) title category:(NSString *) category userInfo:(NSDictionary *) userInfo playSound:(BOOL) playSound;

+(BOOL) canShowNotifications;
+(BOOL) canPlaySoundForNotification;

@end
