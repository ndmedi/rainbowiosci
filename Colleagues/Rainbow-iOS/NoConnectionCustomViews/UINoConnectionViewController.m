/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UINoConnectionViewController.h"
#import <Rainbow/Rainbow.h>
#import "CustomNavigationController.h"
#import "UINetworkLostViewController.h"
#import "UIGuestModeViewController.h"

@interface UINoConnectionViewController ()
// Lost network banner
@property (nonatomic) BOOL lostNetworkViewAsBeenScrolled;
@property (nonatomic) CGPoint lostNetworkOffsetBeforeScroll;
@property (nonatomic) CGFloat lostNetworkScrolledHeight;

// Guest mode banner
@property (nonatomic) BOOL guestBannerViewAsBeenScrolled;
@property (nonatomic) CGPoint guestBannerOffsetBeforeScroll;
@property (nonatomic) CGFloat guestBannerScrolledHeight;
@end

@implementation UINoConnectionViewController

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideNetworkLostView:) name:CustomNavigationControllerDidHideNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tryToReconnect:) name:kLoginManagerTryToReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowGuestModeView:) name:CustomNavigationControllerWillShowGuestModeView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideGuestModeView:) name:CustomNavigationControllerDidHideGuestModeView object:nil];
        _lostNetworkScrolledHeight = kNetworkLostHeight;
        _guestBannerScrolledHeight = kGuestModeHeight;
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerTryToReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideGuestModeView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowGuestModeView object:nil];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    BOOL isDisplayingNetworkView = ((CustomNavigationController *)self.navigationController).isDisplayingNetworkLostView;
    if(isDisplayingNetworkView && ![ServicesManager sharedInstance].loginManager.isConnected && !_lostNetworkViewAsBeenScrolled)
        [self scrollDownLostNetworkView:YES animated:NO offset:_lostNetworkScrolledHeight];
    
    BOOL isDisplayingGuestView = ((CustomNavigationController *)self.navigationController).isDisplayingGuestModeView;
    if((isDisplayingGuestView && !_guestBannerViewAsBeenScrolled)){
        _guestBannerScrolledHeight = ((CustomNavigationController *)self.navigationController).guestBannerViewSize;
        [self scrollDownGuestBannerView:YES animated:NO offset:_guestBannerScrolledHeight];
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(![ServicesManager sharedInstance].loginManager.isConnected && _lostNetworkViewAsBeenScrolled)
        [self scrollDownLostNetworkView:NO animated:NO offset:_lostNetworkScrolledHeight];
}

#pragma mark - Lost network view
-(void) didLostConnection:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self tryToReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_lostNetworkViewAsBeenScrolled){
        _lostNetworkScrolledHeight = ((CustomNavigationController *)self.navigationController).lostNetwotkViewSize;
        [self scrollDownLostNetworkView:YES animated:YES offset:_lostNetworkScrolledHeight];
    }
}

-(void) tryToReconnect:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self tryToReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_lostNetworkViewAsBeenScrolled){
        _lostNetworkScrolledHeight = ((CustomNavigationController *)self.navigationController).lostNetwotkViewSize;
        [self scrollDownLostNetworkView:YES animated:YES offset:_lostNetworkScrolledHeight];
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _lostNetworkViewAsBeenScrolled){
        _lostNetworkScrolledHeight = ((CustomNavigationController *)self.navigationController).lostNetwotkViewSize;
        [self scrollDownLostNetworkView:NO animated:YES offset:_lostNetworkScrolledHeight];
    }
}

-(void) didHideNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _lostNetworkViewAsBeenScrolled)
        [self scrollDownLostNetworkView:NO animated:YES offset:_lostNetworkScrolledHeight];
}

-(void) didHideGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _guestBannerViewAsBeenScrolled)
        [self scrollDownGuestBannerView:NO animated:YES offset:_guestBannerScrolledHeight];
}

-(void) scrollDownLostNetworkView:(BOOL) down animated:(BOOL) animated offset:(CGFloat) offset{
    if(!_lostNetworkViewAsBeenScrolled && down)
        _lostNetworkOffsetBeforeScroll = self.view.frame.origin;
    
    void (^animationBlock)() = nil;
    
    if(down){
        animationBlock = ^void(){
            CGRect frame = self.view.frame;
            frame.origin.y += offset;
            self.view.frame = frame;
            _lostNetworkViewAsBeenScrolled = YES;
        };
    } else {
        animationBlock = ^void(){
            CGRect frame = self.view.frame;
            frame.origin.y -= offset;
            self.view.frame = frame;
            _lostNetworkViewAsBeenScrolled = NO;
        };
    }
    
    if(animated){
        [UIView animateWithDuration:0.1 animations:animationBlock];
    } else
        animationBlock();
}

#pragma mark - Guest mode
-(void) willShowGuestModeView:(NSNotification *) notification {
    if([self isViewLoaded] && !_guestBannerViewAsBeenScrolled){
        [self scrollDownGuestBannerView:YES animated:YES offset:_guestBannerScrolledHeight];
    }
}

-(void) scrollDownGuestBannerView:(BOOL) down animated:(BOOL) animated offset:(CGFloat) offset{
    if(!_guestBannerViewAsBeenScrolled && down)
        _guestBannerOffsetBeforeScroll = self.view.frame.origin;
    
    void (^animationBlock)() = nil;
    
    if(down){
        animationBlock = ^void(){
            CGRect frame = self.view.frame;
            frame.origin.y += offset;
            self.view.frame = frame;
            _guestBannerViewAsBeenScrolled = YES;
        };
    } else {
        animationBlock = ^void(){
            CGRect frame = self.view.frame;
            frame.origin.y -= offset;
            self.view.frame = frame;
            _guestBannerViewAsBeenScrolled = NO;
        };
    }
    
    if(animated){
        [UIView animateWithDuration:0.1 animations:animationBlock];
    } else
        animationBlock();
}
@end
