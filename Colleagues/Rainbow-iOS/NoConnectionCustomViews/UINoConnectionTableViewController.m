/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UINoConnectionTableViewController.h"
#import <Rainbow/Rainbow.h>
#import "CustomNavigationController.h"
#import "UIScrollView+APParallaxHeader.h"

@interface UINoConnectionTableViewController ()
// Lost network
@property (nonatomic) BOOL lostNetworkViewAsBeenScrolled;
@property (nonatomic) CGPoint lostNetworkOffsetBeforeScroll;
@property (nonatomic) CGFloat lostNetworkScrolledHeight;

// Guest Banner
@property (nonatomic) BOOL guestBannerViewAsBeenScrolled;
@property (nonatomic) CGPoint guestBannerOffsetBeforeScroll;
@property (nonatomic) CGFloat guestBannerScrolledHeight;
@end

@implementation UINoConnectionTableViewController

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideNetworkLostView:) name:CustomNavigationControllerDidHideNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowNetworkLostView:) name:CustomNavigationControllerWillShowNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNetworkLostViewFrame:) name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowGuestModeView:) name:CustomNavigationControllerWillShowGuestModeView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideGuestModeView:) name:CustomNavigationControllerDidHideGuestModeView object:nil];
        
        _lostNetworkScrolledHeight = ((CustomNavigationController *)self.navigationController).lostNetwotkViewSize;
        _guestBannerScrolledHeight = ((CustomNavigationController *)self.navigationController).guestBannerViewSize;
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideGuestModeView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowGuestModeView object:nil];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    BOOL isDisplayingNetworkView = ((CustomNavigationController *)self.navigationController).isDisplayingNetworkLostView;
    BOOL isDisplayingGuestView = ((CustomNavigationController *)self.navigationController).isDisplayingGuestModeView;
    if((isDisplayingNetworkView && !_lostNetworkViewAsBeenScrolled)){
        _lostNetworkScrolledHeight = ((CustomNavigationController *)self.navigationController).lostNetwotkViewSize;
        [self scrollDownLostNetworkView:YES animated:NO height:_lostNetworkScrolledHeight];
    }
    
    if((isDisplayingGuestView && !_guestBannerViewAsBeenScrolled)){
        _guestBannerScrolledHeight = ((CustomNavigationController *)self.navigationController).guestBannerViewSize;
        [self scrollDownGuestBannerView:YES animated:NO height:_guestBannerScrolledHeight];
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(_lostNetworkViewAsBeenScrolled)
        [self scrollDownLostNetworkView:NO animated:NO height:_lostNetworkScrolledHeight];
    if(_guestBannerViewAsBeenScrolled)
        [self scrollDownGuestBannerView:NO animated:NO height:_guestBannerScrolledHeight];
}

#pragma mark - Lost network banner
-(void) didChangeNetworkLostViewFrame:(NSNotification *) notification {
    [self scrollDownLostNetworkView:NO animated:NO height:_lostNetworkScrolledHeight];
    if([self isViewLoaded]){
        _lostNetworkScrolledHeight = ((CustomNavigationController *)self.navigationController).lostNetwotkViewSize;
        [self scrollDownLostNetworkView:YES animated:YES height:_lostNetworkScrolledHeight];
    }
}

-(void) willShowNetworkLostView:(NSNotification *) notification {
    if([self isViewLoaded] && !_lostNetworkViewAsBeenScrolled){
        _lostNetworkScrolledHeight = ((CustomNavigationController *)self.navigationController).lostNetwotkViewSize;
        [self scrollDownLostNetworkView:YES animated:YES height:_lostNetworkScrolledHeight];
    }
}

-(void) scrollDownLostNetworkView:(BOOL) down animated:(BOOL) animated height:(CGFloat) height {
    UIEdgeInsets inset = self.tableView.contentInset;
    if(!_lostNetworkViewAsBeenScrolled && down)
        _lostNetworkOffsetBeforeScroll = self.tableView.contentOffset;
    
    if(down){
        inset.top += height;
        
        if(self.tableView.parallaxView) {
            self.tableView.parallaxView.originalTopInset += height;
        }
    } else {
        inset.top -= height;
        
        if(self.tableView.parallaxView) {
            self.tableView.parallaxView.originalTopInset -= height;
        }
    }
    self.tableView.contentInset = inset;
    
    if(down){
        [self.tableView setContentOffset:CGPointMake(0, -inset.top) animated:animated];
        _lostNetworkViewAsBeenScrolled = YES;
    } else {
        [self.tableView setContentOffset:_lostNetworkOffsetBeforeScroll animated:animated];
        _lostNetworkViewAsBeenScrolled = NO;
    }
}

-(void) didHideNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _lostNetworkViewAsBeenScrolled){
        _lostNetworkScrolledHeight = ((CustomNavigationController *)self.navigationController).lostNetwotkViewSize;
        [self scrollDownLostNetworkView:NO animated:YES height:_lostNetworkScrolledHeight];
    }
}

-(void) didHideGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _guestBannerViewAsBeenScrolled){
        _guestBannerScrolledHeight = ((CustomNavigationController *)self.navigationController).guestBannerViewSize;
        [self scrollDownGuestBannerView:NO animated:YES height:_guestBannerScrolledHeight];
    }
}

#pragma mark - Guest mode
-(void) willShowGuestModeView:(NSNotification *) notification {
    if([self isViewLoaded] && !_guestBannerViewAsBeenScrolled){
        _guestBannerScrolledHeight = ((CustomNavigationController *)self.navigationController).guestBannerViewSize;
        [self scrollDownGuestBannerView:YES animated:YES height:_guestBannerScrolledHeight];
    }
}

-(void) scrollDownGuestBannerView:(BOOL) down animated:(BOOL) animated height:(CGFloat) height {
    UIEdgeInsets inset = self.tableView.contentInset;
    if(!_guestBannerViewAsBeenScrolled && down)
        _guestBannerOffsetBeforeScroll = self.tableView.contentOffset;
    
    if(down){
        inset.top += height;
        
        if(self.tableView.parallaxView) {
            self.tableView.parallaxView.originalTopInset += height;
        }
    } else {
        inset.top -= height;
        
        if(self.tableView.parallaxView) {
            self.tableView.parallaxView.originalTopInset -= height;
        }
    }
    self.tableView.contentInset = inset;
    
    if(down){
        [self.tableView setContentOffset:CGPointMake(0, -inset.top) animated:animated];
        _guestBannerViewAsBeenScrolled = YES;
    } else {
        [self.tableView setContentOffset:_guestBannerOffsetBeforeScroll animated:animated];
        _guestBannerViewAsBeenScrolled = NO;
    }
}
@end
