/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */
#import <Rainbow/Rainbow.h>
#import "AppDelegate.h"
#import "UITools.h"
#import <Rainbow/UIDevice+VersionCheck.h>
#import "CustomTabBarViewController.h"
#import "RecentsConversationsTableViewController.h"
#import "UINotificationManager.h"
#import "debuggerCheck.h"
#import <Rainbow/Tools.h>
#import <Rainbow/RTCService.h>
#import <Intents/Intents.h>
#import <Rainbow/NSDictionary+ChangedKeys.h>
#import <Rainbow/RainbowUserDefaults.h>
#import <HockeySDK/HockeySDK.h>
#import "iLink.h"
#import "ContactsManagerService+InvitationPrivate.h"
#import "UIContactDetailsViewController.h"
#import "CustomNavigationController.h"
#import "UIContactsTableViewController.h"
#import <UserNotifications/UserNotifications.h>
#import "Firebase.h"
@interface ServicesManager ()
-(void) migrationDetectionFrom:(NSNumber *) previousAppVersion toVersion:(NSNumber *) currentAppVersion;
@end

@interface AppDelegate () <BITHockeyManagerDelegate, UNUserNotificationCenterDelegate>
@property (nonatomic, strong) MBProgressHUD *hud;
@end

@implementation AppDelegate

+ (void)initialize {
    [super initialize];
    
    [RainbowUserDefaults setSuiteName:@"group.com.alcatellucent.otcl"];
    
    [iLink sharedInstance].verboseLogging = YES;
    [iLink sharedInstance].messageTitle = NSLocalizedString(@"Update available", nil);
    [iLink sharedInstance].message = NSLocalizedString(@"A new version of Rainbow is available. Please update your application.", nil);
    [iLink sharedInstance].cancelButtonLabel = @"";
    [iLink sharedInstance].remindButtonLabel = NSLocalizedString(@"Remind me later", nil);
    [iLink sharedInstance].updateButtonLabel = NSLocalizedString(@"Update now", nil);
    [iLink sharedInstance].remindPeriod = (float)1/8; // A day divided by 8 --> 3 hours
    [iLink sharedInstance].ignoreCacheForRequestPeriod = (float)1/2; // A day divided by 2 --> 12 hours
    
    [[LogsRecorder sharedInstance] stopRecord];
    if(AmIBeingDebugged() == FALSE)
        [[LogsRecorder sharedInstance] startRecord];
    
    [[RainbowUserDefaults sharedInstance] migrateNSUserDefaultsToSharedDefaults];
    
    NSString *appVersion = [[RainbowUserDefaults sharedInstance] stringForKey:@"appVersion"];
    // Set current version
    NSString *currentAppVersion = [NSString stringWithFormat:@"%@%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
    [[RainbowUserDefaults sharedInstance] setObject:currentAppVersion forKey:@"appVersion"];
    
    if(![appVersion isEqualToString:currentAppVersion]){
        NSLog(@"Migration from version %@ to version %@", appVersion, currentAppVersion);
        NSNumber *previousAppVersion = @([appVersion floatValue]);
        [[ServicesManager sharedInstance] migrationDetectionFrom:previousAppVersion toVersion:@([currentAppVersion floatValue])];
    }
    
    // is it a brand new / fresh install ?
    BOOL freshInstall = [appVersion isEqualToString:@"(null)"];
    
    if(freshInstall){
        // Remove the keychain content just to be sure that is not a "re-installation"
        [[ServicesManager sharedInstance].loginManager resetAllCredentials];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window.backgroundColor = [UIColor whiteColor];
    
#if !DEBUG
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"b926d8c830384b7899f9a2dcfd5d812b"];
    [[BITHockeyManager sharedHockeyManager] setDelegate:self];
    [[BITHockeyManager sharedHockeyManager] setDisableMetricsManager:YES];
    [[BITHockeyManager sharedHockeyManager] setDisableUpdateManager:YES];
    [[BITHockeyManager sharedHockeyManager] setDisableFeedbackManager:YES];
    [[BITHockeyManager sharedHockeyManager] startManager];
#endif
    
    [UITools defineApplicationIDForRainbow];
    
#if DEBUG
    // Support for UI tests
    
    NSDictionary *environment = [[NSProcessInfo processInfo] environment];
    NSLog(@"App started with env %@", environment);
    
    NSString *server = [environment objectForKey:@"server"];
    if ([server length]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidChangeUser object:nil];
        [self application:application openURL:[NSURL URLWithString:server] sourceApplication:@"UI tests" annotation:[NSObject new]];
    }
    NSString *username = [environment objectForKey:@"username"];
    NSString *password = [environment objectForKey:@"password"];
    if (username && password) {
        [[ServicesManager sharedInstance].loginManager setUsername:username andPassword:password];
        // Force cache cleaning.
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidChangeUser object:nil];
    }
#endif
    
    NSLog(@"Application did finish launching");
    
#if !DEBUG
    BOOL isRunningTestFlightBeta = [[[[NSBundle mainBundle] appStoreReceiptURL] lastPathComponent] isEqualToString:@"sandboxReceipt"];
#else
    BOOL isRunningTestFlightBeta = NO;
#endif
    
    NSLog(@"Is running a testflight version %@", NSStringFromBOOL(isRunningTestFlightBeta));
    
    [UITools customizeAppAppearanceWithGlobalColor:[UITools defaultTintColor]];
    [UINotificationManager sharedInstance].window = self.window;
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"lastMessagesEntered": [NSMutableDictionary new], @"dontShowRemoveViewerWarningAlert":@NO, @"advancedSettingsDisableTurns":@YES, @"muteNewMeetingParticipantSound":@NO}];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeServer:) name:kLoginManagerDidChangeServer object:nil];
    
    if([UIDevice currentDevice].systemMajorVersion > 9){
        UIApplicationShortcutItem *item = [launchOptions valueForKey:UIApplicationLaunchOptionsShortcutItemKey];
        if (item) {
            NSLog(@"We've launched from shortcut item: %@", item.userInfo);
            
            [ServicesManager sharedInstance].actionToPerformHandler = ^{
                NSString *conversationJid = (NSString *)[item.userInfo objectForKey:@"conversationJid"];
                Conversation *conversation = [[ServicesManager sharedInstance].conversationsManagerService getConversationWithPeerJID:conversationJid];
                [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:conversation.peer withCompletionHandler:nil];
            };
        } else {
            NSLog(@"We've launched properly.");
        }
    }

    
    [[ServicesManager sharedInstance].rtcService startCallKitWithIncomingSoundName:@"incoming-call.mp3" iconTemplate:@"logo_rainbow_mask" appName:[Tools applicationName]];

    [ServicesManager sharedInstance].rtcService.appSoundOutgoingCall = @"outgoing-rings.mp3";
    [ServicesManager sharedInstance].rtcService.appSoundHangup = @"hangup.wav";
    [ServicesManager sharedInstance].rtcService.appSoundHoldTone = @"hold_tone.mp3";
    
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    
    [FIRApp configure];
    [FIRAnalyticsConfiguration.sharedInstance setAnalyticsCollectionEnabled:true];
    return YES;
}

-(void) checkPasteBoardContent {
    NSString *pasteBoardContent = [UIPasteboard generalPasteboard].string;
    BOOL handleURL = [self startWithInvitationInformations:[self parseInvitationInformationsWithUrl:pasteBoardContent]];
    if(handleURL)
        [[UIPasteboard generalPasteboard] setString:@""];
}

-(NSMutableDictionary *) parseInvitationInformationsWithUrl:(NSString *) invitationUrlString {
    
    // Remove "#" to provide a valid url format for NSURLComponents
    NSString *urlWithoutHash = [invitationUrlString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    NSURL *invitationUrl = [NSURL URLWithString:urlWithoutHash];
    NSMutableDictionary *invitationInfos = [NSMutableDictionary dictionary];
    
    // Check url
    if(!invitationUrl){
        NSLog(@"Invitation url is not valid %@", invitationUrl);
        return invitationInfos;
    }
    
    // When user is not a guest, PGI meeting invitation does not contain a "invitationId"
    if ([Tools containsQueryItem:@"invitationId" fromURL:invitationUrl] || [[Tools valueForKey:@"scenario" fromURL:invitationUrl] isEqualToString:@"pstn-conference"]) {
        
        NSString *invitationID = nil;
        NSString *companyName = nil;
        NSString *joinRoomConfId = nil;
        NSString *scenario = nil;
        
        NSData *loginEmailData = [[NSData alloc] initWithBase64EncodedString:[Tools valueForKey:@"loginEmail" fromURL:invitationUrl] options:0];
        NSString *loginEmail = [[NSString alloc] initWithData:loginEmailData encoding:NSUTF8StringEncoding];
        
        
        if([Tools containsQueryItem:@"invitationId" fromURL:invitationUrl]){
            invitationID = [[NSString alloc] initWithUTF8String:[[Tools valueForKey:@"invitationId" fromURL:invitationUrl] UTF8String]];
        }
        
        if([Tools containsQueryItem:@"companyName" fromURL:invitationUrl]){
            NSData *companyNameData = [[NSData alloc] initWithBase64EncodedString:[Tools valueForKey:@"companyName" fromURL:invitationUrl] options:0];
            companyName = [[NSString alloc] initWithData:companyNameData encoding:NSUTF8StringEncoding];
        }
        
        // Scenario "chat" refer to a bubble invitation & "pstn-conference" refer to PGI meeting
        if([Tools containsQueryItem:@"scenario" fromURL:invitationUrl]){
            joinRoomConfId = [[NSString alloc] initWithUTF8String:[[Tools valueForKey:@"joinRoomConfId" fromURL:invitationUrl] UTF8String]];
            scenario = [[NSString alloc] initWithUTF8String:[[Tools valueForKey:@"scenario" fromURL:invitationUrl] UTF8String]];
        }
        
        if(loginEmail.length > 0)
            [invitationInfos setValue:loginEmail forKey:@"loginEmail"];
        
        // If there is a company name it's a company invitation
        if(companyName.length > 0){
            if(invitationID.length > 0)
                [invitationInfos setValue:invitationID forKey:@"joinCompanyID"];
            [invitationInfos setValue:companyName forKey:@"companyName"];
        } else {
            // This is a "normal" invitation or guest invitation
            if(invitationID.length > 0)
                [invitationInfos setValue:invitationID forKey:@"invitationID"];
            if(joinRoomConfId.length > 0)
                [invitationInfos setValue:joinRoomConfId forKey:@"joinRoomConfId"];
            if(scenario.length > 0)
                [invitationInfos setValue:scenario forKey:@"scenario"];
        }
    }
    return invitationInfos;
}

    
-(BOOL) startWithInvitationInformations:(NSMutableDictionary *) invitationInfos {
    
    // Should go to wizard but in a specific state
    NSInteger nbOfExceptedParameters = 1;
    // PGI meeting with no "invitationId"
    if (!invitationInfos[@"invitationID"] && [invitationInfos[@"scenario"] isEqualToString:@"pstn-conference"])
        nbOfExceptedParameters --;
    if([invitationInfos[@"loginEmail"] length] > 0)
        nbOfExceptedParameters ++;
    if([invitationInfos[@"joinRoomConfId"] length] > 0)
        nbOfExceptedParameters ++;
    if([invitationInfos[@"scenario"] length] > 0)
        nbOfExceptedParameters ++;
    if([invitationInfos[@"companyName"] length] > 0)
        nbOfExceptedParameters ++;
    
    
    if(invitationInfos.count != nbOfExceptedParameters){
        NSLog(@"Invitation infos are not valid %@", invitationInfos);
        return NO;
    }

    if(nbOfExceptedParameters == 1){
        // We have only the invitation ID so this is a SMS invitation
        // If the user is already logged we must ask him if he want to accept it or not.
        // If not logged, we must start the wizard in a special mode.
        if([ServicesManager sharedInstance].loginManager.loginDidSucceed){
            // We don't do nothings asked by Christophe
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"startWithInvitation" object:invitationInfos];
        }
        return YES;
    } else {
        if([ServicesManager sharedInstance].loginManager.loginDidSucceed){
            // Nothing done, the app will start normally and the user will use it directly
            // If invited user is the same as logged user, open bubble conversation
            if ([[ServicesManager sharedInstance].myUser.username isEqualToString:invitationInfos[@"loginEmail"]]) {
                Room *room = [[ServicesManager sharedInstance].roomsService getRoomByJid:invitationInfos[@"joinRoomConfId"]];
                [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:room withCompletionHandler:nil];
                return YES;
            }
        } else {
            [[ServicesManager sharedInstance].loginManager resetAllCredentials];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"startWithInvitation" object:invitationInfos];
            return YES;
        }
        return NO;
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(id)annotation {
    NSString *serverHostName = [url host];
    NSString *login = @"";
    NSString *password = @"";

    if (url.path.length > 0) {
        if ([url port]) {
            serverHostName = [NSString stringWithFormat:@"%@:%@%@",[url host], [url port], [url path]];
        } else {
            serverHostName = [NSString stringWithFormat:@"%@/%@",[url host], [url path]];
        }
    }

    if([url query].length > 0) {
        login = [Tools valueForKey:@"login" fromURL:url];
        password = [Tools valueForKey:@"pwd" fromURL:url];
        
        NSLog(@"openURL with paramters: login: %@, password: %@", login, password );
    }
    
    if(!serverHostName)
        serverHostName = @"";
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kChangeServerURLNotification object:@{ @"serverURL": serverHostName, @"login": login, @"password": password}];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    if (AmIBeingDebugged() == FALSE){
        if([[LogsRecorder sharedInstance] isRecording] == FALSE){
            [[LogsRecorder sharedInstance] startRecord];
        } else {
            [[LogsRecorder sharedInstance] stopRecord];
            //new lifecycle -> new log file is any.
            [[LogsRecorder sharedInstance] startRecord];
        }
    } else {
        [[LogsRecorder sharedInstance] stopRecord];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND,0), ^{
        [[LogsRecorder sharedInstance] cleanOldLogs];
    });
    
    
    // Check pastboard content if needed - in case of invitation without Rainbow already installed
    if([ServicesManager sharedInstance].myUser.username.length > 0 && [ServicesManager sharedInstance].myUser.password.length > 0){
        NSLog(@"Don't check pasteboard users info are already provided");
        return;
    }else{
        [self checkPasteBoardContent];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [[NSNotificationCenter defaultCenter] postNotificationName:UIApplicationDidRegisterUserNotificationSettings object:notificationSettings];
}

#pragma mark - Push notification delegate
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[ServicesManager sharedInstance].notificationsManager didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    [[NSNotificationCenter defaultCenter] postNotificationName:UIApplicationDidRegisterForRemoteNotificationWithDeviceToken object:deviceToken];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [[NSNotificationCenter defaultCenter] postNotificationName:UIApplicationDidFailToRegisterForRemoteNotificationsWithError object:error];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *) userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler {
    if ([userInfo isKindOfClass:[NSDictionary class]]) {
        NSLog(@"didReceiveRemoteNotification: %@", [Tools anonymizeNotificationUserInfo:userInfo]);
    }
    if([userInfo valueForKey:@"message-id"]) {
        NSArray *messageID = [[userInfo valueForKey:@"message-id"] componentsSeparatedByString:@"-"];
        [FIRAnalytics logEventWithName:@"recieve_notification" parameters:@{@"message_id":[messageID objectAtIndex:messageID.count - 1]}];
    }
    [[RainbowUserDefaults sharedInstance] setBool:YES forKey:@"shouldRefreshConversationUI"];
    [[ServicesManager sharedInstance].notificationsManager saveNotificationWithUserInfo:userInfo];
    completionHandler(UIBackgroundFetchResultNoData);
}

#pragma mark - NSNotifictionCenterDelegate
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    NSLog(@"didReceiveNotificationResponse foreground"); // JLF ???
    completionHandler(UNNotificationPresentationOptionNone);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler {
    //response.notification.request.content.userInfo
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:response.notification.request.content.userInfo];
    if (userInfo.count) {
        if (![userInfo.allKeys containsObject:@"category"]) {
            if(response.notification.request.content.categoryIdentifier)
                [userInfo setObject:response.notification.request.content.categoryIdentifier forKey:@"category"];
        }
        if (![[userInfo allKeys] containsObject:@"from"]) {//is remote notification
            if([response.notification.request.content.userInfo valueForKey:@"last-message-sender"])
                [userInfo setObject:[response.notification.request.content.userInfo valueForKey:@"last-message-sender"] forKey:@"from"];
        }
    }
    else {
        NSLog(@"didReceiveNotificationResponse : no userInfo, localNotif ???");
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UIApplicationDidReceiveLocalNotification object:userInfo];
    
    if ([response respondsToSelector:@selector(userText)]) {
        NSString *userText = [(UNTextInputNotificationResponse *)response userText];
        [[ServicesManager sharedInstance].notificationsManager handleNotificationWithIdentifier:response.actionIdentifier withUserInfo:userInfo withResponseInformation:@{UIUserNotificationActionResponseTypedTextKey : userText}];
    }
    completionHandler();
}

# pragma mark - Local notifications
-(void) application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:notification.userInfo];
    if(![userInfo.allKeys containsObject:@"category"] && notification.category)
       [userInfo setObject:notification.category forKey:@"category"];
    [[NSNotificationCenter defaultCenter] postNotificationName:UIApplicationDidReceiveLocalNotification object:userInfo];
    [self newApplicationBadgeValueForNotification:notification];
}

-(void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler {
    [self application:application didReceiveLocalNotification:notification];
    if(completionHandler)
        completionHandler();
}

-(void) newApplicationBadgeValueForNotification:(UILocalNotification *) notification {
    NSInteger total = [[ServicesManager sharedInstance].conversationsManagerService totalNbOfUnreadMessagesInAllConversations];
    [UIApplication sharedApplication].applicationIconBadgeNumber = total;
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(nullable NSString *)identifier forLocalNotification:(UILocalNotification *)notification withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void(^)())completionHandler {
    [self newApplicationBadgeValueForNotification:notification];
    NSLog(@"Handle Local notification action withIDForNotif %@ %@ %@", identifier, notification.userInfo, responseInfo);
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:notification.userInfo];
    [userInfo setObject:identifier forKey:@"category"];
    
    [[ServicesManager sharedInstance].notificationsManager handleNotificationWithIdentifier:identifier withUserInfo:userInfo withResponseInformation:responseInfo];
    
    completionHandler();
}

/*
-(void) application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    [[NSNotificationCenter defaultCenter] postNotificationName:kApplicationDidReceiveNotification object:userInfo];
    NSLog(@"Did receive Local notification %@",userInfo);
    
    NSString *type = [notification.userInfo objectForKey:@"type"];
    if([type isEqualToString:@"c2c"]) {
        // If we arrive here, its because the user clicked ON the C2C notification (and not on the actions accept or decline)
        // Then show the prompt so he can choose to call or not.
        NSString *phoneNumber = [notification.userInfo valueForKey:@"phoneNumber"];
        NSString *telWithoutSpaces = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *numberURL = [NSString stringWithFormat:@"telprompt://%@", telWithoutSpaces];
        // Cannot be called on mainthread in this function.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:numberURL]];
        });
    }
}
*/

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler {
    
    [[ServicesManager sharedInstance].notificationsManager handleNotificationWithIdentifier:identifier withUserInfo:userInfo withResponseInformation:nil];
    
    completionHandler();
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(nullable NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void(^)())completionHandler {
    NSLog(@"Handle action withIDForRemoteNotif %@ %@ %@", identifier, userInfo, responseInfo);
    
    [[ServicesManager sharedInstance].notificationsManager handleNotificationWithIdentifier:identifier withUserInfo:userInfo withResponseInformation:responseInfo];

    completionHandler();
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    NSLog(@"Launched from shortcut item %@", shortcutItem.userInfo);
    NSString *conversationJid = (NSString *)[shortcutItem.userInfo objectForKey:@"conversationJid"];
    Conversation *conversation = [[ServicesManager sharedInstance].conversationsManagerService getConversationWithPeerJID:conversationJid];
    [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:conversation.peer withCompletionHandler:nil];
    completionHandler(YES);
}

#pragma mark -

-(void) didChangeUser:(NSNotification *) notification {
    // User have changed we must reset the latest messages dictionary in NSUserDefault
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastMessagesEntered"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSMutableDictionary new] forKey:@"lastMessagesEntered"];
}

-(void) didChangeServer:(NSNotification *) notification {
    NSLog(@"Server has been updated, refresh the appid");
    [UITools defineApplicationIDForRainbow];
}


-(BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler {
    NSLog(@"continueUserActivity %@", userActivity);
    
    // UNIVERSAL LINKS
    if ([userActivity.activityType isEqualToString: NSUserActivityTypeBrowsingWeb]) {
        NSURL *url = userActivity.webpageURL;
        return [self startWithInvitationInformations:[self parseInvitationInformationsWithUrl:[url absoluteString]]];
    }
    
    // AUDIO/VIDEO Intent
    if([userActivity.interaction.intent isKindOfClass:[INStartAudioCallIntent class]] ||
       [userActivity.interaction.intent isKindOfClass:[INStartVideoCallIntent class]]) {
        if(![ServicesManager sharedInstance].rtcService.microphoneAccessGranted){
            [UITools showMicrophoneBlockedPopupInController:application.keyWindow.rootViewController];
            return NO;
        } else {
            __block NSError* searchError = nil;
            __block UIView* currentView = (UIView *)application.keyWindow;
            __block UIViewController* viewController = application.keyWindow.rootViewController;
            
            [ServicesManager sharedInstance].actionToPerformHandler = ^{
                [[ServicesManager sharedInstance].rtcService handleContinueUserActivity:userActivity error:&searchError waitingForResponse:^(BOOL shouldShowActivity){
                    [self showHideHud:shouldShowActivity view:currentView withMessage:NSLocalizedString(@"Searching ...", nil)];
                } restorationHandler:restorationHandler];
                
                if(searchError) {
                    Contact *aContact = [searchError.userInfo objectForKey:@"object"];
                    if(aContact){
                        NSString *msg = NSLocalizedString(@"You couldn't contact this person", nil);
                        NSString *msg2 = @"";
                        if(aContact.sentInvitation.status == InvitationStatusPending){
                            msg2 = NSLocalizedString(@"An invitation as already be sent to this user, he must accept it before you can continue your action", nil);
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [UITools showErrorPopupWithTitle:msg message:msg2 inViewController:viewController];
                        });
                    } else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            BOOL inviteShown = [self showContactInvitationFromUserActivity:userActivity from:viewController];
                            
                            if(!inviteShown)
                                [UITools showErrorPopupWithTitle:NSLocalizedString(searchError.localizedDescription, nil) message:nil inViewController:viewController];
                        });
                    }
                }
            };
            
            return YES;
        }
    }
    
    // MESSAGE Intent
    else if([userActivity.interaction.intent isKindOfClass:[INSendMessageIntent class]]) {
        __block NSError* searchError = nil;
        __block UIView* currentView = (UIView *)application.keyWindow;
        __block UIViewController* viewController = application.keyWindow.rootViewController;
        
        [ServicesManager sharedInstance].actionToPerformHandler = ^{
            [[ServicesManager sharedInstance].conversationsManagerService handleContinueUserActivity:userActivity error:&searchError waitingForResponse:^(BOOL shouldShowActivity){
                [self showHideHud:shouldShowActivity view:currentView withMessage:NSLocalizedString(@"Searching ...", nil)];
            } restorationHandler:restorationHandler];
            
            if(searchError) {
                Contact *aContact = [searchError.userInfo objectForKey:@"object"];
                if(aContact){
                    NSString *msg = NSLocalizedString(@"You couldn't contact this person", nil);
                    NSString *msg2 = @"";
                    if(aContact.sentInvitation.status == InvitationStatusPending){
                        msg2 = NSLocalizedString(@"An invitation as already be sent to this user, he must accept it before you can continue your action", nil);
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UITools showErrorPopupWithTitle:msg message:msg2 inViewController:viewController];
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        BOOL inviteShown = [self showContactInvitationFromUserActivity:userActivity from:viewController];
                        
                        if(!inviteShown)
                            [UITools showErrorPopupWithTitle:NSLocalizedString(searchError.localizedDescription, nil) message:nil inViewController:viewController];
                    });
                }
            }
        };

        return YES;
    }
    
    else
        return NO;
}

-(void) showHideHud:(BOOL) show view:(UIView *) view withMessage:(NSString *) message {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showHideHud:show view:view withMessage:message];
        });
        return;
    }
    
    if(show) {
        _hud = [UITools showHUDWithMessage:message forView:view mode: MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
        [_hud show:YES];
    } else {
        if(_hud) {
            [_hud hide:YES];
        }
    }
}

-(BOOL) showContactInvitationFromUserActivity:(NSUserActivity *)userActivity from:(UIViewController*)fromViewController {
    INInteraction *interaction = userActivity.interaction;
    INPerson *person;

    if([interaction.intent isKindOfClass:[INSendMessageIntent class]]) {
        INSendMessageIntent *startIntent = (INSendMessageIntent *)interaction.intent;
        person = [startIntent.recipients firstObject];
    } else if([interaction.intent isKindOfClass:[INStartAudioCallIntent class]] || [interaction.intent isKindOfClass:[INStartVideoCallIntent class]]) {
        INStartAudioCallIntent *startIntent = (INStartAudioCallIntent *)interaction.intent;
        person = [startIntent.contacts firstObject];
    }
    
    if(person && person.personHandle) {
        INPersonHandle *personHandle = person.personHandle;
        
        if(personHandle.value) {
            Contact *contact = [[ServicesManager sharedInstance].contactsManagerService searchLocalContactWithEmailString:personHandle.value];
        
            if(contact) {
                __block NSUInteger contactCtrlIndex = 0;
                __block UIViewController *ctrl;
                __block UITabBarController *tabBarController = (UITabBarController*)fromViewController;
                [[tabBarController viewControllers] enumerateObjectsUsingBlock:^(UIViewController *ctrlObj, NSUInteger idx, BOOL *stop) {
                    if([[[ctrlObj childViewControllers] firstObject] isKindOfClass:[UIContactsTableViewController class]]) {
                        ctrl = [[ctrlObj childViewControllers] firstObject];
                        contactCtrlIndex = idx;
                        *stop = YES;
                    }
                }];
                
                if(ctrl) {
                    [UITools showInvitePopupForContact:contact inViewController:ctrl withCompletionHandler:^{
                        tabBarController.selectedIndex = contactCtrlIndex;
                        [UITools showInviteContactAlertController:contact fromController:ctrl controllerDelegate:ctrl withCompletionHandler:^(Invitation *invitation) {
                            [((UIContactsTableViewController*)ctrl) setPendingInvitation:invitation];
                        }];
                    }];
                    
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

- (void)iLinkDidFindiTunesInfo{
    // This method would be called after iLink succeeded to fetch all app data from iTunes.
}

#pragma mark - HockeyApp delegate
-(NSString *)userIDForHockeyManager:(BITHockeyManager *)hockeyManager componentManager:(BITHockeyBaseManager *)componentManager {
    return [ServicesManager sharedInstance].myUser.contact.jid;
}

-(NSString *)applicationLogForCrashManager:(BITCrashManager *)crashManager {
    NSArray* logs = [LogsRecorder sharedInstance].logs;
    NSMutableString *lastFileContent = [NSMutableString string];
    NSArray* reversedlogFiles = [[logs reverseObjectEnumerator] allObjects];
    [reversedlogFiles enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop) {
        NSData* data = [[LogsRecorder sharedInstance] dataForLog:obj];
        NSString* separator;
        if (data) {
            separator = [NSString stringWithFormat:@"\n\n#########################\n %@\n#########################\n", obj];
            [lastFileContent appendString:separator];
            [lastFileContent appendString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
        }
    }];

    return lastFileContent;
}

@end
