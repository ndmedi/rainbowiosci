/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "UISearchResultController.h"


@class CustomSearchController;

/**
 *  Search scope
 */
typedef NS_ENUM(NSInteger, SearchScope) {
    SearchScopeRemoteContacts = 1 << 0,
    SearchScopeLocalContacts = 1 << 1,
    SearchScopeConversations = 1 << 2,
    SearchScopeRooms = 1 << 3,
    SearchScopeGroups = 1 << 4,
    SearchScopeCompanies = 1 << 5,
    SearchScopeCall = 1 << 6,
    SearchScopeContacts = SearchScopeRemoteContacts | SearchScopeLocalContacts,
    SearchScopeAll = SearchScopeContacts | SearchScopeConversations | SearchScopeRooms | SearchScopeGroups | SearchScopeCompanies | SearchScopeCall
};

@protocol CustomSearchControllerDelegate <NSObject>
@required
-(void) customSearchController:(CustomSearchController * _Nonnull) customSearchController didPressCancelButton :(UISearchBar * _Nonnull ) searchBar;
-(void) willPresentSearchController:(CustomSearchController * _Nonnull) customSearchController ;
- (void) willDismissSearchController:(CustomSearchController * _Nonnull) customSearchController;

@end

@interface CustomSearchController : UISearchController

// default value All
@property (nonatomic) SearchScope searchScope;

-(_Nonnull instancetype) initWithSearchResultsController:(UISearchResultController  * _Nonnull )searchResultController customSearchControllerDelegate:(id<CustomSearchControllerDelegate> _Nonnull) customSearchDelegate;

-(void) dismissSearchControllerAndPerformAction:(void (^ __nullable)(void)) action;
-(void) dismissSearchController;
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar;

@property (nonatomic) BOOL handleDismissOnCancel;
@end
