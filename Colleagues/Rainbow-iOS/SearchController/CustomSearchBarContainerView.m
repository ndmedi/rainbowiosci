/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CustomSearchBarContainerView.h"

@interface CustomSearchBarContainerView ()
@property (nonatomic, strong) UISearchBar *searchBar;
@end

@implementation CustomSearchBarContainerView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        self.frame = frame;
    }
    return self;
}

-(instancetype) initWithSearchBar:(UISearchBar *) searchBar {
    self = [super initWithFrame:CGRectZero];
    if(self){
        _searchBar = searchBar;
        [self addSubview:_searchBar];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    return [super initWithCoder:aDecoder];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    _searchBar.frame = self.bounds;
}

-(void) setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self setNeedsLayout];
}
@end
