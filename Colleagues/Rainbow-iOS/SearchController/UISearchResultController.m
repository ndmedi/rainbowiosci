/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UISearchResultController.h"
#import "UITools.h"
#import <Rainbow/Tools.h>
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import <Rainbow/ServicesManager.h>
#import "OrderedOptionalSectionedContent.h"
#import "UIRainbowGenericTableViewCell.h"
#if !defined(APP_EXTENSION)
#import "UIGroupsTableViewCell.h"
#import "UICompanyTableViewCell.h"
#import "UIGroupDetailsViewController.h"
#import "UICompanyDetailsViewController.h"
#endif
#import "Room+UIRainbowGenericTableViewCellProtocol.h"
#import "Conversation+UIRainbowGenericTableViewCellProtocol.h"
#import "Contact+UIRainbowGenericTableViewCellProtocol.h"
#import "Contact+Extensions.h"
#import "UIStoryboardManager.h"
#import <Rainbow/ServicesManager.h>
#define kSearchResultKeyRooms @"Bubbles"
#define kSearchResultKeyMyConversation @"My conversations"
#define kSearchResultKeyMyNetwork @"My network"
#define kSearchResultKeyRainbowDirectory @"Rainbow directory"
#define kSearchResultKeyMyContacts @"My contacts"
#define kSearchResultKeyMyLists @"My lists"
#define kSearchResultKeyCompanies @"Companies"
#define kSearchResultKeyEnterpriseContacts @"Enterprise contacts"

#define kSearchResultMakeCall @"Call"

@interface UISearchResultController () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (nonatomic, strong) OrderedOptionalSectionedContent <id> *searchResult;
@property (nonatomic, strong) UIStoryboard * mainStoryboard;
@end

@implementation UISearchResultController

- (void)viewDidLoad {
    [super viewDidLoad];

    _searchResult = [[OrderedOptionalSectionedContent alloc] initWithSections:@[kSearchResultKeyMyConversation, kSearchResultKeyMyNetwork, kSearchResultKeyRooms, kSearchResultKeyRainbowDirectory, kSearchResultKeyMyContacts, kSearchResultKeyEnterpriseContacts, kSearchResultKeyCompanies, kSearchResultMakeCall]];
    
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.sectionIndexColor = [UITools defaultTintColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContactDisplaySettings:) name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
#if !defined(APP_EXTENSION)
    _mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
#endif
}

-(void) changeContactDisplaySettings:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self changeContactDisplaySettings:notification];
        });
        return;
    }
    if([self isViewLoaded]){
        NSLog(@"%@ Reload UI on contact display settings changed", self);
        [self reloadAllData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dealloc {
    _cellSelectedActionBlock = nil;
    _rightViewActionBlock = nil;
    [_searchResult removeAllObjects];
    _searchResult = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
}

-(void) setIsTyping:(BOOL)isTyping {
    _isTyping = isTyping;
    [self.tableView reloadData];
}



-(void) setResults:(NSArray<id> *)results {
    NSLog(@"searchData: setResultsaction starts here ");

    if(_results != results){
        _results = nil;
        _results = results;
        [_searchResult removeAllObjects];
        for (id objectFound in results) {
            if([objectFound isKindOfClass:[Contact class]]){
                Contact *contact = (Contact *)objectFound;
                if(contact.isRainbowUser){
                    if(contact.isInRoster)
                        [self insertContact:contact atKey:kSearchResultKeyMyNetwork];
                    else
                        [self insertContact:contact atKey:kSearchResultKeyRainbowDirectory];
                } else {
                    if(contact.isExternalContact)
                        [self insertContact:contact atKey:kSearchResultKeyEnterpriseContacts];
                    else
                        [self insertContact:contact atKey:kSearchResultKeyMyContacts];
                }
            }
            if([objectFound isKindOfClass:[Conversation class]]){
                Conversation *conversation = (Conversation *)objectFound;
                if(![_searchResult containsObject:conversation.peer inSection:kSearchResultKeyMyNetwork])
                    [self insertConversation:conversation atKey:kSearchResultKeyMyConversation];
            }
            if([objectFound isKindOfClass:[Room class]]){
                Room *room = (Room *)objectFound;
                
                if(![self isPeer:(Peer *)objectFound inSection:kSearchResultKeyMyConversation])
                    [self insertRoom:room atKey:kSearchResultKeyRooms];
            }
            if([objectFound isKindOfClass:[Group class]]){
                Group *group = (Group *)objectFound;
                [self insertGroup:group atKey:kSearchResultKeyMyLists];
            }
            if([objectFound isKindOfClass:[Company class]]){
                Company *company = (Company *) objectFound;
                [self insertCompany:company atKey:kSearchResultKeyCompanies];
            }
            if([objectFound isKindOfClass:[PhoneNumber class]]){
                [_searchResult addObject:objectFound toSection:kSearchResultMakeCall];
            }
        }
    }
    
    _isTyping = NO;
    [self.tableView reloadData];
}

-(void) insertCompany:(Company *) company atKey:(NSString *) key {
    [_searchResult addObject:company toSection:key];
    [[_searchResult sectionForKey:key] sortUsingDescriptors:@[[UITools sortDescriptorForGroupByName]]];
}

-(void) insertGroup:(Group *) group atKey:(NSString *) key {
    [_searchResult addObject:group toSection:key];
    [[_searchResult sectionForKey:key] sortUsingDescriptors:@[[UITools sortDescriptorForGroupByName]]];
}

-(void) insertConversation:(Conversation *) conversation atKey:(NSString *) key {
    if([conversation.peer isKindOfClass:[Room class]]){
        if([((Room *)conversation.peer).topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
            return;
    }
    [_searchResult addObject:conversation toSection:key];
    [[_searchResult sectionForKey:key] sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        
        if([obj1 isKindOfClass:[Conversation class]] && [obj2 isKindOfClass:[Conversation class]]){
            return [((Conversation *)obj1).lastUpdateDate compare:((Conversation *)obj2).lastUpdateDate];
        }
        
        
        if([obj1 isKindOfClass:[Room class]] && [obj2 isKindOfClass:[Room class]]){
            return [((Room *)obj1).displayName caseInsensitiveCompare:((Room *)obj2).displayName];
        }
        return NSOrderedSame;
    }];
}

-(void) insertRoom:(Room *) room atKey:(NSString *) key {
    if([room.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
        return;
    [_searchResult addObject:room toSection:key];
    [[_searchResult sectionForKey:key] sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
      
        if([obj1 isKindOfClass:[Conversation class]] && [obj2 isKindOfClass:[Conversation class]]){
            return [((Conversation *)obj1).lastUpdateDate compare:((Conversation *)obj2).lastUpdateDate];
        }
        
        
        if([obj1 isKindOfClass:[Room class]] && [obj2 isKindOfClass:[Room class]]){
            return [((Room *)obj1).displayName caseInsensitiveCompare:((Room *)obj2).displayName];
        }
        return NSOrderedSame;
    }];
}

-(void) insertContact:(Contact *) contact atKey:(NSString *) key {
    // Ignore myself.
    if (contact == [ServicesManager sharedInstance].myUser.contact) {
        return;
    }
    
    [_searchResult addObject:contact toSection:key];
    
    if([ServicesManager sharedInstance].contactsManagerService.sortByFirstName){
        [[_searchResult sectionForKey:key] sortUsingDescriptors:@[[UITools sortDescriptorForContactByFirstName], [UITools sortDescriptorForContactByLastName]]];
    } else {
        [[_searchResult sectionForKey:key] sortUsingDescriptors:@[[UITools sortDescriptorForContactByLastName], [UITools sortDescriptorForContactByFirstName]]];
    }
}

-(BOOL) isPeer:(Peer*) peer inSection:(NSString*) section {
    NSMutableArray *sectionForKey = [_searchResult sectionForKey:section];
    BOOL found = NO;
    
    for (Conversation *conv in sectionForKey) {
        if([conv.peer isEqual:peer])
            found = YES;
        break;
    }
    
    return found;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_searchResult allNotEmptySections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_searchResult allNotEmptySections] objectAtIndex:section];
    return [[_searchResult sectionForKey:key] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return NSLocalizedString([[_searchResult allNotEmptySections] objectAtIndex:section], nil);
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
    tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:18.0f];
}
- (UIButton *) createCustomButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:nil forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"Checkmark"] forState:UIControlStateSelected];
    [button setBackgroundImage:nil forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor whiteColor]];
    button.layer.borderColor = [UIColor lightGrayColor].CGColor;
    button.layer.borderWidth = 1.0;
    button.clipsToBounds = YES;
    button.layer.cornerRadius = 11.0;
    [button setTitleEdgeInsets:UIEdgeInsetsMake(-4, 4, -4, -4)];
    [button setFrame:CGRectMake(0, 0, 22, 22)];
    return button;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_searchResult allNotEmptySections] objectAtIndex:indexPath.section];
    NSArray<id> *content = [_searchResult sectionForKey:key];
    id object = [content objectAtIndex:indexPath.row];
    if([key isEqualToString:kSearchResultKeyMyConversation] || [key isEqualToString:kSearchResultKeyRooms]){
        if([object isKindOfClass:[Conversation class]]){
        }
    } else if([key isEqualToString:kSearchResultKeyMyContacts] || [key isEqualToString:kSearchResultKeyMyNetwork] || [key isEqualToString:kSearchResultKeyRainbowDirectory] || [key isEqualToString:kSearchResultKeyEnterpriseContacts]){
        Contact *aContact = (Contact *)object;
        if(!aContact.photoData)
           [[ServicesManager sharedInstance].contactsManagerService populateAvatarForContact:aContact];
    } else if ([key isEqualToString:kSearchResultKeyMyLists]){
    } else if([key isEqualToString:kSearchResultKeyCompanies]){
        Company *aCompany = (Company *) object;
        if(!aCompany.logo)
            [[ServicesManager sharedInstance].companiesService populateLogoForCompany:aCompany];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_searchResult allNotEmptySections] objectAtIndex:indexPath.section];
    NSArray<id> *content = [_searchResult sectionForKey:key];
    id object = [content objectAtIndex:indexPath.row];

    if([key isEqualToString:kSearchResultKeyMyConversation] || [key isEqualToString:kSearchResultKeyRooms]){
        
        if([object isKindOfClass:[Conversation class]]){
            
            UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*) [tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey];
            cell.cellObject = (Conversation*)object;
            cell.cellButtonTapHandler = nil;
            return cell;
        } else if ([object isKindOfClass:[Room class]]){
            UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey];
#if !defined(APP_EXTENSION)
            cell.cellButtonTapHandler = ^(UIButton *sender) {
                [self rigthViewClicked:sender];
            };
#else
            cell.cellButtonTapHandler = nil;
#endif
            cell.cellObject = (Room *) object;
            
            return cell;
        } else {
            NSLog(@"No cell found for key %@ and object %@", key, object);
            return nil;
        }
    } else if([key isEqualToString:kSearchResultKeyMyContacts] || [key isEqualToString:kSearchResultKeyMyNetwork] || [key isEqualToString:kSearchResultKeyRainbowDirectory] || [key isEqualToString:kSearchResultKeyEnterpriseContacts]){
        UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
        
        cell.cellObject = (Contact*)object;
#if !defined(APP_EXTENSION)
        cell.cellButtonTapHandler = ^(UIButton *sender) {
            [self rigthViewClicked:sender];
        };
#else
        cell.cellButtonTapHandler = nil;
#endif
        
        return cell;
    } else if ([key isEqualToString:kSearchResultKeyMyLists]){
#if !defined(APP_EXTENSION)
        UIGroupsTableViewCell *cell = (UIGroupsTableViewCell*) [tableView dequeueReusableCellWithIdentifier:@"SearchGroupsCellID"];
        cell.group = (Group *) object;
        return cell;
#endif
    } else if([key isEqualToString:kSearchResultKeyCompanies]){
#if !defined(APP_EXTENSION)
        UICompanyTableViewCell *cell = (UICompanyTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"SearchCompanyCell"];
        cell.company = (Company *) object;
        return cell;
#endif
    } else if ([key isEqualToString:kSearchResultMakeCall]){
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"makeCallCell"];
        cell.textLabel.text = ((PhoneNumber*)object).number;
        cell.imageView.image = [UIImage imageNamed:@"Phone"];
        cell.imageView.tintColor = [UITools defaultTintColor];
        return cell;
    }
    NSLog(@"No cell found for key %@ and object %@", key, object);
    return nil;
}
#if !defined(APP_EXTENSION)
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath){
        NSString *key = [[_searchResult allNotEmptySections] objectAtIndex:indexPath.section];
        NSArray<id> *content = [_searchResult sectionForKey:key];
        id object = [content objectAtIndex:indexPath.row];
       
        if(_cellSelectedActionBlock) {
            [_controllerDelegate dismissSearchControllerAndPerformAction:^{
                _cellSelectedActionBlock(object, key);
            }];
        } else {
            if(_currentViewController){
                if([object isKindOfClass:[Group class]]){
                    UIGroupDetailsViewController *groupDetails = [[UIStoryboardManager sharedInstance].groupDetailsStoryboard instantiateViewControllerWithIdentifier:@"groupDetailsViewControllerID"];
                    groupDetails.group = (Group *)object;
                    [_currentViewController.navigationController pushViewController:groupDetails animated:YES];
                    return;
                }
                
                if([object isKindOfClass:[Company class]]){
                    UICompanyDetailsViewController *companyDetails =
                    [[UIStoryboardManager sharedInstance].companyDetailStoryBoard instantiateViewControllerWithIdentifier:companyDetailsStoryboardIdentifier];
                    companyDetails.company = (Company *) object;
                    companyDetails.fromView = self;
                    [_currentViewController.navigationController pushViewController:companyDetails animated:YES];
                    return;
                }
                
                if([object isKindOfClass:[Contact class]]){
                    Contact *contact = (Contact *)object;
                    
                    if(contact.isRainbowUser && contact.canChatWith) {                        
                        [_controllerDelegate dismissSearchControllerAndPerformAction:^{
                            [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
                        }];
                    } else {
                        if([_uiDelegate respondsToSelector:@selector(presentContactDetailController:)]){
                            [_uiDelegate presentContactDetailController:contact];
                        }
                    }
                } else {
                    [_controllerDelegate dismissSearchControllerAndPerformAction:^{
                        if([object isKindOfClass:[Conversation class]]){
                            [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:((Conversation *)object).peer withCompletionHandler:nil];
                        }
                        if([object isKindOfClass:[Room class]]){
                            [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:(Peer *)object withCompletionHandler:nil];
                        }
                        if([object isKindOfClass:[PhoneNumber class]]){
                            [UITools makeCallToPhoneNumber:(PhoneNumber *)object inController:_currentViewController];
                        }
                    }];
                }
            };
        }
    }
}
#else
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath){
        NSString *key = [[_searchResult allNotEmptySections] objectAtIndex:indexPath.section];
        NSArray<id> *content = [_searchResult sectionForKey:key];
        id object = [content objectAtIndex:indexPath.row];
        if(_cellSelectedActionBlock) {
            [_controllerDelegate dismissSearchControllerAndPerformAction:^{
                _cellSelectedActionBlock(object, key);
            }];
        }
    }
}
#endif

#pragma mark - DNZEmptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = self.isTyping?NSLocalizedString(@"Searching...", nil):NSLocalizedString(@"No result found",nil);
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools boldFontName] size:18.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - Right View action
- (IBAction)rigthViewClicked:(UIButton *)sender {
    
#if !defined(APP_EXTENSION)
    CGPoint point = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    if (indexPath) {
        NSString *key = [[_searchResult allNotEmptySections] objectAtIndex:indexPath.section];
        NSArray<id> *content = [_searchResult sectionForKey:key];
        id object = [content objectAtIndex:indexPath.row];
        if(_rightViewActionBlock)
            _rightViewActionBlock(object, key);
        else {
            if(_currentViewController){
                if([object isKindOfClass:[Contact class]]){
                    Contact *contact = (Contact *)object;
                    if (contact.isPBXContact) {
                        [UITools makeCallToPhoneNumber:[contact.phoneNumbers firstObject] inController:_currentViewController];
                        return;
                    }
                    if(!contact.isRainbowUser){
                        [UITools showInviteContactAlertController:contact fromController:_currentViewController controllerDelegate: _currentViewController withCompletionHandler:nil];
                    } else if (![contact.companyId isEqualToString:[ServicesManager sharedInstance].myUser.contact.companyId] && !contact.isInRoster)
                        [_uiDelegate presentContactDetailController:contact];
                    else if([_uiDelegate respondsToSelector:@selector(presentContactDetailController:)]){
                        [_uiDelegate presentContactDetailController:contact];
                    }
                }
                if([object isKindOfClass:[Room class]]){
                    UIRoomDetailsViewController * roomDetails = [[UIStoryboardManager sharedInstance].roomDetailsStoryBoard instantiateViewControllerWithIdentifier:@"roomDetailsViewControllerID"];
                    roomDetails.room = (Room *)object;
                    [_currentViewController.navigationController pushViewController:roomDetails animated:YES];
                }
            }
        }
    }
#endif
}

-(void) reloadAllData {
    NSArray *resultCopy = [_results copy];
    [_searchResult removeAllObjects];
    _results = nil;
    [self setResults:resultCopy];
    [self.tableView reloadData];
}

-(void) didLogout:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    
    [_controllerDelegate dismissSearchControllerAndPerformAction:nil];
}

@end
