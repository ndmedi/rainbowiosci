/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import <Rainbow/Contact.h>

@class CustomSearchController;

@protocol UISearchResultDisplayDetailControllerProtocol <NSObject>
- (void) presentContactDetailController:(Contact* _Nonnull) contact;
@end

@protocol UISearchResultControllerProtocol <NSObject>
-(void) dismissSearchControllerAndPerformAction:(void (^ __nullable)(void)) action;
@end

@interface UISearchResultController : UITableViewController

@property (nonatomic, strong) NSArray<id> * _Nullable results;

@property (nonatomic) BOOL isTyping;
@property (nonatomic, copy) void (^ __nullable cellSelectedActionBlock)(id _Nonnull object, NSString  * _Nonnull  sectionName);
@property (nonatomic, copy) void (^ __nullable rightViewActionBlock)(id _Nonnull object, NSString  * _Nonnull  sectionName);

@property (nonatomic, weak) UIViewController* __nullable currentViewController;
@property (nonatomic, assign) id<UISearchResultDisplayDetailControllerProtocol> __nullable uiDelegate;
@property (nonatomic, assign) id<UISearchResultControllerProtocol> __nullable controllerDelegate;

@end
