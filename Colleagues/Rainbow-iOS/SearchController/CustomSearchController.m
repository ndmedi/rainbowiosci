/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CustomSearchController.h"
#import "UISearchResultController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/ContactsManagerService.h>
#import "UISearchController+StatusBarStyle.h"
#import "Firebase.h"
@interface CustomSearchController ()  <UISearchBarDelegate, UISearchControllerDelegate, UISearchControllerDelegate, UISearchResultControllerProtocol>

@property (nonatomic, strong) NSArray *rosterSearchedUsers;
@property (nonatomic, strong) NSArray *localSearchedContacts;
@property (nonatomic, strong) NSArray *serverSearchedUsers;
@property (nonatomic, strong) ServicesManager *serviceManager;
@property (nonatomic, strong) ContactsManagerService *contactsManager;
@property (nonatomic, weak) id<CustomSearchControllerDelegate> customSearchDelegate;
@property (nonatomic, strong) NSOperationQueue *searchOperationQueue;

@end

@implementation CustomSearchController

-(instancetype) initWithSearchResultsController:(UISearchResultController*)searchResultController customSearchControllerDelegate:(id<CustomSearchControllerDelegate>) customSearchDelegate {
    self = [super initWithSearchResultsController:searchResultController];
    if(self){
        searchResultController.controllerDelegate = self;
        _customSearchDelegate = customSearchDelegate;
        self.delegate = self;
        self.hidesNavigationBarDuringPresentation = NO;
        self.searchBar.showsCancelButton = NO;
        [ self.searchBar setValue:NSLocalizedString(@"Done", @"Done") forKey:@"_cancelButtonText"];
        self.searchBar.hidden = NO;
        self.searchBar.translucent = YES;
        self.searchBar.backgroundImage = [UITools imageFromColor:[UIColor clearColor]];
        self.searchBar.barTintColor = [UITools defaultBackgroundColor];
        self.searchBar.tintColor = [UITools defaultTintColor];
        self.searchBar.placeholder = NSLocalizedString(@"People, bubbles, ...", nil);
        self.searchBar.searchTextPositionAdjustment = UIOffsetMake(5.0f, 0.0f);
        self.searchBar.delegate = self;
        [self.searchBar setBackgroundImage:[UIImage new]];
        [self.searchBar setScopeBarBackgroundImage:[UIImage new]];
        
        CGSize size = CGSizeMake(30, 30);
        UIGraphicsBeginImageContextWithOptions(size, NO, 1);
        [[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0,0,30,30) cornerRadius:2.0] addClip];
        [[UIColor whiteColor] setFill];
        
        UIRectFill(CGRectMake(0, 0, size.width, size.height));
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [self.searchBar setSearchFieldBackgroundImage:image forState:UIControlStateNormal];
    
        self.searchBar.barStyle = UIBarStyleDefault;
        if([[UIDevice currentDevice].systemVersion compare:@"9.1" options:NSNumericSearch] == NSOrderedDescending){
            self.obscuresBackgroundDuringPresentation = YES;
        }
        
        _serviceManager = [ServicesManager sharedInstance];
        _contactsManager = _serviceManager.contactsManagerService;
        _searchOperationQueue = [NSOperationQueue new];
        _searchOperationQueue.maxConcurrentOperationCount = 1;
        
        _handleDismissOnCancel = YES;
        _searchScope = SearchScopeAll;
    }
    return self;
}

-(void) dealloc {
    [self.searchBar removeFromSuperview];
    [_searchOperationQueue cancelAllOperations];
    _searchOperationQueue = nil;
    _rosterSearchedUsers = nil;
    _serverSearchedUsers = nil;
    _localSearchedContacts = nil;
    _contactsManager = nil;
    _serviceManager = nil;
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    CustomSearchController * __weak blockSelf = self;
    // you can cancel existing operations here if you want
    [_searchOperationQueue cancelAllOperations];
    // Reset also the UI
    ((UISearchResultController*)self.searchResultsController).isTyping = YES;
    ((UISearchResultController*)self.searchResultsController).results = nil;
    NSString *outsideTextAsItWasWhenStarted = [NSString stringWithString:searchText];
//    NSLog(@"Queueing with '%@'", outsideTextAsItWasWhenStarted);
    [_searchOperationQueue addOperationWithBlock:^{
        NSString *textAsItWasWhenStarted = [NSString stringWithString:outsideTextAsItWasWhenStarted] ;
        [NSThread sleepForTimeInterval:0.5];
        if (blockSelf.searchOperationQueue.operationCount == 1) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (![textAsItWasWhenStarted isEqualToString:blockSelf.searchBar.text]) {
//                    NSLog(@"NOT up to date with '%@'", textAsItWasWhenStarted);
                } else {
//                    NSLog(@"Up to date with '%@'", textAsItWasWhenStarted);
                    [blockSelf performSelectorInBackground:@selector(performSearchWithSearchText:) withObject:textAsItWasWhenStarted];
                }
            });
        } else {
//            NSLog(@"Skipped as out of date with '%@'", textAsItWasWhenStarted);
        }
    }];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    ((UISearchResultController*)self.searchResultsController).isTyping = YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    ((UISearchResultController*)self.searchResultsController).isTyping = NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self dismissSearchController];
   // NSMutableArray * selectedResultsArray = ((UISearchResultController*)self.searchResultsController).selectedresultsMuttableArray;
    [_customSearchDelegate customSearchController:self didPressCancelButton:searchBar];
}

#pragma mark - UISearchControllerDelegate
- (void)willPresentSearchController:(UISearchController *)searchController {
    if([_customSearchDelegate respondsToSelector:@selector(willPresentSearchController:)])
        [_customSearchDelegate willPresentSearchController:self];
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.searchBar becomeFirstResponder];
    });
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    if([_customSearchDelegate respondsToSelector:@selector(willDismissSearchController:)])
        [_customSearchDelegate willDismissSearchController:self];
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    [self.searchBar resignFirstResponder];
    
    if(_handleDismissOnCancel){
        // This happens when we click on the gray zone, and we want to act like a cancel.
        if (((UISearchResultController*)self.searchResultsController).results.count == 0){
            [self dismissSearchController];
            [_customSearchDelegate customSearchController:self didPressCancelButton:self.searchBar];
        }
    }
}

-(void) performSearchWithSearchText:(NSString *) searchText {
    if(searchText.length > 0){
        NSMutableArray *results = [NSMutableArray array];
        
        [results addObjectsFromArray:[_contactsManager searchContactsWithPattern:searchText]];
        
        if (_searchScope & SearchScopeLocalContacts)
            [results addObjectsFromArray:[_contactsManager searchAdressBookWithPattern:searchText]];
        
        if(_searchScope & SearchScopeConversations)
            [results addObjectsFromArray:[[ServicesManager sharedInstance].conversationsManagerService searchConversationWithPattern:searchText]];
        
        if(_searchScope & SearchScopeRooms)
            [results addObjectsFromArray:[[ServicesManager sharedInstance].roomsService searchRoomWithPattern:searchText]];
        
        if(_searchScope & SearchScopeGroups)
            [results addObjectsFromArray:[[ServicesManager sharedInstance].groupsService searchGroupsWithPattern:searchText]];
        if(_searchScope & SearchScopeCall){
            // Trim whitespace and unicode special characters
            NSString *trimmedPhoneNumber = [UITools trimAndRemoveUnicodeFormatFromNumber:searchText];
            if([trimmedPhoneNumber rangeOfString:@"^([(\\+)|(00)]?)[0-9]{2,16}$" options:NSRegularExpressionSearch].location != NSNotFound){
                PhoneNumber *aPhoneNumber = [[PhoneNumber alloc] initWithPhoneNumberString:trimmedPhoneNumber];
                [results addObject:aPhoneNumber];
            }
        }
        if(searchText.length >= 2){
            if(_searchScope & SearchScopeRemoteContacts){
                [_contactsManager searchRemoteContactsWithPattern:searchText withCompletionHandler:^(NSString *searchPattern, NSArray<Contact *> *foundContacts) {
                    NSMutableArray *result2 = [NSMutableArray arrayWithArray:((UISearchResultController*)self.searchResultsController).results];
                    [foundContacts enumerateObjectsUsingBlock:^(Contact * contact, NSUInteger idx, BOOL * stop) {
                        if(![result2 containsObject:contact])
                            [result2 addObject:contact];
                    }];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        ((UISearchResultController*)self.searchResultsController).results = result2;
                    });
                }];
            }
            
            if(_searchScope & SearchScopeCompanies){
                [[ServicesManager sharedInstance].companiesService searchRainbowCompaniesWithPattern:searchText withCompletionBlock:^(NSString *searchPattern, NSArray<Company *> *foundCompanies) {
                    NSMutableArray *resultsWithCompanies = [NSMutableArray arrayWithArray:((UISearchResultController*)self.searchResultsController).results];
                    [resultsWithCompanies addObjectsFromArray:foundCompanies];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        ((UISearchResultController*)self.searchResultsController).results = resultsWithCompanies;
                    });
                }];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            ((UISearchResultController*)self.searchResultsController).results = results;
        });
    }
}

- (NSMutableArray *) removeContactsAlreadyInRoster:(NSArray *) searchResult {
    NSMutableArray* contacts = [NSMutableArray new];
    [searchResult enumerateObjectsUsingBlock:
     ^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
         Contact* contact = obj;
         if ( ! [_rosterSearchedUsers containsObject:contact] ) {
             [contacts addObject:contact];
         }
     }
     ];
    return contacts;
}

-(void) dismissSearchControllerAndPerformAction:(void (^ __nullable)(void)) action {
    if(_handleDismissOnCancel){
        [self dismissViewControllerAnimated:YES completion:^{
            if(action)
                action();
        }];
    } else {
        if(action)
            action();
    }
    self.active = NO;
}

- (void) dismissSearchController {
    if(self.active && _handleDismissOnCancel){
            [self dismissViewControllerAnimated:YES completion:nil];
        self.active = NO;
    }
}

@end
