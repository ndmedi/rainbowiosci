/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>

@interface UIStoryboardManager : NSObject
+(UIStoryboardManager *) sharedInstance;
@property (nonatomic, readonly) UIStoryboard *rtcCallStoryBoard;
@property (nonatomic, readonly) UIStoryboard *welcomePagesStoryBoard;
@property (nonatomic, readonly) UIStoryboard *networkLostStoryBoard;
@property (nonatomic, readonly) UIStoryboard *guestModeStoryBoard;
@property (nonatomic, readonly) UIStoryboard *addAttendeesStoryBoard;
@property (nonatomic, readonly) UIStoryboard *joinMeetingStoryBoard;
@property (nonatomic, readonly) UIStoryboard *companyDetailStoryBoard;
@property (nonatomic, readonly) UIStoryboard *advancedSettingsStoryBoard;
@property (nonatomic, readonly) UIStoryboard *contactsDetailsStoryBoard;
@property (nonatomic, readonly) UIStoryboard *myInfosStoryBoard;
@property (nonatomic, readonly) UIStoryboard *myFilesStoryBoard;
@property (nonatomic, readonly) UIStoryboard *roomDetailsStoryBoard;
@property (nonatomic, readonly) UIStoryboard *messagesStoryBoard;
@property (nonatomic, readonly) UIStoryboard *searchStoryboard;
@property (nonatomic, readonly) UIStoryboard *groupDetailsStoryboard;
@property (nonatomic, readonly) UIStoryboard *shareFilesStoryboard;

// Not used
@property (nonatomic, readonly) UIStoryboard *organizeStoryBoard;
@end
