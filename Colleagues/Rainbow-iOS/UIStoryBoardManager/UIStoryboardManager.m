/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIStoryboardManager.h"
#import <UIKit/UIKit.h>

static UIStoryboardManager *singleton = nil;
static UIStoryboard *rtcStoryboard = nil;
static UIStoryboard *welcomePagesStoryboard = nil;
static UIStoryboard *networkLostStoryBoard = nil;
static UIStoryboard *guestModeStoryBoard = nil;
static UIStoryboard *addAttendeesStoryBoard = nil;
static UIStoryboard *joinMeetingStoryBoard = nil;
static UIStoryboard *companyDetailStoryBoard = nil;
static UIStoryboard *advancedSettingsStoryBoard = nil;
static UIStoryboard *contactsDetailsStoryBoard = nil;
static UIStoryboard *myInfosStoryBoard = nil;
static UIStoryboard *myFilesStoryBoard = nil;
static UIStoryboard *roomDetailsStoryBoard = nil;
static UIStoryboard *organizeStoryBoard = nil;
static UIStoryboard *messagesStoryBoard = nil;
static UIStoryboard *searchStoryboard = nil;
static UIStoryboard *groupDetailsStoryboard = nil;
static UIStoryboard *shareFilesStoryboard = nil;

@implementation UIStoryboardManager

+(UIStoryboardManager *) sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[UIStoryboardManager alloc] init];
    });
    return singleton;
}

-(UIStoryboard *) rtcCallStoryBoard {
    if(!rtcStoryboard)
        rtcStoryboard = [UIStoryboard storyboardWithName:@"WebRTCCall" bundle:[NSBundle mainBundle]];
        
        return rtcStoryboard;
}

-(UIStoryboard *) welcomePagesStoryBoard {
    if(!welcomePagesStoryboard)
        welcomePagesStoryboard = [UIStoryboard storyboardWithName:@"WelcomePages" bundle:[NSBundle mainBundle]];
    
    return welcomePagesStoryboard;
}

-(UIStoryboard *) networkLostStoryBoard {
    if(!networkLostStoryBoard)
        networkLostStoryBoard = [UIStoryboard storyboardWithName:@"NetworkLost" bundle:[NSBundle mainBundle]];
    
    return networkLostStoryBoard;
}

-(UIStoryboard *) guestModeStoryBoard {
    if(!guestModeStoryBoard)
        guestModeStoryBoard = [UIStoryboard storyboardWithName:@"GuestMode" bundle:[NSBundle mainBundle]];
    
    return guestModeStoryBoard;
}

-(UIStoryboard *) addAttendeesStoryBoard {
    if(!addAttendeesStoryBoard)
        addAttendeesStoryBoard = [UIStoryboard storyboardWithName:@"AddAttendee" bundle:[NSBundle mainBundle]];
    
    return addAttendeesStoryBoard;
}

-(UIStoryboard *) joinMeetingStoryBoard {
    if(!joinMeetingStoryBoard){
        joinMeetingStoryBoard = [UIStoryboard storyboardWithName:@"JoinMeeting" bundle:[NSBundle mainBundle]];
    }
    return joinMeetingStoryBoard;
}

-(UIStoryboard *) companyDetailStoryBoard {
    if(!companyDetailStoryBoard){
        companyDetailStoryBoard = [UIStoryboard storyboardWithName:@"CompanyDetail" bundle:[NSBundle mainBundle]];
    }
    return companyDetailStoryBoard;
}

-(UIStoryboard *) advancedSettingsStoryBoard {
    if(!advancedSettingsStoryBoard){
        advancedSettingsStoryBoard = [UIStoryboard storyboardWithName:@"AdvancedSettings" bundle:[NSBundle mainBundle]];
    }
    return advancedSettingsStoryBoard;
}

-(UIStoryboard *) contactsDetailsStoryBoard {
    if(!contactsDetailsStoryBoard){
        contactsDetailsStoryBoard = [UIStoryboard storyboardWithName:@"ContactsDetails" bundle:[NSBundle mainBundle]];
    }
    return contactsDetailsStoryBoard;
}


-(UIStoryboard *) myInfosStoryBoard {
    if(!myInfosStoryBoard){
        myInfosStoryBoard = [UIStoryboard storyboardWithName:@"MyInfos" bundle:[NSBundle mainBundle]];
    }
    return myInfosStoryBoard;
}

-(UIStoryboard *) myFilesStoryBoard {
    if(!myFilesStoryBoard){
        myFilesStoryBoard = [UIStoryboard storyboardWithName:@"MyFiles" bundle:[NSBundle mainBundle]];
    }
    return myFilesStoryBoard;
}

-(UIStoryboard *) roomDetailsStoryBoard {
    if(!roomDetailsStoryBoard){
        roomDetailsStoryBoard = [UIStoryboard storyboardWithName:@"RoomDetails" bundle:[NSBundle mainBundle]];
    }
    return roomDetailsStoryBoard;
}

-(UIStoryboard *) organizeStoryBoard {
    if(!organizeStoryBoard){
        organizeStoryBoard = [UIStoryboard storyboardWithName:@"Organize" bundle:[NSBundle mainBundle]];
    }
    return organizeStoryBoard;
}

-(UIStoryboard *) messagesStoryBoard {
    if(!messagesStoryBoard){
        messagesStoryBoard = [UIStoryboard storyboardWithName:@"Messages" bundle:[NSBundle mainBundle]];
    }
    return messagesStoryBoard;
}

-(UIStoryboard *) searchStoryboard {
    if(!searchStoryboard){
        searchStoryboard = [UIStoryboard storyboardWithName:@"Search" bundle:[NSBundle mainBundle]];
    }
    return searchStoryboard;
}

-(UIStoryboard *) groupDetailsStoryboard {
    if(!groupDetailsStoryboard){
        groupDetailsStoryboard = [UIStoryboard storyboardWithName:@"GroupDetails" bundle:[NSBundle mainBundle]];
    }
    return groupDetailsStoryboard;
}

-(UIStoryboard *) shareFilesStoryboard {
    if(!shareFilesStoryboard){
        shareFilesStoryboard = [UIStoryboard storyboardWithName:@"ShareFiles" bundle:[NSBundle mainBundle]];
    }
    return  shareFilesStoryboard;
}

@end
