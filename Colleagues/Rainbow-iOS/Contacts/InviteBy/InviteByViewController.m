/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Rainbow/ServicesManager.h>
#import "InviteByViewController.h"
#import "CLTokenInputView.h"
#import "UITools.h"
#import <Rainbow/Tools.h>
#import <Rainbow/EmailAddress.h>
#import <Rainbow/PhoneNumber.h>
#import <MessageUI/MessageUI.h>

@interface InviteByViewController () <CLTokenInputViewDelegate, MFMessageComposeViewControllerDelegate>
@property (nonatomic, strong) Invitation *pendingInvitation;
@end

@implementation InviteByViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Invite a contact", nil);
    
    [_inviteButton setTitle:NSLocalizedString(@"Invite", nil) forState:UIControlStateNormal];
    [UITools applyCustomFontTo:_inviteButton.titleLabel];
    _inviteButton.tintColor = [UITools defaultTintColor];
    [_cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [UITools applyCustomFontTo:_cancelButton.titleLabel];
    _cancelButton.tintColor = [UITools defaultTintColor];
    
    _input.placeholderText = NSLocalizedString(@"Enter an email or a phone number", nil);
    _input.tintColor = [UITools defaultTintColor];
    _input.fieldColor = [UIColor darkGrayColor];
    _input.drawBottomBorder = YES;
    _input.backgroundColor = [UIColor whiteColor];
    _input.keyboardType = UIKeyboardTypeDefault;
    _input.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _input.delegate = self;
    _input.autoresizesSubviews = YES;
    
    _tableView.tableFooterView = [UIView new];
    _tableView.backgroundColor = [UITools defaultBackgroundColor];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)viewWillAppear:(BOOL)animated {
    _inviteButton.enabled = NO;
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
    if (!_input.editing) {
        [_input beginEditing];
        [_input becomeFirstResponder];
    }

    [super viewDidAppear:animated];
}

-(void) viewDidDisappear:(BOOL)animated {
    if(_dimView)
        [_dimView removeFromSuperview];
    if(!_input.editing)
        [_input endEditing];
    [_input resignFirstResponder];
    
    [super viewWillDisappear:animated];
}

- (void)tokenInputView:(CLTokenInputView *)view didChangeHeightTo:(CGFloat)height {
    _inputHeightConstraint.constant = height;
 }

- (void)tokenInputView:(CLTokenInputView *)view didChangeText:(NSString *)text {
    // Validate is an email
    if(![self isEmail:text] && ![self isPhoneNumber:text] ) {
        _inviteButton.enabled = NO;
    } else {
        _inviteButton.enabled = YES;
    }
}

- (CLToken *)tokenInputView:(CLTokenInputView *)view tokenForText:(NSString *)text {
    return nil;
}

- (BOOL)tokenInputViewShouldReturn:(CLTokenInputView *)view {
    return NO;
}

- (IBAction)didTapOnUIButon:(UIButton *)sender {
    if([sender isEqual:_cancelButton]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    sender.enabled = NO;
    
    // Validate is an email
    if([self isEmail: _input.text]) {
        __block MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString(@"Sending invitation ...", nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:^{}];

        [hud show:YES];
        
        [[ServicesManager sharedInstance].contactsManagerService inviteByEmail:_input.text withCompletionHandler:^(Invitation * _Nullable invitation) {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [hud hide:YES];
                
                if(invitation) {
                    _input.text = nil;
                    [self dismissViewControllerAnimated:YES completion:nil];
                } else {
                    sender.enabled = YES;
                }
            });
        }];
    }
    
    // or a phone number: format is +00123456 (no space)
    else if( [self isPhoneNumber: _input.text] && [MFMessageComposeViewController canSendText] ) {
        __block MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString(@"Preparing invitation...", nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:^{}];

        [hud show:YES];
        
        [[ServicesManager sharedInstance].contactsManagerService inviteByPhoneNumber:_input.text withCompletionHandler:^(Invitation *invitation) {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [hud hide:YES];
                
                if(invitation) {
                    _input.text = nil;
                    _pendingInvitation = nil;
                    _pendingInvitation = invitation;
                    if([MFMessageComposeViewController canSendText]) {
                        [UITools showSMSViewControllerForInvitation:invitation inController:self controllerDelegate:self];
                    } else {
                        NSLog(@"MFMessageComposeViewController not available");
                    }
                } else {
                    sender.enabled = YES;
                }
            });
        }];
    }
    
    // Not a valid value
    else {
        sender.enabled = YES;
    }
}

-(BOOL) isEmail:(NSString *) email {
    return [Tools isValidEmailAddress: email];
}

-(BOOL) isPhoneNumber:(NSString *) phone {
    return [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"^((\\+)|(00))?[0-9]{6,14}$"] evaluateWithObject: _input.text];
}

-(MBProgressHUD *) showHUDWithMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    if( message ) {
        hud.labelText = message;
    }
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}

- (void)messageComposeViewController:(nonnull MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    if( result == MessageComposeResultCancelled || result == MessageComposeResultFailed ) {
        NSLog(@"MessageComposeResultCancelled or MessageComposeResultFailed");
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if(_pendingInvitation){
                Contact * aContact = (Contact *)_pendingInvitation.peer;
                [[ServicesManager sharedInstance].contactsManagerService deleteInvitationWithID:aContact.sentInvitation];
            }
        });
    } else {
        NSLog(@"MessageComposeResultSent");
        if(_pendingInvitation)
            _pendingInvitation = nil;
    }
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
