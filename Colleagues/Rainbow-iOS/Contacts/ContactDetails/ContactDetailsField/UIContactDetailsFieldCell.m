/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIContactDetailsFieldCell.h"
#import <Rainbow/PhoneNumber.h>
#import <Rainbow/EmailAddress.h>
#import <Rainbow/PostalAddress.h>
#import <Rainbow/Tools.h>
#import "UITools.h"
#import <Rainbow/ServicesManager.h>


@interface UIContactDetailsFieldCell ()
@property (nonatomic, weak) IBOutlet UILabel* fieldTypeLabel;
@property (nonatomic, weak) IBOutlet UILabel* fieldValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fieldTypeTopLayoutConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fieldTypeLeftLayoutConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fieldValueLeftLayoutConstraint;
@end

@implementation UIContactDetailsFieldCell
@synthesize content = _content;
@synthesize highlightedField = _highlightedField;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void) awakeFromNib {
    [super awakeFromNib];
    _fieldTypeLabel.textColor = [UIColor lightGrayColor];
    _fieldValueLabel.textColor = [UITools defaultTintColor];
    [UITools applyCustomFontTo:_fieldTypeLabel];
    [UITools applyCustomFontTo:_fieldValueLabel];
}

-(void) prepareForReuse {
    [super prepareForReuse];
    _fieldTypeLabel.textColor = [UIColor lightGrayColor];
    _fieldValueLabel.textColor = [UITools defaultTintColor];
}

-(void) commonInit {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    switch (_content.type) {
        case SectionContentTypeChangePassword:{
            self.userInteractionEnabled = NO;
            break;
        }
        default:
            break;
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    switch (_content.type) {
        case SectionContentTypeChangePassword:{
            self.userInteractionEnabled = YES;
            break;
        }
        default:
            break;
    }
}

-(void) setContent:(SectionContent *)content {
    _content = content;
    _fieldValueLabel.hidden = NO;
    _fieldTypeTopLayoutConstraint.constant = 0;
    _fieldTypeLeftLayoutConstraint.constant = 20;
    _fieldValueLeftLayoutConstraint.constant = 20;
    _fieldValueLabel.numberOfLines = 1;
    
    switch(_content.type) {
        case SectionContentTypeServicePlan: {
            _fieldTypeLabel.text = NSLocalizedString(@"Rainbow service plan", nil);
            NSString *licenceName = @"";
            MyUser *myUser = [ServicesManager sharedInstance].myUser;
            
            // Same mechanic on Android --> Loop and get the higher licence but with containsString (example: Enterprise Demo will be the first case)
            for (NSString *profileName in myUser.profilesName) {
                if ([profileName containsString:@"Enterprise"])
                    licenceName = profileName;
                else if ([profileName containsString:@"Business"] && ![licenceName containsString:@"Enterprise"])
                    licenceName = profileName;
                else if ([profileName containsString:@"Essential"] && ![licenceName containsString:@"Enterprise"] && ![licenceName containsString:@"Business"])
                    licenceName = profileName;
            }
            
            _fieldValueLabel.text = licenceName;
            break;
        }
        case SectionContentTypePhoneNumber: {
            _image.image = [UIImage imageNamed:@"Phone"];
            PhoneNumber* pn = (PhoneNumber*)_content.content;
            if (pn.deviceType == PhoneNumberDeviceTypeMobile){
                _image.image = [UIImage imageNamed:@"invitation_mobile"];
                switch (pn.type) {
                    case PhoneNumberTypeHome:
                        _fieldTypeLabel.text = NSLocalizedString(@"Personal Mobile", nil);
                        break;
                    case PhoneNumberTypeWork:
                        _fieldTypeLabel.text = NSLocalizedString(@"Professional Mobile", nil);
                        break;
                    case PhoneNumberTypeOther:
                        _fieldTypeLabel.text = NSLocalizedString(@"Professional Mobile", nil);
                        break;
                    default:
                        _fieldTypeLabel.text = NSLocalizedString(@"Cell", nil);
                        break;
                }
            } else if (pn.deviceType == PhoneNumberDeviceTypeLandline){
                switch (pn.type) {
                    case PhoneNumberTypeHome:
                        _fieldTypeLabel.text = NSLocalizedString(@"Personal", nil);
                        break;
                    case PhoneNumberTypeWork:
                        _fieldTypeLabel.text = NSLocalizedString(@"Professional", nil);
                        break;
                    default:
                        _fieldTypeLabel.text = NSLocalizedString(@"Work", nil);
                        break;
                }
            } else {
                _fieldTypeLabel.text = NSLocalizedString(pn.label, nil);
            }
            if([ServicesManager sharedInstance].myUser.contact.countryCode != pn.countryCode && pn.numberE164){
                _fieldValueLabel.text = pn.numberE164;
            } else {
                _fieldValueLabel.text = pn.number;
            }
            break;
        }
        case SectionContentTypeEmailAddress: {
            EmailAddress* emailAddress = (EmailAddress*)_content.content;
            if (emailAddress.type == EmailAddressTypeHome)
                _fieldTypeLabel.text = [NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"Email", nil), NSLocalizedString(@"Home", nil)];
            else
                _fieldTypeLabel.text = NSLocalizedString(@"Email", nil);
            _fieldValueLabel.text = emailAddress.address;
            _image.image = [UIImage imageNamed:@"Mail"];
            break;
        }
        case SectionContentTypePostalAddress: {
            PostalAddress *pa = (PostalAddress *)_content.content;
            _fieldTypeLabel.text = NSLocalizedString(@"Address", nil);
            _fieldValueLabel.text = [pa stringRepresentation];
            _fieldValueLabel.numberOfLines = 10;
            _image.image = [UIImage imageNamed:@"Address"];
            break;
        }
        case SectionContentTypeWebsite: {
            _fieldTypeLabel.text = NSLocalizedString(@"Website", nil);
            _fieldValueLabel.text = (NSString *)_content.content;
            _image.image = [UIImage imageNamed:@"website"];
            break;
        }
        case SectionContentTypeStartConversation: {
            _fieldTypeLabel.text = NSLocalizedString(@"Start a conversation", nil);
            _fieldValueLabel.hidden = YES;
            _image.image = [UIImage imageNamed:@"chat"];
            break;
        }
        case SectionContentTypeAddInRoster: {
            _fieldTypeLabel.text = NSLocalizedString(@"Add to my network", nil);
            _fieldValueLabel.hidden = YES;
            _image.image = [UIImage imageNamed:@"AddContact"];
            break;
        }
        case SectionContentTypeInvite: {
            _fieldTypeLabel.text = NSLocalizedString(@"Invite", nil);
            _fieldValueLabel.hidden = YES;
            break;
        }
        case SectionContentTypeWebRTCVideoCall:
        case SectionContentTypeWebRTCAudioCall:{
            if(_content.type == SectionContentTypeWebRTCAudioCall)
                _fieldTypeLabel.text = NSLocalizedString(@"RTC audio call", nil);
            else
                _fieldTypeLabel.text = NSLocalizedString(@"RTC video call", nil);
            _fieldValueLabel.hidden = YES;
            _image.image = [UIImage imageNamed:@"callWebRTC"];
            break;
        }
        case SectionContentTypeGroups:
        case SectionContentTypeJobCompany:
            // Not handled in the class
            break;
        case SectionContentTypeChangePassword:
            _fieldTypeLabel.text = NSLocalizedString(@"Change my password", nil);
            _fieldValueLabel.hidden = YES;
            _image.image = [UIImage imageNamed:@"key"];
            break;
            
        case SectionContentTypeDeleteMySelf:
            break;
    }
    
    _image.tintColor = [UITools defaultTintColor];
    
    // Move type label to cell center when no value is defined
    if(_fieldValueLabel.hidden) {
        _fieldTypeTopLayoutConstraint.constant = -8;
        _fieldTypeLabel.textColor = [UITools defaultTintColor];
    }
    
    if(_image.image == nil) {
        _fieldTypeLeftLayoutConstraint.constant = -_image.frame.size.width;
        _fieldValueLeftLayoutConstraint.constant = -_image.frame.size.width;
    }
    
    if(![ServicesManager sharedInstance].loginManager.isConnected)
        [self didLostConnection:nil];
    else
        [self didReconnect:nil];
    
    [self setNeedsDisplay];
}

-(SectionContent*) content {
    return _content;
}

-(void) setHighlightedField:(BOOL)highlightedField {
    _highlightedField = highlightedField;
    _fieldValueLabel.textColor = (_highlightedField)?[UITools defaultTintColor]:[UIColor blackColor];
}

-(BOOL) highlightedField {
    return _highlightedField;
}

-(NSString *) stringToCopy {
    return _fieldValueLabel.text;
}

-(float) computedHeight {
    
    // Hide features for guest user
    if (([_fieldTypeLabel.text isEqualToString:NSLocalizedString(@"Add to my network", nil)] ||
        [_fieldTypeLabel.text isEqualToString:NSLocalizedString(@"Change my password", nil)]) &&
        [ServicesManager sharedInstance].myUser.isGuest) {
        return 0.0f;
    }
    
    float totalHeight = 20.0f;
    
    if (!_fieldTypeLabel.hidden) {
        CGRect typeRect = [_fieldTypeLabel.text boundingRectWithSize:CGSizeMake(_fieldTypeLabel.frame.size.width, CGFLOAT_MAX)
                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:_fieldTypeLabel.font.pointSize]}
                                                             context:nil];
        
        float typeHeight = typeRect.size.height;
        
        // If the type field is alone and the icon is present, then the icon might be higher than the type label.
        if (_fieldValueLabel.hidden && _image) {
            typeHeight = MAX(typeHeight, _image.frame.size.height);
        }
        
        totalHeight += typeHeight + 5.0;
    }
    
    if (!_fieldValueLabel.hidden) {
        CGRect valueRect = [_fieldValueLabel.text boundingRectWithSize:CGSizeMake(_fieldValueLabel.frame.size.width, CGFLOAT_MAX)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:_fieldValueLabel.font.pointSize]}
                                                       context:nil];
        totalHeight += valueRect.size.height;
    }
    
    return totalHeight;
}

@end
