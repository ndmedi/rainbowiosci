/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIContactDetailsViewController.h"
#import "UIRoomDetailsViewController.h"
#import "UIContactDetailsFieldCell.h"
#import "UITools.h"
#import <Rainbow/EmailAddress.h>
#import "MBProgressHUD.h"
#import "UIMessagesViewController.h"
#import "UIContactsTableViewController.h"
#import <MessageUI/MessageUI.h>
#import <Rainbow/ServicesManager.h>
#import <CoreLocation/CoreLocation.h>
#import "Contact+Extensions.h"
#import "UIContactGroupListCell.h"
#import "UIContactSelfDeleteCell.h"
#import "UIGroupListViewController.h"
#import "UIContactsDetailsViewController+Internal.h"
#import "RTCCallViewController.h"
#import "ChangePasswordViewController.h"
#import "UICallLogsDetailsViewController.h"
#import "MFMessageComposeViewController+StatusBarStyle.h"
#import <Rainbow/NSDate+Utilities.h>

#import "ContactDetailsInviteToJoinTableViewCell.h"
#import "ContactRainbowActionsCell.h"

#import "UIStoryboardManager.h"

#define kContactInviteToJoinCellIdentifier @"contactInviteToJoinCellID"
#define kContactRainbowActionsCellIdentifier @"contactRainbowActionsCell"
#define kContactFielCellIdentifier @"contactFieldCell"
#define kContactGroupListCellIdentifier @"contactGroupListCell"
#define kContactSelfDeleteAccountCellIdentifier @"contactSelfDeleteAccountCell"

// Sections
#define kServicePlanSection @"ServicePlan"
#define kContactTypeSection @"ContactType"
#define kPhoneSection @"Phones"
#define kEmailSection @"Emails"
#define kAdressSection @"Adresses"
#define kWebpageSection @"Webpages"
#define kRainbowActionSection @"RainbowSection"
#define kOtherSection @"Other"
#define kUserSection @"User"

@interface UIContactDetailsViewController () <MFMessageComposeViewControllerDelegate, MBProgressHUDDelegate>{
    BOOL isNetworkConnected;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *fullScreenTapGesture;
@property (strong, nonatomic) IBOutlet UIView *deleteWarningMessageView;
@property (strong, nonatomic) IBOutlet UILabel *deleteWarningMessageLabel;
@property (nonatomic) BOOL fullScreenAvatarLoaded;
@end

@implementation UIContactDetailsViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        _isPresenting3DTouch = NO;
        isNetworkConnected = YES;
        _sectionsContent = [[OrderedOptionalSectionedContent alloc] initWithSections:@[kServicePlanSection, kRainbowActionSection, kContactTypeSection, kPhoneSection, kEmailSection, kAdressSection, kWebpageSection, kOtherSection, kUserSection]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateMyContact object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateGroupsForContact:) name:kGroupsServiceDidUpdateGroupsForContact object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateMyUserProfiles:) name:kMyUserProfilesDidUpdate object:nil];
    }
    return self;
}

-(void) dealloc {
    [_sectionsContent removeAllObjects];
    _sectionsContent = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateMyContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupsServiceDidUpdateGroupsForContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserProfilesDidUpdate object:nil];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _headerView.clipsToBounds = NO;
    _avatarView.asCircle = YES;
    _avatarView.peer = _contact;
    _avatarView.showPresence = YES;
    _avatarView.withBorder = YES;
    
    _backgroundAvatarView.peer = _contact;
    _backgroundAvatarView.asCircle = NO;
    _backgroundAvatarView.showPresence = NO;
    _backgroundAvatarView.cornerRadius = 0;
    
    _contactName.textColor = [UIColor blackColor];
    _subName.textColor = [UIColor blackColor];
    _companyName.textColor = [UIColor blackColor];
    _calendarPresence.tintColor = [UITools defaultTintColor];
    _textualPresence.textColor = [UIColor blackColor];
    
    [UITools applyCustomBoldFontTo:_contactName];
    [UITools applyCustomFontTo:_subName];
    [UITools applyCustomFontTo:_companyName];
    [UITools applyCustomFontTo:_calendarPresence.titleLabel];
    [UITools applyCustomFontTo:_textualPresence];
    
    [self.tableView addParallaxWithView:_headerView andHeight:160 andMinHeight:160 andShadow:NO];
    
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    _visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    _visualEffectView.frame = self.tableView.parallaxView.bounds;
    _maskView.backgroundColor = [UITools colorFromHexa:0XFFFFFF40];
    [_maskView insertSubview:_visualEffectView atIndex:0];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.tableView.parallaxView setDelegate:self];
    self.tableView.parallaxView.layer.zPosition = 1;
    
    if(_contact.photoData) {
        _headerView.userInteractionEnabled = YES;
    
        UIImage *image = [[UIImage alloc] initWithData:_contact.photoData];
        CGRect screen = [[UIScreen mainScreen] bounds];
        _fullScreenAvatarView.backgroundColor = [UIColor lightGrayColor];
        _fullScreenAvatarImage.image = image;

        [_fullScreenAvatarView setFrame: CGRectMake(0, 0, screen.size.width, screen.size.height)];
        
        UIWindow *keyWindow = [UIApplication sharedApplication].windows[0];
        [keyWindow addSubview: _fullScreenAvatarView];
    }

    _fullScreenAvatarLoaded = NO;
    [self showFullscreenAvatar: NO];
    
    _deleteWarningMessageLabel.text = NSLocalizedString(@"If you delete your account, you won’t be able to reactivate it later or retrieve any of the content you have published or shared.", nil);
    
    if(!([ServicesManager sharedInstance].myUser.isInDefaultCompany && [_contact isEqual:[ServicesManager sharedInstance].myUser.contact])) {
        self.tableView.tableFooterView = [UIView new];
        [_deleteWarningMessageView setHidden:YES];
    }
    
    self.tableView.separatorColor = [UITools colorFromHexa:0xE3E3E3FF];
    self.tableView.backgroundColor = [UITools colorFromHexa:0xF3F3F3FF];
    
}


-(void) didTabUIBarButton {
    if(_fullScreenAvatarView.alpha > 0){
        [self showFullscreenAvatar:NO];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(![_contact isEqual:[ServicesManager sharedInstance].myUser.contact]){
        [_editButton setEnabled:NO];
        [_editButton setTintColor: [UIColor clearColor]];
    } else {
        [_editButton setEnabled:YES];
        [_editButton setTintColor:nil];
        
        // refresh UI when go back from Edit my vcard
        if (_contact) {
            [self setContact:_contact];
            [self.tableView reloadData];
        }
    }
    if(!_contact.isBot && !_contact.isInvitedUser) {
        [[ServicesManager sharedInstance].contactsManagerService fetchRemoteContactDetail:_contact];
        [[ServicesManager sharedInstance].contactsManagerService fetchCalendarAutomaticReply:_contact];
    }
    
    if(![ServicesManager sharedInstance].loginManager.isConnected)
        [self didLostConnection:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(![ServicesManager sharedInstance].loginManager.isConnected)
        [self didReconnect:nil];
}

#pragma mark - Login manager notification

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if([_contact isEqual:[ServicesManager sharedInstance].myUser.contact]){
        [_editButton setEnabled:NO];
        [_editButton setTintColor: [UIColor clearColor]];
        
    }
    isNetworkConnected = NO;
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    if([_contact isEqual:[ServicesManager sharedInstance].myUser.contact]){
        [_editButton setEnabled:YES];
        [_editButton setTintColor:nil];
        
    }
    isNetworkConnected = YES;
}

-(void) didUpdateContact:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateContact:notification];
        });
        return;
    }
    if([self isViewLoaded]) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        if([contact isEqual:_contact]){
            // Dont update if not needed (change in photo or displayName)
            [self setContact:contact];
            [self.tableView reloadData];
        }
    }
}

-(void) didUpdateGroupsForContact:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateGroupsForContact:notification];
        });
        return;
    }
    
    Contact *contact = (Contact *)notification.object;
    if([contact isEqual:_contact]){
        [self setContact:contact];
        [self.tableView reloadData];
    }
}

-(void) didUpdateMyUserProfiles:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateMyUserProfiles:notification];
        });
        return;
    }
    
    [self setContact:_contact];
}


-(void)setContact:(Contact *) contact {
    _contact = nil;
    _contact = contact;
    
    [_sectionsContent removeAllObjects];
    
    NSMutableString *nameLabel = [NSMutableString string];
    if(_contact.title.length > 0)
        [nameLabel appendFormat:@"%@ ",_contact.title];
    if(_contact.displayName)
        [nameLabel appendString:_contact.displayName];
    _contactName.text = nameLabel;
    _subName.text = _contact.jobTitle;
    _companyName.text = _contact.companyName;
    if (_contact.isPresenceSubscribed)
        _textualPresence.text = [UITools getPresenceText:_contact];
    else
        _textualPresence.text = @"";
    [_calendarPresence setTintColor:[UITools defaultTintColor]];
    // Calendar Out of office
    if( _contact.calendarPresence.automaticReply.isEnabled ) {
        [_calendarPresence setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
        [_calendarPresence setImage:[UIImage imageNamed:@"infoOutOfOffice"] forState:UIControlStateNormal];
        if( _contact.calendarPresence.automaticReply.untilDate != nil ) {
            [_calendarPresence setTitle:[NSString stringWithFormat:NSLocalizedString(@"Out of office until %@", nil), [UITools optimizedFormatForCalendarPresenceDate: _contact.calendarPresence.automaticReply.untilDate]] forState:UIControlStateNormal];
        } else {
            [_calendarPresence setTitle:NSLocalizedString(@"Out of office", nil) forState:UIControlStateNormal];
        }
    } else if(_contact.calendarPresence.presence == CalendarPresenceBusy ) {
        [_calendarPresence setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
        [_calendarPresence setImage:[UIImage imageNamed:@"inMeeting"] forState:UIControlStateNormal];
        [_calendarPresence setTitle:[NSString stringWithFormat:NSLocalizedString(@"Appointment until %@", nil), [UITools optimizedFormatForCalendarPresenceDate: _contact.calendarPresence.until]] forState:UIControlStateNormal];
    } else if(_contact.calendarPresence.presence == CalendarPresenceOutOfOffice ) {
        [_calendarPresence setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
        [_calendarPresence setImage:[UIImage imageNamed:@"outOfOffice"] forState:UIControlStateNormal];
        [_calendarPresence setTitle:[NSString stringWithFormat:NSLocalizedString(@"Out of office until %@", nil), [UITools optimizedFormatForCalendarPresenceDate: _contact.calendarPresence.until]] forState:UIControlStateNormal];
    } else if(_contact.calendarPresence.presence == CalendarPresenceAvailable ) {
        [_calendarPresence setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_calendarPresence setImage:nil forState:UIControlStateNormal];
        if([_contact.calendarPresence.until isToday]){
            [_calendarPresence setTitle:[NSString stringWithFormat:NSLocalizedString(@"Next meeting at %@", nil), [UITools optimizedFormatForCalendarPresenceDate: _contact.calendarPresence.until]] forState:UIControlStateNormal];
        } else {
             [_calendarPresence setTitle:NSLocalizedString(@"No appointment today", nil) forState:UIControlStateNormal];
        }
    } else {
        [_calendarPresence setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_calendarPresence setTitle:@"" forState:UIControlStateNormal];
        [_calendarPresence setImage:nil forState:UIControlStateNormal];
    }
    
    if(_calendarPresence.imageView.image){
        [_calendarPresence setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    } else {
        [_calendarPresence setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    }

    if([_contact isEqual:[ServicesManager sharedInstance].myUser.contact]){
        [_sectionsContent addObject:[[SectionContent alloc] initWithContent:nil andType:SectionContentTypeServicePlan] toSection:kServicePlanSection];
    }
    
    if (![_contact isEqual:[ServicesManager sharedInstance].myUser.contact]) {
        if(_contact.canChatWith) {
            [_sectionsContent addObject:[[SectionContent alloc] initWithContent:nil andType:SectionContentTypeStartConversation] toSection:kRainbowActionSection];
        }
        
        // We can have 2 cases : user not in our roster and user in our roster but without presence subscribed.
        if(_contact.canChatWith && (!_contact.isInRoster || !_contact.isPresenceSubscribed) && !_contact.sentInvitation)
            [_sectionsContent addObject:[[SectionContent alloc] initWithContent:nil andType:SectionContentTypeAddInRoster] toSection:kContactTypeSection];
        
        if(!_contact.canChatWith && !_contact.sentInvitation && (_contact.emailAddresses.count > 0 || _contact.phoneNumbers.count > 0))
            [_sectionsContent addObject:[[SectionContent alloc] initWithContent:nil andType:SectionContentTypeInvite] toSection:kContactTypeSection];
    }

    for(PhoneNumber *number in _contact.phoneNumbers)
        [_sectionsContent addObject:[[SectionContent alloc] initWithContent:number andType:SectionContentTypePhoneNumber] toSection:kPhoneSection];
    for(EmailAddress *email in _contact.emailAddresses){
        if (email.isVisible)
            [_sectionsContent addObject:[[SectionContent alloc] initWithContent:email andType:SectionContentTypeEmailAddress] toSection:kEmailSection];
    }
    for(PostalAddress *address in _contact.addresses)
        [_sectionsContent addObject:[[SectionContent alloc] initWithContent:address andType:SectionContentTypePostalAddress] toSection:kAdressSection];
    for(NSString *website in _contact.webSitesURL)
        [_sectionsContent addObject:[[SectionContent alloc] initWithContent:website andType:SectionContentTypeWebsite] toSection:kWebpageSection];

    if(_contact.canChatWith && ![_contact isEqual:[ServicesManager sharedInstance].myUser.contact] && ![_contact isBot]){
        // Get contact groups
        NSArray <Group* > *groups = [[ServicesManager sharedInstance].groupsService groupsForContact:_contact];
        [_sectionsContent addObject:[[SectionContent alloc] initWithContent:groups andType:SectionContentTypeGroups] toSection:kOtherSection];
        
    }
    if([_contact isEqual:[ServicesManager sharedInstance].myUser.contact]){
        [_sectionsContent addObject:[[SectionContent alloc] initWithContent:nil andType:SectionContentTypeChangePassword] toSection:kOtherSection];
        
        if([ServicesManager sharedInstance].myUser.isInDefaultCompany)
            [_sectionsContent addObject:[[SectionContent alloc] initWithContent:nil andType:SectionContentTypeDeleteMySelf] toSection:kUserSection];
    }
    
    if([_contact isEqual:[ServicesManager sharedInstance].myUser.contact]){
        self.title = NSLocalizedString(@"My profile", nil);
    } else
        self.title = NSLocalizedString(@"Contact details", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - APParallaxViewDelegate
- (void)parallaxView:(APParallaxView *)view willChangeFrame:(CGRect)frame {
}

- (void)parallaxView:(APParallaxView *)view didChangeFrame:(CGRect)frame {
    CGRect visualEffectFrame = _visualEffectView.frame;
    visualEffectFrame.size.height = frame.size.height;
    [_visualEffectView setFrame:visualEffectFrame];
}

- (IBAction)closeButtonTapped:(UIButton *)sender {
    [self showFullscreenAvatar:NO];
}

-(IBAction)didTapGesture:(UITapGestureRecognizer*)sender {
    _isPresenting3DTouch = NO;
    
    if( sender == _headerTapGesture ) {
        [self showFullscreenAvatar:YES];
    } else {
        [self showFullscreenAvatar:NO];
    }
}

- (void) showFullscreenAvatar: (BOOL)visible {
    if(visible) {
        _fullScreenAvatarView.alpha = 1;
        [self loadHiResAvatar];
        self.tableView.userInteractionEnabled = NO;
    } else {
        _fullScreenAvatarView.alpha = 0;
        self.tableView.userInteractionEnabled = YES;
    }
}

- (void) loadHiResAvatar {
    if( _fullScreenAvatarLoaded == YES ) {
        NSLog(@"High resolution image ALREADY loaded for %@...", _contact);
        return;
    }
    
    if(!_contact.isRainbowUser){
        NSLog(@"Hifh resolution image, not a rainbow user");
        return;
    }
        
    
    if( _contact.photoData ){
        NSLog(@"Loading high resolution image for %@...", _contact);
        
        // display a loading spinner
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_fullScreenAvatarView animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.dismissible = YES;
        hud.delegate = self;
        hud.removeFromSuperViewOnHide = YES;
        [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
        
        // Loading better resolution image
        [[ServicesManager sharedInstance].contactsManagerService loadHiResAvatarForContact:_contact withCompletionBlock:^(NSData *receivedData, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(!error) {
                    UIImage *image = [[UIImage alloc] initWithData:receivedData];
                    _fullScreenAvatarImage.image = image;
                    _fullScreenAvatarLoaded = YES;
                    NSLog(@"loading high resolution image done.");
                } else {
                    NSLog(@"loading high resolution failed. %@", error);
                    [self showFullscreenAvatar:NO];
                }
                
                [hud hide:YES];
            });
        }];
    }
}

#pragma mark - HUD Delegate
- (void)hudWasTapped:(MBProgressHUD *)hud {
    [self showFullscreenAvatar:NO];
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_sectionsContent allNotEmptySections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_sectionsContent allNotEmptySections] objectAtIndex:section];
    return [[_sectionsContent sectionForKey:key] count] +1; // +1 to add custom header cell
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customHeaderCell"];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"customHeaderCell"] init];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        return cell;
    }
    
    NSString *key = [[_sectionsContent allNotEmptySections] objectAtIndex:indexPath.section];
    NSMutableArray<SectionContent *> *contents = [_sectionsContent sectionForKey:key];
    
    SectionContent *content = [contents objectAtIndex:indexPath.row-1];
    
    switch (content.type) {
        case SectionContentTypeGroups:{
            UIContactGroupListCell *cell = (UIContactGroupListCell *) [tableView dequeueReusableCellWithIdentifier:kContactGroupListCellIdentifier];
            cell.content = content;
            return cell;
            break;
        }
        case SectionContentTypeInvite :{
            ContactDetailsInviteToJoinTableViewCell *cell = (ContactDetailsInviteToJoinTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kContactInviteToJoinCellIdentifier];
            return cell;
            break;
        }
        case SectionContentTypeStartConversation: {
            ContactRainbowActionsCell *cell = (ContactRainbowActionsCell *)[tableView dequeueReusableCellWithIdentifier:kContactRainbowActionsCellIdentifier];
            cell.contact = _contact;
            [self setUpActionsCellContents:cell];
            return cell;
            break;
        }
        case SectionContentTypeDeleteMySelf: {
            UIContactSelfDeleteCell *cell = (UIContactSelfDeleteCell*)[tableView dequeueReusableCellWithIdentifier:kContactSelfDeleteAccountCellIdentifier];
            cell.textLabel.text = NSLocalizedString(@"Delete my account", nil);
            return cell;
            break;
        }
        default:{
            UIContactDetailsFieldCell *cell = (UIContactDetailsFieldCell *)[tableView dequeueReusableCellWithIdentifier:kContactFielCellIdentifier];
            cell.content = content;
            return cell;
            break;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if(indexPath.section == 0) {
            return 0.0f;
        }
        
        if(indexPath.section == 1){
            NSString *previousKey = [[_sectionsContent allNotEmptySections] objectAtIndex:indexPath.section-1];
            if([previousKey isEqualToString:kRainbowActionSection])
                return 0.0f;
            if([previousKey isEqualToString:kContactTypeSection])
                return 30.0f;
        }
        
        return 20.0f;
    }
    
    NSString *key = [[_sectionsContent allNotEmptySections] objectAtIndex:indexPath.section];
    NSMutableArray<SectionContent *> *contents = [_sectionsContent sectionForKey:key];

    SectionContent *content = [contents objectAtIndex:indexPath.row-1];
    
    switch (content.type) {
        case SectionContentTypeGroups:{
            UIContactGroupListCell *cell = (UIContactGroupListCell *) [tableView dequeueReusableCellWithIdentifier:kContactGroupListCellIdentifier];
            cell.content = content;
            return cell.computedHeight;
            break;
        }
        case SectionContentTypeInvite :{
            ContactDetailsInviteToJoinTableViewCell *cell = (ContactDetailsInviteToJoinTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kContactInviteToJoinCellIdentifier];
            return cell.computedHeight;
            break;
        }
        case SectionContentTypeStartConversation :{
            ContactRainbowActionsCell *cell = (ContactRainbowActionsCell *)[tableView dequeueReusableCellWithIdentifier:kContactRainbowActionsCell];
            return cell.computedHeight;
            break;
        }
        case SectionContentTypeDeleteMySelf: {
            UIContactSelfDeleteCell *cell = (UIContactSelfDeleteCell *)[tableView dequeueReusableCellWithIdentifier:kContactSelfDeleteAccountCellIdentifier];
            cell.content = content;
            return cell.computedHeight;
            break;
        }
        default:{
            UIContactDetailsFieldCell *cell = (UIContactDetailsFieldCell *)[tableView dequeueReusableCellWithIdentifier:kContactFielCellIdentifier];
            cell.content = content;
            return cell.computedHeight;
            break;
        }
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return nil;
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != 0) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

        if ([cell isKindOfClass:[UIContactDetailsFieldCell class]]) {
            if(((UIContactDetailsFieldCell*)cell).content.type != SectionContentTypeGroups){
                [self performActionForCell:((UIContactDetailsFieldCell*)cell)];
            }
        }
        
        else if ([cell isKindOfClass:[UIContactSelfDeleteCell class]] ) {

            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Are you sure you want to delete your account?", nil) message:[NSString stringWithFormat:@"\n%@", NSLocalizedString(@"We are sorry to see you go. Your account will be deactivated and your friends or colleagues will no longer be able to contact you on Rainbow. You will not be able to reactivate your account or retrieve any of the content you have published or shared.", nil)] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *removeAccountAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction __unused *action) {
                [[ServicesManager sharedInstance].loginManager deleteMyAccount];
            }];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction __unused *action) {
            }];

            [alertController addAction:removeAccountAction];
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

-(BOOL) tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIContactDetailsFieldCell* cell = (UIContactDetailsFieldCell*)[tableView cellForRowAtIndexPath:indexPath];
    if([cell isKindOfClass:[ContactRainbowActionsCell class]] ) {
        return NO;
    }
    if([cell isKindOfClass:[UIContactDetailsFieldCell class]] && (([cell.content isKindOfClass:[NSString class]] && cell.content.type != SectionContentTypeGroups) || cell.content.type ==  SectionContentTypeStartConversation || cell.content.type ==  SectionContentTypeAddInRoster || cell.content.type ==  SectionContentTypeInvite || cell.content.type ==  SectionContentTypeGroups || cell.content.type ==  SectionContentTypeWebRTCAudioCall || cell.content.type ==  SectionContentTypeWebRTCVideoCall || cell.content.type ==  SectionContentTypeChangePassword || cell.content.type == SectionContentTypeDeleteMySelf) )
            return NO;
    return YES;
}

-(BOOL) tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    return action == @selector(copy:);
}

-(void) tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    if (action == @selector(copy:)) {
        UIContactDetailsFieldCell* cell = (UIContactDetailsFieldCell*)[tableView cellForRowAtIndexPath:indexPath];
        [[UIPasteboard generalPasteboard] setString:[cell stringToCopy]];
    }
}

-(void) performActionForCell:(UIContactDetailsFieldCell*)cell {
    switch (cell.content.type) {
        case SectionContentTypeJobCompany: {
            break;
        }
        case SectionContentTypePhoneNumber: {
            PhoneNumber *phoneNum = (PhoneNumber*)cell.content.content;
            // Number is mobile - prompt for action 'Call' or 'Send message'
            if( phoneNum.deviceType == PhoneNumberDeviceTypeMobile ) {
                UIAlertController *inviteMenuActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction* callAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Call", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [UITools makeCallToPhoneNumber:phoneNum inController:self];
                }];
                
                [inviteMenuActionSheet addAction:callAction];
                
                UIAlertAction* smsAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:NSLocalizedString(@"SMS %@ (%@)",nil), phoneNum.number, NSLocalizedString(@"Mobile", nil)] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [self messageComposeViewController: phoneNum.number ];
                }];
                
                [inviteMenuActionSheet addAction:smsAction];
                
                // --- CANCEL ---
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
                [inviteMenuActionSheet addAction:cancel];
                
                // show the menu.
                [inviteMenuActionSheet.view setTintColor:[UITools defaultTintColor]];
                [self presentViewController:inviteMenuActionSheet animated:YES completion:nil];

            } else {
                [UITools makeCallToPhoneNumber:phoneNum inController:self];
            }
            break;
        }
        case SectionContentTypeEmailAddress: {
            if ([MFMailComposeViewController canSendMail]) {
                NSString* mailAddress = ((EmailAddress*)cell.content.content).address;
                MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
                mailViewController.mailComposeDelegate = self;
                [mailViewController.navigationBar setTintColor:[UIColor whiteColor]];
                [mailViewController.navigationBar setBarTintColor:[UITools defaultTintColor]];
                [mailViewController setToRecipients:@[mailAddress]];
                [mailViewController setSubject:@""];
                [self presentViewController:mailViewController animated:YES completion:^{
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                }];
            }
            break;
        }
        case SectionContentTypePostalAddress: {
            PostalAddress *postalAddress = (PostalAddress*)cell.content.content;
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            [geocoder geocodeAddressString:[postalAddress stringRepresentation] completionHandler:^(NSArray *placemarks, NSError *error) {
                if (error) {
                    NSLog(@"Geocode failed with error: %@", error);
                    return;
                }
                
                if (placemarks && placemarks.count > 0) {
                    CLPlacemark *placemark = placemarks[0];
                    CLLocation *location = placemark.location;
                    
                    MKPlacemark *place = [[MKPlacemark alloc] initWithCoordinate:location.coordinate addressDictionary: [postalAddress dictionaryRepresentation]];
                    MKMapItem *mapItem = [[MKMapItem alloc]initWithPlacemark:place];
                    [mapItem openInMapsWithLaunchOptions:nil];
                }
            }];
            break;
        }
        case SectionContentTypeWebsite: {
            NSURL *url = [NSURL URLWithString:(NSString*)cell.content.content];
            if (url.scheme.length == 0)
                url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",url]];
            [[UIApplication sharedApplication] openURL:url];
            break;
        }
        case SectionContentTypeAddInRoster: {
            [[ServicesManager sharedInstance].contactsManagerService inviteContact:_contact];
            break;
        }
        case SectionContentTypeChangePassword:{
            ChangePasswordViewController *pwdView = (ChangePasswordViewController *)[[UIStoryboardManager sharedInstance].myInfosStoryBoard instantiateViewControllerWithIdentifier:@"ChangePasswordView"];
            [self.navigationController pushViewController:pwdView animated:YES];
            break;
        }
        default: {
            // Nothing to do
        }
    }
}

-(void) messageComposeViewController: (NSString *)number {
    if( [MFMessageComposeViewController canSendText] ) {
        MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
        picker.messageComposeDelegate = self;
        picker.recipients = @[ number ];
        [[UINavigationBar appearance] setBackIndicatorImage:[UITools imageFromColor:[UIColor redColor]]];
        [self presentViewController:picker animated:YES completion:nil];
    }
}

-(void) performRainbowAction:(ActionButtonType)actionType {
    if(actionType == ActionButtonTypeChat) {
        if([_fromView isKindOfClass:[UIContactsTableViewController class]] || [_fromView isKindOfClass:[UIRoomDetailsViewController class]] || [_fromView isKindOfClass:[UICallLogsDetailsViewController class]]){
            [self.navigationController popToRootViewControllerAnimated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:_contact withCompletionHandler:nil];
            });
        } else if ([_fromView isKindOfClass:[UIMessagesViewController class]]){
            [[self navigationController] popViewControllerAnimated:YES];
        }
    }
    else if(actionType == ActionButtonTypeAudioCall || actionType == ActionButtonTypeVideoCall ) {
        // click on call : Kick off the call.
        if (_contact.canChatWith && isNetworkConnected) {
            
            RTCCallFeatureFlags features = RTCCallFeatureAudio;
            if (actionType == ActionButtonTypeVideoCall)
                features |= RTCCallFeatureLocalVideo;
            
            // Do not present anykind of UI here.
            // The UI is presented when we'll receive kRTCServiceDidStartNewOutgoingCallNotification
            
            // Check if microphone is allowed before trying to establish the call
            if([ServicesManager sharedInstance].rtcService.microphoneAccessGranted){
                [[ServicesManager sharedInstance].rtcService beginNewOutgoingCallWithPeer:_contact withFeatures:features];
            } else {
                [UITools showMicrophoneBlockedPopupInController:self];
            }
        }
        else{
            // --- ALERT ---
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"You need to be connected to your network to make calls", nil) message:NSLocalizedString(@"Can't Make Call",nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok =[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

#pragma mark - MFMailComposeViewController delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MFMessageComposeViewController delegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if( result == MessageComposeResultCancelled || result == MessageComposeResultFailed ) {
        NSLog(@"MessageComposeResultCancelled or MessageComposeResultFailed");
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[ServicesManager sharedInstance].contactsManagerService deleteInvitationWithID:_contact.sentInvitation];
        });
    }
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"addMemberInGroupSegueID"]){
        UINavigationController *destNavController = [segue destinationViewController];
        UIGroupListViewController *destViewController = [destNavController.viewControllers firstObject];
        destViewController.groups = [[ServicesManager sharedInstance].groupsService groupsForContact:_contact];
        destViewController.contact = _contact;
    }
}

#pragma mark - 3D Touch actions
- (NSArray<id<UIPreviewActionItem>> *)previewActionItems {
    NSMutableArray *actions = [NSMutableArray array];
    
    [_contact.phoneNumbers enumerateObjectsUsingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
        NSString *label = aPhoneNumber.label;
        if (aPhoneNumber.deviceType == PhoneNumberDeviceTypeMobile){
            switch (aPhoneNumber.type) {
                case PhoneNumberTypeHome:
                    label = NSLocalizedString(@"Personal Mobile", nil);
                    break;
                case PhoneNumberTypeWork:
                    label = NSLocalizedString(@"Professional Mobile", nil);
                    break;
                case PhoneNumberTypeOther:
                    label = NSLocalizedString(@"Professional Mobile", nil);
                    break;
                default:
                    label = NSLocalizedString(@"Cell", nil);
                    break;
            }
        } else if (aPhoneNumber.deviceType == PhoneNumberDeviceTypeLandline){
            switch (aPhoneNumber.type) {
                case PhoneNumberTypeHome:
                    label = NSLocalizedString(@"Personal", nil);
                    break;
                case PhoneNumberTypeWork:
                    label = NSLocalizedString(@"Professional", nil);
                    break;
                default:
                    label = NSLocalizedString(@"Work", nil);
                    break;
            }
        } else {
            label = NSLocalizedString(aPhoneNumber.label, nil);
        }
        
        UIPreviewAction *callAction = [UIPreviewAction actionWithTitle:[NSString stringWithFormat:@"%@ : %@",label, aPhoneNumber.number] style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
            [UITools makeCallToPhoneNumber:aPhoneNumber inController:self];
        }];
        
        [actions addObject:callAction];
    }];
    
    UIPreviewAction *addInRosterAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Add to my network", nil) style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[ServicesManager sharedInstance].contactsManagerService inviteContact:_contact];
    }];
    
    UIPreviewAction *startConversationAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Start conversation", nil) style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:_contact withCompletionHandler:nil];
    }];
    
//    UIPreviewAction *inviteAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Invite", nil) style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
//        [[ServicesManager sharedInstance].contactsManagerService inviteContact:_contact];
//    }];
    
    if(_contact.canChatWith)
        [actions addObject:startConversationAction];

    if(_contact.isRainbowUser){
        if(!_contact.isInRoster)
            [actions addObject:addInRosterAction];
    }
//    else {
//        [actions addObject:inviteAction];
//    }
    
    return actions;
}

- (IBAction)didTapCalendarPresence:(UIButton *)sender {
    if(_contact.calendarPresence.automaticReply.isEnabled){
        NSString *title =_contact.calendarPresence.automaticReply.untilDate != nil?
        [NSString stringWithFormat:NSLocalizedString(@"Out of office until %@", nil), [UITools optimizedFormatForCalendarPresenceDate:_contact.calendarPresence.automaticReply.untilDate]]:
            NSLocalizedString(@"Out of office", nil);

        UIAlertController *ctrl = [UIAlertController alertControllerWithTitle:title message:_contact.calendarPresence.automaticReply.message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok =[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [ctrl addAction:ok];
        [self presentViewController:ctrl animated:YES completion:nil];
    }
}

#pragma mark - Cells actions
- (IBAction)didTapInviteToJoinButton:(UIButton *)sender {
    [UITools showInviteContactAlertController:_contact fromController:self controllerDelegate:self withCompletionHandler:^(Invitation *invitation) {
        
    }];
}
-(void)setUpActionsCellContents:(ContactRainbowActionsCell* )cell {
    NSLog(@"[ContactsDetail] ContactId %@ myID %@",_contact.companyId,[ServicesManager sharedInstance].myUser.contact.companyId);
    if((![_contact.companyId isEqualToString:[ServicesManager sharedInstance].myUser.contact.companyId]&& !(_contact.isInRoster)) || (!_contact.companyId)){
        cell.audioCallButton.enabled = NO;
        cell.videoCallButton.enabled = NO;
        cell.chatButton.enabled = NO;
        cell.cellButtonTapHandler = nil;
    }
    else {
        cell.audioCallButton.enabled = YES;
        cell.videoCallButton.enabled = YES;
        cell.chatButton.enabled = YES;
        cell.cellButtonTapHandler = ^(ActionButtonType type) {
            [self performRainbowAction:type];
        };
    }
    cell.audioCallButton.layer.borderColor = (cell.audioCallButton.enabled) ? [UITools defaultTintColor].CGColor : [UIColor lightGrayColor].CGColor;
    cell.audioCallButton.tintColor = (cell.audioCallButton.enabled) ? [UITools defaultTintColor] : [UIColor grayColor];
    cell.videoCallButton.layer.borderColor = (cell.audioCallButton.enabled) ? [UITools defaultTintColor].CGColor : [UIColor lightGrayColor].CGColor;
    cell.videoCallButton.tintColor = (cell.audioCallButton.enabled) ? [UITools defaultTintColor] : [UIColor grayColor];
    cell.chatButton.layer.borderColor = (cell.audioCallButton.enabled) ? [UITools defaultTintColor].CGColor : [UIColor lightGrayColor].CGColor;
    cell.chatButton.tintColor = (cell.audioCallButton.enabled) ? [UITools defaultTintColor] : [UIColor grayColor];
    
}


@end
