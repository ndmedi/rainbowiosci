/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Contact+UIRainbowGenericTableViewCellProtocol.h"
#import <Rainbow/Peer.h>
#import "UITools.h"

@implementation Contact (UIRainbowGenericTableViewCellProtocol)

-(NSString *) mainLabel {
    return self.displayName;
}

-(NSString *) subLabel {
    if( self.isBot ) {
        return nil;
    } else if( self.isPresenceSubscribed ) {
        return [UITools getPresenceText:self];
    } else if( self.companyName ) {
        if([self.companyName isEqualToString:self.displayName])
            return nil;
        return self.companyName;
    }
    
    return nil;
}

-(UIColor *) mainLabelTextColor {
    if( self.calendarPresence.presence == CalendarPresenceOutOfOffice || self.calendarPresence.automaticReply.isEnabled) {
        return [UITools defaultTintColor];
    } else if( self.calendarPresence.presence == CalendarPresenceBusy ) {
        return [UITools defaultTintColor];
    } else {
        return [UIColor blackColor];
    }
}

-(NSDate *) date {
    return nil;
}

-(NSString *) bottomRightLabel {
    return nil;
}

-(Peer *) avatar {
    return (Peer*)self;
}

-(NSArray *) observablesKeyPath {
    return @[kContactLastNameKey,kContactFirstNameKey];
}

-(NSInteger) badgeValue {
    return 0;
}

-(NSString *) cellButtonTitle {
    return nil;
}

-(UIImage *) cellButtonImage {
    if( !self.canChatWith && (self.phoneNumbers.count > 0 || self.emailAddresses.count > 0) ) {
        if (self.isPBXContact)
            return [UIImage imageNamed:@"Phone"];
        if (self.sentInvitation && self.sentInvitation.status == InvitationStatusPending) {
            return [UIImage imageNamed:@"invitation"]; // Pending
        } else if(self.sentInvitation && self.sentInvitation.status == InvitationStatusFailed) {
            return [UIImage imageNamed:@"invitation"]; // Failed
        } else {
            return [UIImage imageNamed:@"invitation"]; // Invite
        }
    }
    else {
        return [UIImage imageNamed:@"Vcard"];
    }
    return nil;
}

-(UIColor *) cellButtonImageColor {
    if (self.sentInvitation && self.sentInvitation.status == InvitationStatusPending) {
        return [UIColor lightGrayColor]; // Pending
    } else if(self.sentInvitation && self.sentInvitation.status == InvitationStatusFailed) {
        return [UIColor redColor]; // Failed
    }
    return nil;
}

-(UIImage *) rightIcon {
    return nil;
}
-(UIColor *) rightIconTintColor {
    return nil;
}

@end
