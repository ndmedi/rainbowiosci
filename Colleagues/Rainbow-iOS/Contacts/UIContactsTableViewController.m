/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import <Rainbow/ContactsManagerService.h>

#import "UIContactsTableViewController.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/Contact.h>
#import "UIRainbowGenericTableViewCell.h"
#import "UIContactsInvitationTableViewCell.h"
#import "UIMessagesViewController.h"
#import "UITools.h"
#import "UIContactDetailsViewController.h"
#import <Rainbow/defines.h>
#import "UIViewController+Visible.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import <Rainbow/ContactsManagerService.h>
#import "CustomSearchController.h"
#import "CustomTabBarViewController.h"
#import "RecentsConversationsTableViewController.h"
#import <Rainbow/EmailAddress.h>
#import "BGTableViewRowActionWithImage.h"
#import <Rainbow/Tools.h>
#import "UISearchResultController.h"
#import <Rainbow/ConversationsManagerService.h>
#import "Contact+Extensions.h"
#import "SCLAlertView.h"
#import "SCLAlertView+Rotation.h"
#import "DZNSegmentedControl.h"
#import "UINotificationManager.h"
#import <Rainbow/GroupsService.h>
#import "UIGroupsTableViewCell.h"
#import "UIGroupDetailsViewController.h"
#import "OrderedOptionalSectionedContent.h"
#import "FilteredSortedSectionedArray.h"
#import "UIScrollView+APParallaxHeader.h"
#import <Rainbow/Invitation.h>
#import "CustomNavigationController.h"
#import <Rainbow/CompaniesService.h>
#import "GoogleAnalytics.h"
#import "MyInfoNavigationItem.h"
#import <MessageUI/MessageUI.h>
#import "Contact+UIRainbowGenericTableViewCellProtocol.h"
#import "MFMessageComposeViewController+StatusBarStyle.h"
#import "UIStoryboardManager.h"
#import "UIView+MGBadgeView.h"
#import "InviteByViewController.h"
#import "UIGuestModeViewController.h"
#import "UINetworkLostViewController.h"
#import "UIOrganizeTableViewController.h"

#define dznButtonDisabledColor 0xB2B2B2FF

@interface DZNSegmentedControl ()
- (UIButton *)buttonAtIndex:(NSUInteger)segment;
@end

typedef NS_ENUM(NSInteger, SelectorType) {
    SelectorTypeMyColleaguesOnly = 0,
    SelectorTypeGroups,
    SelectorTypeInvitations
};

typedef NS_ENUM(NSInteger, FilterType){
    FilterTypeAll = 0,
    FilterTypeConnectedOrInvited,
    FilterTypeStoredValue
};

typedef NS_ENUM(NSInteger, SortOrder){
    SortOrderByName = 0,
    SortOrderByCompany,
    SortOrderStoredValue
};

@interface UIContactsTableViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, UIActionSheetDelegate, DZNSegmentedControlDelegate, UIViewControllerPreviewingDelegate, UISearchResultDisplayDetailControllerProtocol, MFMessageComposeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, CNContactPickerDelegate, UIPopoverPresentationControllerDelegate,OrganizeControllerDelegate>
@property (nonatomic) BOOL lostNetworkViewAsBeenScrolled;
@property (nonatomic) BOOL guestBannerViewAsBeenScrolled;
@property (nonatomic) BOOL isNetworkConnected;

@property (nonatomic, strong) ServicesManager *serviceManager;
@property (nonatomic, strong) ContactsManagerService *contactsManager;
@property (nonatomic, strong) GroupsService *groupsService;
@property (nonatomic, strong) NSArray *letters;
@property (nonatomic, strong) CustomSearchController *searchController;
@property (nonatomic, strong) ConversationsManagerService *conversationsManagerService;
@property (nonatomic, strong) MyUser *myUser;
@property (nonatomic, strong) IBOutlet DZNSegmentedControl *tabSelector;
@property (nonatomic, strong) NSMutableArray<Group*> *groupsList;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addGroupBarButtonItem;
@property (nonatomic) BOOL isFiltering;
@property (nonatomic) FilterType selectedFilterType;
@property (nonatomic) BOOL isSorting;
@property (nonatomic) SortOrder selectedSortOrder;
//@property (strong, nonatomic) IBOutlet UIView *tableViewHeader;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopConstraint;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *organizeBarButtonItem;
// "Allow access to local contacts" banner
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (nonatomic) CGFloat headerViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *allowAccessContactsButton;
@property (weak, nonatomic) IBOutlet UILabel *allowAccessContactsLabel;
@property (strong, nonatomic) IBOutlet UIView *inviteView;
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (weak, nonatomic) IBOutlet UIButton *inviteAllButton;

// New model (will contains Contacts AND Invitations...)
@property (nonatomic, strong) FilteredSortedSectionedArray<Contact *> *allObjects;

// All the filter/sorter/computation blocks used for the previous datamodel
@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) SectionNameComputationBlock sectionedByCompany;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;
@property (nonatomic, strong) NSSortDescriptor *sortByName;

@property (nonatomic, strong) NSPredicate *globalFilterAll;
@property (nonatomic, strong) NSPredicate *globalFilterAllInvited;

@property (nonatomic, strong) NSPredicate *globalFilterMyNetwork;
@property (nonatomic, strong) NSPredicate *globalFilterMyNetworkOnline;

@property (nonatomic, strong) id previewingContext;

@property (nonatomic, strong) Invitation *pendingInvitation;
@property (nonatomic, strong) UIButton *tabSelectorInvitationsButton;

@property (nonatomic) BOOL populated;
//organize
@property (nonatomic, strong) UIOrganizeTableViewController *organizeContactsTableViewController;

@property (nonatomic, strong) NSTimer *refreshUITimer;
@end

@implementation UIContactsTableViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        _groupsList = [NSMutableArray new];
        _letters = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", @"#"];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideNetworkLostView:) name:CustomNavigationControllerDidHideNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowNetworkLostView:) name:CustomNavigationControllerWillShowNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNetworkLostViewFrame:) name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowGuestModeView:) name:CustomNavigationControllerWillShowGuestModeView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideGuestModeView:) name:CustomNavigationControllerDidHideGuestModeView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContactDisplaySettings:) name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddGroup:) name:kGroupsServiceDidAddGroup object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateGroup:) name:kGroupsServiceDidUpdateGroup object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveGroup:) name:kGroupsServiceDidRemoveGroup object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveAllGroups:) name:kGroupsServiceDidRemoveAllGroups object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddInvitation:) name:kContactsManagerServiceDidAddInvitation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateInvitation:) name:kContactsManagerServiceDidUpdateInvitation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveInvitation:) name:kContactsManagerServiceDidRemoveInvitation object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddContact:) name:kContactsManagerServiceDidAddContact object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveContact:) name:kContactsManagerServiceDidRemoveContact object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveAllContacts:) name:kContactsManagerServiceDidRemoveAllContacts object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddInvitation:) name:kCompaniesServiceDidAddCompanyInvitation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateInvitation:) name:kCompaniesServiceDidUpdateCompanyInvitation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveInvitation:) name:kCompaniesServiceDidRemoveCompanyInvitation object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dumpContacts) name:@"dumpContacts" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateBadgeValue) name:kContactsManagerServiceDidUpdateInvitationPendingNumber object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateBadgeValue) name:kCompaniesServiceDidUpdateCompanyInvitationPendingNumber object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndPopulatingMyNetwork:) name:kContactsManagerServiceDidEndPopulatingMyNetwork object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStartConversation:) name:kConversationsManagerDidStartConversation object:nil];
        
        // This will contain all the contacts
        _allObjects = [FilteredSortedSectionedArray new];
        
        NSString *tradInvitation = NSLocalizedString(@"Received invitations", nil);
        NSString *tradInvitationSent = NSLocalizedString(@"Sent invitations", nil);
        NSString *tradDefault = NSLocalizedString(@"Default", nil);
        NSString *tradUnknown = NSLocalizedString(@"Unknown", nil);
        NSString *tradDash = NSLocalizedString(@"#", nil);
        
        // How to get the section name for a given contact, in case of sections by name
        _sectionedByName = ^NSString*(Contact *contact) {
            if (contact.requestedInvitation.status == InvitationStatusPending || contact.companyInvitation.status == InvitationStatusPending)
                return tradInvitation;
            else if(contact.sentInvitation.status == InvitationStatusPending)
                return tradInvitationSent;

            return contact.contactFirstLetter;
        };
        
        // How to get the section name for a given contact, in case of sections by company
        _sectionedByCompany = ^NSString*(Contact *contact) {
            // If the contact have sent us an invitation, show him in a special section.
            if (contact.requestedInvitation.status == InvitationStatusPending || contact.companyInvitation.status == InvitationStatusPending) {
                return tradInvitation;
            }
            
            NSString *companyName = contact.companyName;
            if(companyName.length == 0) {
                // This contact don't have company name so add it to "Default" company or in Unknown company
                if(contact.isRainbowUser)
                    companyName = tradDefault;
                else
                    companyName = tradUnknown;
            }
            return companyName;
        };
        
        // How to sort the section names
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            // Little cheat to make Invitation section appear on top.
            if ([obj1 isEqualToString:tradInvitation]) {
                return NSOrderedAscending;
            }
            if ([obj2 isEqualToString:tradInvitation]) {
                return NSOrderedDescending;
            }
            
            if ([obj1 isEqualToString:tradInvitationSent]) {
                return NSOrderedAscending;
            }
            if ([obj2 isEqualToString:tradInvitationSent]) {
                return NSOrderedDescending;
            }

            // Little cheat to make # section appear on bottom.
            if ([obj1 isEqualToString:tradDash]) {
                return NSOrderedDescending;
            }
            if ([obj2 isEqualToString:tradDash]) {
                return NSOrderedAscending;
            }
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
        }];
        
        
        // Global filter to have all - but only the pending received invitations
        _globalFilterAll = [NSPredicate predicateWithBlock:^BOOL(Contact *contact, NSDictionary<NSString *,id> * bindings) {
            return NO;
        }];
        
        // Global filter to have the pending contact we have invited
        _globalFilterAllInvited = [NSPredicate predicateWithBlock:^BOOL(Contact *contact, NSDictionary<NSString *,id> * bindings) {
            return (contact.sentInvitation.status == InvitationStatusPending || contact.companyInvitation.status == InvitationStatusPending) || contact.requestedInvitation.status == InvitationStatusPending /*|| contact.requestedInvitation.status == InvitationStatusDeleted*/ || contact.requestedInvitation.status == InvitationStatusFailed ||
                (contact.companyInvitation.direction == InvitationDirectionReceived && contact.companyInvitation.status == InvitationStatusPending);
        }];
        
        // Global filter to have only my network contacts (and the pending received invitations)
        _globalFilterMyNetwork = [NSPredicate predicateWithBlock:^BOOL(Contact *contact, NSDictionary<NSString *,id> * bindings) {
            return contact.isInRoster; //  || contact.requestedInvitation.status == InvitationStatusPending || (contact.companyInvitation.direction == InvitationDirectionReceived && contact.companyInvitation.status == InvitationStatusPending)
        }];
        
        // Global filter to have only my network contacts - ONLINE
        _globalFilterMyNetworkOnline = [NSPredicate predicateWithBlock:^BOOL(Contact *contact, NSDictionary<NSString *,id> * bindings) {
            // Online in roster
            return (contact.isInRoster && contact.presence.presence != ContactPresenceUnavailable && contact.presence.presence != ContactPresenceInvisible);
        }];
        
        // Default filters/sorter/.. values
        _allObjects.sectionNameFromObjectComputationBlock = _sectionedByName;
        _allObjects.sectionSortDescriptor = _sortSectionAsc;
        _allObjects.globalFilteringPredicate = _globalFilterAll;
        
        // We have a different way to sort the invitations (by date) from the contact (by name)
        // __default__ has a special meaning : any other sections not found in the dictionary
        _allObjects.objectSortDescriptorForSection = @{@"__default__": [self sortDescriptorsForContact], tradInvitation: @[ [self sortDescriptorForInvitationByDate] ]};
        
        _serviceManager = [ServicesManager sharedInstance];
        _contactsManager = _serviceManager.contactsManagerService;
        _conversationsManagerService = _serviceManager.conversationsManagerService;
        _groupsService = _serviceManager.groupsService;
        _myUser = _serviceManager.myUser;
        _populated = NO;
    }
    return self;
}

-(NSArray<NSSortDescriptor *> *) sortDescriptorsForContact {
    if ([ServicesManager sharedInstance].contactsManagerService.sortByFirstName) {
        return @[[UITools sortDescriptorForContactByFirstName], [UITools sortDescriptorForContactByLastName]];
    } else {
        return @[[UITools sortDescriptorForContactByLastName], [UITools sortDescriptorForContactByFirstName]];
    }
}

- (NSSortDescriptor *) sortDescriptorForInvitationByDate {
    NSSortDescriptor *sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"requestedInvitation.date" ascending:YES comparator:^NSComparisonResult(NSDate* obj1, NSDate* obj2) {
        return [obj1 compare:obj2];
    }];
    return sortByDate;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideGuestModeView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowGuestModeView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupsServiceDidAddGroup object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupsServiceDidUpdateGroup object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupsServiceDidRemoveGroup object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupsServiceDidRemoveAllGroups object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidAddInvitation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateInvitation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidRemoveInvitation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidAddContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidRemoveContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompaniesServiceDidAddCompanyInvitation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompaniesServiceDidUpdateCompanyInvitation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompaniesServiceDidRemoveCompanyInvitation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dumpContacts" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateInvitationPendingNumber object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompaniesServiceDidUpdateCompanyInvitationPendingNumber object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidEndPopulatingMyNetwork object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidStartConversation object:nil];
    
    [self didLogout:nil];
    
    [_groupsList removeAllObjects];
    _groupsList = nil;
    
    [_allObjects removeAllObjects];
    _allObjects = nil;
    
    _contactsManager.delegate = nil;
    _contactsManager = nil;
    _conversationsManagerService = nil;
    _myUser = nil;
    _groupsService = nil;
}

-(void) shouldUpdateBadgeValue {
    if([NSThread isMainThread]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [self shouldUpdateBadgeValue];
        });
        return;
    }
    NSAssert(![NSThread isMainThread], @"This code must not be invoked on main thread !!!");
    NSUInteger totalPendingContacts = [_contactsManager totalNbOfPendingInvitations];
    NSUInteger totalPendingCompanyInvitation = [[ServicesManager sharedInstance].companiesService totalNbOfPendingCompanyInvitations];
    
    NSUInteger totalInvitations = totalPendingContacts+totalPendingCompanyInvitation;
    dispatch_async(dispatch_get_main_queue(), ^{
        _tabSelectorInvitationsButton.titleLabel.badgeView.badgeValue = totalInvitations;
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ((MyInfoNavigationItem*)self.navigationItem).parentViewController = self;
    
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    self.title = NSLocalizedString(@"Contacts", nil);
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];

    [_tabSelector setItems:@[NSLocalizedString(@"My colleagues", nil), NSLocalizedString(@"My lists", nil), NSLocalizedString(@"Invitations", nil)]];
    _tabSelector.delegate = self;
    _tabSelector.showsCount = NO;
    _tabSelector.autoAdjustSelectionIndicatorWidth = NO;
    _tabSelector.height = 60;
    _tabSelector.selectionIndicatorHeight = 4.0f;
    _tabSelector.bouncySelectionIndicator = YES;
        
    _tabSelectorInvitationsButton = [_tabSelector buttonAtIndex:SelectorTypeInvitations];
    _tabSelectorInvitationsButton.titleLabel.badgeView.badgeColor = [UITools notificationBadgeColor];
    _tabSelectorInvitationsButton.titleLabel.badgeView.outlineWidth = 2.0;
    _tabSelectorInvitationsButton.titleLabel.badgeView.position = MGBadgePositionTopRight;
    _tabSelectorInvitationsButton.titleLabel.badgeView.textColor = [UIColor whiteColor];
    _tabSelectorInvitationsButton.titleLabel.badgeView.font = [UIFont fontWithName:[UITools defaultFontName] size:11];
    _tabSelectorInvitationsButton.titleLabel.badgeView.displayIfZero = NO;
    _tabSelectorInvitationsButton.titleLabel.badgeView.bageStyle = MGBadgeStyleRoundRect;
    
    [_tabSelector addTarget:self action:@selector(tabSelectorValueChanged:) forControlEvents:UIControlEventValueChanged];
    CGRect frameTab = _tabSelector.frame;
    frameTab.size.width = self.view.frame.size.width;
    _tabSelector.frame = frameTab;
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, _tabSelector.height, 0);
    
    self.definesPresentationContext = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    // A little trick for removing the cell separators
    self.tableView.tableFooterView = [UIView new];
    self.tableView.sectionIndexColor = [UITools defaultTintColor];
    // If we use clear color for this, the a-z index column will overlap with the section headers, etc.
    self.tableView.sectionIndexBackgroundColor = [UITools defaultBackgroundColor];
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];

    [self filterByFilterType:FilterTypeStoredValue];
    [self sortBySortOrder:SortOrderStoredValue];
    
    
    
    [self displayGoodButtonForSelectorType:_tabSelector.selectedSegmentIndex];
    [self drawGoodImageForOrganizeButtonItem];
    
    [_allowAccessContactsButton setTitle:NSLocalizedString(@"Edit", nil) forState:UIControlStateNormal];
    _allowAccessContactsLabel.text = NSLocalizedString(@"Allow access to local contacts", nil);
    
    // Invite view & button
    [self setInviteViewVisibble:NO];
    [_inviteButton setTitle:NSLocalizedString(@"Invite a contact", nil) forState:UIControlStateNormal];
    _inviteButton.tintColor = [UITools defaultTintColor];
    [UITools applyCustomFontTo:_inviteButton.titleLabel];
    _inviteButton.titleEdgeInsets = UIEdgeInsetsMake((_inviteButton.imageView.frame.size.height+3), -_inviteButton.imageView.frame.size.width, 0, 0);
    _inviteButton.imageEdgeInsets = UIEdgeInsetsMake(-(_inviteButton.titleLabel.bounds.size.height+3), 0, 0, -_inviteButton.titleLabel.bounds.size.width);
    
    //
    [_inviteAllButton setTitle:NSLocalizedString(@"Invite all", nil) forState:UIControlStateNormal];
    _inviteAllButton.tintColor = [UITools defaultTintColor];
    [UITools applyCustomFontTo:_inviteAllButton.titleLabel];
    _inviteAllButton.titleEdgeInsets = UIEdgeInsetsMake((_inviteAllButton.imageView.frame.size.height+3), -_inviteAllButton.imageView.frame.size.width, 0, 0);
    _inviteAllButton.imageEdgeInsets = UIEdgeInsetsMake(-(_inviteAllButton.titleLabel.bounds.size.height+3), 0, 0, -_inviteAllButton.titleLabel.bounds.size.width);

    // Add a banner if we don't have access to local contacts
    if (!_contactsManager.localDeviceAccessGranted) {
        _headerView.clipsToBounds = NO;
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
        _headerViewHeight = _headerView.frame.size.height;
        
        [self.tableView addParallaxWithView:_headerView andHeight:_headerViewHeight andMinHeight:_headerViewHeight andShadow:NO];
        
        // Force the parallax view to be at the top position.
        // Without this the tableview section headers goes on top of the parallax view.
        self.tableView.parallaxView.layer.zPosition = 1;
        
        if (_tabSelector.selectedSegmentIndex != SelectorTypeInvitations) {
            [self setShowsParallaxHeader:NO];
        }
        
        _inviteButton.enabled = NO;
        _inviteAllButton.enabled = NO;
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    BOOL isDisplayingNetworkView = ((CustomNavigationController *)self.navigationController).isDisplayingNetworkLostView;
    if(isDisplayingNetworkView  && !_lostNetworkViewAsBeenScrolled){
        [self didLostConnection:nil];
        [self willShowNetworkLostView:nil];
    }
    
    BOOL isGuestBannerView = ((CustomNavigationController *)self.navigationController).isDisplayingGuestModeView;
    if(isGuestBannerView  && !_guestBannerViewAsBeenScrolled){
        [self willShowGuestModeView:nil];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myInfoNavigationItemWillPresentNewView:) name:@"myInfoNavigationItemWillPresentNewView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myInfoNavigationItemWillPresentNewView:) name:@"myInfoNavigationItemWillPresentMyInfos" object:nil];
    
    if (!_populated) {
        [self didEndPopulatingMyNetwork:nil];
    }
    
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    
    CGSize b1textSize = [_tabSelectorInvitationsButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:_tabSelectorInvitationsButton.titleLabel.font}];
    if(b1textSize.width > _tabSelectorInvitationsButton.frame.size.width )
        // Truncated text
        _tabSelectorInvitationsButton.titleLabel.badgeView.horizontalOffset = -10;
    else
        _tabSelectorInvitationsButton.titleLabel.badgeView.horizontalOffset = 0;
    
    [self shouldUpdateBadgeValue];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if(_lostNetworkViewAsBeenScrolled){
        [self didHideNetworkLostView:nil];
    }
    if(_guestBannerViewAsBeenScrolled){
        [self didHideGuestModeView:nil];
    }
    if(_organizeContactsTableViewController.view.superview){
        [_organizeContactsTableViewController.view removeFromSuperview];
        self.tableView.userInteractionEnabled = YES;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"myInfoNavigationItemWillPresentNewView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"myInfoNavigationItemWillPresentMyInfos" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void) setPendingInvitation:(Invitation *)invitation {
    _pendingInvitation = invitation;
}

#pragma mark - Rotation
-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void) presentContactDetailController:(Contact*) contact{
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    if(_tabSelector.selectedSegmentIndex == SelectorTypeGroups) {
        return 1;
    }
    
    return [[_allObjects sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if(_tabSelector.selectedSegmentIndex == SelectorTypeGroups) {
        return _groupsList.count;
    }
    
    NSString *key = [[_allObjects sections] objectAtIndex:section];
    return [[_allObjects objectsInSection:key] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    // GROUP
    if(_tabSelector.selectedSegmentIndex == SelectorTypeGroups) {
        UIGroupsTableViewCell *cell = (UIGroupsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kGroupsCellTableViewReusableKey forIndexPath:indexPath];
        cell.group = [_groupsList objectAtIndex:indexPath.row];
        return cell;
    }
    
    // CONTACT
    else {
        NSString *key = [[_allObjects sections] objectAtIndex:indexPath.section];
        Contact *contact = [[_allObjects objectsInSection:key] objectAtIndex:indexPath.row];
        
        // Use a special cell for pending contact, so we can answer directly from the tableview
        if (contact.requestedInvitation.status == InvitationStatusPending || (contact.companyInvitation.status == InvitationStatusPending && contact.companyInvitation.direction == InvitationDirectionReceived)) {
            UIContactsInvitationTableViewCell *cell = (UIContactsInvitationTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kContactsInvitationTableViewReusableKey forIndexPath:indexPath];
            cell.contactsManagerService = _contactsManager;
            cell.cellObject = contact;
            if(contact.requestedInvitation)
                cell.invitation = contact.requestedInvitation;
            if(contact.companyInvitation)
                cell.invitation = contact.companyInvitation;
            return cell;
        }
        
        else {
            UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
            
            cell.cellObject = (NSObject<UIRainbowGenericTableViewCellProtocol>*)contact;
            cell.cellButtonTapHandler = nil;
            
            if( !contact.sentInvitation && (contact.canChatWith || contact.phoneNumbers.count > 0 || contact.emailAddresses.count > 0) ) {
                __weak typeof(cell) weakCell = cell;
                cell.cellButtonTapHandler = ^(UIButton *sender) {
                    Contact *contact = (Contact *)weakCell.cellObject;
                    
                    if(!contact.canChatWith && (contact.phoneNumbers.count > 0 || contact.emailAddresses.count > 0)) {
                        // If the contact is not yet invited, we can invite him.
                        if(!contact.sentInvitation) {
                            [UITools showInviteContactAlertController:contact fromController:self controllerDelegate:self withCompletionHandler:^(Invitation *invitation) {
                                if(invitation){
                                    _pendingInvitation = nil;
                                    _pendingInvitation = invitation;
                                }
                            }];
                        } else if(contact.sentInvitation){
                            // TODO : try to display the action normally done by gesture
                        }
                    } else
                        [self showContactDetails:(Contact *)weakCell.cellObject];
                };
            }
            
            return cell;
        }
    }
}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    if(_tabSelector.selectedSegmentIndex == SelectorTypeGroups)
        return @"";
    
    return [[_allObjects sections] objectAtIndex:section];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];

    // Nothing to do in case of Group, done by InterfaceBuilder
    if(_tabSelector.selectedSegmentIndex == SelectorTypeMyColleaguesOnly || _tabSelector.selectedSegmentIndex == SelectorTypeInvitations) {
        
        NSString *key = [[_allObjects sections] objectAtIndex:indexPath.section];
        Contact *contact = [[_allObjects objectsInSection:key] objectAtIndex:indexPath.row];
        
        if(contact.canChatWith)
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
        
       else
           [self showContactDetails:contact];

    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

    if(_tabSelector.selectedSegmentIndex == SelectorTypeGroups)
        return YES;
    
    NSString *key = [[_allObjects sections] objectAtIndex:indexPath.section];
    Contact *contact = [[_allObjects objectsInSection:key] objectAtIndex:indexPath.row];
    
    if(contact.sentInvitation.status == InvitationStatusPending && (contact.emailAddresses.count > 0 || contact.phoneNumbers.count > 0) )
        return YES;
    return (contact.isRainbowUser && contact.isInRoster && !contact.isBot);
}

-(void)removeContextualAction:(Group *)group {
    [_groupsService deleteGroup:group];
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"group" action:@"delete" label:nil value:nil];
}

-(void)cancelContextualAction:(Contact *)contact {
    MBProgressHUD *hud = [UITools showHUDWithMessage:NSLocalizedString(@"Canceling invitation ...", nil) forView:[UIApplication sharedApplication].keyWindow.rootViewController.view mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    [hud showAnimated:YES whileExecutingBlock:^{
        [_contactsManager deleteInvitationWithID:contact.sentInvitation];
    } completionBlock:^{
        [UITools showHUDWithMessage:NSLocalizedString(@"Invitation canceled",nil) forView:[UIApplication sharedApplication].keyWindow.rootViewController.view mode:MBProgressHUDModeCustomView dismissCompletionBlock:nil];
    }];
    [self.tableView setEditing:NO animated:YES];
}

-(void)resendContextualAction:(Contact *)contact {
    MBProgressHUD *hud = [UITools showHUDWithMessage:NSLocalizedString(@"Sending invitation ...", nil) forView:[UIApplication sharedApplication].keyWindow.rootViewController.view mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    [hud showAnimated:YES whileExecutingBlock:^{
        if(contact.sentInvitation.phoneNumber){
            [UITools showSMSViewControllerForInvitation:contact.sentInvitation inController:self controllerDelegate:self];
            return ;
        } else {
            [_contactsManager inviteContact:contact];
            [NSThread sleepForTimeInterval:1];
        }
    } completionBlock:^{
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"contact" action:@"invite" label:nil value:nil];
    }];
    [self.tableView setEditing:NO animated:YES];
}

-(void)removeContactContextualAction:(Contact *)contact {
    [_contactsManager removeContactFromMyNetwork:contact];
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"contact" action:@"delete" label:nil value:nil];
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(_tabSelector.selectedSegmentIndex == SelectorTypeGroups){
        Group *group = [_groupsList objectAtIndex:indexPath.row];
        BGTableViewRowActionWithImage *remove = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Remove", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"removeContact"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
            [self removeContextualAction:group];
        }];
        return @[remove];
    }
    
    NSString *key = [[_allObjects sections] objectAtIndex:indexPath.section];
    Contact *contact = [[_allObjects objectsInSection:key] objectAtIndex:indexPath.row];
    
    if(contact.sentInvitation.status == InvitationStatusPending){
        BGTableViewRowActionWithImage *removeInvitation = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Cancel", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"close_conversation"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
            [self cancelContextualAction:contact];
        }];
        
        BGTableViewRowActionWithImage *recallInvitation = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Resend", nil) backgroundColor:[UIColor lightGrayColor] image:[UIImage imageNamed:@"recallInvitation"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
            [self resendContextualAction:contact];
        }];
        
        return @[removeInvitation, recallInvitation];
    }
    
    if(contact.isRainbowUser && !contact.isBot && contact.isInRoster) {
        BGTableViewRowActionWithImage *remove = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Remove", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"removeContact"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
            [self removeContactContextualAction:contact];
        }];
        
        return @[remove];
    }
    return nil;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if(_tabSelector.selectedSegmentIndex == SelectorTypeGroups || _tabSelector.selectedSegmentIndex == SelectorTypeInvitations)
        return nil;
    
    if(_selectedSortOrder == SortOrderByCompany)
        return nil;
    
    if([self numberOfSectionsInTableView:tableView] > 0)
        return _letters;
    else
        return nil;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [[_allObjects sections] indexOfObject:title];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    view.tintColor = [UITools colorFromHexa:0xF8F8F8FF];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont fontWithName:[UITools defaultFontName] size:20]];
    [header.textLabel setTextColor:[UITools colorFromHexa:0xB8B8B8FF]];
}

-(void) insertContact:(Contact *) contact {
    // Ignore myself.
    if (contact == _serviceManager.myUser.contact)
        return;
    
    if(contact.isBot)
        return;
    
    if (!contact.isInRoster || contact.isTerminated || [contact.lastName isEqualToString:@"***"] || [contact.firstName isEqualToString:@"***"])
        return;
    
    if (![_allObjects containsObject:contact]) {
        [_allObjects addObject:contact];
    }
}

-(void) removeContact:(Contact *) contact {
    [_allObjects removeObject:contact];
}

#pragma mark - Navigation
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(nullable id)sender {
    if([identifier isEqualToString:@"seguePushContactDetailController"]) {
        CGPoint point = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
        
        NSString *key = [[_allObjects sections] objectAtIndex:indexPath.section];
        Contact *contact = [[_allObjects objectsInSection:key] objectAtIndex:indexPath.row];
        
        if(!contact.canChatWith && ([contact.emailAddresses count] || [contact.phoneNumbers count])) {
            // If the contact is not yet invited, we can invite him.
            if(!contact.sentInvitation) {
                [UITools showInviteContactAlertController: contact fromController:self controllerDelegate:self withCompletionHandler:^(Invitation *invitation) {
                    if(invitation){
                        _pendingInvitation = nil;
                        _pendingInvitation = invitation;
                    }
                }];
            }
            // cancel the performSegue which open the vCard.
            return NO;
        }
        
        return YES;
    }
    if([identifier isEqualToString:@"showGroupDetailsSegueID"]){
        return YES;
    }
    if([identifier isEqualToString:@"createNewGroupSegueID"])
        return YES;
    return NO;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"showGroupDetailsSegueID"]){
        UIGroupDetailsViewController *destViewController = [segue destinationViewController];
        CGPoint point = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
        Group *selectedGroup = [_groupsList objectAtIndex:indexPath.row];
        destViewController.fromView = self;
        destViewController.group = selectedGroup;
    }
    else if( [[segue identifier] isEqualToString:@"inviteBySegueID"]) {
        UIView *dimView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.4f;
        dimView.userInteractionEnabled = NO;
        [self.view addSubview:dimView];
        
        InviteByViewController *inviteByViewController = [segue destinationViewController];
        inviteByViewController.popoverPresentationController.delegate = self;
        inviteByViewController.popoverPresentationController.sourceRect = CGRectMake(0, 0, inviteByViewController.view.frame.size.width, inviteByViewController.view.frame.size.height);
        inviteByViewController.dimView = dimView;
    }
}

#pragma mark - Login/Logout notification
-(void) didLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    
    [self checkAndEnableAddGroupButton];
    
    _refreshUITimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(reloadUI:) userInfo:nil repeats:NO];
    
    // Workaround : Check if our contact is in the displayed list, if it's the case remove the contact
    if ([_allObjects containsObject:_serviceManager.myUser.contact])
        [self removeContact:_serviceManager.myUser.contact];
}

-(void) didLogout:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    _populated = NO;
    [_allObjects removeAllObjects];
    if([self isViewLoaded])
        [self.tableView reloadData];
    
    _addGroupBarButtonItem.enabled = NO;

    _organizeBarButtonItem.image = [UIImage imageNamed:@"filter"];
    _tabSelector.selectedSegmentIndex = SelectorTypeMyColleaguesOnly;
    [self tabSelectorValueChanged:(UISegmentedControl*)_tabSelector];
    
    [_refreshUITimer invalidate];
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if([self isViewLoaded]) {
        _addGroupBarButtonItem.enabled = NO;
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded]) {
        [self checkAndEnableAddGroupButton];
    }
}

#pragma mark - network lost & guest mode banners
-(void) didChangeNetworkLostViewFrame:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
}

-(void) willShowNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant += kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

-(void) didHideNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant -= kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = YES;
        }];
    }
}

-(void) didHideGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _guestBannerViewAsBeenScrolled){
        self.topConstraint.constant -= kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:nil];
    }
}

-(void) willShowGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_guestBannerViewAsBeenScrolled){
        self.topConstraint.constant += kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

#pragma mark - DZNEmptyDataSet
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    if(_tabSelector.selectedSegmentIndex == SelectorTypeGroups) {
        return [UIImage imageNamed:@"EmptyGroups"];
    } else {
        if (_isFiltering) {
            return [UIImage imageNamed:@"EmptyAddressBook"];
        } else
            return [UIImage imageNamed:@"EmptyAddressBookCreate"];
    }
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultTintColor];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = nil;
    
    if(_tabSelector.selectedSegmentIndex == SelectorTypeGroups) {
        text = NSLocalizedString(@"Organize a set of contacts and group them together.", nil);
    } else if(_tabSelector.selectedSegmentIndex == SelectorTypeInvitations) {
        if (_isFiltering)
            text = NSLocalizedString(@"You have no contacts\n matching this filter", nil);
        else
            text = NSLocalizedString(@"You have no pending invitation and no invitation has been sent.", nil);
    } else {
        if (_isFiltering)
            text = NSLocalizedString(@"You have no contacts\n matching this filter", nil);
        else
            text = NSLocalizedString(@"You have no contacts yet.", nil);
    }
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"";
    
    if(_tabSelector.selectedSegmentIndex != SelectorTypeGroups) {
        if (!_isFiltering) {
            text = NSLocalizedString(@"Invite people to join your network.", nil);
        }
    }
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    NSString *buttonTitle = @"";
    if(_tabSelector.selectedSegmentIndex == SelectorTypeGroups)
        buttonTitle = NSLocalizedString(@"Create a list", nil);
    else if(_tabSelector.selectedSegmentIndex == SelectorTypeInvitations)
        buttonTitle = NSLocalizedString(@"Invite a contact", nil);
    else {
        if (_isFiltering)
            return nil;
        else
            return nil;
    }
    
    return [[NSAttributedString alloc] initWithString:buttonTitle attributes:@{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor whiteColor]}];
}

-(UIImage *) buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    // Change button color for guest user
    if ([ServicesManager sharedInstance].myUser.isGuest) {
        return [UITools imageWithColor:[UITools colorFromHexa:dznButtonDisabledColor] size:CGSizeMake(1, 40)];
    } else {
        return [UITools imageWithColor:[UITools defaultTintColor] size:CGSizeMake(1, 40)];
    }
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    // Disable interaction if user is guest
    return [ServicesManager sharedInstance].myUser.isGuest ? NO : YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

- (void)emptyDataSetDidTapButton:(UIScrollView *)scrollView {
    if(_tabSelector.selectedSegmentIndex == SelectorTypeGroups){
        [self performSegueWithIdentifier:@"createNewGroupSegueID" sender:nil];
        return;
    }
//    if(_tabSelector.selectedSegmentIndex == SelectorTypeMyColleaguesOnly){
//        _tabSelector.selectedSegmentIndex = SelectorTypeInvitations;
//        [self tabSelectorValueChanged:(UISegmentedControl*)_tabSelector];
//        return;
//    }
    if(_tabSelector.selectedSegmentIndex == SelectorTypeInvitations){
        [self didTapInviteButton:nil];
    }
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    CGFloat offset = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
    offset += CGRectGetHeight(self.navigationController.navigationBar.frame);
    offset -= CGRectGetHeight(_tabSelector.frame);
    return -offset;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 0.0f;
}

#pragma mark - Conversations notifications
#pragma mark - Conversation notifications
-(void) didStartConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didStartConversation:notification];
        });
        return;
    }
    
    // We don't want to open the conversation view if the application is in background, case of message send from a push notification.
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
        return;
    
    if(!self.isVisible)
        return;
    
    Conversation *theConversation = (Conversation *)notification.object;
    [RecentsConversationsTableViewController openConversationViewForConversation:theConversation inViewController:self.navigationController];
}

#pragma mark - Contacts Services Manager notifications

-(void) didEndPopulatingMyNetwork:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didEndPopulatingMyNetwork:notification];
        });
        return;
    }
    NSLog(@"DID END POPULATING MY NETWORK");
    [_contactsManager.myNetworkContacts enumerateObjectsUsingBlock:^(Contact * contact, NSUInteger idx, BOOL * stop) {
        [self insertContact:contact];
    }];
    
    [self requestRefreshUI];
    
    _populated = YES;
}

-(void) didAddContact:(NSNotification *) notification {
    if(!_populated)
        return;
    
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddContact:notification];
        });
        return;
    }
    
    Contact *contact = notification.object;
    [self insertContact:contact];

    if([self isViewLoaded] && _populated){
        [self requestRefreshUI];
    }
}

-(void) didUpdateContact:(NSNotification *) notification {
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Contact *contact = [userInfo objectForKey:kContactKey];
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
    
    BOOL needUpdate = NO;
    if(_populated){
        // We refresh the UI only when only displayed information changed
        if([changedKeys containsObject:@"isInRoster"] || [changedKeys containsObject:@"isPresenceSubscribed"]){
            if([self isViewLoaded] && _populated){
                needUpdate = YES;
            }
        }
    }
    
    if(needUpdate){
        [self didEndPopulatingMyNetwork:nil];
    }
}

-(void) didRemoveContact:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveContact:notification];
        });
        return;
    }
    
    Contact *contact = notification.object;
    [self removeContact:contact];
    
    if(!_populated)
        return;
    
    [self requestRefreshUI];
}

-(void) didRemoveAllContacts:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveAllContacts:notification];
        });
        return;
    }
    
    [self requestRefreshUI];
}

-(void) didAddInvitation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddInvitation:notification];
        });
        return;
    }
    
    // Dont forget to flush the cache data !
    // Because we have updated an object (the contact linked to this invitation)
    // But the filters/sort have not changed, so cache has not been cleaned
    // and the updated element might have changed of section, or should be filtered, ...
    [self requestRefreshUI];
}

-(void) didUpdateInvitation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateInvitation:notification];
        });
        return;
    }
    
    // Dont forget to flush the cache data !
    // Because we have updated an object (the contact linked to this invitation)
    // But the filters/sort have not changed, so cache has not been cleaned
    // and the updated element might have changed of section, or should be filtered, ...
    [self requestRefreshUI];
}

-(void) didRemoveInvitation:(NSNotification *) notification {
    // We reuse the did Update invitation code because there is nothing else todo.
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateInvitation:notification];
        });
        return;
    }
    [self didUpdateInvitation:notification];
}

#pragma mark - Tab selector
- (IBAction)tabSelectorValueChanged:(UISegmentedControl *)sender {
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    // Update the filter
    if(sender.selectedSegmentIndex == SelectorTypeMyColleaguesOnly) {
        _allObjects.globalFilteringPredicate = _globalFilterMyNetwork;
    }
    if (sender.selectedSegmentIndex == SelectorTypeInvitations) {
        _allObjects.globalFilteringPredicate = _globalFilterAllInvited;
    }
    
    [self filterByFilterType:FilterTypeStoredValue];
    [self sortBySortOrder:SortOrderStoredValue];
    
    [self drawGoodImageForOrganizeButtonItem];
    
    if([self isViewLoaded]) {
        if (_tabSelector.selectedSegmentIndex == SelectorTypeInvitations) {
            [self setInviteViewVisibble:YES];
            if (self.tableView.parallaxView)
                [self setShowsParallaxHeader:YES];
        } else {
            [self setInviteViewVisibble:NO];
            if (self.tableView.parallaxView)
                [self setShowsParallaxHeader:NO];
        }
        
        [self reloadUI:nil];
        [self displayGoodButtonForSelectorType:_tabSelector.selectedSegmentIndex];
        
        if(_myUser.isGuest){
            [_inviteButton setUserInteractionEnabled:NO];
            [_inviteButton setBackgroundColor:[UITools colorFromHexa:dznButtonDisabledColor]];
            [_inviteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_inviteButton setTintColor:[UIColor whiteColor]];
            
            [_inviteAllButton setUserInteractionEnabled:NO];
            [_inviteAllButton setBackgroundColor:[UITools colorFromHexa:dznButtonDisabledColor]];
            [_inviteAllButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_inviteAllButton setTintColor:[UIColor whiteColor]];
        }
    }
}

-(void) setInviteViewVisibble:(BOOL) visible {
    
    if(visible) {
        _tableViewTopConstraint.constant = 70;
        _inviteView.hidden = NO;
    } else {
        _tableViewTopConstraint.constant = 0;
        _inviteView.hidden = YES;
    }
}

// Change also the tableview top inset
// when show/hide the header view.
-(void) setShowsParallaxHeader:(BOOL) show {
    if (show) {
        [self.tableView setShowsParallax:YES];
        UIEdgeInsets newInset = self.tableView.contentInset;
        newInset.top = self.tableView.parallaxView.originalTopInset + _headerViewHeight;
        self.tableView.contentInset = newInset;
        [self.tableView setContentOffset:CGPointMake(0.0f, -self.tableView.contentInset.top) animated:NO];
    } else {
        [self.tableView setShowsParallax:NO];
        UIEdgeInsets newInset = self.tableView.contentInset;
        newInset.top = self.tableView.parallaxView.originalTopInset;
        self.tableView.contentInset = newInset;
    }
}

-(void) checkAndEnableAddGroupButton {
    if([ServicesManager sharedInstance].myUser.isGuest){
        _addGroupBarButtonItem.enabled = NO;
    }else {
        _addGroupBarButtonItem.enabled = YES;
    }
}

-(void) changeContactDisplaySettings:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self changeContactDisplaySettings:notification];
        });
        return;
    }
    NSLog(@"%@ Reload UI on contact display settings changed", self);
    [self requestRefreshUI];
}

#pragma mark - Groups notifications
-(void) didAddGroup:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddGroup:notification];
        });
        return;
    }
    
    Group *group = (Group *)notification.object;
    if(![_groupsList containsObject:group]){
        [_groupsList addObject:group];
    }
    
    [_groupsList sortUsingDescriptors:@[[UITools sortDescriptorForGroupByName]]];

    [self requestRefreshUI];
}

-(void) didUpdateGroup:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateGroup:notification];
        });
        return;
    }
    [self requestRefreshUI];
}

-(void) didRemoveGroup:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveGroup:notification];
        });
        return;
    }
    
    Group *group = (Group *)notification.object;
    
    if([_groupsList containsObject:group]){
        [_groupsList removeObject:group];
        [_groupsList sortUsingDescriptors:@[[UITools sortDescriptorForGroupByName]]];
    }
    
    [self requestRefreshUI];
}

-(void) didRemoveAllGroups:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveAllGroups:notification];
        });
        return;
    }
    
    [_groupsList removeAllObjects];
    
    [self requestRefreshUI];
}

-(void) displayGoodButtonForSelectorType:(SelectorType) selectorType {
    NSMutableArray <UIBarButtonItem*> *buttons = [NSMutableArray array];
    switch (selectorType) {
        case SelectorTypeGroups: {
            [buttons addObject:_addGroupBarButtonItem];
            break;
        }
        case SelectorTypeMyColleaguesOnly: {
            [buttons addObject:_organizeBarButtonItem];
            break;
        }
        case SelectorTypeInvitations: {
            //[buttons addObject:_organizeBarButtonItem];
            break;
        }
    }
    ((MyInfoNavigationItem*)self.navigationItem).customRightItems = buttons;
}

-(void) drawGoodImageForOrganizeButtonItem {
    if(_isFiltering || _isSorting) {
        _organizeBarButtonItem.image = [UIImage imageNamed:@"filterOn"];
    } else {
        _organizeBarButtonItem.image = [UIImage imageNamed:@"filter"];
    }
}

#pragma mark - Contact organization
- (IBAction) organizeButtonClicked:(id)sender {
    BOOL toggle = !(_organizeContactsTableViewController.view.superview) ? YES : NO;
    if(toggle){
        _organizeContactsTableViewController = [[UIStoryboardManager sharedInstance].organizeStoryBoard instantiateViewControllerWithIdentifier:@"organizeStoryboardID"];
        _organizeContactsTableViewController.delegate = self;
        _organizeContactsTableViewController.sortArray = @[NSLocalizedString(@"Firstname Name", nil),NSLocalizedString(@"By company", nil)],
        _organizeContactsTableViewController.filterArray = @[NSLocalizedString(@"All", nil),NSLocalizedString(@"Connected", nil)];
        _organizeContactsTableViewController.selectedSortIndex = _selectedSortOrder ;
        _organizeContactsTableViewController.selectedFilterIndex = _selectedFilterType;
        _organizeContactsTableViewController.isSubView = YES;
        _organizeContactsTableViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        
        [_organizeContactsTableViewController setViewHorizontalPosition:self.tableView.frame.origin.y - self.tabSelector.frame.size.height];
        
        UIWindow *keyWindow = [UIApplication sharedApplication].windows[0];
        [keyWindow addSubview:_organizeContactsTableViewController.view];
        [keyWindow bringSubviewToFront:_organizeContactsTableViewController.view];
    } else {
        [_organizeContactsTableViewController.view removeFromSuperview];
        self.tableView.userInteractionEnabled = YES;
    }
}

// Button in the banner displayed
// when we don't have access to the local contacts
- (IBAction)allowAccesToLocalContacts:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

-(void) filterByFilterType:(FilterType) filterType {
    
    // Get saved filters (if exists)
    // Form of : @"contactsFilters": @{@"MyNetwork": (@"Online"|@"All"|ø), @"All": (@"Invited"|@"All"|ø)}
    NSDictionary *storedFilters = [[NSUserDefaults standardUserDefaults] objectForKey:@"contactsFilters"];
    NSMutableDictionary *contactsFilters = [NSMutableDictionary new];
    [contactsFilters addEntriesFromDictionary:storedFilters];
    
    if (_tabSelector.selectedSegmentIndex == SelectorTypeMyColleaguesOnly) {
        
        // If we ask for the stored value, we override the filterType parameter
        // with the stored value (or default value if no stored one)
        if (filterType == FilterTypeStoredValue) {
            // Default to All
            filterType = FilterTypeAll;
            
            NSString *storedFilter = [contactsFilters objectForKey:@"MyNetwork"];
            if ([storedFilter isEqualToString:@"Online"]) {
                filterType = FilterTypeConnectedOrInvited;
            }
        }
        
        // Then apply the filterType (which could be real parameter or override param with stored value)
        if (filterType == FilterTypeAll) {
            _allObjects.globalFilteringPredicate = _globalFilterMyNetwork;
            [contactsFilters setObject:@"All" forKey:@"MyNetwork"];
            
        } else if (filterType == FilterTypeConnectedOrInvited) {
            _allObjects.globalFilteringPredicate = _globalFilterMyNetworkOnline;
            [contactsFilters setObject:@"Online" forKey:@"MyNetwork"];
        }
        
    } else if (_tabSelector.selectedSegmentIndex == SelectorTypeInvitations) {
        
        // If we ask for the stored value, we override the filterType parameter
        // with the stored value (or default value if no stored one)
        if (filterType == FilterTypeStoredValue) {
            // Default to All
            filterType = FilterTypeAll;
            
            NSString *storedFilter = [contactsFilters objectForKey:@"All"];
            if ([storedFilter isEqualToString:@"Invited"]) {
                filterType = FilterTypeConnectedOrInvited;
            }
        }
        
        if (filterType == FilterTypeAll) {
            _allObjects.globalFilteringPredicate = _globalFilterAllInvited;
            [contactsFilters setObject:@"All" forKey:@"All"];
            
        } else if (filterType == FilterTypeConnectedOrInvited) {
            _allObjects.globalFilteringPredicate = _globalFilterAllInvited;
            [contactsFilters setObject:@"Invited" forKey:@"All"];
        }
    }
    
    // Store new filters
    [[NSUserDefaults standardUserDefaults] setObject:contactsFilters forKey:@"contactsFilters"];
    
    _isFiltering = (filterType != FilterTypeAll);
    _selectedFilterType = filterType;
    
    [self requestRefreshUI];
}

-(void) sortBySortOrder:(SortOrder) sortOrder {
    
    // Get saved sorting order (if exists)
    // Form of : @"contactsSortingOrders": @{@"MyNetwork": (@"Name"|@"Company"|ø), @"All": (@"Name"|@"Company"|ø)}
    NSDictionary *storedSortingOrders = [[NSUserDefaults standardUserDefaults] objectForKey:@"contactsSortingOrders"];
    NSMutableDictionary *contactsSortingOrders = [NSMutableDictionary new];
    [contactsSortingOrders addEntriesFromDictionary:storedSortingOrders];
    
    if (_tabSelector.selectedSegmentIndex == SelectorTypeMyColleaguesOnly) {
        
        // If we ask for the stored value, we override the sortOrder parameter
        // with the stored value (or default value if no stored one)
        if (sortOrder == SortOrderStoredValue) {
            // Default to Name
            sortOrder = SortOrderByName;
            
            NSString *storedSortingOrder = [contactsSortingOrders objectForKey:@"MyNetwork"];
            if ([storedSortingOrder isEqualToString:@"Company"]) {
                sortOrder = SortOrderByCompany;
            }
        }
        
        // Then apply the sortOrder (which could be real parameter or override param with stored value)
        if (sortOrder == SortOrderByName) {
            _allObjects.sectionNameFromObjectComputationBlock = _sectionedByName;
            [contactsSortingOrders setObject:@"Name" forKey:@"MyNetwork"];
            
        } else if (sortOrder == SortOrderByCompany) {
            _allObjects.sectionNameFromObjectComputationBlock = _sectionedByCompany;
            [contactsSortingOrders setObject:@"Company" forKey:@"MyNetwork"];
        }
        
    } else if (_tabSelector.selectedSegmentIndex == SelectorTypeInvitations) {
        
        // If we ask for the stored value, we override the sortOrder parameter
        // with the stored value (or default value if no stored one)
        if (sortOrder == FilterTypeStoredValue) {
            // Default to Name
            sortOrder = SortOrderByName;
            
            NSString *storedSortingOrder = [contactsSortingOrders objectForKey:@"All"];
            if ([storedSortingOrder isEqualToString:@"Company"]) {
                sortOrder = SortOrderByCompany;
            }
        }
        
        // Then apply the sortOrder (which could be real parameter or override param with stored value)
        if (sortOrder == SortOrderByName) {
            _allObjects.sectionNameFromObjectComputationBlock = _sectionedByName;
            [contactsSortingOrders setObject:@"Name" forKey:@"All"];
            
        } else if (sortOrder == SortOrderByCompany) {
            _allObjects.sectionNameFromObjectComputationBlock = _sectionedByCompany;
            [contactsSortingOrders setObject:@"Company" forKey:@"All"];
        }
    }
    
    // Store new sorting orders
    [[NSUserDefaults standardUserDefaults] setObject:contactsSortingOrders forKey:@"contactsSortingOrders"];
    
    _isSorting = (sortOrder != SortOrderByName);
    _selectedSortOrder = sortOrder;
    [self requestRefreshUI];
}

-(void) didTapInBackground:(UITapGestureRecognizer *) recognizer {
    [self organizeButtonClicked:_organizeBarButtonItem];
}

#pragma mark - CNContact picker
-(IBAction) didTapInviteButton:(id)sender {
    [self showInviteContactAlertController];
}

-(IBAction) didTapInviteAllButton:(id)sender {
    [UITools showInvitePopupForAllContacts:self withCompletionHandler:^{
        
        // display a loading spinner
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = NSLocalizedString(@"Sending invitations...", nil);
        hud.removeFromSuperViewOnHide = YES;
        [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
        
        // Fetch contacts
        NSError *error;
        CNContactStore *store = [[CNContactStore alloc] init];
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch: @[CNContactEmailAddressesKey]];
        NSMutableArray<CNContact*> *contactsToInvite = [NSMutableArray new];
        
        dispatch_group_t createGroup = dispatch_group_create();
        
        BOOL success = [store enumerateContactsWithFetchRequest:request error:&error usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop) {
            if (error) {
                [hud hide:YES];
                NSLog(@"error fetching contacts %@", error);
            } else {
                if(contact.emailAddresses.count > 0) {
                    [contactsToInvite addObject:contact];
                }
                
                if(contactsToInvite.count == 100) {
                    dispatch_group_enter(createGroup);
                    [_contactsManager inviteCNContacts:contactsToInvite withCompletionHandler:^(BOOL success) {
                        dispatch_group_leave(createGroup);
                        [contactsToInvite removeAllObjects];
                    }];
                }
            }
        }];
        
        if(success && contactsToInvite.count > 0) {
            dispatch_group_enter(createGroup);
            [_contactsManager inviteCNContacts:contactsToInvite withCompletionHandler:^(BOOL success) {
                dispatch_group_leave(createGroup);
            }];
        }
        
        dispatch_group_notify(createGroup, dispatch_get_main_queue(), ^{
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(@"Done", nil);
            hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
            [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:nil];
        });
    }];
}

- (void)contactPickerDidCancel:(CNContactPickerViewController *)picker {
}

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)person {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Picked contact is %@", person);
    
    Contact *contact = [ContactsManagerService createContactFromCNContact:person];
    [UITools showInviteContactAlertController:contact fromController:self controllerDelegate:self withCompletionHandler:^(Invitation *invitation) {}];
}

- (void)showInviteContactAlertController {
    UIAlertController *_inviteMenuActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [_inviteMenuActionSheet setTitle: [NSString stringWithFormat: NSLocalizedString(@"How would you like to invite your contact?", nil)] ];
    
    // --- Address book ---
    UIAlertAction *fromAddressBook = [UIAlertAction actionWithTitle:NSLocalizedString(@"By selecting one of your contacts", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        CNContactPickerViewController *contactPicker = [[CNContactPickerViewController alloc] init];
        contactPicker.delegate = self;
        // Allow only contact that have at least one email or one phone number
        contactPicker.predicateForEnablingContact = [NSPredicate predicateWithFormat:@"emailAddresses.@count > 0 || phoneNumbers.@count > 0"];
        
        [self presentViewController:contactPicker animated:YES completion:nil];
    }];
    [_inviteMenuActionSheet addAction:fromAddressBook];
    
    // --- Invite by email/phone number ---
    UIAlertAction *byEmailOrPhone = [UIAlertAction actionWithTitle:NSLocalizedString(@"By email or phone number", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self performSegueWithIdentifier:@"inviteBySegueID" sender:nil];
    }];
    [_inviteMenuActionSheet addAction:byEmailOrPhone];
    
    // --- CANCEL ---
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [_inviteMenuActionSheet addAction:cancel];
    
    // show the menu.
    [_inviteMenuActionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:_inviteMenuActionSheet animated:YES completion:nil];
}

- (UIModalPresentationStyle) adaptivePresentationStyleForPresentationController: (UIPresentationController * ) controller {
    return UIModalPresentationNone;
}

-(void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    NSLog(@"Did dissmiss popover");
}

#pragma mark - MFMessageComposeViewController delegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if( result == MessageComposeResultCancelled || result == MessageComposeResultFailed ) {
        NSLog(@"MessageComposeResultCancelled or MessageComposeResultFailed");
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if(_pendingInvitation){
                Contact * aContact = (Contact *)_pendingInvitation.peer;
                [_contactsManager deleteInvitationWithID:aContact.sentInvitation];
            }
        });
    } else {
        NSLog(@"MessageComposeResultSent");
        if(_pendingInvitation)
            _pendingInvitation = nil;
    }
}

#pragma mark - 3D touch actions
- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
    [super traitCollectionDidChange:previousTraitCollection];
    if ([UITools isForceTouchAvailableForTraitCollection:self.traitCollection]) {
        if (!self.previewingContext) {
            self.previewingContext = [self registerForPreviewingWithDelegate:self sourceView:self.view];
        }
    } else {
        if (self.previewingContext) {
            [self unregisterForPreviewingWithContext:self.previewingContext];
            self.previewingContext = nil;
        }
    }
}

- (UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location {
    CGPoint remapedLocation = [self.tableView convertPoint:location fromView:self.view];
    NSIndexPath *idx = [self.tableView indexPathForRowAtPoint:remapedLocation];
    if(idx == nil) {
        return nil;
    }
    
    UITableViewCell *tableCell = [self.tableView cellForRowAtIndexPath:idx];
    
    if(self.tabSelector.selectedSegmentIndex == SelectorTypeGroups){
        if ([self.presentedViewController isKindOfClass:[UIGroupDetailsViewController class]]) {
            return nil;
        }
        
        Group *theGroup = nil;
        if(idx){
            
            theGroup = [_groupsList objectAtIndex:idx.row];
            UIGroupDetailsViewController *groupDetails = [[UIStoryboardManager sharedInstance].groupDetailsStoryboard instantiateViewControllerWithIdentifier:@"groupDetailsViewControllerID"];
            groupDetails.group = theGroup;
            
            CustomNavigationController *navCtrl = [[CustomNavigationController alloc] initWithRootViewController:groupDetails];
            
            previewingContext.sourceRect = [self.view convertRect:tableCell.frame fromView:self.tableView];
            
            return navCtrl;
        }
    } else {
        if ([self.presentedViewController isKindOfClass:[UIContactDetailsViewController class]]) {
            return nil;
        }
        
        NSString *key = [[_allObjects sections] objectAtIndex:idx.section];
        Contact *theContact = [[_allObjects objectsInSection:key] objectAtIndex:idx.row];
        
        UIContactDetailsViewController* contactDetails = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
        contactDetails.contact = theContact;
        contactDetails.isPresenting3DTouch = YES;
        
        CustomNavigationController *navCtrl = [[CustomNavigationController alloc] initWithRootViewController:contactDetails];
        
        previewingContext.sourceRect = [self.view convertRect:tableCell.frame fromView:self.tableView];
        
        return navCtrl;
    }
    return nil;
}

- (void)previewingContext:(id<UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit {
    if([viewControllerToCommit isKindOfClass:[CustomNavigationController class]]){
        [self.navigationController pushViewController:((CustomNavigationController*)viewControllerToCommit).topViewController animated:YES];
    }
}

#pragma mark - Company Invitations
-(void) didAddCompanyInvitation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddCompanyInvitation:notification];
        });
        return;
    }
    NSLog(@"Did add company invitation");
    CompanyInvitation *companyInvitation = (CompanyInvitation *) notification.object;
    if(companyInvitation.peer){
        Contact *invintingContact = (Contact*)companyInvitation.peer;
        if(![_allObjects containsObject:invintingContact]){
            [_allObjects addObject:invintingContact];
            [self requestRefreshUI];
        }
    }
}

-(void) didUpdateCompanyInvitation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateCompanyInvitation:notification];
        });
        return;
    }
    NSLog(@"Did update company invitation");
    CompanyInvitation *companyInvitation = (CompanyInvitation *) notification.object;
    if(companyInvitation.peer){
        Contact *invintingContact = (Contact*)companyInvitation.peer;
        if(![_allObjects containsObject:invintingContact]){
            [_allObjects addObject:invintingContact];
        }
        
        [self requestRefreshUI];
    }
}

-(void) didRemoveCompanyInvitation:(NSNotification *) notification {
    //CompanyInvitation *companyInvitation = (CompanyInvitation *) notification.object;
}

#pragma mark - SearchController notification
-(void) myInfoNavigationItemWillPresentNewView:(NSNotification *) notification {
    if(_organizeContactsTableViewController.view.superview)
        [self organizeButtonClicked:_organizeBarButtonItem];
}


#pragma mark - show contact details
-(void) showContactDetails:(Contact *) contact {
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void) scrollToTop {
    [self.tableView setContentOffset:CGPointMake(0.0f, -self.tableView.contentInset.top) animated:YES];
}

-(void) dumpContacts {
    NSLog(@"DUMP CONTACTS : %@", [_allObjects description]);
}

#pragma mark - OrganizeTableViewController delegates

-(void) willSortBySortOrder:(NSInteger)selctedRow {
   _selectedSortOrder = selctedRow;
   [self sortBySortOrder:_selectedSortOrder];
   [self drawGoodImageForOrganizeButtonItem];

}

-(void) willFilterByFilterType:(NSInteger)selctedRow {
    _selectedFilterType=selctedRow;
    [self filterByFilterType:_selectedFilterType];
    [self drawGoodImageForOrganizeButtonItem];
}

#pragma mark - Reload UI timer
-(void) requestRefreshUI {
    [_refreshUITimer invalidate];
    _refreshUITimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(reloadUI:) userInfo:nil repeats:NO];
}

-(void) reloadUI:(NSTimer *) timer {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self reloadUI:timer];
        });
        return;
    }
    
    [_allObjects reloadData];
    if([self isViewLoaded]){
        [self.tableView reloadData];
    }
}
@end
