/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIGroupAddMembersViewController.h"
#import "UITools.h"
#import "UIGenericListOfObjectWithTokenViewController.h"
#import "UIRainbowGenericTableViewCell.h"
#import "Contact+UIRainbowGenericTableViewCellProtocol.h"
#import "GoogleAnalytics.h"

@interface UIGroupAddMembersViewController ()

@end

@implementation UIGroupAddMembersViewController

-(void) awakeFromNib {
    [super awakeFromNib];
    [self.objects addObjectsFromArray:self.servicesManager.contactsManagerService.myNetworkContacts];
    [self sortUI];
}

-(void) setGroup:(Group *)group {
    _group = group;
    self.object = _group;
    [_group.users enumerateObjectsUsingBlock:^(Contact * member, NSUInteger idx, BOOL * stop) {
        if([self.objects containsObject:member]){
            [self.objects removeObject:member];
        }
    }];
    [self sortUI];
}

-(void) viewDidLoad {
    [super viewDidLoad];
    if(_group)
        self.title = NSLocalizedString(@"Add members", nil);
    else
        self.title = NSLocalizedString(@"Create a list", nil);
    
    [self.nameTextField setPlaceholder:NSLocalizedString(@"List name", nil)];
    [self.subNameTextField setPlaceholder:NSLocalizedString(@"Comment (Optional)", nil)];
}

- (void)viewDidAppear:(BOOL)animated {
    if (!self.tokenInputView.editing && _group) {
        [self.tokenInputView beginEditing];
    }
    if(!_group)
        [self.nameTextField becomeFirstResponder];
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    if(indexPath.section == 0){
        if(!self.isTyping)
            cell.cellObject = [self.objects objectAtIndex:indexPath.row];
        else
            cell.cellObject = [self.filteredObjects objectAtIndex:indexPath.row];
    } else {
        cell.cellObject = [self.searchedObjects objectAtIndex:indexPath.row];
        // get the avatar.
        [[ServicesManager sharedInstance].contactsManagerService populateAvatarForContact:(Contact*)cell.cellObject];
    }
    cell.cellButtonTapHandler = nil;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    Contact *selectedContact = nil;
    if(indexPath.section == 0){
        if(!self.isTyping)
            selectedContact = [self.objects objectAtIndex:indexPath.row];
        else
            selectedContact = [self.filteredObjects objectAtIndex:indexPath.row];
    } else {
        selectedContact = [self.searchedObjects objectAtIndex:indexPath.row];
    }
    
    CLToken *token = [[CLToken alloc] initWithDisplayText:selectedContact.displayName context:selectedContact];
    [self.tokenInputView addToken:token];
    
    if(indexPath.section == 0){
        if(self.isTyping)
            [self.filteredObjects removeObjectAtIndex:indexPath.row];
        
        [self.objects removeObject:selectedContact];
    } else {
        [self.searchedObjects removeObject:selectedContact];
    }
    
    [tableView reloadData];
}


- (IBAction)doneButtonClicked:(UIBarButtonItem *)sender {
    [self.tokenInputView endEditing];
    if(!_group){
        [self.nameTextField resignFirstResponder];
        [self.subNameTextField resignFirstResponder];
    }
    
    if(!_group){
        [self createGroupWithName:self.nameTextField.text comment:self.subNameTextField.text members:self.tokenInputView.allTokens];
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"group" action:@"create" label:@"" value:nil];
    } else {
        [self addMembers:self.tokenInputView.allTokens toGroup:_group];
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"group" action:@"add member" label:@"" value:nil];
    }
}

-(void) createGroupWithName:(NSString *) name comment:(NSString *) comment members:(NSArray *) members {
    MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString(@"Creating new group ...", nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    [hud showAnimated:YES whileExecutingBlock:^{
        _group = [[ServicesManager sharedInstance].groupsService createGroupWithName:name andComment:comment];
    } completionBlock:^{
        if(!_group){
            [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error creating list", nil) message:NSLocalizedString(@"Failed to create list",nil) inViewController:self];
        } else {
            [self showHUDWithMessage:NSLocalizedString(@"List created",nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:^{
                [self addMembers:members toGroup:_group];
            }];
        }
    }];
}

-(void) addMembers:(NSArray *) members toGroup:(Group *) group{
    MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString(@"Adding members ...",nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    
    [hud showAnimated:YES whileExecutingBlock:^{
        [members enumerateObjectsUsingBlock:^(CLToken * aToken, NSUInteger idx, BOOL * stop) {
            [[ServicesManager sharedInstance].groupsService addContact:(Contact*)aToken.context inGroup:group];
        }];
    } completionBlock:^{
        [self showHUDWithMessage:NSLocalizedString(@"Done",nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:^{
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }];
    }];
}

-(NSPredicate *) predicateForText:(NSString *) text {
    return [NSPredicate predicateWithFormat:@"fullName contains[cd] %@", text];
}

-(void) sortUI {
    if(self.servicesManager.contactsManagerService.sortByFirstName){
        [self.objects sortUsingDescriptors:@[[UITools sortDescriptorForContactByFirstName], [UITools sortDescriptorForContactByLastName]]];
    } else {
        [self.objects sortUsingDescriptors:@[[UITools sortDescriptorForContactByLastName], [UITools sortDescriptorForContactByFirstName]]];
    }
}

-(BOOL) validateUIContent {
    BOOL isValid = [super validateUIContent];
    if(self.tokenInputView.allTokens.count > [ServicesManager sharedInstance].myUser.maxNumberOfParticipantPerRoom)
        isValid = NO;
    
    if(!_group && (self.nameTextField.text.length < 3 || ([[self.nameTextField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] length] == 0)))
        return NO;
    
    return isValid;
}

#pragma mark - TextField
-(void) textFieldDidChange:(ACFloatingTextField *) textField {
    if(textField == self.nameTextField){
        if(!_group){
            if((textField.text.length < 3 && textField.text.length > 0) || ([[textField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] length] == 0)){
                textField.textColor = [UIColor redColor];
                textField.tintColor = [UIColor redColor];
            } else {
                textField.textColor = [UITools defaultTintColor];
                textField.tintColor = [UITools defaultTintColor];
            }
        }
    }
    
    [super textFieldDidChange:textField];
}

@end
