/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIGroupAvatarView.h"
#import <Rainbow/ServicesManager.h>
#import "UITools.h"


@interface UIAvatarView (internal)
-(void) drawPhotoFromOrderedList:(NSOrderedSet*) orderedList;
@end

@implementation UIGroupAvatarView

-(void) setGroup:(Group *)group {
    _group = nil;
    _group = group;
    [UIAvatarView setNoAvatarImageName:@"Groups"];
    [self setNeedsLayout];
}

-(void) layoutSubviews {
    [super layoutSubviews];
    
    if(_group){
        [self setPhotoForGroup:_group];
    }
}

-(void) setPhotoForGroup:(Group *) group {
    NSMutableOrderedSet<Contact*> *set = [NSMutableOrderedSet orderedSetWithArray:group.users];
    
    if([ServicesManager sharedInstance].contactsManagerService.sortByFirstName){
        [set sortUsingDescriptors:@[[UITools sortDescriptorForContactByFirstName], [UITools sortDescriptorForContactByLastName]]];
    } else {
        [set sortUsingDescriptors:@[[UITools sortDescriptorForContactByLastName], [UITools sortDescriptorForContactByFirstName]]];
    }
    
    [self drawPhotoFromOrderedList:set];
}
@end
