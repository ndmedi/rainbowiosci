/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIGroupDetailsViewController.h"
#import "UIRoomDetailsViewController+Internal.h"
#import <MarqueeLabel/MarqueeLabel.h>
#import "UIGroupAvatarView.h"
#import <Rainbow/GroupsService.h>
#import "UIGroupDetailsAddMembersTableViewCell.h"
#import "UIGroupAddMembersViewController.h"
#import "GoogleAnalytics.h"
#import "UIRainbowGenericTableViewCell.h"
#import "Contact+UIRainbowGenericTableViewCellProtocol.h"
#import "BGTableViewRowActionWithImage.h"

@interface UIGroupDetailsViewController ()
@property (weak, nonatomic) IBOutlet MarqueeLabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet MarqueeLabel *groupCommentLabel;
@property (nonatomic, strong) OrderedOptionalSectionedContent<Contact *> *groupMembers;
@property (nonatomic, strong) UIAlertAction *saveAction;

@end
@implementation UIGroupDetailsViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"List details", nil);
    
    [self drawGroupInfos];
    
    [UITools applyCustomBoldFontTo:_groupNameLabel];
    [UITools applyCustomFontTo:_groupCommentLabel];
    
    if(_group){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateGroup:) name:kGroupsServiceDidUpdateGroup object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveGroup:) name:kGroupsServiceDidRemoveGroup object:nil];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", nil) style:UIBarButtonItemStylePlain target:self action:@selector(editButtonClicked:)];
    }
}

-(void) drawGroupInfos {
    if(_group){
        ((UIGroupAvatarView*)self.avatarView).group = _group;
        self.avatarView.asCircle = YES;
        self.avatarView.showPresence = NO;
        ((UIGroupAvatarView*)self.backgroundAvatar).group = _group;
        self.backgroundAvatar.asCircle = YES;
        self.backgroundAvatar.showPresence = NO;
        _groupNameLabel.text = _group.name;
        _groupCommentLabel.text = _group.comment;
    }
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupsServiceDidUpdateGroup object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupsServiceDidRemoveGroup object:nil];
    
    _group = nil;
}

#pragma mark - Group notifications
-(void) didUpdateGroup:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateGroup:notification];
        });
        return;
    }
    
    Group *group = (Group *)notification.object;
    [self setGroup:group];
    if([self isViewLoaded]){
        [self drawGroupInfos];
        [self.tableView reloadData];
    }
}

-(void) didRemoveGroup:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveGroup:notification];
        });
        return;
    }
    
    Group *group = (Group *)notification.object;
    if([group isEqual:_group]){
        // We are displaying a group that as been removed so leaving this view.
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}


-(void) setGroup:(Group *)group {
    _group = nil;
    _group = group;
    
    [self.participants removeAllObjects];
    self.participants = nil;
    
     _groupMembers = [[OrderedOptionalSectionedContent alloc] initWithSections:@[kAdministratorsSection, kAttendeesSection]];
    
    // UGLY : By chance, when sorted, this fake participant remains on top.
    Contact *fakeAddParticipant = [Contact new];
    [_groupMembers addObject:fakeAddParticipant toSection:kAttendeesSection];
    for (Contact *member in _group.users) {
        [_groupMembers addObject:member toSection:kAttendeesSection];
    }

    // Now we can sort them
    if([ServicesManager sharedInstance].contactsManagerService.sortByFirstName) {
        [[_groupMembers sectionForKey:kAdministratorsSection] sortUsingDescriptors:@[[UITools sortDescriptorForContactByFirstName], [UITools sortDescriptorForContactByLastName]]];
        [[_groupMembers sectionForKey:kAttendeesSection] sortUsingDescriptors:@[[UITools sortDescriptorForContactByFirstName], [UITools sortDescriptorForContactByLastName]]];
    } else {
        [[_groupMembers sectionForKey:kAdministratorsSection] sortUsingDescriptors:@[[UITools sortDescriptorForContactByLastName], [UITools sortDescriptorForContactByFirstName]]];
        [[_groupMembers sectionForKey:kAttendeesSection] sortUsingDescriptors:@[[UITools sortDescriptorForContactByLastName], [UITools sortDescriptorForContactByFirstName]]];
    }
}

#pragma mark - UITableView delegate

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_groupMembers allNotEmptySections] count];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_groupMembers allNotEmptySections] objectAtIndex:section];
    return [[_groupMembers sectionForKey:key] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0 && indexPath.row == 0) {
        // "Add member" line button
        UIGroupDetailsAddMembersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addParticipantCellID" forIndexPath:indexPath];
        return cell;
    } else {
        return [self cellForRowAtIndexPath:indexPath];
    }
}

-(UIRainbowGenericTableViewCell *) cellForRowAtIndexPath:(NSIndexPath *) indexPath {
    
    NSString *key = [[_groupMembers allNotEmptySections] objectAtIndex:indexPath.section];
    NSArray<Contact *> *content = [_groupMembers sectionForKey:key];
    
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    
    cell.cellObject = [content objectAtIndex:indexPath.row];
    __weak typeof(cell) weakCell = cell;
    cell.cellButtonTapHandler = ^(UIButton *sender) {
        [self showContactDetails:(Contact *)weakCell.cellObject];
    };
    
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Give a specific height to the "Add Participant" line button
    if(indexPath.section == 0  && indexPath.row == 0)
        return 44;
    return kRainbowGenericTableViewCellHeight;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 0 && indexPath.row == 0) {
        // Open add participant view
    } else {
        UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        Contact *contact = (Contact*)cell.cellObject;
        if(contact.isRainbowUser)
            [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0 && indexPath.section == 0)
        return NO;
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    Contact *selectedContact = (Contact*)cell.cellObject;
    
    BGTableViewRowActionWithImage *action = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Remove", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"RemoveAttendees"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [[ServicesManager sharedInstance].groupsService removeContact:selectedContact fromGroup:_group];
        });
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"group" action:@"remove member" label:nil value:nil];
    }];
    return @[action];
}

#pragma mark - DZNEmptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"No members",nil);
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools boldFontName] size:18.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - Segues
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"addMembersSegueID"]){
        UINavigationController *destNavController = [segue destinationViewController];
        UIGroupAddMembersViewController *destViewController = [destNavController.viewControllers firstObject];
        destViewController.group = _group;
    }
}

- (void) editButtonClicked:(UIBarButtonItem *)sender {
    UIAlertController *editActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* changeNameAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Change list name", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        UIAlertController *newNameAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Enter new list name", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        [newNameAlert addTextFieldWithConfigurationHandler:^(UITextField * textField) {
            [UITools applyCustomFontToTextField:textField];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:textField];
        }];
        _saveAction = nil;
        _saveAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSString *newName = [newNameAlert.textFields firstObject].text;
            [[ServicesManager sharedInstance].groupsService updateGroup:_group withNewGroupName:newName];
        }];
        _saveAction.enabled = NO;
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
            _saveAction = nil;
        }];
        
        [newNameAlert addAction:_saveAction];
        [newNameAlert addAction:cancel];
        [newNameAlert.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:newNameAlert animated:YES completion:nil];
    }];
    
    [editActionSheet addAction:changeNameAction];
    
    
    UIAlertAction* changeTopicAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Change list comment", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        UIAlertController *newCommentAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Enter new list comment", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        [newCommentAlert addTextFieldWithConfigurationHandler:^(UITextField * textField) {
            [UITools applyCustomFontToTextField:textField];
        }];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSString *newComment = [newCommentAlert.textFields firstObject].text;
            NSLog(@"New group comment %@", newComment);
            [[ServicesManager sharedInstance].groupsService updateGroup:_group withNewComment:newComment];
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        
        [newCommentAlert addAction:ok];
        [newCommentAlert addAction:cancel];
        [newCommentAlert.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:newCommentAlert animated:YES completion:nil];
    }];
    
    [editActionSheet addAction:changeTopicAction];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [editActionSheet addAction:cancel];
    
    // show the menu.
    [editActionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:editActionSheet animated:YES completion:nil];
}

-(void) textFieldDidChange:(NSNotification *) notification {
    UITextField *textField = (UITextField *)notification.object;
    _saveAction.enabled = (textField.text.length >= 3) && ([[textField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] length] != 0);
}

#pragma mark - 3D Touch actions
- (NSArray<id<UIPreviewActionItem>> *)previewActionItems {
    UIPreviewAction *removeAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Remove", nil) style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[ServicesManager sharedInstance].groupsService deleteGroup:_group];
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"group" action:@"delete" label:nil value:nil];
    }];
    
    return @[removeAction];
}

@end
