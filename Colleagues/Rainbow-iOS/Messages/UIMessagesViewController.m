/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIMessagesViewController.h"
#import <Rainbow/Room.h>
#import <Rainbow/Message.h>
#import <Rainbow/ConversationsManagerService.h>
#import "UITools.h"
#import "UIContactDetailsViewController.h"
#import "UIRoomDetailsViewController.h"
#import <Rainbow/MessagesBrowser.h>
#import <Rainbow/Message+Browsing.h>
#import <Rainbow/Tools.h>
#import <Rainbow/ServicesManager.h>
#import <Rainbow/NSDate+Utilities.h>
#import "Contact+Extensions.h"
#import "UIViewController+Visible.h"
#import "JSQMessagesCollectionViewCellOutgoing.h"
#import "GroupedMessages.h"
#import <Rainbow/NSDate+Distance.h>
#import "Message-JSQMessageData.h"
#import "SORelativeDateTransformer.h"
#import <MarqueeLabel/MarqueeLabel.h>
#import "UIRoomsTableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "JSQMessagesTypingIndicatorFooterViewWithAvatar.h"
#import "JSQRainbowOutgoingMessagesCollectionViewCell.h"
#import "JSQRainbowIncomingMessagesCollectionViewCell.h"
#import "JSQRainbowRoomIncomingMessagesCollectionViewCell.h"
#import "JSQMessagesViewController.h"
#import "JSQMessagesAutoLoadEarlierHeaderView.h"
#import "JSQGroupChatEventCell.h"
#import "CustomJSQMessagesBubblesSizeCalculator.h"
#import "MBProgressHUD.h"
#import "Conversation+RainbowID.h"
#import "JSQMessagesComposerTextView+Paste.h"
#import "CustomNavigationController.h"
#import "JSQWebRTCEventsCell.h"
#import <Photos/Photos.h>
#import "SCLAlertView.h"
#import "SCLAlertView+Rotation.h"
#import "UIDevice+JSQMessages.h"
#import "File+DefaultImage.h"
#import "UIPreviewQuickLookController.h"
#import "JSQMessagesInputToolbar+HitTest.h"
#import "UIMyFilesOnServerTableViewController.h"
#import <Rainbow/NSData+MimeType.h>
#import <Rainbow/NSDictionary+JSONString.h>
#import "GoogleAnalytics.h"
#import "Message+BodyForGroupChatEvent.h"
#import "UIPagingMessagesViewController.h"
#import <Rainbow/MessagesAlwaysInSyncBrowserAddOn.h>
#import "UIStoryboardManager.h"
#import "UIGenericBubbleDetailsViewController.h"
#import "Room+Extensions.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "JoinMeetingViewController.h"
#import "UIStoryboardManager.h"
#import "CustomSearchController.h"
#import "ShareFileViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIGuestModeViewController.h"
#import "Firebase.h"



typedef void (^progressBlock) (JSQRainbowMessagesCollectionViewCell *cell, double totalBytesSent, double totalBytesExpectedToSend);
typedef void (^downloadBlock) (File *aFile);

#define kPageSize 20
#define kMaxTextLength 1024

@interface UIMessagesViewController () <CustomSearchControllerDelegate,JSQMessagesComposerTextViewPasteDelegate, CKItemsBrowserDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, JSQMessagesCollectionViewCellDelegate, QLPreviewControllerDataSource, QLPreviewControllerDelegate, UIDocumentPickerDelegate, UIPopoverPresentationControllerDelegate, JoinMeetingViewControllerDelegate, UITabBarControllerDelegate>{
    BOOL isNetworkConnected;
    BOOL isRemoveRoom;
    CGFloat topPadding;
    CGFloat bottomPadding;
    
   
}

- (void)jsq_adjustInputToolbarForComposerTextViewContentSizeChange:(CGFloat)dy;
- (void)jsq_adjustInputToolbarHeightConstraintByDelta:(CGFloat)dy;
- (void)jsq_updateCollectionViewInsets;
- (void)jsq_setCollectionViewInsetsTopValue:(CGFloat)top bottomValue:(CGFloat)bottom;
- (void)collectionView:(JSQMessagesCollectionView *)collectionView accessibilityForCell:(JSQMessagesCollectionViewCell*)cell indexPath:(NSIndexPath *)indexPath message:(id<JSQMessageData>)messageItem;

@property (nonatomic, strong) ContactsManagerService *contactsManagerService;
@property (nonatomic, strong) ConversationsManagerService *conversationsManagerService;
@property (nonatomic, strong) FileSharingService *fileManagerService;

@property (nonatomic, strong) JSQMessagesBubbleImageFactory *bubbleFactory;
@property (nonatomic, strong) JSQMessagesBubbleImage *outgoingBubbleImageData;
// the incoming bubble images, for each senderId (i.e. jid)
@property (nonatomic, strong) NSMutableDictionary<NSString *, JSQMessagesBubbleImage *> *incomingBubbleImageDataDict;
@property (nonatomic, strong) UIImage *groupChatEventBubbleImage;

@property (nonatomic, strong) JSQMessagesAvatarImage *meAvatar;
@property (nonatomic, strong) NSMutableDictionary<NSString *, JSQMessagesAvatarImage *> *contactAvatarDict;

@property (nonatomic, strong) MessagesBrowser *messagesBrowser;
@property (nonatomic) BOOL shouldAnimateAddedItems;
@property (nonatomic) BOOL isRetrievingNextPage;
@property (nonatomic) BOOL needsAvatar;
@property (nonatomic, strong) NSMutableArray <GroupedMessages*> *messages;
@property (nonatomic, strong) NSMutableArray  *filesDownloading;
@property (nonatomic, strong) NSIndexPath *tappedIndexPath;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *vcardDetailsButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *moreMenuButton;
@property (nonatomic, strong) UIAlertController *moreMenuActionSheet;
@property (nonatomic, strong) NSTimer *updateBannerRefreshTimer;
@property (nonatomic, strong) IBOutlet UIView *notificationDisabledView;
@property (nonatomic, weak) IBOutlet UILabel *notificationDisabledLabel;
@property (nonatomic, weak) IBOutlet UIButton *notificationDisabledButton;
@property (nonatomic, strong) NSMutableArray<Contact *> *typingContacts;
@property (nonatomic) BOOL allowAutomaticallyScrollsToMostRecentMessage;
@property (nonatomic, weak) IBOutlet UIButton *scrollToBottomButton;
@property (nonatomic, strong) UIAlertController *leftAccessoryButtonActionSheet;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *callWebRTCButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *joinConferenceButton;

@property (strong, nonatomic) IBOutlet UIView *attachmentView;
@property (weak, nonatomic) IBOutlet UIImageView *attachmentImageView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *attachmentImageHeight;
@property (weak, nonatomic) IBOutlet UILabel *attachmentFileNameLabel;
@property (strong, nonatomic) UIImageView *iconImageView;

@property (nonatomic, strong) File *attachmentFileToSend;
@property (nonatomic, strong) File *selectedMedia;
@property (nonatomic, strong) Message *selectedMessage;
@property (nonatomic, strong) UIImage *attachmentPreview;

@property (nonatomic, strong) UIAlertController *conferenceCallBackAlert;
@property (nonatomic, strong) MessagesAlwaysInSyncBrowserAddOn *alwaysInSyncAddon;
@property (nonatomic) BOOL stopShowingHeaderView;
@property (nonatomic) BOOL startShowingSharingView;
@property (nonatomic, strong) UIColor *previousNavBarTintColor;
@property (nonatomic, strong) NSDictionary *previousTitleTextAttributesColor;

@property (strong, nonatomic) IBOutlet UIView *actionsView;
@property (nonatomic, strong) UIView *actionsViewBackgroundView;
@property (weak, nonatomic) IBOutlet UITabBar *actionViewTabBar;
@property (weak, nonatomic) IBOutlet UITabBarItem *forwardTabBarItem;
@property (weak, nonatomic) IBOutlet UITabBarItem *shareTabBarItem;
@property (weak, nonatomic) IBOutlet UITabBarItem *saveTabBarItem;
@property (strong, retain) IBOutlet UITabBarItem *deleteTabBarItem;

@end

@implementation UIMessagesViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        isNetworkConnected = YES;
        _needsAvatar = NO;
        isRemoveRoom = NO;
        _contactsManagerService = [ServicesManager sharedInstance].contactsManagerService;
        _conversationsManagerService = [ServicesManager sharedInstance].conversationsManagerService;
        _fileManagerService=[ServicesManager sharedInstance].fileSharingService;
        _messages = [NSMutableArray array];
        _bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRainbowImage] capInsets:UIEdgeInsetsZero];
        _outgoingBubbleImageData = [_bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        _incomingBubbleImageDataDict = [NSMutableDictionary new];
        _contactAvatarDict = [NSMutableDictionary new];
        _tappedIndexPath = nil;
        _typingContacts = [NSMutableArray new];
        _filesDownloading = [NSMutableArray array];
        _conferenceCallBackAlert = nil;

        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveComposingMessage:) name:kConversationsManagerDidReceiveComposingMessage object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UITextViewDidChange:) name:UITextViewTextDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wantToSendFile:) name:@"wantToSendFile" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddCall:) name:kTelephonyServiceDidAddCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateCall:) name:kTelephonyServiceDidUpdateCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveCall:) name:kTelephonyServiceDidRemoveCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConference:) name:kConferencesManagerDidUpdateConference object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveConferenceReminder:) name:kConversationsManagerDidReceiveConferenceReminderInConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doJoinConference:) name:@"joinConference" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateProgress:) name:kFileSharingServiceDidUpdateUploadedBytesSent object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateDownloadedProgress:) name:kFileSharingServiceDidUpdateDownloadedBytes object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateAttachment:) name:kFileSharingServiceDidUpdateFile object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRoom:) name:kRoomsServiceDidRemoveRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailRemoveRoom:) name:kRoomsServiceDidFailRemoveRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveFile:) name:kFileSharingServiceDidRemoveFile object:nil];

        
    }
    return self;
}

-(void) dealloc {
    _conferenceCallBackAlert = nil;
    [_messagesBrowser reset];
    [_messages removeAllObjects];
    _messages = nil;
    _alwaysInSyncAddon.browser = nil;
    _alwaysInSyncAddon = nil;
    _messagesBrowser.delegate = nil;
    _messagesBrowser = nil;
    _outgoingBubbleImageData = nil;
    [_incomingBubbleImageDataDict removeAllObjects];
    _incomingBubbleImageDataDict = nil;
    _tappedIndexPath = nil;
    [_contactAvatarDict removeAllObjects];
    _contactAvatarDict = nil;
    [_typingContacts removeAllObjects];
    _typingContacts = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidReceiveComposingMessage object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"wantToSendFile" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConferencesManagerDidUpdateConference object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidReceiveConferenceReminderInConversation object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidAddCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidUpdateCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidRemoveCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidUpdateUploadedBytesSent object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidUpdateDownloadedBytes object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"joinConference" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidUpdateFile object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRemoveRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidRemoveFile object:nil];
   
    [_conversation removeObserver:self forKeyPath:kConversationIsMuted];
    [_conversation removeObserver:self forKeyPath:kConversationUnreadMessagesCount];
    
    if(_updateBannerRefreshTimer){
        [_updateBannerRefreshTimer invalidate];
        _updateBannerRefreshTimer = nil;
    }
    _bubbleFactory = nil;
    [_notificationDisabledView removeFromSuperview];
    _notificationDisabledView = nil;
    
    _attachmentFileToSend = nil;
    _attachmentPreview = nil;
    _conversationsManagerService = nil;
    _contactsManagerService = nil;
    _fileManagerService=nil;
}

-(void) setAvatarForContact:(Contact *) contact {
    UIImage *contactImage = nil;
    if(contact.photoData != nil)
        contactImage = [UIImage imageWithData:contact.photoData];
    
    JSQMessagesAvatarImage *avatar = nil;
    if(contactImage)
        avatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:contactImage diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    else
        avatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:contact.initials backgroundColor:[UITools colorForString:contact.displayName] textColor:[UIColor whiteColor] font:[UIFont fontWithName:[UITools defaultFontName] size:12] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    
    [_contactAvatarDict setObject:avatar forKey:contact.jid];
    [self reloadCollectionView];
}

-(void) setAvatarAndBubbleForContact:(Contact *) contact {
    [_incomingBubbleImageDataDict setObject:[_bubbleFactory incomingMessagesBubbleImageWithColor:[UITools colorForString:contact.displayName]] forKey:contact.jid];
    [self setAvatarForContact:contact];
}

-(void) setConversation:(Conversation *)conversation {
    [_conversation removeObserver:self forKeyPath:kConversationIsMuted];
    [_conversation removeObserver:self forKeyPath:kConversationUnreadMessagesCount];
    
    // flush the existing registred bubbles and avatars
    [_incomingBubbleImageDataDict removeAllObjects];
    [_contactAvatarDict removeAllObjects];
    _alwaysInSyncAddon.browser = nil;
    _alwaysInSyncAddon = nil;
    [_messagesBrowser reset];
    _messagesBrowser.delegate = nil;
    _messagesBrowser = nil;
    
    _conversation = nil;
    _conversation = conversation;
    
    // now, do additional stuff depending on the conversation type.
    if (_conversation.type == ConversationTypeUser || _conversation.type == ConversationTypeBot) {
        // register its incoming bubble, with a default color.
        [_incomingBubbleImageDataDict setObject:[_bubbleFactory incomingMessagesBubbleImageWithColor:[UITools defaultTintColor]] forKey:_conversation.peer.jid];
        [self setAvatarAndBubbleForContact:(Contact*)_conversation.peer];
    } else if (_conversation.type == ConversationTypeRoom) {
        //[self toggleDisableWhenMuted];
        // register a different incoming bubble color for each participant.
        Room *room = (Room*) _conversation.peer;
        for (Participant *participant in room.participants) {
            if(participant.contact.jid)
                [self setAvatarAndBubbleForContact:participant.contact];
        }
    }
    
    [_conversation addObserver:self forKeyPath:kConversationIsMuted options:NSKeyValueObservingOptionNew context:nil];
    [_conversation addObserver:self forKeyPath:kConversationUnreadMessagesCount options:NSKeyValueObservingOptionNew context:nil];
    
    _messagesBrowser = [_conversationsManagerService messagesBrowserForConversation:_conversation withPageSize:kPageSize preloadMessages:YES];
    _messagesBrowser.delegate = self;
    
    _alwaysInSyncAddon = [[MessagesAlwaysInSyncBrowserAddOn alloc] init];
    _alwaysInSyncAddon.browser = _messagesBrowser;
    
    _allowAutomaticallyScrollsToMostRecentMessage = YES;
    [self reloadCollectionView];
    
    // The room name could have been changed in bubble's detail view
    if (_conversation.type == ConversationTypeRoom) {
        Room *room = (Room *) _conversation.peer;
        if(self.navigationController.title != room.displayName){
            [self setConversationTitle:room.displayName];
        }
        
        [[ServicesManager sharedInstance].roomsService fetchRoomDetails:room];
    } else {
        self.navigationController.navigationBar.barTintColor = [UITools colorForString:_conversation.peer.displayName];
        Contact *contact = (Contact*)_conversation.peer;
        
        // Fetch potentially missing informations about the contact
        [[ServicesManager sharedInstance].contactsManagerService fetchRemoteContactDetail:contact];

        // Request calendar Automatic reply
        if( contact.isPresenceSubscribed ) {
            [[ServicesManager sharedInstance].contactsManagerService fetchCalendarAutomaticReply:contact];
        }
    }
}
 
-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    // Update the status banner on presence or  last-activity date update
    if([keyPath isEqualToString:kConversationIsMuted]) {
        if(![NSThread isMainThread]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateBanner];
                [self showHideCallButton];
            });
            return;
        } else {
            [self updateBanner];
            [self showHideCallButton];
        }
    } else if ([keyPath isEqualToString:kConversationUnreadMessagesCount]){
        if(![NSThread isMainThread]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showScrollToBottomButton:!_allowAutomaticallyScrollsToMostRecentMessage];
            });
        } else {
            [self showScrollToBottomButton:!_allowAutomaticallyScrollsToMostRecentMessage];
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

-(void) didUpdateContact:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateContact:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Contact *contact = [userInfo objectForKey:kContactKey];
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
    
    // Dont update if not needed (change in photo or displayName)
    if (![changedKeys containsObject:@"photoData"] &&
        ![changedKeys containsObject:@"displayName"] &&
        ![changedKeys containsObject:@"calendarPresence"] &&
        ![changedKeys containsObject:@"sentInvitation"] &&
        ![changedKeys containsObject:@"presence"] &&
        ![changedKeys containsObject:@"firstName"] &&
        ![changedKeys containsObject:@"lastName"]&&
        ![changedKeys containsObject:@"isConnectedWithMobile"]
        
    ) {
        return;
    }
    
    if (_conversation.type == ConversationTypeUser || _conversation.type == ConversationTypeBot) {
        if([contact isEqual:_conversation.peer]) {
            if([self isViewLoaded]){
                if ([changedKeys containsObject:@"presence"] || [changedKeys containsObject:@"lastActivityDate"]|| [changedKeys containsObject:@"calendarPresence"] || [changedKeys containsObject:@"sentInvitation"] || [changedKeys containsObject:@"isConnectedWithMobile"] || [changedKeys containsObject:@"isInRoster"] || [changedKeys containsObject:@"isPresenceSubscribed"]) {
                    [self updateBanner];
                    [self showHideCallButton];
                }
                [self setAvatarForContact:contact];
                [self setTitleView:contact.displayName];
            }
        }
    }
    
    if (_conversation.type == ConversationTypeRoom) {
        Room *room = (Room*) _conversation.peer;
        for (Participant *participant in room.participants) {
            if ([contact isEqual:participant.contact]) {
                [self setAvatarAndBubbleForContact:contact];
                return;
            }
        }
    }
}

-(void) didUpdateRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateRoom:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Room *room = [userInfo objectForKey:kRoomKey];
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];

    if([_conversation.peer isEqual:room]){
        if([changedKeys containsObject:@"users"]){
            [self toggleDisableWhenOutOfTheRoom];
            for (Participant *participant in room.participants) {
                if(participant.contact.jid)
                [self setAvatarAndBubbleForContact:participant.contact];
            }
        }
        if([changedKeys containsObject:@"name"]){
            self.title = _conversation.peer.displayName;
            [self parentViewController].title = _conversation.peer.displayName;
            [self setConversationTitle:_conversation.peer.displayName];
        }
        if([changedKeys containsObject:@"creator"] || [changedKeys containsObject:@"confEndpoints"]){
            [self showHideCallButton];
        }
    }
}

-(void) didUpdateConference:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateConference:notification];
        });
        return;
    }
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Conference *conference = [userInfo objectForKey:kConferenceKey];
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kConferenceChangedAttributesKey];
    
    if([_conversation.peer isKindOfClass:[Room class]]){
        if([((Room *)_conversation.peer).conference isEqual:conference]){
            if([changedKeys containsObject:@"canJoin"] || [changedKeys containsObject:@"isActive"])
                [self showHideCallButton];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Set the title of the view and after replace it by a button like that the navigation bar calculate the correct size of the back button, the title is always centered correctly.
    [self setTitleView:_conversation.peer.displayName];
    self.senderId = [ServicesManager sharedInstance].myUser.contact.jid;
    self.senderDisplayName = [ServicesManager sharedInstance].myUser.contact.firstName ? [ServicesManager sharedInstance].myUser.contact.firstName : [ServicesManager sharedInstance].myUser.contact.jid;
    
    UIImage *meImage = nil;
    if([ServicesManager sharedInstance].myUser.contact.photoData != nil)
        meImage = [UIImage imageWithData:[ServicesManager sharedInstance].myUser.contact.photoData];
    else
        meImage = [UIImage imageNamed:@"ContactTabBar"];
    
    _meAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:meImage diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    
    // input toolbar
    self.inputToolbar.contentView.textView.jsqPasteDelegate = self;
    
    self.inputToolbar.contentView.textView.font = [UIFont fontWithName:[UITools defaultFontName] size:self.inputToolbar.contentView.textView.font.pointSize];
    [UITools applyCustomBoldFontTo:self.inputToolbar.contentView.rightBarButtonItem.titleLabel];
    self.inputToolbar.maximumHeight = 150;
    
    // input toolbar let button : upload images, files,...
    [self.inputToolbar.contentView.leftBarButtonItem setTitle:@"" forState:UIControlStateDisabled];
    [self.inputToolbar.contentView.leftBarButtonItem setTitle:@"" forState:UIControlStateNormal];
    [self.inputToolbar.contentView.leftBarButtonItem setImage:[UIImage imageNamed:@"add_file"] forState:UIControlStateNormal];
    [self.inputToolbar.contentView.leftBarButtonItem setImage:[UIImage imageNamed:@"add_file"] forState:UIControlStateHighlighted];
    self.inputToolbar.contentView.leftBarButtonItem.contentMode = UIViewContentModeScaleAspectFit;
    [self.inputToolbar.contentView.leftBarButtonItem.imageView setTintColor:[UITools defaultTintColor]];
    self.inputToolbar.contentView.leftBarButtonItemWidth = 30;
    self.inputToolbar.contentView.leftContentPadding = 10;
    
    // input toolbar right button : send
    [self.inputToolbar.contentView.rightBarButtonItem setTitle:@"" forState:UIControlStateDisabled];
    [self.inputToolbar.contentView.rightBarButtonItem setTitle:@"" forState:UIControlStateNormal];
    [self.inputToolbar.contentView.rightBarButtonItem setImage:[UIImage imageNamed:@"Send"] forState:UIControlStateNormal];
    [self.inputToolbar.contentView.rightBarButtonItem setImage:[UIImage imageNamed:@"Send_disabled"] forState:UIControlStateDisabled];
    [self.inputToolbar.contentView.rightBarButtonItem.imageView setTintColor:[UITools defaultTintColor]];
    
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    self.collectionView.collectionViewLayout.messageBubbleFont = [UIFont fontWithName:[UITools defaultFontName] size:14];
    
    [self.collectionView registerNib:[JSQMessagesTypingIndicatorFooterViewWithAvatar nib] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:[JSQMessagesTypingIndicatorFooterViewWithAvatar footerReuseIdentifier]];
    
    // subclass JSQMessagesAutoLoadEarlierHeaderView to load automatically the old messages when scrolling
    [self.collectionView registerNib:[JSQMessagesAutoLoadEarlierHeaderView nib] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[JSQMessagesAutoLoadEarlierHeaderView headerReuseIdentifier]];
    
    if (_conversation.type == ConversationTypeRoom) {
        [self.collectionView registerNib:[JSQGroupChatEventCell nib] forCellWithReuseIdentifier:[JSQGroupChatEventCell cellReuseIdentifier]];
        [self.collectionView registerNib:[JSQRainbowRoomIncomingMessagesCollectionViewCell nib] forCellWithReuseIdentifier:[JSQRainbowRoomIncomingMessagesCollectionViewCell cellReuseIdentifier]];
        self.incomingCellIdentifier = [JSQRainbowRoomIncomingMessagesCollectionViewCell cellReuseIdentifier];
    } else {
        [self.collectionView registerNib:[JSQRainbowIncomingMessagesCollectionViewCell nib] forCellWithReuseIdentifier:[JSQRainbowIncomingMessagesCollectionViewCell cellReuseIdentifier]];
        self.incomingCellIdentifier = [JSQRainbowIncomingMessagesCollectionViewCell cellReuseIdentifier];
    }
    
    self.groupChatEventBubbleImage = [self eventBubbleCellImage];
    
    [self.collectionView registerNib:[JSQRainbowOutgoingMessagesCollectionViewCell nib] forCellWithReuseIdentifier:[JSQRainbowOutgoingMessagesCollectionViewCell cellReuseIdentifier]];
    self.outgoingCellIdentifier = [JSQRainbowOutgoingMessagesCollectionViewCell cellReuseIdentifier];
    
    [self.collectionView registerNib:[JSQWebRTCEventsCell nib] forCellWithReuseIdentifier:[JSQWebRTCEventsCell cellReuseIdentifier]];
    
    // preconfigure status label.
    self.statusLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [UITools applyCustomFontTo:self.statusLabel];
    self.statusLabel.textColor = [UIColor whiteColor];
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    self.statusLabel.backgroundColor = [UIColor darkGrayColor];
    self.statusLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.statusLabel.numberOfLines = 0;
    //
    self.iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AddContact"]];
    self.iconImageView.tintColor = [UIColor whiteColor];
    self.iconImageView.frame = CGRectMake(self.statusLabel.frame.size.width/2-(20/2), 4.0f, 20, 20);
    self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.iconImageView.clipsToBounds = YES;
    [self.statusLabel addSubview: self.iconImageView];
    
    [self toggleDisableWhenOutOfTheRoom];
    
    _notificationDisabledView.hidden = YES;
    
    // update the status banner
    [self updateBanner];
    
    CustomJSQMessagesBubblesSizeCalculator *customJSQMEssagesBubblesSizeCalculator = [CustomJSQMessagesBubblesSizeCalculator new];
    self.collectionView.collectionViewLayout.bubbleSizeCalculator = customJSQMEssagesBubblesSizeCalculator;
    customJSQMEssagesBubblesSizeCalculator.isMUC = (_conversation.type == ConversationTypeRoom) ? YES : NO;
    
    [self addScrollToBottomButton];
    [self addActionsView];
    
    _closeButton.tintColor = [UITools defaultTintColor];
    UIImage *close = [UIImage imageNamed:@"close_conversation"];
    [_closeButton setImage:[close imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    _closeButton.layer.cornerRadius = _closeButton.frame.size.height/2;
    _closeButton.backgroundColor = [UIColor clearColor];
    [_closeButton addTarget:self action:@selector(closeAttachementButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    _attachmentView.backgroundColor = [UITools defaultBackgroundColor];
   
    [self adjustAttachmentViewFrameWithHeight:200];
    [UITools applyCustomFontTo:_attachmentFileNameLabel];
    
    topPadding = 0;
    bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topPadding = window.safeAreaInsets.top;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
}

/**
 * change the navigation bar title
 */
-(void)setConversationTitle:(NSString *)text {
    UINavigationItem *navItem = [self goodNavigationItem];
    if([navItem.titleView isKindOfClass:[UIButton class]]){
        [(UIButton *)navItem.titleView setTitle:text forState:UIControlStateNormal];
    }
}
-(void)setTitleView:(NSString *)title {
    self.title = title;
    [self parentViewController].title = title;
    self.definesPresentationContext = YES;
    UIColor *whiteAlpha90 = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.9];
    UIButton *titleLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleLabelButton setTitle:title forState:UIControlStateNormal];
    [UITools applyCustomBoldFontTo:titleLabelButton.titleLabel];
    [titleLabelButton setTitleColor:whiteAlpha90 forState:UIControlStateNormal];
    [titleLabelButton addTarget:self action:@selector(didTapTitleButton:) forControlEvents:UIControlEventTouchUpInside];
    titleLabelButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    UINavigationItem *item = [self goodNavigationItem];
    item.titleView = titleLabelButton;
}

-(UINavigationItem *) goodNavigationItem {
    if([self.parentViewController isKindOfClass:[UIPagingMessagesViewController class]]){
        return self.parentViewController.navigationItem;
    } else {
        return self.navigationItem;
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self parentViewController].title = _conversation.peer.displayName;
    self.showLoadEarlierMessagesHeader = YES;
    self.navigationController.navigationBar.tintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[[UIColor whiteColor] colorWithAlphaComponent:0.9], NSFontAttributeName:[UIFont fontWithName:[UITools boldFontName] size:17.0]}];
    
    // Mark the conversation as active
    // uncomment this to enable XEP-085 chat-state
    [_conversationsManagerService setStatus:ConversationStatusActive forConversation:_conversation];
    
    // look if there is a last message entered for this conversation
    NSDictionary * dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastMessagesEntered"];
    if([dic objectForKey:_conversation.conversationId]){
        self.inputToolbar.contentView.textView.text = dic[_conversation.conversationId];
        [self.inputToolbar.contentView.textView.delegate textViewDidChange:self.inputToolbar.contentView.textView];
        [self performSelectorOnMainThread:@selector(adjustTextView) withObject:nil waitUntilDone:NO];
        
        NSMutableDictionary *latestMessagesEntereded = [NSMutableDictionary dictionaryWithDictionary:dic];
        [latestMessagesEntereded removeObjectForKey:_conversation.conversationId];
        [[NSUserDefaults standardUserDefaults] setObject:latestMessagesEntereded forKey:@"lastMessagesEntered"];
    }
    
    [self showHideCallButton];
    [self updateConversationStatusAndToggleSendButton];
    
   
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
    
    [[UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[UIViewController class]]] setTintColor:[UITools defaultTintColor]];
    
    [[UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[UINavigationController class]]] setTintColor:[UIColor whiteColor]];
    
    [[UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[UITabBarController class]]] setTintColor:[UIColor whiteColor]];
    
    
}

-(void) adjustTextView {
    UITextView *textView = self.inputToolbar.contentView.textView;
    CGSize oldSize = textView.frame.size;
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    [self jsq_adjustInputToolbarForComposerTextViewContentSizeChange:newFrame.size.height - oldSize.height];
    [self jsq_updateCollectionViewInsets];
    
    [self scrollToBottomAnimated:NO];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [FIRAnalytics setScreenName:@"chat_screen" screenClass:@"UIMessagesViewController"];
    _moreMenuButton.enabled = [self willShowSomethingInMoreMenu];
    if(![ServicesManager sharedInstance].loginManager.isConnected){
        [self didLostConnection:nil];
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [UITools customizeAppAppearanceWithGlobalColor:[UITools defaultTintColor]];
    self.navigationController.navigationBar.barTintColor = [UITools defaultTintColor];
    
    // Mark the conversation as inactive
    // uncomment this to enable XEP-085 chat-state
    [_conversationsManagerService setStatus:ConversationStatusInactive forConversation:_conversation];
    
    if(![ServicesManager sharedInstance].loginManager.isConnected){
        [self didReconnect:nil];
    }
    
    if(_updateBannerRefreshTimer){
        [_updateBannerRefreshTimer invalidate];
        _updateBannerRefreshTimer = nil;
    }
    
    if (_moreMenuActionSheet) {
        [_moreMenuActionSheet dismissViewControllerAnimated:NO completion:nil];
        _moreMenuActionSheet = nil;
    }
    
    if(self.inputToolbar.contentView.textView.text.length > 0){
        NSMutableDictionary *latestMessagesEntered = nil;
        NSDictionary * dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastMessagesEntered"];
        if(dic){
            latestMessagesEntered = [NSMutableDictionary dictionaryWithDictionary:dic];
            [latestMessagesEntered setObject:self.inputToolbar.contentView.textView.text forKey:_conversation.conversationId];
            [[NSUserDefaults standardUserDefaults] setObject:latestMessagesEntered forKey:@"lastMessagesEntered"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Login manager notifications
-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        // Move the UI down
        //self.topContentAdditionalInset += 60;
        //self.showLoadEarlierMessagesHeader = NO;
        self.inputToolbar.contentView.leftBarButtonItem.enabled = NO;
        isNetworkConnected = NO;
    }
}

-(void) didLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        // Move the UI up
        //self.topContentAdditionalInset -= 60;
        self.showLoadEarlierMessagesHeader = YES;
        self.inputToolbar.contentView.leftBarButtonItem.enabled = YES;
        isNetworkConnected = YES;
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        // Move the UI up
        //self.topContentAdditionalInset -= 60;
        self.showLoadEarlierMessagesHeader = YES;
        self.inputToolbar.contentView.leftBarButtonItem.enabled = YES;
        isNetworkConnected = YES;
    }
}

#pragma mark - Navigation
- (IBAction)showDetails:(UIBarButtonItem *)sender {
    [self parentViewController].title = @" ";
    if (_conversation.type == ConversationTypeUser || _conversation.type == ConversationTypeBot) {
        UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
        controller.contact = (Contact *) _conversation.peer;
        controller.fromView = self;
        [self.navigationController pushViewController:controller animated:YES];
    } else if (_conversation.type == ConversationTypeRoom) {
        UIRoomDetailsViewController *controller = [[UIStoryboardManager sharedInstance].roomDetailsStoryBoard instantiateViewControllerWithIdentifier:@"roomDetailsViewControllerID"];
        controller.room = (Room *) _conversation.peer;
        controller.fromView = self;
        if(_parentNavigationController){
            [_parentNavigationController pushViewController:controller animated:YES];
        } else {
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}


/**
 *  Build a "more" menu depending on the conversation type.
 */

-(BOOL) willShowSomethingInMoreMenu {
    if(_messages.count > 0)
        return YES;
    
    if (_conversation.type == ConversationTypeUser) {
        Contact *contact = (Contact *)_conversation.peer;
        if(contact.isPresenceSubscribed || contact.sentInvitation.status == InvitationStatusPending)
            return YES;
    }
    
    if(_conversation.type == ConversationTypeRoom){
        Room *room = (Room*)_conversation.peer;
        if(room.isMyRoom)
            return YES;
    }
    // There is always the mute/unmute button ??
    return YES;
}

- (IBAction)moreMenuClicked:(UIBarButtonItem *)sender {
    
    [FIRAnalytics logEventWithName:@"display_more_menue_popUp" parameters:@{@"category":@"chat_screen"}];
    if (_moreMenuActionSheet) {
        [_moreMenuActionSheet dismissViewControllerAnimated:NO completion:nil];
        _moreMenuActionSheet = nil;
    }
    
    _moreMenuActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    __weak __typeof__(self) weakSelf = self;
    
    NSString *title = @"";
    NSString *eventName = @"";
    if(_conversation.type == ConversationTypeRoom) {
        title = NSLocalizedString(@"Show bubble details", @"");
        eventName =@"show_bubble_details";
        if(((Room *)_conversation.peer).conference){
           title = NSLocalizedString(@"Show meeting details", @"");
            eventName =@"show_meeting_details";
        }
    } else {
        title = NSLocalizedString(@"Show contact details", @"");
        eventName =@"show_contact_details";
    }
    
    [FIRAnalytics logEventWithName:eventName parameters:@{@"action":[NSString stringWithFormat:@"more_menu_%@", eventName], @"category":@"chat_screen"}];

    UIAlertAction* contactDetails = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [weakSelf didTapTitleButton:nil];
    }];
    
    [contactDetails setValue:[[UIImage imageNamed:@"Vcard"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    
    [_moreMenuActionSheet addAction:contactDetails];
    
    UIAlertAction* conversationByEmail = [UIAlertAction actionWithTitle:NSLocalizedString(@"Receive conversation by e-mail", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        MBProgressHUD *hud = [UITools showHUDWithMessage:NSLocalizedString(@"Sending email", nil) forView:self.view mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
        __block BOOL actionDone = NO;
        [hud showAnimated:YES whileExecutingBlock:^{
            actionDone = [[ServicesManager sharedInstance].conversationsManagerService sendConversationByMail:_conversation];
            [FIRAnalytics logEventWithName:@"receive_conversation_by_Email" parameters:@{@"category":@"chat_screen"}];
        } completionBlock:^{
            if(actionDone)
                [UITools showHUDWithMessage:NSLocalizedString(@"Email sent",nil) forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Checkmark" dismissCompletionBlock:nil];
            else
                [UITools showHUDWithMessage:NSLocalizedString(@"Failed to send conversation by email",nil) forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Failed" dismissCompletionBlock:nil];
        }];
    }];
    
    [conversationByEmail setValue:[[UIImage imageNamed:@"Mail"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    
    if(_conversation.type != ConversationTypeBot)
        [_moreMenuActionSheet addAction:conversationByEmail];
    
    UIAlertAction *showReceivedFiles = [UIAlertAction actionWithTitle:NSLocalizedString(@"Show received files", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [FIRAnalytics logEventWithName:@"show_files_received" parameters:@{@"category":@"chat_screen"}];
        UIMyFilesOnServerTableViewController *myFiles = (UIMyFilesOnServerTableViewController *)[[UIStoryboardManager sharedInstance].myFilesStoryBoard instantiateViewControllerWithIdentifier:@"myFilesOnServerID"];
        myFiles.peer = _conversation.peer;
        if(_parentNavigationController)
           [_parentNavigationController pushViewController:myFiles animated:YES];
        else
            [self.navigationController pushViewController:myFiles animated:YES];
    }];
    
    [showReceivedFiles setValue:[UIImage imageNamed:@"folderRainbow"] forKey:@"image"];
    
    if(_conversation.type != ConversationTypeBot)
        [_moreMenuActionSheet addAction:showReceivedFiles];
    
    // We can mute/unmute a conversation
    UIAlertAction* muteUnmute = nil;
    if (!_conversation.isMuted) {
        muteUnmute = [UIAlertAction actionWithTitle:NSLocalizedString(@"Deactivate the notifications", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [[ServicesManager sharedInstance].conversationsManagerService muteConversation:_conversation];
            [FIRAnalytics logEventWithName:@"disable_notification" parameters:@{@"category":@"chat_screen"}];

        }];
        [muteUnmute setValue:[[UIImage imageNamed:@"muteAction"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    } else {
        muteUnmute = [UIAlertAction actionWithTitle:NSLocalizedString(@"Reactivate the notifications", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [[ServicesManager sharedInstance].conversationsManagerService unmuteConversation:_conversation];
            [FIRAnalytics logEventWithName:@"enable_notification" parameters:@{@"category":@"chat_screen"}];
        }];
        [muteUnmute setValue:[[UIImage imageNamed:@"unmuteAction"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    }
    
    if(muteUnmute) {
        [_moreMenuActionSheet addAction:muteUnmute];
    }
    
    UIAlertAction* removeAllMessages = [UIAlertAction actionWithTitle:NSLocalizedString(@"Remove all messages", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [FIRAnalytics logEventWithName:@"display_remove_all_messages_popup" parameters:@{@"category":@"chat_screen"}];
        UIAlertController *confirmDeleteAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Remove all messages", nil) message:NSLocalizedString(@"Do you really want to delete all messages in this conversation ?", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [_conversationsManagerService deleteAllMessagesForConversation:_conversation];
            [FIRAnalytics logEventWithName:@"remove_all_messages" parameters:@{@"action":@"remove_all_messages_popup_confirmed",@"category":@"chat_screen"}];
            //notify RecentConversationsTableViewController to close the conversation and remove it from the list
            //TODO - change kRemoveAllMessagesFromConversation to constant string
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kRemoveAllMessagesFromConversation" object:_conversation];
            //Refresh UI
            [_messages removeAllObjects];
            [self finishReceivingMessage];
            [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"conversation" action:@"delete" label:nil value:nil];
            [self showScrollToBottomButton:NO];
        }];
        
        UIAlertAction *cancelAlert = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [FIRAnalytics logEventWithName:@"remove_all_messages" parameters:@{@"action":@"remove_all_messages_popup_canceled",@"category":@"chat_screen"}];
        }];
        
        [confirmDeleteAlert addAction:ok];
        [confirmDeleteAlert addAction:cancelAlert];
        [self presentViewController:confirmDeleteAlert animated:YES completion:nil];
    }];
    
    [removeAllMessages setValue:[[UIImage imageNamed:@"removeAllMessages"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    if(_messages.count > 0 && _conversation.type != ConversationTypeRoom)
        [_moreMenuActionSheet addAction:removeAllMessages];
    
    // We can remove from network only a contact, not a room.
    if (_conversation.type == ConversationTypeUser) {
        Contact *contact = (Contact *)_conversation.peer;
        UIAlertAction* removeFromNetwork = [UIAlertAction actionWithTitle:NSLocalizedString(@"Remove from my network", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            UIAlertController *confirmRemoveAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Remove from my network", nil) message:NSLocalizedString(@"Do you really want to remove this user from your network ?", nil) preferredStyle:UIAlertControllerStyleAlert];
            [FIRAnalytics logEventWithName:@"display_remove_from_mynetwork_popup" parameters:@{@"category":@"chat_screen"}];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[ServicesManager sharedInstance].contactsManagerService removeContactFromMyNetwork:contact];
                [FIRAnalytics logEventWithName:@"remove_from_mynetwork" parameters:@{@"action":@"remove_from_mynetwork_confirmed",@"category":@"chat_screen"}];

            }];
            
            UIAlertAction *cancelAlert = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [FIRAnalytics logEventWithName:@"remove_from_mynetwork" parameters:@{@"action":@"remove_from_mynetwork_canceled",@"category":@"chat_screen"}];
            }];
            
            [confirmRemoveAlert addAction:ok];
            [confirmRemoveAlert addAction:cancelAlert];
            if(_parentNavigationController)
               [_parentNavigationController presentViewController:confirmRemoveAlert animated:YES completion:nil];
            else
                [self presentViewController:confirmRemoveAlert animated:YES completion:nil];
        }];
        [removeFromNetwork setValue:[[UIImage imageNamed:@"removeFromNetwork"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
        
        if(contact.isPresenceSubscribed || contact.sentInvitation.status == InvitationStatusPending) {
            [_moreMenuActionSheet addAction:removeFromNetwork];
        }
    }
    
    if(_conversation.type == ConversationTypeRoom){
        Room *room = (Room*)_conversation.peer;
        if(!room.conference || (room.conference && room.conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC)){
            if(room.myStatusInRoom == ParticipantStatusAccepted){
                if(!room.conference.isActive){
                    if(room.isMyRoom){
                        UIAlertAction* archiveRoom = [UIAlertAction actionWithTitle:NSLocalizedString(@"Archive", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            [FIRAnalytics logEventWithName:@"archive_bubble" parameters:@{@"category":@"chat_screen"}];
                            [UIRoomsTableViewController archiveAction:room inController:_parentNavigationController?_parentNavigationController:self completionHandler:nil];
                        }];
                        [archiveRoom setValue:[[UIImage imageNamed:@"archiveRoom"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
                        [_moreMenuActionSheet addAction:archiveRoom];
                    }
                    else {
                        UIAlertAction* leaveRoom = [UIAlertAction actionWithTitle:NSLocalizedString(@"Quit", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            [UIRoomsTableViewController leaveAction:room inController:_parentNavigationController?_parentNavigationController:self completionHandler:nil];
                            [FIRAnalytics logEventWithName:@"leave_bubble" parameters:@{@"category":@"chat_screen"}];
                        }];
                        [leaveRoom setValue:[[UIImage imageNamed:@"LeaveBubble"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
                        [_moreMenuActionSheet addAction:leaveRoom];
                    }
                }
            }
            else if(room.myStatusInRoom == ParticipantStatusUnsubscribed || room.myStatusInRoom == ParticipantStatusDeleted){
                UIAlertAction* deleteRoom = [UIAlertAction actionWithTitle:NSLocalizedString(@"Remove", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [UIRoomsTableViewController deleteAction:room inController:_parentNavigationController?_parentNavigationController:self completionHandler:nil];
                    isRemoveRoom = YES;
                }];
                [FIRAnalytics logEventWithName:@"delete_bubble" parameters:@{@"category":@"chat_screen"}];
                [deleteRoom setValue:[[UIImage imageNamed:@"DeleteBubble"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
                [_moreMenuActionSheet addAction:deleteRoom];
            }
        }
    }
    
    // Create a bubble from a one to one conversation
    if (_conversation.type == ConversationTypeUser && ![ServicesManager sharedInstance].myUser.isGuest) {
        UIAlertAction *migrateToBubble = [UIAlertAction actionWithTitle:NSLocalizedString(@"Add attendees", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [FIRAnalytics logEventWithName:@"add_attendees" parameters:@{@"category":@"chat_screen"}];
            UINavigationController *addAttendeeViewController = [[UIStoryboardManager sharedInstance].addAttendeesStoryBoard instantiateViewControllerWithIdentifier:@"addAttendeeViewControllerID"];
            UIGenericBubbleDetailsViewController *ctrl = ((UIGenericBubbleDetailsViewController*)addAttendeeViewController.topViewController);
            
            NSString *myName = [ServicesManager sharedInstance].myUser.contact.fullName ? [ServicesManager sharedInstance].myUser.contact.fullName : [ServicesManager sharedInstance].myUser.contact.jid;
            
            NSString *bubbleName = [NSString stringWithFormat:@"%@,%@", self.title, myName];
            int suffixCount = 0;
            NSArray *matchingBubbles =  [[ServicesManager sharedInstance].roomsService searchMyRoomBeginWith: bubbleName];
            NSMutableArray *matchingBubbleNames = [NSMutableArray array];
            for(Room *room in matchingBubbles){
                [matchingBubbleNames addObject:room.displayName];
            }
            while([matchingBubbleNames containsObject:bubbleName]){
                suffixCount++;
                bubbleName = [NSString stringWithFormat:@"%@,%@-%d", self.title, myName, suffixCount];
            }
            
            ctrl.roomName = bubbleName;
            ctrl.roomParticipants = @[(Contact *)_conversation.peer];
            
            if(_parentNavigationController)
                [_parentNavigationController presentViewController:addAttendeeViewController animated:YES completion:nil];
            else
                [weakSelf presentViewController:addAttendeeViewController animated:YES completion:nil];
        }];
        
        [migrateToBubble setValue:[[UIImage imageNamed:@"Add"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
        migrateToBubble.enabled = [ServicesManager sharedInstance].loginManager.isConnected;
        [_moreMenuActionSheet addAction:migrateToBubble];
    }
    
    // Cancel action
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [_moreMenuActionSheet addAction:cancel];
    
    // show the menu.
    [_moreMenuActionSheet.view setTintColor:[UITools defaultTintColor]];
    if(_parentNavigationController)
       [_parentNavigationController presentViewController:_moreMenuActionSheet animated:YES completion:nil];
    else
        [self presentViewController:_moreMenuActionSheet animated:YES completion:nil];
}

#pragma mark - Audio conference actions
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if([identifier isEqualToString:@"joinMeetingPopoverSegueID"]){
        if(((Room*)_conversation.peer).conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
            return YES;
        } else {
            [self joinConferenceButtonClicked:sender];
        }
    }
    return NO;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"joinMeetingPopoverSegueID"]){
        JoinMeetingViewController *joinMeetingController = [segue destinationViewController];
        joinMeetingController.room = (Room*)_conversation.peer;
        joinMeetingController.popoverPresentationController.delegate = self;
        joinMeetingController.delegate = self;
        [self.inputToolbar.contentView.textView resignFirstResponder];
    }
}

- (UIModalPresentationStyle) adaptivePresentationStyleForPresentationController: (UIPresentationController * ) controller {
    return UIModalPresentationNone;
}

-(void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    NSLog(@"Did dissmiss popover");
}

-(void) joinConference {
    [self performSegueWithIdentifier:@"joinMeetingPopoverSegueID" sender:_joinConferenceButton];
}

-(void) didSelectPhoneNumber:(PhoneNumber *)phoneNumber callBack:(BOOL)callBackRequested {
    _joinConferenceButton.enabled = NO;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *thePhoneNumber = nil;
    if(callBackRequested){
        // We will do a dialIn
        thePhoneNumber = phoneNumber.number;
    } else {
        // the server will call us back
    }
    Room *room = (Room *)_conversation.peer;
    ParticipantRole role = room.isMyRoom ? ParticipantRoleModerator : ParticipantRoleMember;
    [[ServicesManager sharedInstance].conferencesManagerService startAndJoinConference:room.conference inRoom:room phoneNumber:thePhoneNumber role:role completionBlock:^(NSError *error) {
        //[FIRAnalytics logEventWithName:@"start_ANDJoin_Confernce" parameters:@{@"typeOfCall":@"confernce call"}];
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            _joinConferenceButton.enabled = YES;
        });
        if(error){
            if([error.domain isEqualToString:ConferenceManagerErrorDomainSnapshot] || [error.domain isEqualToString:ConferenceManagerErrorDomainAttach]){
                [self showConferenceJoinErrorPopup];
            } else {
                [UITools showErrorPopupWithTitle:NSLocalizedString(@"Conference", nil) message:NSLocalizedString(@"Error while trying to join the conference", nil) inViewController:self];
            }
        } else {
            if(!callBackRequested && room.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                [self dialInForRoom:room withPhoneNumber:phoneNumber];
            }
        }
    }];
}

-(void) dialInForRoom:(Room *) aRoom withPhoneNumber:(PhoneNumber *) phoneNumber {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dialInForRoom:aRoom withPhoneNumber:phoneNumber];

        });
        return;
    }
    //[FIRAnalytics logEventWithName:@"start_dialIn_Call" parameters:@{@"typeOfCall":@"dial call"}];

    NSString *telWithoutSpaces = [phoneNumber.number stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *languageCode = nil;
    if(phoneNumber.needLanguageSelection){
        if([[ServicesManager sharedInstance].myUser.contact.countryCode isEqualToString:phoneNumber.countryCode]){
            languageCode = @"1#,";
        } else
            languageCode = @"2#,";
    }
    NSString *dialInNumber = [NSString stringWithFormat:@"telprompt://%@,%@%@#", telWithoutSpaces, languageCode, aRoom.isMyRoom?aRoom.conference.endpoint.moderatorCode:aRoom.conference.endpoint.participantCode];
    
    NSString *escaped = [dialInNumber stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:escaped];
    if([[UIApplication sharedApplication] canOpenURL:url]){
        [[UIApplication sharedApplication] openURL:url];
    } else {
        [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Cannot call with this device", nil) inViewController:self];
    }
}

-(void) showConferenceJoinErrorPopup {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showConferenceJoinErrorPopup];
        });
        return;
    }
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    
    [alert addButton:NSLocalizedString(@"Stop", nil) actionBlock:^(void) {
        // TODO : Stop the conference
        Room *room = (Room *)_conversation.peer;
        [[ServicesManager sharedInstance].conferencesManagerService terminateConference:room.conference completionHandler:^(NSError *error) {
            NSLog(@"Terminate conference %@", error);
            if(!error)
                [[ServicesManager sharedInstance].roomsService detachConference:room.conference fromRoom:room completionBlock:^(NSError *error) {
                    NSLog(@"Detach conference %@", error);
                }];
        }];
    }];
    
    
    alert.customViewColor = [UITools defaultTintColor];
    alert.iconTintColor = [UITools defaultBackgroundColor];
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = YES;
    
    [alert showError:self title:NSLocalizedString(@"Error while conference creation.", nil) subTitle:NSLocalizedString(@"You are already seen as connected to the conference. Please leave conference or stop it.", nil) closeButtonTitle:NSLocalizedString(@"Cancel", nil) duration:0.0f];
    alert.labelTitle.numberOfLines = 2;
}

#pragma mark - Active conference notifications

-(void)didReceiveConferenceReminder: (NSNotification *)notification {
    Conversation *conversation = notification.object;
    if(conversation == _conversation){
        [self showHideCallButton];
    }
}

#pragma mark -

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {} completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
         [self toggleDisableWhenOutOfTheRoom];
         [self updateBanner];
         [self adjustScrollToBottomButtonFrame];
         if(_attachmentView.superview)
             [self showAttachmentView];
         if(!self.iconImageView.hidden) // Adjust icon position
             self.iconImageView.frame = CGRectMake(self.statusLabel.frame.size.width/2-(self.iconImageView.frame.size.width/2), self.iconImageView.frame.origin.y, self.iconImageView.frame.size.width, self.iconImageView.frame.size.height);
     }];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}


-(void) toggleDisableWhenOutOfTheRoom {
    if (_conversation.type == ConversationTypeRoom) {
        Room *room = (Room *) _conversation.peer;
        if (room.myStatusInRoom == ParticipantStatusAccepted) {
            
            if ([self.inputToolbar.contentView.subviews.lastObject isKindOfClass:[UILabel class]]) {
                // If we have the label, remove it.
                UILabel *label = (UILabel *) self.inputToolbar.contentView.subviews.lastObject;
                [label removeFromSuperview];
            }
            
        } else {
            CGRect frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, self.inputToolbar.contentView.bounds.size.height);
            
            if (![self.inputToolbar.contentView.subviews.lastObject isKindOfClass:[UILabel class]]) {
                // Create the label if missing.
                MarqueeLabel *label = [[MarqueeLabel alloc] initWithFrame:frame];
                [UITools applyCustomFontTo:label];
                [label setText:NSLocalizedString(@"You are no longer a member of this bubble and you can therefore not publish anything.", nil)];
                label.textAlignment = NSTextAlignmentCenter;
                label.backgroundColor = [UITools defaultBackgroundColor];
                label.marqueeType = MLLeftRight;
                label.rate = 60;
                label.leadingBuffer = 5;
                label.trailingBuffer = 5;
                [label setUserInteractionEnabled:YES];
                [self.inputToolbar.contentView addSubview:label];
                
            } else {
                // If label exists, update its size.
                UILabel *label = (UILabel *) self.inputToolbar.contentView.subviews.lastObject;
                label.frame = frame;
            }
        }
    }
}

#pragma mark - Status banner
/*
 * Decide whether to show or no the top status banner
 * and to update it's content.
 */
-(void) updateBanner {
    if (!self.statusLabel)
        return;
    
    // Right now, enable the banner only for conversation of type user with non guest user.
    if (_conversation.type == ConversationTypeUser){
        
        Contact *contact = (Contact*) _conversation.peer;
        self.iconImageView.hidden = YES;
        
        NSMutableString *textToDisplay = [[NSMutableString alloc] initWithString:@""];
        
        if(!contact.isPresenceSubscribed && ![ServicesManager sharedInstance].myUser.isGuest) {
            if(!contact.isPresenceSubscribed && !contact.sentInvitation){
                self.iconImageView.hidden = NO;
                [textToDisplay appendString:NSLocalizedString(@"Add to my network", nil)];
            } else if(contact.sentInvitation.status == InvitationStatusPending) {
                [textToDisplay appendString:NSLocalizedString(@"Invitation sent", nil)];
            }
            
            if(!self.iconImageView.hidden)
                self.statusLabel.text = [NSString stringWithFormat:@"\n%@", NSLocalizedString(textToDisplay, nil)];
            else
                self.statusLabel.text = NSLocalizedString(textToDisplay,nil);
            
            self.statusLabel.backgroundColor = [UIColor grayColor];
        }
        
        else {
            NSString *presence = @"";
            
            if( contact.presence.presence != ContactPresenceAvailable ) {
                presence = [UITools getPresenceText:contact];
                [textToDisplay appendString: NSLocalizedString(presence,nil)];
                self.statusLabel.backgroundColor = [UITools colorForPresence:contact.presence];
            }
            
            if((contact.calendarPresence.presence != CalendarPresenceAvailable && contact.calendarPresence.presence != CalendarPresenceUnavailable) || contact.calendarPresence.automaticReply.isEnabled) {
                if( ![presence isEqualToString:@""] ) {
                    [textToDisplay appendString: @"\n"];
                } else {
                    self.statusLabel.backgroundColor = [UIColor darkGrayColor];
                }
                
                NSString *calendarPresence = @"";
                
                // Calendar Out of office
                if( contact.calendarPresence.automaticReply.isEnabled ) {
                    if( contact.calendarPresence.automaticReply.untilDate != nil ) {
                        calendarPresence = [NSString stringWithFormat:NSLocalizedString(@"Out of office until %@", nil), [UITools optimizedFormatForCalendarPresenceDate: contact.calendarPresence.automaticReply.untilDate]];
                    } else {
                        calendarPresence = [NSString stringWithFormat:NSLocalizedString(@"Out of office", nil)];
                    }
                }
                
                // Calendar presence
                else if( contact.calendarPresence.presence == CalendarPresenceBusy ) {
                    calendarPresence = [NSString stringWithFormat:NSLocalizedString(@"Appointment until %@", nil), [UITools optimizedFormatForCalendarPresenceDate: contact.calendarPresence.until]];
                } else if( contact.calendarPresence.presence == CalendarPresenceOutOfOffice ) {
                    calendarPresence = [NSString stringWithFormat:NSLocalizedString(@"Out of office until %@", nil), [UITools optimizedFormatForCalendarPresenceDate: contact.calendarPresence.until]];
                }
                
                [textToDisplay appendString: NSLocalizedString(calendarPresence,nil)];
            }
            
            self.statusLabel.text = textToDisplay;
        }
        
        if (contact.lastActivityDate) {
            _updateBannerRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:D_MINUTE target:self selector:@selector(updateBanner) userInfo:nil repeats:NO];
        }
        
        if(!contact.isPresenceSubscribed) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnStatusLabel)];
            tap.numberOfTapsRequired = 1;
            tap.numberOfTouchesRequired = 1;
            tap.cancelsTouchesInView = NO;
            [self.statusLabel addGestureRecognizer:tap];
            self.statusLabel.userInteractionEnabled = YES;
        }
        
        // Show the banner if a message has been set
        if( [self.statusLabel.text isEqualToString:@""] ) {
            self.statusLabel.hidden = YES;
        } else {
            self.statusLabel.hidden = NO;
        }
        
        // Use the label height as the offset of the JSQMessagesViewController
        [self calculateTopContentAdditionalInset];
    } else {
        self.statusLabel.hidden = YES;
        [self calculateTopContentAdditionalInset];
        if(self.statusLabel.gestureRecognizers.count > 0)
            [self.statusLabel.gestureRecognizers enumerateObjectsUsingBlock:^(__kindof UIGestureRecognizer *gesture, NSUInteger idx, BOOL * stop) {
                [self.statusLabel removeGestureRecognizer:gesture];
            }];
        self.statusLabel.userInteractionEnabled = NO;
    }
    
    if(_conversation.isMuted)
        [self showNotificationDisabledView];
    else
        [self hideNotificationDisabledView];
}

-(void) didTapOnStatusLabel {
    NSLog(@"didTapOnStatusLabel");
    
    // Right now, enable the banner only for conversation of type user.
    if (_conversation.type != ConversationTypeUser)
        return;
    
    Contact *contact = (Contact*) _conversation.peer;
    
    if(!contact.isPresenceSubscribed && !contact.sentInvitation){
        [_contactsManagerService inviteContact:contact];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self updateBanner];
        });
    }
    
    // We must update the more button status in case of changes of one of his conditions
    _moreMenuButton.enabled = [self willShowSomethingInMoreMenu];
}

-(void) showNotificationDisabledView {
    if(!_notificationDisabledView.hidden){
        [self hideNotificationDisabledView];
    }
    
    _notificationDisabledView.layer.masksToBounds = NO;
    _notificationDisabledView.layer.shadowOffset = CGSizeMake(0, 2);
    _notificationDisabledView.layer.shadowRadius = 5;
    _notificationDisabledView.layer.shadowOpacity = 0.2;
    _notificationDisabledView.backgroundColor = [UITools defaultBackgroundColor];
    _notificationDisabledView.hidden = NO;
    CGRect frame = _notificationDisabledView.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    if(!self.statusLabel.hidden){
        frame.origin.y = self.statusLabel.frame.size.height;
    }
    if ([ServicesManager sharedInstance].myUser.isGuest)
        frame.origin.y += kGuestModeHeight;
    _notificationDisabledView.frame = frame;
    _notificationDisabledView.translatesAutoresizingMaskIntoConstraints = YES;
    _notificationDisabledLabel.text = NSLocalizedString(@"Notification are deactivated", nil);
    [_notificationDisabledButton setTitle:NSLocalizedString(@"Reactivate", nil) forState:UIControlStateNormal];
    [UITools applyCustomFontTo:_notificationDisabledLabel];
    [UITools applyCustomFontTo:_notificationDisabledButton.titleLabel];
    [_notificationDisabledButton setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
    
    [self.view addSubview:_notificationDisabledView];
    [_notificationDisabledView setNeedsLayout];
    [self calculateTopContentAdditionalInset];
}

-(void) hideNotificationDisabledView {
    _notificationDisabledView.hidden = YES;
    CGRect frame = _notificationDisabledView.frame;
    frame.origin.y = 0;
    _notificationDisabledView.frame = frame;
    [_notificationDisabledView removeFromSuperview];
    
    [self calculateTopContentAdditionalInset];
}

-(void) calculateTopContentAdditionalInset {
    NSInteger value = 0;
    if(!self.statusLabel.hidden){
        value += self.statusLabel.frame.size.height;
    }
    if(!_notificationDisabledView.hidden){
        value += _notificationDisabledView.frame.size.height;
    }
    if ([ServicesManager sharedInstance].myUser.isGuest)
        value += kGuestModeHeight;
    self.topContentAdditionalInset = value;
}
- (IBAction)unmuteConversation:(UIButton *)sender {
    [[ServicesManager sharedInstance].conversationsManagerService unmuteConversation:_conversation];
}

#pragma mark - load messages
-(void) loadMessages {
    if(_conversation.isSynchronized && _allowAutomaticallyScrollsToMostRecentMessage && ![_messagesBrowser hasMorePages]){
        NSLog(@"This conversation is already synchronized and we are not loading a new page");
        dispatch_async(dispatch_get_main_queue(), ^{
            BOOL hasPage = [_messagesBrowser hasMorePages];
            self.showLoadEarlierMessagesHeader = hasPage;
        });
        return;
    } else if(!_conversation.isSynchronized){
        NSLog(@"Start resync");
        _isRetrievingNextPage = YES;
        [_messagesBrowser resyncBrowsingCacheWithCompletionHandler:^(NSArray *addedCacheItems, NSArray *removedCacheItems, NSArray *updatedCacheItems, NSError *error) {
            NSLog(@"Resync done");
            dispatch_async(dispatch_get_main_queue(), ^{
                BOOL hasPage = [_messagesBrowser hasMorePages];
                _isRetrievingNextPage = NO;
                self.showLoadEarlierMessagesHeader = hasPage;
                if (hasPage && !_stopShowingHeaderView) { // check if header appear
                    _allowAutomaticallyScrollsToMostRecentMessage = NO;
                    [self loadMessages];
                }
            });
        }];
    } else {
        NSLog(@"We have scrolled so load next page");
        _isRetrievingNextPage = YES;
        self.automaticallyScrollsToMostRecentMessage = NO;
        [_messagesBrowser nextPageWithCompletionHandler:^(NSArray *addedCacheItems, NSArray *removedCacheItems, NSArray *updatedCacheItems, NSError *error) {
            if(!error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    BOOL hasPage = [_messagesBrowser hasMorePages];
                    self.showLoadEarlierMessagesHeader = hasPage;
                    _isRetrievingNextPage = NO;
                });
            }
        }];
    }
}

#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date {
    NSLog(@"Did press send button");
    
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    NSString* action = @"send_text_message";
    NSString* hasCaption = @"YES";
    if([text isEqualToString:@""]){
        hasCaption = @"NO";
    }
    if(_attachmentFileToSend) {
        // disable attachment file button
        [self.inputToolbar.contentView.leftBarButtonItem setEnabled:NO];
        if(_attachmentFileToSend.type == FileTypeImage ){
            action = @"send_photo_message";
        }

    } else if(_attachmentFileToSend.type == FileTypeAudio ) {
        action = @"send_audio_message";
    }
    else if(_attachmentFileToSend.type == FileTypeVideo ) {
        action = @"send_video_message";
    }
   
    
    // _attachmentFileToSend.tag = [_messages count];
    [_conversationsManagerService sendMessage:text fileAttachment:_attachmentFileToSend to:_conversation completionHandler:^(Message *message, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
           [self.inputToolbar.contentView.leftBarButtonItem setEnabled:YES];
            if (error) {
                NSString *msg = NSLocalizedString([error.userInfo objectForKey:NSLocalizedDescriptionKey],nil);
                if(error.code == 413)
                    msg = NSLocalizedString(@"Attachment reach allowed transfer limit", nil);

                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Cannot send message", nil)
                                                                                         message:msg
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK",@"OK")
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:^(UIAlertAction __unused *action) {}];
                [alertController addAction:okAction];
                //[self presentViewController:alertController animated:YES completion:nil];
                return;
            }
            
        });
        [FIRAnalytics logEventWithName:@"send_IM"
                            parameters:@{
                                         @"action" :action,
                                         @"category":@"chat_screen"
                                         }];
        
    } attachmentUploadProgressHandler:nil];
     [self closeAttachementButtonClicked:nil];
     [self finishSendingMessageAnimated:YES];
}

// We override the original method to use our own typing indicator
// Because we want an avatar next to the indicator to see who's typing (particulary in rooms)
- (UICollectionReusableView *)collectionView:(JSQMessagesCollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    if (self.showTypingIndicator && [kind isEqualToString:UICollectionElementKindSectionFooter]) {
        JSQMessagesTypingIndicatorFooterViewWithAvatar *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:[JSQMessagesTypingIndicatorFooterViewWithAvatar footerReuseIdentifier] forIndexPath:indexPath];
        
        NSMutableArray<UIImage *> *avatars = [NSMutableArray new];
        for (Contact *contact in _typingContacts) {
            id image = [_contactAvatarDict objectForKey:contact.jid].avatarImage;
            if (image != nil)
                [avatars addObject:image];
        }
        [footer configureWithAvatar:avatars
                      ellipsisColor:[collectionView typingIndicatorEllipsisColor]
                 messageBubbleColor:[collectionView typingIndicatorMessageBubbleColor]
                  forCollectionView:collectionView];
        
        return footer;
    }
    else if (self.showLoadEarlierMessagesHeader && [kind isEqualToString:UICollectionElementKindSectionHeader]) {
        JSQMessagesAutoLoadEarlierHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                                          withReuseIdentifier:[JSQMessagesAutoLoadEarlierHeaderView headerReuseIdentifier]
                                                                                                 forIndexPath:indexPath];
        return header;
    }
    
    return nil;
}


#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods
- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender {
    //    if ([UIPasteboard generalPasteboard].image) {
    //        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
    //        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
    //        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId senderDisplayName:self.senderDisplayName date:[NSDate date] media:item];
    ////        [self.demoData.messages addObject:message];
    //        [self finishSendingMessage];
    //        return NO;
    //    }
    return YES;
}

#pragma mark - Check for textview changes
// This enables XEP-085 chat-state
// triggered by UITextViewTextDidChangeNotification
-(void) UITextViewDidChange:(NSNotification *)notification {
    
    UITextView *textView = notification.object;
    
    if (textView == self.inputToolbar.contentView.textView) {
        [self updateConversationStatusAndToggleSendButton];
    }
}

-(void) updateConversationStatusAndToggleSendButton {
    NSString *composed = [self.inputToolbar.contentView.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([composed length]) {
        [_conversationsManagerService setStatus:ConversationStatusComposing forConversation:_conversation];
    } else {
        [_conversationsManagerService setStatus:ConversationStatusActive forConversation:_conversation];
    }
    
    BOOL hasText = [self.inputToolbar.contentView.textView hasText] || [_attachmentView superview];
    if (self.inputToolbar.sendButtonOnRight) {
        self.inputToolbar.contentView.rightBarButtonItem.enabled = hasText;
    }
    else {
        self.inputToolbar.contentView.leftBarButtonItem.enabled = hasText;
    }
}

#pragma mark - facilitors for browser cache and grouped messages good index finder
-(NSIndexPath*) rowIndexFromBrowserCacheIndex:(NSInteger)idx {
    return [NSIndexPath indexPathForRow:[_messagesBrowser.browsingCache count]-idx-1 inSection:0];
}

-(NSIndexPath*) rowIndexFromGroupedMessagesIndex:(NSInteger)idx {
    return [NSIndexPath indexPathForRow:[_messages count]-idx-1 inSection:0];
}

-(NSInteger) groupedMessagesIndexForRowIndex:(NSIndexPath*)idx {
    return [_messages count]-idx.item-1;
}

-(NSInteger) insertMessageAtProperIndex:(GroupedMessages *) groupedMessages {
    NSInteger idxForInsertion = NSNotFound;
    if ([_messages indexOfObject:groupedMessages]==NSNotFound) {
        idxForInsertion = [self properIndexForItem:groupedMessages];
        NSMutableArray * groupedMessageArray = [NSMutableArray arrayWithArray:[groupedMessages allGroupedMessages]];
        for (Message * message in groupedMessageArray) {
            if (message.attachment && !message.attachment.isDownloadAvailable) {
                [groupedMessages removeMessage:message];
            }
        }
        if (groupedMessages.allGroupedMessages.count) {
            [_messages insertObject:groupedMessages atIndex:idxForInsertion];
        }
        
    }
    return idxForInsertion;
}

-(NSInteger) properIndexForItem:(GroupedMessages *) groupedMessages {
    __block NSInteger insertIndex = [_messages count];
    // if we don't find a proper place in the list, this means we should add it at the end.
    [_messages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        GroupedMessages *item = obj;
        // if item.date is not later than aNewItem.date
        if (item.date!=nil && ![item isEqual:groupedMessages] && [item.date compare:groupedMessages.date]!=NSOrderedDescending) {
            *stop = YES;
            insertIndex = idx;
        }
    }];
    return insertIndex;
}


#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath {
    //    [self.demoData.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];

    if ([message.senderId isEqualToString:self.senderId]) {
        if([self isTheNextMessageHasSameSender:indexPath]){
            _bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRegularTaillessImage] capInsets:UIEdgeInsetsZero];
            _outgoingBubbleImageData = [_bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
            return _outgoingBubbleImageData;
        }
        else {
            _bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRainbowImage] capInsets:UIEdgeInsetsZero];
            _outgoingBubbleImageData = [_bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
            return _outgoingBubbleImageData;
        }
       
    }
    
    if (message.senderId) {
        JSQMessagesBubbleImage *incomingBubbleImageData;
        if([self isTheNextMessageHasSameSender:indexPath]){
            _bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRegularTaillessImage] capInsets:UIEdgeInsetsZero];
            incomingBubbleImageData = [_bubbleFactory incomingMessagesBubbleImageWithColor:[UITools colorForString:message.lastMessage.peer.displayName]];
            return incomingBubbleImageData;
        }
        else {
            _bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRainbowImage] capInsets:UIEdgeInsetsZero];
            incomingBubbleImageData = [_bubbleFactory incomingMessagesBubbleImageWithColor:[UITools colorForString:message.lastMessage.peer.displayName]];
            return incomingBubbleImageData;
        }
      
    }
    return nil;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return _meAvatar;
    }
    
    if (message.senderId)
        return [_contactAvatarDict objectForKey:message.senderId];
    return nil;
}

// Not used, height is 0
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    if(indexPath.row % 5 == 0){
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    return nil;
}

/**
 *  Show the remote contact firstname, only if this is a "Room" conversation (i.e. with multiple persons)
 */
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    /*
     if (_conversation.type == ConversationTypeRoom) {
     GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
     if (!message.lastMessage.isOutgoing) {
     Peer *peer = message.lastMessage.peer;
     
     if ([peer isKindOfClass:[Contact class]]) {
     Contact *contact = (Contact *) peer;
     return [[NSAttributedString alloc] initWithString:contact.displayName attributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:12]}];
     }
     }
     }
     */
    return nil;
}

/*
 *  The small text under the messages.
 *  We use it to show if the message has been delivered and seen
 */
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    if (![message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    if (!_tappedIndexPath || [_tappedIndexPath compare:indexPath] != NSOrderedSame) {
        return nil;
    }
    
    NSMutableAttributedString *string = [NSMutableAttributedString new];
    
    // TODO: remove this once ACK mecanism will be defined for Rooms
    MessageDeliveryState state = message.lastMessage.state;
    if (_conversation.type == ConversationTypeRoom) {
        // We dont show Received and Read in case of Room conv.
        if (state == MessageDeliveryStateReceived || state == MessageDeliveryStateRead) {
            state = MessageDeliveryStateDelivered;
        }
    }
    // --
    
    switch(state) {
        case MessageDeliveryStateSent: {
            [string.mutableString setString:NSLocalizedString(@"Sending", nil)];
            break;
        }
        case MessageDeliveryStateDelivered: {
            NSDate *timestamp = [message.lastMessage deliveryDateForState:MessageDeliveryStateDelivered];
            if (!timestamp) {
                [string.mutableString setString:NSLocalizedString(@"Sent", nil)];
                break;
            }
            
            if([timestamp isToday]) {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Sent at %@", nil), [UITools formatTimeForDate:timestamp]]];
            } else {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Sent on %@ at %@", nil), [UITools shortFormatForDate:timestamp], [UITools formatTimeForDate:timestamp]]];
            }
            break;
        }
        case MessageDeliveryStateReceived: {
            NSDate *timestamp = [message.lastMessage deliveryDateForState:MessageDeliveryStateReceived];
            if (!timestamp) {
                [string.mutableString setString:NSLocalizedString(@"Received", nil)];
                break;
            }
            
            if([timestamp isToday]) {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Received at %@", nil), [UITools formatTimeForDate:timestamp]]];
            } else {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Received on %@ at %@", nil), [UITools shortFormatForDate:timestamp], [UITools formatTimeForDate:timestamp]]];
            }
            break;
        }
        case MessageDeliveryStateRead: {
            NSDate *timestamp = [message.lastMessage deliveryDateForState:MessageDeliveryStateRead];
            if (!timestamp) {
                [string.mutableString setString:NSLocalizedString(@"Read", nil)];
                break;
            }
            
            if([timestamp isToday]) {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Read at %@", nil), [UITools formatTimeForDate:timestamp]]];
            } else {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Read on %@ at %@", nil), [UITools shortFormatForDate:timestamp], [UITools formatTimeForDate:timestamp]]];
            }
            break;
        }
        case MessageDeliveryStateFailed: {
            [string.mutableString setString:NSLocalizedString(@"Failed", nil)];
            break;
        }
    }
    
    [string setAttributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName]  size:12]} range:NSMakeRange(0, string.length)];
    return string;
}

-(MBProgressHUD *) showHUDWithMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = message;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}

-(MBProgressHUD *) showHUDWithErrorMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = message;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error"]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}


#pragma mark - Responding to UICollectionViewDelegate protocol
- (void)collectionView:(UICollectionView *)collectionView willDisplaySupplementaryView:(UICollectionReusableView *)view
        forElementKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
    self.stopShowingHeaderView = NO;
    if([elementKind isEqualToString:UICollectionElementKindSectionHeader] &&
       [view isKindOfClass:[JSQMessagesAutoLoadEarlierHeaderView class]]) {
        if(!_isRetrievingNextPage){
            
            NSLog(@"Load earlier messages!");
            [self loadMessages];
        } else {
            NSLog(@"Already retrieving a page");
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingSupplementaryView:(UICollectionReusableView *)view forElementOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
    self.stopShowingHeaderView = YES;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if(self.automaticallyScrollsToMostRecentMessage)
        [_conversationsManagerService markAsReadByMeAllMessageForConversation:_conversation];
}

#pragma mark - Adjusting cell label heights
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    if ([self collectionView:collectionView attributedTextForCellBottomLabelAtIndexPath:indexPath]) {
        return 12.f;
    }
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    if([self isThePrevoiusMessageHasSameSender:indexPath]){
      return kJSQMessagesCollectionViewCellLabelHeightGroup;
    }
    else {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_messages count];
}

/**
 * return the strechable background image for group chat events
 */
-(UIImage *)eventBubbleCellImage {
    UIImage *bubbleImage = [[UIImage jsq_bubbleRegularStrokedTaillessImage] jsq_imageMaskedWithColor:[UIColor lightGrayColor]];
    // make image stretchable from center point
    CGPoint center = CGPointMake(bubbleImage.size.width / 2.0f, bubbleImage.size.height / 2.0f);
    UIEdgeInsets capInsets = UIEdgeInsetsMake(center.y, center.x, center.y, center.x);
    bubbleImage = [bubbleImage resizableImageWithCapInsets:capInsets resizingMode:UIImageResizingModeStretch];
    
    return bubbleImage;
}

- (JSQGroupChatEventCell *)groupChatEventCellFor:(JSQMessagesCollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath message:(Message *)message{
    id<JSQMessageData> messageItem = [collectionView.dataSource collectionView:collectionView messageDataForItemAtIndexPath:indexPath];
    JSQGroupChatEventCell *groupChatEventCell = (JSQGroupChatEventCell *)[collectionView dequeueReusableCellWithReuseIdentifier: [JSQGroupChatEventCell cellReuseIdentifier] forIndexPath:indexPath];
    
    groupChatEventCell.messageBubbleImageView.image = self.groupChatEventBubbleImage;
    
    [groupChatEventCell setTimestamp: messageItem.date];
    
    if (message.groupChatEventPeer.jid != nil) {
        id<JSQMessageAvatarImageDataSource> avatarImageDataSource = [_contactAvatarDict objectForKey:message.groupChatEventPeer.jid];
        
        if (avatarImageDataSource != nil) {
            
            UIImage *avatarImage = [avatarImageDataSource avatarImage];
            if (avatarImage == nil) {
                groupChatEventCell.avatarImageView.image = [avatarImageDataSource avatarPlaceholderImage];
                groupChatEventCell.avatarImageView.highlightedImage = nil;
            }
            else {
                groupChatEventCell.avatarImageView.image = avatarImage;
                groupChatEventCell.avatarImageView.highlightedImage = [avatarImageDataSource avatarHighlightedImage];
            }
        }
    }
    
    
    if([message.groupChatEventPeer isKindOfClass:[Contact class]]){
        [groupChatEventCell setCompanyName:((Contact *)message.groupChatEventPeer).companyName];
    } else {
        [groupChatEventCell setCompanyName:@""];
    }
    
    [groupChatEventCell setMessage: message.bodyForGroupChatEvent];
    groupChatEventCell.textView.textColor = [UIColor lightGrayColor];
    
    return groupChatEventCell;
}

- (JSQWebRTCEventsCell *)callRecordingEventCellFor:(JSQMessagesCollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath message:(Message *)message {
    JSQWebRTCEventsCell *webRTCEventCell = (JSQWebRTCEventsCell *)[collectionView dequeueReusableCellWithReuseIdentifier: [JSQWebRTCEventsCell cellReuseIdentifier] forIndexPath:indexPath];
    
    webRTCEventCell.messageBubbleImageView.image = self.groupChatEventBubbleImage;

    id<JSQMessageAvatarImageDataSource> avatarImageDataSource = [_contactAvatarDict objectForKey:message.peer.jid];
    
    if (avatarImageDataSource != nil) {
        UIImage *avatarImage = [avatarImageDataSource avatarImage];
        if (avatarImage == nil) {
            webRTCEventCell.avatarImageView.image = [avatarImageDataSource avatarPlaceholderImage];
            webRTCEventCell.avatarImageView.highlightedImage = nil;
        } else {
            webRTCEventCell.avatarImageView.image = avatarImage;
            webRTCEventCell.avatarImageView.highlightedImage = [avatarImageDataSource avatarHighlightedImage];
        }
    }
    
    [webRTCEventCell setTimestamp: message.date];
    [webRTCEventCell setEventTypeText:@""];
    
    switch (message.type) {
        case MessageTypeCallRecordingStart:{
            [webRTCEventCell setMessage:[NSString stringWithFormat:NSLocalizedString(@"%@ started to record you", nil), message.peer.displayName]];
            [webRTCEventCell setEventTypeColor:[UIColor lightGrayColor]];
            break;
        }
        case MessageTypeCallRecordingStop:{
            [webRTCEventCell setMessage:[NSString stringWithFormat:NSLocalizedString(@"%@ stoped to record you", nil), message.peer.displayName]];
            [webRTCEventCell setEventTypeColor:[UIColor lightGrayColor]];
            break;
        }
        default:{
            break;
        }
    }
    
    return webRTCEventCell;
}

- (JSQWebRTCEventsCell *)webRTCEventCellFor:(JSQMessagesCollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath message:(Message *)message {
    JSQWebRTCEventsCell *webRTCEventCell = (JSQWebRTCEventsCell *)[collectionView dequeueReusableCellWithReuseIdentifier: [JSQWebRTCEventsCell cellReuseIdentifier] forIndexPath:indexPath];
    
    webRTCEventCell.messageBubbleImageView.image = self.groupChatEventBubbleImage;
    [webRTCEventCell setTimestamp:message.callLog.date];
    if(message.isOutgoing){
        // Avatar is a headset
        webRTCEventCell.avatarImageView.image = [UIImage imageNamed:@"headset"];
        webRTCEventCell.avatarImageView.tintColor = [UITools defaultTintColor];
    } else {
        id<JSQMessageAvatarImageDataSource> avatarImageDataSource = [_contactAvatarDict objectForKey:message.peer.jid];
        
        if (avatarImageDataSource != nil) {
            UIImage *avatarImage = [avatarImageDataSource avatarImage];
            if (avatarImage == nil) {
                webRTCEventCell.avatarImageView.image = [avatarImageDataSource avatarPlaceholderImage];
                webRTCEventCell.avatarImageView.highlightedImage = nil;
            } else {
                webRTCEventCell.avatarImageView.image = avatarImage;
                webRTCEventCell.avatarImageView.highlightedImage = [avatarImageDataSource avatarHighlightedImage];
            }
        }
    }
    
    
    switch (message.callLog.state) {
        case CallLogStateMissed:{
            if(message.isOutgoing){
                [webRTCEventCell setEventTypeText:NSLocalizedString(@"Canceled call", nil)];
                [webRTCEventCell setEventTypeColor:[UIColor lightGrayColor]];
            } else {
                [webRTCEventCell setEventTypeText:NSLocalizedString(@"Missed call", nil)];
                [webRTCEventCell setEventTypeColor:[UITools notificationBadgeColor]];
            }
            break;
        }
        case CallLogStateAnswered:{
            NSString *duration = message.callLog.duration;
            NSString *durationReadable = [UITools timeFormatConvertToSeconds:duration zeroFormattingBehaviorunitsStyle:NSDateComponentsFormatterZeroFormattingBehaviorDropAll unitsStyle:NSDateComponentsFormatterUnitsStyleAbbreviated];
            [webRTCEventCell setEventTypeText:[NSString stringWithFormat:NSLocalizedString(@"Duration %@", nil), durationReadable]];
            [webRTCEventCell setEventTypeColor:[UIColor lightGrayColor]];
            break;
        }
        case CallLogStateUnknown:
        default:{
            [webRTCEventCell setEventTypeText:@""];
            break;
        }
    }
    
    [webRTCEventCell setMessage:[message body]];
    
    return webRTCEventCell;
}

-(JSQRainbowOutgoingMessagesCollectionViewCell *) outgoingCellFor:(JSQMessagesCollectionView *) collectionView atIndexPath:(NSIndexPath *) indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    JSQRainbowOutgoingMessagesCollectionViewCell *outgoingCell = (JSQRainbowOutgoingMessagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier: self.outgoingCellIdentifier forIndexPath:indexPath];
    
    Message *lastMessage = message.lastMessage;
    
    // We need the attachment to configure correclty the cell
    outgoingCell.attachment = lastMessage.attachment;
    outgoingCell.progressView.tag = indexPath.row;
    
    [self configureCell:outgoingCell forCollectionView:collectionView indexPath:indexPath];
    
    outgoingCell.textView.textColor = [UIColor blackColor];
    
    
    
    // The timestamp INSIDE the message is always the delivered timestamp.
    // The timestamp UNDER the message (once clicked) is the current status timestamp
    [outgoingCell setTimestamp: [lastMessage deliveryDateForState:MessageDeliveryStateDelivered]];
    
   
    // Hide status image for file transfer message and webrtc
    if(lastMessage.type == MessageTypeWebRTC || lastMessage.type == MessageTypeFileTransfer){
        outgoingCell.status.hidden = YES;
    } else {
        outgoingCell.status.hidden = NO;
    }

    // TODO: remove this once ACK mecanism will be defined for Rooms
    MessageDeliveryState state = lastMessage.state;    
    if (_conversation.type == ConversationTypeRoom) {
        // We dont show Received and Read in case of Room conv.
        if (state == MessageDeliveryStateReceived || state == MessageDeliveryStateRead) {
            state = MessageDeliveryStateDelivered;
        }
    }
    // --
    
    switch(state) {
        case MessageDeliveryStateSent: {
            outgoingCell.status.image = [UIImage imageNamed:@"message_sent"];
            [outgoingCell.status setTintColor:[UIColor lightGrayColor]];
            [outgoingCell setTimestamp: [lastMessage deliveryDateForState:state]];
            break;
        }
        case MessageDeliveryStateDelivered: {
            outgoingCell.status.image = [UIImage imageNamed:@"message_received_server"];
            [outgoingCell.status setTintColor:[UIColor lightGrayColor]];
            break;
        }
        case MessageDeliveryStateReceived: {
            outgoingCell.status.image = [UIImage imageNamed:@"message_received_read_client"];
            [outgoingCell.status setTintColor:[UIColor lightGrayColor]];
            break;
        }
        case MessageDeliveryStateRead: {
            outgoingCell.status.image = [UIImage imageNamed:@"message_received_read_client"];
            [outgoingCell.status setTintColor:[UITools defaultTintColor]];
            break;
        }
        case MessageDeliveryStateFailed: {
            outgoingCell.status.image = [UIImage imageNamed:@"message_ko"];
            [outgoingCell.status setTintColor:[UIColor redColor]];
            break;
        }
    }
    
    if (lastMessage.attachment != nil && lastMessage.attachment.value < 1.0) {
        
        if (( state == MessageDeliveryStateSent) || ([_filesDownloading containsObject:outgoingCell.attachment] )) {
            outgoingCell.progressView.hidden = NO;
            outgoingCell.cellDateLabel.hidden = YES;
            [outgoingCell setProgressViewWithValue:lastMessage.attachment.value];
        }
        else{
            outgoingCell.progressView.hidden = YES;
            outgoingCell.cellDateLabel.hidden = NO;
        }
    }
    else{
        outgoingCell.progressView.hidden = YES;
        outgoingCell.cellDateLabel.hidden = NO;
       
    }
    
    return outgoingCell;
}

-(JSQRainbowIncomingMessagesCollectionViewCell *) incomingCellFor:(JSQMessagesCollectionView *) collectionView atIndexPath:(NSIndexPath *) indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    JSQRainbowIncomingMessagesCollectionViewCell *incomingCell = (JSQRainbowIncomingMessagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier: self.incomingCellIdentifier forIndexPath:indexPath];
    Message *lastMessage = message.lastMessage;
    incomingCell.attachment = lastMessage.attachment;
    _needsAvatar = YES;
    if (_messages.count > 0) {
        if ([self isTheNextMessageHasSameSender:indexPath]) {
             _needsAvatar = NO;
        }
        }
    [self configureCell:incomingCell forCollectionView:collectionView indexPath:indexPath];
    incomingCell.textView.textColor = [UIColor whiteColor];
    
    
    [incomingCell setTimestamp: message.lastMessage.date];
    incomingCell.progressView.tag = indexPath.row;

    if (lastMessage.attachment != nil && lastMessage.attachment.value < 1.0 && [_filesDownloading containsObject:incomingCell.attachment]) {
        
        incomingCell.progressView.hidden = NO;
        incomingCell.cellDateLabel.hidden = YES;
        [incomingCell setProgressViewWithValue:lastMessage.attachment.value];
        
    }
    else{
        
        incomingCell.cellDateLabel.hidden = NO;
        incomingCell.progressView.hidden = YES;
    }
    
    return incomingCell;
}

-(JSQRainbowRoomIncomingMessagesCollectionViewCell *) roomIncomingCellFor:(JSQMessagesCollectionView *) collectionView atIndexPath:(NSIndexPath *) indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    JSQRainbowRoomIncomingMessagesCollectionViewCell *roomIncomingCell = (JSQRainbowRoomIncomingMessagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier: self.incomingCellIdentifier forIndexPath:indexPath];
    Message *lastMessage = message.lastMessage;
    roomIncomingCell.attachment = lastMessage.attachment;
    _needsAvatar = YES;
    if (_messages.count > 0) {
        GroupedMessages *currentMessageGroup = [_messages objectAtIndex:_messages.count-indexPath.item-1];
        Message * currentMessage = currentMessageGroup.lastMessage;
        if (_messages.count - indexPath.item  >= 2) {
            GroupedMessages *nextMessageGroup = [_messages objectAtIndex:_messages.count - indexPath.item - 2];
            Message * nextMessage = nextMessageGroup.lastMessage;
            if([currentMessage.senderId isEqualToString:nextMessage.senderId] && !currentMessage.callLog && !nextMessage.callLog){
                _needsAvatar = NO;
            }
        }
    }
    [self configureCell:roomIncomingCell forCollectionView:collectionView indexPath:indexPath];
    roomIncomingCell.textView.textColor = [UIColor whiteColor];
    
    [roomIncomingCell setTimestamp: message.lastMessage.date];
    
    
    if (!message.lastMessage.isOutgoing) {
        Peer *peer = message.lastMessage.peer;
        if ([peer isKindOfClass:[Contact class]]) {
            Contact *contact = (Contact *) peer;
            [roomIncomingCell setUserName:contact.displayName];
        }
    }
    
    roomIncomingCell.progressView.tag = indexPath.row;
    
    if (lastMessage.attachment != nil && lastMessage.attachment.value < 1.0 && [_filesDownloading containsObject:roomIncomingCell.attachment]) {
        roomIncomingCell.progressView.hidden = NO;
        roomIncomingCell.cellDateLabel.hidden = YES;
        [roomIncomingCell setProgressViewWithValue:lastMessage.attachment.value];
        
    }
    else{
        roomIncomingCell.progressView.hidden = YES;
        roomIncomingCell.cellDateLabel.hidden = NO;
    }

    return roomIncomingCell;
}


-(void) configureCell:(JSQMessagesCollectionViewCell *) cell forCollectionView:(JSQMessagesCollectionView *) collectionView indexPath:(NSIndexPath *) indexPath {
    id<JSQMessageData> messageItem = [collectionView.dataSource collectionView:collectionView messageDataForItemAtIndexPath:indexPath];
    NSParameterAssert(messageItem != nil);
    BOOL isOutgoingMessage = [self isOutgoingMessage:messageItem];
    BOOL isMediaMessage = [((GroupedMessages*)messageItem) isMediaMessage];
    
    cell.delegate = collectionView;
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    Message *lastMessage = message.lastMessage;
    cell.textView.hidden = NO;
    if ([[messageItem text] isEqualToString:lastMessage.attachment.fileName]) {
        cell.textView.hidden = YES;
        
    }
    cell.textView.text = [messageItem text];
    NSParameterAssert(cell.textView.text != nil);
    
    if (isMediaMessage) {
        id<JSQMessageMediaData> messageMedia = [messageItem media];
        cell.mediaView = [messageMedia mediaView]? :[messageMedia mediaPlaceholderView];
        NSParameterAssert(cell.mediaView != nil);
        NSLog(@"message media is shown %@",messageMedia);
    }
    
    id<JSQMessageBubbleImageDataSource> bubbleImageDataSource = [collectionView.dataSource collectionView:collectionView messageBubbleImageDataForItemAtIndexPath:indexPath];
    cell.messageBubbleImageView.image = [bubbleImageDataSource messageBubbleImage];
    cell.messageBubbleImageView.highlightedImage = [bubbleImageDataSource messageBubbleHighlightedImage];
    
    //BOOL needsAvatar = YES;
    if (isOutgoingMessage && CGSizeEqualToSize(collectionView.collectionViewLayout.outgoingAvatarViewSize, CGSizeZero)) {
        _needsAvatar = NO;
    }
    else if (!isOutgoingMessage && CGSizeEqualToSize(collectionView.collectionViewLayout.incomingAvatarViewSize, CGSizeZero)) {
        _needsAvatar = NO;

    }
    
    id<JSQMessageAvatarImageDataSource> avatarImageDataSource = nil;
    if (_needsAvatar) {
        avatarImageDataSource = [collectionView.dataSource collectionView:collectionView avatarImageDataForItemAtIndexPath:indexPath];
        if (avatarImageDataSource != nil) {
            
            UIImage *avatarImage = [avatarImageDataSource avatarImage];
            if (avatarImage == nil) {
                cell.avatarImageView.image = [avatarImageDataSource avatarPlaceholderImage];
                cell.avatarImageView.highlightedImage = nil;
            }
            else {
                cell.avatarImageView.image = avatarImage;
                cell.avatarImageView.highlightedImage = [avatarImageDataSource avatarHighlightedImage];
            }
        }
    }
    
    cell.cellTopLabel.attributedText = [collectionView.dataSource collectionView:collectionView attributedTextForCellTopLabelAtIndexPath:indexPath];
    cell.messageBubbleTopLabel.attributedText = [collectionView.dataSource collectionView:collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:indexPath];
    cell.cellBottomLabel.attributedText = [collectionView.dataSource collectionView:collectionView attributedTextForCellBottomLabelAtIndexPath:indexPath];
    
    CGFloat bubbleTopLabelInset = (avatarImageDataSource != nil) ? 40.0f : 15.0f;
    
    if (isOutgoingMessage) {
        cell.messageBubbleTopLabel.textInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, bubbleTopLabelInset);
    }
    else {
        cell.messageBubbleTopLabel.textInsets = UIEdgeInsetsMake(0.0f, bubbleTopLabelInset, 0.0f, 0.0f);
    }
    
    cell.textView.dataDetectorTypes = UIDataDetectorTypeAll;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.layer.shouldRasterize = YES;
    

    [self collectionView:collectionView accessibilityForCell:cell indexPath:indexPath message:messageItem];
}


- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    JSQMessagesCollectionViewCell *cell = nil;
    
    // If the message is a group chat event we should build a custom cell for it
    if(message.firstMessage && message.firstMessage.type == MessageTypeGroupChatEvent){
        cell = [self groupChatEventCellFor:collectionView atIndexPath:indexPath message:message.firstMessage];
    } else if(message.firstMessage && message.firstMessage.type == MessageTypeWebRTC){
        cell = [self webRTCEventCellFor:collectionView atIndexPath:indexPath message:message.firstMessage];
    } else if(message.firstMessage && (message.firstMessage.type == MessageTypeCallRecordingStart || message.firstMessage.type == MessageTypeCallRecordingStop)){
        cell = [self callRecordingEventCellFor:collectionView atIndexPath:indexPath message:message.firstMessage];
    } else {
        if ([message.senderId isEqualToString:self.senderId]) {
       
            cell = [self outgoingCellFor:collectionView atIndexPath:indexPath];
        } else {
            if(_conversation.type == ConversationTypeRoom)
                cell = [self roomIncomingCellFor:collectionView atIndexPath:indexPath];
            else
                cell = [self incomingCellFor:collectionView atIndexPath:indexPath];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    
    return cell;
}


#pragma mark - scroll to bottom button
/**
 * Add  to the view and setup the scroll to bottom button
 */
-(void) addScrollToBottomButton {
    [UITools applyCustomFontTo:_scrollToBottomButton.titleLabel];
    _scrollToBottomButton.backgroundColor = [UIColor darkGrayColor];
    _scrollToBottomButton.layer.cornerRadius = _scrollToBottomButton.frame.size.height/2;
    //    self.scrollToBottomButton.translatesAutoresizingMaskIntoConstraints = YES;
    [_scrollToBottomButton setTintColor:[UIColor whiteColor]];
    [_scrollToBottomButton setImage:[UIImage imageNamed:@"scroll_down"] forState:UIControlStateNormal];
    [_scrollToBottomButton setImageEdgeInsets:UIEdgeInsetsMake(4, -10, 4, 0)];
    [self.view addSubview:_scrollToBottomButton];
    _scrollToBottomButton.hidden = YES;
    _scrollToBottomButton.alpha = 0.0;
}

/**
 * Update the frame of the scroll to bottom button
 */
-(void)adjustScrollToBottomButtonFrame {
    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat buttonHeight = self.scrollToBottomButton.frame.size.height;
    CGFloat buttonWidth = self.scrollToBottomButton.frame.size.width;
    CGPoint inputToolbarOrigin = self.inputToolbar.frame.origin;
    self.scrollToBottomButton.frame = CGRectMake(viewWidth/2 - buttonWidth/2, inputToolbarOrigin.y - buttonHeight - 16, buttonWidth, buttonHeight);
}

/**
 * Set the scroll to bottom button label and adjust it size accordingly
 */
-(void)setScrollToBottomButtonLabel:(NSString *)text {
    CGFloat viewWidth = self.view.frame.size.width;
    [self.scrollToBottomButton setTitle:text forState:UIControlStateNormal];
    CGFloat buttonHeight = self.scrollToBottomButton.frame.size.height;
    [UITools sizeButtonToText:self.scrollToBottomButton availableSize:CGSizeMake(viewWidth-16, buttonHeight)  padding:UIEdgeInsetsMake(8, 16, 8, 16)];
    [self adjustScrollToBottomButtonFrame];
}

/**
 * Show/hide the scroll to bottom button depending of the show boolean
 * Add the button to the view if needed
 */
-(void) showScrollToBottomButton:(BOOL)show {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showScrollToBottomButton:show];
        });
        return;
    }
    
    NSString *text = @"";
    
    if(_conversation.unreadMessagesCount > 0){
        if(_conversation.unreadMessagesCount == 1)
            text = NSLocalizedString(@"You have a new message", nil);
        else
            text = [NSString stringWithFormat:NSLocalizedString(@"You have %ld messages", nil), _conversation.unreadMessagesCount];
    } else {
        if(!_allowAutomaticallyScrollsToMostRecentMessage)
            text = NSLocalizedString(@"Jump to the last message", nil);
    }
    
    if(show){
        [self setScrollToBottomButtonLabel:text];
        self.scrollToBottomButton.hidden = NO;
    }
    
    [UIView animateWithDuration:0.5 animations:^(){
        if(show){
            self.scrollToBottomButton.alpha = 1.0;
        } else {
            self.scrollToBottomButton.alpha = 0.0;
        }
    } completion:^(BOOL finished) {
        if(!show){
            
            self.scrollToBottomButton.hidden = YES;
            [self setScrollToBottomButtonLabel:text];
        }
    }];
}

/**
 * UIScrollView delegate called when the scroll view will stopped
 */
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    CGFloat contentHeight = scrollView.contentSize.height;
    CGFloat height = scrollView.frame.size.height;
    CGFloat offsetFromBottom = contentHeight - targetContentOffset->y - height;
    _allowAutomaticallyScrollsToMostRecentMessage = offsetFromBottom >= 40.0 ? NO : YES;
    self.automaticallyScrollsToMostRecentMessage = _allowAutomaticallyScrollsToMostRecentMessage;
    
    [self showScrollToBottomButton:!_allowAutomaticallyScrollsToMostRecentMessage];
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    // Delegate invoked when tapping in the status bar to got to top of the list
    _allowAutomaticallyScrollsToMostRecentMessage = NO;
    self.automaticallyScrollsToMostRecentMessage = _allowAutomaticallyScrollsToMostRecentMessage;
    [self showScrollToBottomButton:YES];
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath {
    // We can only tap on our bubbles.
    if (!_startShowingSharingView) {
        GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
        if (![message.senderId isEqualToString:self.senderId]) {
            return;
        }
        // did we re-tapped the same bubble ?
        if (_tappedIndexPath && [indexPath compare:_tappedIndexPath] == NSOrderedSame) {
            _tappedIndexPath = nil;
        } else {
            _tappedIndexPath = indexPath;
        }
        
        [self reloadCollectionView];
        
        // If we click on the last cell, the bottom-label is not fully visible, so scroll.
        NSIndexPath *lastCell = [NSIndexPath indexPathForItem:([self.collectionView numberOfItemsInSection:0] - 1) inSection:0];
        if ([indexPath compare:lastCell] == NSOrderedSame) {
            [self scrollToBottomAnimated:NO];
        }
    }
    
}

-(void) didShareAttachment:(File *) file atCell:(JSQRainbowMessagesCollectionViewCell *) cell  {
  
    if (!_startShowingSharingView) {
        NSIndexPath *path = [self.collectionView indexPathForCell:cell];
        GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
       _selectedMessage = message.lastMessage;
        
        if([self.inputToolbar.contentView.textView isFirstResponder])
            [self.inputToolbar.contentView.textView resignFirstResponder];
        
        _selectedMedia = file;
        [self setupActionsViewAtCell:cell];
        
        _startShowingSharingView = YES;
        [self.collectionView setScrollEnabled:NO];
        self.inputToolbar.hidden = YES;
        [_scrollToBottomButton setEnabled:NO];

        [UIView animateWithDuration:0.4 animations:^{
            _actionsView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 128 - topPadding, [UIScreen mainScreen].bounds.size.width, 64 + bottomPadding);
           
           
        }];
        
        CGRect frame = self.view.frame;
       // frame.size.height -= _actionsView.frame.size.height; // remove this because at sometime user can see the different btw actionsViewBackgroundView and under the actionsView
        _actionsViewBackgroundView = [[UIView alloc] initWithFrame:frame];
        _actionsViewBackgroundView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [_actionsViewBackgroundView addGestureRecognizer:tap];
        [self.view addSubview:_actionsViewBackgroundView];
    
        if(cell.mediaView){
            CGRect pos = [cell.mediaView convertRect:cell.mediaView.bounds toView:self.view];
            UIView *containerView = [[UIView alloc] initWithFrame:pos];
            UIView *mediaView = [file performSelector:@selector(mediaView)];
            mediaView.tintColor = [file tintColor];
            pos.origin.x = pos.origin.y = 0;
            mediaView.frame = pos;
            containerView.layer.shadowColor = [UIColor blackColor].CGColor;
            containerView.layer.shadowRadius = 4.0f;
            containerView.layer.shadowOpacity = 0.5;
            containerView.layer.shadowOffset = CGSizeMake(0, 3);
            containerView.tag = [file.tag longLongValue];
            [containerView addSubview:mediaView];
            [_actionsViewBackgroundView addSubview:containerView];
        }
        
        
         [self.view bringSubviewToFront:_actionsView];
    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self dismissActionView];
}

- (void) hideFileThumbmailForDeletedFile {
    UIView *thumbnailImageView = [_actionsViewBackgroundView viewWithTag:[_selectedMedia.tag longLongValue]];
    thumbnailImageView = nil;
    [thumbnailImageView removeFromSuperview];
}

- (void) dismissActionView {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissActionView];
        });
        return;
    }
   
    [_scrollToBottomButton setEnabled:YES];
    _startShowingSharingView = NO;
    [self.collectionView setScrollEnabled:YES];
    [_actionsViewBackgroundView removeFromSuperview];
    _actionsViewBackgroundView = nil;
    
    [UIView animateWithDuration:0.2 animations:^{
        _actionsView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - topPadding, [UIScreen mainScreen].bounds.size.width, 64);
    }];
    
    self.inputToolbar.hidden = NO;
}
-(void) didFailedSendAttachment:(File *) file atCell:(JSQRainbowMessagesCollectionViewCell *) cell  {
    if ([cell.status.image isEqual:[UIImage imageNamed:@"message_ko"]]) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Your message was not sent.", nil) message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *sendAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Try Again",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction __unused *action) {
            NSIndexPath *path = [self.collectionView indexPathForCell:cell];
            GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
            Message *lastMessage = message.lastMessage;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.inputToolbar.contentView.leftBarButtonItem setEnabled:YES];
                if ([file.tag  isEqualToString: lastMessage.messageID]) {
                    [_messages removeObject:message];
                    [self reloadCollectionView];
                }
            });
            
            [_conversationsManagerService reSendMessage:lastMessage to:_conversation completionHandler:^(Message *message, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.inputToolbar.contentView.leftBarButtonItem setEnabled:YES];
                    if (!error) {
                        [self reloadCollectionView];
                    }
                });
                
            } attachmentUploadProgressHandler:^(Message *message, double totalBytesSent, double totalBytesExpectedToSend) {
                
            }];
        }];
        
        UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Delete",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction __unused *action) {
            NSIndexPath *path = [self.collectionView indexPathForCell:cell];
            GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
            Message *lastMessage = message.lastMessage;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.inputToolbar.contentView.leftBarButtonItem setEnabled:YES];
                if ([file.tag  isEqualToString: lastMessage.messageID]) {
                    [_messages removeObject:message];
                    [self reloadCollectionView];
                }
            });
           
            [_conversationsManagerService deleteFailedMessage:lastMessage to:_conversation];
        }];
       
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction __unused *action) {
        }];
        
        [alertController addAction:sendAction];
        [alertController addAction:deleteAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


-(void) didLongPressOnImageForAttachment:(File *) file atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    NSIndexPath *path = [self.collectionView indexPathForCell:cell];
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
    Message *lastMessage = message.lastMessage;
    if (lastMessage.state == MessageDeliveryStateSent || lastMessage.state == MessageDeliveryStateFailed) {
        [self didFailedSendAttachment:file atCell:cell];
    }
    else{
        [self didShareAttachment:file atCell:cell];
    }
    
}

-(void) didTapOnImageForAttachment:(File *) file atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"chat" action:@"show file" label:nil value:nil];
    if (file.isDownloadAvailable) {
        if(!file.data){
            NSIndexPath *path = [self.collectionView indexPathForCell:cell];
            GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
            Message *lastMessage = message.lastMessage;
            file.tag = lastMessage.messageID;
            file.value = 0.01;
            [_filesDownloading addObject:file];
            __weak typeof (self) weakSelf = self;
            [[ServicesManager sharedInstance].fileSharingService downloadDataForFile:file withCompletionHandler:^(File *aFile, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if(!error){
                        [_filesDownloading removeObject:file];
                        [self reloadCollectionView];
                        [weakSelf openPreviewControllerForAttachment:aFile];
                    } else {
                        MBProgressHUD *errorHud = [UITools showHUDWithMessage:NSLocalizedString(@"Error",nil) forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Failed" dismissCompletionBlock:nil];
                        [errorHud showAnimated:YES whileExecutingBlock:^{
                            [NSThread sleepForTimeInterval:3];
                        } completionBlock:^{
                            [_filesDownloading removeObject:file];
                            [self reloadCollectionView];
                        }];
                    }
                });
            }];
        } else {
            [self reloadCollectionView];
            [self openPreviewControllerForAttachment:file];
        }
    }
}

-(void) didTapImageForAttachment:(File *) file atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"chat" action:@"show file" label:nil value:nil];
    if(!file.data){
        // show progress
        NSIndexPath *path = [self.collectionView indexPathForCell:cell];
        GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
        Message *lastMessage = message.lastMessage;
        file.tag = lastMessage.messageID;
        file.value = 0.01;
        [_filesDownloading addObject:file];
        //__weak typeof (self) weakSelf = self;
        [[ServicesManager sharedInstance].fileSharingService downloadDataForFile:file withCompletionHandler:^(File *aFile, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(!error){
                    [_filesDownloading removeObject:file];
                    [self reloadCollectionView];
                    //[weakSelf openPreviewControllerForAttachment:aFile];
                } else {
                    MBProgressHUD *errorHud = [UITools showHUDWithMessage:NSLocalizedString(@"Error",nil) forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Failed" dismissCompletionBlock:nil];
                    [errorHud showAnimated:YES whileExecutingBlock:^{
                        [NSThread sleepForTimeInterval:3];
                    } completionBlock:^{
                        [_filesDownloading removeObject:file];
                        [self reloadCollectionView];
                    }];
                }
            });
        }];
    } else {
        // open preview first then download image
        [self reloadCollectionView];
        [self openPreviewControllerForAttachment:file];
    }
}

-(void) reloadCollectionView {
    if([self isViewLoaded]){
        [self.collectionView.collectionViewLayout invalidateLayoutWithContext:[JSQMessagesCollectionViewFlowLayoutInvalidationContext context]];
        [self.collectionView reloadData];
    }
}

-(void) openPreviewControllerForAttachment:(File *) attachment {
    // Instanciate a QLPreview​Controller
//    MBProgressHUD *openingHud = [UITools showHUDWithMessage:NSLocalizedString(@"Opening file ...",nil) forView:self.view mode:MBProgressHUDModeIndeterminate imageName:nil dismissCompletionBlock:nil];
//    [openingHud show:YES];
    UIPreviewQuickLookController *previewController = [UIPreviewQuickLookController new];
    previewController.attachment = attachment;
    [self presentViewController:previewController animated:YES completion:^{
       // [openingHud hide:YES];
    }];
}

#pragma mark - Browsing delegate

-(void) itemsBrowser:(CKItemsBrowser*)browser didAddCacheItems:(NSArray*)newItems atIndexes:(NSIndexSet*)indexes {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self itemsBrowser:browser didAddCacheItems:newItems atIndexes:indexes];
        });
        return;
    }
    NSLog(@"[didAddCacheItems] number of new Items: %ld", newItems.count);
    NSLog(@"%@ %@", NSStringFromSelector(_cmd), newItems);
    NSUInteger oldLoadedMessagesCount = [_messages count];
    for (Message *message in newItems) {
        
        if(message.type == MessageTypeGroupChatEvent){
            // for each message check if we have an avatar in our list, we don't have them for event in case we invite some one and we remove it imediately.
            if([message.groupChatEventPeer isKindOfClass:[Contact class]]){
                [self setAvatarForContact:(Contact *)message.groupChatEventPeer];
            }
            
            if (message.isDisplayable == NO)
                continue;
            
        }
        
        // Each one on these new messages could be at a different index
        // Which could be anywhere inside our current grouped messages.
        // For example we could have a new message from A which should appear between 2 messages (already grouped) from B.
        // Then we have to split the grouped message from B in 2 grouped messages and add the message from A in
        // the middle.
        Message *previousMessage = nil;
        GroupedMessages *previousGroupedMessages = nil;
        Message *nextMessage = nil;
        GroupedMessages *nextGroupedMessages = nil;
        
        // We have to enumerate in reverse order because in _messages, the first item is the most recent one.
        for (GroupedMessages *groupedMessages in [_messages reverseObjectEnumerator]) {
            for (Message *msg in groupedMessages.allGroupedMessages) {
                // Dont forget to handle the equal case !
                if ([msg.date compare:message.date] == NSOrderedAscending || [msg.date compare:message.date] == NSOrderedSame) {
                    previousMessage = msg;
                    previousGroupedMessages = groupedMessages;
                }
                if ([msg.date compare:message.date] == NSOrderedDescending && !nextMessage) {
                    nextMessage = msg;
                    nextGroupedMessages = groupedMessages;
                }
            }
        }
        
        if (previousMessage) {
            if (nextMessage) {
                // we arrive here when we have a previous AND a next message.
                // This new message is somewhere in the middle of our list.
                // This is the most complicated case, we might have to un-merge already grouped messages, etc.
                
                // First case is when the previous and next are in the same groupedmessage
                if (previousGroupedMessages == nextGroupedMessages) {
                    // We can add the new message to the existing grouped if same peer, etc.
                    if ([self shouldGroupMessage:previousGroupedMessages.lastMessage with:message]) {
                        [previousGroupedMessages addMessage:message];
                    } else {
                        // HERE we have to un-merge the grouped message and add a new between.
                        GroupedMessages *previousGroup = [[GroupedMessages alloc] initWithMessage:previousGroupedMessages.firstMessage];
                        GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                        GroupedMessages *nextGroup = [[GroupedMessages alloc] initWithMessage:nextMessage];
                        
                        for (Message *m in [previousGroupedMessages allGroupedMessages]) {
                            // Dont forget the equal case !
                            if ([m.date compare:message.date] == NSOrderedAscending || [m.date compare:message.date] == NSOrderedSame) {
                                [previousGroup addMessage:m];
                            }
                            if ([m.date compare:message.date] == NSOrderedDescending) {
                                [nextGroup addMessage:m];
                            }
                        }
                        [_messages removeObject:previousGroupedMessages];
                        [self insertMessageAtProperIndex:previousGroup];
                        [self insertMessageAtProperIndex:newGroup];
                        [self insertMessageAtProperIndex:nextGroup];
                    }
                } else {
                    // prev and next are not the same grouped, then its easy, we can have 3 cases :
                    // put the new message in the previous, in the next, or create a new one.
                    if ([self shouldGroupMessage:previousGroupedMessages.lastMessage with:message]) {
                        [previousGroupedMessages addMessage:message];
                    } else if ([self shouldGroupMessage:nextGroupedMessages.lastMessage with:message]) {
                        [nextGroupedMessages addMessage:message];
                    } else {
                        // create a new grouped message
                        GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                        [self insertMessageAtProperIndex:newGroup];
                    }
                }
                
            } else {
                // This is the most common case, we have a previous but no next
                // This is a new message at the bottom of the list.
                if ([self shouldGroupMessage:previousGroupedMessages.firstMessage with:message]) {
                    [previousGroupedMessages addMessage:message];
                } else {
                    // create a new grouped message
                    GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                    [self insertMessageAtProperIndex:newGroup];
                }
            }
        } else {
            if (nextMessage) {
                // We have no previous message but a next message
                // This message goes to the top of the list (it is the new oldest message)
                if ([self shouldGroupMessage:nextGroupedMessages.firstMessage with:message]) {
                    [nextGroupedMessages addMessage:message];
                } else {
                    // create a new grouped message
                    GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                    [self insertMessageAtProperIndex:newGroup];
                }
            } else {
                // No previous and no next message ?
                // This is the first message of the list.
                GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                [self insertMessageAtProperIndex:newGroup];
            }
        }
    }
    NSInteger newLoadedMessagesCount = [_messages count] - oldLoadedMessagesCount;
    
    [self reloadCollectionView];

    if(_isRetrievingNextPage && newLoadedMessagesCount>0 && !self.automaticallyScrollsToMostRecentMessage){
        NSInteger topMessageIndex = newLoadedMessagesCount>1 ? newLoadedMessagesCount-1 : newLoadedMessagesCount;

        NSIndexPath *cellIdx = [NSIndexPath indexPathForItem:topMessageIndex inSection:0];
        [self scrollToIndexPath:cellIdx animated:NO];
    } else {
        if(self.automaticallyScrollsToMostRecentMessage)
            [self scrollToBottomAnimated:NO];
    }

    if(!self.automaticallyScrollsToMostRecentMessage){
        self.automaticallyScrollsToMostRecentMessage = self.allowAutomaticallyScrollsToMostRecentMessage;
        // TODO : We need to display the jump to bottom button

    }
    
    // We must update the more button status in case of changes of one of his conditions
    _moreMenuButton.enabled = [self willShowSomethingInMoreMenu];

    
}

-(void) itemsBrowser:(CKItemsBrowser*)browser didRemoveCacheItems:(NSArray*)removedItems atIndexes:(NSIndexSet*)indexes {
    if(browser != _messagesBrowser)
        return;
    
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self itemsBrowser:browser didRemoveCacheItems:removedItems atIndexes:indexes];
        });
        return;
    }
    
    NSLog(@"%@ %@", NSStringFromSelector(_cmd), removedItems);
    
    for (Message *removeMessage in removedItems) {
        NSInteger idx = [self indexPathOfGroupedMessageForMessage:removeMessage];
        if(idx != NSNotFound){
            GroupedMessages *grpMsgs = [_messages objectAtIndex:idx];
            [grpMsgs removeMessage:removeMessage];
            if(grpMsgs.allGroupedMessages.count == 0)
                [_messages removeObject:grpMsgs];
        }
    }
    
    [self finishReceivingMessage];
    [self setShowLoadEarlierMessagesHeader:NO];
    
    // We must update the more button status in case of changes of one of his conditions
    _moreMenuButton.enabled = [self willShowSomethingInMoreMenu];
}

-(void) itemsBrowser:(CKItemsBrowser*)browser didUpdateCacheItems:(NSArray*)changedItems atIndexes:(NSIndexSet*)indexes {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self itemsBrowser:browser didUpdateCacheItems:changedItems atIndexes:indexes];
        });
        return;
    }
    NSLog(@"%@ %@", NSStringFromSelector(_cmd), changedItems);

    
    for (Message *message in changedItems) {
        NSString* messageStatus = [Message stringForDeliveryState:message.state];
        NSLog(@"messageStatus %@", messageStatus);
        NSLog(@"Treating notification update %@", [Message stringForDeliveryState:message.state]);
        if(message.state == MessageDeliveryStateReceived){
            if(![message attachment]) {
                // send to firebase when messages Received 
                [FIRAnalytics logEventWithName:@"recieve_IM"
                                    parameters:@{
                                                 @"action" :@"recieve_text_message",
                                                 @"category":@"chat_screen"
                                                 }];
            }
            else {
                NSLog(@"recieveMsg %@",[NSString stringWithFormat:@"recieve_%@_message",[self getSelectedFileType:[message attachment]]]);
                [FIRAnalytics logEventWithName:@"recieve_IM"
                                    parameters:@{
                                       @"action" :[NSString stringWithFormat:@"recieve_%@_message",[self getSelectedFileType:[message attachment]]],
                                       @"category":@"chat_screen"
                                                 }];
            }
            
        }
        
    }

    [self finishReceivingMessage];
}

-(void) shouldUpdateAttachment:(NSNotification *) notification {
    
    dispatch_async(dispatch_get_main_queue(), ^{
          [self reloadCollectionView];
    });
}

-(void) itemsBrowser:(CKItemsBrowser*)browser didReorderCacheItemsAtIndexes:(NSArray*)oldIndexes toIndexes:(NSArray*)newIndexes {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self itemsBrowser:browser didReorderCacheItemsAtIndexes:oldIndexes toIndexes:newIndexes];
        });
        return;
    }
    
    NSLog(@"%@", NSStringFromSelector(_cmd));
    [self finishReceivingMessage];
}

-(void) itemsBrowser:(CKItemsBrowser *)browser didReceiveItemsAddedEvent:(NSArray *)addedItems {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self itemsBrowser:browser didReceiveItemsAddedEvent:addedItems];
        });
        return;
    }
    
    NSLog(@"[UI-IM] DID RECEIVE ITEM ADDED EVENT %@", addedItems);
    Message *message = (Message*)addedItems.firstObject;
    // This message is for this conversation
    NSLog(@"Treating notification %@", [Message stringForDeliveryState:message.state]);
    
    //Added when receive new message while conversation is opened
    if(message.state == MessageDeliveryStateReceived){
        if(![message attachment]) {
            // send to firebase Received actions
            [FIRAnalytics logEventWithName:@"recieve_IM"
                                parameters:@{
                                             @"action" :@"recieve_text_message",
                                             @"category":@"chat_screen"
                                             }];
        }
        else {
            NSLog(@"recieveMsg %@",[NSString stringWithFormat:@"recieve_%@_message",[self getSelectedFileType:[message attachment]]]);
            [FIRAnalytics logEventWithName:@"recieve_IM"
                                parameters:@{
                                             @"action" :[NSString stringWithFormat:@"recieve_%@_message",[self getSelectedFileType:[message attachment]]],
                                             @"category":@"chat_screen"
                                             }];
        }
    }
    
    // The notification (in case of background) or sound-alert is handled by the ConversationTableViewController
    [self finishReceivingMessageAnimated:YES];
}

#pragma mark - composing notification

-(void) didReceiveComposingMessage:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReceiveComposingMessage:notification];
        });
        return;
    }
    
    Message *theMsg = (Message*) notification.object;
    if([theMsg.via isEqual:_conversation.peer]) {
        Contact *contact = (Contact *) theMsg.peer;
        if (theMsg.isComposing) {
            if (![_typingContacts containsObject:contact])
                [_typingContacts addObject:contact];
        } else {
            [_typingContacts removeObject:contact];
        }
        
        self.showTypingIndicator = [_typingContacts count] > 0;
        
        [self reloadCollectionView];
        if(self.allowAutomaticallyScrollsToMostRecentMessage){
            [self scrollToBottomAnimated:YES];
        }
    }
}
#pragma mark - room notification

-(void) didRemoveRoom:(NSNotification *) notification {
    Room *theRoom = (Room *) notification.object;
    Room *currentRoom = (Room *) _conversation.peer;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (isRemoveRoom && [theRoom isEqual:currentRoom]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    });
   
}
-(void) didFailRemoveRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didFailRemoveRoom:notification];
        });
        return;
    }
    Room *theRoom = (Room *) notification.object;
    Room *currentRoom = (Room *) _conversation.peer;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (isRemoveRoom && [theRoom isEqual:currentRoom]) {
            NSString *errorMessage = [NSString stringWithFormat:NSLocalizedString(@"Failed to remove %@ 's bubble",nil),[theRoom displayName]];
            [UITools showHUDWithMessage:errorMessage forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Failed" dismissCompletionBlock:nil];
        }
        
    });
}
#pragma mark - Grouped messages
-(BOOL) isMessageHasAttachment:(Message *) msg{
    if (msg.body != nil) {
        if (([msg.body rangeOfString:@"IMG_"].location != NSNotFound && [msg.body rangeOfString:@"."].location != NSNotFound) || [msg.body isEqualToString:@""]) {
            return YES;
        }
        return NO;
    }
    return YES;
}
-(BOOL) shouldGroupMessage:(Message *) message1 with:(Message *) message2 {
    return NO;
}

-(NSInteger) indexPathOfGroupedMessageForMessage:(Message *) message {
    __block NSInteger index = NSNotFound;
    [_messages enumerateObjectsUsingBlock:^(GroupedMessages * groupedMessages, NSUInteger idx, BOOL * stop) {
        if([groupedMessages.allGroupedMessages containsObject:message]){
            index = idx;
            *stop = YES;
        }
    }];
    return index;
}

-(BOOL)isTheNextMessageHasSameSender:(NSIndexPath*)currentIndex {
    if (_messages.count > 0) {
        GroupedMessages *currentMessageGroup = [_messages objectAtIndex:_messages.count-currentIndex.item-1];
        Message * currentMessage = currentMessageGroup.lastMessage;
        if (!currentMessage.senderId)
            return NO;
        if (_messages.count - currentIndex.item  >= 2) {
            GroupedMessages *nextMessageGroup = [_messages objectAtIndex:_messages.count - currentIndex.item - 2];
            Message *nextMessage = nextMessageGroup.lastMessage;
            
            if(nextMessage.senderId && [currentMessage.senderId isEqualToString:nextMessage.senderId] && !currentMessage.callLog && !nextMessage.callLog && (currentMessage.type == MessageTypeChat || currentMessage.type == MessageTypeGroupChat)){
                return YES;
            }
        }
    }
    return NO;
}
-(BOOL)isThePrevoiusMessageHasSameSender:(NSIndexPath*)currentIndex {

    if (currentIndex.item > 0 && _messages.count > 1) {
        GroupedMessages *currentMessageGroup = [_messages objectAtIndex:[_messages count]-currentIndex.item-1];
        Message * currentMessage = currentMessageGroup.lastMessage;
        if (!currentMessage.senderId)
            return NO;
        if (_messages.count >= currentIndex.item) {
            GroupedMessages *nextMessageGroup = [_messages objectAtIndex:[_messages count]-currentIndex.item];
            Message * nextMessage = nextMessageGroup.lastMessage;
            if(nextMessage.senderId && [currentMessage.senderId isEqualToString:nextMessage.senderId] && !currentMessage.callLog && !nextMessage.callLog && (currentMessage.type == MessageTypeChat || currentMessage.type == MessageTypeGroupChat)){
                return YES;
            }
        }
    }
    return NO;
}
- (IBAction)didTapTitleButton:(UIButton *)sender {
    [self parentViewController].title = @" ";
    if(_conversation.type == ConversationTypeRoom) {
        // Open room details
        UIRoomDetailsViewController *roomDetailsViewController = [[UIStoryboardManager sharedInstance].roomDetailsStoryBoard instantiateViewControllerWithIdentifier:@"roomDetailsViewControllerID"];
        roomDetailsViewController.room = (Room *)_conversation.peer;
        roomDetailsViewController.fromView = self;
        if(_parentNavigationController){
            [_parentNavigationController pushViewController:roomDetailsViewController animated:YES];
        } else {
            [self.navigationController pushViewController:roomDetailsViewController animated:YES];
        }
    } else {
        // Open contact details
        UIContactDetailsViewController* contactDetailsViewController = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
        contactDetailsViewController.contact = (Contact *) _conversation.peer;
        contactDetailsViewController.fromView = self;
        [self.navigationController pushViewController:contactDetailsViewController animated:YES];
    }
}

#pragma mark - 3D Touch actions

- (NSArray<id<UIPreviewActionItem>> *)previewActionItems {
    UIPreviewAction *muteAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Silence", nil) style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [_conversationsManagerService muteConversation:_conversation];
    }];
    
    UIPreviewAction *unmuteAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Alert", nil) style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [_conversationsManagerService unmuteConversation:_conversation];
    }];
    
    UIPreviewAction *closeAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Close",nil) style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [_conversationsManagerService stopConversation:_conversation];
    }];
    
    if(_conversation.isMuted)
        return @[unmuteAction, closeAction];
    else
        return @[muteAction, closeAction];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSUInteger newLength = (textView.text.length - range.length) + text.length;
    if(newLength <= kMaxTextLength) {
        return YES;
    } else {
        NSUInteger emptySpace = kMaxTextLength - (textView.text.length - range.length);
        textView.text = [[[textView.text substringToIndex:range.location]
                          stringByAppendingString:[text substringToIndex:emptySpace]]
                         stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
        return NO;
    }
}

#pragma mark - Scroll to Bottom button actions
- (IBAction)didTapScrollToBottomButton:(UIButton *)sender {
    self.allowAutomaticallyScrollsToMostRecentMessage = YES;
    self.automaticallyScrollsToMostRecentMessage = YES;
    [self showScrollToBottomButton:NO];
    [self scrollToBottomAnimated:YES];
}

#pragma mark - Keyboard show/hide notifications
- (void)keyboardDidShow: (NSNotification *) notif{
    [self adjustScrollToBottomButtonFrame];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    [self adjustScrollToBottomButtonFrame];
}

#pragma mark - Call WebRTC buttons

-(void) showHideCallButton {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showHideCallButton];
        });
        return;
    }
    NSMutableArray <UIBarButtonItem*> *buttons = [NSMutableArray array];
    [buttons addObject:_moreMenuButton];
    
    if(_conversation.type == ConversationTypeUser){
        [_callWebRTCButton setImageInsets:UIEdgeInsetsMake(0, 0, 0, -15)];
        [_moreMenuButton setImageInsets:UIEdgeInsetsMake(0, -15, 0, 0)];
        [buttons addObject:_callWebRTCButton];
        
    } else if(_conversation.type == ConversationTypeRoom){
        Room *room = (Room *)_conversation.peer;
        if(room.canJoin){
            [_joinConferenceButton setImageInsets:UIEdgeInsetsMake(0, 0, 0, -15)];
            [_moreMenuButton setImageInsets:UIEdgeInsetsMake(0, -15, 0, 0)];
            _joinConferenceButton.enabled = YES;
            [buttons addObject:_joinConferenceButton];
        }
    }
    UINavigationItem *navItem = [self goodNavigationItem];
    navItem.rightBarButtonItems = buttons;
}

- (IBAction)callWebRTCButtonClicked:(UIBarButtonItem *)sender {
    if (isNetworkConnected) {
        UIAlertController *ctrl = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        BOOL isAllowedToUseWebRTCMobile = ((Contact *)_conversation.peer).canCallInWebRTC;
        
        if (isAllowedToUseWebRTCMobile) {
            UIAlertAction *callVideoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"RTC video call", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makeCallTo:(Contact *)_conversation.peer features:(RTCCallFeatureAudio|RTCCallFeatureLocalVideo)];
               // [FIRAnalytics logEventWithName:@"start_webRTC_Call" parameters:@{@"typeOfCall":@"RTC video call"}];

            }];
            callVideoAction.enabled = ((Contact*)_conversation.peer).canCallInWebRTCVideo;
            
            [callVideoAction setValue:[[UIImage imageNamed:@"video"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
            [ctrl addAction:callVideoAction];
            
            UIAlertAction *callAudioAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"RTC audio call", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //[FIRAnalytics logEventWithName:@"start_webRTC_Call" parameters:@{@"typeOfCall":@"RTC audio call"}];
                [self makeCallTo:(Contact *)_conversation.peer features:RTCCallFeatureAudio];
            }];
            
            [callAudioAction setValue:[[UIImage imageNamed:@"headset"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
            [ctrl addAction:callAudioAction];
        }
        
        // Contact (caller or callee) phone numbers
        Contact *contact = (Contact*)_conversation.peer;
        [contact.phoneNumbers enumerateObjectsUsingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
            NSString *label = aPhoneNumber.label;
            if (aPhoneNumber.deviceType == PhoneNumberDeviceTypeMobile){
                switch (aPhoneNumber.type) {
                    case PhoneNumberTypeHome:
                        label = NSLocalizedString(@"Personal Mobile", nil);
                        break;
                    case PhoneNumberTypeWork:
                        label = NSLocalizedString(@"Professional Mobile", nil);
                        break;
                    case PhoneNumberTypeOther:
                        label = NSLocalizedString(@"Professional Mobile", nil);
                        break;
                    default:
                        label = NSLocalizedString(@"Cell", nil);
                        break;
                }
            } else if (aPhoneNumber.deviceType == PhoneNumberDeviceTypeLandline){
                switch (aPhoneNumber.type) {
                    case PhoneNumberTypeHome:
                        label = NSLocalizedString(@"Personal", nil);
                        break;
                    case PhoneNumberTypeWork:
                        label = NSLocalizedString(@"Professional", nil);
                        break;
                    default:
                        label = NSLocalizedString(@"Work", nil);
                        break;
                }
            } else {
                label = NSLocalizedString(aPhoneNumber.label, nil);
            }
            
            UIAlertAction *callAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@ : %@",label, aPhoneNumber.number] style:UIAlertActionStyleDefault handler: ^(UIAlertAction * action) {
                //[FIRAnalytics logEventWithName:@"start_normal_Call" parameters:@{@"typeOfCall":@"phone call"}];

                [UITools makeCallToPhoneNumber:aPhoneNumber inController:self];
            }];
            
            [ctrl addAction:callAction];
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        [ctrl addAction:cancel];
        
        [ctrl.view setTintColor:[UITools defaultTintColor]];
        if (ctrl.actions.count > 1) {
            [self presentViewController:ctrl animated:YES completion:nil];
        }
    }
    else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"You need to be connected to your network to make calls" message:@"Can't Make Call" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok =[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
   
}

-(void) makeCallTo:(Contact *) contact features:(RTCCallFeatureFlags) features {
    if([ServicesManager sharedInstance].rtcService.microphoneAccessGranted){
        [[ServicesManager sharedInstance].rtcService beginNewOutgoingCallWithPeer:contact withFeatures:features];

    } else {
        [UITools showMicrophoneBlockedPopupInController:self];
    }
}

#pragma mark - join conference button
- (IBAction)joinConferenceButtonClicked:(UIBarButtonItem *)sender {
    [self didSelectPhoneNumber:nil callBack:NO];
}

#pragma mark - Add attachment to messages

#pragma mark - JSQMessages left accessory button (upload files)
- (void)showInputBarLeftAccessoryButtonMenu {
    if (_leftAccessoryButtonActionSheet) {
        [_leftAccessoryButtonActionSheet dismissViewControllerAnimated:NO completion:nil];
        _leftAccessoryButtonActionSheet = nil;
    }
    
    _leftAccessoryButtonActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    __weak __typeof__(self) weakSelf = self;
    
    UIAlertAction *iCloudAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"iCloud Drive", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [weakSelf didTapICloudDrive:action];
    }];
    [iCloudAction setValue:[[UIImage imageNamed:@"iCloud"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    
    [_leftAccessoryButtonActionSheet addAction:iCloudAction];
    
    UIAlertAction *myFilesFromServerAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"My Rainbow share", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [weakSelf didTapMyFilesAction:action];
    }];
    [myFilesFromServerAction setValue:[UIImage imageNamed:@"folderRainbow"] forKey:@"image"];
    
    [_leftAccessoryButtonActionSheet addAction:myFilesFromServerAction];
    
    UIAlertAction* uploadImageFromLibraryAction = [UIAlertAction actionWithTitle: NSLocalizedString(@"Gallery", @"")  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [weakSelf didTapUploadImageAction:action withSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }];
    [uploadImageFromLibraryAction setValue:[[UIImage imageNamed:@"folder"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    [_leftAccessoryButtonActionSheet addAction:uploadImageFromLibraryAction];
    
    UIAlertAction* uploadImageFromCameraAction = [UIAlertAction actionWithTitle: NSLocalizedString(@"Camera", nil)  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [weakSelf didTapUploadImageAction:action withSource:UIImagePickerControllerSourceTypeCamera];
    }];
    [uploadImageFromCameraAction setValue:[[UIImage imageNamed:@"camera"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
#if TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR
    [_leftAccessoryButtonActionSheet addAction:uploadImageFromCameraAction];
#endif
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [_leftAccessoryButtonActionSheet addAction:cancelAction];
    
    // show the menu.
    [_leftAccessoryButtonActionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:_leftAccessoryButtonActionSheet animated:YES completion:nil];
}

- (void)didPressAccessoryButton:(UIButton *)sender{
    [self showInputBarLeftAccessoryButtonMenu];
}

-(void) showAllowAccessPhotoAndCameraPopup {
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    [alert addButton:NSLocalizedString(@"Go to Settings", nil) actionBlock:^(void) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    alert.customViewColor = [UITools defaultTintColor];
    alert.iconTintColor = [UITools defaultBackgroundColor];
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = YES;
    
    [alert showInfo:self
              title:NSLocalizedString(@"Access photos", nil)
           subTitle:NSLocalizedString(@"Please allow access to photos and camera from your device settings", nil)
   closeButtonTitle:NSLocalizedString(@"Close", nil)
           duration:0.0f];
}

#pragma mark - left menu actions
-(void) didTapMyFilesAction:(UIAlertAction *) sender {
    UINavigationController *viewController = (UINavigationController*) [[UIStoryboardManager sharedInstance].myFilesStoryBoard instantiateViewControllerWithIdentifier:@"myFilesOnServerNavControllerID"];
    viewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)didTapUploadImageAction:(UIAlertAction *)sender withSource:(UIImagePickerControllerSourceType)source {
    
    if (source == UIImagePickerControllerSourceTypeCamera) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (granted) {
                    // Already authorized
                    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                    imagePickerController.delegate = self;
                    imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                    imagePickerController.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeImage, nil];
                    [self presentViewController:imagePickerController animated:YES completion:nil];
                    
                } else {
                    [self showAllowAccessPhotoAndCameraPopup];
                }
            });
        }];
        
    } else {
        // Else photo access
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(status == PHAuthorizationStatusAuthorized) {
                    // Already authorized
                    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                    imagePickerController.delegate = self;
                    imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                    if (source == UIImagePickerControllerSourceTypePhotoLibrary && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
                        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    else if(source == UIImagePickerControllerSourceTypePhotoLibrary)
                        imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                    
                    imagePickerController.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeImage, nil];
                    
                    [self presentViewController:imagePickerController animated:YES completion:nil];
                    
                } else {
                    [self showAllowAccessPhotoAndCameraPopup];
                }
            });
        }];
    }
}

-(void) didTapICloudDrive:(UIAlertAction *) sender {
    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.data"] inMode:UIDocumentPickerModeImport];
    documentPicker.delegate = self;
    
    documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
    documentPicker.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self presentViewController:documentPicker animated:YES completion:^{
        if (@available(iOS 11, *)) {
            // iOS 11 (or newer)
            NSObject<UIAppearance> *appearance = [UINavigationBar appearance];
            self.previousNavBarTintColor = [(UINavigationBar *)appearance tintColor];
            _previousTitleTextAttributesColor = [(UINavigationBar *)appearance titleTextAttributes];
            [(UINavigationBar *)appearance setTintColor:[UITools defaultTintColor]];
        }
    }];
}

#pragma mark - UIImagePickerControllerDelegate protocol
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    
    NSURL *assetURL;
    if([info objectForKey:UIImagePickerControllerReferenceURL])
        assetURL = info[UIImagePickerControllerReferenceURL];
    else if([info objectForKey:UIImagePickerControllerMediaURL])
        assetURL = info[UIImagePickerControllerMediaURL];
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    PHFetchResult<PHAsset *> *asset = nil;
    
    if([picker sourceType] == UIImagePickerControllerSourceTypeCamera) {
        __block NSString *assetLocalIdentifier;
        [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
            PHAssetChangeRequest *assetChangeRequest = nil;
            if([mediaType isEqualToString:(NSString *)kUTTypeVideo] || [mediaType isEqualToString:(NSString *)kUTTypeMovie]){
                assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:assetURL];
            } else {
                UIImage *pickedImage = info[UIImagePickerControllerOriginalImage];
                assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:pickedImage];
            }
            
            NSLog(@"PlaceHolder %@",assetChangeRequest.placeholderForCreatedAsset);
            assetLocalIdentifier = assetChangeRequest.placeholderForCreatedAsset.localIdentifier;
            
        } error:nil];
        
        if (assetLocalIdentifier)
            asset = [PHAsset fetchAssetsWithLocalIdentifiers:@[assetLocalIdentifier] options:nil];
    } else {
        if(assetURL){
            asset = [PHAsset fetchAssetsWithALAssetURLs:@[assetURL] options:nil];
        }
    }

    
    __block NSString *fileName = nil;
    if(asset){
        fileName = [asset.firstObject valueForKey:@"filename"];
    }
    
    __block NSData *dataToSend = nil;
    __block NSURL * cacheURL = nil;
    if(asset.count > 0){
        NSURL *cache = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",[UITools applicationDocumentsCache], fileName]];
        cacheURL = cache;
        //TODO: add loader , here we are blocking the UI
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        [self downloadAsset:asset.firstObject toURL:cache completion:^{
            dataToSend = [NSData dataWithContentsOfURL:cache];
            dispatch_semaphore_signal(semaphore);
        }];
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    }

    [picker dismissViewControllerAnimated:YES completion:^{
        // If no filename or image has been converted - redefine the name
        if(fileName.length==0 || ![[dataToSend extension] isEqualToString:[assetURL pathExtension]]){
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"ddMMyyyy_HHmm"];
            fileName = [NSString stringWithFormat:@"IMG_%@.%@", [dateFormat stringFromDate:[NSDate date]], [dataToSend extension]];
        }

        _attachmentFileToSend = [[ServicesManager sharedInstance].fileSharingService createTemporaryFileWithFileName:fileName andData:dataToSend andURL:cacheURL];

        if(asset && _attachmentFileToSend.type == FileTypeVideo)
            _attachmentPreview = [self generateThumbnailFromAvAsset:asset.firstObject];
        
        [self showAttachmentView];
    }];
}

-(UIImage *) generateThumbnailFromAvAsset:(PHAsset *) videoAsset {
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    PHVideoRequestOptions *option = [PHVideoRequestOptions new];
    __block AVAsset *resultAsset;
    [[PHImageManager defaultManager] requestAVAssetForVideo:videoAsset options:option resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
        resultAsset = asset;
        dispatch_semaphore_signal(semaphore);
    }];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:resultAsset];
    CMTime time = CMTimeMake(1, 1);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

- (void)downloadAsset:(PHAsset *)asset toURL:(NSURL *)url completion:(void (^)(void))completion {
    if (asset.mediaType == PHAssetMediaTypeImage) {
        PHImageRequestOptions *options = [PHImageRequestOptions new];
        options.networkAccessAllowed = YES;
        options.version = PHImageRequestOptionsVersionCurrent;
        options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        options.synchronous = YES;
        [[PHImageManager defaultManager] requestImageDataForAsset:asset options:options resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
            // Convert HEIC to JPEG for not iOS device compatibility
            if( [[url pathExtension] isEqualToString:@"HEIC"] || [[url pathExtension] isEqualToString:@"HEIF"]) {
                UIImage *im = [UIImage imageWithData:imageData];
                imageData = UIImageJPEGRepresentation(im, 0.9);
            }
            
            if ([info objectForKey:PHImageErrorKey] == nil && [[NSFileManager defaultManager] createFileAtPath:url.path contents:imageData attributes:nil]) {
                NSLog(@"downloaded photo:%@", url.path);
            }
            completion();
        }];
    } else if (asset.mediaType == PHAssetMediaTypeVideo) {
        PHVideoRequestOptions *options = [PHVideoRequestOptions new];
        options.networkAccessAllowed = YES;
        options.version = PHVideoRequestOptionsVersionCurrent;
        options.deliveryMode = PHVideoRequestOptionsDeliveryModeAutomatic;
        [[PHImageManager defaultManager] requestExportSessionForVideo:asset options:options exportPreset:AVAssetExportPresetHighestQuality resultHandler:^(AVAssetExportSession * _Nullable exportSession, NSDictionary * _Nullable info) {
            if ([info objectForKey:PHImageErrorKey] == nil)
                {
                exportSession.outputURL = url;
                
                NSArray<PHAssetResource *> *resources = [PHAssetResource assetResourcesForAsset:asset];
                for (PHAssetResource *resource in resources)
                    {
                    exportSession.outputFileType = resource.uniformTypeIdentifier;
                    if (exportSession.outputFileType != nil)
                        break;
                    }
                
                [exportSession exportAsynchronouslyWithCompletionHandler:^{
                    if (exportSession.status == AVAssetExportSessionStatusCompleted){
                        NSLog(@"downloaded video:%@", url.path);
                    }
                    completion();
                }];
                }
        }];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Attachment View
-(void) showAttachmentView {
    if (!_scrollToBottomButton.isHidden) {
        _scrollToBottomButton.hidden = YES;
    }
    
    if(_attachmentView.superview){
        [_attachmentView removeFromSuperview];
        _attachmentImageView.image = nil;
    }
    BOOL isLandscape = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation);
    if(_attachmentFileToSend.type != FileTypeImage && _attachmentFileToSend.type != FileTypeVideo){
        _attachmentImageHeight.constant = isLandscape?44:64;
        _attachmentFileNameLabel.hidden = NO;
    } else {
        _attachmentImageHeight.constant = isLandscape?100:200;
        _attachmentFileNameLabel.hidden = YES;
    }
    CGFloat height = _attachmentImageHeight.constant;
    if(_attachmentFileToSend.type != FileTypeImage && _attachmentFileToSend.type != FileTypeVideo)
        height += _attachmentFileNameLabel.frame.size.height + 20;
    
    [self adjustAttachmentViewFrameWithHeight:height];
    _attachmentFileNameLabel.text = _attachmentFileToSend.fileName;
    if(_attachmentPreview)
        _attachmentImageView.image = _attachmentPreview;
    else
        _attachmentImageView.image = _attachmentFileToSend.thumbnailBig;
    _attachmentImageView.tintColor = [UITools defaultTintColor];
    
    [self.inputToolbar.contentView addSubview:_attachmentView];
    // Very important ! don't forget to enable user interaction to handle events.
    _attachmentView.userInteractionEnabled = YES;
    
    [self jsq_updateCollectionViewInsets];
    [self updateConversationStatusAndToggleSendButton];
}

-(void)jsq_updateCollectionViewInsets {
    CGFloat inputBar = _attachmentView.superview ? (CGRectGetMinY(self.inputToolbar.frame) + CGRectGetMinY(_attachmentView.frame)): CGRectGetMinY(self.inputToolbar.frame);
    [self jsq_setCollectionViewInsetsTopValue:self.topLayoutGuide.length + self.topContentAdditionalInset bottomValue:CGRectGetMaxY(self.collectionView.frame) - inputBar];
}

-(IBAction)closeAttachementButtonClicked:(UIButton *)sender {
    if(_attachmentView.superview){
        if (_scrollToBottomButton.isHidden) {
            _scrollToBottomButton.hidden = NO;
        }
        [_attachmentView removeFromSuperview];
        _attachmentImageView.image = nil;
        [self jsq_updateCollectionViewInsets];
        [self updateConversationStatusAndToggleSendButton];
        _attachmentFileToSend = nil;
        _attachmentPreview = nil;
    }
}

-(void) adjustAttachmentViewFrameWithHeight:(CGFloat) heigth {
    CGRect frame = _attachmentView.frame;
    frame.size.height = heigth;
    frame.origin.y = -frame.size.height;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    
    _attachmentView.frame = frame;
}
- (IBAction)didTapOnAttachmentViewImage:(UITapGestureRecognizer *)sender {
    if(_attachmentFileToSend)
        [self openPreviewControllerForAttachment:_attachmentFileToSend];
}

#pragma mark - resend an file already sent
-(void) wantToSendFile:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self wantToSendFile:notification];
        });
        return;
    }
    
    downloadBlock block = ^(File *aFile) {
        _attachmentFileToSend = aFile;
        [self showAttachmentView];
    };
    
    File *aFile = (File *)notification.object;
    if(aFile.data){
        block(aFile);
    } else {
        MBProgressHUD *downloadHud = [UITools showHUDWithMessage:NSLocalizedString(@"Downloading file ...",nil) forView:self.view mode:MBProgressHUDModeIndeterminate imageName:nil dismissCompletionBlock:nil];
        [downloadHud show:YES];
        
        [[ServicesManager sharedInstance].fileSharingService downloadDataForFile:aFile withCompletionHandler:^(File *file, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [downloadHud hide:YES];
                if(!error){
                    block(file);
                } else {
                    MBProgressHUD *errorHud = [UITools showHUDWithMessage:NSLocalizedString(@"Error", nil) forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Failed" dismissCompletionBlock:nil];
                    [errorHud showAnimated:YES whileExecutingBlock:^{
                        [NSThread sleepForTimeInterval:3];
                    } completionBlock:^{
                        
                    }];
                }
            });
        }];
    }
}

#pragma mark - iCloud files
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
        NSData *data = [NSData dataWithContentsOfURL:url];
        _attachmentFileToSend = [[ServicesManager sharedInstance].fileSharingService createTemporaryFileWithFileName:[url lastPathComponent] andData:data andURL:url];
        
        [self showAttachmentView];
        
    }
    else if (controller.documentPickerMode == UIDocumentPickerModeExportToService){
        MBProgressHUD *openingHud = [UITools showHUDWithMessage:NSLocalizedString(@"Saved",@"Saved") forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Checkmark" dismissCompletionBlock:nil];
        [openingHud show:YES];
        
    }
    if (@available(iOS 11, *)) {
        [[UINavigationBar appearance] setTintColor:self.previousNavBarTintColor];
        [[UINavigationBar appearance] setBarTintColor:self.previousNavBarTintColor];
        [[UINavigationBar appearance] setTitleTextAttributes:_previousTitleTextAttributesColor];
    }
}

-(void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
    if (@available(iOS 11, *)) {
        [[UINavigationBar appearance] setTintColor:self.previousNavBarTintColor];
        [[UINavigationBar appearance] setBarTintColor:self.previousNavBarTintColor];
        [[UINavigationBar appearance] setTitleTextAttributes:_previousTitleTextAttributesColor];
    }
}



#pragma mark - RTC call notifications
-(void) didAddCall:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddCall:notification];
        });
        return;
    }
    
    [self showHideCallButton];
}

-(void) didUpdateCall:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateCall:notification];
        });
        return;
    }
    
    [self showHideCallButton];
}

-(void) didRemoveCall:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveCall:notification];
        });
        return;
    }
   // [FIRAnalytics logEventWithName:@"end_webRTC_Call" parameters:@{}];

     //@"duration":
    [self showHideCallButton];
}

#pragma mark - Join conference notification
-(void) doJoinConference:(NSNotification *) notification {
    if([_conversation.peer isKindOfClass:[Room class]] && [self isViewLoaded]){
        Room *room = (Room *) notification.object;
        if(((Room*)_conversation.peer) == room){
            [self joinConference];
        }
    }
}


-(void) didUpdateProgress:(NSNotification *) notification {
    
    NSDictionary * objects = (NSDictionary *) notification.object;
    NSNumber * valueNumber = (NSNumber *)[objects objectForKey:@"value"];
    NSString * tag =[objects objectForKey:@"tag"];
    dispatch_async(dispatch_get_main_queue(), ^{
        for (JSQRainbowOutgoingMessagesCollectionViewCell *cell in self.collectionView.visibleCells) {
            NSIndexPath *path = [self.collectionView indexPathForCell:cell];
            GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
            Message *lastMessage = message.lastMessage;
            if ([tag  isEqualToString: lastMessage.messageID]) {
                [cell setProgressViewWithValue:[valueNumber floatValue]];
            }
        }
    });
}

-(void) didUpdateDownloadedProgress:(NSNotification *) notification {
    
    NSDictionary * objects = (NSDictionary *) notification.object;
    NSNumber * valueNumber = (NSNumber *)[objects objectForKey:@"value"];
    NSString * tag = [objects objectForKey:@"tag"];
    NSString * fileURL = [NSString stringWithFormat:@"%@",[objects objectForKey:@"fileURL"]];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        for (JSQRainbowMessagesCollectionViewCell *cell in self.collectionView.visibleCells) {
            NSIndexPath *path = [self.collectionView indexPathForCell:cell];
            GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
            Message *lastMessage = message.lastMessage;
            if (([tag  isEqualToString: lastMessage.messageID]) && ([fileURL isEqualToString:[NSString stringWithFormat:@"%@",cell.attachment.url]])) {
                 cell.progressView.hidden = NO;
                 cell.cellDateLabel.hidden = YES;
                 [cell setProgressViewWithValue:[valueNumber floatValue] ];
            }
        }
    });
}

-(void) showErrorPopupWithTitle:(NSString *) title messsage:(NSString *) message {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showErrorPopupWithTitle:title messsage:message];
        });
        return;
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - File Actions

-(void) addActionsView {
    
    _actionsView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - topPadding , [UIScreen mainScreen].bounds.size.width, 64);
    [self.view addSubview:_actionsView];
}

- (void) setupActionsViewAtCell:(JSQRainbowMessagesCollectionViewCell *) cell  {
    _actionViewTabBar.tintColor = [[UITools defaultTintColor] colorWithAlphaComponent:0.8];
    if (@available(iOS 10, *)) {
        [_actionViewTabBar setUnselectedItemTintColor:[UITools defaultTintColor]];
    }
    
    // remove the delete if it's not my file
    NSIndexPath *path = [self.collectionView indexPathForCell:cell];
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
    Message *lastMessage = message.lastMessage;
    NSMutableArray* items = [NSMutableArray arrayWithArray:[_actionViewTabBar items]];
    if(!lastMessage.isOutgoing){
        if([items count] == 4)
            [items removeLastObject];
    } else {
        if(![items containsObject:_deleteTabBarItem])
            [items insertObject:_deleteTabBarItem atIndex:3];
    }
    for (UITabBarItem *item in items) {
        item.title = NSLocalizedString(item.title, nil);
    }
    [self.actionViewTabBar setItems:items];
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    if([item isEqual:_forwardTabBarItem]){
        [self forwardSelectedFile];
    }
    if([item isEqual:_shareTabBarItem]){
        [self shareSelectedFile];
    }
    if([item isEqual:_saveTabBarItem]){
        [self saveSelectedFile];
    }
    if([item isEqual:_deleteTabBarItem]){
        [self deleteMedia];
    }
}

- (void) saveSelectedFile {
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(status == PHAuthorizationStatusAuthorized) {
                // Already authorized
                if (!_selectedMedia.data) {
                    [[ServicesManager sharedInstance].fileSharingService downloadDataForFile:_selectedMedia withCompletionHandler:^(File *aFile, NSError *error) {
                        if (!error) {
                            _selectedMedia = aFile;
                            NSString *actionValue = [NSString stringWithFormat:@"%@ %@ %@", @"save_", [self getSelectedFileType:_selectedMedia],@"_file"];
                            [FIRAnalytics logEventWithName:@"save_file" parameters:@{
                                                                                     @"action":actionValue,
                                                                                     @"category":@"chat_screen"}];
                            [self performSaveFileAction];
                            
                        } else {
                            [FIRAnalytics logEventWithName:@"save_file" parameters:@{
                                                                                      @"action":@"failed_to_save_file",
                                                                                      @"category":@"chat_screen"}];
                            [UITools showErrorPopupWithTitle:nil message:NSLocalizedString(@"Error while downloading the file", @"Error while downloading file") inViewController:self];
                        }
                    }];
                } else {
                    [self performSaveFileAction];
                }
                
            } else {
                [self showAllowAccessPhotoAndCameraPopup];
            }

        });
    }];

}

- (void) shareSelectedFile {
    if (!_selectedMedia.data) {
        [[ServicesManager sharedInstance].fileSharingService downloadDataForFile:_selectedMedia withCompletionHandler:^(File *aFile, NSError *error) {
            if (!error) {
                _selectedMedia = aFile;
                NSString *actionValue = [NSString stringWithFormat:@"%@ %@ %@", @"share_", [self getSelectedFileType:_selectedMedia],@"_file"];
                [FIRAnalytics logEventWithName:@"share_file" parameters:@{
                                                                          @"action":actionValue,
                                                                          @"category":@"chat_screen"}];
                [self performShareFileAction];  
            } else {
                [FIRAnalytics logEventWithName:@"share_file" parameters:@{
                                                                          @"action":@"failed_to_share_file",
                                                                          @"category":@"chat_screen"}];
                [UITools showErrorPopupWithTitle:nil message:NSLocalizedString(@"Error while downloading the file", @"Error while downloading file") inViewController:self];
            }
        }];
    } else {
        NSString *actionValue = [NSString stringWithFormat:@"%@ %@ %@", @"share_", [self getSelectedFileType:_selectedMedia],@"_file"];
        [FIRAnalytics logEventWithName:@"share_file" parameters:@{
                                                                 @"action":actionValue,
                                                                 @"category":@"chat_screen"}];
         [self performShareFileAction];
    }
}

- (void) performSaveFileAction {
    
 
    if (_selectedMedia.type ==  FileTypeImage || _selectedMedia.type ==  FileTypeVideo) {
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *openingHud = [UITools showHUDWithMessage:NSLocalizedString(@"Saved",@"Saved") forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Checkmark" dismissCompletionBlock:nil];
            [openingHud showAnimated:YES whileExecutingBlock:^{
                if (_selectedMedia.type ==  FileTypeImage ) {
                    UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:_selectedMedia.data], nil, nil, nil);
                } else if (_selectedMedia.type ==  FileTypeVideo ) {
                    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                    dispatch_async(q, ^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Write it to cache directory
                            NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:_selectedMedia.fileName];
                            [_selectedMedia.data writeToFile:filePath atomically:YES];
                            UISaveVideoAtPathToSavedPhotosAlbum(filePath, nil, nil, nil);
                            NSError *error;
                            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
                            if (success) {
                                NSLog(@"success!");
                            }
                        });
                    });
                }
                [NSThread sleepForTimeInterval:3];
            } completionBlock:^{
                NSLog(@"completed!");
            }];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:_selectedMedia.fileName];
            [_selectedMedia.data writeToFile:filePath atomically:YES];
            
            NSURL *localURL = [NSURL fileURLWithPath:filePath];
            
            UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithURL:localURL inMode:UIDocumentPickerModeExportToService];
            documentPicker.delegate = self;
            
            documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
            documentPicker.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            
            [self presentViewController:documentPicker animated:YES completion:^{
                if (@available(iOS 11, *)) {
                    // iOS 11 (or newer)
                    NSObject<UIAppearance> *appearance = [UINavigationBar appearance];
                    self.previousNavBarTintColor = [(UINavigationBar *)appearance tintColor];
                    _previousTitleTextAttributesColor = [(UINavigationBar *)appearance titleTextAttributes];
                    [(UINavigationBar *)appearance setTintColor:[UITools defaultTintColor]];
                }
            }];
        });
    }
    [self dismissActionView];
}

- (void) performShareFileAction {
    if (_selectedMedia.type ==  FileTypeImage ) {
        UIImage * image = [UIImage imageWithData:_selectedMedia.data];
        NSArray *activityItems = @[image];
        UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        activityViewControntroller.excludedActivityTypes = @[];
        [activityViewControntroller setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            if (completed) {
                MBProgressHUD *openingHud = [UITools showHUDWithMessage:NSLocalizedString(@"Done",@"Done") forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Checkmark" dismissCompletionBlock:nil];
                [openingHud show:YES];
                 [self dismissActionView];
            } else {
                 [self dismissActionView];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [self presentViewController:activityViewControntroller animated:true completion:^{
        }];
    } else {
        NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:_selectedMedia.fileName];
        [_selectedMedia.data writeToFile:filePath atomically:YES];
        
        NSString* fileName = _selectedMedia.fileName;
        NSArray* dataToShare;
        
        if (_selectedMedia.type ==  FileTypeVideo ) {
            NSURL *urlToShare = [NSURL fileURLWithPath:filePath isDirectory:NO];
            dataToShare = @[fileName, urlToShare];
        } else {
            NSData *fileData = [NSData dataWithContentsOfFile:filePath];
            dataToShare = @[fileName, fileData];
        }
        
        UIActivityViewController* activityViewController =
        [[UIActivityViewController alloc] initWithActivityItems:dataToShare
                                          applicationActivities:nil];
        activityViewController.excludedActivityTypes = @[UIActivityTypePrint,UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact];
        [activityViewController setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            if (completed) {
                MBProgressHUD *openingHud = [UITools showHUDWithMessage:NSLocalizedString(@"Done",@"Done") forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Checkmark" dismissCompletionBlock:nil];
                [openingHud show:YES];
                [self dismissActionView];
            } else {
                [self dismissActionView];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [self presentViewController:activityViewController animated:YES completion:^{
        }];
    }
}

- (void) forwardSelectedFile {
    CustomNavigationController * shareFileControllerNVC = [[UIStoryboardManager sharedInstance].shareFilesStoryboard instantiateViewControllerWithIdentifier:@"shareFilesNavigationControllerID"];
    ShareFileViewController *shareFileController = [shareFileControllerNVC.viewControllers firstObject];
    shareFileController.fileToShare = _selectedMedia;
    shareFileController.currentConversation = _conversation;
    
    NSString *actionValue = [NSString stringWithFormat:@"%@ %@ %@", @"forward_", [self getSelectedFileType:_selectedMedia],@"_file"];
    [FIRAnalytics logEventWithName:@"forward_file" parameters:@{
                                                               @"action":actionValue,
                                                               @"category":@"chat_screen"}];
    [self presentViewController:shareFileControllerNVC animated:YES completion:nil];
    
    [self dismissActionView];
}

- (void) deleteMedia {
    BOOL showWarning = [[NSUserDefaults standardUserDefaults] boolForKey:@"dontShowRemoveViewerWarningAlert"];
    
    if(!showWarning){
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        
        SCLSwitchView *switchView = [alert addSwitchViewWithLabel:NSLocalizedString(@"Don’t show this message again", nil)];
        
        SCLButton *yesButton = [alert addButton:NSLocalizedString(@"Yes", nil) actionBlock:^(void) {
            [[NSUserDefaults standardUserDefaults] setBool:switchView.isSelected forKey:@"dontShowRemoveViewerWarningAlert"];
            [self deleteMessage:_selectedMessage];
        }];
        
        alert.customViewColor = [UITools defaultTintColor];
        alert.iconTintColor = [UITools defaultBackgroundColor];
        alert.backgroundViewColor = [UITools defaultBackgroundColor];
        alert.statusBarStyle = UIStatusBarStyleLightContent;
        [alert setTitleFontFamily:[UITools boldFontName] withSize:14.0];
        [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
        alert.shouldDismissOnTapOutside = YES;
        alert.labelTitle.numberOfLines = 4;
        alert.labelTitle.lineBreakMode = NSLineBreakByWordWrapping;
        
        yesButton.buttonFormatBlock = ^NSDictionary* (void) {
            return @{@"backgroundColor":[UIColor redColor]};
        };
        
        switchView.labelFont = [UIFont fontWithName:[UITools defaultFontName] size:12];
        if([_conversation.peer isKindOfClass:[Room class]]){
            [alert showQuestion:self
                          title:NSLocalizedString(@"This file will be deleted from your personal storage space", nil)
                       subTitle:[NSString stringWithFormat:@"(%@)", NSLocalizedString(@"The participants of this bubble will no longer be able to see this file", nil)]
               closeButtonTitle:NSLocalizedString(@"No", nil)
                       duration:0.0f];
        } else {
            [alert showQuestion:self
                          title:NSLocalizedString(@"This file will be deleted from your personal storage space", nil)
                       subTitle:[NSString stringWithFormat:@"(%@)", NSLocalizedString(@"This user will no longer be able to see this file", nil)]
               closeButtonTitle:NSLocalizedString(@"No", nil)
                       duration:0.0f];
        }
    } else {
        [self deleteMessage:_selectedMessage];
    }
}

- (GroupedMessages *) getMessageForFile:(File *)file{
    for (GroupedMessages * theMessage in _messages) {
        Message * aMessage = theMessage.lastMessage;
        if (aMessage.attachment == file) {
            return theMessage;
        }
    }
    return nil;
}

-(void) deleteMessage:(Message *) message {
     MBProgressHUD * hud = [self showHUDWithMessage:NSLocalizedString(@"Removing file...",nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud show:YES];
    });
    
    [_conversationsManagerService deleteMessage:message inConversation:_conversation];

}

-(void) didRemoveFile:(NSNotification *) notification {
    File * deletedFile = notification.object;
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if(deletedFile){
            NSLog(@"did delete file %@",deletedFile);
            GroupedMessages * message = [self getMessageForFile:deletedFile];
            if (message) {
                Message *lastMessage = message.lastMessage;
                [_messages removeObject:message];
                [self hideFileThumbmailForDeletedFile];
                [self reloadCollectionView];
                // success deleting the file >> add analytices
                NSString *actionValue = [NSString stringWithFormat:@"%@ %@ %@", @"delete_", [self getSelectedFileType:_selectedMedia],@"_file"];
                
                [FIRAnalytics logEventWithName:@"delete_file" parameters:@{
                                                                           @"action":actionValue,
                                                                           @"category":@"chat_screen"}];

                [_conversationsManagerService deleteFailedMessage:lastMessage to:_conversation];
            }
        }
        else{
            // show Error Message
            NSLog(@"failed to delete a file %@",deletedFile);
            [self showHUDWithErrorMessage:NSLocalizedString(@"Error while removing the File",nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:nil];
            
        }
         [self dismissActionView];
    });
}

-(NSString *)getSelectedFileType:(File *)file {
    if(_selectedMedia.type == FileTypeVideo){
        return @"video";
    }
    else if(_selectedMedia.type == FileTypeAudio) {
        return @"audio";
    }
    else if (_selectedMedia.type == FileTypeImage) {
        return @"image";
    }
    else {
       return @"document";
    }
}

@end
