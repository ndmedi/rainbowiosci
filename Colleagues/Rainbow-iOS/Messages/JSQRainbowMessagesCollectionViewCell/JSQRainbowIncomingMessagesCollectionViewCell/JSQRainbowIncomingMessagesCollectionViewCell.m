/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 *
 * Created by Vladimir Vyskocil on 30/11/16.
 */

#import "JSQRainbowIncomingMessagesCollectionViewCell.h"
#import "UIView+JSQMessagesConstant.h"

@implementation JSQRainbowIncomingMessagesCollectionViewCell

+ (UINib *)nib {
    return [UINib nibWithNibName:NSStringFromClass([JSQRainbowIncomingMessagesCollectionViewCell class])
                          bundle:[NSBundle bundleForClass:[JSQRainbowIncomingMessagesCollectionViewCell class]]];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.messageBubbleTopLabel.textAlignment = NSTextAlignmentLeft;
    self.cellBottomLabel.textAlignment = NSTextAlignmentLeft;
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    JSQMessagesCollectionViewLayoutAttributes *customAttributes = (JSQMessagesCollectionViewLayoutAttributes *)layoutAttributes;
    self.avatarViewSize = customAttributes.incomingAvatarViewSize;
}

-(void) setMediaView:(UIView *)mediaView {
    [super setMediaView:mediaView];
    self.mediaView.tintColor = [UIColor whiteColor];
    if (self.attachment.hasThumbnailOnServer ) {
        if(!self.attachment.thumbnailData && !self.attachment.data){
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeBottom constant:70];
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeLeading constant:-10];
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeTrailing constant:5];
        } else {
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeBottom constant:30];
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeLeading constant:-10];
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeTrailing constant:5];
        }
    }
    
    else {
        [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeBottom constant:70];
        [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeLeading constant:-10];
        [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeTrailing constant:5];
    }
   
    
    self.textViewBottomVerticalSpaceConstraint.constant = -(mediaView.frame.size.height - self.textView.frame.size.height);
}
-(void) setProgressViewWithValue : (float) progressValue {
    [super setProgressViewWithValue:progressValue];
}
@end
