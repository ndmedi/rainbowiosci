/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "JSQRainbowOutgoingMessagesCollectionViewCell.h"
#import "UIView+JSQMessagesConstant.h"
#import "File+DefaultImage.h"
#import "UIView+JSQMessages.h"


@implementation JSQRainbowOutgoingMessagesCollectionViewCell
+ (UINib *)nib {
    return [UINib nibWithNibName:NSStringFromClass([JSQRainbowOutgoingMessagesCollectionViewCell class])
                          bundle:[NSBundle bundleForClass:[JSQRainbowOutgoingMessagesCollectionViewCell class]]];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.messageBubbleTopLabel.textAlignment = NSTextAlignmentRight;
    self.cellBottomLabel.textAlignment = NSTextAlignmentRight;
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    JSQMessagesCollectionViewLayoutAttributes *customAttributes = (JSQMessagesCollectionViewLayoutAttributes *)layoutAttributes;
    self.avatarViewSize = customAttributes.outgoingAvatarViewSize;
}

-(void) setMediaView:(UIView *)mediaView {
    [super setMediaView:mediaView];
    if (self.attachment.hasThumbnailOnServer ) {
        if(!self.attachment.thumbnailData && !self.attachment.data){
            
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeBottom constant:70];
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeLeading constant:-5];
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeTrailing constant:10];
        } else {
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeBottom constant:30];
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeLeading constant:-5];
            [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeTrailing constant:10];
            
        }
    }
     else {
         [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeBottom constant:70];
         [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeLeading constant:-5];
         [self.messageBubbleContainerView jsq_pinSubview:mediaView toEdge:NSLayoutAttributeTrailing constant:10];
       
    }
    
  // self.textViewBottomVerticalSpaceConstraint.constant = -(mediaView.frame.size.height - self.textView.frame.size.height);
}

-(void) didRemoveFile:(NSNotification *) notification {
    File *theFile = (File *)notification.object;
    if(![theFile isEqual:self.attachment]){
        return;
    }
    
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveFile:notification];
        });
        return;
    }
    [super didRemoveFile:notification];
}

-(void) setProgressViewWithTag:(NSInteger)tag andValue : (float) progressValue {
    [super setProgressViewWithValue:progressValue];
}
@end
