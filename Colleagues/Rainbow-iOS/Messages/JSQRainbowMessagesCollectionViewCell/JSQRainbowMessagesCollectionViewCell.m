/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 *
 */

#import "JSQRainbowMessagesCollectionViewCell.h"
#import "UITools.h"
#import "File+DefaultImage.h"
#import <Rainbow/NSString+FileSize.h>
#import <MarqueeLabel/MarqueeLabel.h>
#import "JSQMessagesCollectionView.h"
#import "UIView+JSQMessagesConstant.h"
#import "File+JSQMessageMediaData.h"
#import "UIView+JSQMessages.h"

@interface JSQMessagesCollectionView (tapOnImage)
-(void) didTapOnImageForAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell;
-(void) didFailedSendAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell;
-(void) didShareAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell;
-(void) didLongPressOnImageForAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell;
@end

@implementation JSQMessagesCollectionView (tapOnImage)

-(void) didTapOnImageForAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    [self.delegate performSelector:@selector(didTapOnImageForAttachment:atCell:) withObject:attachment withObject:cell];
}

-(void) didLongPressOnImageForAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    [self.delegate performSelector:@selector(didLongPressOnImageForAttachment:atCell:) withObject:attachment withObject:cell];
}

-(void) didFailedSendAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    [self.delegate performSelector:@selector(didFailedSendAttachment:atCell:) withObject:attachment withObject:cell];
}

-(void) didShareAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    [self.delegate performSelector:@selector(didShareAttachment:atCell:) withObject:attachment withObject:cell];
}

@end

@interface JSQRainbowMessagesCollectionViewCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarContainerViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarContainerViewHeightConstraint;
@property (nonatomic, weak) IBOutlet UILabel *fileName;
@property (nonatomic, weak) IBOutlet UILabel *fileSizeLabel;
- (void)jsq_updateConstraint:(NSLayoutConstraint *)constraint withConstant:(CGFloat)constant;
@property (nonatomic, strong) UIView *internalMediaView;
@end

@implementation JSQRainbowMessagesCollectionViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_cellDateLabel];
    [UITools applyCustomFontTo:_fileName];
    [UITools applyCustomFontTo:_fileSizeLabel];
    _fileName.hidden = YES;
    _fileSizeLabel.hidden = YES;
    _progressView.clipsToBounds = YES;
    _progressView.layer.cornerRadius = 2.0;
    _progressView.hidden = YES;
    _shareButton.hidden = YES;
    _status.userInteractionEnabled = YES;
    _shareButton.tintColor = [UITools defaultTintColor];
    
    CGFloat borderWidth = 1.0f;
    
    _shareButton.frame = CGRectInset(self.frame, -borderWidth, -borderWidth);
    _shareButton.layer.borderColor = [UITools defaultTintColor].CGColor;
    _shareButton.layer.borderWidth = borderWidth;
    
    UIImage *image = [[UIImage imageNamed:@"menu_other"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_shareButton setImage:image forState:UIControlStateNormal];
    _shareButton.tintColor = [UITools defaultTintColor];
    _shareButton.imageEdgeInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateAttachment:) name:kFileSharingServiceDidUpdateFile object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnStatus:)];
    [_status addGestureRecognizer:tap];
    
    
}

- (id)initWithCoder:(NSCoder*)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateAttachment:) name:kFileSharingServiceDidUpdateFile object:nil];
        
    }
    
    return self;
}
-(void) dealloc {
    _attachment = nil;
    _fileName = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidUpdateFile object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidRemoveFile object:nil];
}


- (void)setAvatarViewSize:(CGSize)avatarViewSize {
    [self jsq_updateConstraint:self.avatarContainerViewWidthConstraint withConstant:avatarViewSize.width];
    [self jsq_updateConstraint:self.avatarContainerViewHeightConstraint withConstant:avatarViewSize.height];
}

- (void)setTimestamp:(NSDate *)date {
    self.cellDateLabel.text = [UITools optimizedFormatForDate:date];
}

-(void) prepareForReuse {
    [super prepareForReuse];
    _fileName.text = nil;
    _fileSizeLabel.text = nil;
    _fileName.hidden = YES;
    _fileSizeLabel.hidden = YES;
    _shareButton.hidden = YES;
    [_internalMediaView removeFromSuperview];
}

-(void) shouldUpdateAttachment:(NSNotification *) notification {
    File *file = (File *) notification.object;
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([_attachment.url isEqual:file.url]) {
            [self setMediaView:file.mediaView];
            [self setAttachment:file];
        }
    });
}



-(void) setAttachment:(File *)attachment {
    _attachment = attachment;
  
    // remove && attachment.isDownloadAvailable because it cause error when try to download image not available the cell appears empty!
    if(attachment){
        if (!_attachment.isOfflineAttachment) {
            _shareButton.hidden = NO;
        }
        
        // if the attachment type is image and have data or thumbnail data -> hide fileName and fileZiseLabel
        // if the attachment contains data but the type is not
        if((_attachment.data || _attachment.thumbnailData) && _attachment.hasThumbnailOnServer && _attachment.isDownloadAvailable){
            _fileName.hidden = YES;
            _fileSizeLabel.hidden = YES;
         
            
        } else {
            _fileName.text = _attachment.fileName;
            _fileSizeLabel.text = [NSString formatFileSize:_attachment.size];
            _fileName.hidden = NO;
            _fileSizeLabel.hidden = NO;
        }
 
    } else { // this means that the attachment parameter is nill
        _shareButton.hidden = YES;
        _fileName.hidden = YES;
        _fileSizeLabel.hidden = YES;
        _fileName.hidden = YES;
        [_internalMediaView removeFromSuperview];
        _internalMediaView = nil;
    }
    [self setNeedsUpdateConstraints];
}

- (void)didTapOnImage:(UIGestureRecognizer *)sender {
    [self.delegate performSelector:@selector(didTapOnImageForAttachment:atCell:) withObject:_attachment withObject:self];
}


- (void)didTapOnStatus:(UIGestureRecognizer *)sender {
    [self.delegate performSelector:@selector(didFailedSendAttachment:atCell:) withObject:_attachment withObject:self];
}

- (void)setMediaView:(UIView *)mediaView {
    [mediaView setTranslatesAutoresizingMaskIntoConstraints:NO];
    CGRect frame = self.messageBubbleContainerView.bounds;
    frame.size = self.attachment.mediaViewDisplaySize;
    frame.origin.y += self.textView.frame.size.height;
    mediaView.frame = frame;

    if(_internalMediaView){
        [_internalMediaView removeFromSuperview];
    }

    [self.messageBubbleContainerView addSubview:mediaView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnImage:)];
    [mediaView addGestureRecognizer:tap];
    mediaView.userInteractionEnabled = YES;

    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(didLongPressOnImage:)];
    longPress.minimumPressDuration = 1 ;
    longPress.numberOfTouchesRequired = 1 ;
    [longPress requireGestureRecognizerToFail:tap];
    [mediaView addGestureRecognizer:longPress];
    mediaView.userInteractionEnabled = YES;
    mediaView.tintColor = self.attachment.tintColor;

    _internalMediaView = mediaView;
}

-(UIView *) mediaView {
    return _internalMediaView;
}

-(void) setProgressViewWithValue : (float) progressValue {
    self.progressView.hidden = NO;
    self.shareButton.hidden = YES;
    self.progressView.progress = progressValue;
    self.attachment.value = progressValue;
}

-(void) didLongPressOnImage:(UIGestureRecognizer *) gesture {
    [self.delegate performSelector:@selector(didLongPressOnImageForAttachment:atCell:) withObject:_attachment withObject:self];
}
- (IBAction)failedButtonAction:(id)sender {
    [self.delegate performSelector:@selector(didFailedSendAttachment:atCell:) withObject:_attachment withObject:self];
}

- (IBAction)shareButtonAction:(id)sender {
    [self.delegate performSelector:@selector(didShareAttachment:atCell:) withObject:_attachment withObject:self];
}
@end
