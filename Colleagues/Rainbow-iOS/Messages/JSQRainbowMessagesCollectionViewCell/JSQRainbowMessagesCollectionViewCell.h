/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 *
 */

#import "JSQMessagesCollectionViewCell.h"
#import <Rainbow/File.h>
#import "JSQMessagesCollectionViewLayoutAttributes.h"
#import "UITools.h"


@interface JSQRainbowMessagesCollectionViewCell : JSQMessagesCollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellDateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewTopVerticalSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewBottomVerticalSpaceConstraint;

@property (strong, nonatomic) File *attachment;
@property (weak, nonatomic) IBOutlet UIImageView *status;

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;


@property (assign, nonatomic) CGSize avatarViewSize;
-(void) setProgressViewWithValue : (float) progressValue ;
-(void) setTimestamp:(NSDate *)date;
-(void) setAttachment:(File *)attachment;
-(void) didRemoveFile:(NSNotification *) notification;

@end
