/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ShareFileViewController.h"
#import "UIRainbowGenericTableViewCell.h"
#import <Rainbow/ServicesManager.h>
#import "FilteredSortedSectionedArray.h"
#import "UITools.h"


#import "CustomSearchController.h"
#import "UISearchResultController.h"
#import "UIStoryboardManager.h"
#import "CustomSearchBarContainerView.h"
#import "UIView+JSQMessagesConstant.h"
#import "UIScrollView+APParallaxHeader.h"
#import "File+DefaultImage.h"
#import "Firebase.h"
@interface ShareFileViewController () <UITableViewDelegate, UITableViewDataSource, CustomSearchControllerDelegate>
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *conversations;
@property (nonatomic, strong) NSMutableArray *selectedItems;

@property (nonatomic, strong) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic, strong) MBProgressHUD *hud;

@property (nonatomic, strong) UISearchResultController *searchResultController;
@property (nonatomic, strong) CustomSearchController *customSearchController;

@property (nonatomic, strong) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerViewHeigthConstraint;

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *attachmentImageView;
@property (weak, nonatomic) IBOutlet UILabel *attachmentFileName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *attachmentImageHeigthConstraint;
@property (weak, nonatomic) IBOutlet UILabel *attachmentViewTopLabel;

@end

@implementation ShareFileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _conversations = [NSMutableArray array];
    _selectedItems = [NSMutableArray new];
    
    [self getConversations];

    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    self.definesPresentationContext = YES;
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];
    [self.tableView setEditing:YES animated:YES];
    [self showHideSendButton];

    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
    
    _searchResultController = (UISearchResultController*)[[UIStoryboardManager sharedInstance].searchStoryboard instantiateViewControllerWithIdentifier:@"UISearchResultController"];
    
    [UITools applyCustomFontTo:_attachmentViewTopLabel];
    _attachmentViewTopLabel.text = NSLocalizedString(@"Forward this file" , nil);
    
    [UITools applyCustomBoldFontTo:_sendButton.titleLabel];
    [_sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _sendButton.backgroundColor = [UITools defaultTintColor];
    [_sendButton setTitle:NSLocalizedString(@"Forward", nil) forState:UIControlStateNormal];
    
    // Search controller
    _customSearchController = [[CustomSearchController alloc] initWithSearchResultsController:_searchResultController customSearchControllerDelegate:self];
    _searchResultController.modalPresentationStyle = UIModalPresentationFullScreen;
    _customSearchController.handleDismissOnCancel = NO;
    _customSearchController.searchScope = SearchScopeConversations | SearchScopeRooms | SearchScopeContacts;
    
    __weak typeof(self) weakSelf = self;
    __weak typeof(_selectedItems) weakSelectedItems = _selectedItems;
    __weak typeof(_customSearchController) weakCustomSearchController = _customSearchController;
    __weak typeof(_conversations) weakConversations = _conversations;
    __weak typeof(self.tableView) weakTableView = self.tableView;
    _searchResultController.cellSelectedActionBlock = ^(id object, NSString * sectionName) {
        NSLog(@"Selected object %@" ,object);
        Peer * aPeer = (Peer *)object;
        if([object isKindOfClass:[Conversation class]]){
            aPeer = ((Conversation *)object).peer;
        }
        [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:aPeer notifyStartConversation:NO withCompletionHandler:^(Conversation *conversation, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error){
                    [UITools showErrorPopupWithTitle:@"" message:[NSString stringWithFormat:NSLocalizedString(@"Error while creating conversation with %@", nil), ((Peer *)object).displayName] inViewController:weakSelf];
                } else {
                    NSInteger existingConversationIdx = NSNotFound;
                    if([weakConversations containsObject:conversation]){
                        existingConversationIdx = [weakConversations indexOfObject:conversation];
                        [weakConversations removeObject:conversation];
                    }
                    [weakSelectedItems addObject:conversation];
                    [weakSelectedItems sortUsingDescriptors:@[[UITools sortDescriptorByLastMessageDate]]];
                    [weakTableView beginUpdates];
                    NSInteger idx = [weakSelectedItems indexOfObject:conversation];
                    if(existingConversationIdx != NSNotFound){
                        // Move the cell
                        [weakTableView moveRowAtIndexPath:[NSIndexPath indexPathForRow:existingConversationIdx inSection:1] toIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
                    } else {
                        // insert the new cell
                        [weakTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
                    }
                    [weakTableView endUpdates];
                    [weakSelectedItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
                        [weakTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                    }];
                    [weakTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    [weakSelf showHideSendButton];
                    [weakCustomSearchController searchBarCancelButtonClicked:weakCustomSearchController.searchBar];
                }
            });
        }];
    };
    CustomSearchBarContainerView *titleView = [[CustomSearchBarContainerView alloc] initWithSearchBar:_customSearchController.searchBar];
    titleView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
    self.navigationItem.titleView = titleView;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self showAttachmentView];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

-(void) getConversations {
    [_conversations removeAllObjects];
    [_conversations addObjectsFromArray:[ServicesManager sharedInstance].conversationsManagerService.conversations];
    [_conversations removeObject:_currentConversation];
    [_conversations removeObjectsInArray:_selectedItems];
    [_conversations sortUsingDescriptors:@[[UITools sortDescriptorByLastMessageDate]]];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showHideSendButton {
    if(_selectedItems.count > 0){
        if(_footerView.hidden == YES){
            _footerViewHeigthConstraint.constant = 68;
            _footerView.hidden = NO;
        }
    } else {
        _footerViewHeigthConstraint.constant = 0;
        _footerView.hidden = YES;
    }
}

-(void)setFileToShare:(File *)fileToShare {
    _fileToShare = fileToShare;
}

-(void) showAttachmentView {
    BOOL isLandscape = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation);
    if(!_fileToShare.thumbnailData && !_fileToShare.data){
        _attachmentImageHeigthConstraint.constant = isLandscape?44:64;
        _attachmentFileName.hidden = NO;
    } else {
        _attachmentImageHeigthConstraint.constant = 130;
        _attachmentFileName.hidden = YES;
    }
    CGFloat height = _attachmentImageHeigthConstraint.constant;
    if(!_fileToShare.thumbnailData && !_fileToShare.data)
        height += _attachmentFileName.frame.size.height;
    else
        height += 20;
    
    height += _attachmentViewTopLabel.frame.size.height + 20;
    
    [self adjustAttachmentViewFrameWithHeight:height];
    
    _attachmentFileName.text = _fileToShare.fileName;
    _attachmentImageView.image = _fileToShare.thumbnailBig;
    _attachmentImageView.tintColor = [_fileToShare tintColor];
    [self.tableView addParallaxWithView:_headerView andHeight:_headerView.frame.size.height andMinHeight:_headerView.frame.size.height andShadow:NO];
    self.tableView.parallaxView.layer.zPosition = 1;
}

-(void) adjustAttachmentViewFrameWithHeight:(CGFloat) heigth {
    CGRect frame = _headerView.frame;
    frame.size.height = heigth;
    frame.origin.y = -frame.size.height;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    
    _headerView.frame = frame;
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Selected items
    if(section == 0)
        return [_selectedItems count];
    else
        return [_conversations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    
    NSObject *object = nil;
    if(indexPath.section == 0)
        object = [_selectedItems objectAtIndex:indexPath.row];
    else
        object = [_conversations objectAtIndex:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.cellObject = (NSObject<UIRainbowGenericTableViewCellProtocol>*)object;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        [self tableView:tableView didDeselectRowAtIndexPath:indexPath];
    }
    
    if(indexPath.section == 1){
        Conversation *conversation = [_conversations objectAtIndex:indexPath.row];
        [_selectedItems addObject:conversation];
        [_selectedItems sortUsingDescriptors:@[[UITools sortDescriptorByLastMessageDate]]];
        [_conversations removeObject:conversation];
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:[_selectedItems indexOfObject:conversation] inSection:0];
        // move object from current section to first section
        [self.tableView beginUpdates];
        [self.tableView moveRowAtIndexPath:indexPath toIndexPath:newIndexPath];
        [self.tableView endUpdates];
        [self showHideSendButton];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        Conversation *conversation = [_selectedItems objectAtIndex:indexPath.row];
        [self.tableView beginUpdates];
        [_selectedItems removeObjectAtIndex:indexPath.row];
        [self getConversations];
        NSInteger conversationIndex = [_conversations indexOfObject:conversation];
        [self.tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:conversationIndex inSection:1]];
        [self.tableView endUpdates];
        
        [self showHideSendButton];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 0){
        return 0;
    }
    return 44;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return nil;
    } else
        return NSLocalizedString(@"Recent conversations", nil);
}

#pragma mark - CustomSearchControllerDelegate

- (IBAction)cancelAction:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)forwardFileAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    _hud = [UITools showHUDWithMessage:NSLocalizedString(@"Sending file ...", nil) forView:self.view mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    
    [_hud showAnimated:YES whileExecutingBlock:^{
        dispatch_group_t createGroup = dispatch_group_create();
        dispatch_group_enter(createGroup);
        if (!_fileToShare.owner) {
            [[ServicesManager sharedInstance].fileSharingService fetchFileInformation:_fileToShare completionHandler:^(File *file, NSError *error) {
                _fileToShare = file;
                dispatch_group_leave(createGroup);
            }];
        }
        else{
            dispatch_group_leave(createGroup);
        }
        dispatch_group_notify(createGroup, dispatch_get_main_queue(), ^{
            if ([_fileToShare.owner isEqual:[ServicesManager sharedInstance].myUser.contact]) {
                [self performForwardAction];
                
            } else {
                [[ServicesManager sharedInstance].fileSharingService transferFile:_fileToShare withCompletionHandler:^(File *file, NSError *error) {
                    if (!error) {
                        _fileToShare = file;
                        [self performForwardAction];
                    } else {
                        // show error hud
                        NSLog(@"Error while downloading file %@",error);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [UITools showErrorPopupWithTitle:nil message:NSLocalizedString(@"Error while downloading the file", nil) inViewController:self];
                        });
                    }
                }];
            }
        });
         [NSThread sleepForTimeInterval:1];
    } completionBlock:^{
        MBProgressHUD *uploadDoneHud = [UITools showHUDWithMessage:NSLocalizedString(@"Sent", nil) forView:weakSelf.view mode:MBProgressHUDModeCustomView imageName:@"Checkmark" dismissCompletionBlock:nil];
        [uploadDoneHud showAnimated:YES whileExecutingBlock:^{
            [NSThread sleepForTimeInterval:1];
        } completionBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [_hud hide:YES];
                [self dismissViewControllerAnimated:NO completion:nil];
            });
        }];
    }];
}


- (void) performForwardAction {
    dispatch_group_t forwardGlobalGroup = dispatch_group_create();
    NSMutableArray <dispatch_group_t> *waitingGroup = [NSMutableArray array];
    dispatch_group_enter(forwardGlobalGroup);

    for (Conversation * conversation in _selectedItems) {
        dispatch_group_t forwardGroupForEach = dispatch_group_create();
        @synchronized(waitingGroup){
            [waitingGroup addObject:forwardGroupForEach];
        }
        dispatch_group_enter(forwardGroupForEach);
        [[ServicesManager sharedInstance].conversationsManagerService sendMessage:nil fileAttachment:_fileToShare to:conversation completionHandler:^(Message *message, NSError *error) {
            dispatch_group_leave(forwardGroupForEach);
            @synchronized(waitingGroup){
                [waitingGroup removeObject:forwardGroupForEach];
                if(waitingGroup.count == 0)
                    dispatch_group_leave(forwardGlobalGroup);
            }
        } attachmentUploadProgressHandler:nil];
    }
    

}

#pragma mark - CustomSearchBar delegate
-(void) customSearchController:(CustomSearchController * _Nonnull) customSearchController didPressCancelButton :(UISearchBar * _Nonnull ) searchBar {
    
}

-(void) willPresentSearchController:(CustomSearchController * _Nonnull) customSearchController  {
    [self.navigationItem setLeftBarButtonItems:nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.titleView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
}

-(void) willDismissSearchController:(CustomSearchController * _Nonnull) customSearchController {
    ((CustomSearchBarContainerView*)self.navigationController.navigationItem.titleView).frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
    self.navigationItem.rightBarButtonItem = _cancelButton;
}
@end
