/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Message-JSQMessageData.h"
#import "Contact+Extensions.h"
#import <Rainbow/ServicesManager.h>
#import "JSQMessagesMediaViewBubbleImageMasker.h"
#import "File+DefaultImage.h"
#import "File+JSQMessageMediaData.h"

@implementation Message (JSQMessageData)

-(NSString *)senderId {
    if (self.isOutgoing)
        return [ServicesManager sharedInstance].myUser.contact.jid;
    return self.peer.jid;
}

-(NSString *) senderDisplayName {
    if (self.isOutgoing)
        return [ServicesManager sharedInstance].myUser.contact.firstName ? [ServicesManager sharedInstance].myUser.contact.firstName : [ServicesManager sharedInstance].myUser.contact.jid;
    return self.peer.displayName;
}

/**
 * return YES for media only bubbles, NO otherwise
 */
-(BOOL) isMediaMessage {
    // remove && self.attachment.isDownloadAvailable; .. it cause error in UI
    return self.attachment;
}

- (id<JSQMessageMediaData>)media {
    return self.attachment;
}

-(NSUInteger) messageHash {
    return self.hash;
}

-(NSString *) text {
    return self.body;
}
@end
