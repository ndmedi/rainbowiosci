/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "JoinMeetingTableViewCell.h"
#import "UITools.h"

@interface JoinMeetingTableViewCell ()
@property (weak, nonatomic) IBOutlet UIButton *phoneNumberButton;

@end
@implementation JoinMeetingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_phoneNumberButton.titleLabel];
    _phoneNumberButton.tintColor = [UITools defaultTintColor];
}

-(void) setPhoneNumber:(PhoneNumber *)phoneNumber {
    _phoneNumber = phoneNumber;
    NSString *title = [NSString stringWithFormat:@"%@ (%@)",(phoneNumber.type == PhoneNumberTypeConference)?(_phoneNumber.countryName)?_phoneNumber.countryName:@"":NSLocalizedString(_phoneNumber.label,nil), _phoneNumber.number];
    [_phoneNumberButton setTitle:title forState:UIControlStateNormal];
    if(phoneNumber.type == PhoneNumberTypeConference){
        [_phoneNumberButton setImage:nil forState:UIControlStateNormal];
        [_phoneNumberButton setImage:nil forState:UIControlStateHighlighted];
        [_phoneNumberButton setImage:nil forState:UIControlStateSelected];
        [_phoneNumberButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    } else {
        [_phoneNumberButton setImage:[UIImage imageNamed:@"RadioButton"] forState:UIControlStateNormal];
        [_phoneNumberButton setImage:[UIImage imageNamed:@"RadioButton_selected"] forState:UIControlStateHighlighted];
        [_phoneNumberButton setImage:[UIImage imageNamed:@"RadioButton_selected"] forState:UIControlStateSelected];
        [_phoneNumberButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
    _phoneNumberButton.selected = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _phoneNumberButton.selected = selected;
}

@end
