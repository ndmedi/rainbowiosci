/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "JoinMeetingViewController.h"
#import "UITools.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import <Rainbow/ServicesManager.h>
#import "JoinMeetingTableViewCell.h"
#import "FilteredSortedSectionedArray.h"
#import <Rainbow/RainbowUserDefaults.h>

@interface PhoneNumber (readwrite)
@property (nonatomic, readwrite) NSString *number;
@end

#define kOtherNumberConferenceCallBackKey @"otherNumberConferenceCallBackSetting"
#define kSelectedConferenceCallBackNumberKey @"selectedConferenceCallBackNumber"

@interface JoinMeetingViewController () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *titleViewLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelButtonCenterConstraint;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *callButtonCenterConstraint;

@property (weak, nonatomic) IBOutlet UIView *callFromMobileView;
@property (weak, nonatomic) IBOutlet UIButton *callFromMobileRadioButton;
@property (weak, nonatomic) IBOutlet UIButton *otherNumberRadioButton;

@property (weak, nonatomic) IBOutlet UIView *callBackServerView;
@property (weak, nonatomic) IBOutlet UIButton *callBackServerRadioButton;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *phoneNumbersTableView;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *otherPhoneNumberTextField;


@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) NSPredicate *filterCallFromMobile;
@property (nonatomic, strong) NSPredicate *filterCallMeBack;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;

@property (nonatomic, strong) FilteredSortedSectionedArray<PhoneNumber*> *phoneNumbersDataSource;

@property (nonatomic, strong) PhoneNumber *selectedPhoneNumber;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@property (nonatomic, strong) PhoneNumber *defaultPhoneNumber;
@end

@implementation JoinMeetingViewController

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        _phoneNumbersDataSource = [FilteredSortedSectionedArray new];
        
        _sectionedByName = ^NSString*(PhoneNumber *phoneNumber) {
            return @"";
        };
        _filterCallFromMobile = [NSPredicate predicateWithBlock:^BOOL(PhoneNumber *phoneNumber, NSDictionary<NSString *,id> * bindings) {
            if(phoneNumber.type == PhoneNumberTypeConference)
                return YES;
            return NO;
        }];
        _filterCallMeBack = [NSPredicate predicateWithBlock:^BOOL(PhoneNumber *phoneNumber, NSDictionary<NSString *,id> * bindings) {
            if(phoneNumber.type != PhoneNumberTypeConference)
                return YES;
            return NO;
        }];
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(PhoneNumber* obj1, PhoneNumber* obj2) {
            // Make the default country phone number on top of list
            if([obj1 isEqual:_defaultPhoneNumber])
                return NSOrderedAscending;
            if([obj2 isEqual:_defaultPhoneNumber])
                return NSOrderedDescending;
            // Return all others phone numbers ordered by contry name
            return [obj1.countryName compare:obj2.countryName options:NSCaseInsensitiveSearch];
        }];
        
        _phoneNumbersDataSource.sectionNameFromObjectComputationBlock = _sectionedByName;
        _phoneNumbersDataSource.globalFilteringPredicate = _filterCallFromMobile;
        _phoneNumbersDataSource.sectionSortDescriptor = _sortSectionAsc;
        _phoneNumbersDataSource.objectSortDescriptorForSection = @{@"__default__": @[_sortSectionAsc]};
    }
    return self;
}

-(void) viewDidLoad {
    [super viewDidLoad];
    [UITools applyCustomFontTo:_titleViewLabel];
    _titleViewLabel.text = NSLocalizedString(@"Join meeting", nil);
    [UITools applyCustomFontTo:_cancelButton.titleLabel];
    [_cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    _cancelButton.tintColor = [UITools defaultTintColor];
    [_callButton setTitle:NSLocalizedString(@"Call", nil) forState:UIControlStateNormal];
    _callButton.tintColor = [UITools defaultTintColor];
    
    [UITools applyCustomFontTo:_callFromMobileRadioButton.titleLabel];
    [UITools applyCustomFontTo:_callBackServerRadioButton.titleLabel];
    _callFromMobileRadioButton.tintColor = [UITools defaultTintColor];
    _callBackServerRadioButton.tintColor = [UITools defaultTintColor];
    [_callBackServerRadioButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    
    [_callFromMobileRadioButton setTitle:NSLocalizedString(@"Call from my mobile", nil) forState:UIControlStateNormal];
    [_callBackServerRadioButton setTitle:NSLocalizedString(@"Call me at", nil) forState:UIControlStateNormal];
    
    _otherNumberRadioButton.tintColor = [UITools defaultTintColor];
    [UITools applyCustomFontToTextField:_otherPhoneNumberTextField];
    _otherPhoneNumberTextField.placeholder = NSLocalizedString(@"Other phone number", nil);
    
    _phoneNumbersTableView.tableFooterView = [UIView new];
    _phoneNumbersTableView.emptyDataSetSource = self;
    _phoneNumbersTableView.emptyDataSetDelegate = self;
    
    [self adjustButtonsPositions];
    _footerViewHeightConstraint.constant = 0;
    _footerView.hidden = YES;
    
    NSString *savedOtherNumber = [[RainbowUserDefaults sharedInstance] objectForKey: kOtherNumberConferenceCallBackKey];
    if(savedOtherNumber){
        _otherPhoneNumberTextField.text = savedOtherNumber;
        _otherNumberRadioButton.selected = YES;
    }
    
    if([[RainbowUserDefaults sharedInstance] objectForKey: kSelectedConferenceCallBackNumberKey]){
        NSInteger selectedPhoneNumberIndex = [[[RainbowUserDefaults sharedInstance] objectForKey: kSelectedConferenceCallBackNumberKey] integerValue];
        if(selectedPhoneNumberIndex != NSNotFound && selectedPhoneNumberIndex != -1){
            _phoneNumbersDataSource.globalFilteringPredicate = _filterCallMeBack;
            NSString *key = [[_phoneNumbersDataSource sections] firstObject];
            if(key){
                NSArray * phoneNumbers = [_phoneNumbersDataSource objectsInSection:key];
                if(selectedPhoneNumberIndex < phoneNumbers.count)
                    _selectedIndexPath = [NSIndexPath indexPathForRow:selectedPhoneNumberIndex inSection:0];
            }
            _phoneNumbersDataSource.globalFilteringPredicate = _filterCallFromMobile;
        }
    }
    
    // DialOut value define the capability to use callBackServer radio button
    if(_room.conference.endpoint.isDialOutDisabled) {
        [_callBackServerRadioButton setEnabled:NO];
        _callBackServerRadioButton.tintColor = [UIColor lightGrayColor];
    } else {
        [_callBackServerRadioButton setEnabled:YES];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateMyContact:) name:kContactsManagerServiceDidUpdateMyContact object:nil];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_phoneNumbersDataSource reloadData];
    [_phoneNumbersTableView reloadData];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) setPhoneNumbers {
    [_phoneNumbersDataSource removeAllObjects];
    
    NSArray *phoneNumbers = _room.conference.endpoint.phoneNumbers;
    if(_room.isMyRoom && _room.conference.type == ConferenceTypeInstant){
        if(!_room.conference.endpoint.confEndpointId){
            // we have an instant meeting without any endpoint ... so use the data from the default instant pstn endpoint
            ConfEndpoint *instantBridge = [[ServicesManager sharedInstance].conferencesManagerService pstnInstantConferenceEndPoint];
            phoneNumbers = instantBridge.phoneNumbers;
        }
    }
    
    // Add the default phone number from the user country
    [phoneNumbers enumerateObjectsUsingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
        // This default phone number is used as default selected country
        if([[ServicesManager sharedInstance].myUser.contact.countryCode isEqualToString:aPhoneNumber.countryCode]) {
            _defaultPhoneNumber = aPhoneNumber;
        }

        [_phoneNumbersDataSource addObject:aPhoneNumber];
    }];
    
    [[ServicesManager sharedInstance].myUser.contact.phoneNumbers enumerateObjectsUsingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
        [_phoneNumbersDataSource addObject:aPhoneNumber];
    }];
}

-(void) setRoom:(Room *)room {
    _room = room;

    [self setPhoneNumbers];
    
    // Fetch potentially missing informations about the contact
    [[ServicesManager sharedInstance].contactsManagerService fetchRemoteContactDetail:[ServicesManager sharedInstance].myUser.contact];
}

-(void) didUpdateMyContact:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateMyContact:notification];
        });
        return;
    }
    
    [self setPhoneNumbers];

    [_phoneNumbersDataSource reloadData];
    [_phoneNumbersTableView reloadData];
}

#pragma mark - Buttons actions
- (IBAction)didTapCancelButton:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)didTapCallButton:(UIButton *)sender {
    if(_callBackServerRadioButton.selected){
        [[RainbowUserDefaults sharedInstance] removeObjectForKey:kSelectedConferenceCallBackNumberKey];
        if(_selectedIndexPath)
            [[RainbowUserDefaults sharedInstance] setObject:[NSNumber numberWithInteger: _selectedIndexPath.row] forKey:kSelectedConferenceCallBackNumberKey];
        if(_otherNumberRadioButton.selected){
            [[RainbowUserDefaults sharedInstance] removeObjectForKey:kOtherNumberConferenceCallBackKey];
            [[RainbowUserDefaults sharedInstance] setObject:_otherPhoneNumberTextField.text forKey:kOtherNumberConferenceCallBackKey];
        }
    }
    if([_delegate respondsToSelector:@selector(didSelectPhoneNumber:callBack:)]){
        PhoneNumber *number = nil;
        if(_selectedPhoneNumber)
            number = _selectedPhoneNumber;
        else {
            number = [PhoneNumber new];
            number.number = _otherPhoneNumberTextField.text;
        }
        
        [_delegate didSelectPhoneNumber:number callBack:_callBackServerRadioButton.selected];
    }
    [self didTapCancelButton:nil];
}

-(void) adjustButtonsPositions {
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGFloat desiredCallButtonConstraintConstant = screen.size.width * 0.20;
    CGFloat desiredCancelButtonConstraintConstant = -(screen.size.width * 0.20);
    
    void(^block)() = ^() {
        if(_selectedPhoneNumber || _otherPhoneNumberTextField.text.length > 0){
            _callButton.alpha = 1.0f;
            _callButtonCenterConstraint.constant = desiredCallButtonConstraintConstant;
            _cancelButtonCenterConstraint.constant = desiredCancelButtonConstraintConstant;
        } else {
            _callButton.alpha = 0;
            _callButtonCenterConstraint.constant = 0;
            _cancelButtonCenterConstraint.constant = 0;
        }
        [self.view layoutIfNeeded];
    };
    
    [UIView animateWithDuration:0.2 delay:0 options: UIViewAnimationOptionCurveLinear animations:block completion:nil];
}

#pragma mark - Radio button actions

- (IBAction)didTapCallFromMyMobileButton:(UIButton *)sender {
    if(_callFromMobileRadioButton.selected)
        return;
    
    _selectedPhoneNumber = nil;
    _callBackServerRadioButton.selected = NO;
    _callFromMobileRadioButton.selected = YES;
    
    _phoneNumbersDataSource.globalFilteringPredicate = _filterCallFromMobile;
    [_phoneNumbersDataSource reloadData];
    [_phoneNumbersTableView reloadData];
    [self adjustButtonsPositions];
    _footerViewHeightConstraint.constant = 0;
    _footerView.hidden = YES;
    [_otherPhoneNumberTextField resignFirstResponder];
}

- (IBAction)didTapCallMeBackButton:(UIButton *)sender {
    if(_callBackServerRadioButton.selected)
        return;
    
    _selectedPhoneNumber = nil;
    _callBackServerRadioButton.selected = YES;
    _callFromMobileRadioButton.selected = NO;
    
    _phoneNumbersDataSource.globalFilteringPredicate = _filterCallMeBack;
    [_phoneNumbersDataSource reloadData];
    [_phoneNumbersTableView reloadData];
    
    _footerViewHeightConstraint.constant = 48;
    _footerView.hidden = NO;
    if(_selectedIndexPath){
        [_phoneNumbersTableView selectRowAtIndexPath:_selectedIndexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
        [self tableView:_phoneNumbersTableView didSelectRowAtIndexPath:_selectedIndexPath];
    }
    
    // check if the user has no phone number set in his profile
    if([ServicesManager sharedInstance].myUser.contact.phoneNumbers.count == 0){
        _otherNumberRadioButton.selected = YES;
        [_otherPhoneNumberTextField becomeFirstResponder];
        _footerViewHeightConstraint.constant = self.view.bounds.size.height - (2*24 + 26 + 24 + 30);
    }
    
    [self adjustButtonsPositions];
}

- (IBAction)didTapOtherNumberRadioButton:(UIButton *)sender {
    sender.selected = YES;
    [_otherPhoneNumberTextField becomeFirstResponder];
}

#pragma mark - Textfield
- (IBAction)otherNumberTextFieldDidBeginEditing:(UITextField *)sender {
    _otherNumberRadioButton.selected = YES;
    if(_selectedIndexPath)
        [_phoneNumbersTableView deselectRowAtIndexPath:_selectedIndexPath animated:YES];
    _selectedIndexPath = nil;
    _selectedPhoneNumber = nil;
    [self adjustButtonsPositions];
}
- (IBAction)otherNumberTextFieldDidEndEditing:(UITextField *)sender {
    NSLog(@"Did end editing %@", sender);
}
- (IBAction)otherNumberTextFieldDidChanged:(UITextField *)sender {
    [self adjustButtonsPositions];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_phoneNumbersDataSource sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_phoneNumbersDataSource sections] objectAtIndex:section];
    return [[_phoneNumbersDataSource objectsInSection:key] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionName = [[_phoneNumbersDataSource sections] objectAtIndex:section];
    if([_phoneNumbersDataSource objectsInSection:sectionName].count > 0){
        return NSLocalizedString(sectionName, nil);
    }
    return @"";
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
    tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools boldFontName] size:18.0f];
    tableViewHeaderFooterView.textLabel.textColor = [UITools defaultTintColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JoinMeetingTableViewCell *cell = (JoinMeetingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:joinMeetingPhoneNumberCellID];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *key = [[_phoneNumbersDataSource sections] objectAtIndex:indexPath.section];
    PhoneNumber *phoneNumber = [[_phoneNumbersDataSource objectsInSection:key] objectAtIndex:indexPath.row];
    cell.phoneNumber = phoneNumber;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_phoneNumbersDataSource sections] objectAtIndex:indexPath.section];
    PhoneNumber *phoneNumber = [[_phoneNumbersDataSource objectsInSection:key] objectAtIndex:indexPath.row];
    _selectedPhoneNumber = phoneNumber;
    
    if(_callFromMobileRadioButton.selected){
        [self didTapCallButton:_callButton];
        return;
    }
    
    [self adjustButtonsPositions];
    if(_callBackServerRadioButton.selected){
        _selectedIndexPath = indexPath;
        [_otherPhoneNumberTextField resignFirstResponder];
        _otherNumberRadioButton.selected = NO;
        _otherPhoneNumberTextField.text = @"";
    }
}

#pragma mark - DZNEmptyDataSet

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 20.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return NO;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}
@end
