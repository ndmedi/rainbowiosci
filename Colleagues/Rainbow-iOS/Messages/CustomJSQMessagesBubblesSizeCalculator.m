/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 *
 * Created by Vladimir Vyskocil on 01/12/16.
 */

#import "CustomJSQMessagesBubblesSizeCalculator.h"
#import "JSQMessagesCollectionView.h"
#import "JSQMessagesCollectionViewDataSource.h"
#import "JSQMessagesCollectionViewFlowLayout.h"
#import "JSQMessageData.h"
#import "GroupedMessages.h"
#import "UITools.h"
#import "UIImage+JSQMessages.h"
#import "JSQMessagesTimestampFormatter.h"
#import "File+JSQMessageMediaData.h"

@interface JSQMessagesBubblesSizeCalculator ()
@property (strong, nonatomic, readonly) NSCache *cache;
@end

@implementation CustomJSQMessagesBubblesSizeCalculator

- (id)init
{
    self = [super init];
    if (self != nil) {
        _isMUC = NO;
    }
    
    return self;
}

- (CGSize)messageBubbleSizeForMessageData:(id<JSQMessageData>)messageData
                              atIndexPath:(NSIndexPath *)indexPath
                               withLayout:(JSQMessagesCollectionViewFlowLayout *)layout
{

//    NSValue *cachedSize = [self.cache objectForKey:@([messageData messageHash])];
//    if (cachedSize != nil) {
//        if([messageData isKindOfClass:[GroupedMessages class]]){
//            GroupedMessages *groupedMessages = (GroupedMessages *)messageData;
//            // if the message has a thumbnail image don't returned chached size
//            if(!groupedMessages.lastMessage.attachment){
//                return [cachedSize CGSizeValue];
//            }
//        }
//    }
    
    CGSize size = [super messageBubbleSizeForMessageData:messageData atIndexPath:indexPath withLayout:layout];
    
    
    size.height = size.height + 26;
    // Add height for incoming messages to take account of the sender name label when in a MUC
    if (self.isMUC && ![[messageData senderId] isEqualToString:[layout.collectionView.dataSource senderId]]) {
        size.height += 18;
    }
    if(size.width < 200){
        size.width = 200;
    }
    
    if([messageData isKindOfClass:[GroupedMessages class]]){
        GroupedMessages *groupedMessages = (GroupedMessages *)messageData;
        // if the message has a thumbnail image account of its height
        if(groupedMessages.lastMessage.attachment){
            if((groupedMessages.lastMessage.attachment.thumbnailData || groupedMessages.lastMessage.attachment.data)&&
               (groupedMessages.lastMessage.attachment.hasThumbnailOnServer)){
               
                size.width = groupedMessages.lastMessage.attachment.mediaViewDisplaySize.width;
                size.height += groupedMessages.lastMessage.attachment.mediaViewDisplaySize.height +20;
                
                // check if image without text
                NSString *msg = groupedMessages.lastMessage.body;

                if (msg == nil) {
                    size.height -= 29;
                }
                else if (([msg rangeOfString:@"IMG_"].location != NSNotFound && [msg rangeOfString:@"."].location != NSNotFound) || !msg.length || [msg hasSuffix:@".jpg"] || [msg hasSuffix:@".png"]) {
                    size.height -= 29;
                }
                else{
                     size.height -= 4;
                }

            } else {
                // Icon 32 + filename label 30 + filesize label 30 + imageview top constraint 10
                size.height += 82;
                size.width += 60;
                
            }
            
            
            // For the file name
            if(_isMUC)
                size.height += 10;
        }
        else{
            size.height += 14;
        }
        
        
        
        if( size.width > ([[UIScreen mainScreen] bounds].size.width)* 0.66 + 20)
            size.width = ([[UIScreen mainScreen] bounds].size.width)* 0.66 + 20;
        
        if(!groupedMessages.isMediaMessage){
            if(groupedMessages.lastMessage && groupedMessages.lastMessage.type == MessageTypeGroupChatEvent){
                size.height = 74;
            }
            if(groupedMessages.lastMessage && groupedMessages.lastMessage.type == MessageTypeWebRTC)
                size.height = 60;
        }
    }
    
    [self.cache setObject:[NSValue valueWithCGSize:size] forKey:@([messageData messageHash])];
    return size;
}

@end
