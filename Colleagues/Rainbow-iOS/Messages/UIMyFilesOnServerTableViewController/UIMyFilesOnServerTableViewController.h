/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import <Rainbow/File.h>
#import <Rainbow/Peer.h>

@interface UIMyFilesOnServerTableViewController : UITableViewController
@property (nonatomic, assign) id fromView;
@property (nonatomic, strong) Peer *peer;

@end
