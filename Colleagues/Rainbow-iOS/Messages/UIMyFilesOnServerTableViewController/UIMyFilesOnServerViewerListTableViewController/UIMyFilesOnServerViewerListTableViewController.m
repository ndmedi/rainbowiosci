/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIMyFilesOnServerViewerListTableViewController.h"
#import "UITools.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "UIRainbowGenericTableViewCell.h"
#import "Contact+UIRainbowGenericTableViewCellProtocol.h"
#import <Rainbow/ServicesManager.h>
#import "SCLAlertView.h"
#import "SCLAlertView+Rotation.h"

@interface UIRainbowGenericTableViewCell (RightButton)
@property (readonly) IBOutlet UIButton *rightButton;
@end

@interface UIMyFilesOnServerViewerListTableViewController () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@end

@implementation UIMyFilesOnServerViewerListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.allowsSelection = NO;
    self.navigationController.navigationBar.translucent = NO;
    self.title = NSLocalizedString(@"Who see this file", nil);
    
    _doneButton.title = NSLocalizedString(@"Close", nil);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _file.viewers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Peer * peer = [_file.viewers objectAtIndex:indexPath.row];
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];

    cell.cellObject = (Contact*)peer;
    [cell.rightButton setImage:[UIImage imageNamed:@"removeViewer"] forState:UIControlStateNormal];
    [cell.rightButton setImage:[UIImage imageNamed:@"removeViewer"] forState:UIControlStateHighlighted];
    __weak typeof(cell) weakCell = cell;
    cell.cellButtonTapHandler = ^(UIButton *sender) {
        BOOL showWarning = [[NSUserDefaults standardUserDefaults] boolForKey:@"dontShowRemoveViewerWarningAlert"];
        
        if(!showWarning){
            SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
            
            SCLSwitchView *switchView = [alert addSwitchViewWithLabel:NSLocalizedString(@"Don’t show this message again", nil)];
            
            SCLButton *yesButton = [alert addButton:NSLocalizedString(@"OK", nil) actionBlock:^(void) {
                [[NSUserDefaults standardUserDefaults] setBool:switchView.isSelected forKey:@"dontShowRemoveViewerWarningAlert"];
                [self removeViewer:(Contact *)weakCell.cellObject fromFile:_file];
            }];
            
            alert.customViewColor = [UITools defaultTintColor];
            alert.iconTintColor = [UITools defaultBackgroundColor];
            alert.backgroundViewColor = [UITools defaultBackgroundColor];
            alert.statusBarStyle = UIStatusBarStyleLightContent;
            [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
            [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
            alert.shouldDismissOnTapOutside = YES;
            alert.labelTitle.numberOfLines = 3;
            
            yesButton.buttonFormatBlock = ^NSDictionary* (void) {
                return @{@"backgroundColor":[UIColor redColor]};
            };
            
            switchView.labelFont = [UIFont fontWithName:[UITools defaultFontName] size:12];
            if([peer isKindOfClass:[Room class]]){
                [alert showQuestion:self title:NSLocalizedString(@"Unshare shared file", nil) subTitle:NSLocalizedString(@"The participants of this bubble will no longer be able to see this file", nil) closeButtonTitle:NSLocalizedString(@"Cancel", nil) duration:0.0f];
            } else {
                [alert showQuestion:self title:NSLocalizedString(@"Unshare shared file", nil) subTitle:NSLocalizedString(@"This user will no longer be able to see this file", nil) closeButtonTitle:NSLocalizedString(@"Cancel", nil) duration:0.0f];
            }
        } else {
            [self removeViewer:(Contact *)weakCell.cellObject fromFile:_file];
        }  
    };
    return cell;
}

-(void) removeViewer:(Peer *) peer fromFile:(File *) file {
    __weak typeof(self) weakSelf = self;
    [[ServicesManager sharedInstance].fileSharingService removeViewer:peer fromFile:file completionHandler:^(File *file, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.file = file;
            [weakSelf.tableView reloadData];
        });
    }];
}

#pragma mark - DZNEmptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"";
    
    text = NSLocalizedString(@"No user can view this file", nil);
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools boldFontName] size:18.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 20.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}
- (IBAction)didTapOnDoneButton:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
