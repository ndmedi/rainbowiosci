/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIMyFilesOnServerTableViewController.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/FileSharingService.h>
#import "UIMyFilesOnServerTableViewCell.h"
#import "UITools.h"
#import "MBProgressHUD.h"
#import "UIScrollView+APParallaxHeader.h"
#import <Rainbow/NSString+FileSize.h>
#import "UIPreviewQuickLookController.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "FilteredSortedSectionedArray.h"
#import <Rainbow/NSDate+Utilities.h>
#import "NSDate+WithoutDays.h"
#import "BGTableViewRowActionWithImage.h"
#import "CustomNavigationController.h"
#import "UIMyFilesOnServerViewerListTableViewController.h"
#import "MyInfosTableViewController.h"
#import "UIOrganizeTableViewController.h"
#import "UIStoryboardManager.h"


#define kTableViewRowHeight 70

typedef NS_ENUM(NSInteger, SortOrder){
    SortOrderByDate= 0,
    SortOrderByName,
    SortOrderBySize,
    SortOrderStoredValue
};

@interface UIMyFilesOnServerTableViewController () <UIMyFilesOnServerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, UIPopoverPresentationControllerDelegate,OrganizeControllerDelegate>{
    NSUInteger currentOffset;
    NSUInteger totalFilesNumber;
    Contact *myContact;
    BOOL isTriggerLoadMore;
}
@property (nonatomic, strong) ServicesManager *servicesManager;
@property (nonatomic, strong) FileSharingService *fileSharingService;
@property (strong, nonatomic) IBOutlet UIView *quotaView;
@property (weak, nonatomic) IBOutlet UIProgressView *quotaProgressView;
@property (weak, nonatomic) IBOutlet UILabel *currentSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *quotaViewTitle;
@property (nonatomic, strong) FilteredSortedSectionedArray <File *> *allFiles;
@property (nonatomic, strong) NSObject *allFilesMutex;

@property (nonatomic, strong) NSString *olderThan6MonthsTrad;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *longDateFormatter;

// All the filter/sorter/computation blocks used for the previous datamodel
@property (nonatomic, strong) SectionNameComputationBlock sectionedByDate;
@property (nonatomic, strong) NSSortDescriptor *sortSectionByDate;
@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) SectionNameComputationBlock sectionedBySize;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;
@property (nonatomic, strong) NSSortDescriptor *sortSizeAsc;

@property (nonatomic) BOOL isSorting;
@property (nonatomic) SortOrder selectedSortOrder;

@property (nonatomic) BOOL isFiltering;
@property (nonatomic) FilterFileType selectedFileType;

@property (nonatomic, strong) NSArray *letters;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *organizeButtonItem;
@property (nonatomic, strong) UIOrganizeTableViewController* organizeFilesTableViewController;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@end

@implementation UIMyFilesOnServerTableViewController

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
         _letters = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", @"#"];
        _servicesManager = [ServicesManager sharedInstance];
        _fileSharingService = _servicesManager.fileSharingService;
        _allFiles = [FilteredSortedSectionedArray new];
        _allFilesMutex = [NSObject new];
        
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:kDateStyleShortWithoutDays];
        
        _longDateFormatter = [[NSDateFormatter alloc] init];
        NSString *dateComponents = kDateStyleLongWithoutDays;
        NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:dateComponents options:0 locale:[NSLocale currentLocale]];
        [_longDateFormatter setDateFormat:dateFormat];
        _longDateFormatter.locale = [NSLocale currentLocale];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateAttachment:) name:kFileSharingServiceDidUpdateFile object:nil];
        
        // How to sort the section names
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
        }];
        _sortSizeAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO comparator:^(NSString * string1,             NSString * string2) {
              return [string1 compare:string2 options:NSNumericSearch];
        }];
        _sectionedByName = ^NSString*(File *file) {
            return [[file.fileName substringToIndex:1] uppercaseString];
        };
        _sectionedBySize = ^NSString*(File *file) {
            return [NSString stringWithFormat:@"%@",  @(file.size)];
        };

    }
    return self;
}


-(void) dealloc {
    @synchronized (_allFilesMutex) {
        [_allFiles removeAllObjects];
        _allFiles = nil;
    }
    _allFilesMutex = nil;
    _dateFormatter = nil;
    _longDateFormatter = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidUpdateFile object:nil];
}

-(void) shouldUpdateAttachment:(NSNotification *) notification {
    File * file = (File *) notification.object;
    dispatch_async(dispatch_get_main_queue(), ^{
        //        NSMutableArray *toHandleArray = [NSMutableArray array];
        //        for (File *aFile in _allFiles) {
        //            if ([file.url isEqual:aFile.url]) {
        //
        //                if (file.isDownloadAvailable) {
        //                   [toHandleArray addObject:aFile];
        //                }
        //                else{
        //                    [toHandleArray addObject:file];
        //                }
        //            }
        //        }
        //
        //        for (File *theFile in toHandleArray) {
        //             [_allFiles removeObject:theFile];
        //            if (file.isDownloadAvailable ) {
        //                [_allFiles addObject:theFile];
        //            }
        //
        //        }
        
        [self.tableView reloadData];
        [self adjustQuotaViewContent];
        
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentOffset = 0;
    isTriggerLoadMore = NO;
    
    self.tableView.rowHeight = kTableViewRowHeight;
    
    if(_peer)
        self.title = NSLocalizedString(@"Received files", nil);
    else
        self.title = NSLocalizedString(@"My Rainbow share", nil);
    
    NSDate *sixMonthInPastFromNow = [[NSDate date] dateBySubtractingMonths:6];
    _olderThan6MonthsTrad = NSLocalizedString(@"Older than 6 months", nil);
    __weak typeof(_olderThan6MonthsTrad) weakTrad = _olderThan6MonthsTrad;
    
    _sectionedByDate = ^NSString*(File *aFile) {
        if([aFile.uploadDate isEarlierThanDate:sixMonthInPastFromNow])
            return weakTrad;
        if(aFile.uploadDate)
            return aFile.uploadDate.shortDateStringWithoutDays;
        else if (aFile.dateToSort)
            return aFile.dateToSort.shortDateStringWithoutDays;
        else
            return weakTrad;
    };
    _allFiles.objectSortDescriptorForSection = @{@"__default__": @[[self sortDescriptorByDateToSort]]};
    
    _allFiles.sectionNameFromObjectComputationBlock = _sectionedByDate;
    _allFiles.sectionSortDescriptor = [self sortDescriptorMonth];
    _allFiles.globalFilteringPredicate = nil;
    
    [UITools applyCustomFontTo:_maxSizeLabel];
    [UITools applyCustomFontTo:_currentSizeLabel];
    [UITools applyCustomFontTo:_quotaViewTitle];
    _quotaProgressView.tintColor = [UITools defaultTintColor];
    _quotaView.backgroundColor = [UITools defaultBackgroundColor];
    
    if(!_peer){
        _quotaViewTitle.text = NSLocalizedString(@"Your quota", nil);
        _maxSizeLabel.text = [NSString stringWithFormat:@"%ld Gb", (long)[ServicesManager sharedInstance].fileSharingService.maxQuotaSize];
        [self adjustQuotaViewContent];
        [self.tableView addParallaxWithView:_quotaView andHeight:_quotaView.frame.size.height andMinHeight:_quotaView.frame.size.height andShadow:NO];
    }
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView setBackgroundColor:[UITools defaultBackgroundColor]];
    
    if([_fromView isKindOfClass:[MyInfosTableViewController class]]){
        // No ok button when comming from MyInfosTableViewController
        self.navigationItem.rightBarButtonItems = @[_organizeButtonItem];
    } else {
        // No peer means that we open the My Rainbow sharing from the conversation
        if(!_peer){
            self.navigationItem.rightBarButtonItems = @[_doneButton, _organizeButtonItem];
        } else {
            self.navigationItem.rightBarButtonItems = nil;
        }
    }
    
    self.tableView.parallaxView.layer.zPosition = 1;
    [self getStoredSelections];
   
}

-(void) fetchAllFilesWithType:(FilterFileType)typeMIME {

    
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self fetchAllFilesWithType:typeMIME];
        });
        return;
    }
    myContact = [ServicesManager sharedInstance].myUser.contact;
    __weak typeof (self) weakSelf = self;
    if(!_peer){
        [_allFiles removeAllObjects];
        [_fileSharingService.files enumerateObjectsUsingBlock:^(File * file, NSUInteger idx, BOOL * stop) {
                @synchronized (_allFilesMutex) {
                    
                    if(![_allFiles containsObject:file])
                        [_allFiles addObject:file];
                }
        }];
        [_fileSharingService refreshSharedFileListFromOffset:currentOffset withLimit:50 withTypeMIME:typeMIME withCompletionHandler:^(NSArray<File *> *files,NSUInteger offset , NSUInteger total, NSError *error) {
            
            if (error || !files) {
                NSLog(@"error while trying to fetch filed from server %@",error);
                return;
            }
            
            currentOffset = offset += 50;
            totalFilesNumber = total;
//            [_allFiles removeAllObjects];
//
//            [files enumerateObjectsUsingBlock:^(File * file, NSUInteger idx, BOOL * stop) {
//                    @synchronized (_allFilesMutex) {
//
//                        if(![_allFiles containsObject:file])
//                            [_allFiles addObject:file];
//                    }
//            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                @synchronized (_allFilesMutex) {
                    [_allFiles removeAllObjects];
                    for (int i=0; i< files.count; i++) {
                        File *f = files[i];
                        if(![_allFiles containsObject:f])
                            [_allFiles addObject:f];
                    }
                }
                _quotaViewTitle.text = NSLocalizedString(@"Your quota", nil);
                _maxSizeLabel.text = [NSString stringWithFormat:@"%ld Gb", (long)[ServicesManager sharedInstance].fileSharingService.maxQuotaSize];
                [self adjustQuotaViewContent];
                [weakSelf.tableView reloadData];
            });
        }];
    } else {
        // Fetch files for a peer
        
        MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString(@"Loading files ...",nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
        
        [hud show:YES];
        
        [_fileSharingService loadSharedFilesWithPeer:_peer fromOffset:0 completionHandler:^(NSArray<File *> *files, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hide:YES];
                if(!error){
//                    [files enumerateObjectsUsingBlock:^(File * aFile, NSUInteger idx, BOOL * stop) {
//                        @synchronized (_allFilesMutex) {
//                            if(![_allFiles containsObject:aFile])
//                                [_allFiles addObject:aFile];
//                        }
//                    }];
                    @synchronized (_allFilesMutex) {
                        for (int i=0; i< files.count; i++) {
                            File *f = files[i];
                            if(![_allFiles containsObject:f])
                                [_allFiles addObject:f];
                        }
                    }
                    [weakSelf.tableView reloadData];
                }
            });
        }];
    }
}

-(void) getStoredSelections {
    NSInteger sortOrder = SortOrderByDate;
    NSInteger filterOrder = FilterFilesAll;
    
    if([_fromView isKindOfClass:[MyInfosTableViewController class]] || !_peer){
        NSNumber *previousSortOrder = (NSNumber*) [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedSortType"];
        NSNumber *previousFilterOrder = (NSNumber*) [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedFilterType"];
        if(previousSortOrder){
            sortOrder = [previousSortOrder integerValue];
            _selectedSortOrder = sortOrder;
        }
        if(previousFilterOrder) {
            filterOrder = [previousFilterOrder integerValue];
            _selectedFileType = filterOrder;
        }
    }


    [self willSortBySortOrder:sortOrder];
    [self willFilterByFilterType:filterOrder];
}

-(void) adjustQuotaViewContent {
    NSInteger currentSize = (long)[ServicesManager sharedInstance].fileSharingService.currentSize;
    _currentSizeLabel.text = [NSString formatFileSize:currentSize];
    unsigned long long max = [_maxSizeLabel.text fileSizeFromFormat];
    _quotaProgressView.progress = (double)currentSize/(double)max;
    if(_quotaProgressView.progress >= 0.8 && _quotaProgressView.progress <= 0.9){
        _quotaProgressView.tintColor = [UIColor orangeColor];
    } else if(_quotaProgressView.progress >= 0.9){
        _quotaProgressView.tintColor = [UIColor redColor];
    } else {
        _quotaProgressView.tintColor = [UITools defaultTintColor];
    }
}

-(NSSortDescriptor *) sortDescriptorMonth {
    NSSortDescriptor *sortMonth = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO comparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
        if([obj1 isEqualToString:_olderThan6MonthsTrad] && [obj2 isEqualToString:_olderThan6MonthsTrad])
            return NSOrderedAscending;
        
        NSDate *date1 = [_dateFormatter dateFromString:obj1];
        NSDate *date2 = [_dateFormatter dateFromString:obj2];
        
        return [[NSDate currentCalendar] compareDate:date1 toDate:date2 toUnitGranularity:NSCalendarUnitMonth];
    }];
    return sortMonth;
}

-(NSSortDescriptor *) sortDescriptorByDateToSort {
    NSSortDescriptor *sortByLastMessageDate = [NSSortDescriptor sortDescriptorWithKey:@"dateToSort" ascending:NO comparator:^NSComparisonResult(NSDate* date1, NSDate* date2) {
        return [date1 compare:date2];
    }];
    return sortByLastMessageDate;
}

-(NSSortDescriptor *) sortDescriptorByUploadDate {
    NSSortDescriptor *sortByLastMessageDate = [NSSortDescriptor sortDescriptorWithKey:@"uploadDate" ascending:NO comparator:^NSComparisonResult(NSDate* date1, NSDate* date2) {
        return [date1 compare:date2];
    }];
    return sortByLastMessageDate;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_allFiles sections] count];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
  if(_selectedSortOrder == SortOrderByName)
        return _letters;
   else
        return nil;
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [[_allFiles sections] indexOfObject:title];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_allFiles sections] objectAtIndex:section];
    return [[_allFiles objectsInSection:key] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if(_selectedSortOrder == SortOrderByDate) {
    NSString *title = [[_allFiles sections] objectAtIndex:section];
    if([title isEqualToString:_olderThan6MonthsTrad])
        return title;
    
    NSDate *date = [_dateFormatter dateFromString:title];
    if([date isThisYear]){
        return [[_dateFormatter.monthSymbols objectAtIndex:date.month-1] capitalizedString];
    } else {
        
        return [_longDateFormatter stringFromDate:date];
    }
    }
    if(_selectedSortOrder == SortOrderBySize)
        return @"";
    else
    return [[_allFiles sections] objectAtIndex:section];

}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
    tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools boldFontName] size:18.0f];
    tableViewHeaderFooterView.textLabel.textColor = [UITools defaultTintColor];
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == [[_allFiles sections] count]-1 && isTriggerLoadMore && !_peer) {
        
        UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
        headerView.backgroundColor = [UIColor clearColor];
        
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(tableView.frame.size.width/2 -22, 0, 44, 44)];
        spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [headerView addSubview:spinner];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            //back to the main thread for the UI call
            dispatch_async(dispatch_get_main_queue(), ^{
                [spinner startAnimating];
            });
            
        });
        
        return headerView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == [[_allFiles sections] count]-1 && isTriggerLoadMore && !_peer) {
        return 44.0;
    } else {
        return 0.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIMyFilesOnServerTableViewCell *cell = (UIMyFilesOnServerTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"myFileOnServerCell" forIndexPath:indexPath];
    
    NSString *key = [[_allFiles sections] objectAtIndex:indexPath.section];
    File *aFile = [[_allFiles objectsInSection:key] objectAtIndex:indexPath.row];
   
    cell.file = aFile;
    cell.delegate = self;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *key = [[_allFiles sections] objectAtIndex:indexPath.section];
    File *theFile = [[_allFiles objectsInSection:key] objectAtIndex:indexPath.row];
    
    if(!_peer){
        if(![_fromView isKindOfClass:[MyInfosTableViewController class]]){
            [self onDoneClicked:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"wantToSendFile" object:theFile];
        } else {
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            UINavigationController *ctrl = [self.storyboard instantiateViewControllerWithIdentifier:@"viewersListNavControllerID"];
            UIMyFilesOnServerViewerListTableViewController *viewersList = [ctrl.viewControllers firstObject];
            viewersList.file = theFile;
            viewersList.preferredContentSize = CGSizeMake(300, 300);
            
            ctrl.modalPresentationStyle = UIModalPresentationPopover;
            ctrl.preferredContentSize = [viewersList preferredContentSize];
            UIPopoverPresentationController *popoverCtrl = ctrl.popoverPresentationController;
            popoverCtrl.delegate = self;
            popoverCtrl.sourceView = self.view;
            popoverCtrl.sourceRect = cell.frame;
            [popoverCtrl setPermittedArrowDirections:UIPopoverArrowDirectionAny];
            popoverCtrl.backgroundColor = [UITools defaultTintColor];
            
            [self presentViewController:ctrl animated:YES completion:nil];
        }
    } else {
        UIMyFilesOnServerTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [self didTapOnImageForFile:theFile forCell:cell];
    }
}

- (UIModalPresentationStyle) adaptivePresentationStyleForPresentationController: (UIPresentationController * ) controller {
    return UIModalPresentationNone;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return !_peer;
}

-(NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *key = [[_allFiles sections] objectAtIndex:indexPath.section];
    File *theFile = [[_allFiles objectsInSection:key] objectAtIndex:indexPath.row];
    
    BGTableViewRowActionWithImage *delete = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Delete", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"removeContact"] forCellHeight:kTableViewRowHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self deleteActionForFile:theFile atKey:key indexPath:indexPath];
    }];
    delete.accessibilityLabel = NSLocalizedString(@"Delete", nil);
    return @[delete];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Check scrolled percentage
    CGFloat yOffset = tableView.contentOffset.y;
    CGFloat height = tableView.contentSize.height - tableView.frame.size.height;
    CGFloat scrolledPercentage = yOffset / height;
    
    // Check if all the conditions are met to allow loading the next page
    if (scrolledPercentage > .88f && !isTriggerLoadMore && currentOffset < totalFilesNumber && !_peer){
        // This is the bottom of the table view, load more data here.
        __weak typeof (self) weakSelf = self;
        isTriggerLoadMore = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView reloadData];
        });
        NSLog(@"load more here!");
        [_fileSharingService refreshSharedFileListFromOffset:currentOffset withLimit:50 withTypeMIME:_selectedFileType  withCompletionHandler:^(NSArray<File *> *files,NSUInteger offset , NSUInteger total, NSError *error) {
            
            if (error || !files) {
                NSLog(@"error while trying to fetch filed from server %@",error);
                return;
            }
            currentOffset = offset += 50;
            totalFilesNumber = total;
            
//            [files enumerateObjectsUsingBlock:^(File * file, NSUInteger idx, BOOL * stop) {
//                    @synchronized (_allFilesMutex) {
//                        if(![_allFiles containsObject:file])
//                            [_allFiles addObject:file];
//                    }
//            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                @synchronized (_allFilesMutex) {
                    for (int i=0; i< files.count; i++) {
                        File *f = files[i];
                        if(![_allFiles containsObject:f])
                            [_allFiles addObject:f];
                    }
                }
                isTriggerLoadMore = NO;
                [weakSelf.tableView reloadData];
            });
        }];
    }
}

-(void) deleteActionForFile:(File *) file atKey:(NSString *) key indexPath:(NSIndexPath *) indexPath {
    // Delete the row from the data source
    MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString(@"Removing file...",nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    
    [hud showAnimated:YES whileExecutingBlock:^{
        [_fileSharingService deleteFile:file];
    } completionBlock:^{
        [self showHUDWithMessage:NSLocalizedString(@"File removed",nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([_allFiles containsObject:file]) {
                    [_allFiles removeObject:file];
                    if([_allFiles objectsInSection:key].count == 0)
                        [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
                    else
                        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    [self adjustQuotaViewContent];
                }
                
            });
            
        }];
    }];
    self.tableView.editing = NO;
}

- (IBAction)onDoneClicked:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(MBProgressHUD *) showHUDWithMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = message;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}

-(void) didTapOnImageForFile:(File *) file forCell:(UIMyFilesOnServerTableViewCell *) cell {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if(file.data){
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [self openPreviewControllerForAttachment:file];
    } else {
        MBProgressHUD *downloadHud = [self showHUDWithMessage:NSLocalizedString(@"Downloading file ...",nil) mode:MBProgressHUDModeIndeterminate imageName:nil dismissCompletionBlock:nil];
        [downloadHud show:YES];
        
        [[ServicesManager sharedInstance].fileSharingService downloadDataForFile:file withCompletionHandler:^(File *aFile, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [downloadHud hide:YES];
                if(!error){
                    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    [self openPreviewControllerForAttachment:aFile];
                } else {
                    MBProgressHUD *errorHud = [self showHUDWithMessage:NSLocalizedString(@"Error", nil) mode:MBProgressHUDModeCustomView imageName:@"Failed" dismissCompletionBlock:nil];
                    [errorHud showAnimated:YES whileExecutingBlock:^{
                        [NSThread sleepForTimeInterval:3];
                    } completionBlock:^{
                        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }];
                }
            });
        }];
    }
}

-(void) openPreviewControllerForAttachment:(File *) attachment {
    // Instanciate a QLPreview​Controller
    MBProgressHUD *openingHud = [self showHUDWithMessage:NSLocalizedString(@"Opening file ...",nil) mode:MBProgressHUDModeIndeterminate imageName:nil dismissCompletionBlock:nil];
    [openingHud show:YES];
    UIPreviewQuickLookController *previewController = [UIPreviewQuickLookController new];
    previewController.attachment = attachment;
    [self presentViewController:previewController animated:YES completion:^{
        [openingHud hide:YES];
    }];
}

-(MBProgressHUD *) showHUDWithMessage:(NSString *) message mode:(MBProgressHUDMode) mode imageName:(NSString *) imageName dismissCompletionBlock:(void (^)()) dismissCompletion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = message;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}

#pragma mark - DZNEmptyDataSet
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"EmptyFiles"];
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultTintColor];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"";
    if(_peer)
        text = NSLocalizedString(@"Other users have not yet shared files with you.", nil);
    else
        text = NSLocalizedString(@"Use your Rainbow storage space to share any kind of file with other users.", nil);
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    if(_peer)
        return nil;
    NSString *text = @"";
    text = NSLocalizedString(@"Files you will share in conversations will appear here.", nil);
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    CGFloat offset = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
    offset += CGRectGetHeight(self.navigationController.navigationBar.frame);
    if(!_peer)
        offset -= CGRectGetHeight(_quotaView.frame);
    return -offset;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 0.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

#pragma mark - organizeButton Action
- (IBAction)organizeButtonClicked:(UIBarButtonItem *) sender {
    BOOL toggle = !(_organizeFilesTableViewController.view.superview) ? YES : NO;
    if(toggle){
         [self.tableView setContentOffset:CGPointMake(0.0f, -self.tableView.contentInset.top) animated:NO];
        _organizeFilesTableViewController = [[UIStoryboardManager sharedInstance].organizeStoryBoard instantiateViewControllerWithIdentifier:@"organizeStoryboardID"];
        _organizeFilesTableViewController.delegate = self;
        _organizeFilesTableViewController.sortArray = @[NSLocalizedString(@"Date", nil) ,NSLocalizedString(@"Name", nil), NSLocalizedString(@"Size", nil)];
        
        _organizeFilesTableViewController.filterArray = @[NSLocalizedString(@"All", nil),NSLocalizedString(@"Images", nil), NSLocalizedString(@"Audio/Video", nil),NSLocalizedString(@"PDF", nil), NSLocalizedString(@"Microsoft Office", nil)];
        _organizeFilesTableViewController.selectedSortIndex= _selectedSortOrder;
        _organizeFilesTableViewController.selectedFilterIndex= _selectedFileType;
        _organizeFilesTableViewController.isSubView = YES;
        _organizeFilesTableViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        
        [_organizeFilesTableViewController setViewHorizontalPosition:-self.tableView.parallaxView.frame.size.height];
        [self.view addSubview:_organizeFilesTableViewController.view];
        [self.view bringSubviewToFront:_organizeFilesTableViewController.view];
        // Move the view to the view layer to be placed on top of the parralax view
        _organizeFilesTableViewController.view.layer.zPosition = 1;
         self.tableView.scrollEnabled = NO;
    } else {
        [_organizeFilesTableViewController.view removeFromSuperview];
        self.tableView.userInteractionEnabled = YES;
        self.tableView.scrollEnabled=YES;
    }
    
}
-(void) drawGoodImageForOrganizeButtonItem {
    if(_isFiltering || _isSorting) {
        _organizeButtonItem.image = [UIImage imageNamed:@"filterOn"];
    } else {
        _organizeButtonItem.image = [UIImage imageNamed:@"filter"];
    }
}
#pragma mark - UIOrganizeTableViewController delegates
- (void)willFilterByFilterType:(NSInteger)selectedRow {
     currentOffset = 0;
    _selectedFileType = selectedRow;
    self.tableView.scrollEnabled=YES;
    switch (selectedRow) {
        case 0:
            _selectedFileType = FilterFilesAll;
            break;
        case 1:
            _selectedFileType = FilterTypeImage;
            break;
        case 2:
            _selectedFileType = FilterTypeAudioORVideo;
            break;
        case 3:
            _selectedFileType = FilterTypePDF;
            break;
        case 4:
            _selectedFileType = FilterTypeMicrosoftOffice;
            break;
        default:
            break;
    }
     [self fetchAllFilesWithType:_selectedFileType];

    _isFiltering = (_selectedFileType != FilterFilesAll);
    [self drawGoodImageForOrganizeButtonItem];
    if([_fromView isKindOfClass:[MyInfosTableViewController class]] || !_peer){
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:_selectedFileType] forKey:@"selectedFilterType"];
    }
    if([self isViewLoaded]){
        dispatch_async(dispatch_get_main_queue(), ^{
          [self.tableView reloadData];
        });
        
    }
}

- (void)willSortBySortOrder:(NSInteger)selctedRow {
    _selectedSortOrder = selctedRow;
    if(!_organizeFilesTableViewController.view.superview)
        self.tableView.scrollEnabled = YES;
    if(_selectedSortOrder == SortOrderByName) {
        _allFiles.sectionNameFromObjectComputationBlock = _sectionedByName;
        _allFiles.sectionSortDescriptor = _sortSectionAsc;
    }
    else if(_selectedSortOrder == SortOrderBySize) {
        _allFiles.sectionNameFromObjectComputationBlock = _sectionedBySize;
        _allFiles.sectionSortDescriptor = _sortSizeAsc;
    }
    //Default by Date
    else {
        _allFiles.sectionNameFromObjectComputationBlock = _sectionedByDate;
        _allFiles.sectionSortDescriptor = [self sortDescriptorMonth];
    }
    _isSorting = (_selectedSortOrder != SortOrderByDate);
    [self drawGoodImageForOrganizeButtonItem];
    if([_fromView isKindOfClass:[MyInfosTableViewController class]] || !_peer){
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:_selectedSortOrder] forKey:@"selectedSortType"];
    }
    if([self isViewLoaded]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }
   
}
-(void) didFooterPressed {
    self.tableView.userInteractionEnabled = YES;
    self.tableView.scrollEnabled = YES;
    
}

@end
