/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIConferenceStatusViewController.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/ContactsManagerService.h>
#import <Rainbow/ConferenceParticipant.h>
#import "UITools.h"
#import "MBProgressHUD.h"
#import "OrderedOptionalSectionedContent.h"
#import "UIRainbowGenericTableViewCell.h"
#import "ConferenceParticipant+UIRainbowGenericTableViewCell.h"

#define kParticipantTableViewCell @"ParticipantTableViewCell"

#define kOrganizerSection @"organizer"
#define kAttendeesSection @"attendees"
#define kMembersSection @"members"

@interface UIConferenceStatusViewController () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (nonatomic, weak) IBOutlet UIToolbar *conferenceToolbar;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *muteAllButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *terminateConferenceButton;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) ConferencesManagerService *conferencesManagerService;

@property (nonatomic, strong) OrderedOptionalSectionedContent<ConferenceParticipant *> *participants;

@property (nonatomic, strong) NSObject *participantListMutex;
@property (nonatomic, strong) UIAlertController *actionSheet;
@property (nonatomic, strong) ConferenceActiveTalkersViewController *activeTalkersViewController;
@property (nonatomic, strong) Conference *conference;
@property (nonatomic, strong) MBProgressHUD *callingHUD;
@property (nonatomic) NSUInteger connectedParticipants;
@property (nonatomic, strong) AVAudioPlayer *participantConnectSoundPlayer;
@end

@implementation UIConferenceStatusViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        _conferencesManagerService = [ServicesManager sharedInstance].conferencesManagerService;
        
        _participantListMutex = [[NSObject alloc] init];
        
        _participants = [[OrderedOptionalSectionedContent alloc] initWithSections:@[kOrganizerSection, kAttendeesSection, kMembersSection]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConference:) name:kConferencesManagerDidUpdateConference object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRTCCall:) name:kTelephonyServiceDidRemoveCallNotification object:nil];
        _tableView.emptyDataSetSource = self;
        _tableView.emptyDataSetDelegate = self;
        _activeTalkersViewController = [[ConferenceActiveTalkersViewController alloc] initWithNibName:@"ConferenceActiveTalkersViewController" bundle:nil];
        _connectedParticipants = 0;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _activeTalkersViewController.conference = _conference;
    _conferenceToolbar.barTintColor = [UITools defaultTintColor];
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
    self.tableView.tableFooterView = [UIView new];
    
    NSURL * soundURL = [[NSBundle mainBundle] URLForResource:@"join-meeting" withExtension:@"mp3"];
    self.participantConnectSoundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: soundURL error:nil];
    [self.participantConnectSoundPlayer prepareToPlay];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    if(_conference.owner == [ServicesManager sharedInstance].myUser.contact){
        [self showToolbar:YES];
        _muteAllButton.enabled = YES;
    } else {
        [self showToolbar:NO];
        _muteAllButton.enabled = NO;
    }
    [self updateMuteAllButton];
    [self showActiveTalkers:YES];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self showActiveTalkers:NO];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)dealloc {
    _conferencesManagerService = nil;
    _activeTalkersViewController = nil;
    _connectedParticipants = 0;
    [_participants removeAllObjects];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConferencesManagerDidUpdateConference object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidRemoveCallNotification object:nil];
}

-(void) setRoom:(Room *)room {
    _room = room;
    [self setConference:_room.conference];
}

-(void) setConference:(Conference *)conference {
    _conference = conference;
    [_participants removeAllObjects];
    NSMutableArray<Contact*> *connectedContact = [NSMutableArray new];
    for (ConferenceParticipant *participant in conference.participants) {
        [connectedContact addObject:participant.contact];
        
        if(participant.contact == _conference.owner){
            if(![_participants containsObject:participant inSection:kOrganizerSection])
                [_participants addObject:participant toSection:kOrganizerSection];
        } else {
            if(participant.state == ParticipantStateConnected){
                if(![_participants containsObject:participant inSection:kAttendeesSection])
                    [_participants addObject:participant toSection:kAttendeesSection];
            } else {
                if(![_participants containsObject:participant inSection:kMembersSection])
                    [_participants addObject:participant toSection:kMembersSection];
            }
        }
    }

    // Create ConferenceParticipant from room Participant list and add the ones not actually in organizer or attendees to complete the list
    // NOTE: these ConferenceParticipant are created artificially and do not contains all the datas (participantId)
    NSArray<ConferenceParticipant*> *allParticipants = [_conferencesManagerService createConferenceParticipantsFromRoom:_room];
    [allParticipants enumerateObjectsUsingBlock:^(ConferenceParticipant * _Nonnull roomParticipant, NSUInteger idx, BOOL * _Nonnull stop) {
        if(![connectedContact containsObject:roomParticipant.contact]) {
            if(![_participants containsObject:roomParticipant inSection:kMembersSection])
                [_participants addObject:roomParticipant toSection:kMembersSection];
        }
    }];
    
    if([self isViewLoaded]){
        [self.tableView reloadData];
    }
}

-(void) playNewParticipantSound {
    if(!self.participantConnectSoundPlayer.isPlaying){
        [self.participantConnectSoundPlayer play];
    }
}

-(void) didUpdateConference:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateConference:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kConferenceChangedAttributesKey];
    Conference *aConference = (Conference *)[userInfo objectForKey:kConferenceKey];
    NSLog(@"[UI] DID UPDATE CONFERENCE %@ %@", aConference, changedKeys);
    if([aConference isEqual:_conference]){
        if([changedKeys containsObject:@"participants"]){
            [self setConference:aConference];
            NSUInteger newConnectedParticipants = _conference.participants.count;
            BOOL isParticipantConnectSoundMuted = [[NSUserDefaults standardUserDefaults] boolForKey:@"muteNewMeetingParticipantSound"];
            if(!isParticipantConnectSoundMuted && newConnectedParticipants > self.connectedParticipants){
                [self playNewParticipantSound];
            }
            self.connectedParticipants = newConnectedParticipants;
            
            if(_conference.myConferenceParticipant.state == ParticipantStateRinging){
                if(_callingHUD){
                    [_callingHUD hide:NO];
                    _callingHUD = nil;
                }
                
                _callingHUD = [UITools showHUDWithMessage:NSLocalizedString(@"Connecting ...",nil) detailedMessage:nil forView:self.view mode:MBProgressHUDModeIndeterminate imageName:nil dismissCompletionBlock:nil];
                [_callingHUD show:YES];
            }
            if(_conference.myConferenceParticipant.state == ParticipantStateConnected){
                [_callingHUD hide:YES];
            }
        }
        if([changedKeys containsObject:@"allParticipantsMuted"]){
            [self updateMuteAllButton];
        }
    } else {
        NSLog(@"Not updating the good conference %@ vs %@", aConference, _conference);
    }
}


-(void)didRemoveRTCCall:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveRTCCall:notification];
        });
        return;
    }
}

-(void)showToolbar:(BOOL)show {
    _conferenceToolbar.hidden = !show;
}

-(void) updateMuteAllButton {
    if(_conference.allParticipantsMuted){
        self.muteAllButton.image = [UIImage imageNamed:@"UnmuteAll"];
    } else {
        self.muteAllButton.image = [UIImage imageNamed:@"MuteAll"];
    }
}

-(void) showActiveTalkers:(BOOL)show {
    if(show){
        if(![self.activeTalkersViewController.view superview]){
            [self.view addSubview:self.activeTalkersViewController.view];
            [self addChildViewController:self.activeTalkersViewController];
            [self.activeTalkersViewController didMoveToParentViewController:self];
        }
    } else {
        if([self.activeTalkersViewController.view superview]){
            [self.activeTalkersViewController willMoveToParentViewController:nil];
            [self.activeTalkersViewController removeFromParentViewController];
            [self.activeTalkersViewController.view removeFromSuperview];
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_participants allNotEmptySections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:section];
    return [[_participants sectionForKey:key] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title = @"";
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:section];
    NSUInteger participantCount = [[_participants sectionForKey:key] count];
    
    if([key isEqualToString:kOrganizerSection]){
        if(_conference.isMyConference)
            title = NSLocalizedString(@"Organizer (Me)", nil);
        else
            title = NSLocalizedString(@"Organizer", nil);
    }
    
    if([key isEqualToString:kAttendeesSection])
        title = (participantCount>0) ? [NSString stringWithFormat:@"%@ (%lu)", NSLocalizedString(@"Attendees", nil), (unsigned long)participantCount] : NSLocalizedString(@"Attendees", nil);
    
    if([key isEqualToString:kMembersSection])
        title = (participantCount>0) ? [NSString stringWithFormat:@"%@ (%lu)", NSLocalizedString(@"Non-attendees", nil), (unsigned long)participantCount] : NSLocalizedString(@"Non-attendees", nil);
        
    return title;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];;
    
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:indexPath.section];
    NSArray<ConferenceParticipant *> *content = [_participants sectionForKey:key];
    ConferenceParticipant *participant = [content objectAtIndex:indexPath.row];
    cell.cellObject = participant;
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_actionSheet) {
        [_actionSheet dismissViewControllerAnimated:NO completion:nil];
        _actionSheet = nil;
    }
    
    _actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:indexPath.section];
    NSArray<ConferenceParticipant *> *content = [_participants sectionForKey:key];
    ConferenceParticipant *participant = [content objectAtIndex:indexPath.row];
    
    if(participant.state != ParticipantStateConnected){
        return;
    }
    
     __weak __typeof__(self) weakSelf = self;
    if(_conference.isMyConference || participant.contact == [ServicesManager sharedInstance].myUser.contact){
        UIAlertAction* muteUnmuteAction = [UIAlertAction actionWithTitle:participant.muted ? NSLocalizedString(@"Unmute", @"") : NSLocalizedString(@"Mute", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            if(participant.muted)
               [_conferencesManagerService unmuteParticipant:participant inConference:_conference];
            else
                [_conferencesManagerService muteParticipant:participant inConference:_conference];
        }];
        [_actionSheet addAction:muteUnmuteAction];
        
        UIAlertAction* hangupAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Hangup", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            if(participant.contact != [ServicesManager sharedInstance].myUser.contact){
                [weakSelf.conferencesManagerService disconnectParticipant:participant inConference:_conference];
            } else {
                if([ServicesManager sharedInstance].rtcService.calls.count>0){
                    RTCCall *call = [[ServicesManager sharedInstance].rtcService.calls firstObject];
                    [[ServicesManager sharedInstance].rtcService hangupCall:call];
                    if (_conference.myConferenceParticipant){
                        [_conferencesManagerService disconnectParticipant:_conference.myConferenceParticipant inConference:_conference];
                    } else {
                        NSLog(@"[UIConferenceStatusViewController] No participant to disconnect");
                    }
                }
            }
        }];
        [_actionSheet addAction:hangupAction];
    }
    
    if(_actionSheet.actions.count > 0){
        // Cancel action
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        [_actionSheet addAction:cancel];
        
        // show the menu.
        [_actionSheet.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:_actionSheet animated:YES completion:nil];
    }
}

#pragma mark - toolbar actions

-(IBAction)muteAllAction:(id)sender {
    if(!_conference.confId){
        return;
    }
    if(_conference.allParticipantsMuted){
        self.muteAllButton.image = [UIImage imageNamed:@"UnmuteAll"];
    } else {
        self.muteAllButton.image = [UIImage imageNamed:@"MuteAll"];
    }
    if(_conference.allParticipantsMuted)
       [[ServicesManager sharedInstance].conferencesManagerService unmuteAllParticipantsInConference:_conference];
    else
        [[ServicesManager sharedInstance].conferencesManagerService muteAllParticipantsInConference:_conference];
}

-(IBAction)terminateConferenceAction:(id)sender {
    if(!_conference.confId){
        return;
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"End meeting", nil) message:NSLocalizedString(@"Do you want to end this meeting for all participants?", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"End meeting",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [[ServicesManager sharedInstance].conferencesManagerService terminateConference:_conference completionHandler:^(NSError *error) {
            if(!error){
                self.connectedParticipants = 0;
                [[ServicesManager sharedInstance].roomsService detachConference:_conference fromRoom:_room completionBlock:nil];
            }
        }];
    }];
    
    [alert addAction:defaultAction];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - DZNEmptyDataSet

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"Connection in progress...", nil);
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools boldFontName] size:17.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 20.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

@end
