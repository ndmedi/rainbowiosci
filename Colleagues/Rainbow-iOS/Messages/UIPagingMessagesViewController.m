/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIPagingMessagesViewController.h"
#import "UIConferenceStatusViewController.h"
#import <Rainbow/ServicesManager.h>
#import "UITools.h"
#import <Rainbow/Tools.h>
#import "UIStoryboardManager.h"

@interface UIPagingMessagesViewController ()
@property (nonatomic, assign) NSInteger presentationPageIndex;
@property (nonatomic, strong) NSArray *viewControllersArray;
@property (nonatomic, strong) UIMessagesViewController *messagesViewController;
@property (nonatomic, strong) UIConferenceStatusViewController *conferenceStatusPageViewController;
@end

@implementation UIPagingMessagesViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        _presentationPageIndex = 0;
    }
    return self;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    self.dataSource = nil;
    _messagesViewController = [[UIStoryboardManager sharedInstance].messagesStoryBoard instantiateViewControllerWithIdentifier:@"messageViewControllerID"];
    
    // TODO : Put all UI in a storyboard
    _conferenceStatusPageViewController = [[UIConferenceStatusViewController alloc] initWithNibName:@"UIConferenceStatusViewController" bundle:nil];
    _viewControllersArray = @[_messagesViewController, _conferenceStatusPageViewController];
    self.hidesBottomBarWhenPushed = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConference:) name:kConferencesManagerDidUpdateConference object:nil];
}

-(void) setConversation:(Conversation *)conversation {
    _conversation = conversation;
    if([_conversation.peer isKindOfClass:[Room class]]){
        if(((Room *)_conversation.peer).conference)
            _conferenceStatusPageViewController.room = (Room *)_conversation.peer;
    }
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if([self isConferenceJoined]){
        [self showConferencePage:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

}

- (void)dealloc {
    self.messagesViewController = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setViewControllers {
    self.presentationPageIndex = 0;
    [self setViewControllers:@[self.messagesViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {}];
}

-(void) showConferencePage:(BOOL)show {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showConferencePage:show];
        });
        return;
    }
    __weak typeof (self) weakSelf = self;
    if(show){
        _messagesViewController.parentNavigationController = (CustomNavigationController*)self.navigationController;
        self.presentationPageIndex = 1;
        // We have to set the messageViewController to avoid crash on iOS 11 only if it's not the currently presented view controller
        if([self.viewControllers containsObject:self.messagesViewController])
            [self setViewControllers:@[self.conferenceStatusPageViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        else
            [self setViewControllers:@[self.messagesViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
        [self setViewControllers:@[self.conferenceStatusPageViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {
            weakSelf.dataSource = weakSelf;
            UIPageControl *pageControl = [UIPageControl appearance];
            pageControl.pageIndicatorTintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
            pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
            pageControl.backgroundColor = [UITools defaultTintColor];
        }];
        
    // The test avoid flickering
    } else if(self.presentationPageIndex == 1){
        // We have to set the messageViewController to avoid crash on iOS 11 only if it's not the currently presented view controller
        if([self.viewControllers containsObject:self.messagesViewController])
            [self setViewControllers:@[self.conferenceStatusPageViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        else
            [self setViewControllers:@[self.messagesViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        self.dataSource = nil;
        self.presentationPageIndex = 0;
        [self setViewControllers:@[self.messagesViewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
        }];
    }
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    if([self isConferenceJoined]){
        NSUInteger currentIndex =  [self.viewControllersArray indexOfObject:viewController];
        if (currentIndex == NSNotFound) {
            return nil;
        }
        NSUInteger previousIndex = (currentIndex == 0) ? [self.viewControllersArray count] - 1 : (currentIndex - 1) % [self.viewControllersArray count];
        return [self.viewControllersArray objectAtIndex: previousIndex];
    }   else {
        return nil;
    }
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    if([self isConferenceJoined]){
        NSUInteger currentIndex =  [self.viewControllersArray indexOfObject:viewController];
        if (currentIndex == NSNotFound) {
            return nil;
        }
        NSUInteger nextIndex = (currentIndex + 1) % [self.viewControllersArray count];
        return [self.viewControllersArray objectAtIndex: nextIndex];
    }   else {
        return nil;
    }
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    if([self isConferenceJoined])
        return 2;
    else
        return 0;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return self.presentationPageIndex;
}

-(BOOL) isConferenceJoined {
    if([_conversation.peer isKindOfClass:[Room class]]){
        Room *room = (Room *) _conversation.peer;
        if(room.conference){
            return [[ServicesManager sharedInstance].conferencesManagerService hasContact:[ServicesManager sharedInstance].myUser.contact joinedConference:room.conference];
        }
    }
    
    return NO;
}

#pragma mark - Conferences notifications

-(void) didUpdateConference:(NSNotification *) notification {
    [self showConferencePage: [self isConferenceJoined]];
}

@end
