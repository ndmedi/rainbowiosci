/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "GroupedMessages.h"
#import "Contact+Extensions.h"
#import <Rainbow/ServicesManager.h>
#import "Message-JSQMessageData.h"

@interface GroupedMessages ()
@property (nonatomic, strong, readwrite) NSMutableArray<Message *> * allGroupedMessages;
@end

@implementation GroupedMessages

-(instancetype) initWithMessage:(Message *) message {
    self = [super init];
    if(self){
        _allGroupedMessages = [NSMutableArray array];
        [self insertMessageAtProperIndex:message];
    }
    return self;
}

-(void) dealloc {
    [_allGroupedMessages removeAllObjects];
    _allGroupedMessages = nil;
}

-(void) addMessage:(Message *) message {
    [self insertMessageAtProperIndex:message];
}

-(void) removeMessage:(Message *) message {
    [_allGroupedMessages removeObject:message];
}

-(NSInteger) insertMessageAtProperIndex:(Message *) message {
    NSInteger idxForInsertion = NSNotFound;
    if ([_allGroupedMessages indexOfObject:message]==NSNotFound) {
        idxForInsertion = [self properIndexForItem:message];
        [_allGroupedMessages insertObject:message atIndex:idxForInsertion];
    }
    return idxForInsertion;
}

-(NSInteger) properIndexForItem:(Message *) message {
    __block NSInteger insertIndex = [_allGroupedMessages count];
    // if we don't find a proper place in the list, this means we should add it at the end.
    [_allGroupedMessages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        GroupedMessages *item = obj;
        // if item.date is not later than aNewItem.date
        if (item.date!=nil && ![item isEqual:message] && [item.date compare:message.date]!=NSOrderedAscending) {
            *stop = YES;
            insertIndex = idx;
        }
    }];
    return insertIndex;
}

-(Message *) lastMessage {
    return [_allGroupedMessages lastObject];
}

-(Message *) firstMessage {
    return [_allGroupedMessages firstObject];
}

-(NSString *) description {
    return [NSString stringWithFormat:@"Grouped Messages : %lu content: %@",(unsigned long)_allGroupedMessages.count, [_allGroupedMessages componentsJoinedByString:@"\n"] ];
}

#pragma mark - JSQMessageData protocol
-(NSString *)senderId {
    if (self.lastMessage.isOutgoing)
        return [ServicesManager sharedInstance].myUser.contact.jid;
    return [_allGroupedMessages lastObject].peer.jid;
}

-(NSString *) senderDisplayName {
    if (self.lastMessage.isOutgoing)
        return [ServicesManager sharedInstance].myUser.contact.firstName ? [ServicesManager sharedInstance].myUser.contact.firstName : [ServicesManager sharedInstance].myUser.contact.jid;
    return [_allGroupedMessages lastObject].peer.displayName;
}

-(BOOL) isMediaMessage {
    return [self.lastMessage isMediaMessage];
}

-(NSUInteger) messageHash {
    // The JSQ doc says :
    // "This value is used to cache layout information in the collection view."
    // This is used as the message bubble-size cache identifier.
    // But our outgoing-bubble and incomming-bubble does not have the same size (due to the received/read icon)
    return [[NSString stringWithFormat:@"%d%@", (int)[_allGroupedMessages lastObject].isOutgoing, self.text] hash];
}

-(NSString *) text {
    NSMutableString *body = [NSMutableString string];
    BOOL shouldReturnToNewLine = _allGroupedMessages.count > 1;
    for (Message *message in _allGroupedMessages) {
        if ([message.body length]) {
            [body appendString:message.body];
            if(shouldReturnToNewLine)
                [body appendString:@"\n"];
        }
    }
    
    return [body stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
}

-(NSDate *) date {
    return [_allGroupedMessages lastObject].date;
}

- (id<JSQMessageMediaData>)media {
    return [self.lastMessage media];
}

@end
