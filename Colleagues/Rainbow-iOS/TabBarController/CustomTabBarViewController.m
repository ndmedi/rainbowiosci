/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CustomTabBarViewController.h"
#import "UITabBarController+Orientation.h"
#import "LoginViewController.h"
#import <Rainbow/LoginManager.h>
#import <Rainbow/ServicesManager.h>
#import <Rainbow/Contact.h>
#import "UITools.h"
#import "UITabBarItem+CustomBadge.h"
#import "MyInfosTableViewController.h"
#import <Rainbow/defines.h>
#import "RecentsConversationsTableViewController.h"
#import <Rainbow/LogsRecorder.h>
#import "UIImage+Additions.h"
#import <Rainbow/ContactsManagerService.h>
#import "UIImageView+Letters.h"
#import "Contact+Extensions.h"
#import <Rainbow/RTCService.h>
#import "RTCCallViewController.h"
#import "UINotificationManager.h"
#import "FirstStartViewController.h"
#import <Rainbow/UIDevice+VersionCheck.h>
#import "UICallInProgressViewController.h"
#import "CustomNavigationController.h"
#import <Rainbow/CallLogsService.h>
#import "UIStoryboardManager.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>

@interface CustomTabBarViewController () <UITabBarControllerDelegate, UIPopoverControllerDelegate, AVAudioPlayerDelegate>
@property (nonatomic, strong) LoginViewController *loginViewController;
@property (nonatomic, strong) LoginManager *loginManager;
@property (nonatomic, strong) ContactsManagerService *contactsManager;
@property (nonatomic, strong) ConversationsManagerService *conversationsManager;
@property (nonatomic, strong) RoomsService *roomsService;
@property (nonatomic, strong) CallLogsService *callLogsService;
@property (nonatomic, strong) FirstStartViewController *firstStartViewController;
@property (nonatomic, strong) UICallInProgressViewController *callInProgressCallController;
@property (nonatomic, strong) UIWindow *callInProgressWindow;
@property (nonatomic, strong) UIAlertController *recordCallAlert;
@property (nonatomic, strong) AVAudioPlayer *recordAlertSoundPlayer;
@end

@implementation CustomTabBarViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onApplicationDidFinishLauching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onApplicationDidEnterInBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailedToAuthenticate:) name:kLoginManagerDidFailedToAuthenticate object:nil];
        
        // Badge updates on multiple triggers
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateConversationBadgeValue:) name:kConversationsManagerDidUpdateMessagesUnreadCount object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateRoomBadgeValue:) name:kRoomsServiceDidReceiveRoomInvitation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateRoomBadgeValue:) name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateContactBadgeValue:) name:kContactsManagerServiceDidUpdateInvitationPendingNumber object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateContactBadgeValue:) name:kCompaniesServiceDidUpdateCompanyInvitationPendingNumber object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateCallLogBadgeValue:) name:kCallLogsServiceDidUpdateCallLogsUnreadCount object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateCallLogBadgeValue:) name:kMyUserVoicemailCountDidUpdate object:nil];
        
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddCall:) name:kTelephonyServiceDidAddCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveCall:) name:kTelephonyServiceDidRemoveCallNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clickToCallMobile:) name:kContactsManagerServiceClickToCallMobile object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startWithInvitation:) name:@"startWithInvitation" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createAccountTapped:) name:@"createAccountTapped" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alreadyMemberTapped:) name:@"alreadyMemberTapped" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLocalNotification:) name:UIApplicationDidReceiveLocalNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideCallView:) name:@"hideCallViewFromView" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showCallView:) name:@"showCallView" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeRecordingCallState:) name:kRTCServiceDidChangeRecordingCallStateNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willFinalizeGuestAccount:) name:@"willFinalizeGuestAccount" object:nil];
        self.delegate = self;
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidFinishLaunchingNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidUpdateMessagesUnreadCount object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidReceiveRoomInvitation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceClickToCallMobile object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidAddCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidRemoveCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCallLogsServiceDidUpdateCallLogsUnreadCount object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserVoicemailCountDidUpdate object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"startWithInvitation" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"createAccountTapped" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"alreadyMemberTapped" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"hideCallViewFromView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showCallView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidChangeRecordingCallStateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"willFinalizeGuestAccount" object:nil];
    _loginManager = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) didAddCall:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddCall:notification];
        });
        return;
    }
    
    Call *call = (Call*) notification.object;
    
    if (call.isIncoming) {
        
        BOOL isCallKitAvailable = [ServicesManager sharedInstance].rtcService.isCallKitAvailable;
        if (UIApplication.sharedApplication.applicationState == UIApplicationStateActive) {
            if(call.status == CallStatusConnecting || call.status == CallStatusEstablished || (call.status == CallStatusRinging && !isCallKitAvailable)){
            if([call isKindOfClass:[Call class]])
                [self showCallView:notification];
            }
        } else if (!isCallKitAvailable) {
                if([call isKindOfClass:[Call class]]){
                    [UINotificationManager presentUILocalNotificationWithBody:NSLocalizedString(@"Incoming call", nil) title:call.peer.displayName category:@"call_category" userInfo:nil];
                }
            }
    } else {
        NSLog(@"didStartNewOutgoingCall %@", call);
        if(UIApplication.sharedApplication.applicationState != UIApplicationStateActive){
            if([call isKindOfClass:[Call class]] && !call.isIncoming){
                [UINotificationManager presentUILocalNotificationWithBody:NSLocalizedString(@"Manage your call", nil) title:call.peer.displayName category:nil userInfo:nil];
            }
        }
        [self showCallView:notification];
    }
}

-(void) didRemoveCall:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveCall:notification];
        });
        return;
    }
    [self hideCallInProgressView];
    Call *call = (Call *)notification.object;
    if(call.status == CallStatusDeclined){
        // Show missed call local notification
        if([UINotificationManager canShowNotifications]){
            if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground){
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSDictionary *userInfo = @{@"type" : @"user", @"from" : call.peer.rtcJid};
                    [UINotificationManager presentUILocalNotificationWithBody:NSLocalizedString(@"Missed call", nil) title:call.peer.displayName category:@"im_category" userInfo:userInfo];
                });
            }
        }
    }
}

-(void) handleLocalNotification:(NSNotification *) notification {
    NSDictionary *userInfo = (NSDictionary*)notification.object;
    NSString *category = userInfo[@"category"];
    if([category isEqualToString:@"im_category"]) {
        // If we have a call presented we must "hide" the view to allow user to display his conversation
        [self hideCallView:notification];
    }
    if([category isEqualToString:@"call"]){
        // Answer the call
        RTCCall *call = [[ServicesManager sharedInstance].rtcService.calls firstObject];
        RTCCallFeatureFlags features = call.features;
        if(call.isVideoEnabled)
            features |= RTCCallFeatureLocalVideo;
        [[ServicesManager sharedInstance].rtcService acceptIncomingCall:call withFeatures:features];
    }
}

-(void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        BOOL callInProgressVisible = YES;
        UIInterfaceOrientation orientation;
        
        if(size.width < size.height) {
        //    NSLog(@"orient: UIInterfaceOrientationPortrait - w:%f - h:%f", size.width, size.height);
            orientation = UIInterfaceOrientationPortrait;
        } else {
        //    NSLog(@"orient: UIInterfaceOrientationLandscape - w:%f - h:%f", size.width, size.height);
            callInProgressVisible = NO;
            orientation = UIInterfaceOrientationLandscapeLeft;
        }
        
        [self setCallInProgressWindowVisible: callInProgressVisible];
        [self resizeMainWindow: orientation];
        
    } completion:nil];
}

#pragma mark - Call view
-(void) showCallView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showCallView:notification];
        });
        return;
    }
    
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground){
        NSLog(@"Application is in background, don't present call view");
        return;
    }
    
    if(_callInProgressCallController){
        // Reuse the code in didRemoveCall
        [self didRemoveCall:nil];
    }
    Call *call = (Call *) notification.object;
    RTCCallViewController *rtcCallViewController = [[UIStoryboardManager sharedInstance].rtcCallStoryBoard instantiateViewControllerWithIdentifier:@"RTCCallViewControllerID"];
    rtcCallViewController.call = call;
    
    // We dismiss modal view presented (my infos view) to present the rtcCallView correclty
    if([self.presentedViewController isKindOfClass:[CustomNavigationController class]]){
        [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    }
    rtcCallViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:rtcCallViewController animated:YES completion:nil];
}

-(void) hideCallView:(NSNotification *) notification {
    if([self.presentedViewController isKindOfClass:[RTCCallViewController class]]){
        RTCCallViewController *viewController = (RTCCallViewController*) self.presentedViewController;
        Call *call = viewController.call;

        [viewController hideCallView:nil];
        if (call.status == CallStatusRinging || call.status == CallStatusConnecting || call.status == CallStatusEstablished)
            [self showCallInProgressViewForCall:call];
    }
}

-(void) didChangeRecordingCallState:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didChangeRecordingCallState:notification];
        });
        return;
    }

    RTCCall *call = (RTCCall *)notification.object;
    if(_recordCallAlert)
        [_recordCallAlert dismissViewControllerAnimated:YES completion:nil];
    if(call.isRecording) {
        // Play a "Begin recording" sound
        NSURL * soundURL = [[NSBundle mainBundle] URLForResource:@"beep-01a" withExtension:kJSQSystemSoundTypeAIFF];
        
        if(_recordAlertSoundPlayer){
            [_recordAlertSoundPlayer stop];
            _recordAlertSoundPlayer = nil;
        }
        _recordAlertSoundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: soundURL error:nil];
        [_recordAlertSoundPlayer prepareToPlay];
        [_recordAlertSoundPlayer play];
        
        if([UIApplication sharedApplication].applicationState != UIApplicationStateBackground){
            _recordCallAlert = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"You are being recorded", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
            [_recordCallAlert addAction:defaultAction];
            if(self.presentedViewController)
                [self.presentedViewController presentViewController:_recordCallAlert animated:YES completion:nil];
            else
                [self presentViewController:_recordCallAlert animated:YES completion:nil];
        } else {
            [UINotificationManager presentUILocalNotificationWithBody:NSLocalizedString(@"You are being recorded", nil) title:nil category:nil userInfo:nil];
        }
    }
}


#pragma mark - CallInProgress banner
-(void) showCallInProgressViewForCall:(Call *) call {
    if(!_callInProgressCallController){
        _callInProgressCallController = [self.storyboard instantiateViewControllerWithIdentifier:@"callInProgressViewID"];
        _callInProgressCallController.call = call;
        
        float topSafeAreInset = [[UIApplication sharedApplication] statusBarFrame].size.height;
        if (@available(iOS 11.0, *)) {
            topSafeAreInset = MAX(topSafeAreInset, self.view.safeAreaInsets.top);
        }
        
        CGRect statusBarFrame = [[UIScreen mainScreen] bounds];
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        _callInProgressCallController.view.frame = CGRectMake(statusBarFrame.origin.x, statusBarFrame.origin.y, orientation==UIInterfaceOrientationPortrait?statusBarFrame.size.width:statusBarFrame.size.height, kCallInProgressHeight+topSafeAreInset);
        
        CATransition *applicationLoadViewIn = [CATransition animation];
        [applicationLoadViewIn setDuration:1];
        [applicationLoadViewIn setType:kCATransitionFromTop];
        [applicationLoadViewIn setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [[_callInProgressCallController.view layer] addAnimation:applicationLoadViewIn forKey:kCATransitionFromTop];
        
        if(!_callInProgressWindow){
            CGRect callInProgressWindowFrame = _callInProgressCallController.view.frame;
            _callInProgressWindow = [[UIWindow alloc] initWithFrame:callInProgressWindowFrame];
            _callInProgressWindow.rootViewController = _callInProgressCallController;
        }
        _callInProgressWindow.windowLevel = UIWindowLevelNormal;
        if([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationPortrait){
            UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
            CGRect callInProgressFrame = _callInProgressCallController.view.frame;
            _callInProgressCallController.view.frame = callInProgressFrame;
            
            CGRect keyWindowFrame = keyWindow.frame;
            keyWindowFrame.origin.y += kCallInProgressHeight;
            keyWindowFrame.size.height -= kCallInProgressHeight;
            [keyWindow setFrame:keyWindowFrame];
            [_callInProgressWindow setHidden:NO];
        }
    }
}

-(void) setCallInProgressWindowVisible:(BOOL)visible {
    if(_callInProgressWindow) { // toggle visibility only if the window exists (call in progress)
        if(visible && [_callInProgressWindow isHidden]) {
            [_callInProgressWindow setHidden:NO];
        } else if(visible == NO && [_callInProgressWindow isHidden] == NO) {
            [_callInProgressWindow setHidden:YES];
        }
    }
}

-(void) resizeMainWindow: (UIInterfaceOrientation) orientation {
    if(_callInProgressWindow) {
        CGRect screenFrame = [[UIScreen mainScreen] bounds];
        UIWindow *keyWindow = [UIApplication sharedApplication].windows[0];
        CGRect keyWindowFrame = keyWindow.frame;
        
        if([_callInProgressWindow isHidden] && orientation == UIInterfaceOrientationLandscapeLeft) {
            [keyWindow setFrame:screenFrame];
        } else if(![_callInProgressWindow isHidden] && orientation == UIInterfaceOrientationPortrait) {
            if(keyWindowFrame.origin.y == 0) {
                keyWindowFrame.origin.y += kCallInProgressHeight;
                keyWindowFrame.size.height = screenFrame.size.height - kCallInProgressHeight;
                [keyWindow setFrame:keyWindowFrame];
            }
        }
    }
}

-(void) hideCallInProgressView {
    // We don't hide here the callView controller we just hide the callInProgressView if it as been presented
    if(_callInProgressCallController){
        UIWindow *keyWindow = [UIApplication sharedApplication].windows[0];
        [_callInProgressCallController.view removeFromSuperview];
        [_callInProgressCallController removeFromParentViewController];
        _callInProgressCallController = nil;
        
        CGRect keyWindowFrame = keyWindow.frame;
        if(keyWindowFrame.origin.y == kCallInProgressHeight)
            keyWindowFrame.origin.y -= kCallInProgressHeight;
        if([[UIScreen mainScreen] bounds].size.height > keyWindowFrame.size.height)
            keyWindowFrame.size.height += kCallInProgressHeight;
        [keyWindow setFrame:keyWindowFrame];
        
        [keyWindow makeKeyWindow];
        [_callInProgressWindow removeFromSuperview];
        _callInProgressWindow = nil;
        UIView *statusBar = [UIApplication.sharedApplication valueForKey:@"statusBar"];
        if([statusBar respondsToSelector:@selector(setBackgroundColor:)]){
            statusBar.backgroundColor = [UITools defaultTintColor];
        }
    }
}

-(void) onApplicationDidFinishLauching:(NSNotification *) notification {
    _loginManager = [ServicesManager sharedInstance].loginManager;
    [self localizeTabBar];
    [UITools customizeAppAppearanceWithGlobalColor:[UITools defaultTintColor]];
    [self showLoginView:NO autoLogin:YES];
    
    _contactsManager = [ServicesManager sharedInstance].contactsManagerService;
    _conversationsManager = [ServicesManager sharedInstance].conversationsManagerService;
    _roomsService = [ServicesManager sharedInstance].roomsService;
    _callLogsService = [ServicesManager sharedInstance].callLogsService;
}

-(void) onApplicationDidEnterInBackground:(NSNotification *) notification {
    [self shouldUpdateConversationBadgeValue:nil];
    [self shouldUpdateRoomBadgeValue:nil];
    [self shouldUpdateContactBadgeValue:nil];
    [self shouldUpdateCallLogBadgeValue:nil];
}


-(void) applicationDidBecomeActive:(NSNotification *) notification {
    RTCService *rtcService = [ServicesManager sharedInstance].rtcService;
    if(rtcService.hasActiveCalls && !_callInProgressCallController){
        RTCCall *call = [rtcService.calls firstObject];
        if ( (call.status == CallStatusEstablished || call.status == CallStatusRinging || call.status == CallStatusConnecting) && !(call.isMediaPillarCall) )
            [self showCallView:[NSNotification notificationWithName:kTelephonyServiceDidAddCallNotification object:call]];
        
    }
}


-(void) showLoginView:(BOOL) animated autoLogin:(BOOL) autoLogin {
    if(autoLogin && [ServicesManager sharedInstance].myUser.username.length > 0 && [ServicesManager sharedInstance].myUser.password.length > 0){
        // Auto Login
        [self autoLogin];
    } else {
        UIWindow* window = [[UIApplication sharedApplication].windows firstObject];
        
        if(!_loginViewController){
            _loginViewController = [[UIStoryboardManager sharedInstance].welcomePagesStoryBoard instantiateViewControllerWithIdentifier:@"loginControllerID"];
        }
        
        if([ServicesManager sharedInstance].myUser.username.length == 0 && [ServicesManager sharedInstance].myUser.password.length == 0){
            _firstStartViewController = (FirstStartViewController *)[[UIStoryboardManager sharedInstance].welcomePagesStoryBoard instantiateViewControllerWithIdentifier:@"firstStartID"];
            [window.rootViewController presentViewController:_firstStartViewController animated:animated completion:nil];
        } else {
            if([self.presentedViewController isKindOfClass:[LoginViewController class]]){
                NSLog(@"Login view already presented, don't show it again");
                return;
            }
            [window.rootViewController presentViewController:_loginViewController animated:animated completion:nil];
        }
    }
}

-(void) showWelcomeWizard {
    // User is not initialized we must show the welcome wizard.
    if(!_loginViewController){
        _loginViewController = [[UIStoryboardManager sharedInstance].welcomePagesStoryBoard instantiateViewControllerWithIdentifier:@"loginControllerID"];
    }
    
    if([_loginViewController shoulShowWelcomeWizard]){
        NSLog(@"We should display wizard");
        [_loginViewController showWelcomeWizard];
        if(![self.presentedViewController isKindOfClass:[LoginViewController class]]){
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:_loginViewController animated:YES completion:nil];
        }
    } else {
        NSLog(@"Wizard is not needed");
        [_loginViewController dismissViewControllerAnimated:YES completion:nil];
        _loginViewController = nil;
    }
}

-(void) autoLogin {
    [_loginManager connect];
}

-(void) didLogin:(NSNotification*) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    
    // Open wizard if needed
    [self showWelcomeWizard];
}

-(void) didReconnect:(NSNotification*) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    // Open wizard if needed
    [self showWelcomeWizard];
}

-(void) didLogout:(NSNotification*) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    NSError *error = (NSError *)notification.object;
    // error = nil in case of logout by user, code 57 or DomainNetwork error is network lost
    if(!_loginManager.loginDidSucceed){
        NSLog(@"Login never succeed so we must display the login view");
        if(!error || (error && !(error.code == 57 || [error.domain isEqualToString:(NSString *)kCFErrorDomainCFNetwork]))){
            [self showLoginView:YES autoLogin:NO];
        }
    }
    // Specific error sent when user has been logged out after account deletion
    else if(error && error.code == -998) {
        [self showLoginView:NO autoLogin:NO];
    }
    
    // We don't use the customBadge on iOS 10 and more
    [self.tabBar.items enumerateObjectsUsingBlock:^(UITabBarItem * obj, NSUInteger idx, BOOL * stop) {
        if([UIDevice currentDevice].systemMajorVersion < 10){
            [obj setCustomBadgeValue:nil withFont:nil andFontColor:nil andBackgroundColor:nil presentAtTop:NO forPresence:ContactPresenceUnavailable showMobileIcon:NO];
        } else {
            obj.badgeValue = nil;
        }
    }];
}

-(void) didFailedToAuthenticate:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didFailedToAuthenticate:notification];
        });
        return;
    }
    
    // Display error popup only if we never succeed to login.
    if(!_loginManager.loginDidSucceed){
        NSError *error = (NSError *) notification.object;
        [self showLoginView:YES autoLogin:NO];
        [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error while login", nil) message:error? NSLocalizedString([error localizedDescription],nil):NSLocalizedString(@"Could not read server answer", nil) inViewController:self];
    }
}

-(void) clickToCallMobile:(NSNotification *) notification {
    NSString *phoneNumber = notification.object;
    if (phoneNumber) {
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
            // prompt to call the number
            NSString *telWithoutSpaces = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *numberURL = [NSString stringWithFormat:@"telprompt://%@", telWithoutSpaces];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:numberURL]];
        }
    }
}

- (BOOL)  tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
        // http://stackoverflow.com/questions/5161730/iphone-how-to-switch-tabs-with-an-animation
    NSUInteger controllerIndex = [self.viewControllers indexOfObject:viewController];
    
    if (controllerIndex == tabBarController.selectedIndex) {
        
        // Re-click on the same tab ?
        // We handle this as a 'scroll-to-top', if the displayed controller is the root-controller and a TableViewController
        if ([viewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *navigationController = (UINavigationController*) viewController;
            UIViewController *rootController = navigationController.viewControllers[0];
            UIViewController *topController = navigationController.topViewController; // this is the currently displayed controller.
            
            if (rootController == topController) {
                if([topController isKindOfClass:[UITableViewController class]]){
                    UITableViewController *tableController = (UITableViewController*)topController;
                    // The content inset is important here !
                    [tableController.tableView setContentOffset:CGPointMake(0.0f, -tableController.tableView.contentInset.top) animated:YES];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
                } else if([topController canPerformAction:@selector(scrollToTop) withSender:nil]){
                    [topController performSelector:@selector(scrollToTop) withObject:nil afterDelay:0];
                }
#pragma clang diagnostic pop
            }
        }
        
        return NO;
    }
    
    return YES;
}

-(void) tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"tabBarControllerDidSelectViewController" object:nil];
    if ([viewController isKindOfClass:[UINavigationController class]])
        [(UINavigationController *)viewController popToRootViewControllerAnimated:NO];
}

-(void) localizeTabBar {
    UITabBar* tabBar = self.tabBar;
    for (int i = 0; i<tabBar.items.count; i++) {
        UITabBarItem *item = (UITabBarItem *)(self.tabBar.items[i]);
        item.title = NSLocalizedString(item.title, nil);
    }
}


-(void) shouldUpdateConversationBadgeValue:(NSNotification *) notification {
    NSInteger totalConversation = [_conversationsManager totalNbOfUnreadMessagesInAllConversations];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UITabBar* tabBar = self.tabBar;
        // Update number of unread messages for Conversation tab badge.
        UITabBarItem *firstItem = (UITabBarItem*) [tabBar.items objectAtIndex:0];
        
        if([UIDevice currentDevice].systemMajorVersion > 10){
            firstItem.badgeValue = (totalConversation>0)?[NSString stringWithFormat:@"%ld",(long)totalConversation]:nil;
            firstItem.badgeColor = [UITools notificationBadgeColor];
            [firstItem setBadgeTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:12]} forState:UIControlStateNormal];
        } else {
            [firstItem setCustomBadgeValue:(totalConversation>0)?[NSString stringWithFormat:@"%ld",(long)totalConversation]:nil withFont:[UIFont fontWithName:[UITools defaultFontName] size:12] andFontColor:[UIColor whiteColor] andBackgroundColor:[UITools notificationBadgeColor] presentAtTop:YES forPresence:ContactPresenceUnavailable showMobileIcon:NO];
        }
    });
    
}
-(void) shouldUpdateRoomBadgeValue:(NSNotification *) notification {
    NSUInteger totalRooms = [_roomsService numberOfPendingRoomInvitations];
    NSUInteger totalConferenceRooms = [_roomsService numberOfPendingConferenceRoomInvitations];
    dispatch_async(dispatch_get_main_queue(), ^{
        UITabBar* tabBar = self.tabBar;
        UITabBarItem *secondItem = (UITabBarItem*) [tabBar.items objectAtIndex:1];
        if([UIDevice currentDevice].systemMajorVersion > 10){
            secondItem.badgeValue = (totalRooms > 0)?[NSString stringWithFormat:@"%ld",(long)totalRooms]:nil;
            secondItem.badgeColor = [UITools notificationBadgeColor];
            [secondItem setBadgeTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:12]} forState:UIControlStateNormal];
        } else {
            [secondItem setCustomBadgeValue:(totalRooms > 0)?[NSString stringWithFormat:@"%ld",(long)totalRooms]:nil withFont:[UIFont fontWithName:[UITools defaultFontName] size:12] andFontColor:[UIColor whiteColor] andBackgroundColor:[UITools notificationBadgeColor] presentAtTop:YES forPresence:ContactPresenceUnavailable showMobileIcon:NO];
        }
        
        UITabBarItem *conferencesItem = (UITabBarItem*) [tabBar.items objectAtIndex:2];
        if([UIDevice currentDevice].systemMajorVersion > 10){
            conferencesItem.badgeValue = (totalConferenceRooms > 0)?[NSString stringWithFormat:@"%ld",(long)totalConferenceRooms]:nil;
            conferencesItem.badgeColor = [UITools notificationBadgeColor];
            [conferencesItem setBadgeTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:12]} forState:UIControlStateNormal];
        } else {
            [conferencesItem setCustomBadgeValue:(totalConferenceRooms > 0)?[NSString stringWithFormat:@"%ld",(long)totalConferenceRooms]:nil withFont:[UIFont fontWithName:[UITools defaultFontName] size:12] andFontColor:[UIColor whiteColor] andBackgroundColor:[UITools notificationBadgeColor] presentAtTop:YES forPresence:ContactPresenceUnavailable showMobileIcon:NO];
        }
    });
}


-(void) shouldUpdateContactBadgeValue:(NSNotification *) notification {
    NSUInteger totalPendingContacts = [_contactsManager totalNbOfPendingInvitations];
    NSUInteger totalPendingCompanyInvitation = [[ServicesManager sharedInstance].companiesService totalNbOfPendingCompanyInvitations];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UITabBar* tabBar = self.tabBar;
        NSUInteger totalInvitations = totalPendingContacts+totalPendingCompanyInvitation;
        UITabBarItem *thirdItem = (UITabBarItem*) [tabBar.items objectAtIndex:3];
        if([UIDevice currentDevice].systemMajorVersion > 10){
            thirdItem.badgeValue = (totalInvitations > 0)?[NSString stringWithFormat:@"%ld",(long)totalInvitations]:nil;
            thirdItem.badgeColor = [UITools notificationBadgeColor];
            [thirdItem setBadgeTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:12]} forState:UIControlStateNormal];
        } else {
            [thirdItem setCustomBadgeValue:(totalInvitations > 0)?[NSString stringWithFormat:@"%ld",(long)totalInvitations]:nil withFont:[UIFont fontWithName:[UITools defaultFontName] size:12] andFontColor:[UIColor whiteColor] andBackgroundColor:[UITools notificationBadgeColor] presentAtTop:YES forPresence:ContactPresenceUnavailable showMobileIcon:NO];
        }
    });
}

-(void) shouldUpdateCallLogBadgeValue:(NSNotification *) notification {
    NSUInteger totalVoiceMails = [[ServicesManager sharedInstance].myUser voiceMailCount];
    NSInteger totalCallLogs = _callLogsService.totalNbOfUnreadCallLogs;
    dispatch_async(dispatch_get_main_queue(), ^{
        UITabBar* tabBar = self.tabBar;
        NSUInteger totalCallLogsAndVoiceMails = totalVoiceMails+totalCallLogs;
        // Update number of unread messages for callLogs and voice mails tab badge.
        UITabBarItem *callLogItem = (UITabBarItem*) [tabBar.items objectAtIndex:4];
        if([UIDevice currentDevice].systemMajorVersion > 10){
            callLogItem.badgeValue = (totalCallLogsAndVoiceMails > 0)?[NSString stringWithFormat:@"%ld",(long)totalCallLogsAndVoiceMails]:nil;
            callLogItem.badgeColor = [UITools notificationBadgeColor];
            [callLogItem setBadgeTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:12]} forState:UIControlStateNormal];
        } else {
            [callLogItem setCustomBadgeValue:(totalCallLogsAndVoiceMails>0)?[NSString stringWithFormat:@"%ld",(long)totalCallLogsAndVoiceMails]:nil withFont:[UIFont fontWithName:[UITools defaultFontName] size:12] andFontColor:[UIColor whiteColor] andBackgroundColor:[UITools notificationBadgeColor] presentAtTop:YES forPresence:ContactPresenceUnavailable showMobileIcon:NO];
        }
    });
}

-(void) createAccountTapped:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self createAccountTapped:notification];
        });
        return;
    }
    
    if(!_loginViewController){
        _loginViewController = [[UIStoryboardManager sharedInstance].welcomePagesStoryBoard instantiateViewControllerWithIdentifier:@"loginControllerID"];
    }
    
    [_loginViewController createAccountTapped:nil];
    _loginViewController.modalPresentationStyle = UIModalPresentationCustom;
    _loginViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [_firstStartViewController dismissViewControllerAnimated:NO completion:nil];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:_loginViewController animated:YES completion:nil];
}

-(void) alreadyMemberTapped:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alreadyMemberTapped:notification];
        });
        return;
    }
    
    if(!_loginViewController){
        _loginViewController = [[UIStoryboardManager sharedInstance].welcomePagesStoryBoard instantiateViewControllerWithIdentifier:@"loginControllerID"];
    }
    
    [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    _loginViewController.modalPresentationStyle = UIModalPresentationCustom;
    _loginViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [_firstStartViewController dismissViewControllerAnimated:NO completion:nil];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:_loginViewController animated:YES completion:nil];
}

#pragma mark - start with invitation
-(void) startWithInvitation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self startWithInvitation:notification];
        });
        return;
    }
    
    if(!_loginManager.loginDidSucceed && [ServicesManager sharedInstance].myUser.isGuest){
        [[ServicesManager sharedInstance].loginManager connect];
    }else {
        if(_loginManager.loginDidSucceed)
            [_loginManager performSelectorInBackground:@selector(disconnect) withObject:nil];
    }
    
    if(!_loginViewController){
        _loginViewController = [[UIStoryboardManager sharedInstance].welcomePagesStoryBoard instantiateViewControllerWithIdentifier:@"loginControllerID"];
    }
    
    NSDictionary *userInfo = (NSDictionary*)notification.object;
    [_loginViewController showWelcomeWizardWithInfo:userInfo];
    _loginViewController.modalPresentationStyle = UIModalPresentationCustom;
    _loginViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [_firstStartViewController dismissViewControllerAnimated:NO completion:nil];
    if(![self.presentedViewController isKindOfClass:[LoginViewController class]]){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:_loginViewController animated:YES completion:nil];
        });
    }
}

-(void) willFinalizeGuestAccount:(NSNotification *) notification {
    
    if(!_loginViewController){
        _loginViewController = [[UIStoryboardManager sharedInstance].welcomePagesStoryBoard instantiateViewControllerWithIdentifier:@"loginControllerID"];
    }

    _loginViewController.guestAccountFinalizationNeeded = YES;
    [_loginViewController showWelcomeWizard];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:_loginViewController animated:YES completion:nil];
}

@end
