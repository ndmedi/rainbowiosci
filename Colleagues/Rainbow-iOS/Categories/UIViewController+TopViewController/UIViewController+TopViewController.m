/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIViewController+TopViewController.h"

@implementation UIViewController (TopViewController)
+ (UIViewController *) topViewController {
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    return [rootViewController topVisibleViewController];
}

- (UIViewController *)topVisibleViewController {
    if ([self isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarController = (UITabBarController *)self;
        return [tabBarController.selectedViewController topVisibleViewController];
    } else if ([self isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)self;
        return [navigationController.visibleViewController topVisibleViewController];
    } else if (self.presentedViewController) {
        return [self.presentedViewController topVisibleViewController];
    } else if (self.childViewControllers.count > 0) {
        return [self.childViewControllers.lastObject topVisibleViewController];
    }
    
    return self;
}
@end
