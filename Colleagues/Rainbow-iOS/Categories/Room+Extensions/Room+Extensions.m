/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Room+Extensions.h"

@implementation Room (Extensions)
-(BOOL) canJoin {
    if([ServicesManager sharedInstance].conferencesManagerService.hasJoinedConference){
        // We couldn't join more than one conference at a time
        return NO;
    }
    if(self.isMyRoom){
        // I'm the owner
        if(self.conference.type == ConferenceTypeInstant){
            if(self.conference.endpoint){
                if(self.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                    ConfEndpoint *instantBridge = [[ServicesManager sharedInstance].conferencesManagerService pstnInstantConferenceEndPoint];
                    if(instantBridge.attachedRoomID && [instantBridge.attachedRoomID isEqualToString:self.rainbowID]){
                        if(self.conference.isActive){
                            if([ServicesManager sharedInstance].myUser.isAllowedToUseTelephonyConference)
                                return YES;
                        }
                    } else {
                        if(instantBridge.attachedRoomID)
                            return NO;
                        return YES;
                    }
                } else if(self.conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC) {
                    if([ServicesManager sharedInstance].myUser.isAllowedToUseWebRTCTelephonyConference && self.participants.count <= [ServicesManager sharedInstance].myUser.maxNumberOfWebRTCParticipantsPerRoom &&
                       ![[ServicesManager sharedInstance].conferencesManagerService hasContact:[ServicesManager sharedInstance].myUser.contact joinedConference:self.conference])
                        return YES;
                    else {
                        return NO;
                    }
                }
            } else {
                if(![ServicesManager sharedInstance].conferencesManagerService.pstnInstantConferenceEndPoint.attachedRoomID)
                    return YES;
            }
            
        } else if(self.conference.type == ConferenceTypeScheduled){
            if(self.conference.endedConference){
                return NO;
            }
            if([self.conference.end isEarlierThanDate:[NSDate date]]){
                return NO;
            }
            NSTimeInterval intervalToBeginning = [self.conference.start timeIntervalSinceNow];
            if(intervalToBeginning <= kMeetingJoinBeforeStartIntervalInSeconds)
                return YES;
        } else if(!self.conference){
            // Check if we can attach a webrtc bridge
            if([ServicesManager sharedInstance].myUser.isAllowedToUseWebRTCTelephonyConference && self.participants.count <= [ServicesManager sharedInstance].myUser.maxNumberOfWebRTCParticipantsPerRoom){
                    return YES;
            }
        }
    } else {
        if(self.conference.endpoint){
            if(self.conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC) {
                if([ServicesManager sharedInstance].myUser.isAllowedToParticipateInWebRTCMobile)
                    if((self.conference.myConferenceParticipant == nil  || (self.conference.myConferenceParticipant.state != ParticipantStateConnected &&
                       self.conference.myConferenceParticipant.state != ParticipantStateRinging)) &&
                       self.conference.isActive && self.conference.canJoin)
                        return YES;
            }
            if(self.conference.type == ConferenceTypeInstant){
                if(self.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                    if(self.conference.isActive /*&& self.conference.canJoin*/){
                        return YES;
                    }
                }
            }
            else if(self.conference.type == ConferenceTypeScheduled){
                if(!self.conference.isActive){
                    return NO;
                }
                if([self.conference.end isEarlierThanDate:[NSDate date]]){
                    return NO;
                }
                if(self.conference.endedConference){
                    return NO;
                }
                NSTimeInterval intervalToBeginning = [self.conference.start timeIntervalSinceNow];
                if(intervalToBeginning <= kMeetingJoinBeforeStartIntervalInSeconds)
                    return YES;
            }
        }
    }
    return NO;
}

@end
