/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "File+DefaultImage.h"
#import "UITools.h"

@implementation File (DefaultImage)
-(UIImage *) thumbnail {
    NSString *imageName = @"unknown";
    
    switch (self.type) {
        case FileTypeImage:{
            if(self.data || self.thumbnailData)
                return self.data?[UIImage imageWithData:self.data]:[UIImage imageWithData:self.thumbnailData];
            else
                imageName = @"image";
            break;
        }
        case FileTypePDF:{
            if(self.thumbnailData)
                return [UIImage imageWithData:self.thumbnailData];
            else
                imageName = @"pdf";
            break;
        }
        case FileTypeDoc:{
            imageName = @"doc";
            break;
        }
        case FileTypePPT:{
            imageName = @"ppt";
            break;
        }
        case FileTypeXLS:{
            imageName = @"xls";
            break;
        }
        case FileTypeVideo:
        case FileTypeAudio:{
            imageName = @"audioVideo";
            break;
        }
        case FileTypeOther:{
            imageName = @"unknown";
            break;
        }
        default:
            break;
    }
    
    if(!self.isDownloadAvailable || !self.canDownloadFile){
        imageName = [NSString stringWithFormat:@"%@Failed",imageName];
    }
    
    return [UIImage imageNamed:imageName];
}

-(UIImage *) thumbnailBig {
    NSString *imageName = @"unknownBig";

    switch (self.type) {
        case FileTypeImage:{
            if(self.data || self.thumbnailData)
                return self.data?[UIImage imageWithData:self.data]:[UIImage imageWithData:self.thumbnailData];
            else
                imageName = @"imageBig";
            break;
        }
        case FileTypePDF:{
            imageName = @"pdfBig";
            break;
        }
        case FileTypeDoc:{
            imageName = @"docBig";
            break;
        }
        case FileTypePPT:{
            imageName = @"pptBig";
            break;
        }
        case FileTypeXLS:{
            imageName = @"xlsBig";
            break;
        }
        case FileTypeVideo:
        case FileTypeAudio:{
            imageName = @"audioVideoBig";
            break;
        }
        case FileTypeOther:{
            imageName = @"unknownBig";
            break;
        }
        default:
            break;
    }
    
    return [UIImage imageNamed:imageName];
}

-(UIColor *) tintColor {
    switch (self.type) {
        case FileTypeImage:{
            if(!self.data)
                return [UITools colorFromHexa:0x00B2A9FF];
            break;
        }
        case FileTypePDF:{
            return [UITools colorFromHexa:0xFF4500FF];
            break;
        }
        case FileTypeDoc:{
            return [UITools colorFromHexa:0x0085CAFF];
            break;
        }
        case FileTypePPT:{
            return [UITools colorFromHexa:0xFF4500FF];
            break;
        }
        case FileTypeXLS:{
            return [UITools colorFromHexa:0x34B233FF];
            break;
        }
        case FileTypeVideo:
        case FileTypeAudio:{
            return [UITools colorFromHexa:0x007356FF];
            break;
        }
        case FileTypeOther:{
            return [UITools defaultTintColor];
            break;
        }
        default:
            return [UITools defaultTintColor];
            break;
    }
    return [UITools defaultTintColor];
}
@end
