/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Rainbow/Contact.h>
#import <UIKit/UIKit.h>

@interface Contact (Extensions)
@property (nonatomic, readonly) NSString *displayName;
@property (nonatomic, readonly) NSString *initials;

/**
 *  Get the contact first letter based on lastname or firstname or companyname etc...
 *
 *  @return First letter for the contact
 */
@property (nonatomic, readonly) NSString * contactFirstLetter;


@property (nonatomic, readonly) BOOL canCallInWebRTC;

@property (nonatomic, readonly) BOOL canCallInWebRTCVideo;

@property (nonatomic, readonly) UIImage *image;

-(UIImage *) imageWithSize:(CGSize) size;
@end
