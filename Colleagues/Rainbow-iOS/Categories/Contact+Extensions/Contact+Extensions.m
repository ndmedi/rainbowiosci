/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Contact+Extensions.h"
#import <Rainbow/ServicesManager.h>
#import "UITools.h"
#import "UIImageView+Letters.h"
#import <objc/runtime.h>

@interface Contact (Private)
@end

@implementation Contact (Extensions)

-(NSString *) fullName {
    BOOL displayFirstNameFirst = [ServicesManager sharedInstance].contactsManagerService.displayFirstNameFirst;
    
    NSMutableString *format = [[NSMutableString alloc] init];
    NSInteger nbParams = 0;
    if(self.firstName) {
        [format appendString:@"%@"];
        nbParams++;
    }
    if(self.firstName && self.lastName)
        [format appendString:@" "];
    if(self.lastName){
        [format appendString:@"%@"];
        nbParams++;
    }
    
    if(nbParams == 1){
        if(self.firstName)
            return [NSString stringWithFormat:format, [self.firstName capitalizedString]];
        else
            return [NSString stringWithFormat:format, [self.lastName capitalizedString]];
    } else {
        if(displayFirstNameFirst)
            return [NSString stringWithFormat:format, [self.firstName capitalizedString], [self.lastName capitalizedString]];
        else
            return [NSString stringWithFormat:format, [self.lastName capitalizedString], [self.firstName capitalizedString]];
    }
}


-(NSString *) displayName {
    @synchronized (self) {
        if(self.fullName.length > 0) {
            return self.fullName;
        } else if (self.emailAddresses.count > 0){
            NSString *firstEmail = ((EmailAddress *)self.emailAddresses[0]).address;
            return firstEmail;
        } else if(self.companyName.length > 0){
            return self.companyName;
        } else if(self.phoneNumbers.count > 0){
            NSString *firstPhoneNumber = ((PhoneNumber*)self.phoneNumbers[0]).number;
            return firstPhoneNumber;
        }
         else {
            return NSLocalizedString(@"Unknown", nil);
        }
    }
}


-(NSString *) initials {
    BOOL displayFirstNameFirst = [ServicesManager sharedInstance].contactsManagerService.displayFirstNameFirst;
    NSMutableString *displayString = [NSMutableString stringWithString:@""];
    
    NSMutableArray *words = nil;
    if(self.fullName.length > 0){
        if(displayFirstNameFirst){
            // First name world
            if(self.firstName.length > 0){
                NSRange firstLetterFirstNameRange = [self.firstName rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 1)];
                [displayString appendString:[self.firstName substringWithRange:firstLetterFirstNameRange]];
                [displayString appendString:@" "];
            }
            
            if(self.lastName.length > 0){
                NSRange firstLetterLastNameRange = [self.lastName rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 1)];
                [displayString appendString:[self.lastName substringWithRange:firstLetterLastNameRange]];
            }
        } else {
            if(self.lastName.length > 0){
                NSRange firstLetterLastNameRange = [self.lastName rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 1)];
                [displayString appendString:[self.lastName substringWithRange:firstLetterLastNameRange]];
                [displayString appendString:@" "];
            }
            
            // First name world
            if(self.firstName.length > 0){
                NSRange firstLetterFirstNameRange = [self.firstName rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 1)];
                [displayString appendString:[self.firstName substringWithRange:firstLetterFirstNameRange]];
            }
        }
        return displayString;
    } else if(self.companyName.length > 0)
        words = [[self.companyName componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] mutableCopy];
    else if(self.phoneNumbers.count > 0)
        words = [[@"#" componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] mutableCopy];
    else if(self.emailAddresses.count > 0)
        words = [[((EmailAddress*)self.emailAddresses[0]).address componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] mutableCopy];
    else
        words = [[@"?" componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] mutableCopy];
    
    //
    // Get first letter of the first and last word
    //
    if ([words count]) {
        NSString *firstWord = [words firstObject];
        if ([firstWord length]) {
            // Get character range to handle emoji (emojis consist of 2 characters in sequence)
            NSRange firstLetterRange = [firstWord rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 1)];
            [displayString appendString:[firstWord substringWithRange:firstLetterRange]];
        }
        
        if ([words count] >= 2) {
            NSString *lastWord = [words lastObject];
            
            while ([lastWord length] == 0 && [words count] >= 2) {
                [words removeLastObject];
                lastWord = [words lastObject];
            }
            
            if ([words count] > 1) {
                // Get character range to handle emoji (emojis consist of 2 characters in sequence)
                NSRange lastLetterRange = [lastWord rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 1)];
                [displayString appendString:[lastWord substringWithRange:lastLetterRange]];
            }
        }
    }
    return displayString;
}

-(NSString *) contactFirstLetter {
    BOOL sortByFirstName = [ServicesManager sharedInstance].contactsManagerService.sortByFirstName;
    NSString *contactFirstLetter = nil;
    if(sortByFirstName){
        if(self.firstName.length > 0)
            contactFirstLetter = [[self.firstName substringToIndex:1] uppercaseString];
        if(contactFirstLetter.length == 0 && self.lastName.length > 0)
            contactFirstLetter = [[self.lastName substringToIndex:1] uppercaseString];
    } else {
        if(self.lastName.length > 0)
            contactFirstLetter = [[self.lastName substringToIndex:1] uppercaseString];
        if(contactFirstLetter.length == 0 && self.firstName.length > 0)
            contactFirstLetter = [[self.firstName substringToIndex:1] uppercaseString];
    }
    if(contactFirstLetter.length == 0 && self.companyName.length > 0)
        contactFirstLetter = [[self.companyName substringToIndex:1] uppercaseString];
    if(contactFirstLetter.length == 0 && self.emailAddresses.count > 0)
        contactFirstLetter = [[((EmailAddress *)self.emailAddresses[0]).address substringToIndex:1] uppercaseString];

    if(contactFirstLetter.length == 0){
        if([contactFirstLetter rangeOfString:@"\\d" options:NSRegularExpressionSearch].location != NSNotFound || [contactFirstLetter rangeOfString:@"\\w+" options:NSRegularExpressionSearch].location == NSNotFound){
            contactFirstLetter = @"#";
        }
    }
    return contactFirstLetter;
}

-(BOOL) canCallInWebRTC {
    /**
     *     Pour chaque contact, j’ai un objet avec ses capabilities :
     o   Je peux lui faire un appel vers ce contact si :
     o   je n’ai pas déjà un appel avec lui
     o   si son statut est « online » ou « away » ou « online-mobile » (mais il y a un ressource web/desktop aussi présent)
     o   si son statut est « busy » mais en appel téléphonique (peut-être pas très intéressant pour vous dans un premier temps..)
     
     Mes capabilities :
     -          je peux faire un appel si je ne suis pas déjà dans un appel WebRTC
     *
     */
    if(![ServicesManager sharedInstance].myUser.isAllowedToUseWebRTCMobile){
        NSLog(@"Can't call in webrtc, user not allow to use webrtc on mobile");
        return NO;
    }
    
    if([[ServicesManager sharedInstance].rtcService hasActiveCalls]){
        NSLog(@"Can't call in webrtc, user already have calls");
        return NO;
    }
    if(self.isBot){
        NSLog(@"Can't call in webrtc, user is a bot");
        return NO;
    }
    
    BOOL canCall = self.canChatWith;
    if(!canCall){
        NSLog(@"Can't call in webrtc, can't tchat with this contact");
    }
    
    // If no presence we can call
    if(self.presence && self.presence.presence == ContactPresenceUnavailable){
        NSLog(@"Can't call in webrtc, contact presence is not in a valid case");
        canCall = NO;
    }
    
    return canCall;
}

-(BOOL) canCallInWebRTCVideo {
    return self.canCallInWebRTC;
}

-(UIImage *) image {
   return [self imageWithSize:CGSizeMake(30, 30)];
}

-(UIImage *) imageWithSize:(CGSize) size {
    UIImage *image = nil;
    if(self.photoData.length > 0) {
        image = [UITools imageWithImage:[UIImage imageWithData:self.photoData] scaledToSize:size circular: YES];
    } else if (self.displayName.length > 0 || self.initials.length > 0) {
        UIImageView *temp = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        temp.contentMode = UIViewContentModeScaleAspectFit;
        
        UIColor* color = [UITools colorForString:self.displayName];
        if(self.initials.length == 2)
            [temp setImageWithString:self.displayName color:color  circular:YES fontName:[UITools defaultFontName]];
        else
            [temp setImageWithString:self.initials color:color  circular:YES fontName:[UITools defaultFontName]];
        image = temp.image;
    }
    return image;
}

@end
