/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RTCCallView.h"
#import <Rainbow/RTCService.h>
#import <AVFoundation/AVFoundation.h>
#import "Contact+Extensions.h"
#import "UITools.h"


static CGFloat const kButtonPadding = 16;
static CGFloat const kButtonSize = 64;

static CGFloat const kContactSize = 128;

static CGFloat const kLocalVideoViewSize = 120;
static CGFloat const kLocalVideoViewPadding = 8;


@interface RTCCallView () <RTCEAGLVideoViewDelegate>

@property(nonatomic, weak) id<RTCCallViewDelegate> delegate;

@property(nonatomic, strong) UILabel *contactNameLabel;

@end

@implementation RTCCallView {
    UIButton *_answerButton;
    UIButton *_cameraSwitchButton;
    UIButton *_hangupButton;
    UIButton *_statsButton;
    UIImageView *_contactPhoto;
    CGSize _remoteVideoSize;
    BOOL _useRearCamera;
}

- (instancetype)initWithFrame:(CGRect)frame withDelegate:(id<RTCCallViewDelegate>)delegate with:(Contact *)contact features:(RTCCallFeatureFlags)features {
    NSLog(@"initCallView");
    if (self = [super initWithFrame:frame]) {
        _delegate = delegate;
        
        // Init the view.
        // Create all the components here.
        
        //RTCCall *call = [_delegate rtcCall];
        
        
        self.backgroundColor = [UITools defaultBackgroundColor];
        
        // The contact image
        UIImage *contactPhotoImage = contact.photoData ? [UIImage imageWithData:contact.photoData]:[UIImage imageNamed:@"DefaultAvatar"];
        _contactPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 128, 128)];
        _contactPhoto.layer.cornerRadius = kContactSize / 2;
        _contactPhoto.layer.masksToBounds = YES;
        [_contactPhoto setImage:contactPhotoImage];
        [self addSubview:_contactPhoto];
        
        // The contact name
        _contactNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _contactNameLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:16];
        _contactNameLabel.textColor = [UIColor darkGrayColor];
        _contactNameLabel.text = contact.displayName;
        [self addSubview:_contactNameLabel];
        
        // The video renderers (if needed)
        if (features & RTCCallFeatureVideo) {
            
            _remoteVideoView = [[RTCEAGLVideoView alloc] initWithFrame:CGRectZero];
            _remoteVideoView.delegate = self;
            [self addSubview:_remoteVideoView];
            //[_remoteVideoView setHidden:YES];
            
            _localVideoView = [[RTCCameraPreviewView alloc] initWithFrame:CGRectZero];
            [self addSubview:_localVideoView];
            //[_localVideoView setHidden:YES];
            
            _cameraSwitchButton = [UIButton buttonWithType:UIButtonTypeCustom];
            _cameraSwitchButton.backgroundColor = [UIColor whiteColor];
            _cameraSwitchButton.layer.cornerRadius = kButtonSize / 2;
            _cameraSwitchButton.layer.masksToBounds = YES;
            [_cameraSwitchButton setImage:[UIImage imageNamed:@"switch_camera"] forState:UIControlStateNormal];
            [_cameraSwitchButton addTarget:self
                                    action:@selector(onCameraSwitch:)
                          forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:_cameraSwitchButton];
        }
        
        // The answer green button
        _answerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _answerButton.backgroundColor = [UIColor greenColor];
        _answerButton.layer.cornerRadius = kButtonSize / 2;
        _answerButton.layer.masksToBounds = YES;
        [_answerButton setImage:[UIImage imageNamed:@"Phone"] forState:UIControlStateNormal];
        [_answerButton addTarget:self
                          action:@selector(onAnswer:)
                forControlEvents:UIControlEventTouchUpInside];
        _answerButton.titleLabel.text = NSLocalizedString(@"Answer", nil);
        _answerButton.tintColor = [UIColor whiteColor];
        //_answerButton.hidden = call.isAnswered;
        [self addSubview:_answerButton];
        
        // the hangup/cancel red button
        _hangupButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _hangupButton.backgroundColor = [UIColor redColor];
        _hangupButton.layer.cornerRadius = kButtonSize / 2;
        _hangupButton.layer.masksToBounds = YES;
        [_hangupButton setImage:[UIImage imageNamed:@"Phone"] forState:UIControlStateNormal];
        [_hangupButton addTarget:self action:@selector(onHangup:) forControlEvents:UIControlEventTouchUpInside];
        _hangupButton.tintColor = [UIColor whiteColor];
        [self addSubview:_hangupButton];
        
        // The stats button
        _statsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _statsButton.backgroundColor = [UIColor grayColor];
        _statsButton.layer.cornerRadius = kButtonSize / 2;
        _statsButton.layer.masksToBounds = YES;
        [_statsButton addTarget:self
                          action:@selector(onStats:)
                forControlEvents:UIControlEventTouchUpInside];
        _statsButton.titleLabel.text = NSLocalizedString(@"Stats", nil);
        _statsButton.tintColor = [UIColor grayColor];
        [self addSubview:_statsButton];
    }
    NSLog(@"initCallView OK");
    [self setNeedsLayout];
    return self;
}



- (void)layoutSubviews {
    NSLog(@"layoutSubview CallView");
    CGRect bounds = self.bounds;
    if (_remoteVideoSize.width > 0 && _remoteVideoSize.height > 0) {
        NSLog(@"layoutSubview width > 0");
        // Aspect fill remote video into bounds.
        CGRect remoteVideoFrame =
        AVMakeRectWithAspectRatioInsideRect(_remoteVideoSize, bounds);
        CGFloat scale = 1;
        if (remoteVideoFrame.size.width > remoteVideoFrame.size.height) {
            // Scale by height.
            scale = bounds.size.height / remoteVideoFrame.size.height;
        } else {
            // Scale by width.
            scale = bounds.size.width / remoteVideoFrame.size.width;
        }
        remoteVideoFrame.size.height *= scale;
        remoteVideoFrame.size.width *= scale;
        _remoteVideoView.frame = remoteVideoFrame;
        _remoteVideoView.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    } else {
        NSLog(@"layoutSubview frame = bounds");
        _remoteVideoView.frame = bounds;
    }
    
    // Aspect fit local video view into a square box.
    CGRect localVideoFrame = CGRectMake(0, 0, kLocalVideoViewSize, kLocalVideoViewSize);
    // Place the view in the bottom right.
    localVideoFrame.origin.x = CGRectGetMaxX(bounds) - localVideoFrame.size.width - kLocalVideoViewPadding;
    localVideoFrame.origin.y = CGRectGetMaxY(bounds) - localVideoFrame.size.height - kLocalVideoViewPadding;
    _localVideoView.frame = localVideoFrame;
    
    
    CGFloat photoYPosition = CGRectGetMidY(bounds) - (CGRectGetMaxY(bounds)/3) - 20;
    
    // Place contact photo in center, top
    _contactPhoto.frame =
    CGRectMake(CGRectGetMidX(bounds) - (kContactSize/2),
               photoYPosition,
               kContactSize,
               kContactSize);
    
    // Place the contact name, under the previous picture
    [_contactNameLabel sizeToFit];
    _contactNameLabel.center = CGPointMake(CGRectGetMidX(bounds),
                                           photoYPosition + kContactSize + 10);
    
    // Place answer button in the middle right.
    _answerButton.frame =
    CGRectMake(CGRectGetMidX(bounds) - (kButtonSize/2) + kButtonSize,
               photoYPosition + kContactSize + 350,
               kButtonSize,
               kButtonSize);
    
    // Place hangup button in the middle left.
    if(_answerButton.hidden){
        _hangupButton.frame =
        CGRectMake(self.frame.size.width / 2 - kButtonSize/2,
                   photoYPosition + kContactSize + 350,
                   kButtonSize,
                   kButtonSize);
    } else {
        _hangupButton.frame =
        CGRectMake(CGRectGetMidX(bounds) - (kButtonSize/2) - kButtonSize,
                   photoYPosition + kContactSize + 350,
                   kButtonSize,
                   kButtonSize);
    }
    
    // Place camera switch button in the bottom left.
    _cameraSwitchButton.frame =
    CGRectMake(CGRectGetMinX(bounds) + kButtonPadding,
               CGRectGetMaxY(bounds) - kButtonPadding - kButtonSize,
               kButtonSize,
               kButtonSize);
    
    // Place the stats button to bottom right
    _statsButton.frame =
    CGRectMake(CGRectGetMaxX(bounds) - kButtonPadding - kButtonSize,
               CGRectGetMaxY(bounds) - kButtonPadding - kButtonSize,
               kButtonSize,
               kButtonSize);
}

#pragma mark - RTCEAGLVideoViewDelegate

- (void)videoView:(RTCEAGLVideoView*)videoView didChangeVideoSize:(CGSize)size {
    if (videoView == _remoteVideoView) {
        NSLog(@"didChangeVideoSize Right videoView %f %f", size.width, size.height);
        _remoteVideoSize = size;
    }
    NSLog(@"didChangeVideoSize RELAYOUT");
    [self setNeedsLayout];
}
-(void) answerCall {
    [self onAnswer:nil];
}
#pragma mark - Private

- (void)onAnswer:(id)sender {
    NSLog(@"onAnswer");
    _answerButton.hidden = YES;
    [self layoutSubviews];
    [_delegate rtcCallViewDidAnswer:self];
}

- (void)onHangup:(id)sender {
    NSLog(@"onHangup");
    [_delegate rtcCallViewDidHangup:self];
}

- (void)onCameraSwitch:(id)sender {
    NSLog(@"onCameraSwitch");
    [_delegate rtcCallViewDidSwitchCamera:self];
}

- (void)onStats:(id) sender {
    NSLog(@"onStats");
    [_delegate rtcCallViewDidStats:self];
}

@end
