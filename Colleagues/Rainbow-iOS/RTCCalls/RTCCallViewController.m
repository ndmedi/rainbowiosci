/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RTCCallViewController.h"
#import "RTCCallView.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/RTCService.h>
#import <WebRTC/WebRTC.h>
#import <AVFoundation/AVFoundation.h>
#import "RTCCallStatsViewController.h"
#import "UITools.h"
#import <MarqueeLabel/MarqueeLabel.h>
#import "UIAvatarView.h"
#import "UIView+MGBadgeView.h"
#import "GoogleAnalytics.h"
#import "RTCCallBubblesView.h"
#import "UIImage+Additions.h"
#import "RTCCameraPreviewView+PreviewLayer.h"
#import <AVFoundation/AVFoundation.h>
#import <GLKit/GLKit.h>
#import <Rainbow/UIDevice+VersionCheck.h>
#import "PublishersTableViewController.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import <objc/runtime.h>
#import "Firebase.h"
#define kAnimationDuration 0.2f
#define kAnswerDeclinePositionRatio 0.20f

#define answerDefaultColor 0x34B233FF
#define answerDisabledColor 0xB2B2B2FF
#define hangupDefaultColor 0xCF0072FF
#define hangupDisabledColor 0xB2B2B2FF
#define blueDefaultColor 0x0085CAFF
#define blueSelectedColor 0x4DAADAFF

typedef NS_ENUM(NSInteger, RTCQualityIndicator) {
    RTCQualityIndicatorNone = 0,
    RTCQualityIndicatorLow,
    RTCQualityIndicatorMedium,
    RTCQualityIndicatorHigh
};

typedef NS_ENUM(NSInteger, RTCStatParameters) {
    audioRttOnSent = 0,
    audioPacketsLostOnSentInPercent,
    audioJitterOnRecv,
    audioPacketsLostOnRecvInPercent,
    videoRttOnSent,
    videoPacketsLostOnSentInPercent,
    videoJitterOnRecv,
    videoPacketsLostOnRecvInPercent
};

@interface RTCCallViewController () <UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning, RTCEAGLVideoViewDelegate, UIPopoverPresentationControllerDelegate>

@property (nonatomic, strong) RTCService *rtcService;
@property (nonatomic, strong) TelephonyService *telephonyService;

@property (nonatomic, assign) BOOL isUsingBackCamera;

// UI elements
@property (weak, nonatomic) IBOutlet UIAvatarView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIAvatarView *backgroundAvatarImageView;
@property (weak, nonatomic) IBOutlet UIView *blueMaskView;
@property (weak, nonatomic) IBOutlet MarqueeLabel *nameLabel;
@property (weak, nonatomic) IBOutlet MarqueeLabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIView *callSettingsView;
@property (weak, nonatomic) IBOutlet UIButton *muteButton;
@property (weak, nonatomic) IBOutlet UIButton *speakerButton;
@property (weak, nonatomic) IBOutlet UIView *answerView;
@property (weak, nonatomic) IBOutlet UIButton *answerButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIView *declineCancelOrHangupView;
@property (weak, nonatomic) IBOutlet UIButton *declineCancelOrHangupButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *answerHorizontalAlignementConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *declineCancelOrHangupHorizontalAlignementConstraint;
@property (weak, nonatomic) IBOutlet UIButton *backToRainbowButton;
@property (weak, nonatomic) IBOutlet UIView *actionButtonView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet RTCEAGLVideoView *remoteVideoWithSharing;
@property (weak, nonatomic) IBOutlet UIView *callRecordingView;
@property (nonatomic, strong) UIImageView *topViewGradientView;
@property (nonatomic, strong) UIImageView *actionButtonViewGradientView;

// To shutdown the screen, when phone goes to the ear,
// we create a black full-size view manually
@property(nonatomic, strong) UIView *blackBackgroundView;

// Our blur effect when we open the CallView
@property (nonatomic) UIVisualEffectView *blurView;
@property (weak, nonatomic) IBOutlet RTCCallBubblesView *bubblesView;
@property (weak, nonatomic) IBOutlet RTCCallBubblesView *localPreviewBubblesView;

@property (nonatomic, strong) NSTimer *elapsedTimer;

// persistent data used by quality indicator
@property (nonatomic        ) int audioPacketsSent;
@property (nonatomic        ) int audioPacketsLostOnSent;
@property (nonatomic        ) int audioPacketsRecv;
@property (nonatomic        ) int audioPacketsLostOnRecv;
@property (nonatomic        ) int audioRttOnSent;
@property (nonatomic        ) int audioPacketsLostOnSentInPercent;
@property (nonatomic        ) int audioJitterOnRecv;
@property (nonatomic        ) int audioPacketsLostOnRecvInPercent;

@property (nonatomic        ) int videoPacketsSent;
@property (nonatomic        ) int videoPacketsLostOnSent;
@property (nonatomic        ) int videoPacketsRecv;
@property (nonatomic        ) int videoPacketsLostOnRecv;
@property (nonatomic        ) int videoRttOnSent;
@property (nonatomic        ) int videoPacketsLostOnSentInPercent;
@property (nonatomic        ) int videoJitterOnRecv;
@property (nonatomic        ) int videoPacketsLostOnRecvInPercent;

@property (nonatomic,strong) NSArray (*highQualitySpec);
@property (nonatomic,strong) NSArray (*mediumQualitySpec);
@property (nonatomic       ) RTCQualityIndicator qualityLevel;
@property (nonatomic       ) RTCQualityIndicator audioQualityLevel;
@property (nonatomic       ) RTCQualityIndicator videoQualityLevel;

// Quality indicator outlets
@property (weak, nonatomic) IBOutlet UIView *qualityIndicatorContainerView;
@property (weak, nonatomic) IBOutlet UIView *qualityIndicatoryLowView;
@property (weak, nonatomic) IBOutlet UIView *qualityIndicatoryMediumView;
@property (weak, nonatomic) IBOutlet UIView *qualityIndicatorHighView;

@property (nonatomic, strong) RTCCallStatsViewController *rtcCallStatsViewController;
@property (weak, nonatomic) IBOutlet UIButton *switchCamera;
@property (weak, nonatomic) IBOutlet UIButton *publishersButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *publisherButtonRightConstraint;


// Video
@property (nonatomic, weak) IBOutlet RTCCameraPreviewView *localVideoView;
@property (nonatomic, weak) IBOutlet RTCEAGLVideoView *remoteVideoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remoteVideoViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remoteVideoViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remoteVideoViewWithSharingWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remoteVideoViewWithSharingHeightConstraint;

@property (nonatomic) CGSize remoteVideoSize;
@property (nonatomic) CGSize remoteVideoWithSharingSize;
@property (nonatomic, strong) RTCVideoTrack *localVideoTrack;
@property (nonatomic, strong) RTCVideoTrack *remoteVideoTrack;
@property (nonatomic, strong) RTCVideoTrack *remoteVideoTrackWithSharing;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *localVideoTopConstraint;
@property (nonatomic, strong) NSTimer *hideUITimer;

@property (nonatomic) float lastScale;

// Set to YES when the user switch on or off the remote video or choose a remote stream
@property (nonatomic) BOOL manuallySwitchedRemoteVideo;

@property (nonatomic) UIAlertController *recordCallAlert;

@property(nonatomic, strong) RTCCameraVideoCapturer *cameraVideoCapturer;

@end

@implementation RTCCallViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCallState:) name:kTelephonyServiceDidUpdateCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCallState:) name:kTelephonyServiceDidRemoveCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddCaptureSession:) name:kRTCServiceDidAddCaptureSessionNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddLocalVideoTrack:) name:kRTCServiceDidAddLocalVideoTrackNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveLocalVideoTrack:) name:kRTCServiceDidRemoveLocalVideoTrackNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddRemoteVideoTrack:) name:kRTCServiceDidAddRemoteVideoTrackNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRemoteVideoTrack:) name:kRTCServiceDidRemoveRemoteVideoTrackNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateMessageUnreadCount:) name:kConversationsManagerDidUpdateMessagesUnreadCount object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newStatsAvailable:) name:kRTCServiceCallStatsNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConference:) name:kConferencesManagerDidUpdateConference object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alertRecordingStateChanged:) name:kRTCServiceDidChangeRecordingCallStateNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(proximityChanged:) name:UIDeviceProximityStateDidChangeNotification object:[UIDevice currentDevice]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectPublisher:) name:kPublishersDidSelected object:nil];
        
        _rtcService = [ServicesManager sharedInstance].rtcService;
        _telephonyService = [ServicesManager sharedInstance].telephonyService;
        
        _audioPacketsSent =0;
        _audioPacketsLostOnSent =0;
        _audioPacketsRecv =0;
        _audioPacketsLostOnRecv =0;
        _audioRttOnSent = 0;
        _audioPacketsLostOnSentInPercent = 0;
        _audioJitterOnRecv = 0;
        _audioPacketsLostOnRecvInPercent = 0;
        
        _videoPacketsSent =0;
        _videoPacketsLostOnSent =0;
        _videoPacketsRecv =0;
        _videoPacketsLostOnRecv =0;
        _videoRttOnSent = 0;
        _videoPacketsLostOnSentInPercent = 0;
        _videoJitterOnRecv = 0;
        _videoPacketsLostOnRecvInPercent = 0;
       
        _highQualitySpec = [NSArray alloc];
        
        _highQualitySpec = @[
          /* _audioRttOnSent */                 @[@250,@250,@250],
          /* _audioPacketsLostOnSentInPercent */@[@2  ,@2  ,@2  ],
          /* _audioJitterOnRecv */              @[@100,@100,@100],
          /* _audioPacketsLostOnRecvInPercent */@[@2  ,@2  ,@2  ],
          /* _videoRttOnSent */                 @[@50 ,@100,@200],
          /* _videoPacketsLostOnSentInPercent */@[@3  ,@2  ,@1  ],
          /* _videoJitterOnRecv */              @[@100,@100,@100],
          /* _videoPacketsLostOnRecvInPercent */@[@3  ,@2  ,@1  ]
                            ];
        
        _mediumQualitySpec = [NSArray alloc];
        
        _mediumQualitySpec = @[
          /* _audioRttOnSent */                 @[@1000,@1000,@1000,@1000,@1000],
          /* _audioPacketsLostOnSentInPercent */@[@5   ,@5   ,@5   ,@5   ,@5   ],
          /* _audioJitterOnRecv */              @[@200 ,@200 ,@200 ,@200 ,@200 ],
          /* _audioPacketsLostOnRecvInPercent */@[@5   ,@5   ,@5   ,@5   ,@5   ],
          /* _videoRttOnSent */                 @[@100 ,@150 ,@200 ,@300 ,@500 ],
          /* _videoPacketsLostOnSentInPercent */@[@8   ,@4   ,@3   ,@2   ,@1   ],
          /* _videoJitterOnRecv */              @[@200 ,@200 ,@200 ,@200 ,@200 ],
          /* _videoPacketsLostOnRecvInPercent */@[@8   ,@4   ,@3   ,@2   ,@1   ]
                             ];
        _qualityLevel = RTCQualityIndicatorNone;
        _audioQualityLevel = RTCQualityIndicatorNone;
        _videoQualityLevel = RTCQualityIndicatorNone;
        
        // Important so the Blur transition works
        // UIModalPresentationOverFullScreen is mandatory (and not UIModalPresentationOverCurrentContext)
        // because it will force the usage of OUR desired orientation (which is portrait)
        // otherwise we might depend on the previous controller orientation.
        // and we WANT to be in portrait, never landscape.
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        if (!UIAccessibilityIsReduceTransparencyEnabled()) {
            self.transitioningDelegate = self;
        }
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidUpdateCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidRemoveCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidUpdateMessagesUnreadCount object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceCallStatsNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidAddCaptureSessionNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidAddLocalVideoTrackNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidAddRemoteVideoTrackNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConferencesManagerDidUpdateConference object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidChangeRecordingCallStateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPublishersDidSelected object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceProximityStateDidChangeNotification object:[UIDevice currentDevice]];
}

#pragma mark - UIViewController

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    // This code is required for the use-case where we are on a landscape screen
    // We open the call screen, it switch to portrait during transition
    // and we relayout after transition to remain correct.
    self.blurView.frame = self.view.bounds;
    _blackBackgroundView.frame = [[UIScreen mainScreen] bounds];
    CGRect frame = _actionButtonViewGradientView.frame;
    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    _actionButtonViewGradientView.frame = frame;
    CGRect frameTop = _topView.frame;
    frameTop.size.width = [[UIScreen mainScreen] bounds].size.width;
    _topViewGradientView.frame = frameTop;
}

-(void) setCall:(NSObject<CallProtocol> *)call {
    _call = call;
    object_setClass(_call, [call class]);
    NSLog(@"SET CALL %@ WITH CLASS %@", _call, [call class]);
}

-(void) viewDidLoad {
    [super viewDidLoad];
    _topView.backgroundColor = [UIColor clearColor];
    
    UIImage *gradient = [UIImage add_imageWithGradient:@[[UITools colorFromHexa:0X0085CAFF], [UITools colorFromHexa:0X0085CAE6], [UITools colorFromHexa:0X0085CA00]] size:_topView.frame.size direction:ADDImageGradientDirectionVertical];
    _topViewGradientView = [[UIImageView alloc] initWithImage:gradient];
    [_topView addSubview:_topViewGradientView];
    [_topView sendSubviewToBack:_topViewGradientView];
    _topViewGradientView.hidden = YES;
    
    UIImage *gradientBottom = [UIImage add_imageWithGradient:@[[UITools colorFromHexa:0X0085CA00], [UITools colorFromHexa:0X0085CAFF]] size:_actionButtonView.frame.size direction:ADDImageGradientDirectionVertical];
    _actionButtonViewGradientView = [[UIImageView alloc] initWithImage:gradientBottom];
    [_actionButtonView addSubview:_actionButtonViewGradientView];
    [_actionButtonView sendSubviewToBack:_actionButtonViewGradientView];
    _actionButtonViewGradientView.hidden = YES;
    
    // Tune some UI elements :
    // Round buttons
    _answerButton.layer.cornerRadius = _answerButton.frame.size.width / 2;
    [_answerButton setBackgroundImage:[UITools imageFromColor:[UITools colorFromHexa:answerDefaultColor]] forState:UIControlStateNormal];
    [_answerButton setBackgroundImage:[UITools imageFromColor:[UITools colorFromHexa:answerDisabledColor]] forState:UIControlStateDisabled];
    
    _answerButton.layer.masksToBounds = YES;
    _answerButton.tintColor = [UIColor whiteColor];
    _answerButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _answerButton.layer.borderWidth = 2;
    
    _declineCancelOrHangupButton.layer.cornerRadius = _declineCancelOrHangupButton.frame.size.width / 2;
    _declineCancelOrHangupButton.layer.masksToBounds = YES;
    [_declineCancelOrHangupButton setBackgroundImage:[UITools imageFromColor:[UITools colorFromHexa:hangupDefaultColor]] forState:UIControlStateNormal];
    [_declineCancelOrHangupButton setBackgroundImage:[UITools imageFromColor:[UITools colorFromHexa:hangupDisabledColor]] forState:UIControlStateDisabled];
    _declineCancelOrHangupButton.tintColor = [UIColor whiteColor];
    _declineCancelOrHangupButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _declineCancelOrHangupButton.layer.borderWidth = 2;
    
    _muteButton.layer.cornerRadius = _muteButton.frame.size.width / 2;
    _muteButton.layer.masksToBounds = YES;
    _muteButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _muteButton.layer.borderWidth = 2;
    _muteButton.tintColor = [UIColor whiteColor];
    
    _speakerButton.layer.cornerRadius = _speakerButton.frame.size.width / 2;
    _speakerButton.layer.masksToBounds = YES;
    _speakerButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _speakerButton.layer.borderWidth = 2;
    _speakerButton.tintColor = [UIColor whiteColor];
    
    _cameraButton.layer.cornerRadius = _answerButton.frame.size.width / 2;
    _cameraButton.layer.masksToBounds = YES;
    _cameraButton.tintColor = [UIColor whiteColor];
    _cameraButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _cameraButton.layer.borderWidth = 2;
    
    _switchCamera.tintColor = [UIColor whiteColor];
    _switchCamera.hidden = YES;
    
    _avatarImageView.asCircle = YES;
    _avatarImageView.showPresence = NO;
    _avatarImageView.layer.shadowColor = [UIColor whiteColor].CGColor;
    _avatarImageView.layer.shadowOpacity = 0.8;
    _avatarImageView.layer.shadowRadius = 20;
    
    _switchCamera.tintColor = [UIColor whiteColor];

    _backgroundAvatarImageView.asCircle = NO;
    _backgroundAvatarImageView.showPresence = NO;
    _backgroundAvatarImageView.blackAndWhite = YES;
    
    // Prepare our blur effect
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        _blueMaskView.backgroundColor = [UITools colorFromHexa:0X0085CA80];
        self.blurView = [UIVisualEffectView new];
        [_blueMaskView insertSubview:self.blurView atIndex:0];
        self.blurView.frame = self.view.bounds;
    } else {
        _blueMaskView.backgroundColor = [UITools colorFromHexa:0X0085CA80];
    }
    
    // Custom font
    [UITools applyCustomBoldFontTo:_nameLabel];
    [UITools applyCustomFontTo:_statusLabel];
    
    // Prepare our black background, which is used when the user put
    // its phone to the ear
    _blackBackgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _blackBackgroundView.backgroundColor = [UIColor blackColor];
    
    _backToRainbowButton.badgeView.badgeColor = [UITools notificationBadgeColor];
    _backToRainbowButton.badgeView.outlineWidth = 0.0;
    _backToRainbowButton.badgeView.position = MGBadgePositionTopRight;
    _backToRainbowButton.badgeView.textColor = [UIColor whiteColor];
    _backToRainbowButton.badgeView.font = [UIFont fontWithName:[UITools defaultFontName] size:13];
    _backToRainbowButton.badgeView.verticalOffset = 7;
    _backToRainbowButton.badgeView.displayIfZero = NO;
    _backToRainbowButton.badgeView.bageStyle = MGBadgeStyleRoundRect;
    _backToRainbowButton.badgeView.padding = 5;
    
    _backToRainbowButton.tintColor = [UIColor whiteColor];
    
    _qualityIndicatorContainerView.hidden = YES;
    
    _localVideoView.layer.cornerRadius = _localVideoView.frame.size.width / 2;
    _localVideoView.layer.masksToBounds = YES;
    _localVideoView.layer.borderColor = [UIColor whiteColor].CGColor;
    _localVideoView.layer.borderWidth = 2;
    
    _remoteVideoView.delegate = self;
    _remoteVideoView.frame = CGRectZero;
    
    _localPreviewBubblesView.hidden = YES;
    
    _manuallySwitchedRemoteVideo = NO;
    
    _statusLabel.text = @"";
    
    _publishersButton.tintColor = [UIColor whiteColor];
    [self hidePublisherButton];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Activate the proximity sensor to shut-off the screen
    // if the user raise his phone to the ear
    UIDevice *device = [UIDevice currentDevice];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(proximityChanged:) name:UIDeviceProximityStateDidChangeNotification object:device];
    device.proximityMonitoringEnabled = YES;
    
    // Disable luminosity fade-out for video-calls
    if ([_call isKindOfClass:[RTCCall class]]){
        if([((RTCCall*)_call) isVideoEnabled])
            [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        
    }
    
    [self didUpdateMessageUnreadCount:nil];

    [_elapsedTimer invalidate];
    
    if (_call.status == CallStatusEstablished) {
        // Start our time-elapsed counter
        _elapsedTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateEverySecond:) userInfo:_call repeats:YES];
        [self updateEverySecond:_elapsedTimer];
    }
    if([_call isKindOfClass:[RTCCall class]]){
        if(((RTCCall*)_call).isVideoEnabled || ((RTCCall*)_call).isSharingEnabled)
            [self didAddRemoteVideoTrack:nil];
        if(((RTCCall*)_call).isLocalVideoEnabled)
            [self didAddLocalVideoTrack:nil];
    }
    [self layoutUIForCallStateAnimated:NO];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self layoutUIForCallStateAnimated:YES];
    // show automatically the first video stream if any
    [self automaticallyShowPublisher];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self layoutSubviews];
        [self rotateLocalVideoView];
        [self adjustLocalVideoViewConstraint];
        [self layoutUIForCallStateAnimated:YES];
    } completion:nil];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [_elapsedTimer invalidate];
    
    // re-enable the luminosity fade-out
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    
    // disable the proximity sensor
    UIDevice *device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = NO;
    
    // Remove the black view if its was here
    [_blackBackgroundView removeFromSuperview];
    [_remoteVideoTrack removeRenderer:_remoteVideoView];
    
    [_remoteVideoView removeFromSuperview];
    [_localVideoView removeFromSuperview];
    _remoteVideoView.delegate = nil;
    _remoteVideoView = nil;
    _localVideoView = nil;
    
    _remoteVideoTrack = nil;
    _localVideoTrack = nil;
    if(_hideUITimer){
        [_hideUITimer invalidate];
        _hideUITimer = nil;
    }
}

-(BOOL) shouldAutorotate {
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if([identifier isEqualToString:@"showPublisherView"]){
        if([_call.peer isKindOfClass:[Room class]])
            return YES;
    }
    return NO;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"showPublisherView"]){
        UIView *dimView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.4f;
        dimView.userInteractionEnabled = NO;
        [self.view addSubview:dimView];
        
        PublishersTableViewController *publisherViewController = [segue destinationViewController];
        publisherViewController.room = (Room*)_call.peer;
        publisherViewController.popoverPresentationController.delegate = self;
        publisherViewController.popoverPresentationController.sourceView = (UIView *)sender;
        publisherViewController.popoverPresentationController.sourceRect = ((UIView *)sender).bounds;
        publisherViewController.dimView = dimView;
    }
}

- (UIModalPresentationStyle) adaptivePresentationStyleForPresentationController: (UIPresentationController * ) controller {
    return UIModalPresentationNone;
}

-(void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    NSLog(@"Did dissmiss popover");
}

#pragma mark - UI actions

- (IBAction)answerAction:(UIButton *)sender {
    // Audio only today
    if([_call isKindOfClass:[RTCCall class]])
        [_rtcService acceptIncomingCall:(RTCCall*)_call withFeatures:RTCCallFeatureAudio];
}

- (IBAction)declineCancelOrHangupAction:(UIButton *)sender {
    if([_call.peer isKindOfClass:[Room class]]){
        Room *theRoom = (Room *)_call.peer;
        if(theRoom.isMyRoom){
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"End meeting", nil) message:NSLocalizedString(@"Do you want to end this meeting for all participants?", nil) preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"End meeting",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [self hangupAction:_call];
                [[ServicesManager sharedInstance].conferencesManagerService terminateConference:theRoom.conference completionHandler:^(NSError *error) {
                    if(!error || error.code == 404)
                        [[ServicesManager sharedInstance].roomsService detachConference:theRoom.conference fromRoom:theRoom completionBlock:nil];
                }];
            }];
            
            [alert addAction:defaultAction];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            
            [self hangupAction:_call];
            if (theRoom.conference.myConferenceParticipant)
                [[ServicesManager sharedInstance].conferencesManagerService disconnectParticipant:theRoom.conference.myConferenceParticipant inConference:theRoom.conference];
            else
                NSLog(@"No participant ");
        }
    } else {
        [self hangupAction:_call];
    }
}

-(void) hangupAction:(NSObject<CallProtocol> *) call {
    if([call isKindOfClass:[RTCCall class]]){
        if (_call.status == CallStatusRinging) {
            if (_call.isIncoming) {
                [_rtcService declineIncomingCall:((RTCCall*)_call)];
            } else {
                [_rtcService cancelOutgoingCall:((RTCCall*)_call)];
            }
        } else {
            [_rtcService hangupCall:((RTCCall*)_call)];
        }
    } else {
        [_telephonyService releaseCall:_call];
    }
    
    _qualityIndicatorContainerView.hidden = YES;
    _declineCancelOrHangupButton.enabled = NO;
    _backToRainbowButton.hidden = YES;
}

- (IBAction)muteUnmuteAction:(UIButton *)sender {
    if([_call isKindOfClass:[RTCCall class]] || [_call isKindOfClass:[Call class]]){
        if([_call.peer isKindOfClass:[Contact class]]){
            if (sender.selected) {
                [_rtcService unMuteLocalAudioForCall:((RTCCall*)_call)];
            } else {
                [_rtcService muteLocalAudioForCall:((RTCCall*)_call)];
            }
        } else {
            Room *room = (Room *) _call.peer;
            if (sender.selected) {
                [[ServicesManager sharedInstance].conferencesManagerService unmuteParticipant:room.conference.myConferenceParticipant inConference:room.conference];
            } else {
                [[ServicesManager sharedInstance].conferencesManagerService muteParticipant:room.conference.myConferenceParticipant inConference:room.conference];
            }
        }
    }
    sender.selected = !sender.selected;
    [self layoutUIForCallStateAnimated:YES];
}

- (IBAction)speakerButton:(UIButton *)sender {
    if (sender.selected) {
        [_rtcService unForceAudioOnSpeaker];
    } else {
        [_rtcService forceAudioOnSpeaker];
    }
    sender.selected = !sender.selected;
    [self layoutUIForCallStateAnimated:YES];
}

- (IBAction)cameraButton:(UIButton *)sender {
    if([_call isKindOfClass:[RTCCall class]]){
        if(sender.selected)
            [_rtcService removeVideoMediaFromCall:((RTCCall*)_call)];
        else
            [_rtcService addVideoMediaToCall:((RTCCall*)_call)];
        sender.selected = !sender.selected;
        [self layoutUIForCallStateAnimated:YES];
    }
}


#pragma mark - Notifications

-(void) updateCallState:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateCallState:notification];
        });
        return;
    }
    NSObject<CallProtocol> *call = notification.object;
    
    [_elapsedTimer invalidate];
    
    if (call.status == CallStatusEstablished) {
        // Start our time-elapsed counter
        _elapsedTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateEverySecond:) userInfo:call repeats:YES];
        //[FIRAnalytics logEventWithName:@"end_webRTC_Call" parameters:@{@"duration":_elapsedTimer}];
        NSLog(@"elapsedTimer : %@", _elapsedTimer);
        if (!call.connectionDate)
            NSLog(@"No connectionDate in %@", call);

        if([call isKindOfClass:[RTCCall class]]){
            if(((RTCCall*)call).isRemoteVideoEnabled && ((RTCCall*)call).isLocalVideoEnabled){
                [_hideUITimer invalidate];
                _hideUITimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(hideUI:) userInfo:nil repeats:YES];
            }
        }
    }
    
    [self layoutUIForCallStateAnimated:YES];
    if (call.status != CallStatusEstablished) {
        [_elapsedTimer invalidate];
        [_hideUITimer invalidate];
    }
    if (call.status == CallStatusCanceled || call.status == CallStatusHangup || call.status == CallStatusDeclined) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // Close the call screen
            if (![self isBeingDismissed]) {
                [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            }
        });
    }
}

-(void)didSelectPublisher:(NSNotification *) notification {
    _manuallySwitchedRemoteVideo = YES;
}

-(void) updateEverySecond:(NSTimer *) timer {
    RTCCall *call = (RTCCall *)timer.userInfo;
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:call.connectionDate];
    if (!call.connectionDate)
        NSLog(@"No connectionDate in %@", call);
    _statusLabel.text = [UITools timeFormatConvertToSeconds:[NSString stringWithFormat:@"%f", interval] zeroFormattingBehaviorunitsStyle:NSDateComponentsFormatterZeroFormattingBehaviorNone unitsStyle:NSDateComponentsFormatterUnitsStylePositional];
    _statusLabel.textColor = [self colorQualityIndicatorForLevel:_qualityLevel];
}


-(void) newStatsAvailable:(NSNotification *) notification {
    
    if ([notification.object objectForKey:@"values"]) {
        [self newQualityStatsAvailable:notification];
    } else {
        [self newCallStatsAvailable:notification];
    }
}


-(void) newCallStatsAvailable:(NSNotification *) notification {
    if(![_call isKindOfClass:[RTCCall class]])
        return;
    //NSDictionary* googCandidatePair = [notification.object objectForKey:@"googCandidatePair"];
    NSDictionary* localcandidate = [notification.object objectForKey:@"localcandidate"];
    NSDictionary* remotecandidate = [notification.object objectForKey:@"remotecandidate"];
    NSDictionary* ssrcAudio = [notification.object objectForKey:@"ssrcAudio"];
    NSDictionary* ssrcVideo = [notification.object objectForKey:@"ssrcVideo"];

    NSString* codecAudio = [ssrcAudio objectForKey:@"googCodecName"];
    NSString* codecVideo = [ssrcVideo objectForKey:@"googCodecName"];
    NSString* videoOrSharing = @"-";
    
    NSString* iceDistribution = @"-|-";
    NSString* iceTopology = @"-|-";
    
    if(((RTCCall*)_call).isVideoEnabled) videoOrSharing = @"video";
    if(((RTCCall*)_call).isSharingEnabled) videoOrSharing = @"sharing";
    
    NSString* callsDistribution = [NSString stringWithFormat:@"audio|%@",videoOrSharing];

    if (!codecAudio) codecAudio=@"-";
    if (!codecVideo) codecVideo=@"-";
    
    NSString* codecsDistribution = [NSString stringWithFormat:@"%@|%@",codecAudio,codecVideo];
    
    if (localcandidate) {
    
    iceDistribution = [NSString stringWithFormat:@"%@|%@",[self getCandidateType:[localcandidate objectForKey:@"candidateType"]],[self getCandidateType:[remotecandidate objectForKey:@"candidateType"]]];
    
    iceTopology = [NSString stringWithFormat:@"%@|%@",[localcandidate objectForKey:@"networkType"],[localcandidate objectForKey:@"transport"]];
    }
    
    NSString *callsDirection = [NSString stringWithFormat:@"%@ %@ call", _call.isIncoming?@"incoming":@"outgoing", ((RTCCall*)_call).isVideoEnabled?@"video":@"audio"];
    
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"webrtc" action:@"ICEDistribution" label:iceDistribution value:nil];
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"webrtc" action:@"ICETopology" label:iceTopology value:nil];
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"webrtc" action:@"CodecsDistribution" label:codecsDistribution value:nil];

    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"webrtc" action:@"CallsDirection" label:callsDirection value:nil];
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"webrtc" action:@"CallsDistribution" label:callsDistribution value:nil];
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"webrtc" action:@"CallsDurationRange" label:[self getCallDuration] value:nil];
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"webrtc" action:@"CallsQualityIndicator" label:[self getQualityIndicator] value:nil];
    
    NSLog(@"WebRTC stats to GoogleAnalytics :%@:%@:%@:%@:%@:%@:%@",iceDistribution,iceTopology,codecsDistribution,callsDistribution,
          callsDirection,[self getCallDuration],[self getQualityIndicator]);
    //WebRTC stats to GoogleAnalytics :relay|relay:wwan|udp:opus|H264:audio|video:outgoing video call:short_less_60s:high|high
    
}

- (NSString*) getCandidateType : (NSString*)type {
    
    if ([type isEqualToString:@"relayed"])
         return @"relay";
    else if ([type isEqualToString:@"serverreflexive"])
            return @"stun";
    else if ([type isEqualToString:@"peerreflexive"])
              return @"stun";
    else return type;
    
}

- (NSString*) getQualityIndicator {
    
    NSString* audioQuality = @"";
    NSString* videoQuality = @"";
    
    switch (_audioQualityLevel) {
        case RTCQualityIndicatorLow:
            audioQuality = @"low";
            break;
        case RTCQualityIndicatorMedium:
            audioQuality = @"medium";
            break;
        case RTCQualityIndicatorHigh:
            audioQuality = @"high";
            break;
        case RTCQualityIndicatorNone:
        default:
            audioQuality = @"-";
    }
    switch (_videoQualityLevel) {
        case RTCQualityIndicatorLow:
            videoQuality = @"low";
            break;
        case RTCQualityIndicatorMedium:
            videoQuality = @"medium";
            break;
        case RTCQualityIndicatorHigh:
            videoQuality = @"high";
            break;
        case RTCQualityIndicatorNone:
        default:
            videoQuality = @"-";
    }
     return [NSString stringWithFormat:@"%@|%@",audioQuality,videoQuality];

}


- (NSString*) getCallDuration {
    
    NSTimeInterval durationInSeconds = [[NSDate date] timeIntervalSinceDate:_call.connectionDate];
    
    if (durationInSeconds < 15) {
        return @"very_short_less_15s";
    }
    else if (durationInSeconds < 30) {
        return @"very_short_less_30s";
    }
    else if (durationInSeconds < 60) {
        return @"short_less_60s";
    }
    else if (durationInSeconds < 120) {
        return @"short_less_120s";
    }
    else if (durationInSeconds < 300) {
        return @"short_less_300s";
    }
    else if (durationInSeconds < 600) {
        return @"normal_less_600s";
    }
    else if (durationInSeconds < 1200) {
        return @"normal_less_1200s";
    }
    else if (durationInSeconds < 1800) {
        return @"normal_less_1800s";
    }
    else if (durationInSeconds < 3600) {
        return @"long_less_3600s";
    }
    else {
        return @"long_more_3600s";
    }
}

-(void) newQualityStatsAvailable:(NSNotification *) notification {
    
    NSDictionary* notificationValues = [notification.object objectForKey:@"values"];

    int newAudioPacketsSent = 0;
    int newAudioPacketsLostOnSent = 0;
    int newAudioPacketsRecv = 0;
    int newAudioPacketsLostOnRecv = 0;
    
    int newVideoPacketsSent = 0;
    int newVideoPacketsLostOnSent = 0;
    int newVideoPacketsRecv = 0;
    int newVideoPacketsLostOnRecv = 0;

    
    if ([[notificationValues objectForKey:@"mediaType"] isEqualToString:@"audio"]) {
        if ([notificationValues objectForKey:@"packetsSent"]) {
            newAudioPacketsSent = [[notificationValues objectForKey:@"packetsSent"] intValue];
            newAudioPacketsLostOnSent = [[notificationValues objectForKey:@"packetsLost"] intValue];
            _audioRttOnSent = [[notificationValues objectForKey:@"googRtt"] intValue];
            if (_audioRttOnSent ==0) _audioRttOnSent =1; //sometimes googRtt is missing ...
            if( newAudioPacketsSent > 0 && newAudioPacketsSent != _audioPacketsSent) {
                _audioPacketsLostOnSentInPercent = 100 * (newAudioPacketsLostOnSent - _audioPacketsLostOnSent)/(newAudioPacketsSent - _audioPacketsSent);
            } else {
                _audioPacketsLostOnSentInPercent = 0;
            }
            _audioPacketsSent = newAudioPacketsSent;
            _audioPacketsLostOnSent = newAudioPacketsLostOnSent;
            NSLog(@"Audio sent : RTT= %d Sent= %d Lost= %d In percent= %d",_audioRttOnSent,_audioPacketsSent,_audioPacketsLostOnSent,_audioPacketsLostOnSentInPercent);
        } else {
            newAudioPacketsRecv = [[notificationValues objectForKey:@"packetsReceived"] intValue];
            newAudioPacketsLostOnRecv = [[notificationValues objectForKey:@"packetsLost"] intValue];
            _audioJitterOnRecv = [[notificationValues objectForKey:@"googJitterBufferMs"] intValue];
            
            _audioPacketsLostOnRecvInPercent = 100 * (newAudioPacketsLostOnRecv - _audioPacketsLostOnRecv)/(newAudioPacketsRecv - _audioPacketsRecv);
            _audioPacketsRecv = newAudioPacketsRecv;
            _audioPacketsLostOnRecv = newAudioPacketsLostOnRecv;
            NSLog(@"Audio recv : JIT= %d Recv= %d Lost= %d In percent= %d",_audioJitterOnRecv,_audioPacketsRecv,_audioPacketsLostOnRecv,_audioPacketsLostOnRecvInPercent);
        }
    } else {
        // mediaType = video
        if ([notificationValues objectForKey:@"packetsSent"]) {
            newVideoPacketsSent = [[notificationValues objectForKey:@"packetsSent"] intValue];
            newVideoPacketsLostOnSent = [[notificationValues objectForKey:@"packetsLost"] intValue];
            _videoRttOnSent = [[notificationValues objectForKey:@"googRtt"] intValue];
            if(newVideoPacketsSent != 0 && _videoPacketsSent != 0 && (newVideoPacketsSent - _videoPacketsSent) != 0)
                _videoPacketsLostOnSentInPercent = 100 * (newVideoPacketsLostOnSent - _videoPacketsLostOnSent)/(newVideoPacketsSent - _videoPacketsSent);
            _videoPacketsSent = newVideoPacketsSent;
            _videoPacketsLostOnSent = newVideoPacketsLostOnSent;
            NSLog(@"Video sent : RTT= %d Sent= %d Lost= %d In percent= %d",_videoRttOnSent,_videoPacketsSent,_videoPacketsLostOnSent,_videoPacketsLostOnSentInPercent);
        } else {
            newVideoPacketsRecv = [[notificationValues objectForKey:@"packetsReceived"] intValue];
            newVideoPacketsLostOnRecv = [[notificationValues objectForKey:@"packetsLost"] intValue];
            _videoJitterOnRecv = [[notificationValues objectForKey:@"googJitterBufferMs"] intValue];
            
            _videoPacketsLostOnRecvInPercent = 100 * (newVideoPacketsLostOnRecv - _videoPacketsLostOnRecv)/(newVideoPacketsRecv - _videoPacketsRecv);
            _videoPacketsRecv = newVideoPacketsRecv;
            _videoPacketsLostOnRecv = newVideoPacketsLostOnRecv;
            NSLog(@"Video recv : JIT= %d Recv= %d Lost= %d In percent= %d",_videoJitterOnRecv,_videoPacketsRecv,_videoPacketsLostOnRecv,_videoPacketsLostOnRecvInPercent);
        }

        
        
    }
    
    
    if ((_audioRttOnSent != 0) && (_audioJitterOnRecv !=0)) {
        
        if (self.checkAudioHighQuality) {
            _audioQualityLevel = RTCQualityIndicatorHigh;
            NSLog(@"Audio quality = High");
        } else if (self.checkAudioMediumQuality) {
            _audioQualityLevel = RTCQualityIndicatorMedium;
            NSLog(@"Audio quality = Medium");
        } else {
            _audioQualityLevel = RTCQualityIndicatorLow;
            NSLog(@"Audio quality = Low");
        }
    }
    
    if ((_videoRttOnSent != 0) && (_videoJitterOnRecv !=0)) {
        
        if (self.checkVideoHighQuality) {
            _videoQualityLevel = RTCQualityIndicatorHigh;
            NSLog(@"Video quality = High");
        } else if (self.checkVideoMediumQuality) {
            _videoQualityLevel = RTCQualityIndicatorMedium;
            NSLog(@"Video quality = Medium");
        } else {
            _videoQualityLevel = RTCQualityIndicatorLow;
            NSLog(@"Video quality = Low");
        }
    }
    
    if  (_videoQualityLevel != RTCQualityIndicatorNone)
            _qualityLevel = (_audioQualityLevel + _videoQualityLevel)/2;
    else    _qualityLevel = _audioQualityLevel;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self drawQualityIndicatorForLevel:_qualityLevel];
    });
    
}

-(BOOL) checkAudioHighQuality {
    if ((_audioRttOnSent <= [_highQualitySpec[audioRttOnSent][0] intValue]) &&
        (_audioJitterOnRecv <= [_highQualitySpec[audioJitterOnRecv][0] intValue]) &&
        (_audioPacketsLostOnSentInPercent <= [_highQualitySpec[audioPacketsLostOnSentInPercent][0] intValue]) &&
        (_audioPacketsLostOnRecvInPercent <= [_highQualitySpec[audioPacketsLostOnRecvInPercent][0] intValue])) {
         return YES;
    } else {
         return NO;
    }
}

-(BOOL) checkAudioMediumQuality {
    if ((_audioRttOnSent <= [_mediumQualitySpec[audioRttOnSent][0]intValue]) &&
        (_audioJitterOnRecv <= [_mediumQualitySpec[audioJitterOnRecv][0] intValue]) &&
        (_audioPacketsLostOnSentInPercent <= [_mediumQualitySpec[audioPacketsLostOnSentInPercent][0] intValue]) &&
        (_audioPacketsLostOnRecvInPercent <= [_mediumQualitySpec[audioPacketsLostOnRecvInPercent][0] intValue])){
        return YES;
    } else {
        return NO;
    }
}

-(BOOL) checkVideoHighQuality {
    for (int i=0; i < 3; i++){
        if ((_videoRttOnSent <= [_highQualitySpec[videoRttOnSent][i] intValue]) &&
            (_videoJitterOnRecv <= [_highQualitySpec[videoJitterOnRecv][i] intValue]) &&
            (_videoPacketsLostOnSentInPercent <= [_highQualitySpec[videoPacketsLostOnSentInPercent][i] intValue]) &&
            (_videoPacketsLostOnRecvInPercent <= [_highQualitySpec[videoPacketsLostOnRecvInPercent][i] intValue])) {
          return YES;
        }
    }
    return NO;
}

-(BOOL) checkVideoMediumQuality {
    for (int i=0; i < 5; i++){
        if ((_videoRttOnSent <= [_mediumQualitySpec[videoRttOnSent][i] intValue]) &&
            (_videoJitterOnRecv <= [_mediumQualitySpec[videoJitterOnRecv][i] intValue]) &&
            (_videoPacketsLostOnSentInPercent <= [_mediumQualitySpec[videoPacketsLostOnSentInPercent][i] intValue]) &&
            (_videoPacketsLostOnRecvInPercent <= [_mediumQualitySpec[videoPacketsLostOnRecvInPercent][i] intValue])) {
          return YES;
        }
    }
    return NO;
}

-(void) drawQualityIndicatorForLevel:(RTCQualityIndicator) level {
    if(level == RTCQualityIndicatorNone)
        _qualityIndicatorContainerView.hidden = YES;
    else
//        _qualityIndicatorContainerView.hidden = NO;
    switch (level) {
        case RTCQualityIndicatorLow:{
            _qualityIndicatoryLowView.backgroundColor = [UIColor redColor];
            _qualityIndicatoryMediumView.backgroundColor = [UIColor lightGrayColor];
            _qualityIndicatorHighView.backgroundColor = [UIColor lightGrayColor];
            break;
        }
        case RTCQualityIndicatorMedium:{
            _qualityIndicatoryLowView.backgroundColor = [UIColor orangeColor];
            _qualityIndicatoryMediumView.backgroundColor = [UIColor orangeColor];
            _qualityIndicatorHighView.backgroundColor = [UIColor lightGrayColor];
            break;
        }
        case RTCQualityIndicatorHigh:{
            _qualityIndicatoryLowView.backgroundColor = [UIColor greenColor];
            _qualityIndicatoryMediumView.backgroundColor = [UIColor greenColor];
            _qualityIndicatorHighView.backgroundColor = [UIColor greenColor];
            break;
        }
        case RTCQualityIndicatorNone:{
            _qualityIndicatoryLowView.backgroundColor = [UIColor lightGrayColor];
            _qualityIndicatoryMediumView.backgroundColor = [UIColor lightGrayColor];
            _qualityIndicatorHighView.backgroundColor = [UIColor lightGrayColor];
            break;
        }
    }
}

-(UIColor *) colorQualityIndicatorForLevel:(RTCQualityIndicator) level {

    switch (level) {
        case RTCQualityIndicatorLow:
            return [UIColor redColor];
        case RTCQualityIndicatorMedium:
            return [UIColor orangeColor];
        
        case RTCQualityIndicatorHigh:
        case RTCQualityIndicatorNone:
        default:
            return [UIColor whiteColor];
    }
}

#pragma mark - CaptureSession Settings
-(void)startCapturer {
    AVCaptureDevicePosition position = _isUsingBackCamera ?  AVCaptureDevicePositionBack : AVCaptureDevicePositionFront;
    AVCaptureDevice *device = [self findDeviceForPosition:position];
    AVCaptureDeviceFormat *format = [self selectFormatForDevice:device];
    
    if (format == nil) {
        RTCLogError(@"No valid formats for device %@", device);
        NSAssert(NO, @"");
        
        return;
    }
    
    NSInteger fps = [self selectFpsForFormat:format];
    
    [_cameraVideoCapturer startCaptureWithDevice:device format:format fps:fps];
}

- (AVCaptureDevice *)findDeviceForPosition:(AVCaptureDevicePosition)position {
    NSArray<AVCaptureDevice *> *captureDevices = [RTCCameraVideoCapturer captureDevices];
    for (AVCaptureDevice *device in captureDevices) {
        if (device.position == position) {
            return device;
        }
    }
    return captureDevices[0];
}

- (NSInteger)selectFpsForFormat:(AVCaptureDeviceFormat *)format {
    Float64 maxFramerate = 0;
    for (AVFrameRateRange *fpsRange in format.videoSupportedFrameRateRanges) {
        maxFramerate = fmax(maxFramerate, fpsRange.maxFrameRate);
    }
    return maxFramerate;
}

- (AVCaptureDeviceFormat *)selectFormatForDevice:(AVCaptureDevice *)device {
    NSArray<AVCaptureDeviceFormat *> *formats =
    [RTCCameraVideoCapturer supportedFormatsForDevice:device];
    int targetWidth = _remoteVideoViewWidthConstraint.constant;
    int targetHeight = _remoteVideoViewHeightConstraint.constant;
    AVCaptureDeviceFormat *selectedFormat = nil;
    int currentDiff = INT_MAX;
    
    for (AVCaptureDeviceFormat *format in formats) {
        CMVideoDimensions dimension = CMVideoFormatDescriptionGetDimensions(format.formatDescription);
        FourCharCode pixelFormat = CMFormatDescriptionGetMediaSubType(format.formatDescription);
        int diff = abs(targetWidth - dimension.width) + abs(targetHeight - dimension.height);
        if (diff < currentDiff) {
            selectedFormat = format;
            currentDiff = diff;
        } else if (diff == currentDiff && pixelFormat == [_cameraVideoCapturer preferredOutputPixelFormat]) {
            selectedFormat = format;
        }
    }
    
    return selectedFormat;
}

#pragma mark - UI layout and animations
-(void) adjustLocalVideoViewConstraint {
    NSInteger value = 0;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(_topView.hidden)
        value = -_topView.frame.size.height + ((orientation == UIInterfaceOrientationPortrait)?50:20);
    else
        value = 10;
    
    _localVideoTopConstraint.constant = value;
}

-(void) layoutUIForCallStateAnimated:(BOOL) animated {
    NSAssert([NSThread isMainThread], @"Should be on mainthread");
    // Check mute state and speaker state
    if([_call isKindOfClass:[RTCCall class]]){
        _cameraButton.selected = [((RTCCall*)_call) isLocalVideoEnabled];
        _speakerButton.selected = [_rtcService isSpeakerEnabled];
        if([_call.peer isKindOfClass:[Contact class]] || ((RTCCall*)_call).isMediaPillarCall){
            _muteButton.selected = [_rtcService isCallMuted:((RTCCall*)_call)];
        } else {
            Room *room = (Room *)_call.peer;
            _muteButton.selected = room.conference.myConferenceParticipant.muted;
        }
    }
    
    _avatarImageView.peer = _call.peer;
    _backgroundAvatarImageView.peer = _call.peer;
    
    _nameLabel.text = _call.peer.displayName;
    
    // Call Settings buttons are :
    // just circles with border but no background IF NOT ACTIVE
    // round buttons with background IF ACTIVE
    if (_muteButton.selected) {
        _muteButton.backgroundColor = [UIColor whiteColor];
        _muteButton.tintColor = [UIColor blackColor];
    } else {
        _muteButton.backgroundColor = [UIColor clearColor];
        _muteButton.tintColor = [UIColor whiteColor];
    }
    if (_speakerButton.selected) {
        _speakerButton.backgroundColor = [UIColor whiteColor];
        _speakerButton.tintColor = [UIColor blackColor];
    } else {
        _speakerButton.backgroundColor = [UIColor clearColor];
        _speakerButton.tintColor = [UIColor whiteColor];
    }
    
    if(_cameraButton.selected){
        _cameraButton.backgroundColor = [UIColor whiteColor];
        _cameraButton.tintColor = [UIColor blackColor];
    } else {
        _cameraButton.backgroundColor = [UIColor clearColor];
        _cameraButton.tintColor = [UIColor whiteColor];
    }

    // Disable camera button by defaut, enabled it only when the call is in established state and the user have the good right
    _cameraButton.enabled = NO;
    
    _muteButton.hidden = !(_call.capabilities & CallCapabilitiesMute);
    _speakerButton.hidden = !(_call.capabilities & CallCapabilitiesSpeaker);
    _declineCancelOrHangupView.hidden = !(_call.capabilities & CallCapabilitiesHangup);
    _answerView.hidden = !(_call.capabilities & CallCapabilitiesAnswer);
    _callSettingsView.hidden = NO;
    _backToRainbowButton.hidden = NO;
    _cameraButton.hidden = !(_call.capabilities & CallCapabilitiesVideo);
    _avatarImageView.hidden = NO;
    _callRecordingView.hidden = YES;
    _callRecordingView.tintColor = [UIColor redColor];
    
    switch (_call.status) {
        case CallStatusRinging: {
            _statusLabel.text = NSLocalizedString(@"Ringing", nil);
            _qualityIndicatorContainerView.hidden = YES;
            if (_call.isIncoming){
                _statusLabel.text = NSLocalizedString(@"Incoming call", nil);
                if(_call.capabilities & CallCapabilitiesAnswer)
                    [self showGreenAndRedButtonsAnimated:animated];
                else
                    [self showOnlyRedButtonAnimated:animated];
            } else
                [self showOnlyRedButtonAnimated:animated];
            
            break;
        }
        case CallStatusConnecting: {
            _statusLabel.text = NSLocalizedString(@"Connecting", nil);
            _answerButton.enabled = NO;
            _declineCancelOrHangupButton.enabled = YES;
            _answerView.hidden = YES;
            [self showOnlyRedButtonAnimated:animated];
            break;
        }
        case CallStatusDeclined: {
            _statusLabel.text = NSLocalizedString(@"Declined", nil);
            _answerButton.enabled = NO;
            _declineCancelOrHangupButton.enabled = NO;
            _qualityIndicatorContainerView.hidden = YES;
            break;
        }
        case CallStatusTimeout: {
            _statusLabel.text = NSLocalizedString(@"Timeout", nil);
            _answerButton.enabled = NO;
            _declineCancelOrHangupButton.enabled = NO;
            _qualityIndicatorContainerView.hidden = YES;
            break;
        }
        case CallStatusCanceled: {
            _statusLabel.text = NSLocalizedString(@"Canceled", nil);
            _answerButton.enabled = NO;
            _declineCancelOrHangupButton.enabled = NO;
            _qualityIndicatorContainerView.hidden = YES;
            break;
        }
        case CallStatusEstablished: {
            // Status label is used for time-elapsed here
            _answerButton.enabled = NO;
            _declineCancelOrHangupButton.enabled = YES;
            if(_qualityLevel == RTCQualityIndicatorNone) {
                _qualityIndicatorContainerView.hidden = YES;
            }
            else {
//                _qualityIndicatorContainerView.hidden = NO;
            }
            
            if([_call.peer isKindOfClass:[Room class]])
                _cameraButton.enabled = YES;
            else if([_call isKindOfClass:[RTCCall class]])
                _cameraButton.enabled = !((RTCCall*)_call).isSharingEnabled;
            
            [self showOnlyRedButtonAnimated:animated];
            if([_call isKindOfClass:[RTCCall class]]){
                if(((RTCCall*)_call).isRemoteVideoEnabled || ((RTCCall*)_call).isSharingEnabled){
                    [self hideElementForVideoMode];
                } else if(((RTCCall*)_call).isLocalVideoEnabled){
                    [self hideElementForVideoMode];
                    if(!((RTCCall*)_call).isRemoteVideoEnabled && !((RTCCall*)_call).isSharingEnabled){
                        [self showElementForAudioCall];
                    }
                } else {
                    [self showElementForAudioCall];
                }
                _callRecordingView.hidden = !((RTCCall*)_call).isRecording;
            } else {
                [self showElementForAudioCall];
            }
            
            [self showHidePublisherButton];
            
            break;
        }
        case CallStatusHangup: {
            [self showElementForAudioCall];
            [self deactivateProximitySensor];
            [self hidePublisherButton];
            _statusLabel.text = NSLocalizedString(@"Call ended", nil);
            _answerButton.enabled = NO;
            _declineCancelOrHangupButton.enabled = NO;
            _muteButton.hidden = YES;
            _speakerButton.hidden = YES;
            _cameraButton.hidden = YES;
            _declineCancelOrHangupView.hidden = YES;
            _backToRainbowButton.hidden = YES;
            _qualityIndicatorContainerView.hidden = YES;
            _localVideoView.hidden = YES;
            _remoteVideoView.hidden = YES;
            _switchCamera.hidden = YES;
            _topViewGradientView.hidden = YES;
            _actionButtonViewGradientView.hidden = YES;
            
            break;
        }
    }
    
    if([_call isKindOfClass:[RTCCall class]]){
        _topViewGradientView.hidden = ![((RTCCall*)_call) isVideoEnabled];
        _actionButtonViewGradientView.hidden = ![((RTCCall*)_call) isVideoEnabled];
    }
    
    if(!_cameraButton.enabled){
        _cameraButton.layer.borderColor = [UITools colorFromHexa:0XFFFFFF80].CGColor;
    } else {
        _cameraButton.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    [self.view layoutIfNeeded];
}

-(void) alertRecordingStateChanged:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alertRecordingStateChanged:notification];
        });
        return;
    }
    [self updateCallState:notification];
}

-(void) showGreenAndRedButtonsAnimated:(BOOL) animated {
    // Show the 2 buttons.
    // The only use-case here is
    // answer(green) + decline(red)
    // But : buttons can be misplaced, and green button can be hidden.
    
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGFloat desiredRedButtonConstraintConstant = screen.size.width * kAnswerDeclinePositionRatio;
    CGFloat desiredGreenButtonConstraintConstant = -(screen.size.width * kAnswerDeclinePositionRatio);
    
    void(^block)() = ^() {
        _answerView.alpha = 1.0f;
        _answerHorizontalAlignementConstraint.constant = desiredGreenButtonConstraintConstant;
        _declineCancelOrHangupView.alpha = 1.0f;
        _answerView.alpha = 1.0f;
        _declineCancelOrHangupHorizontalAlignementConstraint.constant = desiredRedButtonConstraintConstant;
        [self.view layoutIfNeeded];
    };
    
    if (animated) {
        [UIView animateWithDuration:kAnimationDuration
                              delay:0
                            options: UIViewAnimationOptionCurveLinear
                         animations:block
                         completion:nil];
    } else {
        block();
    }
}

-(void) showOnlyRedButtonAnimated:(BOOL) animated {
    // Show only the red button.
    // The use-case here are
    // cancel
    // hangup
    
    CGRect screen = [[UIScreen mainScreen] bounds];
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGFloat desiredRedButtonConstraintConstant = (orientation == UIInterfaceOrientationPortrait)?0:_declineCancelOrHangupHorizontalAlignementConstraint.constant;
    CGFloat desiredGreenButtonConstraintConstant = -(screen.size.width * kAnswerDeclinePositionRatio);
    
    void(^block)() = ^() {
        _answerView.alpha = 0.0f;
        _answerHorizontalAlignementConstraint.constant = desiredGreenButtonConstraintConstant;
        _declineCancelOrHangupView.alpha = 1.0f;
        _declineCancelOrHangupHorizontalAlignementConstraint.constant = desiredRedButtonConstraintConstant;
        [self.view layoutIfNeeded];
    };
    
    if (animated) {
        [UIView animateWithDuration:kAnimationDuration delay:0 options: UIViewAnimationOptionCurveLinear animations:block completion:nil];
    } else {
        block();
    }
}


#pragma mark - Proximity handler
-(void) activateProximitySensor {
    UIDevice *device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = YES;
    [UIScreen mainScreen].wantsSoftwareDimming = YES;
    [self proximityChanged:nil];
}

-(void) deactivateProximitySensor {
    UIDevice *device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = NO;
    [UIScreen mainScreen].wantsSoftwareDimming = NO;
    [self proximityChanged:nil];
}

- (void)proximityChanged:(NSNotification *)notification {
    UIDevice *device = [notification object];
    NSLog(@"In proximity: %i", device.proximityState);

    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self proximityChanged:notification];
        });
        return;
    }
    if (device.proximityState == YES && [_call isKindOfClass:[RTCCall class]] && !((RTCCall*)_call).isVideoEnabled) {
        UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
        [window addSubview:_blackBackgroundView];
    } else {
        [_blackBackgroundView removeFromSuperview];
    }
}


#pragma mark - UIViewControllerTransitioningDelegate UIViewControllerAnimatedTransitioning

// Blur effect
// http://stackoverflow.com/questions/39671408/uivisualeffectview-in-ios-10
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source {
    return self;
}

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.3;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIView *container = [transitionContext containerView];
    
    [container addSubview:self.view];
    
    self.blurView.effect = nil;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.blurView.effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:finished];
    }];
}

-(IBAction)hideCallView:(id)sender {
    if ([self isViewLoaded] && self.view.superview != nil) {
        [UIView beginAnimations:@"hideCallView" context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        self.view.transform = CGAffineTransformMakeScale(0.0, 0.0);
        self.view.alpha = 0.2;
        [UIView commitAnimations];
    }
}

-(void)animationDidStop:(NSString *)animacallsListControllernID finished:(NSNumber *)finished context:(void *)context {
    if ([animacallsListControllernID isEqualToString:@"hideCallView"]) {
        if (![self isBeingDismissed]) {
            [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}
- (IBAction)backToRainbow:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hideCallViewFromView" object:nil];
}

-(void) didUpdateMessageUnreadCount:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateMessageUnreadCount:notification];
        });
        return;
    }
    NSInteger unreadMessagesCount = [ServicesManager sharedInstance].conversationsManagerService.totalNbOfUnreadMessagesInAllConversations;
    if(unreadMessagesCount > 10) {
        _backToRainbowButton.badgeView.bageStyle = MGBadgeStyleRoundRect;
        _backToRainbowButton.badgeView.padding = 5;
    } else {
        _backToRainbowButton.badgeView.bageStyle = MGBadgeStyleEllipse;
        _backToRainbowButton.badgeView.padding = 0;
    }
    _backToRainbowButton.badgeView.badgeValue = unreadMessagesCount;
}

#pragma mark - video
-(void) hideElementForVideoMode {
    NSAssert([NSThread isMainThread], @"Should be on mainthread");
    if(((RTCCall*)_call).isRemoteVideoEnabled || ((RTCCall*)_call).isSharingEnabled){
        _bubblesView.hidden = YES;
        _avatarImageView.hidden = YES;
        _switchCamera.hidden = YES;
    }
    if(((RTCCall*)_call).isLocalVideoEnabled){
        _localPreviewBubblesView.hidden = NO;
        _switchCamera.hidden = NO;
    }
    if ([((RTCCall*)_call) isVideoEnabled] || ((RTCCall*)_call).isSharingEnabled){
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        [self deactivateProximitySensor];
    }
    
}

-(void) showElementForAudioCall {
//    if(_rtcCall.isVideoEnabled || _rtcCall.isSharingEnabled)
//        return;
    // Remote video must be hidden
    
    [self hideTrackInMainUIPart];
    [self hideTrackInSecondUIPart];
    
    _bubblesView.hidden = NO;
    _avatarImageView.hidden = NO;
    if([_call isKindOfClass:[RTCCall class]]){
        if(!((RTCCall*)_call).isLocalVideoEnabled){
            _localPreviewBubblesView.hidden = YES;
            _switchCamera.hidden = YES;
        }
    }
    _topView.hidden = NO;
    _actionButtonView.hidden = NO;
    [self adjustLocalVideoViewConstraint];
    [_hideUITimer invalidate];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    [self activateProximitySensor];
}

- (IBAction)didTapRemoteVideoView:(UITapGestureRecognizer *)sender {
    if(!_remoteVideoView.hidden){
        _actionButtonView.hidden = !_actionButtonView.hidden;
        _topView.hidden = !_topView.hidden;
        if(((RTCCall*)_call).isLocalVideoEnabled){
            if(!_topView.hidden){
                _localVideoView.hidden = NO;
                _localPreviewBubblesView.hidden = NO;
                _switchCamera.hidden = NO;
            }
            [self adjustLocalVideoViewConstraint];
        }
        
        [_hideUITimer invalidate];
        _hideUITimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(hideUI:) userInfo:nil repeats:YES];
    }
}

- (IBAction)didTapLocalVideoView:(UITapGestureRecognizer *)sender {
    if(((RTCCall*)_call).isRemoteVideoEnabled){
        _localVideoView.hidden = YES;
        _localPreviewBubblesView.hidden = YES;
        _switchCamera.hidden = YES;
    }
}

#pragma mark - Local video

-(void) rotateLocalVideoView {
    AVCaptureConnection *previewLayerConnection=_localVideoView.previewLayer.connection;
    if ([previewLayerConnection isVideoOrientationSupported]){
        AVCaptureVideoOrientation videoOrientation;
        switch ([[UIApplication sharedApplication] statusBarOrientation]) {
            default:
            case UIInterfaceOrientationPortrait:
                videoOrientation = AVCaptureVideoOrientationPortrait;
                break;
            case UIInterfaceOrientationPortraitUpsideDown:
                videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
                break;
            case UIInterfaceOrientationLandscapeLeft:
                videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
                break;
            case UIInterfaceOrientationLandscapeRight:
                videoOrientation = AVCaptureVideoOrientationLandscapeRight;
                break;
        }
        
        [previewLayerConnection setVideoOrientation:videoOrientation];
    }
}

-(void)didAddCaptureSession:(NSNotification *) notification {
    self.cameraVideoCapturer = (RTCCameraVideoCapturer *) notification.object;
    [self startCapturer];
}

-(void)didAddLocalVideoTrack:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddLocalVideoTrack:notification];
        });
        return;
    }
    
    RTCVideoTrack *localVideoTrack = (RTCVideoTrack *) notification.object;
    if(!localVideoTrack){
        RTCMediaStream *localStream = [_rtcService localVideoStreamForCall:((RTCCall*)_call)];
        localVideoTrack = localStream.videoTracks[0];
    }
    if(_localVideoTrack == localVideoTrack)
        return;
    
    _localVideoTrack = nil;
    _localVideoTrack = localVideoTrack;
    
    RTCVideoSource *source = nil;
    if ([localVideoTrack.source isKindOfClass:[RTCVideoSource class]]) {
        source = (RTCVideoSource*)localVideoTrack.source;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStartCaptureSession:) name:AVCaptureSessionDidStartRunningNotification object:nil];
    _localVideoView.captureSession = self.cameraVideoCapturer.captureSession;
    _localVideoView.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self hideElementForVideoMode];
    _localVideoView.hidden = NO;
}

-(void) didRemoveLocalVideoTrack:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveLocalVideoTrack:notification];
        });
        return;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureSessionDidStartRunningNotification object:nil];
    _localVideoTrack = nil;
    _localVideoView.captureSession = nil;
    _localVideoView.hidden = YES;
    _localPreviewBubblesView.hidden = YES;
}

-(void) didStartCaptureSession:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self didStartCaptureSession:notification];
        });
        return;
    }
        
    [self rotateLocalVideoView];
}

#pragma mark - Remote Video
-(void) didAddRemoteVideoTrack:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddRemoteVideoTrack:notification];
        });
        return;
    }
    
    [self hideTrackInMainUIPart];
    [self hideTrackInSecondUIPart];
    
    if(((RTCCall*)_call).isSharingEnabled && ![((RTCCall*)_call) isRemoteVideoEnabled]){
        NSLog(@"the track is a sharing, display it in the center of the screen");
        RTCMediaStream *sharingStream = [_rtcService remoteSharingStreamForCall:((RTCCall*)_call)];
        if(sharingStream && sharingStream.videoTracks && [sharingStream.videoTracks count] > 0) {
            RTCVideoTrack *track = [sharingStream.videoTracks objectAtIndex:0];
            [self showTrackInMainUIPart:track];
            return;
        } else {
            NSLog(@"No track found at index 0. Remote Sharing videoTracks empty or not initialized.");
        }
    }
    
    if(!((RTCCall*)_call).isSharingEnabled && [((RTCCall*)_call) isRemoteVideoEnabled]){
        NSLog(@"There is only remote video display it in the center of the screen");
        RTCMediaStream *videoStream = [_rtcService remoteVideoStreamForCall:((RTCCall*)_call)];
        if(videoStream && videoStream.videoTracks && [videoStream.videoTracks count] > 0) {
            RTCVideoTrack *track = [videoStream.videoTracks objectAtIndex:0];
            [self showTrackInMainUIPart:track];
            return;
        } else {
            NSLog(@"No track found at index 0. Remote Video videoTracks empty or not initialized.");
        }
    }
    
    if(((RTCCall*)_call).isSharingEnabled && [((RTCCall*)_call) isRemoteVideoEnabled]){
        NSLog(@"There is two track Video + Sharing");
        RTCMediaStream *sharingStream = [_rtcService remoteSharingStreamForCall:((RTCCall*)_call)];
        RTCMediaStream *videoStream = [_rtcService remoteVideoStreamForCall:((RTCCall*)_call)];
        
        if((sharingStream && sharingStream.videoTracks && [sharingStream.videoTracks count] > 0) && (videoStream && videoStream.videoTracks)) {
            RTCVideoTrack *remoteSharing = [sharingStream.videoTracks objectAtIndex:0];
            
            RTCVideoTrack *remoteVideo;
            if([((RTCCall*)_call).peer isKindOfClass:[Room class]] && [videoStream.videoTracks count] > 0)
                remoteVideo = [videoStream.videoTracks objectAtIndex:0];
            else if( [videoStream.videoTracks count] > 1)
                remoteVideo = [videoStream.videoTracks objectAtIndex:1];
            
            if(remoteSharing)
                [self showTrackInMainUIPart:remoteSharing];
            if(remoteVideo)
                [self showTrackInSecondUIPart:remoteVideo];
            
            if(((RTCCall*)_call).isLocalVideoEnabled){
                // Drop the local video
                [_rtcService removeVideoMediaFromCall:((RTCCall*)_call)];
                MBProgressHUD *hud = [UITools showHUDWithMessage:@"Video removed" forView:self.view mode:MBProgressHUDModeText dismissCompletionBlock:nil];
                [hud hide:YES afterDelay:2];
            }
            return;
        } else {
            NSLog(@"No track found. Remote Video and/or sharing videoTracks empty or not initialized.");
        }
    }
    NSLog(@"Unable to determine what to draw for call %@", ((RTCCall*)_call));
}


-(void) didRemoveRemoteVideoTrack:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveRemoteVideoTrack:notification];
        });
        return;
    }
    if(![_call isKindOfClass:[RTCCall class]])
        return;
    
    if(!((RTCCall*)_call).isSharingEnabled && ![((RTCCall*)_call) isRemoteVideoEnabled]){
        // We had a video and there is no more video, so remove it.
        [self hideTrackInMainUIPart];
        [self showElementForAudioCall];
        return;
    }
    
    if(((RTCCall*)_call).isSharingEnabled && ![((RTCCall*)_call) isRemoteVideoEnabled]){
        // the track is a sharing, display it in the center of the screen
        [self hideTrackInSecondUIPart];
        return;
    }
    
    if(!((RTCCall*)_call).isSharingEnabled && [((RTCCall*)_call) isRemoteVideoEnabled]){
        // There is only remote video display it in the center of the screen
        [self hideTrackInSecondUIPart];
        return;
    }
    
    if(((RTCCall*)_call).isSharingEnabled && [((RTCCall*)_call) isRemoteVideoEnabled]){
        // There is two track Video + Sharing
        return;
    }

}

-(void) showTrackInMainUIPart:(RTCVideoTrack *) videoTrack {
    //Dead locks happen in the next line...
    //NSLog(@"ShowTrack in Main UI %@", videoTrack);
    
    [_remoteVideoTrack removeRenderer:_remoteVideoView];
    _remoteVideoTrack = nil;
    [_remoteVideoView renderFrame:nil];
    _remoteVideoTrack = videoTrack;
    [_remoteVideoTrack addRenderer:_remoteVideoView];
    
    _remoteVideoView.hidden = NO;
    [self layoutUIForCallStateAnimated:YES];
    [self layoutSubviews];
    [self.view setNeedsLayout];
}

-(void) showTrackInSecondUIPart:(RTCVideoTrack *) videoTrack {
    // TODO : Draw the 2nd UI
    NSLog(@"ShowTrack in 2nd UI %@", videoTrack);
    
    [_remoteVideoTrackWithSharing removeRenderer:_remoteVideoWithSharing];
    _remoteVideoTrackWithSharing = nil;
    [_remoteVideoWithSharing renderFrame:nil];
    
    _remoteVideoTrackWithSharing = videoTrack;
    [_remoteVideoTrackWithSharing addRenderer:_remoteVideoWithSharing];
    
    _remoteVideoWithSharing.hidden = NO;
    [self layoutUIForCallStateAnimated:YES];
//    [self layoutSubviews];
    [self.view setNeedsLayout];
}

-(void) hideTrackInMainUIPart {
    NSLog(@"hideTrackInMainUIPart");
    [_remoteVideoTrack removeRenderer:_remoteVideoView];
    _remoteVideoTrack = nil;
    [_remoteVideoView renderFrame:nil];
    _remoteVideoView.hidden = YES;
    [self.view setNeedsLayout];
}

-(void) hideTrackInSecondUIPart {
    NSLog(@"hideTrackInSecondUIPart");
    [_remoteVideoTrackWithSharing removeRenderer:_remoteVideoWithSharing];
    _remoteVideoTrackWithSharing = nil;
    [_remoteVideoWithSharing renderFrame:nil];
    _remoteVideoWithSharing.hidden = YES;
    
    [self.view setNeedsLayout];
}


- (void)layoutSubviews {
    CGRect bounds = self.view.bounds;
    if (_remoteVideoSize.width > 0 && _remoteVideoSize.height > 0) {
        // Aspect fill remote video into bounds.
        CGRect remoteVideoFrame = AVMakeRectWithAspectRatioInsideRect(_remoteVideoSize, bounds);
        [self setRemoteVideoViewFrame:remoteVideoFrame];
    } else {
        [self setRemoteVideoViewFrame:bounds];
    }
    if(_remoteVideoWithSharingSize.width > 0 && _remoteVideoWithSharingSize.height > 0){
        CGRect remoteVideoWithSharingFrame = AVMakeRectWithAspectRatioInsideRect(_remoteVideoWithSharingSize, _remoteVideoWithSharing.frame);
        [self setRemoteVideoViewWithSharingFrame:remoteVideoWithSharingFrame];
    }
}

-(void) setRemoteVideoViewFrame:(CGRect) rect {
    _remoteVideoView.frame = rect;
    _remoteVideoViewWidthConstraint.constant = rect.size.width;
    _remoteVideoViewHeightConstraint.constant = rect.size.height;
}

-(void) setRemoteVideoViewWithSharingFrame:(CGRect) rect {
    _remoteVideoWithSharing.frame = rect;
    _remoteVideoViewWithSharingWidthConstraint.constant = rect.size.width;
    _remoteVideoViewWithSharingHeightConstraint.constant = rect.size.height;
}

#pragma mark - RTCEAGLViewDelegate
- (void)videoView:(RTCEAGLVideoView*)videoView didChangeVideoSize:(CGSize)size {
    if (videoView == _remoteVideoView) {
        NSLog(@"didChangeVideoSize Right videoView %f %f", size.width, size.height);
        _remoteVideoSize = size;
    }
    if(videoView == _remoteVideoWithSharing){
        NSLog(@"didChangeVideoSize ViewWithSharing Right videoView %f %f", size.width, size.height);
        _remoteVideoWithSharingSize = size;
    }
    [self layoutSubviews];
    [self.view setNeedsLayout];
}

#pragma mark - stats
-(void) rtcCallViewDidStats:(RTCCallView *)view {
    _rtcCallStatsViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:_rtcCallStatsViewController animated:YES completion:nil];
}

-(void) hideUI:(NSTimer *) timer {
    if(!_actionButtonView.hidden)
        [self didTapRemoteVideoView:nil];
}

- (IBAction)didTapSwithCamera:(UIButton *)sender {
    if(_cameraVideoCapturer != nil) {
        [_cameraVideoCapturer stopCapture];
        _isUsingBackCamera = !_isUsingBackCamera;
        [self startCapturer];
    }
}

- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position {
    NSArray *devices = [RTCCameraVideoCapturer captureDevices];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == position) return device;
    }
    return nil;
}

- (IBAction)zoomRemoteVideo:(UIPinchGestureRecognizer *)sender {
    if(((RTCCall*)_call).isSharingEnabled){
        if (UIGestureRecognizerStateBegan == sender.state || UIGestureRecognizerStateChanged == sender.state) {
            float currentScale = [[sender.view.layer valueForKeyPath:@"transform.scale.x"] floatValue];
            float minScale = 1.0;
            float maxScale = 4.0;
            float zoomSpeed = 1;
            float deltaScale = sender.scale;
            deltaScale = ((deltaScale - 1) * zoomSpeed) + 1;
            deltaScale = MIN(deltaScale, maxScale / currentScale);
            deltaScale = MAX(deltaScale, minScale / currentScale);
            CGAffineTransform zoomTransform = CGAffineTransformScale(sender.view.transform, deltaScale, deltaScale);
            sender.view.transform = zoomTransform;
            sender.scale = 1;
        }
    }
}

#pragma mark - publishers

-(void) didUpdateConference:(NSNotification *) notification {
    if(![_call.peer isKindOfClass:[Room class]])
        return;
    
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateConference:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kConferenceChangedAttributesKey];
    Conference *aConference = (Conference *)[userInfo objectForKey:kConferenceKey];
    
    Room *room = (Room*)_call.peer;

    if([aConference isEqual:room.conference]){
        NSLog(@"DID UPDATE CONFERENCE IN RTCCALL %@", room.conference);
        if([changedKeys containsObject:@"publishers"]){
            [self showHidePublisherButton];
            // show automatically the first video stream
            if(room.conference.publishers.count == 1){
                ConferencePublisher *publisher = [room.conference.publishers objectAtIndex:0];
                if(!publisher.subscribed){
                    [[ServicesManager sharedInstance].conferencesManagerService subscribeToVideoSharedByConferencePublisher:publisher inConference:room.conference];
                }
            }
        }
        if([changedKeys containsObject:@"participants"]){
            [self layoutUIForCallStateAnimated:YES];
        }
        // show automatically the first video stream
        [self automaticallyShowPublisher];
    }
}

-(void) automaticallyShowPublisher {
    if(![_call.peer isKindOfClass:[Room class]]){
        return;
    }
    Room *room = (Room*)_call.peer;
    if(!_manuallySwitchedRemoteVideo && room.conference.publishers.count > 0){
        ConferencePublisher *publisher = [room.conference.publishers objectAtIndex:0];
        if(!publisher.subscribed){
            [[ServicesManager sharedInstance].conferencesManagerService subscribeToVideoSharedByConferencePublisher:publisher inConference:room.conference];
        }
    }
}

-(void) showHidePublisherButton {
    if([_call.peer isKindOfClass:[Room class]]){
        Room *room = (Room*)_call.peer;
        if(room.conference.publishers.count > 0){
            [self showPublisherButton];
        } else
            [self hidePublisherButton];
    } else
        [self hidePublisherButton];
}

-(void) showPublisherButton {
    _publishersButton.hidden = NO;
    if(!_switchCamera.hidden)
        _publisherButtonRightConstraint.constant = _switchCamera.frame.size.width + 4;
    else
        _publisherButtonRightConstraint.constant = -_switchCamera.frame.size.width;
}

-(void) hidePublisherButton {
    _publishersButton.hidden = YES;
    _publisherButtonRightConstraint.constant = 0;
}
@end
