/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CustomNavigationController.h"
#import "UINavigationController+Orientation.h"
#import "UINavigationController+StatusBar.h"
#import <Rainbow/Rainbow.h>
#import "MyInfosTableViewController.h"
#import "UINetworkLostViewController.h"
#import "UIGuestModeViewController.h"
#import "UIStoryboardManager.h"
#import "Firebase.h"

@interface CustomNavigationController ()
@property (nonatomic, strong) UINetworkLostViewController *networkLostViewCtrl;
@property (nonatomic, strong) UIGuestModeViewController *guestModeViewCtrl;
@property (nonatomic, strong) UIStoryboard *networkLostStoryBoard, *guestModeStoryBoard;
@property (nonatomic, strong) NSTimer *displayTimer;
@end

@implementation CustomNavigationController

-(void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willLogin:) name:kLoginManagerWillLogin object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tryToReconnect:) name:kLoginManagerTryToReconnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kServicesManagerNetworkConnectionDetected object:nil];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerWillLogin object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerTryToReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kServicesManagerNetworkConnectionDetected object:nil];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Login manager notifications

-(void) willLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self willLogin:notification];
        });
        return;
    }
}

-(void) displayNetworkLossBanner {
    // Network error banner
    if(![ServicesManager sharedInstance].loginManager.loginDidSucceed || ![ServicesManager sharedInstance].loginManager.isConnected){
        if ([ServicesManager sharedInstance].isNetworkDisconnected && _networkLostViewCtrl != nil && !_networkLostViewCtrl.noNetworkConnectionIssue) {
            [self hideNetworkLostNotification];
        }
        if(!_networkLostViewCtrl){
            _networkLostViewCtrl = [[UIStoryboardManager sharedInstance].networkLostStoryBoard instantiateViewControllerWithIdentifier:@"networkErrorViewID"];
            _networkLostViewCtrl.noNetworkConnectionIssue = [ServicesManager sharedInstance].isNetworkDisconnected;
            _networkLostViewCtrl.view.frame = CGRectMake(0, self.navigationBar.frame.size.height + [UIApplication sharedApplication].statusBarFrame.size.height + _guestModeViewCtrl.view.frame.size.height, [UIApplication sharedApplication].statusBarFrame.size.width, kNetworkLostHeight);
            _lostNetwotkViewSize = _networkLostViewCtrl.view.frame.size.height;
            CATransition *applicationLoadViewIn = [CATransition animation];
            [applicationLoadViewIn setDuration:1];
            [applicationLoadViewIn setType:kCATransitionReveal];
            [applicationLoadViewIn setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [[_networkLostViewCtrl.view layer] addAnimation:applicationLoadViewIn forKey:kCATransitionFromTop];
            
            [self.view addSubview:_networkLostViewCtrl.view];
            [_networkLostViewCtrl.view setUserInteractionEnabled:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:CustomNavigationControllerWillShowNetworkLostView object:nil];
            _isDisplayingNetworkLostView = YES;
            NSLog(@"Display network loss banner");
        }
    }
    [_displayTimer invalidate];
    _displayTimer = nil;
}

-(void) displayGuestModeBanner {
    // Guest banner
    if ([ServicesManager sharedInstance].myUser.isGuest && !_isDisplayingGuestModeView) {
        if(!_guestModeViewCtrl){
            _guestModeViewCtrl = [[UIStoryboardManager sharedInstance].guestModeStoryBoard instantiateViewControllerWithIdentifier:@"guestModeViewID"];
            
            _guestModeViewCtrl.view.frame = CGRectMake(0, self.navigationBar.frame.size.height + [UIApplication sharedApplication].statusBarFrame.size.height + _networkLostViewCtrl.view.frame.size.height, [UIApplication sharedApplication].statusBarFrame.size.width, kGuestModeHeight);
            _guestBannerViewSize = _guestModeViewCtrl.view.frame.size.height;
            CATransition *applicationLoadViewIn = [CATransition animation];
            [applicationLoadViewIn setDuration:1];
            [applicationLoadViewIn setType:kCATransitionReveal];
            [applicationLoadViewIn setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [[_guestModeViewCtrl.view layer] addAnimation:applicationLoadViewIn forKey:kCATransitionFromTop];
            
            [self.view addSubview:_guestModeViewCtrl.view];
            [_guestModeViewCtrl.view setUserInteractionEnabled:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:CustomNavigationControllerWillShowGuestModeView object:nil];
            _isDisplayingGuestModeView = YES;
        }
    } else {
        if(_isDisplayingGuestModeView){
            [self willHideGuestModeNotification];
        }
    }
}

-(void) didLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    
    [self willHideNetworkLostNotification];
    [self displayGuestModeBanner];
}

-(void) didLogout:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    
    [self willHideNetworkLostNotification];
    [self willHideGuestModeNotification];
}

-(void) didReconnect:(NSNotification *) notification {
    NSString *networkStatus = (NSString *)notification.object;
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    [self willHideNetworkLostNotification];
    [self displayGuestModeBanner];
    NSLog(@"network_status %@",networkStatus);
    if (networkStatus != nil) {
        [FIRAnalytics setUserPropertyString:networkStatus forName:@"network_reachability"];
    }
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    if(!_displayTimer)
        _displayTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(displayNetworkLossBanner) userInfo:nil repeats:NO];
}

-(void) tryToReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self tryToReconnect:notification];
        });
        return;
    }
}

-(void) willHideNetworkLostNotification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self willHideNetworkLostNotification];
        });
        return;
    }
    
    [_displayTimer invalidate];
    _displayTimer = nil;
    if(_networkLostViewCtrl)
        [self hideNetworkLostNotification];
}

-(void) willHideGuestModeNotification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self willHideGuestModeNotification];
        });
        return;
    }
    
    if(_guestModeViewCtrl)
        [self hideGuestModeNotification];
}

-(void) hideNetworkLostNotification {
    [_networkLostViewCtrl.view removeFromSuperview];
    _networkLostViewCtrl = nil;
    _isDisplayingNetworkLostView = NO;
    NSLog(@"Hide network loss banner");
    [[NSNotificationCenter defaultCenter] postNotificationName:CustomNavigationControllerDidHideNetworkLostView object:nil];
}

-(void) hideGuestModeNotification {
    if (![ServicesManager sharedInstance].myUser.isGuest) {
        [_guestModeViewCtrl.view removeFromSuperview];
        _guestModeViewCtrl = nil;
        _isDisplayingGuestModeView = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:CustomNavigationControllerDidHideGuestModeView object:nil];
    }
}

@end
