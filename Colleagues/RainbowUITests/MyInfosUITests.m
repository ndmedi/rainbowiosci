///*
// * Rainbow
// *
// * Copyright (c) 2016, ALE International
// * All rights reserved.
// *
// * ALE International Proprietary Information
// *
// * Contains proprietary/trade secret information which is the property of
// * ALE International and must not be made available to, or copied or used by
// * anyone outside ALE International without its written authorization
// *
// * Not to be disclosed or used except in accordance with applicable agreements.
// */
//
//#import <Foundation/Foundation.h>
//#import "RainbowUITests.h"
//
//
//@interface MyInfosUITests : RainbowUITests
//@end
//
//
//@implementation MyInfosUITests
//
//-(void) testChangeMyPresence {
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    
//    [self.app.navigationBars[@"Conversations"].images[@"Avatar"] tap];
//   
//    // no fucking way to access to the parallax header view to check the avatar presence icon...
//    
//    [self.app.tables.staticTexts[@"Away"] tap];
//    XCUIElementQuery *cell = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Away"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"value MATCHES %@", @"Active"] evaluatedWithObject:[cell elementBoundByIndex:0] handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    [self.app.tables.staticTexts[@"Invisible"] tap];
//    cell = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Invisible"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"value MATCHES %@", @"Active"] evaluatedWithObject:[cell elementBoundByIndex:0] handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    [self.app.tables.staticTexts[@"Do not disturb"] tap];
//    cell = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Do not disturb"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"value MATCHES %@", @"Active"] evaluatedWithObject:[cell elementBoundByIndex:0] handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    [self.app.tables.staticTexts[@"Online"] tap];
//    cell = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Online"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"value MATCHES %@", @"Active"] evaluatedWithObject:[cell elementBoundByIndex:0] handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    
//    //XCTAssert(NO);
//}
//
//@end
