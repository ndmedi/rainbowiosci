///*
// * Rainbow
// *
// * Copyright (c) 2016, ALE International
// * All rights reserved.
// *
// * ALE International Proprietary Information
// *
// * Contains proprietary/trade secret information which is the property of
// * ALE International and must not be made available to, or copied or used by
// * anyone outside ALE International without its written authorization
// *
// * Not to be disclosed or used except in accordance with applicable agreements.
// */
//
//#import <Foundation/Foundation.h>
//#import "RainbowUITests.h"
//#import <RainbowUnitTestFakeServer/NSDictionary+JSON.h>
//
//@interface OnboardingUITests : RainbowUITests
//@end
//
//
//@implementation OnboardingUITests
//
//
//-(void) enterLogin:(NSString *) login password:(NSString *) password {
//    // Now login
//    XCUIElement *eMailAddressTextField = self.app.textFields[@"E-mail address"];
//    [eMailAddressTextField tap];
//    
//    // If we have some content, the clear text button is shown, otherwise not.
//    if (self.app.buttons[@"Clear text"].exists) {
//        [self.app.buttons[@"Clear text"] tap];
//    }
//    [eMailAddressTextField typeText:login];
//    
//    XCUIElement *passwordSecureTextField = self.app.secureTextFields[@"Password"];
//    [passwordSecureTextField tap];
//    
//    // If we have some content, the clear text button is shown, otherwise not.
//    if (self.app.buttons[@"Clear text"].exists) {
//        [self.app.buttons[@"Clear text"] tap];
//    }
//    
//    [passwordSecureTextField typeText:password];
//    
//    XCUIElement *loginButton = self.app.buttons[@"Login"];
//    [loginButton tap];
//}
//
//// Move this test to LoginUITests.m
//-(void) DISABLEDtestLogin {
//    
//    [self launchApplicationWithoutAutoLogin];
//    
//    // Correct login, wrong password
//    [self enterLogin:[self.server myLoginEmail] password:@"Blablabla"];
//    
//    XCTAssert(self.app.alerts[@"Error during login"].exists);
//    XCTAssert(self.app.alerts[@"Error during login"].staticTexts[@"Unknown login or wrong password"].exists);
//    [self.app.alerts[@"Error during login"].buttons[@"OK"] tap];
//}
//
//
//
//-(void) NOtestEmailClipboardOnboardingWhileAlreadyLogged {
//    
//    NSDictionary *invitation = @{@"invitationId": @"586bb1c624c246a8411550be", @"loginEmail": @"amVyb21lLmhleW1vbmV0MkBqaGUudGVzdC5vcGVucmFpbmJvdy5uZXQ="};
//    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
//    pasteboard.string = [invitation jsonStringRepresentation];
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    
//    // We are on the JoinRainbow screen
//    /*
//     XCUIElement *joinRainbow = [self.app.staticTexts elementMatchingPredicate:[NSPredicate predicateWithFormat:@"label BEGINSWITH %@", @"Join Rainbow"]];
//     }*/
//    
//    // Check the login email is present in text
//    // [app.staticTexts[@"Join Rainbow\n\nYour login is jerome.heymonet2@jhe.test.openrainbow.net"] tap];
//    XCUIElement *text = [self.app.staticTexts elementMatchingPredicate:[NSPredicate predicateWithFormat:@"label CONTAINS %@", @"jerome.heymonet2@jhe.test.openrainbow.net"]];
//    XCTAssert(text.exists);
//    
//    XCUIElement *passwordSecureTextField = self.app.secureTextFields[@"New password"];
//    [passwordSecureTextField tap];
//    
//    [passwordSecureTextField typeText:@"123456"];
//    
//    [self.app.scrollViews.otherElements.buttons[@"Continue"] tap];
//    
//    // handle POST /api/rainbow/enduser/v1.0/users/self-register
//    
//    XCTAssert(NO);
//}
//
//-(void) DISABLEDtestOnboarding {
//    
//    // Now launch application.
//    [self launchApplicationWithoutAutoLogin];
//    
//    [self.app.buttons[@"Create account"] tap];
//    
//    XCUIElement *eMailAddressTextField = self.app.textFields[@"E-mail address"];
//    [eMailAddressTextField tap];
//    [eMailAddressTextField typeText:[self.server myLoginEmail]];
//    
//    [self.app.buttons[@"Continue"] tap];
//    
//    XCUIElement *codeTextField = self.app.textFields[@"1 2 3 4"];
//    [codeTextField tap];
//    [codeTextField typeText:@"0000"];
//    
//    XCUIElement *passwordTextField = self.app.secureTextFields[@"New password"];
//    [passwordTextField tap];
//    [passwordTextField typeText:[self.server myLoginPassword]];
//    
//    [self.app.buttons[@"Continue"] tap];
//    
//    [NSThread sleepForTimeInterval:3.0f];
//    
//    //XCTAssert(NO);
//}
//
//@end
