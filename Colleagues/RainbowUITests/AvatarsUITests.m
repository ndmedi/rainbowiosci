///*
// * Rainbow
// *
// * Copyright (c) 2016, ALE International
// * All rights reserved.
// *
// * ALE International Proprietary Information
// *
// * Contains proprietary/trade secret information which is the property of
// * ALE International and must not be made available to, or copied or used by
// * anyone outside ALE International without its written authorization
// *
// * Not to be disclosed or used except in accordance with applicable agreements.
// */
//
//#import <Foundation/Foundation.h>
//#import "RainbowUITests.h"
//
//
//@interface AvatarsUITests : RainbowUITests
//@end
//
//
//@implementation AvatarsUITests
//
//-(void) testConversationsAvatarsNoAvatars {
//    
//    // Prepare our initial state, with 2 conversations.
//    // Conversation 1 P2P with Marcel which is in my roster and Online.
//    // Conversation 3 Room with Marcel and Micheline, conv is muted.
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(1);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ConversationAPI *conversation3 = [ConversationAPI new];
//    conversation3.peerID = @"rainbow_room_789";
//    conversation3.type = @"room";
//    conversation3.jid = @"room_equipe@domain.com";
//    conversation3.lastMessage = @"Je fais des tests";
//    conversation3.lastMessageDate = @"2016-11-01T09:00:00.000Z";
//    conversation3.unread = @(0);
//    conversation3.mute = YES;
//    [self.server addConversation:conversation3];
//    
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    ContactAPI *contact2 = [ContactAPI new];
//    contact2.ID = @"rainbow_456";
//    contact2.jid = @"micheline@domain.com";
//    contact2.loginEmail = @"micheline@dupre.com";
//    contact2.firstname = @"Micheline";
//    contact2.lastname = @"Dupré";
//    contact2.inRoster = YES;
//    contact2.isPending = YES;
//    contact2.presence = @"Away";
//    [self.server addContact:contact2];
//    
//    RoomAPI *room1 = [RoomAPI new];
//    room1.ID = @"rainbow_room_789";
//    room1.jid = @"room_equipe@domain.com";
//    room1.name = @"Room equipe dev";
//    room1.topic = @"Topic de room";
//    room1.visibility = @"private";
//    room1.creationDate = @"2016-03-28T18:27:10.970Z";
//    room1.creator = [self.server myRainbowID];
//    [room1 addUser:[self.server myRainbowID] privilege:@"moderator" status:@"accepted"];
//    [room1 addUser:@"rainbow_123" privilege:@"user" status:@"accepted"];
//    [room1 addUser:@"rainbow_456" privilege:@"user" status:@"accepted"];
//    [self.server addRoom:room1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    XCTAssert([[cell1.images[@"Avatar"] value] isEqualToString:@"Initials"]);
//    
//    
//    XCUIElementQuery *cell2 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Room equipe dev"];
//    XCTAssert([[cell2.images[@"Avatar"] value] isEqualToString:@"Initials|Initials|Initials"]);
//    
//    //XCTAssert(NO);
//}
//
//
//-(void) testConversationsAvatarsWithAvatars {
//    
//    // Prepare our initial state, with 2 conversations.
//    // Conversation 1 P2P with Marcel which is in my roster and Online.
//    // Conversation 3 Room with Marcel and Micheline, conv is muted.
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(1);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ConversationAPI *conversation3 = [ConversationAPI new];
//    conversation3.peerID = @"rainbow_room_789";
//    conversation3.type = @"room";
//    conversation3.jid = @"room_equipe@domain.com";
//    conversation3.lastMessage = @"Je fais des tests";
//    conversation3.lastMessageDate = @"2016-11-01T09:00:00.000Z";
//    conversation3.unread = @(0);
//    conversation3.mute = YES;
//    
//    [self.server addConversation:conversation3];
//    
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    contact1.avatarColor = [UIColor redColor];
//    [self.server addContact:contact1];
//    
//    ContactAPI *contact2 = [ContactAPI new];
//    contact2.ID = @"rainbow_456";
//    contact2.jid = @"micheline@domain.com";
//    contact2.loginEmail = @"micheline@dupre.com";
//    contact2.firstname = @"Micheline";
//    contact2.lastname = @"Dupré";
//    contact2.inRoster = YES;
//    contact2.isPending = YES;
//    contact2.presence = @"Away";
//    contact2.avatarColor = [UIColor blueColor];
//    [self.server addContact:contact2];
//    
//    RoomAPI *room1 = [RoomAPI new];
//    room1.ID = @"rainbow_room_789";
//    room1.jid = @"room_equipe@domain.com";
//    room1.name = @"Room equipe dev";
//    room1.topic = @"Topic de room";
//    room1.visibility = @"private";
//    room1.creationDate = @"2016-03-28T18:27:10.970Z";
//    room1.creator = [self.server myRainbowID];
//    [room1 addUser:[self.server myRainbowID] privilege:@"moderator" status:@"accepted"];
//    [room1 addUser:@"rainbow_123" privilege:@"user" status:@"accepted"];
//    [room1 addUser:@"rainbow_456" privilege:@"user" status:@"accepted"];
//    [self.server addRoom:room1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    // wait for avatars to be loaded ?
//    [NSThread sleepForTimeInterval: 10.0f];
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    // When the user has an avatar, the value is set to its displayName
//    XCTAssert([[cell1.images[@"Avatar"] value] isEqualToString:@"Marcel Dupont"]);
//    
//    
//    XCUIElementQuery *cell2 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Room equipe dev"];
//    // When the user has an avatar, the value is set to its displayName
//    // For the rooms, the full value is a concatenation of the participant avatars separated by '|'
//    // Here MyUser (Alice TESTER) dont have an avatar, value is still 'Initials' for her.
//    XCTAssert([[cell2.images[@"Avatar"] value] isEqualToString:@"Micheline Dupré|Initials|Marcel Dupont"]);
//    
//    //XCTAssert(NO);
//}
//
//@end
