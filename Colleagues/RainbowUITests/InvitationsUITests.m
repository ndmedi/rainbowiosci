///*
// * Rainbow
// *
// * Copyright (c) 2016, ALE International
// * All rights reserved.
// *
// * ALE International Proprietary Information
// *
// * Contains proprietary/trade secret information which is the property of
// * ALE International and must not be made available to, or copied or used by
// * anyone outside ALE International without its written authorization
// *
// * Not to be disclosed or used except in accordance with applicable agreements.
// */
//
//#import <Foundation/Foundation.h>
//#import "RainbowUITests.h"
//
//
//@interface InvitationsUITests : RainbowUITests
//@end
//
//
//@implementation InvitationsUITests
//
//-(void) testAcceptReceivedInvitation {
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    InvitationAPI *invitation1 = [InvitationAPI new];
//    invitation1.received = YES;
//    invitation1.invitingUserId = @"rainbow_123";
//    invitation1.invitingUserEmail = @"marcel@dupont.com";
//    invitation1.status = @"pending";
//    invitation1.invitingDate = @"2016-09-28T16:31:36.881Z";
//    [self.server addInvitation:invitation1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [self.app.tabBars.buttons[@"Contacts"] tap];
//    
//    [self.app.buttons[@"Invitations"] tap];
//    
//    // Check if invitation section is present
//    XCTAssert(self.app.tables.staticTexts[@"Received"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"Received"].isHeader);
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    XCTAssertNotNil(cell1);
//    XCTAssert(cell1.staticTexts[@"Marcel Dupont"].exists);
//
//    // Buttons are AcceptInvitation and DeclineInvitation
//    [self.app.buttons[@"AcceptInvitation"] tap];
//    
//    // Now the contact should be in the section of it's lastname
//    // and not anymore in the invitation section.
//    // Accept and Decline buttons should have disapear
//    
//    XCTAssertFalse(self.app.tables.staticTexts[@"Invitations"].exists);
//   
//    
//    [self.app.buttons[@"My network"] tap];
//    // Check new section
//    XCTAssert(self.app.tables.staticTexts[@"D"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"D"].isHeader);
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    XCUIElementQuery *cell2 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    XCTAssertNotNil(cell2);
//    XCTAssert(cell2.staticTexts[@"Marcel Dupont"].exists);
//    XCTAssert(self.app.tables.buttons[@"Vcard"].exists);
//    
//    //XCTAssert(NO);
//}
//
//
//-(void) testDeclineReceivedInvitation {
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    InvitationAPI *invitation1 = [InvitationAPI new];
//    invitation1.received = YES;
//    invitation1.invitingUserId = @"rainbow_123";
//    invitation1.invitingUserEmail = @"marcel@dupont.com";
//    invitation1.status = @"pending";
//    invitation1.invitingDate = @"2016-09-28T16:31:36.881Z";
//    [self.server addInvitation:invitation1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [self.app.tabBars.buttons[@"Contacts"] tap];
//    
//    [self.app.buttons[@"Invitations"] tap];
//    
//    // Check if invitation section is present
//    XCTAssert(self.app.tables.staticTexts[@"Received"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"Received"].isHeader);
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    XCTAssertNotNil(cell1);
//    XCTAssert(cell1.staticTexts[@"Marcel Dupont"].exists);
//    
//    // Buttons are AcceptInvitation and DeclineInvitation
//    [self.app.buttons[@"DeclineInvitation"] tap];
//    
//    // Now the contact should be in the section of it's lastname
//    // and not anymore in the invitation section.
//    // Accept and Decline buttons should have disapear
//    
//    XCTAssertFalse(self.app.tables.staticTexts[@"Invitations"].exists);
//    
//
//    //XCTAssert(NO);
//}
//
//-(void) testSentInvitationToNonRaibow {
//    
//
//    InvitationAPI *invitation1 = [InvitationAPI new];
//    invitation1.received = NO;
//    invitation1.invitedUserId = @"";
//    invitation1.invitedUserEmail = @"marcel@dupont.com";
//    invitation1.status = @"pending";
//    invitation1.invitingDate = @"2016-09-28T16:31:36.881Z";
//    [self.server addInvitation:invitation1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [self.app.tabBars.buttons[@"Contacts"] tap];
//    
//    [self.app.buttons[@"Invitations"] tap];
//    
//    // We'll have a contact with only the email address.
//    // With the button 'Pending' instead of vcard
//    // Check if M section is present (_M_ arcel@dupont.com)
//    XCTAssert(self.app.tables.staticTexts[@"Sent"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"Sent"].isHeader);
//    
//    // TODO: Sometimes we have another contact, because we had a conversation with him (in another test for example)
//    // The conversation is removed, because does not exists anymore, but the contact remains..
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    XCTAssert(self.app.tables.staticTexts[@"marcel@dupont.com"].exists);
//    
//    
//    
//    //XCTAssert(NO);
//}
//
//-(void) testSdentInvitationToNonRaibowEvent {
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [self.app.tabBars.buttons[@"Contacts"] tap];
//    
//    [self.app.buttons[@"Invitations"] tap];
//    
//    XCTAssertEqual([self.app.tables.cells count], 0);
//    
//    [NSThread sleepForTimeInterval:0.2f];
//    
//    InvitationAPI *invitation1 = [InvitationAPI new];
//    invitation1.received = NO;
//    invitation1.invitedUserId = @"";
//    invitation1.invitedUserEmail = @"marcel@dupont.com";
//    invitation1.status = @"pending";
//    invitation1.invitingDate = @"2016-09-28T16:31:36.881Z";
//    [self.server addInvitation:invitation1];
//    
//    [NSThread sleepForTimeInterval:2.0f];
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"marcel@dupont.com"];
//    XCTAssertNotNil(cell1);
//    XCTAssert(cell1.staticTexts[@"marcel@dupont.com"].exists);
//    
//    // We'll have a contact with only the email address.
//    // With the button 'Pending' instead of vcard
//    // Check if M section is present (_M_ arcel@dupont.com)
//    XCTAssert(self.app.tables.staticTexts[@"Sent"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"Sent"].isHeader);
//    
//   
//    
//    //XCTAssert(NO);
//}
//
//-(void) testSentInvitationToNonRaibowEventResend {
//    
//  
//    InvitationAPI *invitation1 = [InvitationAPI new];
//    invitation1.received = NO;
//    invitation1.invitedUserId = @"";
//    invitation1.invitedUserEmail = @"marcel@dupont.com";
//    invitation1.status = @"pending";
//    invitation1.invitingDate = @"2016-09-28T16:31:36.881Z";
//    [self.server addInvitation:invitation1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    [NSThread sleepForTimeInterval:2.0f];
//    [self.app.tabBars.buttons[@"Contacts"] tap];
//    
//     [self.app.buttons[@"Invitations"] tap];
//    
//    // We'll have a contact with only the email address.
//    // With the button 'Pending' instead of vcard
//    // Check if M section is present (_M_ arcel@dupont.com)
//    XCTAssert(self.app.tables.staticTexts[@"Sent"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"Sent"].isHeader);
//    
//    // TODO: Sometimes we have another contact, because we had a conversation with him (in another test for example)
//    // The conversation is removed, because does not exists anymore, but the contact remains..
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    XCTAssert(self.app.tables.staticTexts[@"marcel@dupont.com"].exists);
//    //XCTAssert(self.app.tables.buttons[@"Pending"].exists);
//    
//    [self.server addInvitation:invitation1];
//    [NSThread sleepForTimeInterval:0.3f];
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    XCTAssert(self.app.tables.staticTexts[@"marcel@dupont.com"].exists);
//    //XCTAssert(self.app.tables.buttons[@"Pending"].exists);
//    
//    //XCTAssert(NO);
//}
//
//-(void) testSentInvitationToNonRaibowEventWithLocalContactExisting {
//    
//    // Now launch application.
//    [self launchApplication];
//    [NSThread sleepForTimeInterval:2.0f];
//    [self.app.tabBars.buttons[@"Contacts"] tap];
//    
//    [self.app.buttons[@"Invitations"] tap];
//    
//    [self.app.buttons[@"Invite a contact"] tap];
//    
//    [self.app.sheets.buttons[@"By selecting one of your contacts"] tap];
//
//    
//    [self.app.tables[@"ContactsListView"]/*@START_MENU_TOKEN@*/.staticTexts[@"Gerald Local"]/*[[".cells[@\"Gerald Local\"].staticTexts[@\"Gerald Local\"]",".staticTexts[@\"Gerald Local\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
//    [self.app.sheets[@"How do you want to invite Gerald Local?"].buttons[@"Send email"] tap];
//    // We have our local contact already loaded.
//    
//    [NSThread sleepForTimeInterval:2.0f];
//    
//   // XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    // Our local contact will be now a rainbow contact with Pending button
//    // Section 'L' because of name 'Local'
//  
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"gerald@petit.com"];
//    XCTAssertNotNil(cell1);
//    XCTAssert(cell1.staticTexts[@"gerald@petit.com"].exists);
//    
//    
//
//    XCTAssert(self.app.tables.staticTexts[@"Sent"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"Sent"].isHeader);
//}
//
//
//// TODO: di it in right way .. 
//
//-(void) testSentInvitationToNonRaibowWithLocalContactExistingCreatesItsAccount {
//    // I invited a local contact to rainbow,
//    // It accept and creates its account while my app running.
//   
//    InvitationAPI *invitation1 = [InvitationAPI new];
//    invitation1.received = NO;
//    invitation1.invitedUserId = @"";
//    invitation1.invitedUserEmail = @"gerald@petit.com";
//    invitation1.status = @"pending";
//    invitation1.invitingDate = @"2016-09-28T16:31:36.881Z";
//    [self.server addInvitation:invitation1];
//
//    // Now launch application.
//    [self launchApplication];
//    
//    [NSThread sleepForTimeInterval:2.0f];
//    
//  
//    
//    [self.app.tabBars.buttons[@"Contacts"] tap];
//    
//    [self.app.buttons[@"Invitations"] tap];
//    
//    
//    // Our local contact will be now a rainbow contact with Pending button
//
//    XCTAssert(self.app.tables.staticTexts[@"Sent"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"Sent"].isHeader);
//    
//    XCTAssert(self.app.tables.staticTexts[@"gerald@petit.com"].exists);
//   // XCTAssert(self.app.tables.buttons[@"Pending"].exists);
//
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_777";
//    contact1.jid = @"gerald@domain.com";
//    contact1.loginEmail = @"gerald@petit.com";
//    contact1.firstname = @"Gerald";
//    contact1.lastname = @"Petit";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    contact1.companyId = [self.server myCompanyID];
//    [self.server addContact:contact1];
//    
//    invitation1.invitedUserId = @"rainbow_777";
//    invitation1.status = @"accepted";
//    [self.server updateInvitation:invitation1];
// 
//    
//    [NSThread sleepForTimeInterval:30.0f];
//    
//    XCTAssertEqual([self.app.tables.cells count], 0);
//
//    [self.app.tabBars.buttons[@"Contacts"] tap];
//
//    [self.app.buttons[@"My network"] tap];
//    
//     [NSThread sleepForTimeInterval:2.0f];
//    
//    
//    XCTAssert(self.app.tables.staticTexts[@"P"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"P"].isHeader);
//
//    XCTAssert(self.app.tables.staticTexts[@"Gerald Petit"].exists);
//    // vCard will be visible only if in my company (temp situation ?)
//    //XCTAssert(NO);
//}
//
//// TODO: what is roster invitation
//-(void) test1RosterInvitationAnd1APIInvitation {
//    
//    // Marcel is a roster invitation
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.hasSubscribe = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    
//    ContactAPI *contact2 = [ContactAPI new];
//    contact2.ID = @"rainbow_456";
//    contact2.jid = @"thierry@domain.com";
//    contact2.loginEmail = @"thierry@renard.com";
//    contact2.firstname = @"Thierry";
//    contact2.lastname = @"Renard";
//    contact2.inRoster = NO;
//    contact2.presence = @"Online";
//    [self.server addContact:contact2];
//    
//    InvitationAPI *invitation1 = [InvitationAPI new];
//    invitation1.received = YES;
//    invitation1.invitingUserId = @"rainbow_456";
//    invitation1.invitingUserEmail = @"marcel@dupont.com";
//    invitation1.status = @"pending";
//    invitation1.invitingDate = @"2016-09-28T16:31:36.881Z";
//    [self.server addInvitation:invitation1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [NSThread sleepForTimeInterval:2.0f];
//    [self.app.tabBars.buttons[@"Contacts"] tap];
//    
//  
//    [self.app.buttons[@"Invitations"] tap];
//    
//    [NSThread sleepForTimeInterval:2.0f];
//    
//    // Check if invitation section is present
//    XCTAssert(self.app.tables.staticTexts[@"Received"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"Received"].isHeader);
//    
//    
////    XCTAssertEqual([self.app.tables.cells count], 2);
////    XCTAssert(self.app.tables.staticTexts[@"Marcel DUPONT"].exists);
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Thierry Renard"];
//    XCTAssertNotNil(cell1);
//    XCTAssert(cell1.staticTexts[@"Thierry Renard"].exists);
//   
//    [NSThread sleepForTimeInterval:2.0f];
//    
//    //XCTAssert(NO);
//}
//
//
//@end
