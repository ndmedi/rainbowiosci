///*
// * Rainbow
// *
// * Copyright (c) 2016, ALE International
// * All rights reserved.
// *
// * ALE International Proprietary Information
// *
// * Contains proprietary/trade secret information which is the property of
// * ALE International and must not be made available to, or copied or used by
// * anyone outside ALE International without its written authorization
// *
// * Not to be disclosed or used except in accordance with applicable agreements.
// */
//
//#import <Foundation/Foundation.h>
//#import "RainbowUITests.h"
//#import <RainbowUnitTestFakeServer/ConversationAPI.h>
//
//
//@interface MessagesUITests : RainbowUITests
//@end
//
//
//@implementation MessagesUITests
//
//// TODO : how to get elements in cell
//-(void) testMamMessages {
//    
//    // Prepare our initial state, with 1 conv with 5 messages
//    
//    for (int i = 0; i < 100; i++) {
//        if(i %5 !=0){
//            MessageAPI *message1 = [MessageAPI new];
//            message1.from_jid = @"marcel@domain.com";
//            message1.to_jid = [self.server myJID];
//            message1.date = [NSString stringWithFormat:@"2017-11-07T10:10:11.%03uZ", i];
//            message1.body = [NSString stringWithFormat:@"Rcv msg id : %i", i];
//            message1.read = NO;
//            message1.received = NO;
//            [self.server addMessage:message1];
//        } else {
//            MessageAPI *messageSent = [MessageAPI new];
//            messageSent.from_jid = [self.server myJID];
//            messageSent.to_jid = @"marcel@domain.com";
//            messageSent.date = [NSString stringWithFormat:@"2017-11-07T10:10:11.%03uZ", i];
//            messageSent.body = [NSString stringWithFormat:@"Sent msg id : %i", i];
//            messageSent.read = NO;
//            messageSent.received = NO;
//            [self.server addMessage:messageSent];
//        }
//    }
//    
//   
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(0);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//
//   
//
//    // Wait a little for message loading from server
//    [NSThread sleepForTimeInterval:2.0f];
//    
//    // We are already on the Conversation view
//    
//    [self.app.tables.cells.staticTexts[@"Marcel Dupont"] tap];
//    
//  
//    [NSThread sleepForTimeInterval:10.0f];
//
//    XCUIElementQuery *cell1 = [self.app.collectionViews.cells containingType:XCUIElementTypeAny identifier:@"Alice: Sent msg id : 50"];
//    XCTAssertNotNil(cell1);
////    XCTAssert(cell1.staticTexts[@"Alice: Sent msg id : 50"].hittable);
////
//
//
//    XCTAssert([cell1.textViews[@"Message"].value isEqualToString:@"Alice: Sent msg id : 50"]);
////    XCTAssert([[cells elementBoundByIndex:1].staticTexts[@"Date"].value isEqualToString:@"02:30 PM"]);
////
////    XCTAssert([[cells elementBoundByIndex:2].textViews[@"Message"].value isEqualToString:@"msg 2"]);
////    XCTAssert([[cells elementBoundByIndex:2].staticTexts[@"Date"].value isEqualToString:@"02:30 PM"]);
////
////    XCTAssert([[cells elementBoundByIndex:3].textViews[@"Message"].value isEqualToString:@"msg 3"]);
////    XCTAssert([[cells elementBoundByIndex:3].staticTexts[@"Date"].value isEqualToString:@"02:30 PM"]);
////
////    XCTAssert([[cells elementBoundByIndex:4].textViews[@"Message"].value isEqualToString:@"msg 4"]);
////    XCTAssert([[cells elementBoundByIndex:4].staticTexts[@"Date"].value isEqualToString:@"02:30 PM"]);
//    
//    //XCTAssert(NO);
//}
//
//
//-(void) testSendMessage {
//    
//    // Prepare our initial state, with 1 conv
//    /*MessageAPI *message1 = [MessageAPI new];
//     message1.from_jid = @"marcel@domain.com";
//     message1.to_jid = [self.server myJID];
//     message1.body = [NSString stringWithFormat:@"msg %u", arc4random()];
//     message1.date = @"2016-11-11T11:11:11.000Z";
//     message1.read = NO;
//     message1.received = NO;
//     [self.server addMessage:message1];*/
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(0);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    [NSThread sleepForTimeInterval:2.0f];
//    // We are already on the Conversation view
//
//    
//    [self.app.tables/*@START_MENU_TOKEN@*/.staticTexts[@"Marcel Dupont"]/*[[".cells.staticTexts[@\"Marcel Dupont\"]",".staticTexts[@\"Marcel Dupont\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
//    
//    
//    NSString *newMsg = [NSString stringWithFormat:@"%u", arc4random()];
//    
//    [self.app.toolbars.textViews[@"New Message"] tap];
//    [self.app.toolbars.textViews[@"New Message"] typeText:newMsg];
//    [self.app.toolbars.buttons[@"Send"] tap];
//    
//    NSLog(@"AAA %@", [self.app.collectionViews.cells debugDescription]);
//    
//    //XCTAssert(NO);
//}
//
//@end
