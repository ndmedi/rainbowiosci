/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "RainbowUITests.h"
#import <RainbowUnitTestFakeServer/ContactAPI.h>


@interface ContactsUITests : RainbowUITests
@end


@implementation ContactsUITests

-(void) testRoster {
    
    // Add 2 contacts in my roster
    ContactAPI *contact1 = [ContactAPI new];
    contact1.ID = @"rainbow_123";
    contact1.jid = @"marcel@domain.com";
    contact1.loginEmail = @"marcel@dupont.com";
    contact1.firstname = @"Marcel";
    contact1.lastname = @"Dupont";
    contact1.inRoster = YES;
    contact1.isPending = NO;
    contact1.presence = @"Online";
    [self.server addContact:contact1];
    
    ContactAPI *contact2 = [ContactAPI new];
    contact2.ID = @"rainbow_456";
    contact2.jid = @"josette@domain.com";
    contact2.loginEmail = @"josette@moulin.com";
    contact2.firstname = @"Josette";
    contact2.lastname = @"Moulin";
    contact2.inRoster = YES;
    contact2.isPending = NO;
    contact2.presence = @"Away";
    [self.server addContact:contact2];
    
    // Now launch application.
    [self launchApplication];
    [NSThread sleepForTimeInterval:6];
    [self.app.tabBars.buttons[@"Contacts"] tap];

    [self.app.buttons[@"My network"] tap];
    
    XCTAssertEqual([self.app.tables.cells count], 0);
     NSLog(@"SampleUILog for testing !");

//    XCTAssert(self.app.tables.staticTexts[@"D"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"D"].isHeader);
//
//    XCUIElement *cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Marcel Dupont"].exists);
//    XCTAssert([[cell1.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Online"]);
//
//
//    XCTAssert(self.app.tables.staticTexts[@"M"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"M"].isHeader);
//
//    XCUIElement *cell2 = [self.app.tables.cells elementBoundByIndex:1];
//    XCTAssert(cell2.staticTexts[@"Josette Moulin"].exists);
//    XCTAssert([[cell2.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Away"]);
    
    //XCTAssert(NO);
}

@end
