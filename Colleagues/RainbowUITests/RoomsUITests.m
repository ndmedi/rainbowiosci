///*
// * Rainbow
// *
// * Copyright (c) 2016, ALE International
// * All rights reserved.
// *
// * ALE International Proprietary Information
// *
// * Contains proprietary/trade secret information which is the property of
// * ALE International and must not be made available to, or copied or used by
// * anyone outside ALE International without its written authorization
// *
// * Not to be disclosed or used except in accordance with applicable agreements.
// */
//
//#import <Foundation/Foundation.h>
//#import "RainbowUITests.h"
//#import <RainbowUnitTestFakeServer/ContactAPI.h>
//#import <RainbowUnitTestFakeServer/RoomAPI.h>
//
//@interface RoomsUITests : RainbowUITests
//@end
//
//
//@implementation RoomsUITests
//
//-(void) test1RoomAsModerator {
//    
//    // Add 1 room with 3 persons (plus me as creator)
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    ContactAPI *contact2 = [ContactAPI new];
//    contact2.ID = @"rainbow_456";
//    contact2.jid = @"micheline@domain.com";
//    contact2.loginEmail = @"micheline@dupre.com";
//    contact2.firstname = @"Micheline";
//    contact2.lastname = @"Dupre";
//    contact2.inRoster = YES;
//    contact2.isPending = NO;
//    contact2.presence = @"Away";
//    [self.server addContact:contact2];
//    
//    ContactAPI *contact3 = [ContactAPI new];
//    contact3.ID = @"rainbow_789";
//    contact3.jid = @"thierry@domain.com";
//    contact3.loginEmail = @"thierry@marchand.com";
//    contact3.firstname = @"Thierry";
//    contact3.lastname = @"Marchand";
//    contact3.inRoster = YES;
//    contact3.isPending = NO;
//    contact3.presence = @"Do not disturb";
//    [self.server addContact:contact3];
//    
//    RoomAPI *room1 = [RoomAPI new];
//    room1.ID = @"rainbow_room_789";
//    room1.jid = @"room_equipe@domain.com";
//    room1.name = @"Room equipe dev";
//    room1.topic = @"Topic de room";
//    room1.visibility = @"private";
//    room1.creationDate = @"2016-03-28T18:27:10.970Z";
//    room1.creator = [self.server myRainbowID];
//    [room1 addUser:[self.server myRainbowID] privilege:@"moderator" status:@"accepted"];
//    [room1 addUser:@"rainbow_123" privilege:@"user" status:@"accepted"];
//    [room1 addUser:@"rainbow_456" privilege:@"user" status:@"invited"];
//    [room1 addUser:@"rainbow_789" privilege:@"user" status:@"unsubscribed"];
//    [self.server addRoom:room1];
//    
//    // Now launch application.
//    [self launchApplication];
//   
//    [self.app.tabBars.buttons[@"Bubbles"] tap];
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    XCUIElement *cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Room equipe dev"].exists);
//    XCTAssert(cell1.staticTexts[@"Topic de room"].exists);
//    
//    // Go to details
//    [cell1.buttons[@"Vcard"] tap];
//    
//    
//    XCTAssert(self.app.tables.staticTexts[@"Organizer"].exists);
//    XCTAssert(self.app.tables.staticTexts[@"Organizer"].isHeader);
//    
//
//    XCUIElementQuery *cell2 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Alice Tester"];
//    
//   
//    XCTAssert(cell2.staticTexts[@"Alice Tester"].exists);
//    XCTAssert([[cell2.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Online mobile"]);
//    
//    [NSThread sleepForTimeInterval:10.f];
//
//    XCTAssert(self.app.buttons[@"Add attendees"].exists);
//    
//    
//    XCUIElementQuery *cell3 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    
//    
//    XCTAssert(cell3.staticTexts[@"Marcel Dupont"].exists);
//    XCTAssert([[cell3.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Online"]);
//    
//    
//    XCUIElementQuery *cell4 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Micheline Dupre"];
//    
//    
//    XCTAssert(cell4.staticTexts[@"Micheline Dupre"].exists);
//    XCTAssert([[cell4.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Away"]);
//    
//    //XCTAssert(NO);
//}
//
//
//-(void) testUpdateMyRoomTopic {
//    
//    // Add 1 room with 2 persons (plus me as creator)
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    ContactAPI *contact2 = [ContactAPI new];
//    contact2.ID = @"rainbow_456";
//    contact2.jid = @"micheline@domain.com";
//    contact2.loginEmail = @"micheline@dupre.com";
//    contact2.firstname = @"Micheline";
//    contact2.lastname = @"Dupré";
//    contact2.inRoster = YES;
//    contact2.isPending = NO;
//    contact2.presence = @"Away";
//    [self.server addContact:contact2];
//    
//    RoomAPI *room1 = [RoomAPI new];
//    room1.ID = @"rainbow_room_789";
//    room1.jid = @"room_equipe@domain.com";
//    room1.name = @"Room equipe dev";
//    room1.topic = @"Topic de room";
//    room1.visibility = @"private";
//    room1.creationDate = @"2016-03-28T18:27:10.970Z";
//    room1.creator = [self.server myRainbowID];
//    [room1 addUser:[self.server myRainbowID] privilege:@"moderator" status:@"accepted"];
//    [room1 addUser:@"rainbow_123" privilege:@"user" status:@"accepted"];
//    [room1 addUser:@"rainbow_456" privilege:@"user" status:@"invited"];
//    [self.server addRoom:room1];
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [NSThread sleepForTimeInterval:2.f];
//    [self.app.tabBars.buttons[@"Bubbles"] tap];
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    XCUIElement *cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Room equipe dev"].exists);
//    XCTAssert(cell1.staticTexts[@"Topic de room"].exists);
//    
//    // Go to details
//    [cell1.buttons[@"Vcard"] tap];
//    
//    [self.app.navigationBars[@"Bubble details"].buttons[@"Modify"] tap];
//    
//    [self.app.sheets.buttons[@"Change bubble topic"] tap];
//    
//    // Wait for the alert to show up
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == true"] evaluatedWithObject:self.app.alerts[@"Enter new bubble topic"] handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    [self.app.alerts[@"Enter new bubble topic"] typeText:@"New topic"];
//    
//    [self.app.alerts[@"Enter new bubble topic"].buttons[@"OK"] tap];
//    
//    // I dont know how to access the parallax view, so check the new topic against the Rooms list
//    // Go back
//    [self.app.navigationBars[@"Bubble details"].buttons[@"Bubbles"] tap];
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Room equipe dev"].exists);
//    XCTAssert(cell1.staticTexts[@"New topic"].exists);
//    
//    //XCTAssert(NO);
//}
//
//-(void) testUpdateMyRoomName {
//    
//    // Add 1 room with 2 persons (plus me as creator)
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    ContactAPI *contact2 = [ContactAPI new];
//    contact2.ID = @"rainbow_456";
//    contact2.jid = @"micheline@domain.com";
//    contact2.loginEmail = @"micheline@dupre.com";
//    contact2.firstname = @"Micheline";
//    contact2.lastname = @"Dupré";
//    contact2.inRoster = YES;
//    contact2.isPending = NO;
//    contact2.presence = @"Away";
//    [self.server addContact:contact2];
//    
//    RoomAPI *room1 = [RoomAPI new];
//    room1.ID = @"rainbow_room_789";
//    room1.jid = @"room_equipe@domain.com";
//    room1.name = @"Room equipe dev";
//    room1.topic = @"Topic de room";
//    room1.visibility = @"private";
//    room1.creationDate = @"2016-03-28T18:27:10.970Z";
//    room1.creator = [self.server myRainbowID];
//    [room1 addUser:[self.server myRainbowID] privilege:@"moderator" status:@"accepted"];
//    [room1 addUser:@"rainbow_123" privilege:@"user" status:@"accepted"];
//    [room1 addUser:@"rainbow_456" privilege:@"user" status:@"invited"];
//    [self.server addRoom:room1];
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [self.app.tabBars.buttons[@"Bubbles"] tap];
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    XCUIElement *cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Room equipe dev"].exists);
//    XCTAssert(cell1.staticTexts[@"Topic de room"].exists);
//    
//    // Go to details
//    [cell1.buttons[@"Vcard"] tap];
//    
//    [self.app.navigationBars[@"Bubble details"].buttons[@"Modify"] tap];
//    
//    [self.app.sheets.buttons[@"Change bubble name"] tap];
//    
//    // Wait for the alert to show up
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == true"] evaluatedWithObject:self.app.alerts[@"Enter new bubble name"] handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    [self.app.alerts[@"Enter new bubble name"] typeText:@"New name"];
//    
//    [self.app.alerts[@"Enter new bubble name"].buttons[@"OK"] tap];
//    
//    // I dont know how to access the parallax view, so check the new topic against the Rooms list
//    // Go back
//    [self.app.navigationBars[@"Bubble details"].buttons[@"Bubbles"] tap];
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"New name"].exists);
//    XCTAssert(cell1.staticTexts[@"Topic de room"].exists);
//    
//    //XCTAssert(NO);
//}
//
//-(void) test1RoomAsModerator1RoomAsParticipant1RoomArchivedInCorrectTab {
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    ContactAPI *contact2 = [ContactAPI new];
//    contact2.ID = @"rainbow_456";
//    contact2.jid = @"micheline@domain.com";
//    contact2.loginEmail = @"micheline@dupre.com";
//    contact2.firstname = @"Micheline";
//    contact2.lastname = @"Dupré";
//    contact2.inRoster = YES;
//    contact2.isPending = NO;
//    contact2.presence = @"Away";
//    [self.server addContact:contact2];
//    
//    ContactAPI *contact3 = [ContactAPI new];
//    contact3.ID = @"rainbow_789";
//    contact3.jid = @"thierry@domain.com";
//    contact3.loginEmail = @"thierry@marchand.com";
//    contact3.firstname = @"Thierry";
//    contact3.lastname = @"Marchand";
//    contact3.inRoster = YES;
//    contact3.isPending = NO;
//    contact3.presence = @"Do not disturb";
//    [self.server addContact:contact3];
//    
//    // Add 1 room with 3 persons (plus me as creator)
//    RoomAPI *room1 = [RoomAPI new];
//    room1.ID = @"rainbow_room_789";
//    room1.jid = @"room_equipe@domain.com";
//    room1.name = @"Room equipe dev";
//    room1.topic = @"Topic de room";
//    room1.visibility = @"private";
//    room1.creationDate = @"2016-03-28T18:27:10.970Z";
//    room1.creator = [self.server myRainbowID];
//    [room1 addUser:[self.server myRainbowID] privilege:@"moderator" status:@"accepted"];
//    [room1 addUser:@"rainbow_123" privilege:@"user" status:@"accepted"];
//    [room1 addUser:@"rainbow_456" privilege:@"user" status:@"invited"];
//    [room1 addUser:@"rainbow_789" privilege:@"user" status:@"unsubscribed"];
//    [self.server addRoom:room1];
//    
//    // Add 1 room with 3 persons (plus me as participant)
//    RoomAPI *room2 = [RoomAPI new];
//    room2.ID = @"rainbow_room_000";
//    room2.jid = @"room_tennis@domain.com";
//    room2.name = @"Room tennis";
//    room2.topic = @"Concours tennis";
//    room2.visibility = @"private";
//    room2.creationDate = @"2016-03-27T18:27:10.970Z";
//    room2.creator = @"rainbow_123"; // Marcel is the owner
//    [room2 addUser:[self.server myRainbowID] privilege:@"user" status:@"accepted"];
//    [room2 addUser:@"rainbow_123" privilege:@"moderator" status:@"accepted"];
//    [room2 addUser:@"rainbow_456" privilege:@"user" status:@"invited"];
//    [room2 addUser:@"rainbow_789" privilege:@"user" status:@"unsubscribed"];
//    [self.server addRoom:room2];
//    
//    // Add 1 room with 3 persons (plus me as participant unsubscribed)
//    RoomAPI *room3 = [RoomAPI new];
//    room3.ID = @"rainbow_room_666";
//    room3.jid = @"room_manger@domain.com";
//    room3.name = @"Room manger";
//    room3.topic = @"Pour allez manger";
//    room3.visibility = @"private";
//    room3.creationDate = @"2016-03-27T18:27:10.970Z";
//    room3.creator = @"rainbow_123"; // Marcel is the owner
//    [room3 addUser:[self.server myRainbowID] privilege:@"user" status:@"unsubscribed"];
//    [room3 addUser:@"rainbow_123" privilege:@"moderator" status:@"accepted"];
//    [room3 addUser:@"rainbow_456" privilege:@"user" status:@"invited"];
//    [room3 addUser:@"rainbow_789" privilege:@"user" status:@"unsubscribed"];
//    [self.server addRoom:room3];
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [self.app.tabBars.buttons[@"Bubbles"] tap];
//    
//    // In All bubbles tab, we have the 2 bubbles
//    XCTAssertEqual([self.app.tables.cells count], 2);
//    
//    XCUIElement *cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Room equipe dev"].exists);
//    XCTAssert(cell1.staticTexts[@"Topic de room"].exists);
//    
//    cell1 = [self.app.tables.cells elementBoundByIndex:1];
//    XCTAssert(cell1.staticTexts[@"Room tennis"].exists);
//    XCTAssert(cell1.staticTexts[@"Concours tennis"].exists);
//    
//    
//    // In MY bubbles tab, we have only the one I own
//    [self.app.tables.buttons[@"My bubbles"] tap];
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Room equipe dev"].exists);
//    XCTAssert(cell1.staticTexts[@"Topic de room"].exists);
//    
//    
//    // 1 archived bubble
//    [self.app.tables.buttons[@"Archives"] tap];
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Room manger"].exists);
//    XCTAssert(cell1.staticTexts[@"Pour allez manger"].exists);
//    
//    //XCTAssert(NO);
//}
//
//-(void) testAddAttendeeToMyRoom {
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    // Add 1 room with only me
//    RoomAPI *room1 = [RoomAPI new];
//    room1.ID = @"rainbow_room_789";
//    room1.jid = @"room_equipe@domain.com";
//    room1.name = @"Room equipe dev";
//    room1.topic = @"Topic de room";
//    room1.visibility = @"private";
//    room1.creationDate = @"2016-03-28T18:27:10.970Z";
//    room1.creator = [self.server myRainbowID];
//    [room1 addUser:[self.server myRainbowID] privilege:@"moderator" status:@"accepted"];
//    [self.server addRoom:room1];
//    
//    // Now launch application.
//    [self launchApplication];
//    [NSThread sleepForTimeInterval:6.f];
//    [self.app.tabBars.buttons[@"Bubbles"] tap];
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    XCUIElement *cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Room equipe dev"].exists);
//    XCTAssert(cell1.staticTexts[@"Topic de room"].exists);
//    
//    // Go to details
//    [cell1.buttons[@"Vcard"] tap];
//    
//    [self.app.tables.buttons[@"Add attendees"] tap];
//    
//    // Marcel is in my roster, so by default presented in list
//    [self.app.tables.staticTexts[@"Marcel Dupont"] tap];
//    
//
//    [self.app.navigationBars[@"Add attendees"].buttons[@"Ok"] tap];
//    
//    
//    // SECTION 1 : ADMIN -> ME
//    XCUIElement *text = [self.app.tables.staticTexts elementBoundByIndex:0];
//    XCTAssert([text.label isEqualToString:@"Organizer"]);
//    XCTAssert(text.isHeader);
//    
//    cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Alice Tester"].exists);
//    XCTAssert([[cell1.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Online mobile"]);
//
//    
//   XCTAssert(self.app.buttons[@"Add attendees"].exists);
//    
//    //XCTAssert(NO);
//}
//
//-(void) testRemoveAttendeeFromMyRoom {
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    // Add 1 room with only me and marcel
//    RoomAPI *room1 = [RoomAPI new];
//    room1.ID = @"rainbow_room_789";
//    room1.jid = @"room_equipe@domain.com";
//    room1.name = @"Room equipe dev";
//    room1.topic = @"Topic de room";
//    room1.visibility = @"private";
//    room1.creationDate = @"2016-03-28T18:27:10.970Z";
//    room1.creator = [self.server myRainbowID];
//    [room1 addUser:[self.server myRainbowID] privilege:@"moderator" status:@"accepted"];
//    [room1 addUser:@"rainbow_123" privilege:@"user" status:@"accepted"];
//    [self.server addRoom:room1];
//    
//    // Now launch application.
//    [self launchApplication];
//   
//    [NSThread sleepForTimeInterval:6.f];
//    [self.app.tabBars.buttons[@"Bubbles"] tap];
//    
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    XCUIElement *cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Room equipe dev"].exists);
//    XCTAssert(cell1.staticTexts[@"Topic de room"].exists);
//    
//    // Go to details
//    [cell1.buttons[@"Vcard"] tap];
//    
//    // Swipe marcel to remove him
//    [self.app.tables.staticTexts[@"Marcel Dupont"] swipeLeft];
//
//     [[self.app.tables childrenMatchingType:XCUIElementTypeButton].element tap];
//    // Now Attendees section contains only the Add button
//    // and Marcel has gone to Away section
//    
//    // SECTION 1 : ADMIN -> ME
//    XCUIElement *text = [self.app.tables.staticTexts elementBoundByIndex:0];
//    XCTAssert([text.label isEqualToString:@"Organizer"]);
//    XCTAssert(text.isHeader);
//    
//    cell1 = [self.app.tables.cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Alice Tester"].exists);
//    XCTAssert([[cell1.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Online mobile"]);
//    
// 
//    XCTAssert(self.app.buttons[@"Add attendees"].exists);
//    
//    
//    //XCTAssert(NO);
//}
//
//@end
