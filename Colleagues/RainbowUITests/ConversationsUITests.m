///*
// * Rainbow
// *
// * Copyright (c) 2016, ALE International
// * All rights reserved.
// *
// * ALE International Proprietary Information
// *
// * Contains proprietary/trade secret information which is the property of
// * ALE International and must not be made available to, or copied or used by
// * anyone outside ALE International without its written authorization
// *
// * Not to be disclosed or used except in accordance with applicable agreements.
// */
//
//#import <Foundation/Foundation.h>
//#import "RainbowUITests.h"
//#import <RainbowUnitTestFakeServer/ConversationAPI.h>
//
//@interface ConversationsUITests : RainbowUITests
//@end
//
//
//@implementation ConversationsUITests
//
//-(void) testConversationsList {
//    
//    // Prepare our initial state, with 3 conversations.
//    // Conversation 1 with Marcel which is in my roster and Online.
//    // Conversation 2 with Micheline which is in my roster but pending
//    // Conversation 3 Room with Marcel and Micheline, conv is muted.
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(1);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ConversationAPI *conversation2 = [ConversationAPI new];
//    conversation2.peerID = @"rainbow_456";
//    conversation2.type = @"user";
//    conversation2.jid = @"micheline@domain.com";
//    conversation2.lastMessage = @"Blablabla";
//    conversation2.lastMessageDate = @"2016-12-10T10:11:03.000Z";
//    conversation2.unread = @(2);
//    conversation2.mute = NO;
//    [self.server addConversation:conversation2];
//    
//    ConversationAPI *conversation3 = [ConversationAPI new];
//    conversation3.peerID = @"rainbow_room_789";
//    conversation3.type = @"room";
//    conversation3.jid = @"room_equipe@domain.com";
//    conversation3.lastMessage = @"Je fais des tests";
//    conversation3.lastMessageDate = @"2016-11-01T09:00:00.000Z";
//    conversation3.unread = @(0);
//    conversation3.mute = YES;
//    [self.server addConversation:conversation3];
//    
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    ContactAPI *contact2 = [ContactAPI new];
//    contact2.ID = @"rainbow_456";
//    contact2.jid = @"micheline@domain.com";
//    contact2.loginEmail = @"micheline@dupre.com";
//    contact2.firstname = @"Micheline";
//    contact2.lastname = @"Dupré";
//    contact2.inRoster = YES;
//    contact2.isPending = YES;
//    contact2.presence = @"Away";
//    [self.server addContact:contact2];
//    
//    RoomAPI *room1 = [RoomAPI new];
//    room1.ID = @"rainbow_room_789";
//    room1.jid = @"room_equipe@domain.com";
//    room1.name = @"Room equipe dev";
//    room1.topic = @"Topic de room";
//    room1.visibility = @"private";
//    room1.creationDate = @"2016-03-28T18:27:10.970Z";
//    room1.creator = [self.server myRainbowID];
//    [room1 addUser:[self.server myRainbowID] privilege:@"moderator" status:@"accepted"];
//    [room1 addUser:@"rainbow_123" privilege:@"user" status:@"accepted"];
//    [room1 addUser:@"rainbow_456" privilege:@"user" status:@"accepted"];
//    [self.server addRoom:room1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    [NSThread sleepForTimeInterval:1.f];
//    // We are already on the Conversation view
//    // But check the unread badge value
//    XCTAssertEqualObjects([self.app.tabBars.buttons[@"Conversations"] value],@"3 items",@"part 1 failed");
//    
//    // We have 3 conversations
//    XCTAssertEqual([self.app.tables.cells count], 3);
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    XCTAssertNotNil(cell1);
//    XCTAssert(cell1.staticTexts[@"Marcel Dupont"].exists);
//    XCTAssert(cell1.staticTexts[@"Hello There"].exists);
//    XCTAssert(cell1.staticTexts[@"Oct 11, 2016"].exists);
//    XCTAssert([[cell1.otherElements[@"badgeText"] value] isEqualToString:@"1"]);
//    XCTAssert([[cell1.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Online"]);
//    
//    // Swipe the conversation to check the presented buttons (this one it NOT muted)
//    [cell1.staticTexts[@"Marcel Dupont"] swipeLeft];
//    XCTAssertFalse(cell1.buttons[@"Alert"].exists);
//    XCTAssertTrue([cell1 containingType:XCUIElementTypeButton identifier:@"Colse"]);
//    XCTAssertFalse(cell1.buttons[@"Read"].exists);
//    XCTAssertTrue([cell1 containingType:XCUIElementTypeButton identifier:@"Silence"]);
//    [cell1.staticTexts[@"Marcel Dupont"] swipeRight];
//    
//    XCUIElementQuery *cell2 = [self.app.tables.cells containingType:XCUIElementTypeAny identifier:@"Micheline Dupré"];
//    
//    XCTAssert(cell2.staticTexts[@"Micheline Dupré"].exists);
//    XCTAssert(cell2.staticTexts[@"Blablabla"].exists);
//    XCTAssert(cell2.staticTexts[@"Dec 10, 2016"].exists);
//    XCTAssert([[cell2.otherElements[@"badgeText"] value] isEqualToString:@"2"]);
//    XCTAssert(cell2.images[@"AvatarPresenceIcon"].exists);
//    XCTAssertTrue([cell2 containingType:XCUIElementTypeImage identifier:@"AvatarPresenceIcon"]);
//    //XCTAssert([[cell2.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Pending"]);
//
//    // Swipe the conversation to check the presented buttons (this one it NOT muted)
//    [cell2.staticTexts[@"Micheline Dupré"] swipeLeft];
//    XCTAssertFalse(cell2.buttons[@"Alert"].exists);
//    XCTAssertTrue([cell2 containingType:XCUIElementTypeButton identifier:@"Colse"]);
//    XCTAssertTrue([cell1 containingType:XCUIElementTypeButton identifier:@"Silence"]);
//    [cell2.staticTexts[@"Micheline Dupré"] swipeRight];
//    
//    
//    XCUIElementQuery *cell3 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Room equipe dev"];
//    XCTAssertNotNil(cell3);
//    XCTAssert(cell3.staticTexts[@"Room equipe dev"].exists);
//    XCTAssert(cell3.staticTexts[@"Je fais des tests"].exists);
//    XCTAssert(cell3.staticTexts[@"Nov 1, 2016"].exists);
//    XCTAssertTrue(cell3.otherElements[@"badgeText"].exists); //  is the badgeText realy exsit?
//   // XCTAssert([[cell3.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Rainbow user"]);
//    
//    // Swipe the conversation to check the presented buttons (this on IS muted)
//    [cell3.staticTexts[@"Room equipe dev"] swipeLeft];
//
//    XCTAssertTrue([cell3 containingType:XCUIElementTypeButton identifier:@"Colse"]);
//    XCTAssertTrue([cell3 containingType:XCUIElementTypeButton identifier:@"Alert"]);
//    XCTAssertTrue([cell3 containingType:XCUIElementTypeButton identifier:@"Archive"]);
//    XCTAssertFalse(cell3.buttons[@"Read"].exists);
//    XCTAssertFalse(cell3.buttons[@"Silence"].exists);
//    [cell3.staticTexts[@"Room equipe dev"] swipeRight];
//   
//    
//   
//    
//}
//
//
//-(void) testSearchConversation {
//    
//    // Prepare our initial state, with 1 conv
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(1);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [NSThread sleepForTimeInterval:16.0f];
//    
//    // tap on search button
//
//    XCUIElement *searchBarStaticText = self.app.navigationBars[@"Conversations"].staticTexts[@"Conversations"];
//
//    [self.app tapElementAndWaitForKeyboardToAppear:searchBarStaticText];
//
//    // TODO: this is caused thread **ssertion Failure: ConversationsUITests.m:208: Neither element nor any descendant has keyboard focus. Element:Attributes: Other**
//
//    [self.app typeText:@"arc"];
//    
//    // Wait for results
//    // We should have 2 results : The conversation with Marcel and the Marcel contact
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"count == 2"] evaluatedWithObject:self.app.tables[@"Search results"].cells handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    
//    // My conversation section
//    XCTAssert(self.app.tables[@"Search results"].staticTexts[@"My conversations"].exists);
//    XCTAssert(self.app.tables[@"Search results"].staticTexts[@"My conversations"].isHeader);
//
//    // First is conversation (with last message)
//    XCUIElement *cell1 = [self.app.tables[@"Search results"].cells elementBoundByIndex:0];
//    XCTAssert(cell1.staticTexts[@"Marcel Dupont"].exists);
//    XCTAssert(cell1.staticTexts[@"Hello There"].exists);
//    XCTAssert([[cell1.otherElements[@"badgeText"] value] isEqualToString:@"1"]);
//    
//    // Rainbow directory section
//    XCTAssert(self.app.tables[@"Search results"].staticTexts[@"Rainbow directory"].exists);
//    XCTAssert(self.app.tables[@"Search results"].staticTexts[@"Rainbow directory"].isHeader);
//    
//    // Second is contact, with vcard button
//    XCUIElement *cell2 = [self.app.tables[@"Search results"].cells elementBoundByIndex:1];
//    XCTAssert(cell2.staticTexts[@"Marcel Dupont"].exists);
//    XCTAssert(cell2.buttons[@"Vcard"].exists);
//    
//    //XCTAssert(NO);
//}
//
//-(void) testCreateConversationWithSearchedContactNotInRoster {
//    
//    // Prepare our initial state, with 0 conv
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    // We are already on the Conversation view
//    
//    // We have 0 conversations
//    XCTAssertEqual([self.app.tables.cells count], 0);
//    
//    // create a conversation
//    [self.app.navigationBars[@"Conversations"].staticTexts[@"Conversations"] tap];
//    
//    // search for marcel
//    // TODO: search here not work : fail
//    [self.app typeText:@"Marcel"];
//    
//    // Wait for search results to be populated
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"count > 0"] evaluatedWithObject:self.app.tables[@"Search results"].cells handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    // Make sure we have only 1 result !
//    XCTAssertEqual([self.app.tables[@"Search results"].cells count], 1);
//    
//    // Open conv with this contact
//    [self.app.tables[@"Search results"].staticTexts[@"Marcel Dupont"] tap];
//    
//    
//    // Check we are on the message view with the correct contact.
//    // Name in top navigation bar
//    XCTAssert(self.app.navigationBars[@"Marcel Dupont"].exists);
//    
//    // Button back to conv exists
//    XCTAssert(self.app.navigationBars[@"Marcel Dupont"].buttons[@"Conversations"].exists);
//    
//    XCTAssert(self.app.toolbars.textViews[@"New Message"].exists);
//    XCTAssert(self.app.toolbars.buttons[@"Send"].exists);
//    
//    //XCTAssert(NO);
//}
//
//
//-(void) testDeleteConversation {
//    
//    // Prepare our initial state, with 1 conv
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(1);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    // We are already on the Conversation view
//    
//    // We have 1 conversations
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    
//    [NSThread sleepForTimeInterval:0.2f];
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    [cell1.staticTexts[@"Marcel Dupont"] swipeLeft];
//   
//
//      [[[self.app.tables childrenMatchingType:XCUIElementTypeButton] elementBoundByIndex:1] tap];
//    
//    // We now have 0 conversation
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"count == 0"] evaluatedWithObject:self.app.tables.cells handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    //XCTAssert(NO);
//}
//
//
//-(void) testDeleteConversationEvent {
//    
//    // Prepare our initial state, with 1 conv
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(1);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    // We are already on the Conversation view
//    
//    // We have 1 conversations
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    
//    [NSThread sleepForTimeInterval:0.2f];
//    
//    // Add a new conversation while app is connected.
//    // This will send a XMPP event
//    [self.server deleteConversation:conversation1];
//   
//    
//    // We now have 0 conversation
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"count == 0"] evaluatedWithObject:self.app.tables.cells handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    //XCTAssert(NO);
//}
//
//
//
//
//-(void) testConversationCreatedOnNewMessage {
//    
//    // Prepare our initial state, with 0 conv
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    // We are already on the Conversation view
//    
//    // We have 0 conversations
//    XCTAssertEqual([self.app.tables.cells count], 0);
//    
//    [NSThread sleepForTimeInterval:6.0f];
//    
//    // Add a new message while app is connected.
//    MessageAPI *message1 = [MessageAPI new];
//    message1.from_jid = @"marcel@domain.com";
//    message1.to_jid = [self.server myJID];
//    message1.body = @"New message sent";
//    
//    [self.server addMessage:message1];
//    
//    // We now have 1 new conversation, with 1 unread message
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"count > 0"] evaluatedWithObject:self.app.tables.cells handler:nil];
//    [self waitForExpectationsWithTimeout:15.0f handler:nil];
//    
//   
//    [NSThread sleepForTimeInterval:16.0f];
//
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    XCTAssertNotNil(cell1);
//    XCTAssert(cell1.staticTexts[@"Marcel Dupont"].exists);
//    XCTAssertTrue([cell1 containingType:XCUIElementTypeAny identifier:@"New message sent"]);
//    
//    XCTAssert([[cell1.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Rainbow user"]); // User is online, but I dont see its presence.
//    [cell1.staticTexts[@"Marcel Dupont"] tap];
//    [NSThread sleepForTimeInterval:6.0f];
//    //XCTAssert(NO);
//}
//
//
//-(void) testNewMessageInExistingConversation {
//    
//    // Prepare our initial state, with 1 conv
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(1);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    // We are already on the Conversation view
//    
//    // We have 1 conversations
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    [NSThread sleepForTimeInterval:0.2f];
//    
//    // Add a new message while app is connected.
//    MessageAPI *message1 = [MessageAPI new];
//    message1.from_jid = @"marcel@domain.com";
//    message1.to_jid = [self.server myJID];
//    message1.body = @"New message sent";
//    [self.server addMessage:message1];
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    XCTAssertNotNil(cell1);
//    
//    // Wait for unread badge to be = 2
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"value MATCHES %@", @"2"] evaluatedWithObject:cell1.otherElements[@"badgeText"] handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    XCTAssert(cell1.staticTexts[@"Marcel Dupont"].exists);
//    [NSThread sleepForTimeInterval:0.2f];
//    XCTAssert(cell1.staticTexts[@"Oct 11, 2016"].exists);
//    XCTAssert([[cell1.images[@"AvatarPresenceIcon"] value] isEqualToString:@"Rainbow user"]); // User is online, but I dont see its presence.
//    
//    XCTAssert([[self.app.tabBars.buttons[@"Conversations"] value] isEqualToString:@"2 items"]);
//    
//   
//    
//    [NSThread sleepForTimeInterval:0.2f];
//    //XCTAssert(NO);
//}
//
//// TODO : what is from resume mean ..
//-(void) testReceiveNewMessageAndDeleteConversationFromResume {
//    
//    // Prepare our initial state, with 1 conv
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(1);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    // We are already on the Conversation view
//    
//    // We have 1 conversations
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    [NSThread sleepForTimeInterval:0.2f];
//    
//    // Add a new message while app is connected.
//    MessageAPI *message1 = [MessageAPI new];
//    message1.from_jid = @"marcel@domain.com";
//    message1.to_jid = [self.server myJID];
//    message1.body = @"New message sent";
//    message1.receivedDate = @"2017-11-03T15:04:03.000Z";
//    message1.isPush = NO;
//    message1.isArchived = YES;
//    message1.archiveDate = @"2017-11-03T15:03:03.000Z";
//    message1.needAck = YES;
//    [self.server addMessage:message1];
//    
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    XCTAssert([[cell1.otherElements[@"badgeText"] value] isEqualToString:@"2"]);
//    [cell1.staticTexts[@"Marcel Dupont"] swipeLeft];
//    
//    
//    [[[self.app.tables childrenMatchingType:XCUIElementTypeButton] elementBoundByIndex:1] tap];
//     [self.server deleteConversation:conversation1];
//    
//    
//    
//    // Now simulate the reception of a delete conversation from another device
//    [NSThread sleepForTimeInterval:0.2f];
//    
//    
//    
//    [NSThread sleepForTimeInterval:10.0f];
//    
//    XCTAssertEqual([self.app.tables.cells count], 0);
//    
//    XCTAssertEqualObjects([self.app.tabBars.buttons[@"Conversations"] value],@"",@"part 1 failed");
//    
//    //XCTAssert(NO);
//}
//
//// TODO: mark as read not updated!
//
//-(void) testMarkAsReadConversation {
//    
//    // Prepare our initial state, with 1 conv
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(1);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = NO;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    // We are already on the Conversation view
//    
//    // We have 1 conversations
//    XCTAssertEqual([self.app.tables.cells count], 1);
//    
//    
//    [NSThread sleepForTimeInterval:0.2f];
//    
//    XCUIElementQuery *cell1 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Marcel Dupont"];
//    XCTAssert([[cell1.otherElements[@"badgeText"] value] isEqualToString:@"1"]);
//    [self.app.tables/*@START_MENU_TOKEN@*/.staticTexts[@"Marcel Dupont"]/*[[".cells.staticTexts[@\"Marcel Dupont\"]",".staticTexts[@\"Marcel Dupont\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
//   
//    
//    XCUIElement *newMessageTextView = self.app.toolbars.textViews[@"New Message"];
//    [newMessageTextView tap];
//    [newMessageTextView typeText:@"hello!"];
//  
//    
//    [self.app.navigationBars[@"Marcel Dupont"].buttons[@"Conversations"] tap];
//    [NSThread sleepForTimeInterval:20.0f];
//    // Wait for unread badge to be = 0
////    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == false"] evaluatedWithObject:cell1.otherElements[@"badgeText"] handler:nil];
////    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//     XCTAssertEqualObjects([self.app.tabBars.buttons[@"Conversations"] value],@"0 items",@"part 1 failed");
//    
//}
//
//// TODO: to be sure that is worked!
//
//-(void) testMuteEvent {
//    
//    // Prepare our initial state, with 3 conversations.
//    // Conversation 1 with Marcel which is in my roster and Online.
//    // Conversation 2 with Micheline which is in my roster but pending
//    // Conversation 3 Room with Marcel and Micheline, conv is muted.
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(1);
//    conversation1.mute = NO;
//    [self.server addConversation:conversation1];
//    
//    ConversationAPI *conversation2 = [ConversationAPI new];
//    conversation2.peerID = @"rainbow_456";
//    conversation2.type = @"user";
//    conversation2.jid = @"micheline@domain.com";
//    conversation2.lastMessage = @"Blablabla";
//    conversation2.lastMessageDate = @"2016-12-10T10:11:03.000Z";
//    conversation2.unread = @(2);
//    conversation2.mute = NO;
//    [self.server addConversation:conversation2];
//    
//    ConversationAPI *conversation3 = [ConversationAPI new];
//    conversation3.peerID = @"rainbow_room_789";
//    conversation3.type = @"room";
//    conversation3.jid = @"room_equipe@domain.com";
//    conversation3.lastMessage = @"Je fais des tests";
//    conversation3.lastMessageDate = @"2016-11-01T09:00:00.000Z";
//    conversation3.unread = @(0);
//    conversation3.mute = YES;
//    [self.server addConversation:conversation3];
//    
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    ContactAPI *contact2 = [ContactAPI new];
//    contact2.ID = @"rainbow_456";
//    contact2.jid = @"micheline@domain.com";
//    contact2.loginEmail = @"micheline@dupre.com";
//    contact2.firstname = @"Micheline";
//    contact2.lastname = @"Dupré";
//    contact2.inRoster = YES;
//    contact2.isPending = YES;
//    contact2.presence = @"Away";
//    [self.server addContact:contact2];
//    
//    RoomAPI *room1 = [RoomAPI new];
//    room1.ID = @"rainbow_room_789";
//    room1.jid = @"room_equipe@domain.com";
//    room1.name = @"Room equipe dev";
//    room1.topic = @"Topic de room";
//    room1.visibility = @"private";
//    room1.creationDate = @"2016-03-28T18:27:10.970Z";
//    room1.creator = [self.server myRainbowID];
//    [room1 addUser:[self.server myRainbowID] privilege:@"moderator" status:@"accepted"];
//    [room1 addUser:@"rainbow_123" privilege:@"user" status:@"accepted"];
//    [room1 addUser:@"rainbow_456" privilege:@"user" status:@"accepted"];
//    [self.server addRoom:room1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    [NSThread sleepForTimeInterval:1.f];
//   
//    XCUIElementQuery *cell3 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Room equipe dev"];
//    XCTAssertNotNil(cell3);
//  
//    
//    // Swipe the conversation to check the presented buttons (this on IS muted)
//  
//    [cell3.staticTexts[@"Room equipe dev"] swipeLeft];
//  
//    XCTAssertTrue([cell3 containingType:XCUIElementTypeButton identifier:@"Alert"]);
//    XCTAssertFalse(cell3.buttons[@"Silence"].exists);
//   
// 
//  
//    [[[self.app.tables childrenMatchingType:XCUIElementTypeButton] elementBoundByIndex:0] tap];
//    
//    [NSThread sleepForTimeInterval:5.f];
//
//    [cell3.staticTexts[@"Room equipe dev"] swipeLeft];
//
//    XCTAssertTrue([cell3 containingType:XCUIElementTypeButton identifier:@"Silence"]);
//    XCTAssertFalse(cell3.buttons[@"Alert"].exists);
//    [NSThread sleepForTimeInterval:5.f];
//    
//    [[[self.app.tables childrenMatchingType:XCUIElementTypeButton] elementBoundByIndex:0] tap];
//    
//     [NSThread sleepForTimeInterval:5.f];
//  
//}
//
//-(void) testUnMuteEvent {
//    
//    // Prepare our initial state, with 3 conversations.
//    // Conversation 1 with Marcel which is in my roster and Online.
//    // Conversation 2 with Micheline which is in my roster but pending
//    // Conversation 3 Room with Marcel and Micheline, conv is muted.
//    
//    ConversationAPI *conversation1 = [ConversationAPI new];
//    conversation1.peerID = @"rainbow_123";
//    conversation1.type = @"user";
//    conversation1.jid = @"marcel@domain.com";
//    conversation1.lastMessage = @"Hello There";
//    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
//    conversation1.unread = @(1);
//    conversation1.mute = YES;
//    [self.server addConversation:conversation1];
//    
//    ConversationAPI *conversation2 = [ConversationAPI new];
//    conversation2.peerID = @"rainbow_456";
//    conversation2.type = @"user";
//    conversation2.jid = @"micheline@domain.com";
//    conversation2.lastMessage = @"Blablabla";
//    conversation2.lastMessageDate = @"2016-12-10T10:11:03.000Z";
//    conversation2.unread = @(2);
//    conversation2.mute = NO;
//    [self.server addConversation:conversation2];
//    
//    ConversationAPI *conversation3 = [ConversationAPI new];
//    conversation3.peerID = @"rainbow_room_789";
//    conversation3.type = @"room";
//    conversation3.jid = @"room_equipe@domain.com";
//    conversation3.lastMessage = @"Je fais des tests";
//    conversation3.lastMessageDate = @"2016-11-01T09:00:00.000Z";
//    conversation3.unread = @(0);
//    conversation3.mute = YES;
//    [self.server addConversation:conversation3];
//    
//    
//    ContactAPI *contact1 = [ContactAPI new];
//    contact1.ID = @"rainbow_123";
//    contact1.jid = @"marcel@domain.com";
//    contact1.loginEmail = @"marcel@dupont.com";
//    contact1.firstname = @"Marcel";
//    contact1.lastname = @"Dupont";
//    contact1.inRoster = YES;
//    contact1.isPending = NO;
//    contact1.presence = @"Online";
//    [self.server addContact:contact1];
//    
//    ContactAPI *contact2 = [ContactAPI new];
//    contact2.ID = @"rainbow_456";
//    contact2.jid = @"micheline@domain.com";
//    contact2.loginEmail = @"micheline@dupre.com";
//    contact2.firstname = @"Micheline";
//    contact2.lastname = @"Dupré";
//    contact2.inRoster = YES;
//    contact2.isPending = YES;
//    contact2.presence = @"Away";
//    [self.server addContact:contact2];
//    
//    RoomAPI *room1 = [RoomAPI new];
//    room1.ID = @"rainbow_room_789";
//    room1.jid = @"room_equipe@domain.com";
//    room1.name = @"Room equipe dev";
//    room1.topic = @"Topic de room";
//    room1.visibility = @"private";
//    room1.creationDate = @"2016-03-28T18:27:10.970Z";
//    room1.creator = [self.server myRainbowID];
//    [room1 addUser:[self.server myRainbowID] privilege:@"moderator" status:@"accepted"];
//    [room1 addUser:@"rainbow_123" privilege:@"user" status:@"accepted"];
//    [room1 addUser:@"rainbow_456" privilege:@"user" status:@"accepted"];
//    [self.server addRoom:room1];
//    
//    
//    // Now launch application.
//    [self launchApplication];
//    [NSThread sleepForTimeInterval:1.f];
//    
//    XCUIElementQuery *cell3 = [self.app.tables.cells containingType:XCUIElementTypeStaticText identifier:@"Room equipe dev"];
//    XCTAssertNotNil(cell3);
//    
//    
//    // Swipe the conversation to check the presented buttons (this on IS muted)
//    
//    [cell3.staticTexts[@"Room equipe dev"] swipeLeft];
//    
//    XCTAssertTrue([cell3 containingType:XCUIElementTypeButton identifier:@"Silence"]);
//    XCTAssertFalse(cell3.buttons[@"Alert"].exists);
//    
//    
//    
//    [[[self.app.tables childrenMatchingType:XCUIElementTypeButton] elementBoundByIndex:0] tap];
//    
//    [NSThread sleepForTimeInterval:5.f];
//    
//    [cell3.staticTexts[@"Room equipe dev"] swipeLeft];
//    
//    XCTAssertTrue([cell3 containingType:XCUIElementTypeButton identifier:@"Alert"]);
//    XCTAssertFalse(cell3.buttons[@"Silence"].exists);
//    [NSThread sleepForTimeInterval:5.f];
//    
//    [[[self.app.tables childrenMatchingType:XCUIElementTypeButton] elementBoundByIndex:0] tap];
//    
//    [NSThread sleepForTimeInterval:5.f];
//    
//}
//
//
//// TODO: Add tests
///* - reception of carbon ACK for an unread msg, pass message as read. (unread 1 -> 0)
// 
// */
//@end
