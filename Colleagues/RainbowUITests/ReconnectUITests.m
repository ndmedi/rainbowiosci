///*
// * Rainbow
// *
// * Copyright (c) 2016, ALE International
// * All rights reserved.
// *
// * ALE International Proprietary Information
// *
// * Contains proprietary/trade secret information which is the property of
// * ALE International and must not be made available to, or copied or used by
// * anyone outside ALE International without its written authorization
// *
// * Not to be disclosed or used except in accordance with applicable agreements.
// */
//
//#import <Foundation/Foundation.h>
//#import "RainbowUITests.h"
//
//@interface ReconnectUITests : RainbowUITests
//@end
//
//
//@implementation ReconnectUITests
//
//-(void) testAppGoBackgroundComeForeground {
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [NSThread sleepForTimeInterval:0.5f];
//    
//    // Go home
//    [self putAppToBackground];
//    
//    [NSThread sleepForTimeInterval:1.0f];
//    
//    [self putAppToForeground];
//    
//    [NSThread sleepForTimeInterval:1.0f];
//    
//    //XCTAssert(NO);
//}
//
//-(void) testServerReconnectWhileInBackgroundBannerIsVisible {
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [NSThread sleepForTimeInterval:1.0f];
//    
//    [self stopServer];
//    
//    // Check the Not connected banner has appeared
//    XCUIElement *not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == true"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    [NSThread sleepForTimeInterval:1.0f];
//    
//    // Go home
//    [self putAppToBackground];
//    
//    [NSThread sleepForTimeInterval:2.0f];
//    
//    // Server restart while in background
//    [self startServer];
//    
//    [NSThread sleepForTimeInterval:2.0f];
//    
//    [self putAppToForeground];
//    
//    // Check the Not connected banner has *DIS*appeared
//    not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == false"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:10.0f handler:nil];
//    
//    //XCTAssert(NO);
//}
//
//
//-(void) testServerDisconnectWhileInBackgroundBannerIsVisible {
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    // Wait for app to be fully init
//    [NSThread sleepForTimeInterval:3.0f];
//    
//    // Check the Not connected banner is not here
//    XCUIElement *not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == false"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:3.0f handler:nil];
//    
//    // Go home
//    [self putAppToBackground];
//    
//    [NSThread sleepForTimeInterval:2.0f];
//    
//    // Server disconnect while in background
//    [self stopServer];
//    
//    [NSThread sleepForTimeInterval:5.0f];
//    
//    [self putAppToForeground];
//    
//    [NSThread sleepForTimeInterval:3.0f];
//    
//    // Check the Not connected banner has appeared
//    not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == false"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    //XCTAssert(NO);
//}
//
//
//-(void) testServerDisconnectsBannerIsVisibleAndButtonsAreDisabled {
//    
//    // Now launch application.
//    [self launchApplication];
//    
//    [self stopServer];
//    
//    // Check the Not connected banner is here
//    XCUIElement *not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == true"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    // Check that buttons are disabled
//    
//    XCTAssert(self.app.navigationBars[@"Conversations"].buttons[@"Add"].isDisabled);
//    
//    [self.app.tabBars.buttons[@"Bubbles"] tap];
//    
//    not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == true"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    // Check that buttons are disabled
//    XCTAssert(self.app.navigationBars[@"Bubbles"].buttons[@"Add"].isDisabled);
//    
//    [self.app.buttons[@"My bubbles"] tap];
//    
//    XCTAssert(self.app.navigationBars[@"Bubbles"].buttons[@"Add"].isDisabled);
//    
//    [self.app.buttons[@"Archives"] tap];
//    
//    XCTAssert(self.app.navigationBars[@"Bubbles"].buttons[@"Add"].isDisabled);
//    
//    [self.app.buttons[@"All"] tap];
//    
//   
//    
//    [self.app.tabBars.buttons[@"Contacts"] tap];
//    
//    not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == true"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    // Check that buttons are disabled in List tab (search and add)
//    
//    XCTAssert(self.app.navigationBars[@"Contacts"].buttons[@"Add"].isDisabled);
//    
//    [self.app.buttons[@"My network"] tap];
//  
//    // here the filter button is allowed !
//
//    XCUIElement *filterButton = [[[XCUIApplication alloc] init].navigationBars[@"Contacts"] childrenMatchingType:XCUIElementTypeButton].element;
//    [filterButton tap];
//    
//    [NSThread sleepForTimeInterval:2.0f];
//    
//    [filterButton tap];
//    
//    
//    [self.app.tabBars.buttons[@"Recents"] tap];
//    
//    not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == true"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    // Check that buttons are disabled
//    XCTAssert(self.app.navigationBars[@"Recents"].buttons[@"Edit"].isDisabled);
//    
//    [self.app.buttons[@"Missed"] tap];
//    
//    XCTAssert(self.app.navigationBars[@"Recents"].buttons[@"Edit"].isDisabled);
//    
//    
//    [self.app.tabBars.buttons[@"Meetings"] tap];
//    
//    not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == true"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    // Check that buttons are disabled
//    // add
//    XCTAssert([[self.app.navigationBars[@"Meetings"] childrenMatchingType:XCUIElementTypeButton].element isDisabled]);
//    
//    [self.app.buttons[@"History"] tap];
//    
//    // add
//    XCTAssert([[self.app.navigationBars[@"Meetings"] childrenMatchingType:XCUIElementTypeButton].element isDisabled]);
//    
//    [self startServer];
//    
//    // Wait a little bit longer here, for client to reconnect
//    not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == false"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:15.0f handler:nil];
//    
//    
//    [self.app.tabBars.buttons[@"Contacts"] tap];
//    
//    not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == false"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    // Check that buttons are all enabled in List tab (add)
//    [self.app.buttons[@"My lists"] tap];
//    
//  
//    [[self.app.navigationBars[@"Contacts"] childrenMatchingType:XCUIElementTypeButton].element tap];
//    [self.app.navigationBars[@"Create a list"].buttons[@"Cancel"] tap];
//    
//    [self.app.buttons[@"My network"] tap];
//    // here the filter button is allowed !
//
//    [filterButton tap];
//    
//    [NSThread sleepForTimeInterval:2.0f];
//    
//    [filterButton tap];
//    
//    [self.app.buttons[@"Invitations"] tap];
//    // here the filter button is allowed !
// 
//   
//    
//    [self.app.tabBars.buttons[@"Bubbles"] tap];
//    not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == false"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    // Check that buttons are enabled
//  
//    [[self.app.navigationBars[@"Bubbles"] childrenMatchingType:XCUIElementTypeButton].element tap];
//    
//    [self.app.navigationBars[@"Create a bubble"].buttons[@"Cancel"] tap];
//    
//    [self.app.tabBars.buttons[@"Conversations"] tap];
//    not_connected = self.app.staticTexts[@"No network connection"];
//    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == false"] evaluatedWithObject:not_connected handler:nil];
//    [self waitForExpectationsWithTimeout:5.0f handler:nil];
//    
//    
//    
//    //XCTAssert(NO);
//}
//
//@end
