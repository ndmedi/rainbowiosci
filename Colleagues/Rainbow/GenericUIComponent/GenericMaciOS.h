/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


#if (TARGET_OS_MAC && !(TARGET_OS_EMBEDDED || TARGET_OS_IPHONE))

#import <AddressBook/ABAddressBookC.h>
#import <AppKit/AppKit.h>
#define W_ABPropertyType NSString*
#define kABPersonPhoneMainLabel kABPhoneMainLabel
#define kABPersonPhoneMobileLabel kABPhoneMobileLabel
#define kABPersonPhoneIPhoneLabel kABPhoneiPhoneLabel
#define kABPersonPhoneHomeFAXLabel kABPhoneHomeFAXLabel
#define kABPersonPhoneWorkFAXLabel kABPhoneWorkFAXLabel
#define kABPersonPhonePagerLabel kABPhonePagerLabel
#define kABPersonEmailProperty kABEmailProperty
#define kABPersonPhoneProperty kABPhoneProperty
#define kABPersonFirstNameProperty kABFirstNameProperty
#define kABPersonLastNameProperty kABLastNameProperty
#define kABPersonOrganizationProperty kABOrganizationProperty
#define kABPersonJobTitleProperty kABJobTitleProperty
#define kABPersonInstantMessageProperty kABInstantMessageProperty
#define kABPersonInstantMessageUsernameKey kABJabberInstantProperty
#define kABPersonURLProperty kABURLsProperty
#define kABPersonAddressProperty kABAddressProperty
#define kABPersonAddressZIPKey kABAddressZIPKey
#define kABPersonAddressCityKey kABAddressCityKey
#define kABPersonAddressStateKey kABAddressStateKey
#define kABPersonAddressCountryKey kABAddressCountryKey
#define kABPersonAddressStreetKey kABAddressStreetKey

#define UIBackgroundTaskInvalid 0
#define ABRecordID NSString*

// Implement the iOS signature from the MacOS implementation
CFIndex ABMultiValueGetCount (ABMultiValueRef multiValue);
CFStringRef ABRecordGetRecordID(ABRecordRef record);

typedef NSImage APPImage;
typedef NSUInteger UIBackgroundTaskIdentifier;

#endif

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#define W_ABPropertyType ABPropertyID

typedef UIImage APPImage;
#endif

