/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UITools.h"
#import "UILabel+FontAppearance.h"
#import "DZNSegmentedControl.h"
#if !defined(APP_EXTENSION)
#import "SCLAlertView.h"
#import "SCLAlertView+Rotation.h"
#endif
#import <Rainbow/ServicesManager.h>
#import <Rainbow/NSDate+Utilities.h>
#import <Rainbow/NSDate+Distance.h>
#import "SORelativeDateTransformer.h"
#import <MessageUI/MessageUI.h>
#import "MBProgressHUD.h"
#import "ContactsManagerService+InvitationPrivate.h"
#import "Server+IsAllInOne.h"
#import <Photos/Photos.h>

@implementation UITools
+(void) customizeAppAppearanceWithGlobalColor:(UIColor *) globalColor {
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControl.backgroundColor = [UIColor whiteColor];
    #if !defined(APP_EXTENSION)
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    #endif
    [[UITabBar appearance] setTintColor:globalColor];
    [[UINavigationBar appearance] setBarTintColor:globalColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    UIColor *whiteAlpha90 = [[UIColor whiteColor] colorWithAlphaComponent:0.9];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:whiteAlpha90, NSFontAttributeName:[UIFont fontWithName:[UITools boldFontName] size:17.0]}];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:10.0f]} forState:UIControlStateNormal];
    
    [[UISegmentedControl appearance] setTintColor:globalColor];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:13.0f]} forState:UIControlStateNormal];
    // No need to specify font, the method do it for us
    [[UILabel appearanceWhenContainedIn:UIAlertController.class, nil] setAppearanceFont:nil];
    #if !defined(APP_EXTENSION)
    [[DZNSegmentedControl appearance] setFont:[UIFont fontWithName:[UITools defaultFontName] size:15]];
    [[DZNSegmentedControl appearance] setHairlineColor:[UITools defaultTintColor]];
    [[DZNSegmentedControl appearance] setTintColor:[UITools defaultTintColor]];
    #endif
    [[UITextField appearance] setFont:[UIFont fontWithName:[UITools defaultFontName] size:15.0]];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:[UITools boldFontName] size:15.0]} forState:UIControlStateNormal];
#endif
}

+ (UIColor *) colorFromHexa:(int)hexaCode {
    return [UIColor colorWithRed:((hexaCode & 0xFF000000) >> 24) / 255.0f
                           green:((hexaCode & 0x00FF0000) >> 16) /255.0f
                            blue:((hexaCode & 0x0000FF00) >> 8) / 255.0f
                            alpha:(hexaCode & 0x000000FF)/255.0f];
}

+(UIColor *) defaultTintColor {
    return [UITools colorFromHexa:0x0085CAFF];
}

+(UIColor *) defaultBackgroundColor {
    return [UIColor whiteColor];
}

+(UIColor *) notificationBadgeColor {
    return [UITools colorFromHexa:0XD0006FFF];
}

+(UIColor *) foregroundGrayColor {
    return [UITools colorFromHexa:0x989898FF];
}

+(UIColor *) backgroundGrayColor {
    return [UITools colorFromHexa:0xF8F8F8FF];
}

+(UIColor *) redColor {
    return [UITools colorFromHexa:0xD81818FF];
}

+(NSString *) defaultFontName {
    return @"ClanOT-NarrowNews";
}

+(NSString *) boldFontName {
    return @"ClanOT-Medium";
}
+(NSString *) thinFontName {
    return @"ClanOT-Book";
}

+(void) applyCustomFontTo:(UILabel*)lbl {
    CGFloat size =  lbl.font.pointSize;
    UIFont* font = [UIFont fontWithName:[UITools defaultFontName] size:size];
    lbl.font = font;
}

+(void) applyCustomFontToTextField:(UITextField*)textField {
    CGFloat size =  textField.font.pointSize;
    UIFont* font = [UIFont fontWithName:[UITools defaultFontName] size:size];
    textField.font = font;
}

+(void) applyCustomBoldFontTo:(UILabel*)lbl {
    CGFloat size =  lbl.font.pointSize;
    UIFont* font = [UIFont fontWithName:[UITools boldFontName] size:size];
    lbl.font = font;
}

+(void) applyThinCustomFontTo:(UILabel*)lbl {
    CGFloat size =  lbl.font.pointSize;
    UIFont* font = [UIFont fontWithName:[UITools thinFontName] size:size];
    lbl.font = font;
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

+ (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+(UIColor *)readableForegroundColorForBackgroundColor:(UIColor*)backgroundColor {
    size_t count = CGColorGetNumberOfComponents(backgroundColor.CGColor);
    const CGFloat *componentColors = CGColorGetComponents(backgroundColor.CGColor);
    
    CGFloat darknessScore = 0;
    if (count == 2) {
        darknessScore = (((componentColors[0]*255) * 299) + ((componentColors[0]*255) * 587) + ((componentColors[0]*255) * 114)) / 1000;
    } else if (count == 4) {
        darknessScore = (((componentColors[0]*255) * 299) + ((componentColors[1]*255) * 587) + ((componentColors[2]*255) * 114)) / 1000;
    }
    
    if (darknessScore >= 175) {
        return [UIColor blackColor];
    }
    
    return [UIColor whiteColor];
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize circular:(bool) isCircular {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    if(isCircular){
        CGFloat maskRadius = MIN(newSize.width, newSize.height);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGPathRef path = CGPathCreateWithEllipseInRect(CGRectMake((newSize.width - maskRadius)/2, (newSize.height - maskRadius)/2, maskRadius, maskRadius) , NULL);
        CGContextAddPath(context, path);
        CGContextClip(context);
        CGPathRelease(path);
    }
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    return [UITools imageWithImage:image scaledToSize:newSize circular: NO];
}

+(UIColor *) colorForPresence:(Presence *) presence {
    switch (presence.presence) {
        default:
        case ContactPresenceUnavailable: {
            return [UIColor grayColor];
            break;
        }
        case ContactPresenceAvailable:{
            return [UITools colorFromHexa:0x34B233FF];
            break;
        }
        case ContactPresenceBusy:
        case ContactPresenceDoNotDisturb: {
            return [UIColor redColor];
            break;
        }
        case ContactPresenceAway: {
            return [UITools colorFromHexa:0XFFA000FF];
            break;
        }
        case ContactPresenceInvisible: {
            return [UIColor whiteColor];
            break;
        }
    }
    return [UIColor grayColor];
}

+ (NSString *) formatTimeForDate:(NSDate *) date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
    BOOL is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
    
    formatter = [[NSDateFormatter alloc] init];
    if (is24h){
        // device is set to 24-hour mode
        [formatter setDateFormat:@"HH:mm"];
    } else {
        [formatter setDateFormat:@"hh:mm a"];
    }
    NSString *returnedDate = [formatter stringFromDate:date];
    return returnedDate;
}

+ (NSString *) formatDateTimeForDate:(NSDate *) date {
    return [NSDateFormatter localizedStringFromDate: date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
}

+ (NSString *) shortFormatForDate:(NSDate *) date {
    if ([date isThisYear]){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSString *template = @"MMMd";
        dateFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:template options:0 locale:[NSLocale currentLocale]];
        return [dateFormatter stringFromDate:date];
    } else {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateStyle = NSDateFormatterMediumStyle;
        dateFormatter.timeStyle = NSDateFormatterNoStyle;
        [dateFormatter setLocale:[NSLocale currentLocale]];
        return [dateFormatter stringFromDate:date];
    }
}

+ (NSString *) optimizedFormatForDate:(NSDate *) date {
    if(date){
        if([date isToday]) {
            return [self formatTimeForDate:date];
        } else if ([date isThisYear]){
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            NSString *template = @"MMMd HH:mm";
            dateFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:template options:0 locale:[NSLocale currentLocale]];
            return [dateFormatter stringFromDate:date];
        } else {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateStyle = NSDateFormatterMediumStyle;
            dateFormatter.timeStyle = NSDateFormatterShortStyle;
            [dateFormatter setLocale:[NSLocale currentLocale]];
            return [dateFormatter stringFromDate:date];
        }
    } else {
        return @"";
    }
}

+ (NSString *) optimizedFormatForCalendarPresenceDate:(NSDate *) date {
    if(date){
        if([date isToday]) {
            return [self formatTimeForDate:date];
        } else if ([date isThisYear]){
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            NSString *template = @"MMMd";
            dateFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:template options:0 locale:[NSLocale currentLocale]];
            return [dateFormatter stringFromDate:date];
        } else {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateStyle = NSDateFormatterMediumStyle;
            dateFormatter.timeStyle = NSDateFormatterShortStyle;
            [dateFormatter setLocale:[NSLocale currentLocale]];
            return [dateFormatter stringFromDate:date];
        }
    } else {
        return @"";
    }
}

+ (UIColor*) colorForString:(NSString*) string {
    NSArray* colors = @[@0xFF4500FF, @0xD38700FF, @0x348833FF, @0x007356FF, @0x00B2A9FF, @0x00B0E5FF, @0x0085CAFF, @0x6639B7FF, @0x91278AFF , @0xCF0072FF, @0xA50034FF, @0xD20000FF];
    
    NSString* capitalizedString = [string uppercaseString];
    int sum = 0;
    for (int i=0; i< capitalizedString.length; i++) {
        sum += [capitalizedString characterAtIndex:i];
    }
    NSInteger value = [colors[0 + sum%[colors count]] integerValue];
    UIColor* color = [UITools colorFromHexa:(int)value];
    return color;
}

#pragma mark - Sort descriptors
+(NSSortDescriptor *) sortDescriptorForContactByLastName {
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
        if (obj1 == nil)
            return NSOrderedDescending;
        else
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
    }];
    return sortByName;
}

+(NSSortDescriptor *) sortDescriptorForContactByFirstName {
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
        if (obj1 == nil)
            return NSOrderedDescending;
        else
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
    }];
    return sortByName;
}

+(NSSortDescriptor *) sortDescriptorForParticipantByLastName {
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"contact.lastName" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
        if (obj1 == nil)
            return NSOrderedDescending;
        else
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
    }];
    return sortByName;
}

+(NSSortDescriptor *) sortDescriptorForParticipantByFirstName {
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"contact.firstName" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
        if (obj1 == nil)
            return NSOrderedDescending;
        else
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
    }];
    return sortByName;
}

+(NSSortDescriptor *) sortDescriptorForGroupByName {
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
        if (!obj1  && !obj2)
            return NSOrderedSame;
        
        if (!obj1)
            return NSOrderedDescending;
        
        if (!obj2)
            return NSOrderedAscending;
        
        return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
    }];
    return sortByName;
}

+(NSSortDescriptor *) sortDescriptorByLastMessageDate {
    NSSortDescriptor *sortByLastMessageDate = [NSSortDescriptor sortDescriptorWithKey:@"lastUpdateDate" ascending:NO comparator:^NSComparisonResult(NSDate* dateMessage1, NSDate* dateMessage2) {
        if (!dateMessage1  && !dateMessage2)
            return NSOrderedSame;
        
        if (!dateMessage1)
            return NSOrderedDescending;
        
        if (!dateMessage2)
            return NSOrderedAscending;

        return [dateMessage1 compare:dateMessage2];
    }];
    return sortByLastMessageDate;
}

+(NSSortDescriptor *) sortDescriptorByDisplayName {
    NSSortDescriptor *sortByDisplayName = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES comparator:^NSComparisonResult(NSString* displayNameRoom1, NSString* displayNameRoom2) {
        if (!displayNameRoom1  && !displayNameRoom2)
            return NSOrderedSame;
        
        if (!displayNameRoom1)
            return NSOrderedDescending;
        
        if (!displayNameRoom2)
            return NSOrderedAscending;

        return [displayNameRoom1 compare:displayNameRoom2 options:NSCaseInsensitiveSearch];
    }];
    return sortByDisplayName;
}

+(void) sizeButtonToText:(UIButton *)button availableSize:(CGSize)availableSize padding:(UIEdgeInsets)padding {
    CGRect boundsForText = button.frame;
    
    // Measures string
    // NB : if we use button.titleLabel.text
    // The string value might not be correct (not up-to-date) if we just changed the value.
    // use button.currentTitle instead which contains the right value.
    CGSize stringSize = [button.currentTitle sizeWithAttributes:@{NSFontAttributeName:button.titleLabel.font}];
    stringSize.width = MIN(stringSize.width + padding.left + padding.right + button.imageView.frame.size.width, availableSize.width);
    
    // Center's location of button
    boundsForText.origin.x += (boundsForText.size.width - stringSize.width);
    boundsForText.size.width = stringSize.width;
    [button setFrame:boundsForText];
}

#if !defined(APP_EXTENSION)
+ (void) showInvitePopupForContact:(Contact*) contact inViewController:(UIViewController*) controller withCompletionHandler:(void(^)())completionHandler {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [UITools showInvitePopupForContact:contact inViewController:controller withCompletionHandler:completionHandler];
        });
        return;
    }
    // Show a confirmation popup.
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    [alert addButton:NSLocalizedString(@"Invite", nil) actionBlock:^(void) {
        if (completionHandler)
            completionHandler();
    }];
    
    alert.customViewColor = [UITools defaultTintColor]; // button color and top icon background
    alert.iconTintColor = [UITools defaultBackgroundColor]; // top icon font color
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = YES;
    alert.labelTitle.numberOfLines = 2;
    alert.labelTitle.lineBreakMode = NSLineBreakByWordWrapping;
    alert.labelTitle.textAlignment = NSTextAlignmentCenter;
    
    [alert showInfo:controller
              title:[NSString stringWithFormat:NSLocalizedString(@"Invite %@", nil), contact.displayName]
           subTitle:NSLocalizedString(@"This contact has no Rainbow account. Would you like to invite this contact to join the Rainbow community?", nil)
   closeButtonTitle:NSLocalizedString(@"Cancel", nil)
           duration:0.0f];
}

+ (void) showInvitePopupForAllContacts:(UIViewController*) controller withCompletionHandler:(void(^)())completionHandler {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [UITools showInvitePopupForAllContacts:controller withCompletionHandler:completionHandler];
        });
        return;
    }
    // Show a confirmation popup.
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    [alert addButton:NSLocalizedString(@"Yes", nil) actionBlock:^(void) {
        if (completionHandler)
            completionHandler();
    }];
    
    alert.customViewColor = [UITools defaultTintColor]; // button color and top icon background
    alert.iconTintColor = [UITools defaultBackgroundColor]; // top icon font color
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = YES;
    alert.labelTitle.numberOfLines = 2;
    alert.labelTitle.lineBreakMode = NSLineBreakByWordWrapping;
    alert.labelTitle.textAlignment = NSTextAlignmentCenter;
    
    [alert showInfo:controller
              title:[NSString stringWithFormat:NSLocalizedString(@"Invite to join Rainbow", nil)]
           subTitle:NSLocalizedString(@"Would you like to invite by email all your local contacts to join the Rainbow community?", nil)
   closeButtonTitle:NSLocalizedString(@"Cancel", nil)
           duration:0.0f];
}

+ (void) showRequestJoinCompanyPopupForCompany:(Company *) company inViewController:(UIViewController*) controller actionTarget:(id) target actionSelector:(SEL) selector {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [UITools showRequestJoinCompanyPopupForCompany:company inViewController:controller actionTarget:target actionSelector:selector];
        });
        return;
    }
    // Show a confirmation popup.
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    [alert addButton:NSLocalizedString(@"Send request", nil) target:target selector:selector];
    
    alert.customViewColor = [UITools defaultTintColor]; // button color and top icon background
    alert.iconTintColor = [UITools defaultBackgroundColor]; // top icon font color
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = YES;
    alert.labelTitle.numberOfLines = 2;
    alert.labelTitle.lineBreakMode = NSLineBreakByWordWrapping;
    alert.labelTitle.textAlignment = NSTextAlignmentCenter;
    
    [alert showQuestion:controller
              title:company.name
           subTitle:NSLocalizedString(@"Would you like to send a request to join this company?", nil)
   closeButtonTitle:NSLocalizedString(@"Cancel", nil)
           duration:0.0f];
}

+ (void) showErrorPopupWithTitle:(NSString *) title message:(NSString *) message inViewController:(UIViewController*) controller {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [UITools showErrorPopupWithTitle:title message:message inViewController:controller];
        });
        return;
    }
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    alert.customViewColor = [UITools defaultTintColor]; // button color and top icon background
    alert.iconTintColor = [UITools defaultBackgroundColor]; // top icon font color
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = YES;
    alert.labelTitle.numberOfLines = 2;
    alert.labelTitle.lineBreakMode = NSLineBreakByWordWrapping;
    alert.labelTitle.textAlignment = NSTextAlignmentCenter;
    
    [alert showError:controller title:title subTitle:message closeButtonTitle:NSLocalizedString(@"OK", nil) duration:0.0f];
}

+ (void) showInfoPopupWithTitle:(NSString *) title message:(NSString *) message inViewController:(UIViewController*) controller {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [UITools showInfoPopupWithTitle:title message:message inViewController:controller];
        });
        return;
    }
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    alert.customViewColor = [UITools defaultTintColor]; // button color and top icon background
    alert.iconTintColor = [UITools defaultBackgroundColor]; // top icon font color
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = YES;
    alert.labelTitle.numberOfLines = 2;
    alert.labelTitle.lineBreakMode = NSLineBreakByWordWrapping;
    alert.labelTitle.textAlignment = NSTextAlignmentCenter;
    
    [alert showInfo:controller title:title subTitle:message closeButtonTitle:NSLocalizedString(@"OK", nil) duration:0.0f];
}
#endif

+(CGSize) findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
    if (text) {
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size;
}

+(NSString *)valueForKey:(NSString *)key fromQueryItems:(NSArray *)queryItems {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", key];
    NSURLQueryItem *queryItem = [[queryItems filteredArrayUsingPredicate:predicate] firstObject];
    return queryItem.value;
}

+(MBProgressHUD *) showHUDWithMessage:(NSString *) message forView:(UIView *) view mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion {
    return [UITools showHUDWithMessage:message detailedMessage:nil forView:view  mode:mode imageName:nil dismissCompletionBlock:dismissCompletion];
}

+(MBProgressHUD *) showHUDWithMessage:(NSString *) message forView:(UIView *) view mode:(MBProgressHUDMode) mode imageName:(NSString *) imageName dismissCompletionBlock:(void (^)()) dismissCompletion {
    return [UITools showHUDWithMessage:message detailedMessage:nil forView:view  mode:mode imageName:imageName dismissCompletionBlock:dismissCompletion];
}

+(MBProgressHUD *) showHUDWithMessage:(NSString *) message detailedMessage:(NSString *) detailedMessage forView:(UIView *) view mode:(MBProgressHUDMode) mode imageName:(NSString *) imageName dismissCompletionBlock:(void (^)()) dismissCompletion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = message;
    if(detailedMessage){
        hud.detailsLabelText = detailedMessage;
        hud.detailsLabelFont = [UIFont fontWithName:[UITools defaultFontName] size:14.0f];
    }
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}

+ (BOOL)isForceTouchAvailableForTraitCollection:(UITraitCollection *)traitCollection {
    BOOL isForceTouchAvailable = NO;
    if ([traitCollection respondsToSelector:@selector(forceTouchCapability)]) {
        isForceTouchAvailable = traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable;
    }
    return isForceTouchAvailable;
}

+ (NSString *)timeFormatConvertToSeconds:(NSString *)timeSecs zeroFormattingBehaviorunitsStyle:(NSDateComponentsFormatterZeroFormattingBehavior) zeroFormattingBehavior unitsStyle: (NSDateComponentsFormatterUnitsStyle) unitsStyle {
    int totalSeconds = [timeSecs intValue];
    NSDateComponentsFormatter *dcFormatter = [[NSDateComponentsFormatter alloc] init];
    dcFormatter.zeroFormattingBehavior = zeroFormattingBehavior;
    dcFormatter.allowedUnits = NSCalendarUnitMinute | NSCalendarUnitSecond;
    if(totalSeconds >= 3600)
        dcFormatter.allowedUnits |= NSCalendarUnitHour;
    
    dcFormatter.unitsStyle = unitsStyle;
    return [dcFormatter stringFromTimeInterval:totalSeconds];
}

+ (NSString *)applicationDocumentsCache {
    NSString * pathFromApp = @"";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    if([paths count] > 0){
        pathFromApp = [paths objectAtIndex:0];
    } else {
        pathFromApp = nil;
    }
    return pathFromApp;
}

#if !defined(APP_EXTENSION)
+(void) showMicrophoneBlockedPopupInController:(UIViewController *) controller {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [UITools showMicrophoneBlockedPopupInController:controller];
        });
        return;
    }
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        
        [alert addButton:NSLocalizedString(@"Go to Settings", nil) actionBlock:^(void) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        
        alert.customViewColor = [UITools defaultTintColor];
        alert.iconTintColor = [UITools defaultBackgroundColor];
        alert.backgroundViewColor = [UITools defaultBackgroundColor];
        alert.statusBarStyle = UIStatusBarStyleLightContent;
        [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
        [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
        alert.shouldDismissOnTapOutside = YES;
        
        [alert showInfo:controller title:NSLocalizedString(@"Access to microphone", nil) subTitle:NSLocalizedString(@"Without your auhtorisation to use microphone, the callee will not be able to hear you during call.", nil) closeButtonTitle:NSLocalizedString(@"OK", nil) duration:0.0f];
}
#endif

+ (UIImage *)convertImageToGrayScale:(UIImage *)image {
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [image CGImage]);
    
    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    // Create a new UIImage object
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    // Return the new grayscale image
    return newImage;
}

+(NSString *) getPresenceText:(Contact *) contact {
    
    NSString *presenceText = @"";
    BOOL displayAdditionalInfo = YES;
        
    presenceText = NSLocalizedString([Presence stringForContactPresence:contact.presence],nil);
        
    if (contact.presence.presence == ContactPresenceAvailable) {
        displayAdditionalInfo = NO;
        if (contact.isConnectedWithMobile) {
            presenceText = NSLocalizedString(@"Online on mobile", nil);
        }
    }
    
    if(!contact.isPresenceSubscribed) {
        //display company name instead of presence state
        presenceText = contact.companyName;
        displayAdditionalInfo = NO;
    }
    
    if(contact.presence.presence == ContactPresenceAway){
        NSString *text = nil;
        // Search for the away time or the offline time
        if(contact.lastActivityDate){
            NSInteger distanceInSeconds = [contact.lastActivityDate distanceInSecondsToNow];
            if(distanceInSeconds > D_MINUTE){
                text = [NSString stringWithFormat:NSLocalizedString(@"Away for %@", nil),[[SORelativeDateTransformer registeredTransformer] transformedValue:contact.lastActivityDate withAgoMessage:NO]];
            } else {
                text = NSLocalizedString(@"Away for few seconds", nil);
            }
        } else {
            text = NSLocalizedString(@"Away", nil);
        }
        
        return text;
    }
    
    if(contact.presence.presence == ContactPresenceUnavailable){
        NSString *text = nil;
        // Search for the away time or the offline time
        if(contact.lastActivityDate){
            NSInteger distanceInSeconds = [contact.lastActivityDate distanceInSecondsToNow];
            if(distanceInSeconds > D_MINUTE){
                text = [NSString stringWithFormat:NSLocalizedString(@"Offline for %@", nil),[[SORelativeDateTransformer registeredTransformer] transformedValue:contact.lastActivityDate withAgoMessage:NO]];
            } else {
                text = NSLocalizedString(@"Offline for few seconds", nil);
            }
        } else {
            text = NSLocalizedString(@"Offline", nil);
        }
        
        return text;
    }
    
    if(displayAdditionalInfo){
        NSString *additionalInfo = @"";
        if(contact.presence.status.length > 0 && ![contact.presence.status isEqualToString:@"busy"])
            additionalInfo = NSLocalizedString(contact.presence.status, nil);
        
        return [NSString stringWithFormat:@"%@ %@", presenceText, additionalInfo];
    } else {
        return [NSString stringWithFormat:@"%@", presenceText];
    }
}


+(void) showInviteContactAlertController:(Contact *) contact fromController:(UIViewController *) controller controllerDelegate:(id) controllerDelegate withCompletionHandler:(void(^)(Invitation *invitation))completionHandler {
    UIAlertController *_inviteMenuActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [_inviteMenuActionSheet setTitle: [NSString stringWithFormat: NSLocalizedString(@"How do you want to invite %@ ?", nil), contact.displayName] ];
    
    // --- INVITE BY EMAIL ---
    if( contact.emailAddresses.count > 0 ) {
        UIAlertAction* inviteByEmail = [UIAlertAction actionWithTitle:NSLocalizedString(@"Send email", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [[ServicesManager sharedInstance].contactsManagerService inviteContact:contact];
        }];
        
        [inviteByEmail setValue:[UIImage imageNamed:@"invitation"] forKey:@"image"];
        [_inviteMenuActionSheet addAction:inviteByEmail];
    }
    
    // --- INVITE BY SMS ---
    [contact.phoneNumbers enumerateObjectsUsingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
        
        NSString *label = aPhoneNumber.label;
        if (aPhoneNumber.deviceType == PhoneNumberDeviceTypeMobile){
            switch (aPhoneNumber.type) {
                case PhoneNumberTypeHome:
                    label = NSLocalizedString(@"Mobile", nil);
                    break;
                case PhoneNumberTypeWork:
                    label = NSLocalizedString(@"Mobile", nil);
                    break;
                case PhoneNumberTypeOther:
                    label = NSLocalizedString(@"Mobile", nil);
                    break;
                default:
                    label = NSLocalizedString(@"Mobile", nil);
                    break;
            }
        } else {
            label = NSLocalizedString(aPhoneNumber.label, nil);
        }

        
        UIAlertAction* inviteBySMS = [UIAlertAction actionWithTitle:[NSString stringWithFormat:NSLocalizedString(@"SMS %@ (%@)",nil), aPhoneNumber.number, label] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            // Prepare SMS message
            if ([MFMessageComposeViewController canSendText]) {
                // display a loading spinner
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:controller.view animated:YES];
                hud.mode = MBProgressHUDModeIndeterminate;
                hud.labelText = NSLocalizedString(@"Preparing invitation...", nil);
                hud.removeFromSuperViewOnHide = YES;
                [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
                
                
                [[ServicesManager sharedInstance].contactsManagerService inviteContact:contact withPhoneNumber: aPhoneNumber.number withCompletionHandler:^(Invitation *invitation) {
                    if(completionHandler)
                        completionHandler(invitation);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(invitation){
                            [UITools showSMSViewControllerForInvitation:invitation inController:controller controllerDelegate:controllerDelegate];
                        }
                        
                        [hud hide:YES];
                    });
                }];
            }
        }];
        
        [inviteBySMS setValue:[UIImage imageNamed:@"invitation_mobile"] forKey:@"image"];
        
        [_inviteMenuActionSheet addAction:inviteBySMS];
    }];
    
    // --- CANCEL ---
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [_inviteMenuActionSheet addAction:cancel];
    
    // show the menu.
    [_inviteMenuActionSheet.view setTintColor:[UITools defaultTintColor]];
    [controller presentViewController:_inviteMenuActionSheet animated:YES completion:nil];

}

+(void) showSMSViewControllerForInvitation:(Invitation *) invitation inController:(UIViewController *) controller controllerDelegate:(id) controllerDelegate {
    
    NSString *urlPrefix = @"web.";
    if([ServicesManager sharedInstance].myUser.server.isAllInOne)
        urlPrefix = @"web-";
    NSString *url = [NSString stringWithFormat: @"https://%@%@/#/invite?invitationId=%@", urlPrefix, [ServicesManager sharedInstance].myUser.server.serverDisplayedName, invitation.invitationID];
    Contact *contact = [ServicesManager sharedInstance].myUser.contact;
    // Create the text message
    NSString *smsBody = [NSString stringWithFormat: NSLocalizedString(@"You have been invited to Rainbow by %@ from %@.\nTo start the discussion, follow this link: %@\n\nWith Rainbow, get group chat, audio or video calls and file or screen sharing from anywhere and for free! Learn more about Rainbow at https://www.openrainbow.com.\n\nThe Rainbow Team", nil), contact.displayName, contact.companyName, url ];
    
    // Create a message compose controller
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate = controllerDelegate;
    picker.recipients = @[ invitation.phoneNumber ];
    picker.body = smsBody;
    [[UINavigationBar appearance] setBackIndicatorImage:[UITools imageFromColor:[UIColor redColor]]];
    [controller presentViewController:picker animated:YES completion:nil];
}
#if !defined(APP_EXTENSION)
+(void)openImagePicker:(UIImagePickerControllerSourceType)source delegate:(id <UINavigationControllerDelegate, UIImagePickerControllerDelegate>) delegate presentingViewController:(UIViewController *) presentingViewController {
    if(source == UIImagePickerControllerSourceTypeCamera){
        switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo]) {
            case AVAuthorizationStatusNotDetermined:{
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if(granted){
                        [UITools showImagePickerForSource:source delegate:delegate presentingViewController:presentingViewController];
                    } else {
                        [UITools showAccessNotGrantedPopupPresentingViewController:presentingViewController];
                    }
                }];
                break;
            }
            case AVAuthorizationStatusAuthorized:{
                [UITools showImagePickerForSource:source delegate:delegate presentingViewController:presentingViewController];
                break;
            }
            default:{
                [UITools showAccessNotGrantedPopupPresentingViewController:presentingViewController];
                break;
            }
        }
    } else {
        switch ([PHPhotoLibrary authorizationStatus]) {
            case PHAuthorizationStatusNotDetermined:{
                [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                    if(status == PHAuthorizationStatusAuthorized){
                        [UITools showImagePickerForSource:source delegate:delegate presentingViewController:presentingViewController];
                    } else {
                        [UITools showAccessNotGrantedPopupPresentingViewController:presentingViewController];
                    }
                }];
                break;
            }
            case PHAuthorizationStatusAuthorized:{
                [UITools showImagePickerForSource:source delegate:delegate presentingViewController:presentingViewController];
                break;
            }
            default:
                [UITools showAccessNotGrantedPopupPresentingViewController:presentingViewController];
                break;
        }
    }
}

+(void) showImagePickerForSource:(UIImagePickerControllerSourceType)source delegate:(id <UINavigationControllerDelegate, UIImagePickerControllerDelegate>) delegate presentingViewController:(UIViewController *) presentingViewController {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [UITools showImagePickerForSource:source delegate:delegate presentingViewController:presentingViewController];
        });
        return;
    }
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = delegate;
    imagePickerController.allowsEditing = YES;
    imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    if (source == UIImagePickerControllerSourceTypePhotoLibrary && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    else if(source == UIImagePickerControllerSourceTypePhotoLibrary)
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    else if (source == UIImagePickerControllerSourceTypeCamera)
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    if(source == UIImagePickerControllerSourceTypeCamera){
        // Don't use our custom overlay, it create pictures not squarred..
        // We disable editing because overlay block it
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
            imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    }
    
    [presentingViewController presentViewController:imagePickerController animated: YES completion: nil];
}

+(void) showAccessNotGrantedPopupPresentingViewController:(UIViewController *) presentingViewController {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [UITools showAccessNotGrantedPopupPresentingViewController:presentingViewController];
        });
        return;
    }
    
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    [alert addButton:NSLocalizedString(@"Go to Settings", nil) actionBlock:^(void) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    alert.customViewColor = [UITools defaultTintColor];
    alert.iconTintColor = [UITools defaultBackgroundColor];
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = YES;
    
    [alert showInfo:presentingViewController
              title:NSLocalizedString(@"Access photos", nil)
           subTitle:NSLocalizedString(@"Please allow access to photos and camera from your device settings", nil)
   closeButtonTitle:NSLocalizedString(@"Close", nil)
           duration:0.0f];
}
#endif

char *appendRandom(char *str, char *alphabet, int amount) {
    for (int i = 0; i < amount; i++) {
        int r = arc4random() % strlen(alphabet);
        *str = alphabet[r];
        str++;
    }
    
    return str;
}

+(NSString *) generateUniquePassword {
    NSString *uniqueID = [[[[[UIDevice currentDevice] identifierForVendor] UUIDString] stringByReplacingOccurrencesOfString:@"-" withString:@""] substringToIndex:24];
    
    int letters  = 6;
    int capitals = 1;
    int digits   = 1;
    int symbols  = 1;
    int length   = letters + capitals + digits + symbols;
    
    // Build the password using C strings - for speed
    char *cPassword = calloc(length + 1, sizeof(char));
    char *ptr       = cPassword;
    
    cPassword[length - 1] = '\0';
    
    char *lettersAlphabet = "abcdefghijklmnopqrstuvwxyz";
    ptr = appendRandom(ptr, lettersAlphabet, letters);
    
    char *capitalsAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    ptr = appendRandom(ptr, capitalsAlphabet, capitals);
    
    char *digitsAlphabet = "0123456789";
    ptr = appendRandom(ptr, digitsAlphabet, digits);
    
    char *symbolsAlphabet = "!@#$%*[];?()";
    ptr = appendRandom(ptr, symbolsAlphabet, symbols);
    
    // Shuffle the string!
    for (int i = 0; i < length; i++) {
        int  r    = arc4random() % length;
        char temp = cPassword[i];
        cPassword[i] = cPassword[r];
        cPassword[r] = temp;
    }

    NSString *generatedPassword = [NSString stringWithFormat:@"%@|%@",uniqueID, [NSString stringWithCString:cPassword encoding:NSUTF8StringEncoding]];
    return generatedPassword;
}

+(void) defineApplicationIDForRainbow {
    if([[ServicesManager sharedInstance].myUser.server defaultServer])
        [[ServicesManager sharedInstance] setAppID:@"940ad8d000f011e88ce9e7ca1c2ea6f1" secretKey:@"XH7NXbfcyxqndNBnC3N2BsxT8moFZuerqFAYtRAA0mv6WW2Av1L0UcaqH9QO8igq"];
    else
        [[ServicesManager sharedInstance] setAppID:@"940ad8d000f011e88ce9e7ca1c2ea6f1" secretKey:@"ub34cEskQG4vnxl2liurNkrORC8hsjT6UMv5r3ulCTqUczCleQFw0Dpd0b9wGVqU"];
    
}
#if !defined(APP_EXTENSION)
+(void) makeCallToPhoneNumber:(PhoneNumber *) phoneNumber inController:(UIViewController *) controller {
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    [alert addButton:NSLocalizedString(@"Cancel call ?", nil) actionBlock:^(void) {
        // TODO: Implement cancel call
        [[ServicesManager sharedInstance].telephonyService cancelOutgoingCall];
    }];
    
    alert.customViewColor = [UITools defaultTintColor];
    alert.iconTintColor = [UITools defaultBackgroundColor];
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = NO;
    
    [alert showWaiting:controller title:[NSString stringWithFormat:NSLocalizedString(@"Calling %@", nil), phoneNumber.number] subTitle:NSLocalizedString(@"Please wait until the call is established...", nil) closeButtonTitle:nil duration:0.0];

    [[ServicesManager sharedInstance].telephonyService makeCallTo:phoneNumber fallBackHandler:^(NSError * _Nonnull error, PhoneNumber * _Nonnull phoneNumber) {
        [alert hideView];
        NSString *telWithoutSpaces = [phoneNumber.number stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *numberURL = [NSString stringWithFormat:@"telprompt://%@", telWithoutSpaces];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:numberURL] options:@{} completionHandler:nil];
    } completionHandler:^(NSError * _Nonnull error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [alert hideView];
            if (error) {
                // To do : parse callCause, e.g. "DESTNOTOBTAINABLE"
                NSLog(@"Make call failed with cause '%@'", error.userInfo[NSLocalizedFailureReasonErrorKey]);
                [UITools showErrorPopupWithTitle:NSLocalizedString(@"Outgoing call error", nil) message:[NSString stringWithFormat:NSLocalizedString(@"An error occurred while calling %@", nil), phoneNumber.number] inViewController:controller];
            }
            else
                NSLog(@"Make call done");
        });
    }];
}
#endif

+(NSString *) trimAndRemoveUnicodeFormatFromNumber:(NSString *) number {
    
    // Remove unwanted charaters for example \U0000202d wich which are added during a copy paste
    number = [number stringByReplacingOccurrencesOfString:@"\\p{Cf}" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [number length])];
    // Trim whitespace dot and dash
    number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    return number;
}
@end
