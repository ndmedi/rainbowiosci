/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Rainbow/Rainbow.h>
#import <Rainbow/CalendarPresence.h>
#import "UIRainbowGenericTableViewCell.h"
#import "UITools.h"
#import <Rainbow/Peer.h>
#import <Rainbow/NSDate+Utilities.h>
#import <Rainbow/NSDate+Distance.h>
#import "NSDate+MediumFormat.h"
#import "UIView+MGBadgeView.h"

@interface UIAvatarView (Private)
@property (nonatomic, readonly) UIImageView *contactPresenceView;
@end

static int mainLabelLeftConstraintDefaultValue = 14;
static int mainLabelLeftConstraintWithIconValue = 34;
static int twoLabelsTopConstraintValue = 16;
static int threeLabelsTopConstraintValue = 20;

@interface UIRainbowGenericTableViewCell ()
@property (strong, nonatomic) IBOutlet UIAvatarView *avatarView;
@property (weak, nonatomic) IBOutlet UIImageView *maskView;

@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mainLabelTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mainLabelLeftConstraint;
@property (weak, nonatomic) IBOutlet UILabel *midLabel;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topRightLabelTopConstraint;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIImageView *leftIcon;
@property (weak, nonatomic) IBOutlet UIImageView *rightIcon;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *rightIconLeadingContraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *rightIconLeadingContraintMid;
@property (nonatomic, strong) NSTimer *refreshTopRightLabelTimer;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *rightButtonLeadingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *rightButtonLeadingConstraintMid;
- (IBAction)didTapRightButton:(UIButton *)sender;

@property (nonatomic, strong) UIColor *defaultMaskedViewColor;

@property (nonatomic) BOOL isAbleToRefreshUI;

@end

@implementation UIRainbowGenericTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    [UITools applyCustomFontTo: _mainLabel];
    [UITools applyCustomFontTo: _midLabel];
    [UITools applyCustomFontTo: _subLabel];
    [UITools applyCustomFontTo: _dateLabel];
    [_rightButton setTintColor:[UITools defaultTintColor]];
    [_rightButton setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
    [_rightButton setTitleColor:[UITools defaultTintColor] forState:UIControlStateHighlighted];
    [UITools applyCustomFontTo:_rightButton.titleLabel];
    _rightButton.contentEdgeInsets = UIEdgeInsetsMake(0, -20, 0, -20);
    _rightButton.hidden = YES;
    _rightIcon.tintColor = [UITools defaultTintColor];
    
    [self setTintColor:[UITools defaultTintColor]];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor whiteColor];
    
    [self.avatarView setAsCircle:YES];
    
    _maskView.layer.cornerRadius = _maskView.frame.size.width/2.0;
    _maskView.layer.masksToBounds = YES;
    _maskView.tintColor = [UIColor whiteColor];
    _maskView.backgroundColor = [UITools colorFromHexa:0x0085CA66];
    _maskView.hidden = NO;
    _maskView.layer.shadowColor = [UIColor blackColor].CGColor;
    _maskView.layer.shadowOffset = CGSizeMake(0, 1);
    _maskView.layer.shadowOpacity = 1;
    _maskView.layer.shadowRadius = 1.0;
    _maskView.clipsToBounds = NO;
    
    _defaultMaskedViewColor = [UITools colorFromHexa:0x0085CA66];
    
    [self.avatarView bringSubviewToFront:_maskView];
    [self.avatarView bringSubviewToFront:self.avatarView.contactPresenceView];
    
    self.avatarView.badgeView.badgeColor = [UITools notificationBadgeColor];
    self.avatarView.badgeView.outlineWidth = 0.0;
    self.avatarView.badgeView.position = MGBadgePositionTopRight;
    self.avatarView.badgeView.textColor = [UIColor whiteColor];
    self.avatarView.badgeView.font = [UIFont fontWithName:[UITools defaultFontName] size:13];
    self.avatarView.badgeView.verticalOffset = 7;
    self.avatarView.badgeView.horizontalOffset = -7;
    self.avatarView.badgeView.displayIfZero = NO;
    self.avatarView.badgeView.bageStyle = MGBadgeStyleRoundRect;
    self.avatarView.badgeView.padding = 5;
    self.avatarView.badgeView.isAccessibilityElement = YES;
    self.avatarView.badgeView.accessibilityLabel = @"badgeText";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContactDisplaySettings:) name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isAbleToRefreshUIChanged:) name:kIsAbleToRefreshUIChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endReceiveXMPPRequestsAfterResume:) name:kEndReceiveXMPPRequestsAfterResume object:nil];
    
    _rightButtonLeadingConstraint.active = NO;
    _rightButtonLeadingConstraintMid.active = NO;
    _rightIconLeadingContraint.active = NO;
    _rightIconLeadingContraintMid.active = NO;
    self.accessoryType = UITableViewCellAccessoryNone;
    
    _showMidLabel = NO;
    _isAbleToRefreshUI = YES;
}

-(void) dealloc {
    if(_refreshTopRightLabelTimer){
        [_refreshTopRightLabelTimer invalidate];
        _refreshTopRightLabelTimer = nil;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kIsAbleToRefreshUIChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kEndReceiveXMPPRequestsAfterResume object:nil];
    
    [_cellObject.observablesKeyPath enumerateObjectsUsingBlock:^(NSString * keyPath, NSUInteger idx, BOOL * stop) {
        [_cellObject removeObserver:self forKeyPath:keyPath context:nil];
    }];
}

-(void) prepareForReuse {
    [super prepareForReuse];
    
    [self setRightButtonVisible: NO];
    _mainLabelLeftConstraint.constant = mainLabelLeftConstraintDefaultValue;
    [self setCalendarPresenceIcon];
//    
//    [self setMainLabelTextColor];
//    [self setMainLabelFont];
}

// UI Setters
-(void) setMainText:(NSString *)mainLabel {
    _mainLabel.text = mainLabel;
}

-(void) setMainLabelTextColor {
    if([_cellObject respondsToSelector:@selector(mainLabelTextColor)])
        _mainLabel.textColor = _cellObject.mainLabelTextColor;
    else
        _mainLabel.textColor = [UIColor blackColor];
}

-(void) setMainLabelFont {
    if([_cellObject respondsToSelector:@selector(mainLabelFont)])
        _mainLabel.font = _cellObject.mainLabelFont;
    else
        [UITools applyCustomFontTo:_mainLabel];
}

-(void) setMainLabelVCentered:(BOOL)centered {
    if(centered) {
        _mainLabelTopConstraint.constant = 0;
    } else {
        _mainLabelTopConstraint.constant = (_midLabel.text.length > 0) ? -threeLabelsTopConstraintValue : -twoLabelsTopConstraintValue;
    }
    _midLabel.hidden = centered;
    _subLabel.hidden = centered;
}

-(void) setMidText:(NSString *)midLabel {
    _midLabel.text = midLabel;
}

-(void) setMidLabelTextColor {
    if([_cellObject respondsToSelector:@selector(midLabelTextColor)])
        _midLabel.textColor = _cellObject.midLabelTextColor;
    else
        _midLabel.textColor = [UIColor lightGrayColor];
}

-(void) setSubText:(NSString *)subLabel {
    _subLabel.text = subLabel;
}

-(void) setDateText:(NSDate *)date {
    if(date){
        _dateLabel.hidden = NO;
        if([date isToday]){
            _dateLabel.text = [date shortTimeString];
            _refreshTopRightLabelTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 + [[date dateAtEndOfDay] timeIntervalSinceNow] target:self selector:@selector(updateDate) userInfo:nil repeats:NO];
        } else if (date.year == [NSDate date].year){
            _dateLabel.text = [date mediumDateStringNoYear];
            _refreshTopRightLabelTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 + [[date dateAtEndOfYear] timeIntervalSinceNow] target:self selector:@selector(updateDate) userInfo:nil repeats:NO];
        } else {
            _dateLabel.text = [date mediumDateString];
        }
    } else {
        [_refreshTopRightLabelTimer invalidate];
        _dateLabel.hidden = YES;
    }
   
}

-(void) setDateLabelTextColor {
    if([_cellObject respondsToSelector:@selector(dateLabelTextColor)]){
        _dateLabel.textColor = _cellObject.dateLabelTextColor;
    } else {
        _dateLabel.textColor = [UIColor lightGrayColor];
    }
}

-(void) setDateLabelVCenter:(BOOL)centered {
    if(centered && !_rightIcon) {
        _topRightLabelTopConstraint.constant = 0;
    } else {
        _topRightLabelTopConstraint.constant = (_midLabel.text.length > 0) ? -threeLabelsTopConstraintValue+1 : -twoLabelsTopConstraintValue+1;
    }
}

-(void) udpateLabelsPositions {
    BOOL centered = NO;
    if([_cellObject respondsToSelector:@selector(midLabel)]) {
        centered = ((!_cellObject.midLabel || _cellObject.midLabel.length == 0 || !_showMidLabel) && (!_cellObject.subLabel || _cellObject.subLabel.length == 0));
    } else {
        centered = ((!_cellObject.subLabel || _cellObject.subLabel.length == 0));
    }
    
    [self setMainLabelVCentered: centered ]; // If midLabel AND subLabel are empty, set mainLabel to center
    [self setDateLabelVCenter: centered];
}

-(void) setAvatar: (Peer *)peer {
    _avatarView.peer = peer;
}

-(void) updateCellButtonContent {
    
    if( _cellObject.cellButtonTitle != nil) {
        _rightButton.hidden = NO;
        [_rightButton setImage:nil forState:UIControlStateNormal];
        [_rightButton setImage:nil forState:UIControlStateHighlighted];
        [_rightButton setTitle:_cellObject.cellButtonTitle forState:UIControlStateNormal];
    } else if( _cellObject.cellButtonImage != nil) {
        _rightButton.hidden = NO;
        [_rightButton setTitle:nil forState:UIControlStateNormal];
        [_rightButton setImage:_cellObject.cellButtonImage forState:UIControlStateNormal];
        [_rightButton setImage:_cellObject.cellButtonImage forState:UIControlStateHighlighted];
        
        if( [_cellObject respondsToSelector:@selector(cellButtonImageColor)] && _cellObject.cellButtonImageColor != nil ) {
            _rightButton.tintColor = _cellObject.cellButtonImageColor;
        } else {
            _rightButton.tintColor = [UITools defaultTintColor];
        }
    } else {
        _rightButton.hidden = YES;
        [_rightButton setImage:nil forState:UIControlStateNormal];
        [_rightButton setImage:nil forState:UIControlStateHighlighted];
        [_rightButton setTitle:nil forState:UIControlStateNormal];
        [_rightButton setTitle:nil forState:UIControlStateHighlighted];
    }
    [self setCellButtonTapHandler:_cellButtonTapHandler];
}

-(void)updateContent {
    // Do not refresh UI if we are still receiving XMPP messages after a resume
    if (!_isAbleToRefreshUI)
        return;
    
    // Set labels content
    [self setMidText:(([_cellObject respondsToSelector:@selector(midLabel)] && _showMidLabel) ? _cellObject.midLabel : @"")];
    [self setSubText:_cellObject.subLabel];
    [self setMainText:_cellObject.mainLabel];
    // Update layouts
    [self udpateLabelsPositions];
    [self setMainLabelTextColor];
    [self setMainLabelFont];
    [self setMidLabelTextColor];
    [self setDateLabelTextColor];
    // Right labels
    [self setDateText:_cellObject.date];
    [self setBadge:_cellObject.badgeValue];
    [self updateCellButtonContent];
    // icon
    [self setRightIconImage: _cellObject.rightIcon];
    [self setCalendarPresenceIcon];
    if([_cellObject respondsToSelector:@selector(showMaskedView)])
        [self showHideMaskView:_cellObject.showMaskedView];
    else
        [self showHideMaskView:NO];
    if([_cellObject respondsToSelector:@selector(maskedImage)])
        [self maskImage:_cellObject.maskedImage];
    else
        [self maskImage:nil];
    [self defineMaskedViewColor];
}

// Content from Object
-(void)setCellObject:(NSObject<UIRainbowGenericTableViewCellProtocol> *)cellObject {    
    // Reset to default values
    [self setRightButtonVisible: NO];
    _mainLabelLeftConstraint.constant = mainLabelLeftConstraintDefaultValue;
    _leftIcon.image = nil;
    _leftIcon.hidden = YES;
    _mainLabel.textColor = [UIColor blackColor];
    [UITools applyCustomFontTo:_mainLabel];
    
    [_cellObject.observablesKeyPath enumerateObjectsUsingBlock:^(NSString * keyPath, NSUInteger idx, BOOL * stop) {
        [_cellObject removeObserver:self forKeyPath:keyPath context:nil];
    }];
    
    _cellObject = nil;
    _cellObject = cellObject;
    
    [cellObject.observablesKeyPath enumerateObjectsUsingBlock:^(NSString * keyPath, NSUInteger idx, BOOL * stop) {
        [cellObject addObserver:self forKeyPath:keyPath options:NSKeyValueObservingOptionNew context:nil];
    }];
    
    // Avatar
    [self setAvatar:_cellObject.avatar];
    
    [self updateContent];
}

-(void) setCellButtonTapHandler:(RightButtonTapHandler)cellButtonTapHandler {
    _cellButtonTapHandler = cellButtonTapHandler;
    if(cellButtonTapHandler){
        _rightButton.hidden = NO;
        _rightButtonLeadingConstraint.active = YES;
        _rightButtonLeadingConstraintMid.active = YES;
    } else {
        _rightButton.hidden = YES;
        _rightButtonLeadingConstraint.active = NO;
        _rightButtonLeadingConstraintMid.active = NO;
    }
}

-(void) showHideMaskView:(BOOL) show {
    _maskView.hidden = !show;
}

-(void) maskImage:(UIImage *) image {
    _maskView.image = image;
}

-(void) defineMaskedViewColor {
    if([_cellObject respondsToSelector:@selector(maskedViewColor)])
        _maskView.backgroundColor = _cellObject.maskedViewColor;
    else
        _maskView.backgroundColor = _defaultMaskedViewColor;
}

-(void) setBadge: (NSInteger) value {
    if(value > 10) {
        self.avatarView.badgeView.bageStyle = MGBadgeStyleRoundRect;
        self.avatarView.badgeView.padding = 5;
    } else {
        self.avatarView.badgeView.bageStyle = MGBadgeStyleEllipse;
        self.avatarView.badgeView.padding = 0;
    }
    self.avatarView.badgeView.badgeValue = value;
    self.avatarView.badgeView.accessibilityValue = [NSString stringWithFormat:@"%ld",(long)value];
}

-(void) setRightButtonVisible: (BOOL) visible {
    self.rightButton.hidden = !visible;
}

-(void) setRightIconImage: (UIImage *) image {
    if( image == nil ) {
        self.rightIcon.hidden = YES;
        self.rightIconLeadingContraint.active = NO;
        self.rightIconLeadingContraintMid.active = NO;
        self.rightIcon.image = nil;
        
    } else {
        self.rightIcon.hidden = NO;
        self.rightIconLeadingContraint.active = YES;
        self.rightIconLeadingContraintMid.active = YES;
        self.rightIcon.image = image;
        self.rightIcon.tintColor = _cellObject.rightIconTintColor;
    }
}

-(void) setCalendarPresenceIcon {
    if(![_cellObject.avatar isKindOfClass:[Contact class]]){
        _mainLabelLeftConstraint.constant = mainLabelLeftConstraintDefaultValue;
        [self setMainLabelTextColor];
        _leftIcon.image = nil;
        _leftIcon.hidden = YES;
        return;
    }
    
    Contact *contact = (Contact *)_cellObject.avatar;
//    NSLog(@"Set Calendar presence icon for %@ presence %@", contact.jid, contact.calendarPresence);
    // Calendar presence
    if( contact.calendarPresence.presence == CalendarPresenceOutOfOffice || contact.calendarPresence.automaticReply.isEnabled) {
        _mainLabelLeftConstraint.constant = mainLabelLeftConstraintWithIconValue;
        _leftIcon.image = [UIImage imageNamed:@"outOfOffice"];
        _leftIcon.hidden = NO;
    } else if( contact.calendarPresence.presence == CalendarPresenceBusy ) {
        _mainLabelLeftConstraint.constant = mainLabelLeftConstraintWithIconValue;
        _leftIcon.image = [UIImage imageNamed:@"inMeeting"];
        _leftIcon.hidden = NO;
    } else {
        _mainLabelLeftConstraint.constant = mainLabelLeftConstraintDefaultValue;
        _leftIcon.image = nil;
        _leftIcon.hidden = YES;
    }
    
    [self setMainLabelTextColor];
}

-(void)updateDate {
    [self setDateText:_cellObject.date];
}

-(void) changeContactDisplaySettings:(NSNotification *) notification {
    if(![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self changeContactDisplaySettings:notification];
        });
        return;
    }
    [self updateContent];
}

-(void) didUpdateContact: (NSNotification *) notification {
    NSDictionary *userInfo = notification.object;
    Contact *contact = [userInfo objectForKey:kContactKey];
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
    BOOL needUpdate = NO;
    if (contact == _cellObject.avatar) {
        if ([changedKeys containsObject:@"photoData"] ||
            [changedKeys containsObject:@"presence"] ||
            [changedKeys containsObject:@"isConnectedWithMobile"] ||
            [changedKeys containsObject:@"sentInvitation"] ||
            [changedKeys containsObject:@"isPresenceSubscribed"] ||
            [changedKeys containsObject:@"isInRoster"]
            ) {
            // They are monitored by the avatar view directly
        }
        if([changedKeys containsObject:@"displayName"])
            needUpdate = YES;
        
        
        if([changedKeys containsObject:@"calendarPresence"])
            needUpdate = YES;
    }
    
    
    if(needUpdate){
        if(![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setMainText:_cellObject.mainLabel];
                [self setCalendarPresenceIcon];
            });
            return;
        } else {
            [self setMainText:_cellObject.mainLabel];
            [self setCalendarPresenceIcon];
        }
    }
}

-(void) didUpdateRoom: (NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateRoom:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Room *room = [userInfo objectForKey:kRoomKey];
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
    
    if([_cellObject.avatar isEqual:room]){
        _leftIcon.image = nil;
        _leftIcon.hidden = YES;
        _mainLabelLeftConstraint.constant = mainLabelLeftConstraintDefaultValue;
        [self setMainLabelTextColor];
        
        if([changedKeys containsObject:@"name"]||[changedKeys containsObject:@"conference"]){
            [self setMainText:_cellObject.mainLabel];
            [self setMainLabelTextColor];
        }
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        });
        return;
    }
    if([_cellObject.observablesKeyPath containsObject:keyPath]){
        [self updateContent];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [_avatarView setSelected:selected animated:animated];
    [self defineMaskedViewColor];
}

-(void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    [_avatarView setHighlighted:highlighted animated:animated];
    [self defineMaskedViewColor];
}

- (IBAction)didTapRightButton:(UIButton *)sender {
    if(_cellButtonTapHandler) {
        _cellButtonTapHandler(sender);
    }
}

#pragma mark - XMPPService notification
-(void) isAbleToRefreshUIChanged:(NSNotification *) notification {
    _isAbleToRefreshUI = [[notification.userInfo objectForKey:@"isAbleToRefreshUI"] boolValue];
}

-(void) endReceiveXMPPRequestsAfterResume:(NSNotification *)notification {
    OTCLog(@"No more XMPP requests after resume - reload content");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateContent];
    });
}

@end

