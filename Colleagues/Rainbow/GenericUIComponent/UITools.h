/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


#import <Rainbow/Presence.h>
#import <Rainbow/Contact.h>
#import "MBProgressHUD.h"
#import <Rainbow/Company.h>
#import <UIKit/UIKit.h>

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_WIDESCREEN ( IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6P || IS_IPAD )


@interface UITools : NSObject
+(void) customizeAppAppearanceWithGlobalColor:(UIColor *) globalColor;
+(UIColor *) colorFromHexa:(int)hexaCode;
+(UIColor *) defaultTintColor;
+(UIColor *) defaultBackgroundColor;
+(UIColor *) notificationBadgeColor;
+(UIColor *) foregroundGrayColor;
+(UIColor *) backgroundGrayColor;
+(UIColor *) redColor;

+(NSString *) defaultFontName;
+(NSString *) boldFontName;
+(NSString *) thinFontName;

+(void) applyCustomFontTo:(UILabel*)lbl;
+(void) applyCustomFontToTextField:(UITextField*)textField;
+(void) applyCustomBoldFontTo:(UILabel*)lbl;
+(void) applyThinCustomFontTo:(UILabel*)lbl;

#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
#endif

+(UIImage *)imageFromColor:(UIColor *)color;
+(UIColor *)readableForegroundColorForBackgroundColor:(UIColor*)backgroundColor;
+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize circular:(BOOL)isCircular;
+(UIColor *) colorForPresence:(Presence *) presence;


#if !(TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
+ (NSRect)adjustedFrameToVerticallyCenterText:(NSRect)rect;
#endif

+(NSString *) formatTimeForDate:(NSDate *) date;
+(NSString *) formatDateTimeForDate:(NSDate *) date;
+(NSString *) shortFormatForDate:(NSDate *) date;
+(NSString *) optimizedFormatForDate:(NSDate *) date;
+(NSString *) optimizedFormatForCalendarPresenceDate:(NSDate *) date;

+(UIColor*) colorForString:(NSString*) string;

+(NSSortDescriptor *) sortDescriptorForContactByLastName;
+(NSSortDescriptor *) sortDescriptorForContactByFirstName;
+(NSSortDescriptor *) sortDescriptorForParticipantByLastName;
+(NSSortDescriptor *) sortDescriptorForParticipantByFirstName;
+(NSSortDescriptor *) sortDescriptorForGroupByName;
+(NSSortDescriptor *) sortDescriptorByLastMessageDate;
+(NSSortDescriptor *) sortDescriptorByDisplayName;
+(void) sizeButtonToText:(UIButton *)button availableSize:(CGSize)availableSize padding:(UIEdgeInsets)padding;

+(CGSize) findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font;
+(NSString *)valueForKey:(NSString *)key fromQueryItems:(NSArray *)queryItems;



+(MBProgressHUD *) showHUDWithMessage:(NSString *) message forView:(UIView *) view mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion;
+(MBProgressHUD *) showHUDWithMessage:(NSString *) message forView:(UIView *) view mode:(MBProgressHUDMode) mode imageName:(NSString *) imageName dismissCompletionBlock:(void (^)()) dismissCompletion;
+(MBProgressHUD *) showHUDWithMessage:(NSString *) message detailedMessage:(NSString *) detailedMessage forView:(UIView *) view mode:(MBProgressHUDMode) mode imageName:(NSString *) imageName dismissCompletionBlock:(void (^)()) dismissCompletion;
+(MBProgressHUD *) showHUDWithMessage:(NSString *) message detailedMessage:(NSString *) detailedMessage mode:(MBProgressHUDMode) mode imageName:(NSString *) imageName dismissCompletionBlock:(void (^)()) dismissCompletion;
#if !defined(APP_EXTENSION)
+(void) showInvitePopupForContact:(Contact*) contact inViewController:(UIViewController*) controller withCompletionHandler:(void(^)())completionHandler;
+(void) showInvitePopupForAllContacts:(UIViewController*) controller withCompletionHandler:(void(^)())completionHandler;
+(void) showRequestJoinCompanyPopupForCompany:(Company *) company inViewController:(UIViewController*) controller actionTarget:(id) target actionSelector:(SEL) selector;
+(void) showErrorPopupWithTitle:(NSString *) title message:(NSString *) message inViewController:(UIViewController*) controller;
+(void) showInfoPopupWithTitle:(NSString *) title message:(NSString *) message inViewController:(UIViewController*) controller;
#endif

+(BOOL)isForceTouchAvailableForTraitCollection:(UITraitCollection *)traitCollection;
+(NSString *)timeFormatConvertToSeconds:(NSString *)timeSecs zeroFormattingBehaviorunitsStyle:(NSDateComponentsFormatterZeroFormattingBehavior) zeroFormattingBehavior unitsStyle: (NSDateComponentsFormatterUnitsStyle) unitsStyle;
/**
 Returns the URL to the application's Documents directory.
 */
+(NSString *)applicationDocumentsCache;
#if !defined(APP_EXTENSION)
+(void) showMicrophoneBlockedPopupInController:(UIViewController *) controller;
#endif
+(UIImage *)convertImageToGrayScale:(UIImage *)image;

+(NSString *) getPresenceText:(Contact *) contact;

+(void) showInviteContactAlertController:(Contact *) contact fromController:(UIViewController *) controller controllerDelegate:(id) controllerDelegate withCompletionHandler:(void(^)(Invitation *invitation))completionHandler;

+(void) showSMSViewControllerForInvitation:(Invitation *) invitation inController:(UIViewController *) controller controllerDelegate:(id) controllerDelegate;

+(void)openImagePicker:(UIImagePickerControllerSourceType)source delegate:(id <UINavigationControllerDelegate, UIImagePickerControllerDelegate>) delegate presentingViewController:(UIViewController *) presentingViewController;
+(NSString *) generateUniquePassword;
+(void) defineApplicationIDForRainbow;
#if !defined(APP_EXTENSION)
+(void) makeCallToPhoneNumber:(PhoneNumber *) phoneNumber inController:(UIViewController *) controller;
#endif
+(NSString *) trimAndRemoveUnicodeFormatFromNumber:(NSString *) number;
@end
