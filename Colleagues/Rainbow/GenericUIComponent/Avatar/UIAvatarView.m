/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIAvatarView.h"
#import <QuartzCore/QuartzCore.h>
#import <Rainbow/ServicesManager.h>
#import "UITools.h"
#import "UIImageView+Letters.h"
#import <Rainbow/EmailAddress.h>
#import "Contact+Extensions.h"
#import <Rainbow/NSDate+Utilities.h>

#define kAvatarDefaultCornerRadius 8
#define kContactPresenceViewWidth 20
#define kContactPresencePaddingLeft 4
#define kContactPresencePaddingTop 1

#define CLAMP(x, low, high) ({\
__typeof__(x) __x = (x); \
__typeof__(low) __low = (low);\
__typeof__(high) __high = (high);\
__x > __high ? __high : (__x < __low ? __low : __x);\
})

static NSString* noAvatarImageName = @"DefaultRoomAvatar";
static UIImage* localContactImage = nil;

@interface UIAvatarView ()
@property (nonatomic, strong) UIImageView* contactPictureView;
@property (nonatomic, strong) UIImageView *contactPresenceView;
@property (nonatomic, strong) UIImage *originalImage;
@property (nonatomic) BOOL isAbleToRefreshUI;
@end

@implementation UIAvatarView

@synthesize asCircle = _asCircle;
@synthesize peer = _peer;

-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        [self commonInit];
    }
    return self;
}

-(void) commonInit {
    _cornerRadius = 8;
    CGFloat margin = 0;
    
    _contactPictureView = [[UIImageView alloc] initWithFrame:CGRectMake(margin,margin, self.frame.size.width-2*margin, self.frame.size.height-2*margin)];
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    _contactPictureView.contentMode = UIViewContentModeScaleAspectFill;
    _contactPictureView.clipsToBounds = YES;
#endif
    _contactPictureView.image = [UIImage imageNamed:noAvatarImageName];
    _contactPictureView.tintColor = [UIColor lightGrayColor];
    _contactPictureView.layer.masksToBounds = YES;
    _contactPictureView.layer.cornerRadius = _cornerRadius;
    _contactPictureView.layer.borderColor = [UIColor whiteColor].CGColor;
    [self addSubview:_contactPictureView];
    
    _contactPresenceView = [[UIImageView alloc] initWithFrame:CGRectMake(_contactPictureView.frame.size.width - kContactPresenceViewWidth + kContactPresencePaddingLeft, _contactPictureView.frame.size.height - kContactPresenceViewWidth + kContactPresencePaddingTop, kContactPresenceViewWidth, kContactPresenceViewWidth)];
    _contactPresenceView.backgroundColor = [UIColor purpleColor];
    _contactPresenceView.layer.masksToBounds = YES;
    _contactPresenceView.layer.cornerRadius = _contactPresenceView.frame.size.width/2.0;
    
    // Accessibility stuff
    _contactPictureView.isAccessibilityElement = YES;
    _contactPictureView.accessibilityLabel = @"Avatar";
    _contactPresenceView.isAccessibilityElement = YES;
    _contactPresenceView.accessibilityLabel = @"AvatarPresenceIcon";
    
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    _contactPresenceView.contentMode = UIViewContentModeScaleAspectFill;
    _contactPresenceView.clipsToBounds = YES;
#endif
    [self addSubview:_contactPresenceView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateMyContact object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveContact:) name:kContactsManagerServiceDidRemoveContact object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isAbleToRefreshUIChanged:) name:kIsAbleToRefreshUIChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endReceiveXMPPRequestsAfterResume:) name:kEndReceiveXMPPRequestsAfterResume object:nil];

    
    _showPresence = YES;
    _blackAndWhite = NO;
    _originalImage = nil;
    _isAbleToRefreshUI = YES;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateMyContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidRemoveContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kIsAbleToRefreshUIChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kEndReceiveXMPPRequestsAfterResume object:nil];

    // release peer and remove KVOs
    [self setPeer:nil];
}


-(void) adjustFrame {
    // Adjust presence view
    float ratio = _contactPictureView.frame.size.width / 57.0f;
    int offset = (_contactPresenceView.layer.borderWidth>0) ? _contactPresenceView.layer.borderWidth -2 : 0;
    
    // Define min & max size from default to 25% bigger
    int clampedInt = CLAMP(ratio*kContactPresenceViewWidth, kContactPresenceViewWidth, kContactPresenceViewWidth*1.25);
    
    CGRect newFrame = _contactPresenceView.frame;
    newFrame.size.width = clampedInt;
    newFrame.size.height = clampedInt;
    newFrame.origin.x = _contactPictureView.frame.size.width - (newFrame.size.width) + offset + kContactPresencePaddingLeft;
    newFrame.origin.y = _contactPictureView.frame.size.height - (newFrame.size.height) + offset;
    
    _contactPresenceView.frame = newFrame;
    _contactPresenceView.layer.cornerRadius = _contactPresenceView.frame.size.width/2.0;
}

#pragma mark - getter/setter

+(void) setNoAvatarImageName:(NSString *)imageName {
    noAvatarImageName = nil;
    noAvatarImageName = imageName;
}

+(void) setLocalContactImageName:(NSString *)imageName {
    localContactImage = nil;
    localContactImage = [UIImage imageNamed:imageName];
}

-(void) setBlackAndWhite:(BOOL)blackAndWhite {
    if(_blackAndWhite == blackAndWhite){
        return;
    }
    _blackAndWhite = blackAndWhite;
    if(_blackAndWhite){
        self.originalImage = [UIImage imageWithCGImage:_contactPictureView.image.CGImage];
        _contactPictureView.image = [UITools convertImageToGrayScale:_contactPictureView.image];
    } else {
        if(self.originalImage){
            _contactPictureView.image = self.originalImage;
        }
    }
}

-(void) setAsCircle:(BOOL)circle {
    _asCircle = circle;
    CGRect f = _contactPictureView.frame;
    self.layer.cornerRadius = _asCircle?f.size.width/2.0:_cornerRadius;
    _contactPictureView.layer.cornerRadius = _asCircle?f.size.width/2.0:_cornerRadius;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    [self setNeedsDisplay];
#else
    self.layer.masksToBounds = YES;
    _contactPictureView.layer.masksToBounds = YES;
    [self setNeedsDisplay:YES];
#endif
}

-(BOOL) asCircle {
    return _asCircle;
}

-(void) setWithBorder:(BOOL)withBorder {
    _withBorder = withBorder;
    _contactPictureView.layer.borderWidth = (withBorder) ? 3.0f : 0;
}

-(void) setCornerRadius:(NSInteger)cornerRadius {
    _cornerRadius = cornerRadius;
    [self setAsCircle:_asCircle];
}

-(void) setShowPresence:(BOOL)showPresence {
    _showPresence = showPresence;
    if(_showPresence && [_peer isKindOfClass:[Contact class]])
        [self setPresence:((Contact *)_peer).presence];
    else
        _contactPresenceView.hidden = YES;
}

#if !(TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
-(void) layoutSublayersOfLayer:(CALayer *)layer {
    CGRect f = _contactPictureView.frame;
    self.layer.cornerRadius = _asCircle?f.size.width/2.0:kAvatarDefaultCornerRadius;
    _contactPictureView.layer.cornerRadius = _asCircle?f.size.width/2.0:kAvatarDefaultCornerRadius;
    self.layer.masksToBounds = YES;
    _contactPictureView.layer.masksToBounds = YES;
}
#endif

-(void) layoutSubviews {
    // Do not refresh UI if we are still receiving XMPP messages after a resume
    if (!_isAbleToRefreshUI)
        return;
    
    CGFloat margin = 0.0;
    CGRect f = CGRectMake(margin, margin, self.frame.size.width-2*margin, self.frame.size.height-2*margin);
    _contactPictureView.frame = f;
    [self setAsCircle:_asCircle];
    
    if(_peer){
        [self setPhotoForPeer:_peer];
        
        if([_peer isKindOfClass:[Contact class]]) {
            if(_showPresence)
                [self setPresence:((Contact *)_peer).presence];
        } else if ([_peer isKindOfClass:[Room class]]) {
            _contactPresenceView.hidden = YES;
        }
    }
    
    // Adjust presence view frame
    [self adjustFrame];
    [self setBlackAndWhite:_blackAndWhite];
}

-(void) setPeer:(Peer *)peer {
    
    // First clean the previous peer
    if ([_peer isKindOfClass:[Contact class]]) {
        [_peer removeObserver:self forKeyPath:kContactLastNameKey context:nil];
        [_peer removeObserver:self forKeyPath:kContactFirstNameKey context:nil];
    }
    if ([_peer isKindOfClass:[Room class]]) {
        [_peer removeObserver:self forKeyPath:kRoomParticipantsKey];
    }
    
    _peer = nil;
    _peer = peer;
    
    if(_peer) {
        if([_peer isKindOfClass:[Contact class]]) {
            [_peer addObserver:self forKeyPath:kContactLastNameKey options:NSKeyValueObservingOptionNew context:nil];
            [_peer addObserver:self forKeyPath:kContactFirstNameKey options:NSKeyValueObservingOptionNew context:nil];
        } else if ([_peer isKindOfClass:[Room class]]) {
            [_peer addObserver:self forKeyPath:kRoomParticipantsKey options:NSKeyValueObservingOptionNew context:nil];
        }
    }
    [self setNeedsLayout];
}

-(UIImage *) cropImage:(UIImage *) image withRect:(CGRect) croprect {
    // The scale parameter might not be used here, but it works so...
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], CGRectMake(croprect.origin.x * image.scale,
                                                                                   croprect.origin.y * image.scale,
                                                                                   croprect.size.width * image.scale,
                                                                                   croprect.size.height * image.scale));
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return croppedImage;
}

// Copied from UIImageView+Letters
// Because we need to draw text on a UIImage, without the category constraints.
- (UIImage *)imageSnapshotFromText:(NSString *)text backgroundColor:(UIColor *)color textAttributes:(NSDictionary *)textAttributes {
    
    CGFloat scale = [UIScreen mainScreen].scale;
    
    CGSize size = self.bounds.size;
    if (self.contentMode == UIViewContentModeScaleToFill ||
        self.contentMode == UIViewContentModeScaleAspectFill ||
        self.contentMode == UIViewContentModeScaleAspectFit ||
        self.contentMode == UIViewContentModeRedraw)
    {
        size.width = floorf(size.width * scale) / scale;
        size.height = floorf(size.height * scale) / scale;
    }
    
    UIGraphicsBeginImageContextWithOptions(size, NO, scale);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //
    // Fill background of context
    //
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));
    
    //
    // Draw text in the context
    //
    CGSize textSize = [text sizeWithAttributes:textAttributes];
    CGRect bounds = self.bounds;
    
    [text drawInRect:CGRectMake(bounds.size.width/2 - textSize.width/2,
                                bounds.size.height/2 - textSize.height/2,
                                textSize.width,
                                textSize.height)
      withAttributes:textAttributes];
    
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return snapshot;
}


/**
 *  This function returns the correct UIImage for a given contact.
 *  It returns the photoData if exists, or a generated avatar with initials and 
 *  generated color.
 *
 *  @param contact The contact
 *
 *  @return The avatar UIImage
 */
-(UIImage *) UIImageForContact:(Contact *) contact {
    if(contact.photoData) {
        UIImage *image = [[UIImage alloc] initWithData:contact.photoData];
        if(contact.displayName)
            image.accessibilityValue = contact.displayName;
        else
            image.accessibilityValue = @"No name";
        return image;
    }
    
    UIColor* color = [UITools colorForString:contact.displayName];
    UIImageView *blankView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 128, 128)];
    
    if(contact.initials.length == 2) {
        [blankView setImageWithString:contact.displayName color:color  circular:NO fontName:[UITools defaultFontName]];
    } else {
        [blankView setImageWithString:contact.initials color:color  circular:NO fontName:[UITools defaultFontName]];
    }
    
    blankView.image.accessibilityValue = @"Initials";
    return blankView.image;
}

-(void) setAvatarFromData:(NSData*) data {
    _contactPictureView.image = [UIImage imageWithData: data];
    _contactPictureView.accessibilityValue = _contactPictureView.image.accessibilityValue;
}

/**
 *  This function compute the correct avatar for a given Peer (Contact or Room)
 *
 *  @param peer The peer.
 */
-(void) setPhotoForPeer:(Peer *) peer {
    if ([peer isKindOfClass:[Contact class]]) {
        Contact *contact = (Contact *) peer;
        _contactPictureView.image = [self UIImageForContact:contact];
        _contactPictureView.accessibilityValue = _contactPictureView.image.accessibilityValue;
        
    } else if ([peer isKindOfClass:[Room class]]) {
        
        Room *room = (Room *)peer;
        
        // If defined, display the custom avatar
        if( room.photoData != nil ) {
            UIImage *image = [[UIImage alloc] initWithData:room.photoData];
            _contactPictureView.image = image;
            _contactPictureView.accessibilityValue = _contactPictureView.image.accessibilityValue;
            return;
        }
        
        // Build the avatar only with the accepted participants only if it's not my room else use all participants list
        NSMutableOrderedSet<Participant *> *participantsToDisplay = nil;
        
        if(room.isMyRoom)
            participantsToDisplay = [NSMutableOrderedSet orderedSetWithArray:room.participants];
        else {
            participantsToDisplay = [NSMutableOrderedSet orderedSetWithArray:[[room participants] filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(Participant *evaluatedObject, NSDictionary<NSString *,id> * bindings) {
                return evaluatedObject.status == ParticipantStatusAccepted;
            }]]];
            
            Participant *myParticipant = [room participantFromContact:[ServicesManager sharedInstance].myUser.contact];
            if(myParticipant && ![participantsToDisplay containsObject:myParticipant]){
                // Not in participant list add myself in this list
                [participantsToDisplay addObject:myParticipant];
            }
        }
        
        [participantsToDisplay sortUsingComparator:^NSComparisonResult(Participant *obj1, Participant *obj2) {
            if([obj1.addedDate isLaterThanDate:obj2.addedDate])
                return NSOrderedDescending;
            return NSOrderedAscending;
        }];
        
        [self drawPhotoFromOrderedList:participantsToDisplay];
    }
}

-(void) drawPhotoFromOrderedList:(NSOrderedSet*) orderedList {
    if ([orderedList count] == 0) {
        _contactPictureView.image = [UIImage imageNamed:noAvatarImageName];
        _contactPictureView.accessibilityValue = @"No avatar";
        return;
    }
    if ([orderedList count] == 1) {
        id obj = orderedList[0];
        if([obj isKindOfClass:[Participant class]])
            [self setPhotoForPeer:((Participant*)obj).contact];
        else
            [self setPhotoForPeer:obj];
        return;
    }
    
    CGSize newSize = CGSizeMake(128, 128);
    
    if ([orderedList count] == 2) {
        UIImage *img1, *img2 = nil;
        id obj0 = orderedList[0];
        id obj1 = orderedList[1];
        if([obj0 isKindOfClass:[Participant class]] && [obj1 isKindOfClass:[Participant class]]){
            // This will open its own CG context.
            img1 = [self UIImageForContact:((Participant*)obj0).contact];
            img2 = [self UIImageForContact:((Participant*)obj1).contact];
        } else {
            // This will open its own CG context.
            img1 = [self UIImageForContact:obj0];
            img2 = [self UIImageForContact:obj1];
        }
        
        // Generate a accessibility value like
        // Initials|Initials if both contact have no avatar
        // John Doe|Initials if only first contact have an avatar
        // etc..
        _contactPictureView.accessibilityValue = [@[img1.accessibilityValue, img2.accessibilityValue] componentsJoinedByString:@"|"];
        
        // Now open our CG context for our image.
        UIGraphicsBeginImageContextWithOptions(newSize, NO, [UIScreen mainScreen].scale);
        
        // participant 1 : left half of the avatar
        CGRect rect1 = CGRectMake(img1.size.width/4, 0, img1.size.width/2, img1.size.height);
        UIImage *croppedImg1 = [self cropImage:img1 withRect:rect1];
        [croppedImg1 drawInRect:CGRectMake(0, 0, newSize.width/2, newSize.height)];
        
        // participant 2 : right half of the avatar
        CGRect rect2 = CGRectMake(img2.size.width/4, 0, img2.size.width/2, img2.size.height);
        UIImage *croppedImg2 = [self cropImage:img2 withRect:rect2];
        [croppedImg2 drawInRect:CGRectMake(newSize.width/2, 0, newSize.width/2, newSize.height)];
    }
    if ([orderedList count] >= 3) {
        
        UIImage *img1, *img2 , *img3 = nil;
        id obj0 = orderedList[0];
        id obj1 = orderedList[1];
        id obj2 = orderedList[2];
        if([obj0 isKindOfClass:[Participant class]] && [obj1 isKindOfClass:[Participant class]] && [obj2 isKindOfClass:[Participant class]]){
            // This will open its own CG context.
            img1 = [self UIImageForContact:((Participant*)obj0).contact];
            img2 = [self UIImageForContact:((Participant*)obj1).contact];
            img3 = [self UIImageForContact:((Participant*)obj2).contact];
        } else {
            // This will open its own CG context.
            img1 = [self UIImageForContact:obj0];
            img2 = [self UIImageForContact:obj1];
            img3 = [self UIImageForContact:obj2];
        }
        
        // Generate a accessibility value like
        // Initials|Initials|Initials if all 3 contacts have no avatar
        // John Doe|Initials|Initials if only first contact have an avatar
        // etc..
#if DEBUG
        _contactPictureView.accessibilityValue = [@[img1.accessibilityValue, img2.accessibilityValue, img3.accessibilityValue] componentsJoinedByString:@"|"];
#endif
        
        // Now open our CG context for our image.
        UIGraphicsBeginImageContextWithOptions(newSize, NO, [UIScreen mainScreen].scale);
        
        // participant 1 : left half of the avatar
        CGRect rect1 = CGRectMake(img1.size.width/4, 0, img1.size.width/2, img1.size.height);
        UIImage *croppedImg1 = [self cropImage:img1 withRect:rect1];
        [croppedImg1 drawInRect:CGRectMake(0, 0, newSize.width/2, newSize.height)];
        
        // participant 2 : right top quarter of the avatar
        [img2 drawInRect:CGRectMake(newSize.width/2, 0, newSize.width/2, newSize.height/2)];
        
        // participant 3 : right bottom quarter of the avatar
        [img3 drawInRect:CGRectMake(newSize.width/2, newSize.height/2, newSize.width/2, newSize.height/2)];
    }
    
    _contactPictureView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

-(void) didUpdateContact:(NSNotification *) notification {
    NSDictionary *userInfo = notification.object;
    Contact *contact = [userInfo objectForKey:kContactKey];
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
    
    BOOL needUpdate = NO;
    if (contact == _peer) {
        if ([changedKeys containsObject:@"photoData"] ||
            [changedKeys containsObject:@"presence"] ||
            [changedKeys containsObject:@"displayName"] ||
            [changedKeys containsObject:@"isConnectedWithMobile"] ||
            [changedKeys containsObject:@"sentInvitation"] ||
            [changedKeys containsObject:@"isPresenceSubscribed"] ||
            [changedKeys containsObject:@"isInRoster"]
        ) {
            needUpdate = YES;
        }
    }
    
    if ([_peer isKindOfClass:[Room class]]) {
        Room *room = (Room *)_peer;
        if ([room participantFromContact:contact]) {
            if ([changedKeys containsObject:@"photoData"] ||
                [changedKeys containsObject:@"presence"] ||
                [changedKeys containsObject:@"displayName"] ||
                [changedKeys containsObject:@"isConnectedWithMobile"]
            ) {
                needUpdate = YES;
            }
        }
    }
    if(needUpdate){
        if(![NSThread isMainThread]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setNeedsLayout];
            });
            return;
        } else {
            [self setNeedsLayout];
        }
    }
        
}

-(void) didUpdateRoom:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateRoom:notification];
        });
        return;
    }
    
    NSDictionary *roomInfo = notification.object;
    NSArray<NSString *> *changedKeys = [roomInfo objectForKey:kRoomChangedAttributesKey];
    
    if ([_peer isKindOfClass:[Room class]]) {
        if ([changedKeys containsObject:@"photoData"] ||
            [changedKeys containsObject:@"lastAvatarUpdateDate"] ||
            [changedKeys containsObject:@"creator"] ||
            [changedKeys containsObject:@"participants"]
            ) {
            [self setPhotoForPeer:_peer];
            [self setNeedsLayout];
        }
    }
}

-(void) didRemoveContact:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveContact:notification];
        });
        return;
    }
    
    Contact *contact = (Contact *) notification.object;
    if (contact == (Contact *)_peer) {
        [self setNeedsLayout];
    }
}

-(void) setPresence:(Presence *) presence {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setPresence:presence];
            return;
        });
    }
    
    if (![_peer isKindOfClass:[Contact class]])
        return;
    
    Contact *contact = (Contact *) _peer;
    
    if(_showPresence) {
        if(contact.isPresenceSubscribed || contact.sentInvitation.status == InvitationStatusPending){
            if(contact.sentInvitation.status == InvitationStatusPending) {
                [_contactPresenceView setImageWithString:@"• •" color:[UIColor grayColor] circular:YES fontName:[UITools defaultFontName]];
                _contactPresenceView.accessibilityValue = NSLocalizedString(@"Pending", nil);
            }
            else {
                _contactPresenceView.image = nil;
                if(presence) {
                    _contactPresenceView.accessibilityValue = NSLocalizedString([Presence stringForContactPresence:contact.presence], nil);;
                    _contactPresenceView.backgroundColor = [UITools colorForPresence:presence];
                    if(presence.presence == ContactPresenceDoNotDisturb)
                        _contactPresenceView.image = [UIImage imageNamed:@"dnd"];
                    if(presence.presence == ContactPresenceAvailable && contact.isConnectedWithMobile) {
                        _contactPresenceView.backgroundColor = [UIColor whiteColor];
                        _contactPresenceView.image = [UIImage imageNamed:@"online_mobile"];
                        _contactPresenceView.tintColor = [UITools defaultTintColor];
                        _contactPresenceView.accessibilityValue = NSLocalizedString(@"Online mobile", nil);
                    }
                } else
                    _contactPresenceView.backgroundColor = [UIColor grayColor];
            }
            _contactPresenceView.layer.cornerRadius = _contactPresenceView.frame.size.width/2.0;
            
            if(!(presence.presence == ContactPresenceAvailable && contact.isConnectedWithMobile)){
                CGFloat borderWidth = 3.0f;
                if(presence.presence == ContactPresenceInvisible){
                    borderWidth = 1.5f;
                    _contactPresenceView.layer.borderColor = [UIColor lightGrayColor].CGColor;
                } else {
                    _contactPresenceView.layer.borderColor = [UIColor whiteColor].CGColor;
                }
                _contactPresenceView.layer.borderWidth = borderWidth;
            } else {
                _contactPresenceView.layer.borderWidth = 0.0f;
            }
            _contactPresenceView.hidden = NO;
        } else {
            if([_peer isKindOfClass:[Contact class]] && ((Contact *)_peer).canChatWith) {
                _contactPresenceView.backgroundColor = [UIColor clearColor];
                _contactPresenceView.image = nil;
                _contactPresenceView.layer.borderWidth = 0;
                _contactPresenceView.layer.cornerRadius = 0;
                
                _contactPresenceView.accessibilityValue = NSLocalizedString(@"Rainbow user", nil);
                _contactPresenceView.hidden = NO;
            } else
                _contactPresenceView.hidden = YES;
        }
    } else {
        _contactPresenceView.hidden = YES;
    }

//    NSLog(@"[UIAvatarView] SET PRESENCE %@ FOR %@", presence, _peer.displayName);
}

-(void) setSelected:(BOOL)selected animated:(BOOL)animated {
    if (![_peer isKindOfClass:[Contact class]])
        return;
    
    Contact *contact = (Contact *) _peer;
    [self setPresence:contact.presence];
}

-(void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    if (![_peer isKindOfClass:[Contact class]])
        return;
    
    Contact *contact = (Contact *) _peer;
    [self setPresence:contact.presence];
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    // Contact names update
    if([keyPath isEqualToString:kContactLastNameKey] || [keyPath isEqualToString:kContactFirstNameKey]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setPhotoForPeer:_peer];
            
        });
    }
    
    // Conversation participants update
    if([keyPath isEqualToString:kRoomParticipantsKey]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setPhotoForPeer:_peer];
        });
    }
}

#pragma mark - XMPPService notification
-(void) isAbleToRefreshUIChanged:(NSNotification *) notification {
    _isAbleToRefreshUI = [[notification.userInfo objectForKey:@"isAbleToRefreshUI"] boolValue];
}

-(void) endReceiveXMPPRequestsAfterResume:(NSNotification *) notification {
    OTCLog(@"No more XMPP requests after resume - reload views");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self layoutSubviews];
    });
}

@end
