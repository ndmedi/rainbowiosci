/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>


typedef NSString *(^SectionNameComputationBlock)(id object);

/**
 *  This UI component can be used like an NSMutableArray with methods
 *  like addObject, removeObject, count, etc.
 *  But you can also define a way to compute a section-name from a given object
 *  and some filtering/sorting predicates.
 *  Doing this, you will be able to use the 2 methods "sections" and "objectsInSection:"
 *  to easily render the objects in a sectioned tableview.
 *
 *  This component also includes a cache system to perform better. The cache
 *  is flushed when you change the way section-names are computed or the global filtering predicate.
 */
@interface FilteredSortedSectionedArray<ObjectType> : NSObject <NSFastEnumeration>


/**
 *  Objects in the array
 */
@property (nonatomic, readonly) NSArray *objects;

/**
 *  The number of items in the array
 *  Global filtering is _not_ applied on this value.
 */
@property (readonly) NSUInteger count;

/**
 *  Give the index for a given object in the array
 *
 *  @param anObject The object you want to find the index
 *
 *  @return The index of the object in the array
 *  @see [NSArray indexOfObject:]
 */
-(NSUInteger) indexOfObject:(ObjectType)anObject;

/**
 *  Returns YES whether or not the object exists in the array
 *
 *  @param anObject The object to check
 *
 *  @return Whether or not the object is in the array
 *  @see [NSArray containsObject:]
 */
-(BOOL) containsObject:(ObjectType)anObject;

/**
 *  Add an object to the array
 *
 *  @param anObject The object to add
 *  @see [NSMutableArray addObject:]
 */
-(void) addObject:(ObjectType)anObject;

/**
 *  Remove an object from the array
 *
 *  @param anObject The object to remove
 *  @see [NSMutableArray removeObject:]
 */
-(void) removeObject:(ObjectType)anObject;

/**
 *  Remove all objects from the array
 *
 *  @see [NSMutableArray removeAllObjects]
 */
-(void) removeAllObjects;

/**
 *  Iterate over the array of objects
 *
 *  @param block Block called for each object
 */
-(void) enumerateObjectsUsingBlock:(void (^)(id obj, NSUInteger idx, BOOL * stop))block;

/**
 *  Clean the internal cache manually
 *  You might call this if you change some objects yourself and do not update the
 *  computation blocks/filters/...
 */
-(void) reloadData;

/**
 *  Return all the section-names
 *
 *  @return Return all the section-names
 */
-(NSArray<NSString *> *) sections;

/**
 *  Return all the objects sorted and filtered for a given section
 *
 *  @param section The section name desired
 *
 *  @return Array of the objects
 */
-(NSArray *) objectsInSection:(NSString *) section;

/**
 *  A computation block to compute the section name for a given object
 *  If nil, you will not be able to use the methods sections and objectsInSection:
 */
@property (nonatomic, strong) SectionNameComputationBlock sectionNameFromObjectComputationBlock;

/**
 *  Defines how to sort the section names
 */
@property (nonatomic, strong) NSSortDescriptor *sectionSortDescriptor;

/**
 *  _Optional_ global filter applied to all the objects in the array
 *  If nil, no filter is applied
 */
@property (nonatomic, strong) NSPredicate *globalFilteringPredicate;

/**
 *  Defines how to sort the objects inside a section.
 *  Either you can set the dict with a per-section sorting.
 *  or use the generic section name "__default__" to be used for any other sections
 */
@property (nonatomic, strong) NSDictionary<NSString *, NSArray<NSSortDescriptor *> *> *objectSortDescriptorForSection;

// return the index of the given object in his own section (not filtered)
-(NSUInteger) indexOfObjectInHisSection:(id)anObject;

@end
