/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "FilteredSortedSectionedArray.h"


@interface FilteredSortedSectionedArray ()

/**
 *  This contains the list of all the objects we should sort, filter and display
 */
@property (nonatomic, strong) NSMutableArray *objects;

/**
 *  This is a "section-names" cache
 *  It holds all the section names for the current object list
 *  Global filtering is applied, however they are not sorted
 */
@property (nonatomic, strong) NSMutableArray<NSString *> *sectionCache;

/**
 *  This is an "objects by section" cache
 *  It holds all the objects split by sections
 *  Global filtering is applied, however they are not sorted
 */
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSMutableArray *> *objectsCache;

@end


@implementation FilteredSortedSectionedArray

-(instancetype) init {
    if(self = [super init]) {
        _objects = [NSMutableArray new];
        _sectionCache = [NSMutableArray new];
        _objectsCache = [NSMutableDictionary new];
    }
    return self;
}

-(void) dealloc {
    [_objects removeAllObjects];
    _objects = nil;
    
    [_sectionCache removeAllObjects];
    _sectionCache = nil;
    
    [_objectsCache removeAllObjects];
    _objectsCache = nil;
}


#pragma mark - NSMutableArray mapped function

-(NSUInteger) count {
    return [_objects count];
}

-(NSUInteger) indexOfObject:(id) anObject {
    return [_objects indexOfObject:anObject];
}

-(BOOL) containsObject:(id) anObject {
    return [_objects containsObject:anObject];
}

-(NSUInteger) indexOfObjectInHisSection:(id)anObject {
    if (_sectionNameFromObjectComputationBlock) {
        NSPredicate *currentGlobalFilteringPredicate = _globalFilteringPredicate;
        _globalFilteringPredicate = nil;
        NSString *section = _sectionNameFromObjectComputationBlock(anObject);
        NSArray *sectionContent = [self objectsInSection:section];
        NSUInteger idx = [sectionContent indexOfObject:anObject];
        _globalFilteringPredicate = currentGlobalFilteringPredicate;
        return idx;
    }
    
    return NSNotFound;
}

-(void) addObject:(id) anObject {
    [_objects addObject:anObject];
    
    if (_sectionNameFromObjectComputationBlock) {
        if (_globalFilteringPredicate) {
            if (![_globalFilteringPredicate evaluateWithObject:anObject]) {
                return;
            }
        }
        
        NSString *section = _sectionNameFromObjectComputationBlock(anObject);
        if (![_sectionCache containsObject:section]) {
            [_sectionCache addObject:section];
            [_objectsCache setObject:[NSMutableArray new] forKey:section];
        }
        
        NSMutableArray *cachedObjects = [_objectsCache objectForKey:section];
        [cachedObjects addObject:anObject];
    }
}

-(void) removeObject:(id) anObject {
    [_objects removeObject:anObject];
    
    // We cannot use the section name compute block,
    // as maybe the object has changed.
    
    __block NSString *section = nil;
    [_objectsCache enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSMutableArray *obj, BOOL *stop) {
        if ([obj containsObject:anObject]) {
            section = key;
            *stop = YES;
        }
    }];
    
    if ([section length]) {
        NSMutableArray *cachedObjectsInSection = [_objectsCache objectForKey:section];
        [cachedObjectsInSection removeObject:anObject];
        
        // No more object in this section -> remove it.
        if ([cachedObjectsInSection count] == 0) {
            [_objectsCache removeObjectForKey:section];
            [_sectionCache removeObject:section];
        }
    }
}

-(void) removeAllObjects {
    [_sectionCache removeAllObjects];
    [_objectsCache removeAllObjects];
    [_objects removeAllObjects];
}

// Block enumeration
- (void)enumerateObjectsUsingBlock:(void (^)(id obj, NSUInteger idx, BOOL * stop))block {
    [_objects enumerateObjectsUsingBlock:block];
}

// FastEnumeration
- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained [])stackbuf count:(NSUInteger)len {
    return [_objects countByEnumeratingWithState:state objects:stackbuf count:len];
}


#pragma mark - Override setters for cache cleaning

-(void) setSectionNameFromObjectComputationBlock:(SectionNameComputationBlock)sectionNameFromObjectComputationBlock {
    // If we change the way section-names are computed
    // Then we can flush the caches, there is a great chance they are invalid
    [_sectionCache removeAllObjects];
    [_objectsCache removeAllObjects];
    
    _sectionNameFromObjectComputationBlock = nil;
    _sectionNameFromObjectComputationBlock = sectionNameFromObjectComputationBlock;
}

-(void) setGlobalFilteringPredicate:(NSPredicate *)globalFilteringPredicate {
    // If we change the global filtering predicate
    // Then we can flush the caches, there is a great chance they are invalid
    [_sectionCache removeAllObjects];
    [_objectsCache removeAllObjects];
    
    _globalFilteringPredicate = nil;
    _globalFilteringPredicate = globalFilteringPredicate;
}


#pragma mark - special functions

-(void) reloadData {
    // Clean the cache manually
    [_sectionCache removeAllObjects];
    [_objectsCache removeAllObjects];
}

-(NSArray<NSString *> *) sections {
    
    if (!_sectionNameFromObjectComputationBlock || !_sectionSortDescriptor) {
        return nil;
    }
    
    // If we have cached section-names, use it
    if ([_sectionCache count] > 0) {
        return [_sectionCache sortedArrayUsingDescriptors:@[_sectionSortDescriptor]];
    }
    
    NSMutableSet *sectionSet = [NSMutableSet new];
    
    for (id object in _objects) {
        // If we have a global filtering, then apply it.
        if (_globalFilteringPredicate) {
            if (![_globalFilteringPredicate evaluateWithObject:object]) {
                continue;
            }
        }
        [sectionSet addObject:_sectionNameFromObjectComputationBlock(object)];
    }
    
    _sectionCache = [[sectionSet sortedArrayUsingDescriptors:@[_sectionSortDescriptor]] mutableCopy];
    return _sectionCache;
}

- (NSArray *)objectsInSection:(NSString *) section {
    
    // get the specific sort descriptor ? if exists, otherwise use the __default__ one
    NSArray<NSSortDescriptor *> *objectSortDescriptor = [_objectSortDescriptorForSection objectForKey:section];
    if (!objectSortDescriptor) {
        objectSortDescriptor = [_objectSortDescriptorForSection objectForKey:@"__default__"];
        NSAssert(objectSortDescriptor, @"Sort descriptor __default__ should be at least defined.");
    }
    
    // If we have a cache for this section, use it
    if ([[_objectsCache objectForKey:section] count] > 0) {
        
        return [[_objectsCache objectForKey:section] sortedArrayUsingDescriptors:objectSortDescriptor];
    }
    
    NSMutableArray *objectsInSection = [NSMutableArray new];
    
    for (id object in _objects) {
        // If we have a global filtering, then apply it.
        if (_globalFilteringPredicate) {
            if (![_globalFilteringPredicate evaluateWithObject:object]) {
                continue;
            }
        }
        NSString *sectionName = _sectionNameFromObjectComputationBlock(object);
        // In the right section
        if ([sectionName isEqualToString:section]) {
            [objectsInSection addObject:object];
        }
    }
    
    [_objectsCache setObject:objectsInSection forKey:section];
    
    return [objectsInSection sortedArrayUsingDescriptors:objectSortDescriptor];
}

-(NSString *) description {
    return [NSString stringWithFormat:@"%@ <%p> count %ld \n objects : %@", [self class], self, self.count, [_objects debugDescription]];
}

@end
