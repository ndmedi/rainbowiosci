# Rainbow iOS opensource list
To generate an updated legal page, use the following command
<code>python foss_license_builder.py -o `pwd`/Colleagues/Rainbow-iOS/MyInfos/LegalsView/legals.html -i `pwd`/README.md -i `pwd`/external_dependencies/rainbow-objc-sdk/README.md
</code>


Follow list of all opensource used in iOS rainbow app.


Opensource name     			| Website          | Licence |Modified by ALE       | Version |
------------------------------|------------------|---------|--------------|--------------|
MBProgressHUD         			| <https://github.com/Foxnolds/MBProgressHUD-OSX.git> | [MIT](https://raw.githubusercontent.com/Foxnolds/MBProgressHUD-OSX/master/LICENSE) | Yes (Forked) | 0.2
BGTableViewRowActionWithImage | <https://github.com/benguild/BGTableViewRowActionWithImage.git> | [MIT](https://raw.githubusercontent.com/benguild/BGTableViewRowActionWithImage/0.3.5/LICENSE) | YES | 0.4.0
DZNEmptyDataSet           		| <https://github.com/dzenbot/DZNEmptyDataSet.git> | [MIT](https://raw.githubusercontent.com/dzenbot/DZNEmptyDataSet/master/LICENSE) | Yes (Forked) | 1.8.1
UIImageView+Letters           | <https://github.com/bachonk/UIImageView-Letters.git> | [MIT](https://raw.githubusercontent.com/bachonk/UIImageView-Letters/1.1.4/LICENSE) | NO | 1.1.4
UITabBarItem+CustomBadge      | <https://github.com/enryold/UITabBarItem-CustomBadge.git> | MIT | Yes (Forked) | 1.0.1
JSQSystemSoundPlayer          | <https://github.com/jessesquires/JSQSystemSoundPlayer.git> | [MIT](https://raw.githubusercontent.com/jessesquires/JSQSystemSoundPlayer/develop/LICENSE) | Yes (Forked) | 4.4.0
JSQMessagesViewController     | <https://github.com/jessesquires/JSQMessagesViewController.git> | [MIT](https://raw.githubusercontent.com/jessesquires/JSQMessagesViewController/develop/LICENSE) | Yes (Forked) | 7.3.4
UIImage-Additions               | <https://github.com/vilanovi/UIImage-Additions> | [MIT](https://raw.githubusercontent.com/vilanovi/UIImage-Additions/2.1.1/LICENSE.txt) | NO | 2.1.1
JFMinimalNotifications		| <https://github.com/atljeremy/JFMinimalNotifications.git> | [MIT](https://raw.githubusercontent.com/atljeremy/JFMinimalNotifications/master/LICENSE) | NO | 0.0.8
SCLAlertView                | <https://github.com/dogo/SCLAlertView.git> | [MIT](https://raw.githubusercontent.com/dogo/SCLAlertView/develop/LICENSE) | YES (Forked) | 1.0.4
UIDevice-Hardware           | <https://github.com/erichoracek/UIDevice-Hardware.git> | [MIT](https://raw.githubusercontent.com/erichoracek/UIDevice-Hardware/master/LICENSE) | NO | 0.1.7
XLForm                      | <https://github.com/xmartlabs/XLForm.git> | [MIT](https://raw.githubusercontent.com/xmartlabs/XLForm/3.2.0/LICENSE) | YES (Forked) | 3.3.0
DZNSegmentedControl         | <https://github.com/dzenbot/DZNSegmentedControl.git> | [MIT](https://raw.githubusercontent.com/dzenbot/DZNSegmentedControl/master/LICENSE) | NO | 1.3.3
APParallaxHeader            | <https://github.com/krajniak/APParallaxHeader.git> | [MIT](https://raw.githubusercontent.com/krajniak/APParallaxHeader/master/LICENSE.txt) | YES (Forked) | 0.1.6
SORelativeDateTransformer   | <https://github.com/billgarrison/SORelativeDateTransformer.git> | [MIT](https://raw.githubusercontent.com/billgarrison/SORelativeDateTransformer/master/LICENSE.md) | YES | 1.3.0
CLTokenInputView            | <https://github.com/clusterinc/CLTokenInputView.git> | [MIT](https://raw.githubusercontent.com/clusterinc/CLTokenInputView/master/LICENSE)  | NO | 2.4.0
TFDemoApp                   | <https://github.com/ErAbhishekChandani/TFDemoApp.git> | [MIT](https://raw.githubusercontent.com/ErAbhishekChandani/TFDemoApp/master/LICENSE) | NO | f1edee3
MarqueeLabel                | <https://github.com/cbpowell/MarqueeLabel.git> | [Common](https://raw.githubusercontent.com/cbpowell/MarqueeLabel/master/LICENSE) | YES | 3.0.1
UIView-MGBadgeView          | <https://github.com/NextFaze/UIView-MGBadgeView.git> | [MIT](https://raw.githubusercontent.com/NextFaze/UIView-MGBadgeView/master/LICENSE) | YES | 0.1
GoogleAnalytics             | <https://developers.google.com/analytics/devguides/collection/ios/v3/sdk-download> |  | NO | 3.17
HockeyApp                   | <https://github.com/bitstadium/HockeySDK-iOS.git> | [MIT](https://raw.githubusercontent.com/bitstadium/HockeySDK-iOS/develop/LICENSE) | NO | 5.1.2
iLink                       | <https://github.com/sidan5/iLink> | [MIT](https://raw.githubusercontent.com/sidan5/iLink/master/LICENCE.md) | YES | 1.0.6
