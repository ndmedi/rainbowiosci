## What's new
---

Welcome to the new release of the Rainbow SDK for iOS. There are a number of significant updates in this version that we hope you will like, some of the key highlights include:

### SDK for iOS 1.0.15 - Aug 2018
---

***3-Release SDK Breaking Changes***

* None

***API Breaking Changes***

* None

***API Changes***

* None

***Others Changes***

* Some bugs have been fixed in this version

### SDK for iOS 1.0.14 - Aug 2018
---

***3-Release SDK Breaking Changes***

* Due to data privacy improvements, Rainbow platform will introduce breaking changes in the way data associated to users are located around the world in one hand and the way users connect to the platform in other hand. Consequently, any SDK IOS prior to version 1.0.14 are entered deprecation period and will no more work once Rainbow platform 1.47 will be deployed on production (starting Sept, 30th)”. Before Sept’30, your application has to migrate to SDK IOS version 1.14 at least in order for your application to continue to work after this date.

***API Breaking Changes***

* None

***API Changes***

* None

***Others Changes***

* Fix: When a privilege (user, moderator, owner) was changed for a room participant, his status was set to unknown and the privilege was not updated accordingly.
* Fixing many issues and crashes

### SDK for iOS 1.0.13 - Jul 2018
---

***API Changes***

* Added `-(void)saveNotificationWithUserInfo:(NSDictionary *) userInfo` in `NotificationsManager`
* Added `-(void) cancelOutgoingCall` in `TelephonyService`
* The `RTCCall` notifications have been renamed, `RTCCallStatusRinging` is now `CallStatusRinging`, `RTCCallStatusConnecting` is now `CallStatusConnecting`, ...
* The `TelephonyService` notifications have been renamed, `kRTCServiceDidAddCallNotification` is now `kTelephonyServiceDidAddCallNotification`, `kRTCServiceDidUpdateCallNotification` is now  `kTelephonyServiceDidUpdateCallNotification`, ...


***Others Changes***

* Load roster cache at init of the SDK
* Fixing many issues and crashes

### SDK for iOS 1.0.12 - May 2018
---

***API Breaking Changes***

* iOS SDK have now a minmal deployement version set to **iOS 10**

***API Changes***

* The method `acceptInvitation` and `declineInvitation` now take in parameters a completion block, that give a feedback on the operation

***Others Changes***

* Fixing many issues and crashes


### SDK for iOS 1.0.11 - May 2018
---

**We are proud to publish our first public release of the Rainbow SDK for iOS.**

Thanks to this `SDK` you will be able to develop amazing applications for chatting or doing live audio and video communications.

**Some available features:**

- Instance messaging

- Contact management

- Presence

- Multimedia calls

Have a look into [Getting started](/#/documentation/doc/sdk/ios/guides/Getting_Started).
