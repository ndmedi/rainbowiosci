## Managing Channels
---

### What is a channel

Channels are targetted to provide rooms for publishing information for a large audience by  few writers.

There are 3 kind of channels, providing different visibility,

| private | company | public |
|--------------- |---------------- |---------------- |
| don't appear in search, you have to be invited | visible only when in the same company | visible for everyone |

Each channel keep upto `max_items` persistent items (30 by default), when this limit is reached the older items are retracted.

A channel item has a fixed structure 

| channel item |
| ------- |
| title |
| body |
| URL |


### Accessing the user subscribed channels

Once connected, you can browse the channels you are involved using the `channels` property of the `ChannelsService` singleton object. The `channels` property is a read-only dictionary that is maintained by the SDK. The dictionary key is the channel id, 

```objective-c
NSDictionary<NSString *, Channel *> *channels = 
		[ServicesManager sharedInstance].channelsService.channels;
```

Each channels object in the dictionary has some informations about the channel and the last pusblished items.

### Discovering public and company channels

You can search for public or company channels by their names using,

```objective-c
[[ServicesManager sharedInstance].channelsService findChannelByName: @"name" 
	limit: maxChannels 
	completionHandler: ^(NSArray<ChannelDescription *> *channelDescriptions, NSError *error){
   		if(!error){
    		...
    	}
    }
 ];
```

or their topics using,

```objective-c
[[ServicesManager sharedInstance].channelsService findChannelByTopic: @"topic" 
	limit: maxChannels 
	completionHandler: ^(NSArray<ChannelDescription *> *channelDescriptions, NSError *error){
   		if(!error){
    		...
    	}
    }
 ];
```

### Publishing to channels

You may post items to a channel for which you have subscribed and are allowed to write using,

```objective-c
[[ServicesManager sharedInstance].channelsService publishMessageToChannel:channel title:@"A title" message:@"A message" url:nil completionHandler:^(NSError *error) {
	if(error){
		...
	} else {
		...
	}
}];
```
