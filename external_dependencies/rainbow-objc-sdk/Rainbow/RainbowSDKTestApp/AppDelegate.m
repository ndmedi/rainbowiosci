//
//  AppDelegate.m
//  RainbowSDKTestApp
//
//  Created by Jerome Heymonet on 05/04/2016.
//  Copyright © 2016 ALE. All rights reserved.
//

#import "AppDelegate.h"
#import "ServicesManager.h"
#import "ServicesManager+Internal.h"
#import "ContactsManagerService.h"
#import <AddressBook/AddressBook.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
//    [Logger new];
//    // Override point for customization after application launch.
//    [[ServicesManager sharedInstance].contactsManagerService addObserver:self forKeyPath:@"localDeviceAccessGranted" options:NSKeyValueObservingOptionNew context:nil];
//    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidLoginSucceeded object:nil];
//    
//    if([ServicesManager sharedInstance].contactsManagerService.localDeviceAccessGranted){
//        NSInteger groupID = [self createGroupForTestContacts];
//        [self createContactsInLoop:2000 inGroup:groupID];
//    } else {
//        [[ServicesManager sharedInstance].contactsManagerService requestAddressBookAccess];
//    }
    
    return YES;
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
//    if([keyPath isEqualToString:@"localDeviceAccessGranted"]){
//        // Start creating many contacts
//        NSInteger groupID = [self createGroupForTestContacts];
//        NSLog(@"Start creating contact in loop");
//        [self createContactsInLoop:2000 inGroup:groupID];
//    }
}

-(NSInteger) createGroupForTestContacts {
    CFErrorRef error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    ABRecordRef newGroup = ABGroupCreate();
    ABRecordSetValue(newGroup, kABGroupNameProperty,(__bridge CFTypeRef)@"TestGroup", nil);
    ABAddressBookAddRecord(addressBook, newGroup, nil);
    ABAddressBookSave(addressBook, nil);
    NSInteger groupId = ABRecordGetRecordID(newGroup);
    NSLog(@"GROUP ID : %ld", (long) groupId);
    CFRelease(newGroup);
    CFRelease(addressBook);
    return groupId;
}

-(void) createContactsInLoop:(NSInteger) nbOfLoop inGroup:(NSInteger) groupID {
    CFErrorRef error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    
    for (int i = 0; i< nbOfLoop; i++) {
        NSString *firstName = [[NSString alloc] initWithFormat:@"Test%d",i];
        NSString *lastName = [[NSString alloc] initWithFormat:@"User%d",i];
        NSString *email = [NSString stringWithFormat:@"%@.%@@domaineTest.com",firstName,lastName];
        [self insertIphoneContactWithFirstName:firstName andLastName:lastName andWorkEmailAddress:email forAddressBook:addressBook inGroup:groupID];
    }
    
    CFRelease(addressBook);
}

-(void) insertIphoneContactWithFirstName:(NSString*)firstName andLastName:(NSString*)lastName andWorkEmailAddress:(NSString*) workEmailAddress forAddressBook:(ABAddressBookRef) addressBook inGroup:(NSInteger) groupID {
    NSLog(@"Creating contact with %@ %@ %@", firstName, lastName, workEmailAddress);
    ABRecordRef person = ABPersonCreate();
    CFErrorRef anError;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFTypeRef)(firstName), &anError);
    ABRecordSetValue(person, kABPersonLastNameProperty, (__bridge CFTypeRef)(lastName), &anError);
    
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(workEmailAddress), kABWorkLabel, NULL);
    ABRecordSetValue(person, kABPersonEmailProperty, multiEmail, nil);
    
    ABAddressBookAddRecord(addressBook, person, &anError);
    ABAddressBookSave(addressBook, &anError);
    
    ABRecordRef group = ABAddressBookGetGroupWithRecordID(addressBook, (int)groupID);
    
    ABGroupAddMember(group, person, &anError); // add the person to the group
    ABAddressBookSave(addressBook, &anError);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
