/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "NSDictionary+MD5.h"
#import "NSString+MD5.h"
#import "NSDate+MD5.h"
#import "NSNumber+MD5.h"
#import "NSData+MD5.h"


@interface NSArray (MD5)

- (NSString *)MD5ExcludingKeysFromSet:(NSSet *)exclusionKeySet;
- (NSString *)stringExcludingKeysFromSet:(NSSet *)exclusionKeySet;

@end
