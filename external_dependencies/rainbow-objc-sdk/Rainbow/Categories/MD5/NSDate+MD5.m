/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSDate+MD5.h"

@implementation NSDate (MD5)


// Note that we first get the number of seconds (NSNumber) since 1/1/1970 and then
// compute the hash on that value.
//
- (NSString *)MD5
{
    NSTimeInterval interval = [self timeIntervalSince1970];
    NSNumber *intervalNum = [NSNumber numberWithDouble:interval];
    
    // NSLog(@"interval=%f intervalNum=%@",interval,intervalNum);
    
    //return [[self description] MD5];
    return [intervalNum MD5];
    
}

- (NSString *)string
{
    NSTimeInterval interval = [self timeIntervalSince1970];
    NSNumber *intervalNum = [NSNumber numberWithDouble:interval];
    
    NSString *intervalStr = [NSString stringWithFormat:@"%@",intervalNum];
    
    return intervalStr;
}


@end


