/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSData+MimeType.h"

@implementation NSData (MimeType)

// https://en.wikipedia.org/wiki/List_of_file_signatures

static const char bmp[2] = {'B', 'M'};
static const char gif[3] = {'G', 'I', 'F'};
static const char jpg[3] = {0xff, 0xd8, 0xff};
static const char psd[4] = {'8', 'B', 'P', 'S'};
static const char iff[4] = {'F', 'O', 'R', 'M'};
static const char webp[12] = {0x52, 0x49, 0x46, 0x46, 0x00, 0x00, 0x00, 0x00, 0x57, 0x45, 0x42, 0x50};
static const char ico[4] = {0x00, 0x00, 0x01, 0x00};
static const char tif_ii[4] = {'I','I', 0x2A, 0x00};
static const char tif_mm[4] = {'M','M', 0x00, 0x2A};
static const char png[8] = {0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a};
static const char jp2[12] = {0x00, 0x00, 0x00, 0x0c, 0x6a, 0x50, 0x20, 0x20, 0x0d, 0x0a, 0x87, 0x0a};
static const char pdf[4] = {'%','P','D','F'};
static const char heic[12] = {0x00, 0x00, 0x00, 0x18, 0x66, 0x74, 0x79, 0x70, 0x68, 0x65, 0x69, 0x63};

static const char wav[12] = {0x52, 0x49, 0x46, 0x46, 0x00, 0x00, 0x00, 0x00, 0x57, 0x41, 0x56, 0x45};
static const char mp3[3] = {0x49, 0x44, 0x33};

static const char mp4[12] = {0x00, 0x00, 0x00, 0x1c, 0x66, 0x74, 0x79, 0x70, 0x6d, 0x70, 0x34, 0x32};
static const char mov[12] = {0x00, 0x00, 0x00, 0x14, 0x66, 0x74, 0x79, 0x70, 0x71, 0x74, 0x20, 0x20};
static const char m4a[12] = {0x00, 0x00, 0x00, 0x1c, 0x66, 0X74, 0X79, 0x70, 0x4d, 0x34, 0x41, 0x20};

- (NSString *)mimeTypeByGuessing {
    char bytes[12] = {0};
    [self getBytes:&bytes length:12];
    
    if (!memcmp(bytes, bmp, 2)) {
        return @"image/x-ms-bmp";
    } else if (!memcmp(bytes, gif, 3)) {
        return @"image/gif";
    } else if (!memcmp(bytes, jpg, 3)) {
        return @"image/jpeg";
    } else if (!memcmp(bytes, psd, 4)) {
        return @"image/psd";
    } else if (!memcmp(bytes, iff, 4)) {
        return @"image/iff";
    } else if (!memcmp(bytes, webp, 4)) {
        return @"image/webp";
    } else if (!memcmp(bytes, ico, 4)) {
        return @"image/vnd.microsoft.icon";
    } else if (!memcmp(bytes, tif_ii, 4) || !memcmp(bytes, tif_mm, 4)) {
        return @"image/tiff";
    } else if (!memcmp(bytes, png, 8)) {
        return @"image/png";
    } else if (!memcmp(bytes, jp2, 12)) {
        return @"image/jp2";
    } else if (!memcmp(bytes, pdf, 4)) {
        return @"application/pdf";
    } else if(!memcmp(bytes, heic, 12)) {
        return @"image/heic";
    } else if (!memcmp(bytes, wav, 12)) {
        return @"audio/x-wav";
    } else if (!memcmp(bytes, mp3, 3)) {
        return @"audio/mp3";
    } else if (!memcmp(bytes, mp4, 12)) {
        return @"video/mp4";
    } else if (!memcmp(bytes, mov, 12)) {
        return @"video/quicktime";
    } else if (!memcmp(bytes, m4a, 12)) {
        return @"audio/mpeg";
    }
    
    return @"application/octet-stream";
}

-(NSString *) extension {
    char bytes[12] = {0};
    [self getBytes:&bytes range:NSMakeRange(0, 12)];
    
    if (!memcmp(bytes, bmp, 2)) {
        return @"bmp";
    } else if (!memcmp(bytes, gif, 3)) {
        return @"gif";
    } else if (!memcmp(bytes, jpg, 3)) {
        return @"jpeg";
    } else if (!memcmp(bytes, psd, 4)) {
        return @"psd";
    } else if (!memcmp(bytes, iff, 4)) {
        return @"iff";
    } else if (!memcmp(bytes, webp, 4)) {
        return @"webp";
    } else if (!memcmp(bytes, ico, 4)) {
        return @"ico";
    } else if (!memcmp(bytes, tif_ii, 4) || !memcmp(bytes, tif_mm, 4)) {
        return @"tiff";
    } else if (!memcmp(bytes, png, 8)) {
        return @"png";
    } else if (!memcmp(bytes, jp2, 12)) {
        return @"jp2";
    } else if (!memcmp(bytes, pdf, 4)) {
        return @"pdf";
    } else if(!memcmp(bytes, heic, 12)) {
        return @"heic";
    } else if (!memcmp(bytes, wav, 12)) {
        return @"wav";
    } else if (!memcmp(bytes, mp3, 3)) {
        return @"mp3";
    } else if (!memcmp(bytes, mp4, 12)){
        return @"mp4";
    } else if (!memcmp(bytes, mov, 12)){
        return @"mp4";
    } else if (!memcmp(bytes, m4a, 12)){
        return @"m4a";
    }
    return @"";
}

@end
