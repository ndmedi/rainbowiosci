# Rainbow iOS SDK Change Log
## SDK_1.0.16] - [xxxx-xx-xx]
### Added
* Add a method setAvatar: to MyUser to let change the avatar image

## SDK_1.0.15] - [2018-08-14]
### Fixed
* The first presence was not always sent at login --> fixed (appId was not sent in that case = no stat)

## [SDK_1.0.14] - [2018-08-01]
### Fixed
* When a privilege (user, moderator, owner) was changed for a room participant, his status was set to unknown and the privilege was not updated accordingly.
* Rework WebRTC conferences
* Fix crashes and bugs

### Changed
* For XMPP federation purpose, the comparison between 2 user jids is now done without taking into account the domain. 

## [SDK_1.0.13] - [2018-07-12]
### Fixed
* Load roster cache at init of the SDK
* Fix crashes and bugs

### Changed
* Remove useless logs
* Register to mediapillar is done internally (not on client side)
* Added  -(void)saveNotificationWithUserInfo:(NSDictionary *) userInfo in NotificationsManager
* Added  -(void) cancelOutgoingCall in TelephonyService
* The RTCCall notifications have been renamed, RTCCallStatusRinging is now CallStatusRinging, RTCCallStatusConnecting is now CallStatusConnecting, ...
* The TelephonyService notifications have been renamed, kRTCServiceDidAddCallNotification is now kTelephonyServiceDidAddCallNotification, kRTCServiceDidUpdateCallNotification is now  kTelephonyServiceDidUpdateCallNotification, ...

## [SDK_1.0.12] - [2018-05-28]
### Added
* Add support of app groups (save cache content in shared folder if configured)
* Load SDK cache at initialization of the SDK

### Fixed
* Fix problem with ice-server when they don't have credentials
* Fix inline docs to avoid spurious */ in jazzy outputs and clean some comments
* Fix timestamp issue when handling push notification
* Fix problem with searchByJid method that doesn't return any contact
* Fix problem when updating call log in cache
* Fix reconnection problem when leaving plane mode
* Fix filtering in my rainbow sharing request
* Fix some crash
* Fix probblem with avatar update not refreshed correctly

### Changed
* Add completion handled in accept / decline rooms api
* SDK is no more compatible with iOS 9 (min version > iOS 10)

## [SDK_1.0.11] - 2018-05-04
### Added
* Fetch ICE server using new geolocalized API

### Fixed
* Don't count answered call logs in conversation unread messages
* Fix crash when a special character is searched
* Fix strange case where connection to xmpp says it connected but it's not the case 
* Fix freeze when hangup a call
* Fix pb with synchronisation between cache and list of fetched room at full login
* Fix duplicate issue with active directory search
* Fix pb when adding a contact into roster that is not marked as inRoster
* Check if user have a telephony resource to allow him to use nomadic or call forward features
* Do some SDK documentation harmonisations

### Changed
* Minimal deployment target is now iOS 10
* The field profilesName of MyUser now uses the offerName from server

### Restrictions

## [SDK_1.0.10] - 2018-04-30
### Added
* Add error feedback when creating a room 
* Add support of Guest participant in room
* Add support of call forward configuration
* Add API to search contact by Jid

### Fixed
* Load conversations from cache at creation of ConversationsManagerService
* Fix crash
* Fix callLog display name issue
* Fix problem with room cache not updated correctly
* Fix problem with conference ID not updated
* Fix problem with unknown contacts
* Fix login-logout issue with many contacts

### Changed
* Rename presenceDoNotDistrub into presenceDoNotDisturb

### Restrictions

## [SDK_1.0.9] - 2018-04-16
### Added
* Identify ressource using SDK
* Add API to update a participant privilege in a Room
* Add notification on File removed
* Resume session after application kill and restart
* Add notification on end of conversation loading
* Add filtering options in list of shared files
* Improvement when changing user password
* Add API populateVcardForContactsJid in public
* Add a default displayName for contacts
* Add notification for Failed remove room
* Add nomadic api

### Fixed
* Improvement in ChannelsService
* Fix problem with date of messages in callLog and group event message
* Fix problem with last message not updated correctly
* Fix issue with RTCCall that can be blocked in a wrong state
* Fix problem with attachement sent that don't have thumbnail imediatelly available
* Don't force automatically speaker phone if there is a headset connected in video call
* Fix pb with get room that doesn't fetch all rooms on server side
* Fix high memory consumption with NSDateFormatter

### Changed
* No api changed

### Restrictions
* No restrictions

## [SDK_1.0.8] - 2018-03-16
### Added
* Disable room creation notifications
* Add PBX CallLog support
* Add call recording indictator for webRTC calls
* Add webRTC conference support
* Add room owner chnaged support (read only)
* Add support of Guest
* Add profile update monitoring
* Add detection of change password from another device or by admin (automatic logout)

### Fixed
* Reduce data consumption
* Fix room name update length problem
* Fix message date problem when received in carbon copy
* Fix nil attachment file in message in some cases
* Fix problem of re-invitation in a room
* Fix problem with contact not added in roster after accepting his invitation
* Fix crash on rooms
* Fix webrtc P2P call problem when removing a video stream
* Fix issue with outgoing messages becoming incoming messages after loading from server
* Fix avatar download problem when user set his avatar for the first time
* Detect server shutdown and reconnect correctly

### Changed
* CallLog object have now a service property
* RTCCall object have now a isRecording property
* RTCService class now send a notification when a recording is started or stopped by the remote
* Message object now have two new type for recording
* LoginManager class have new register method to allow setting the visibility
* MyUser class have now a isGuest property
* MyUser class now send a notification when the user profile is updated
* Add some error in cases of wrong usage of LoginManager send* methods



### Restriction

## [SDK_1.0.7] - 2018-02-22
### Added
* Add Participant privilege Owner in rooms
* Add conference right parameter in myuser object
* Add custom data api for rooms
* Improve file sharing api for thumbnail automatic download
* Improve file sharing mecanism to upload and download file data by chunk if needed
* Add api to invite contact to join Rainbow by email adresss
* Add P2P webrtc call video + desktop sharing feature
* Add api for channels
* Add api to get room participant list using a range

### Fixed
* Fix crash and issues with room creator and participants
* Fix profile name lost after application restart
* Fix contact firstname-lastname display order parameter retreival
* Fix with date of message loaded from cache
* Fix some crashes
* Fix problem with video added in webrtc call at the same time on both side

### Changed
* Update WebRTC stack to version 63
* Update application autentication mechanism

## [SDK_1.0.6] - 2018-02-02
### Added
* Add API to create a contact from a CNContact
* Allow searching in address book via ContactsManagerService
* Invite local contacts to join Rainbow via ContactsManagerService
* Add startAndJoinConference in ConferencesManagerService
* Add groups in cache and load them at startup
* Add companies invitations in cache and load them at startup
* Add notifications in ContactsManagerService for notifying the end of loading user network
* Add multi administrator display on rooms
* Add  dialOutDisabled flag for conferences
* Add API in ServicesManager to set the ApplicationID and secret
* Add API in NotificationManager to enable push notification support
* Add first step for Channel

### Fixed
* Bug fix on offline mode and conversation loading
* Fix crashes
* Retreive correctly the profileName of the user
* Fix problem with ICE servers list
* Fix problem on reconnection with rooms
* Don't load local contacts in memory
* Fix problem with last message in conversations
* Improved network reconnections
* Fix message dates
* Fix problem with unread message count
* Fix problem with carbon copy message received
* Fix for unknown users in network
* Fix problem with download of avatars perfomed too many times
* Fix problem with messages that are transformed from outgoing to incoming messages
* Don't fetch shared file list automatically at startup

### Changed
* Rework of resending xmpp messages mechanism
* Update websocket component to support proxies
* Notification didUpdateMessagesUnreadCount send from conversationsManagerServices is now sent in a background thread


## [SDK_1.0.5] - 2017-12-15
### Added
* Add category to calculate distance between too date
* Implementation of offline mode, preload cached data, allow start of application without network, automatically re-execute network request when network is retrieved.
* Add handler to continue NSUserActivities for tchat, audio and video calls
* Add method to search a contact by email address

### Fixed
* Fix crash in contacts
* Fix company search that return empty company
* Fix crash in file sharing

### Changed
* update PINCache module
* Use CNContact api instead of deprecated contact api
* Use release version of webRTC library

## [SDK_1.0.4] - 2017-11-17
### Added
* Add support of new media format for file sharing
* Add support of WebRTC Conference
* Add application token support

### Fixed
* Improve PGI conferences
* Improve reconnection mecanism

### Changed
* Update WebRTC Stack to version 61

## [SDK_1.0.3] - 2017-10-09
### Added
* Allow customization of RTCService
* Add "hasActiveCall" property in conversation object

### Fixed
* Fix in search in Microsoft Active directory
* Improve PGI conferences
* Fix audio issue with iOS 11
* Crash with search engine

### Changed
* Update Webrtc framework version 60

## [SDK_1.0.2] - 2017-09-08
### Added
* Scheduled conferences
* Search in Microsoft Active directory
* Custom avatar in rooms

### Fixed
* Wrong state for calendar presence
* Calendar presence automatic reply is always prioritary
* Fix pb on applicationDidBecomeActive

## [SDK_1.0.1] - 2017-08-04
### Added
* Calendar presence
* Conferences (Step 1)
* Invite contact by SMS
* File Sharing refresh action
* My User profile infos
* Add support of CDN for avatars
* Increase size of downloaded avatar
* Add call log service
* Add webrtc video support and desktop sharing viewer support
* New notification on before login kLoginManagerWillLogin
* Add automatic refresh extension for Message Browser
* On webrtc call set presence automatically
* Collect webrtc call statistics during and at end of the call
* Improve webrtc call connection and stability

### Fixed
* Retreive correctly File upload date, date to sort
* Fix problem with delivery state in messages
* Automatically retreive images files with size less than 1Mo
* Fix pb when changing server
* Fix pb with notification handler that doesn't execute the good action when starting app from notification
* Fix slow search in companies
* Fix pb with fetching Rainbow contact details not in our roster
* Fix crash when preloading conversations and rooms from cache


### Changed
* Remove updloadFile public api it's done automatically when sending a file attachment
* Return rainbow user information first

## [SDK_1.0] - 2017-06-16
### Added
* First version of Rainbow iOS SDK
