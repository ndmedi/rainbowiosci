/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "LogsRecorder.h"
#import "NSDate+Utilities.h"
#import "Tools.h"
#import <zlib.h>

@interface LogsRecorder(privateAPI)
-(NSString*) pathForLog:(NSString*)logName;
@end

static LogsRecorder* singleton;

@implementation LogsRecorder

+(LogsRecorder*) sharedInstance {
	if (!singleton)
		singleton=[[LogsRecorder alloc] init];
	return singleton;
}

- (void)dealloc {
    _fileHandle = nil;
    _fileName = nil;
}

-(BOOL) isRecording {
	return _isRecording;
}

-(NSString *) loggerPath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *appDocumentDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
    NSString *rainbowAppGroupPath = [Tools rainbowAppGroupPath];
    NSString *documentDirectory = appDocumentDirectory;
    if(rainbowAppGroupPath){
        documentDirectory = [rainbowAppGroupPath stringByAppendingString:@"/Documents"];
        
        if (![fileManager fileExistsAtPath:documentDirectory]) {
            [fileManager createDirectoryAtPath:documentDirectory withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        NSArray *appDocumentDirectoryContents = [fileManager contentsOfDirectoryAtPath:appDocumentDirectory error:nil];
        NSArray *appGroupDocumentDirectoryContents = [fileManager contentsOfDirectoryAtPath:documentDirectory error:nil];
        if(appGroupDocumentDirectoryContents.count == 0 && appDocumentDirectoryContents.count > 0){
            for (NSString *content in appDocumentDirectoryContents) {
                NSError *error = nil;
                NSString *source = [NSString stringWithFormat:@"%@/%@",appDocumentDirectory, content];
                NSString *dest = [NSString stringWithFormat:@"%@/%@",documentDirectory, content];
                [fileManager moveItemAtPath:source toPath:dest error:&error];
                NSLog(@"Error while moving log file %@", error);
            }
        }
    }
    
    
    return documentDirectory;
}

-(void) startRecord {
	if (!_isRecording) {
        NSString *documentsDirectory = [self loggerPath];
        _fileName = [[NSDate date] stringWithFormat:@"dd-MM-yyyy HH_mm_ss"];
        _fileName = [[NSString alloc] initWithFormat:@"log_%@-%@_%@.txt", [Tools applicationName], [Tools applicationVersion], _fileName];
		NSString* ressource = [NSString stringWithFormat:@"%@/%@", documentsDirectory, _fileName];
		_isRecording=YES;
        
        stderrSave = dup (STDERR_FILENO);
        
        freopen([ressource cStringUsingEncoding:NSASCIIStringEncoding],"a",stderr);
        NSLog(@"[LOGS-RECORDER] recording to %@", ressource);
	}
}

-(void) stopRecord {
    if (_isRecording) {
        _isRecording=NO;
        fflush(stderr);
        dup2(stderrSave,STDERR_FILENO);
        close(stderrSave);
	}
}

- (NSArray*) logs {
    NSString *documentsDirectory = [self loggerPath];
    NSArray *directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil];

	NSMutableArray* foundLogs = [NSMutableArray array];
    for (NSString *content  in directoryContents) {
        if([content hasPrefix:@"log_"]){
            [foundLogs addObject:content];
        }
    }
    
    NSArray*  sortedLogs;
    sortedLogs = [foundLogs sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        NSDictionary* first_properties  = [[NSFileManager defaultManager] attributesOfItemAtPath:[NSString stringWithFormat:@"%@/%@", documentsDirectory, obj1] error:nil];
        NSDate *first = [first_properties  objectForKey:NSFileModificationDate];
        NSDictionary *second_properties = [[NSFileManager defaultManager] attributesOfItemAtPath:[NSString stringWithFormat:@"%@/%@", documentsDirectory, obj2] error:nil];
        NSDate *second = [second_properties objectForKey:NSFileModificationDate];
        return [second compare:first];
    }];
    
	return sortedLogs;
}

-(NSData*) dataForLog:(NSString*)logName {
	NSString* logPath = [self pathForLog:logName];
	NSData* data = [NSData dataWithContentsOfFile:logPath] ;
	return data;
}

-(NSDictionary*) attributes:(NSString*) logName {
	NSString* trackPath = [self pathForLog:logName];
	return [[NSFileManager defaultManager] attributesOfItemAtPath:trackPath error:nil];
}

-(NSInteger) deleteAllLogs {
	NSArray* logs = [self logs];
	for (NSString* log in logs) {
		[self deleteLog:log];
	}
	return [logs count];
}

-(NSArray*) deleteLogsBefore:(NSDate*)date {
    NSArray* logs = [self logs];
    NSMutableArray* deletedLogs = [NSMutableArray array];
    for (NSString* logPath in logs) {
        NSDictionary* attrs = [self attributes:logPath];
        NSDate *dateAttr = (NSDate*)[attrs objectForKey: NSFileCreationDate];
        if ([dateAttr compare:date] == NSOrderedAscending) {
            [self deleteLog:logPath];
            [deletedLogs addObject:logPath];
        }
    }
    return deletedLogs;
}

-(BOOL) deleteLog:(NSString*)logName {
	NSString* itemPath = [self pathForLog:logName];
	BOOL result = [[NSFileManager defaultManager] removeItemAtPath:itemPath error:nil];
	return result;
}
	
-(NSString*) pathForLog:(NSString*)logName {
    NSString *documentsDirectory = [self loggerPath];
	NSString* itemPath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, logName];
    return itemPath;
}

-(NSURL *) zippedApplicationLogs {
    NSError *error = nil;
    NSURL *zippedLogFileURL = [self zippedFiles:[self logs] andZipFilename:[NSString stringWithFormat:@"%@-%@-logs",[Tools applicationName],[Tools applicationVersion]] andError:&error];
    return (!error)?zippedLogFileURL:nil;
}

- (NSURL*) zippedFiles:(NSArray*)files andZipFilename:(NSString*)zipFilename andError:(NSError**)error {
    if (files.count == 0)
        return nil;
    
    // Get the document directory
    NSString* documentsDirectory = [self loggerPath];
    
    // Fetch the default file manager
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    // Remove existing logZipFile (if any)
    NSString* compressFilePath = [NSString stringWithFormat:@"%@/%@.txt.zip", documentsDirectory, zipFilename];
    if ([fileManager fileExistsAtPath:compressFilePath]) {
        if (![fileManager removeItemAtPath:compressFilePath error:error]) {
            return nil;
        }
    }
    
    // Create the zip file
    const char* pathToZip = [compressFilePath cStringUsingEncoding:NSASCIIStringEncoding];
    __block gzFile file = gzopen(pathToZip, "wbl");
    if (file == NULL) {
        if(error)
            *error = [NSError errorWithDomain:@"ZLIB" code:0xbabe userInfo:nil];
        return nil;
    }
    
    // Append all files content in zip file
    NSArray* reversedlogFiles = [[files reverseObjectEnumerator] allObjects];
    
    [reversedlogFiles enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop) {
        NSString *filePath = [NSString stringWithFormat:@"%@/%@",documentsDirectory, obj];
        NSData* data = [NSData dataWithContentsOfFile:filePath];
        NSString* separator;
        if (data) {
            separator = [NSString stringWithFormat:@"\n\n#########################\n %@\n#########################\n", obj];
            gzwrite(file, [separator cStringUsingEncoding:NSASCIIStringEncoding], (unsigned int)separator.length);
            gzwrite(file, [data bytes], (unsigned int)[data length]);
        }
        else {
            separator = [NSString stringWithFormat:@"\n\n#########################\n %@ not Found\n#########################\n", obj];
            gzwrite(file, [separator cStringUsingEncoding:NSASCIIStringEncoding], (unsigned int)separator.length);
        }
    }];
    
    // Close zip file
    gzclose(file);
    
    return [NSURL fileURLWithPath:compressFilePath];
}

-(void) cleanOldLogs {
    NSArray *allLogs = [self logs];
    [allLogs enumerateObjectsUsingBlock:^(id aLog, NSUInteger idx, BOOL * stop) {
        NSDictionary *attr = [self attributes:aLog];
        NSDate *creationDate = attr[@"NSFileCreationDate"];
        if([creationDate isEarlierThanDate:[NSDate dateWithDaysBeforeNow:2]]){
            [self deleteLog:aLog];
        }
    }];
    
    // Clean also existing zipped files
    NSString *documentsDirectory = [self loggerPath];
    NSArray *directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil];
    NSMutableArray* foundZippedLogs = [NSMutableArray array];
    for (NSString *content  in directoryContents) {
        if([content hasPrefix:@"Rainbow-"]){
            [foundZippedLogs addObject:content];
        }
    }
    
    [foundZippedLogs enumerateObjectsUsingBlock:^(NSString *zippedFileName, NSUInteger idx, BOOL * stop) {
        NSLog(@"%@/%@",documentsDirectory,zippedFileName);
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@",documentsDirectory,zippedFileName] error:nil];
    }];
}

@end
