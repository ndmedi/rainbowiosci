/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowSDKTestAbstract.h"
#import "ServicesManager+Internal.h"
#import "ContactInternal.h"
#import "RainbowContact.h"
#import "Participant+internal.h"

@interface RoomsServiceTests : RainbowSDKTestAbstract

@end

@implementation RoomsServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

/**
 *  We start the application and automatically we will get rooms.
 *  We must have one room and we must be the owner of this room
 */
- (void)testGetRoomsAfterDidLogin {
    
    __block NSInteger nbOfDidAddNotifReceived = 0;
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *theRoom = (Room *)notification.object;
        NSLog(@"DID ADD ROOM %@", theRoom);
        
        if([theRoom.displayName isEqualToString:@"name of my room"]){
            nbOfDidAddNotifReceived++;
        }
        if([theRoom.displayName isEqualToString:@"Chat room 1"]){
            XCTAssertTrue(theRoom.participants.count == 2, @"We must have 2 participants in this room");
            XCTAssertTrue(theRoom.isMyRoom,@"This room must be our room");
            XCTAssertTrue(theRoom.myStatusInRoom == ParticipantStatusAccepted, @"My status must be accepted");
            nbOfDidAddNotifReceived++;
        }
        if([theRoom.displayName isEqualToString:@"Chat room"]){
            XCTAssertTrue(theRoom.participants.count == 2, @"We must have 2 participants in this room");
            XCTAssertTrue(!theRoom.isMyRoom,@"This room is NOT my room");
            XCTAssertTrue(theRoom.myStatusInRoom == ParticipantStatusInvited, @"My status in this room must be in invited state");
            nbOfDidAddNotifReceived++;
        }
        
        if([theRoom.displayName isEqualToString:@"Chat room to leave"]){
            XCTAssertTrue(theRoom.participants.count == 2, @"We must have 2 participants in this room");
            XCTAssertTrue(!theRoom.isMyRoom,@"This room is NOT my room");
            XCTAssertTrue(theRoom.myStatusInRoom == ParticipantStatusAccepted, @"My status in this room must be in accepted state");
            nbOfDidAddNotifReceived++;
        }
        
        // 3 before one room will be created by conversation API
        if(nbOfDidAddNotifReceived == 4){
            XCTAssertTrue(_serviceManager.roomsService.rooms.count == 4, @"We must now have four rooms");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}


#pragma mark - I'm the participant of a room
/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  We receive an invitation to join 'Chat room 2'
 */
-(void) testReceiveRoomInvitation {
    NSString *roomID = @"57e3d2db87c0b9a19e574930";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"rooms/%@", roomID]] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString* fixture = OHPathForFile(@"getRoomDetails.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        // Login is OK wait a while and simulate an incoming room invitation
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud' to='%@' type='chat'><x xmlns='jabber:x:conference' jid='room1@muc.mycompany.com' thread='%@' reason='JustForFun'/></message>", [self getFull], roomID]];
        });
        return YES;
    }];
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        // Room created by get conversation and get rooms
        if([room.displayName isEqualToString:@"Chat room invitation"]){
            XCTAssertTrue([room.displayName isEqualToString:@"Chat room invitation"], @"Room name must be Chat invitation");
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusInvited, @"My Statustus in room must be invited");
            XCTAssertTrue(room.participants.count == 2, @"We must have 2 participant in this room");
            XCTAssertTrue(room.isMyRoom == NO, @"This room must be NOT our room");
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidReceiveRoomInvitation object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        XCTAssertTrue(room.myStatusInRoom == ParticipantStatusInvited, @"My status is room must be invited");
        XCTAssertTrue([_serviceManager.roomsService numberOfPendingRoomInvitations] == 1, @"Number of pending room invitation must be equal to 1");
        return YES;
    }];

    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room' populated in the sdk by fetchRoom
 *  I accept to join this room from this device
 *  We will receive a xmpp message that inform us of our new status
 *  A didUpdateRoom notification must be posted
 *  We must have a also notification that the invitation status changed
 */
-(void) testAsParticipantOfARoomIAcceptAnInvitation {
    NSString *roomID = @"57e3d2db87c0b9a19e574927";
    NSString *roomJID = @"room@muc.mycompany.com";

    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"rooms/%@/users/%@", roomID, _myUser.contact.rainbowID]] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"data":@[]} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    __block BOOL acceptInvitation = NO;
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room"]) {
            acceptInvitation = YES;
            [_serviceManager.roomsService acceptInvitation:room completionBlock:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='accepted'/></message>", [self getBare], roomID, roomJID, [self getBare]]];
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        
        if([room.displayName isEqualToString:@"Chat room"] && acceptInvitation) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My status in room must be accepted");
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidRoomInvitationStatusChanged object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room"] && acceptInvitation) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My status in room must be accepted");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room' populated in the sdk by fetchRoom
 *  We join this room from another device
 *  We will receive a xmpp message that inform us of our new status
 *  A didUpdateRoom notification must be posted
 *  We must have a also notification that the invitation status changed
 */
-(void) testAsParticipantOfARoomIAcceptTheInvitationFromOtherDevice {
    NSString *roomID = @"57e3d2db87c0b9a19e574927";
    NSString *roomJID = @"room@muc.mycompany.com";
    
    __block BOOL acceptInvitation = NO;
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room"]) {
            acceptInvitation = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='accepted'/></message>", [self getBare], roomID, roomJID, [self getBare]]];
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        if([room.displayName isEqualToString:@"Chat room"] && acceptInvitation) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My status in room must be accepted");
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidRoomInvitationStatusChanged object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room"] && acceptInvitation) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My status in room must be accepted");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room' populated in the sdk by fetchRoom
 *  I accept to join this room from this device
 *  We will receive a xmpp message that inform us of our new status
 *  A didUpdateRoom notification must be posted
 *  We must have a also notification that the invitation status changed
 */
-(void) testAsParticipantOfARoomIDeclineAnInvitation {
    NSString *roomID = @"57e3d2db87c0b9a19e574927";
    NSString *roomJID = @"room@muc.mycompany.com";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"rooms/%@/users/%@", roomID, _myUser.contact.rainbowID]] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"data":@[]} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    __block BOOL declineInvitation = NO;
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room"]) {
            declineInvitation = YES;
            [_serviceManager.roomsService declineInvitation:room completionBlock:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='rejected'/></message>", [self getBare], roomID, roomJID, [self getBare]]];
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        if([room.displayName isEqualToString:@"Chat room"] && declineInvitation) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusRejected, @"My status in room must be rejected");
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidRoomInvitationStatusChanged object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room"] && declineInvitation) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusRejected, @"My status in room must be rejected");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room' populated in the sdk by fetchRoom
 *  We join this room from another device
 *  We will receive a xmpp message that inform us of our new status
 *  A didUpdateRoom notification must be posted
 *  We must have a also notification that the invitation status changed
 */
-(void) testAsParticipantOfARoomIDeclineTheInvitationFromOtherDevice {
    NSString *roomID = @"57e3d2db87c0b9a19e574927";
    NSString *roomJID = @"room@muc.mycompany.com";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"rooms/%@/users/%@", roomID, _myUser.contact.rainbowID]] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"data":@[]} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    __block BOOL declineInvitation = NO;
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room"]) {
            declineInvitation = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='rejected'/></message>", [self getBare], roomID, roomJID, [self getBare]]];
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        if([room.displayName isEqualToString:@"Chat room"] && declineInvitation) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusRejected, @"My status in room must be rejected");
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidRoomInvitationStatusChanged object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room"] && declineInvitation) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusRejected, @"My status in room must be rejected");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room to leave' populated in the sdk by fetchRoom
 *  I'm in state accepted
 *  I want to leave this room from this device
 *  We will receive a xmpp message that inform us of our new status
 *  A didUpdateRoom notification must be posted
 */
-(void) testAsAParticipantIWantToLeaveARoom {
    NSString *roomID = @"57e3d2db87c0b9a19e574928";
    NSString *roomJID = @"roomLeave@muc.mycompany.com";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"rooms/%@/users/%@", roomID, _myUser.contact.rainbowID]] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"data":@[]} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    __block BOOL unsubscribedSent = NO;
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room to leave"]) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"We must be in accepted state in this room");
            [_serviceManager.roomsService leaveRoom:room];
            unsubscribedSent = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='unsubscribed'/></message>", [self getBare], roomID, roomJID, [self getBare]]];
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        if([room.displayName isEqualToString:@"Chat room to leave"] && unsubscribedSent) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusUnsubscribed, @"My status in room must be unsubscribed");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room to leave' populated in the sdk by fetchRoom
 *  I'm in state accepted
 *  We leave this room from another device
 *  We will receive a xmpp message that inform us of our new status
 *  A didUpdateRoom notification must be posted
 */
-(void) testAsAParticipantIWantToLeaveARoomFromOtherDevice {
    NSString *roomID = @"57e3d2db87c0b9a19e574928";
    NSString *roomJID = @"roomLeave@muc.mycompany.com";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"rooms/%@/users/%@", roomID, _myUser.contact.rainbowID]] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"data":@[]} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    __block BOOL unsubscribedSent = NO;
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room to leave"]) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"We must be in accepted state in this room");
            unsubscribedSent = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='unsubscribed'/></message>", [self getBare], roomID, roomJID, [self getBare]]];
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        if([room.displayName isEqualToString:@"Chat room to leave"] && unsubscribedSent) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusUnsubscribed, @"My status in room must be unsubscribed");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room to leave' populated in the sdk by fetchRoom
 *  I'm in state accepted
 *  I want to delete this room from this device
 *  We will receive a xmpp message that inform us of our new status
 *  A didRemoveRoom notification must be posted
 */
-(void) testAsAParticipantIWantToDeleteARoom {
    NSString *roomID = @"57e3d2db87c0b9a19e574928";
    NSString *roomJID = @"roomLeave@muc.mycompany.com";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"rooms/%@/users/%@", roomID, _myUser.contact.rainbowID]] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"data":@[]} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    __block BOOL deleteSent = NO;
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room to leave"]) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"We must be in accepted state in this room");
            [_serviceManager.roomsService deleteRoom:room];
            deleteSent = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='deleted'/></message>", [self getBare], roomID, roomJID, [self getBare]]];
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidRemoveRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room to leave"] && deleteSent) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusDeleted, @"My status in room must be unsubscribed");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room to leave' populated in the sdk by fetchRoom
 *  I'm in state accepted
 *  We delete this room from another device
 *  We will receive a xmpp message that inform us of our new status
 *  A didRemoveRoom notification must be posted
 */
-(void) testAsAParticipantIWantToDeleteARoomFromOtherDevice {
    NSString *roomID = @"57e3d2db87c0b9a19e574928";
    NSString *roomJID = @"roomLeave@muc.mycompany.com";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"rooms/%@/users/%@", roomID, _myUser.contact.rainbowID]] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"data":@[]} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    __block BOOL deleteSent = NO;
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room to leave"]) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"We must be in accepted state in this room");
            deleteSent = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='deleted'/></message>", [self getBare], roomID, roomJID, [self getBare]]];
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidRemoveRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        if([room.displayName isEqualToString:@"Chat room to leave"] && deleteSent) {
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusDeleted, @"My status in room must be unsubscribed");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}


#pragma mark - I'm the owner of a room
/**
 *  We create a new room from from this device!
 *  So we receive an xmpp message with information about this new room
 *  We will fetch all the information from the server (REST)
 *  So during a short time the room will not have any information
 */
-(void) testAsOwnerIWantToCreateRoom {
    NSString *roomID = @"56f9777e0e5e7d7535a48fa2";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/rooms"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString* fixture = OHPathForFile(@"createMyTestRoom.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"/rooms/%@/users", roomID]] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString* fixture = OHPathForFile(@"inviteUserInMyRoom.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    Contact *contactToInvite = [Contact new];
    RainbowContact *rainbow = [RainbowContact new];
    rainbow.rainbowID = @"56e05bb83456faf3384272ef";
    rainbow.jid = @"c2bd56bf4ce54225a2bec296fb76e818@jerome-all-in-one-dev-1.opentouch.cloud";
    contactToInvite.rainbow = rainbow;
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            Room *createdRoom = [_serviceManager.roomsService createRoom:@"My test room" withTopic:@"This is a topic"];
            //[_serviceManager.roomsService inviteContact:contactToInvite inRoom:createdRoom];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        NSLog(@"DID ADD ROOM %@", room);
        if([room.displayName isEqualToString:@"My test room"]){
            XCTAssertTrue(room.participants.count == 1, @"We must have only one participant");
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My status in room must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        if([room.displayName isEqualToString:@"My test room"]){
            XCTAssertTrue(room.participants.count == 2, @"We must have two participants");
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My status in room must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  We create a new room from another device
 *  So we receive an xmpp message with information about this new room
 *  We will fetch all the information from the server (REST)
 *  So during a short time the room will not have any information
 */
-(void) testAsOwnerIWantToCreateRoomFromOtherDevice {
    NSString *roomID = @"57e3d2db87c0b9a19e574940";
    NSString *roomJID = @"room3@muc.mycompany.com";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"rooms/%@", roomID]] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString* fixture = OHPathForFile(@"getMyRoomDetails.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        // Login is OK wait a while and simulate an incoming room invitation
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='accepted'/></message>", [self getBare], roomID, roomJID, [self getBare]]];
        });
        return YES;
    }];
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        NSLog(@"DID ADD ROOM %@", room);
        // ignore room created by get conversation and get rooms
        if(_serviceManager.roomsService.rooms.count <= 4)
            return NO;
        
        XCTAssertTrue(room.displayName.length == 0, @"We don't have any name, and that is normal");
        
        return YES;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        NSLog(@"DID UPDATE ROOM %@", room);
        if([room.displayName isEqualToString:@"My chat room details"]){
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My Status must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  We add a new participant from this device
 */
-(void) testAsOwnerIWantToAddParticipantInMyRoom {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"/rooms/%@/users", roomID]] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString* fixture = OHPathForFile(@"inviteUserInMyRoom.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    Contact *contactToInvite = [Contact new];
    RainbowContact *rainbow = [RainbowContact new];
    rainbow.rainbowID = @"56e05bb83456faf3384272ef";
    rainbow.jid = @"c2bd56bf4ce54225a2bec296fb76e818@jerome-all-in-one-dev-1.opentouch.cloud";
    contactToInvite.rainbow = rainbow;
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        NSLog(@"DID ADD ROOM %@", room);
        if([room.displayName isEqualToString:@"Chat room 1"]){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                //[_serviceManager.roomsService inviteContact:contactToInvite inRoom:room];
            });
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        if([room.displayName isEqualToString:@"Chat room 1"]){
            XCTAssertTrue(room.participants.count == 3, @"We must have 3 participants");
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My status in room must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  We add a new participant from another device
 *  So we receive an xmpp message with information about this participant
 */
-(void) testAsOwnerIAddParticipantInMyRoomFromOtherDevice {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    NSString *roomJID = @"room1@muc.mycompany.com";
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        // Login is OK wait a while and simulate an incoming room invitation
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='invited'/></message>", [self getBare], roomID, roomJID, @"0emily@mycompany.com"]];
        });
        return YES;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        NSLog(@"DID UPDATE ROOM %@", room);
        // ignore room created by get conversation and get rooms
        if([room.rainbowID isEqualToString:roomID]){
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My Status must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            XCTAssertTrue(room.participants.count == 3, @"We must have 3 participants");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  We remove a participant from this device
 */
-(void) testAsOwnerIWantToDeleteParticipantFromMyRoom {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    NSString *roomJID = @"room1@muc.mycompany.com";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"/rooms/%@/users/%@", roomID, @"56e05bb83456faf3384272ee"]] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"status":@"OK"} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        NSLog(@"DID ADD ROOM %@", room);
        if([room.displayName isEqualToString:@"Chat room 1"]){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                // We force the participant status just to avoid changing all the test data
                Contact *contactToDelete = [_contactsManagerService getContactWithRainbowID:@"56e05bb83456faf3384272ee"];
                Participant *theParticipant = [room participantFromContact:contactToDelete];
                theParticipant.status = ParticipantStatusAccepted;
                [_serviceManager.roomsService removeParticipant:theParticipant fromRoom:room];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='deleted'/></message>", [self getBare], roomID, roomJID, contactToDelete.jid]];
                });
            });
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        NSLog(@"DID UPDATE ROOM %@", room);
        if([room.displayName isEqualToString:@"Chat room 1"]){
            XCTAssertTrue(room.participants.count == 2, @"We must have two participants");
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My status in room must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            // One of the participants must be in state deleted
            BOOL participantInDeletedState = NO;
            for (Participant *participant in room.participants) {
                if(participant.status == ParticipantStatusDeleted && participant.privilege == ParticipantPrivilegeUser){
                    participantInDeletedState = YES;
                    break;
                }
            }
            XCTAssertTrue(participantInDeletedState, @"We must have a participant in deleted state");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  We remove a participant from another device
 *  So we receive an xmpp message with information about this participant
 */
-(void) testAsOwnerIRemoveParticipantFromMyRoomFromOtherDevice {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    NSString *roomJID = @"room1@muc.mycompany.com";
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        NSLog(@"DID ADD ROOM %@", room);
        if([room.displayName isEqualToString:@"Chat room 1"]){
            // We force the participant status just to avoid changing all the test data
            Contact *contactToDelete = [_contactsManagerService getContactWithRainbowID:@"56e05bb83456faf3384272ee"];
            Participant *theParticipant = [room participantFromContact:contactToDelete];
            theParticipant.status = ParticipantStatusAccepted;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='deleted'/></message>", [self getBare], roomID, roomJID, @"c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud"]];
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        // ignore room created by get conversation and get rooms
        if([room.rainbowID isEqualToString:roomID]){
            NSLog(@"DID UPDATE ROOM %@", room);
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My Status must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            XCTAssertTrue(room.participants.count == 2, @"We must have 2 participants");
            // One of the participants must be in state deleted
            BOOL participantInDeletedState = NO;
            for (Participant *participant in room.participants) {
                if(participant.status == ParticipantStatusDeleted && participant.privilege == ParticipantPrivilegeUser){
                    participantInDeletedState = YES;
                    break;
                }
            }
            XCTAssertTrue(participantInDeletedState, @"We must have a participant in deleted state");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  A participant of my room 'Chat room 1' accept to join it.
 *  So we receive an xmpp message with information about this participant
 */
-(void) testAsOwnerAParticipantOfMyRoomAcceptInvitation {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    NSString *roomJID = @"room1@muc.mycompany.com";

    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        // Login is OK wait a while and simulate an incoming room invitation
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='accepted'/></message>", [self getBare], roomID, roomJID, @"c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud"]];
        });
        return YES;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        // ignore room created by get conversation and get rooms
        if([room.rainbowID isEqualToString:roomID]){
            NSLog(@"DID UPDATE ROOM %@", room);
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My Status must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            XCTAssertTrue(room.participants.count == 2, @"We must have 2 participants");
            // One of the participants must be in state deleted
            BOOL participantInAcceptedState = NO;
            for (Participant *participant in room.participants) {
                if(participant.status == ParticipantStatusAccepted && participant.privilege == ParticipantPrivilegeUser){
                    participantInAcceptedState = YES;
                    break;
                }
            }
            XCTAssertTrue(participantInAcceptedState, @"We must have a participant in accepted state");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  A participant of my room 'Chat room 1' refuse to join it.
 *  So we receive an xmpp message with information about this participant
 */
-(void) testAsOwnerAParticipantOfMyRoomRefuseInvitation {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    NSString *roomJID = @"room1@muc.mycompany.com";
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        // Login is OK wait a while and simulate an incoming room invitation
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='rejected'/></message>", [self getBare], roomID, roomJID, @"c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud"]];
        });
        return YES;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        // ignore room created by get conversation and get rooms
        if([room.rainbowID isEqualToString:roomID]){
            NSLog(@"DID UPDATE ROOM %@", room);
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My Status must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            XCTAssertTrue(room.participants.count == 2, @"We must have 2 participants");
            // One of the participants must be in state deleted
            BOOL participantInRejectedState = NO;
            for (Participant *participant in room.participants) {
                if(participant.status == ParticipantStatusRejected && participant.privilege == ParticipantPrivilegeUser){
                    participantInRejectedState = YES;
                    break;
                }
            }
            XCTAssertTrue(participantInRejectedState, @"We must have a participant in rejected state");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  I change the topic of this room using this device
 */
-(void) testAsOwnerIChangeMyRoomTopic {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    
    Contact *contactToDelete = [Contact new];
    RainbowContact *rainbow = [RainbowContact new];
    rainbow.rainbowID = @"56e05bb83456faf3384272ee";
    rainbow.jid = @"c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud";
    contactToDelete.rainbow = rainbow;
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        NSLog(@"REQUEST DELETE %@", request);
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"/rooms/%@", roomID]] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"changeTopicAnswer.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        NSLog(@"DID ADD ROOM %@", room);
        if([room.displayName isEqualToString:@"Chat room 1"]){
            [_serviceManager.roomsService updateRoom:room withTopic:@"The new topic"];
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        NSLog(@"DID UPDATE ROOM %@", room);
        if([room.displayName isEqualToString:@"Chat room 1"]){
            XCTAssertTrue([room.topic isEqualToString:@"The new topic"], @"Topic must have changed");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  We change the topic of this room using another device
 *  So we receive an xmpp message with the new topic
 */
-(void) testAsOwnerIChangeMyRoomTopicFromOtherDevice {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    NSString *roomJID = @"room1@muc.mycompany.com";
    NSString *newTopic = @"New Topic";
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        // Login is OK wait a while and simulate an incoming room invitation
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' topic='%@'/></message>", [self getBare], roomID, roomJID, newTopic]];
        });
        return YES;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        // ignore room created by get conversation and get rooms
        if([room.rainbowID isEqualToString:roomID]){
            NSLog(@"DID UPDATE ROOM %@", room);
            XCTAssertTrue([room.topic isEqualToString:newTopic], @"We must have the new topic");
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My Status must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            XCTAssertTrue(room.participants.count == 2, @"We must have 2 participants");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  I change the name of this room using this device
 */
-(void) testAsOwnerIChangeMyRoomName {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    NSString *newName = @"The new name";

    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        NSLog(@"REQUEST CHANGE ROOM NAME %@", request);
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"/rooms/%@", roomID]] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"changeNameAnswer.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        NSLog(@"DID ADD ROOM %@", room);
        if([room.rainbowID isEqualToString:roomID]){
            [_serviceManager.roomsService updateRoom:room withName:newName];
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        NSLog(@"DID UPDATE ROOM %@", room);
        if([room.topic isEqualToString:@"Easter holidays"]){
            XCTAssertTrue([room.displayName isEqualToString:@"The new name"], @"Name must have changed");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  We change the name of this room using another device
 *  So we receive an xmpp message with the new name
 */
-(void) testAsOwnerIChangeMyRoomNameFromOtherDevice {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    NSString *roomJID = @"room1@muc.mycompany.com";
    NSString *newName = @"The new name";
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        // Login is OK wait a while and simulate an incoming room invitation
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' name='%@'/></message>", [self getBare], roomID, roomJID, newName]];
        });
        return YES;
    }];
    
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        // ignore room created by get conversation and get rooms
        if([room.rainbowID isEqualToString:roomID]){
            NSLog(@"DID UPDATE ROOM %@", room);
            XCTAssertTrue([room.displayName isEqualToString:newName], @"We must have the new name");
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My Status must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            XCTAssertTrue(room.participants.count == 2, @"We must have 2 participants");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

/**
 *  We have a room 'Chat room 1' populated in the sdk by fetchRoom
 *  I cancel an invitation sent for my room using this device
 */
-(void) testAsOwnerICancelAnInvitation {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    NSString *roomJID = @"room1@muc.mycompany.com";
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"/rooms/%@/users/%@", roomID, @"56e05bb83456faf3384272ee"]] && [request.HTTPMethod isEqualToString:@"DELETE"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"status":@"OK"} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        NSLog(@"DID ADD ROOM %@", room);
        if([room.displayName isEqualToString:@"Chat room 1"]){
            Contact *contactToCancel = [_contactsManagerService getContactWithRainbowID:@"56e05bb83456faf3384272ee"];
            [_serviceManager.roomsService cancelInvitationForContact:contactToCancel inRoom:room];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [_serviceManager.roomsService cancelInvitationForContact:contactToCancel inRoom:room];
            });
            return YES;
        }
        return NO;
    }];
    __block NSInteger nbOfUpdatedNotification = 0;
    // We will receive two didUpdateRoom notification, one because of the remove contact (but number of participant is not changed) , one because the API Cancel clean the participant list.
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        NSLog(@"DID UPDATE ROOM %@", room);
        if([room.displayName isEqualToString:@"Chat room 1"] && nbOfUpdatedNotification == 1){
            XCTAssertTrue(room.participants.count == 1, @"We must have one participants");
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My status in room must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            return YES;
        }
        nbOfUpdatedNotification++;
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testAsOwnerICancelAnInvitationFromOtherDevice {
    NSString *roomID = @"57e3d2db87c0b9a19e574926";
    NSString *roomJID = @"room1@muc.mycompany.com";
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *room = (Room *)notification.object;
        NSLog(@"DID ADD ROOM %@", room);
        if([room.displayName isEqualToString:@"Chat room 1"]){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542' to='%@' type='management' id='d7c6c2f3-4c16-42b2-853f-73359dda8e68_175'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='deleted'/></message>", [self getBare], roomID, roomJID, @"c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud"]];
            });
            return YES;
        }
        return NO;
    }];
    __block NSInteger nbOfUpdatedNotification = 0;
    // We will receive two didUpdateRoom notification and one because we receive the new status of the user via XMPP.
    [self expectationForNotification:kRoomsServiceDidUpdateRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Room *room = [userInfo objectForKey:kRoomKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        NSLog(@"DID UPDATE ROOM %@", room);
        if([room.displayName isEqualToString:@"Chat room 1"] /*&& nbOfUpdatedNotification == 2*/){
            XCTAssertTrue(room.participants.count == 1, @"We must have one participants");
            XCTAssertTrue(room.myStatusInRoom == ParticipantStatusAccepted, @"My status in room must be accepted");
            XCTAssertTrue(room.isMyRoom, @"This room must be mine");
            return YES;
        }
        nbOfUpdatedNotification++;
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

- (void)testUpdateParticipantPrivilege {
    
    [self expectationForNotification:kRoomsServiceDidAddRoom object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Room *theRoom = (Room *)notification.object;
        NSLog(@"DID ADD ROOM %@", theRoom);
        if([theRoom.displayName isEqualToString:@"Chat room"]){
            Contact *contactToUpdate = [_contactsManagerService getContactWithRainbowID:@"56e6bc34c219157cb207e813"];
            Participant *theParticipant = [theRoom participantFromContact:contactToUpdate];
            [_serviceManager.roomsService updateParticipant:theParticipant withPrivilege:ParticipantPrivilegeModerator inRoom:theRoom withCompletionBlock:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                XCTAssertTrue(theParticipant.privilege == ParticipantPrivilegeModerator, @"Participant privilege should be moderator");
            });
        }
        
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

@end
