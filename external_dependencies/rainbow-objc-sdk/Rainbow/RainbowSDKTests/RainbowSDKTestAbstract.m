/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowSDKTestAbstract.h"
#import "DirectoryService.h"
#import "MyUser.h"
#import "Server.h"
#import "ContactsManagerService.h"
#import "XMPPStub.h"
#import "LoginManager.h"
#import "ServicesManager.h"
#import "Tools.h"
#import "NSDictionary+JSONString.h"
#import "KeychainItemWrapper.h"

#import "XMPPMessageArchiveManagementCoreDataStorage.h"
#import "ContactsManagerService+Internal.h"
#import "ConversationsManagerService+Internal.h"
#import "RoomsService+Internal.h"
#import "NSData+JSON.h"
#import "Server+Internal.h"

#include <netdb.h>
#include <arpa/inet.h>
#import <RainbowUnitTestFakeServer/RainbowUnitTestFakeServer.h>


#define FAKE_SERVER_HOST @"localhost"
#define FAKE_SERVER_PORT 6670

static BOOL first = YES;
static NSString *barejid;
static NSString *resource;
static NSString *fulljid;
static BOOL doLogout = YES;

@implementation RainbowSDKTestAbstract {
    HTTPServer *httpServer;
}

-(Class) server {
    return [RESTAPIServer class];
}

+(void)initialize {
    [[KeychainItemWrapper defaultKeychain] overrideAppNameWithName:@"Rainbow-Tester"];
}

- (void)setUp {
    [super setUp];
    
    [self setupServer];
    
    //[self deleteAllObjectsForContext:[[XMPPMessageArchiveManagementCoreDataStorage sharedInstance] mainThreadManagedObjectContext]];
    
    [OHHTTPStubs setEnabled:YES];
    [OHHTTPStubs removeAllStubs];
    [XMPPStub removeAllStubs];
    
    // Use the SDK as a blackbox
    _serviceManager = [ServicesManager new];
    _loginManager = _serviceManager.loginManager;
    _contactsManagerService = _serviceManager.contactsManagerService;
    _myUser = _serviceManager.myUser;
    _conversationsManagerService = _serviceManager.conversationsManagerService;
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidChangeUser object:nil];
    
    first = YES;
    resource = nil;
    fulljid = nil;
    barejid = nil;

    // Fake the server
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeServerURL" object:@{@"serverURL":[NSString stringWithFormat:@"%@:%u/unsecure", FAKE_SERVER_HOST, FAKE_SERVER_PORT]}];
    [self startServer];
    [self populateServerWithDefaultsData];
}

- (void)tearDown {
    [self stopServer];
    [self.server flushData];
    [[XMPPWebSocketServer stubDescriptors] removeAllObjects];
    [[RESTAPIServer stubDescriptors] removeAllObjects];
    [NSThread sleepForTimeInterval:1];
    _conversationsManagerService = nil;
    _myUser = nil;
    _contactsManagerService = nil;
    _loginManager = nil;
    _serviceManager = nil;
    
    [OHHTTPStubs removeAllStubs];
    [XMPPStub removeAllStubs];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super tearDown];
}

-(NSString*) getFull {
    return fulljid;
}

-(NSString *) getBare {
    return barejid;
}

-(void) doLogout:(BOOL) doit {
    doLogout = doit;
}

- (void) deleteAllObjectsForContext:(NSManagedObjectContext *) managedObjectContext {
    NSError * error;
    // retrieve the store URL
    
    NSURL * storeURL = [[managedObjectContext persistentStoreCoordinator] URLForPersistentStore:[[[managedObjectContext persistentStoreCoordinator] persistentStores] lastObject]];
    // lock the current context
    //[managedObjectContext lock];
    [managedObjectContext reset];//to drop pending changes
                                 //delete the store from the current managedObjectContext
    if ([[managedObjectContext persistentStoreCoordinator] removePersistentStore:[[[managedObjectContext persistentStoreCoordinator] persistentStores] lastObject] error:&error])
        {
        // remove the file containing the data
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:&error];
        //recreate the store like in the  appDelegate method
        [[managedObjectContext persistentStoreCoordinator] addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];//recreates the persistent store
        }
    //[managedObjectContext unlock];
    //that's it !
}

-(void) doLoginAndStartXMPP {
    // This will also start the xmpp connection
    //    [_loginManager setUsername:@"ios.tester@openrainbow.com" andPassword:@"test"];
    [_loginManager setUsername:[self.server myLoginEmail] andPassword:[self.server myLoginPassword]];
    [_loginManager connect];
}


-(void) doLogout {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/logout"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSString* fixture = OHPathForFile(@"logout.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        NSLog(@"REQUEST FOR LOGOUT %@", request);
        if([request[@"presence"][@"type"] isEqualToString:@"unavailable"])
            return YES;
        return NO;
    } withStubResponse:^NSArray<NSString *> *(NSDictionary *request) {
        return @[];
    }];
    
    [self expectationForNotification:kLoginManagerDidLogoutSucceeded object:nil handler:^BOOL(NSNotification * notification) {
        NSLog(@"Logout notification received");
        
        // *VERY IMPORTANT*
        // As this notification is used to do a lot of things in the SDK, we have to wait for the other
        // Services to use it and do their stuff.
        // If we don't wait here, then we'll kill the SDK before it's shutdown is complete.
        [NSThread sleepForTimeInterval:1];
        return YES;
    }];
    
    // Stop the SDK
    [_loginManager performSelectorInBackground:@selector(disconnect) withObject:nil];
    
    
    // Wait for logout notification
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}




/**
 *  Add the default REST stub for the /login HTTP request
 *  With this default stub, the application should go on, with success.
 */
-(void) addRESTAuthStubForSuccess {
    // Stub the login REST request
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/login"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSString* fixture = OHPathForFile(@"login.json", self.class);
        
        NSData *data = [NSData dataWithContentsOfFile:fixture];
        NSDictionary *dict = [data objectFromJSONData];
        
        barejid = dict[@"loggedInUser"][@"jid_im"];
        
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTSourcePostStubForSuccess {
    // Stub the source creating REST request
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/sources"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSString* fixture = OHPathForFile(@"create-source.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTContactPostStubForSuccess {
    // Stub the upload user REST request
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/contacts"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSMutableDictionary *requestJson = [NSMutableDictionary dictionaryWithDictionary:[[request OHHTTPStubs_HTTPBody] objectFromJSONData]];
        [requestJson setObject:[Tools generateUniqueID] forKey:@"id"];
        
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"data": requestJson} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }].name = @"Contact creation stub";
}

/**
 *  Add the default XMPP stub for the <open> message
 *  With this default stub, the application should go on, with success.
 */
-(void) addXMPPOpenStubForSuccess {
    // This message is received 2 times. first time when not logged, second time when logged.
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"open"][@"xmlns"] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-framing"]) {
            XCTAssert([request[@"open"][@"to"] isEqualToString:[Server opentouchRainbowServer].serverHostname]);
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        if (first) {
            first = NO;
            return @[@"<open xmlns='urn:ietf:params:xml:ns:xmpp-framing' version='1.0' xml:lang='en' id='11223344' from='openrainbow.com'/>",
                     @"<stream:features xmlns:stream='http://etherx.jabber.org/streams'><c xmlns='http://jabber.org/protocol/caps' hash='sha-1' node='http://www.process-one.net/en/ejabberd/' ver='zCuUJRIymQcRFUx/lk/2yoOH4WQ='/><register xmlns='http://jabber.org/features/iq-register'/><mechanisms xmlns='urn:ietf:params:xml:ns:xmpp-sasl'><mechanism>DIGEST-MD5</mechanism><mechanism>SCRAM-SHA-1</mechanism><mechanism>PLAIN</mechanism></mechanisms></stream:features>"];
        } else {
            // not the same features the second time (once logged)
            return @[@"<open xmlns='urn:ietf:params:xml:ns:xmpp-framing' version='1.0' xml:lang='en' id='55667788' from='openrainbow.com'/>",
                     @"<stream:features xmlns:stream='http://etherx.jabber.org/streams'><c xmlns='http://jabber.org/protocol/caps' hash='sha-1' node='http://www.process-one.net/en/ejabberd/' ver='zCuUJRIymQcRFUx/lk/2yoOH4WQ='/><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'/><session xmlns='urn:ietf:params:xml:ns:xmpp-session'/><ver xmlns='urn:xmpp:features:rosterver'/><sm xmlns='urn:xmpp:sm:2'/><sm xmlns='urn:xmpp:sm:3'/><csi xmlns='urn:xmpp:csi:0'/></stream:features>"];
        }
    }];
}


/**
 *  Add the default XMPP stub for the <auth> message
 *  With this default stub, the application should go on, with success.
 */
-(void) addXMPPAuthStubForSuccess {
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"auth"][@"xmlns"] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-sasl"]) {
            XCTAssert([request[@"auth"][@"mechanism"] isEqualToString:@"SCRAM-SHA-1"]);
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        return @[@"<challenge xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>cj1DQzAxNjJDMy0yN0Q2LTQ0RkYtOEY5Ni01NTRCMkY0NjIzMkRXY2hzZlJvZkk0UmNMNUN4d2NpNnZnPT0scz1tbmNuVXdtay9kUnkvVWs5VzNnaFhRPT0saT00MDk2</challenge>"];
    }];
}


/**
 *  Add the default XMPP stub for the <response> message
 *  With this default stub, the application should go on, with success.
 */
-(void) addXMPPResponseStubForSuccess {
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"response"][@"xmlns"] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-sasl"]) {
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        return @[@"<success xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>dj11SVZlaVdMNlJaWHBQSnVDbkZFVlVnWUdvS2s9</success>"];
    }];
}


/**
 *  Add the default XMPP stub for the <iq> bind message
 *  With this default stub, the application should go on, with success.
 */
-(void) addXMPPIQBindStubForSuccess {
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"iq"][@"type"] isEqualToString:@"set"] &&
            [request[@"iq"][@"bind"][@"xmlns"] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-bind"]) {
            // save the resource.
            resource = request[@"iq"][@"bind"][@"resource"][@"text"];
            fulljid = [NSString stringWithFormat:@"%@/%@", barejid, resource];
            
            XCTAssert([request[@"iq"][@"bind"][@"resource"][@"text"] hasPrefix:@"mobile_ios_"]);
            
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // The client ask for its resource name. Accept it.
        return @[[NSString stringWithFormat:@"<iq xmlns='jabber:client' id='%@' type='result'><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'><jid>%@</jid></bind></iq>", request[@"iq"][@"id"], fulljid]];
    }];
}


/**
 *  Add the default XMPP stub for the <iq> session message
 *  With this default stub, the application should go on, with success.
 */
-(void) addXMPPIQSessionStubForSuccess {
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"iq"][@"type"] isEqualToString:@"set"] &&
            [request[@"iq"][@"session"][@"xmlns"] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-session"]) {
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        return @[[NSString stringWithFormat:@"<iq type='result' xmlns='jabber:client' id='%@'/>", request[@"iq"][@"id"]]];
    }];
}


/**
 *  Add the default XMPP stub for the <iq> disco items message
 *  With this default stub, the application should go on, with success.
 */
-(void) addXMPPIQDiscoItemsStubForSuccess {
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"iq"][@"type"] isEqualToString:@"get"] &&
            [request[@"iq"][@"query"][@"xmlns"] isEqualToString:@"http://jabber.org/protocol/disco#items"] &&
            [request[@"iq"][@"to"] isEqualToString:barejid]) {
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // Simulate only one resource connected (itself)
        return @[[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='%@' type='result'>\
                  <query xmlns='http://jabber.org/protocol/disco#items'>\
                  <item jid='%@' name='%@'/>\
                  </query>\
                  </iq>", barejid, fulljid, request[@"iq"][@"id"], fulljid, resource]];
    }];
}


/**
 *  Add the default XMPP stub for the <iq> enable carbon message
 *  With this default stub, the application should go on, with success.
 */
-(void) addXMPPIQEnableCarbonStubForSuccess {
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"iq"][@"type"] isEqualToString:@"set"] &&
            [request[@"iq"][@"enable"][@"xmlns"] isEqualToString:@"urn:xmpp:carbons:2"]) {
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        return @[[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='%@' type='result'/>", barejid, fulljid, request[@"iq"][@"id"]]];
    }];
}


/**
 *  Add the default XMPP stub for the <iq> get roster message
 *  With this default stub, the application should go on, with success.
 */
-(void) addXMPPIQGetRosterStubForSuccess {
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"iq"][@"type"] isEqualToString:@"get"] &&
            [request[@"iq"][@"query"][@"xmlns"] isEqualToString:@"jabber:iq:roster"]) {
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // Build a fictionnal roster.
        
        return @[[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='%@' type='result'>\
                  <query xmlns='jabber:iq:roster'>\
                  <item subscription='both' jid='1234@openrainbow.com'/>\
                  <item subscription='both' jid='tel_1234@openrainbow.com'/>\
                  </query>\
                  </iq>", barejid, fulljid, request[@"iq"][@"id"]]]; // <group>favorites</group></item>
    }];
}

-(void) addXMPPIQPresenceJoinMUCROOMForSuccess {
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"presence"][@"x"][@"xmlns"] isEqualToString:@"http://jabber.org/protocol/muc"]){
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[];
    }];
}

-(void) addXMPPEnableSM {
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"enable"][@"xmlns"] isEqualToString:@"urn:xmpp:sm:3"]){
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[@"<enabled xmlns='urn:xmpp:sm:3'/>"];
    }];
}

-(void) addRESTGetRoomsForSuccess {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/rooms"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSString* fixture = OHPathForFile(@"get_rooms.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTGetConversationsForSuccess {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSString* fixture = OHPathForFile(@"get_conversations.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTGetAvatar {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"avatar"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        return [OHHTTPStubsResponse responseWithData:[NSData data] statusCode:404 headers:@{}];
    }];
}

-(void) addRESTGetInvitationsForSuccess {
    stubGetInvitationsEmpty = [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && ([request.URL.path hasSuffix:@"/invitations/received"] || [request.URL.path hasSuffix:@"/invitations/sent"]) && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSString* fixture = OHPathForFile(@"get_invitations.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTSearchForRainbowContactsOnServerForSuccess {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/search"] && [request.HTTPMethod isEqualToString:@"POST"] && [[[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding] containsString:@"loginEmail"] ;
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"users": @[]} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTSearchForRainbowContactByRainbowIDOnServerForSuccess {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] &&
                [request.URL.path containsString:@"/users/"] &&
                ![request.URL.path containsString:@"/conversations"] &&
                ![request.URL.path containsString:@"/rooms"] &&
                ![request.URL.path containsString:@"/groups"] &&
                ![request.URL.path containsString:@"/invitations"] &&
                ![request.URL.path containsString:@"/users/loginemails"] &&
                ![request.URL.path containsString:@"/users/jids"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString *rainbowID = request.URL.lastPathComponent;
        NSLog(@"RAINBOW ID %@ URL %@", rainbowID, request.URL);
        NSString *fixture = OHPathForFile(@"searchByRainbowID.json", self.class);
        NSData *data = [NSData dataWithContentsOfFile:fixture];
        
        NSDictionary *jsonResponse = [data objectFromJSONData];
        NSMutableDictionary *requestJson = [NSMutableDictionary dictionaryWithDictionary:jsonResponse[@"data"]];
        [requestJson setObject:rainbowID forKey:@"id"];
        [requestJson setObject:[NSString stringWithFormat:@"login_%@@openrainbow.com",rainbowID] forKey:@"loginEmail"];
        [requestJson setObject:[NSString stringWithFormat:@"FirstName_%@",rainbowID] forKey:@"firstName"];
        [requestJson setObject:[NSString stringWithFormat:@"LastName_%@",rainbowID] forKey:@"lastName"];
        [requestJson setObject:[NSString stringWithFormat:@"%@@openrainbow.com",rainbowID] forKey:@"jid_im"];
        
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"data":requestJson} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTSearchForRainbowContactsbyJidsOnServerForSuccess {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/users/jids"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString *body = [[NSString alloc] initWithData:request.OHHTTPStubs_HTTPBody encoding:NSUTF8StringEncoding];
        NSString* fixture = nil;
        if([body containsString:@"1234@openrainbow.com"])
            fixture = OHPathForFile(@"search_by_jid_1234.json", self.class);
        else if ([body containsString:@"fake_conversations_at_startup@openrainbow.com"])
            fixture = OHPathForFile(@"search_by_jid_fake_conversations.json", self.class);
        else if ([body containsString:@"new_friend@openrainbow.com"])
            fixture = OHPathForFile(@"search_by_jid_new_friend.json", self.class);
        else if ([body containsString:@"new_friend_not_in_same_company@openrainbow.com"])
            fixture = OHPathForFile(@"search_by_jid_new_friend_not_same_company.json", self.class);
        else
            fixture = OHPathForFile(@"search_by_jids_response.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTSearchForRainbowContactsbyLoginEmailsOnServerForSucess {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/users/loginemails"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString *body = [[NSString alloc] initWithData:request.OHHTTPStubs_HTTPBody encoding:NSUTF8StringEncoding];
        NSString* fixture = nil;
        if([body containsString:@"1234@openrainbow.com"])
            fixture = OHPathForFile(@"search_by_jid_1234.json", self.class);
        else if ([body containsString:@"fake_conversations_at_startup@openrainbow.com"])
            fixture = OHPathForFile(@"search_by_jid_fake_conversations.json", self.class);
        else if ([body containsString:@"new_friend@openrainbow.com"])
            fixture = OHPathForFile(@"search_by_jid_new_friend.json", self.class);
        else if ([body containsString:@"new_friend_not_in_same_company@openrainbow.com"])
            fixture = OHPathForFile(@"search_by_jid_new_friend_not_same_company.json", self.class);
        else
            fixture = OHPathForFile(@"search_by_jids_response.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTGetServerVersion {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/about"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"version":@"1.15.0", @"description":@"Rainbow"} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTGetGroups {
    stubGetGroupsEmpty = [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/groups"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"getGroupsEmpty.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTGetFeatures {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"profiles/features"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"getFeatures.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTGetCompaniesRequests {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/join-companies/requests"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTGetCompaniesInvitations {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/join-companies/invitations"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTGetFileSharingConsumption {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/users/consumption"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTGetFileStorage {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/filestorage/v1.0/files"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTGetUserNetwork {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/users/networks"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"getUserNetworks.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) addRESTGetUserSettings {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/settings"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

#pragma mark - ContactsManagerService delegates

-(void) contactsService:(ContactsManagerService *) contactService didAddContact:(Contact *) addedContact {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:addedContact];
}
-(void) contactsService:(ContactsManagerService *) contactService didUpdateContact:(Contact *) updatedContact withChangedKeys:(NSArray<NSString *> *) changedKeys {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:@{kContactKey: updatedContact, kChangedAttributesKey: changedKeys}];
}
-(void) contactsService:(ContactsManagerService *) contactService didRemoveContact:(Contact *) removedContact {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:removedContact];
}


#pragma mark - Server
-(void) setupServer {
    // Check if the hostname we use in correctly 127.0.0.1, or tests will not work
    struct hostent *host_entry = gethostbyname(FAKE_SERVER_HOST.UTF8String);
    NSAssert(host_entry != NULL, @"Error: you have to add entry '127.0.0.1 %@' to your /etc/hosts or UI tests will not work.", FAKE_SERVER_HOST);
    char *buff;
    buff = inet_ntoa(*((struct in_addr *)host_entry->h_addr_list[0]));
    NSAssert(strncmp("127.0.0.1", buff, strlen("127.0.0.1")) == 0, @"Error: you have to add entry '127.0.0.1 %@' to your /etc/hosts or UI tests will not work.", FAKE_SERVER_HOST);
    
    // Add myself to as user in server, so search will work
    ContactAPI *contact = [ContactAPI new];
    contact.ID = [self.server myRainbowID];
    contact.jid = [self.server myJID];
    contact.loginEmail = [self.server myLoginEmail];
    contact.firstname = @"Alice";
    contact.lastname = @"Tester";
    contact.companyId = [self.server myCompanyID];
    contact.companyName = [self.server myCompanyName];
    [self.server addContact:contact];
    
    
    // This server will fake both the REST API *and* the XMPP websocket servers.
    httpServer = [[HTTPServer alloc] init];
    [httpServer setPort:FAKE_SERVER_PORT];
    [httpServer setConnectionClass:self.server];
    
    NSString *bundlePath = [NSBundle bundleForClass:self.class].bundlePath;
    [self.server setBundlePathForDocuments:bundlePath];
}

-(void) startServer {
    NSError *error = nil;
    if([httpServer start:&error]) {
        NSLog(@"Started HTTP Server on port %hu", [httpServer listeningPort]);
    } else {
        NSLog(@"Error starting HTTP Server: %@", error);
    }
}

-(void) stopServer {
    NSLog(@"Stopping HTTP Server on port %hu", [httpServer listeningPort]);
    [self.server closeWebsocket];
    [self.server clean];
    [httpServer stop];
}

-(void) populateServerWithDefaultsData {
    
}


@end
