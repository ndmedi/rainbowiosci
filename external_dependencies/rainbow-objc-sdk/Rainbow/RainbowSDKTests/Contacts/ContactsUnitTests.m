/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import "ContactInternal.h"
#import "EmailAddressInternal.h"

#ifndef TARGET_IPHONE_SIMULATOR
#error "Simulator only !"
#endif

@interface ContactsUnitTests : XCTestCase
@end

@implementation ContactsUnitTests

-(void) testKVOonRainbowContact {
    RainbowContact *rainbow = [RainbowContact new];
    Presence *presence = [Presence presenceAway];
    NSDate *date = [NSDate date];
    
    [self keyValueObservingExpectationForObject:rainbow keyPath:kContactFirstNameKey expectedValue:@"Marcel"];
    [self keyValueObservingExpectationForObject:rainbow keyPath:kContactLastNameKey expectedValue:@"Dupont"];
    [self keyValueObservingExpectationForObject:rainbow keyPath:kContactPresenceKey expectedValue:presence];
    [self keyValueObservingExpectationForObject:rainbow keyPath:kContactLastActivityDateKey expectedValue:date];
    
    rainbow.firstName = @"Marcel";
    rainbow.lastName = @"Dupont";
    rainbow.presence = presence;
    rainbow.lastActivityDate = date;
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testKVOAfterAddedRainbowPart {
    Contact *contact = [Contact new];
    contact.rainbow = [RainbowContact new];

    Presence *presence = [Presence presenceAway];
    NSDate *date = [NSDate date];
    
    [self keyValueObservingExpectationForObject:contact keyPath:kContactFirstNameKey expectedValue:@"Marcel"];
    [self keyValueObservingExpectationForObject:contact keyPath:kContactLastNameKey expectedValue:@"Dupont"];
    [self keyValueObservingExpectationForObject:contact keyPath:kContactPresenceKey expectedValue:presence];
    [self keyValueObservingExpectationForObject:contact keyPath:kContactLastActivityDateKey expectedValue:date];
    
    contact.rainbow.firstName = @"Marcel";
    contact.rainbow.lastName = @"Dupont";
    contact.rainbow.presence = presence;
    contact.rainbow.lastActivityDate = date;
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testKVOWhenAddingRainbowPart {
    Contact *contact = [Contact new];
    Presence *presence = [Presence presenceAway];
    NSDate *date = [NSDate date];
    RainbowContact *rainbow = [RainbowContact new];
    
    rainbow.firstName = @"Marcel";
    rainbow.lastName = @"Dupont";
    rainbow.presence = presence;
    rainbow.lastActivityDate = date;
    
    // Adding KVO on the contact.
    [self keyValueObservingExpectationForObject:contact keyPath:kContactFirstNameKey expectedValue:@"Marcel"];
    [self keyValueObservingExpectationForObject:contact keyPath:kContactLastNameKey expectedValue:@"Dupont"];
    [self keyValueObservingExpectationForObject:contact keyPath:kContactPresenceKey expectedValue:presence];
    [self keyValueObservingExpectationForObject:contact keyPath:kContactLastActivityDateKey expectedValue:date];
    
    // Now, when affecting the rainbow part to the contact
    // it should trigger the KVOs.
    
    contact.rainbow = rainbow;
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testKVOWhenAddingRainbowPartToLocalContact {
    // We have a contact with a local part.
    Contact *contact = [Contact new];
    LocalContact *local = [LocalContact new];
    
    local.firstName = @"Marcel";
    local.lastName = @"Dupont";
    [contact addLocalContact:local];
    
    // create a rainbow part
    RainbowContact *rainbow = [RainbowContact new];
    
    rainbow.firstName = @"Marcel";
    rainbow.lastName = @"Dujardin";
    
    // Adding KVO on the contact (we DONT expect them to happend)
    // If they happend, they will fail in the observeValueForKeyPath:ofObject: function
    [contact addObserver:self forKeyPath:kContactFirstNameKey options:NSKeyValueObservingOptionNew context:nil];
    [contact addObserver:self forKeyPath:kContactLastNameKey options:NSKeyValueObservingOptionNew context:nil];
    
    // But when adding the rainbow part to the contact
    // The local part has priority
    // So the names of the rainbow part are ignored.
    // KVOs SHOULD NOT be triggered
    contact.rainbow = rainbow;
    
    [NSThread sleepForTimeInterval:0.2f];
    
    [contact removeObserver:self forKeyPath:kContactFirstNameKey];
    [contact removeObserver:self forKeyPath:kContactLastNameKey];
}

-(void) testKVOWhenAddingRainbowPartToLocalContactWithoutLastName {
    // We have a contact with a local part.
    Contact *contact = [Contact new];
    LocalContact *local = [LocalContact new];
    
    local.firstName = @"Marcel";
    // NO lastname defined in local part.
    [contact addLocalContact:local];
    
    // create a rainbow part
    RainbowContact *rainbow = [RainbowContact new];
    
    rainbow.firstName = @"Marcel-Gerard";
    rainbow.lastName = @"Dujardin";
    
    // Adding KVO on the contact (we DONT expect them to happend)
    // If they happend, they will fail in the observeValueForKeyPath:ofObject: function
    [contact addObserver:self forKeyPath:kContactFirstNameKey options:NSKeyValueObservingOptionNew context:nil];
    
    // But we expect the lastName KVO to be triggered !
    [self keyValueObservingExpectationForObject:contact keyPath:kContactLastNameKey expectedValue:@"Dujardin"];
    
    // But when adding the rainbow part to the contact
    // The local part has priority
    // So only the lastname of the rainbow part is used.
    // KVOs SHOULD BE PARTIALLY triggered
    contact.rainbow = rainbow;
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    [NSThread sleepForTimeInterval:0.2f];
    
    [contact removeObserver:self forKeyPath:kContactFirstNameKey];
}





// DO NOT CHANGE/REMOVE THIS.
// The purpose it to make fail anybody which come here.
-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    // FAIL in any case.
    XCTAssert(NO, @"Got KVO change on %@ with path %@", object, keyPath);
}

@end
