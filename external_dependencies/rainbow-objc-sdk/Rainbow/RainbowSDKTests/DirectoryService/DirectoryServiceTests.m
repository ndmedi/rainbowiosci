/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowSDKTestAbstract.h"

@interface DirectoryServiceTests: RainbowSDKTestAbstract

@end

@implementation DirectoryServiceTests

- (void)setUp {
    [super setUp];
    [self doLoginAndStartXMPP];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)NO_testSearchRequestWithPattern {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"search"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSString* fixture = OHPathForFile(@"search.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"High Expectations"];
    /*
    [_directoryService searchRequestWithPattern:@"he" withCompletionBlock:^(NSArray<Contact *> *searchResult, NSError *error) {
        XCTAssert(!error);
        XCTAssert(searchResult.count == 5, @"Search result must be equal to 5");
        [expectation fulfill];
    }];*/
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

//

@end
