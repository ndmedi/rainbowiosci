/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import "RainbowSDKTestAbstract.h"
#import "XMPPMessage.h"
#import "XMPPService.h"
#import "XMPPDateTimeProfiles.h"
#import "NSDate+Utilities.h"
#import "NSDate+Equallity.h"

////// DUMMY MOCKS ///////

@interface ContactMock : NSObject
@property NSString *jid;
@property NSString *displayName;
@end
@implementation ContactMock
@end

@interface ContactsManagerServiceMock : NSObject
@property Contact *myContact;
@property MyUser *myUser;
@property NSMutableArray <ContactMock *> *contacts;
-(Contact *) getContactWithJid:(NSString *) jid;
-(Contact *) createOrUpdateRainbowContactWithJid:(NSString *) jid;
@end
@implementation ContactsManagerServiceMock
-(instancetype) init {
    _contacts = [NSMutableArray new];
    return self;
}
-(Contact *) getContactWithJid:(NSString *) jid {
    for (ContactMock *contact in _contacts) {
        if ([contact.jid isEqualToString:jid]) {
            return (Contact *) contact;
        }
    }
    return nil;
}
-(Contact *) createOrUpdateRainbowContactWithJid:(NSString *) jid {
    ContactMock *contact = [ContactMock new];
    contact.jid = jid;
    [_contacts addObject:contact];
    return (Contact *) contact;
}
@end

@interface RoomsServiceMock : NSObject
-(Room*) getRoomByJid:(NSString *) jid;
@end
@implementation RoomsServiceMock
-(Room*) getRoomByJid:(NSString *) jid {
    // cheat a little bit
    ContactMock *contact = [ContactMock new];
    contact.jid = jid;
    return (Room *) contact;
}
@end
///////////////////

@interface XMPPService (UnitTest)
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message;
@end

@interface XMPPServiceUnitTest : XCTestCase <XMPPServiceConversationDelegate>
@property XMPPService *xmppService;
@property ContactsManagerServiceMock *cms;
@property RoomsServiceMock *rs;
@end

@interface XMPPService (UnitTest)
-(Message *) messageFromXmppMessage:(XMPPMessage *) aMessage;
@end

@implementation XMPPServiceUnitTest

- (void)setUp {
    [super setUp];
    
    _cms = [ContactsManagerServiceMock new];
    _cms.myContact = [_cms createOrUpdateRainbowContactWithJid:@"me@domain.com"];
    _xmppService = [[XMPPService alloc] initWithCertificateManager:nil contactManagerService:(ContactsManagerService *) _cms];
    
    _rs = [RoomsServiceMock new];
    _xmppService.roomsService = (RoomsService *) _rs;
    _xmppService.conversationDelegate = self;
}

- (void)tearDown {
    [super tearDown];
    
    _xmppService.roomsService = nil;
    _rs = nil;
    _xmppService = nil;
    _cms = nil;
}

-(void) testMessageFromXMPPMessage_1to1_outgoing {
    NSError *error = nil;
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message type='chat' to='remote@domain.com' id='1234'><body>Envoi 1 to 1</body></message>" error:&error];
    
    Message *message = [_xmppService messageFromXmppMessage:[XMPPMessage messageFromElement:element]];
    
    XCTAssertEqual(message.type, MessageTypeChat);
    XCTAssertTrue([message.messageID isEqualToString:@"1234"]);
    XCTAssertTrue(message.isOutgoing);
    XCTAssertTrue([message.peer.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue([message.via.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue([message.body isEqualToString:@"Envoi 1 to 1"]);
    XCTAssertFalse(message.isComposing);
}

-(void) testMessageFromXMPPMessage_1to1_chatstate_incoming {
    NSError *error = nil;
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='remote@domain.com/resource' to='me@domain.com' type='chat'><composing xmlns='http://jabber.org/protocol/chatstates'/></message>" error:&error];
    
    Message *message = [_xmppService messageFromXmppMessage:[XMPPMessage messageFromElement:element]];
    
    XCTAssertEqual(message.type, MessageTypeChat);
    XCTAssertFalse(message.isOutgoing);
    XCTAssertTrue([message.peer.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue([message.via.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue(message.isComposing);
}

-(void) testMessageFromXMPPMessage_1to1_incoming {
    NSError *error = nil;
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='remote@domain.com/resource1' to='me@domain.com' type='chat' id='1234'><archived xmlns='urn:xmpp:mam:tmp' by='domain.com' id='mam_id'/><stanza-id xmlns='urn:xmpp:sid:0' by='domain.com' id='mam_id'/><body xmlns='jabber:client'>Reception 1 to 1</body><request xmlns='urn:xmpp:receipts'/><active xmlns='http://jabber.org/protocol/chatstates'/></message>" error:&error];
    
    Message *message = [_xmppService messageFromXmppMessage:[XMPPMessage messageFromElement:element]];
    
    XCTAssertEqual(message.type, MessageTypeChat);
    XCTAssertFalse(message.isOutgoing);
    XCTAssertTrue([message.messageID isEqualToString:@"1234"]);
    XCTAssertTrue([message.peer.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue([message.via.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue([message.body isEqualToString:@"Reception 1 to 1"]);
    XCTAssertFalse(message.isComposing);
}

-(void) testMessageFromXMPPMessage_1to1_mam_outgoing {
    NSError *error = nil;
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='me@domain.com/resource' to='remote@domain.com' type='chat' id='1234'><body>Envoi 1 to 1</body><request xmlns='urn:xmpp:receipts'/><ack received='true' read='true'/></message>" error:&error];
    
    Message *message = [_xmppService messageFromXmppMessage:[XMPPMessage messageFromElement:element]];
    
    XCTAssertEqual(message.type, MessageTypeChat);
    XCTAssertTrue(message.isOutgoing);
    XCTAssertTrue([message.messageID isEqualToString:@"1234"]);
    XCTAssertTrue([message.peer.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue([message.via.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue([message.body isEqualToString:@"Envoi 1 to 1"]);
    XCTAssertFalse(message.isComposing);
}

#pragma mark - isOutgoing with groupchat

-(void) testMessageFromXMPPMessage_1toN_outgoing {
    NSError *error = nil;
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message type='groupchat' to='room_1@muc.domain.com' id='1234'><body>Envoi 1 to N</body></message>" error:&error];
    
    Message *message = [_xmppService messageFromXmppMessage:[XMPPMessage messageFromElement:element]];
    
    XCTAssertEqual(message.type, MessageTypeGroupChat);
    XCTAssertTrue(message.isOutgoing);
    XCTAssertTrue([message.messageID isEqualToString:@"1234"]);
    XCTAssertTrue([message.peer.jid isEqualToString:@"room_1@muc.domain.com"]);
    XCTAssertTrue([message.via.jid isEqualToString:@"room_1@muc.domain.com"]);
    XCTAssertTrue([message.body isEqualToString:@"Envoi 1 to N"]);
    XCTAssertFalse(message.isComposing);
}

-(void) testMessageFromXMPPMessage_1toN_room_outgoing {
    NSError *error = nil;
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='room_1@muc.domain.com/me@domain.com/resource' to='me@domain.com/resource' type='groupchat' id='1234'><archived xmlns='urn:xmpp:mam:tmp' by='muc.domain.com' id='mam_id'/><stanza-id xmlns='urn:xmpp:sid:0' by='muc.domain.com' id='mam_id'/><body xmlns='jabber:client'>Envoi 1 to N</body><request xmlns='urn:xmpp:receipts'/></message>" error:&error];
    
    Message *message = [_xmppService messageFromXmppMessage:[XMPPMessage messageFromElement:element]];
    
    XCTAssertEqual(message.type, MessageTypeGroupChat);
    XCTAssertTrue(message.isOutgoing);
    XCTAssertTrue([message.messageID isEqualToString:@"1234"]);
    XCTAssertTrue([message.peer.jid isEqualToString:@"me@domain.com"]);
    XCTAssertTrue([message.via.jid isEqualToString:@"room_1@muc.domain.com"]);
    XCTAssertTrue([message.body isEqualToString:@"Envoi 1 to N"]);
    XCTAssertFalse(message.isComposing);
}

-(void) testMessageFromXMPPMessage_1toN_incoming {
    NSError *error = nil;
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='room_1@muc.domain.com/remote@domain.com/resource' to='me@domain.com/resource' type='groupchat' id='1234'><archived xmlns='urn:xmpp:mam:tmp' by='muc.domain.com' id='mam_id'/><stanza-id xmlns='urn:xmpp:sid:0' by='muc.domain.com' id='mam_id'/><body xmlns='jabber:client'>Reception 1 to N</body><request xmlns='urn:xmpp:receipts'/></message>" error:&error];
    
    Message *message = [_xmppService messageFromXmppMessage:[XMPPMessage messageFromElement:element]];
    
    XCTAssertEqual(message.type, MessageTypeGroupChat);
    XCTAssertFalse(message.isOutgoing);
    XCTAssertTrue([message.messageID isEqualToString:@"1234"]);
    XCTAssertTrue([message.peer.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue([message.via.jid isEqualToString:@"room_1@muc.domain.com"]);
    XCTAssertTrue([message.body isEqualToString:@"Reception 1 to N"]);
    XCTAssertFalse(message.isComposing);
}

-(void) testMessageFromXMPPMessage_1toN_chatstate_incoming {
    NSError *error = nil;
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='room_1@muc.domain.com/remote@domain.com/resource' to='me@domain.com/resource' type='groupchat'><composing xmlns='http://jabber.org/protocol/chatstates'/></message>" error:&error];
    
    Message *message = [_xmppService messageFromXmppMessage:[XMPPMessage messageFromElement:element]];
    
    XCTAssertEqual(message.type, MessageTypeGroupChat);
    XCTAssertFalse(message.isOutgoing);
    XCTAssertTrue([message.peer.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue([message.via.jid isEqualToString:@"room_1@muc.domain.com"]);
    XCTAssertTrue(message.isComposing);
}

-(void) testMessageFromXMPPMessage_1to1_chatstate_delayed_after_reconnection {
    NSError *error = nil;

    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='remote@domain.com/resource' to='me@domain.com' type='chat'><composing xmlns='http://jabber.org/protocol/chatstates'/><delay xmlns='urn:xmpp:delay' from='domain.com' stamp='2016-09-13T14:05:24.184Z'>Resent</delay></message>" error:&error];
    
    Message *message = [_xmppService messageFromXmppMessage:[XMPPMessage messageFromElement:element]];
    
    XCTAssertEqual(message.type, MessageTypeChat);
    XCTAssertFalse(message.isOutgoing);
    XCTAssertTrue([message.peer.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue([message.via.jid isEqualToString:@"remote@domain.com"]);
    XCTAssertTrue(message.isComposing);
    XCTAssert([message.timestamp isEqualToDate:[XMPPDateTimeProfiles parseDateTime:@"2016-09-13T14:05:24.184Z"]]);
}


-(void) testInjectMessage {
    NSError *error = nil;
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='3f5e210b41ff4e0aae5afba660cd277a@openrainbow.com' to='3f5e210b41ff4e0aae5afba660cd277a@openrainbow.com/mobile_ios_AE8A4CA3-4895-4B80-BFE6-913BBBE0549F' type='chat'><received xmlns='urn:xmpp:carbons:2'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='18a53255c15d47ea81d0b8f12aff0b8c@openrainbow.com/desk_win_1.36.8_JCdmxlcy' to='3f5e210b41ff4e0aae5afba660cd277a@openrainbow.com/web_win_1.36.8_AdHR0eD6' type='chat' id='web_a43b1520-6603-43b9-8df2-9a4212936d4a31' lang='fr'><archived xmlns='urn:xmpp:mam:tmp' by='openrainbow.com' stamp='2018-02-19T16:55:30.338621Z'/><body>m********e</body><request xmlns='urn:xmpp:receipts'/><active xmlns='http://jabber.org/protocol/chatstates'/><delay xmlns='urn:xmpp:delay' from='openrainbow.com' stamp='2018-02-19T16:55:30.339Z'/><retransmission xmlns='jabber:iq:notification' source='push'/></message></forwarded></received><delay xmlns='urn:xmpp:delay' from='openrainbow.com' stamp='2018-02-19T17:40:16.432Z'>Resent</delay></message>" error:&error];
    
    Message *message = [_xmppService messageFromXmppMessage:[XMPPMessage messageFromElement:element]];
    
    XCTAssertEqual(message.type, MessageTypeChat);
    XCTAssert([message.timestamp isEqualToDate:[XMPPDateTimeProfiles parseDateTime:@"2018-02-19T16:55:30.338621Z"]]);
}

-(void) testOnResumeCheckMessageDate {
    [self expectationForNotification:@"XCT-xmppService:didReceiveMessage:-Delegate" object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Message *message = (Message *)notification.object;
        NSLog(@"MESSAGE %@", message);
        XCTAssert(message.timestamp);
    }];
    NSString* fileRoot = [[NSBundle bundleForClass:[self class]] pathForResource:@"messagesOnResume" ofType:@"xml"];
    NSString* fileContents = [NSString stringWithContentsOfFile:fileRoot encoding:NSUTF8StringEncoding error:nil];
    NSArray* allLinedStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    NSString *regexp = @"^.*stamp='(.*?)'>.*$";
    NSError *regExpError = nil;
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexp options:NSRegularExpressionDotMatchesLineSeparators error:&regExpError];
    for (NSString *line in allLinedStrings) {
        NSTextCheckingResult *matchText = [regExp firstMatchInString:line options:NSMatchingReportProgress range:NSMakeRange(0, line.length)];
        NSRange matchRange = [matchText rangeAtIndex:1];
        NSString *stamp = [line substringWithRange:matchRange];
        NSDate *stampDate = [XMPPDateTimeProfiles parseDateTime:stamp];
        NSError *error = nil;
        NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:line error:&error];
        XMPPMessage *xmppMessage = [XMPPMessage messageFromElement:element];
        [_xmppService xmppStream:nil didReceiveMessage:xmppMessage];
        
        Message *message = [_xmppService messageFromXmppMessage:xmppMessage];
        NSLog(@"LINE IS %@", line);
        NSLog(@"Message date is %@ : Test : %@ stamp %@", [message.timestamp xmppDateTimeString], NSStringFromBOOL([message.timestamp isEqualToDateAndTime:stampDate]), stamp);
        XCTAssertFalse([message.timestamp isEqualToDateAndTime:stampDate]);
    }
    
//    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError *error) {
//        if (error) {
//            NSLog(@"Timeout Error: %@", error);
//        }
//    }];
}

#pragma mark - xmpp delegate
-(void) xmppService:(XMPPService *) service willSendMessage:(Message *) message {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:message];
}
-(void) xmppService:(XMPPService *) service didSendMessage:(Message *) message {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:message];
}
-(void) xmppService:(XMPPService *) service didReceiveMessage:(Message *) message {
    NSString *name = GENERATE_NOTIF_NAME;
    [[NSNotificationCenter defaultCenter] postNotificationName:name object:message];
}
-(void) xmppService:(XMPPService *) service didReceiveCarbonCopyMessage:(Message *)message {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:message];
}
-(void) xmppService:(XMPPService *) service didReceiveComposingMessage:(Message *)message {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:message];
}

-(void) xmppService:(XMPPService *) service didReceiveArchivedMessage:(Message *)message {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:message];
}

-(void) xmppService:(XMPPService *) service didReceiveDeliveryState:(MessageDeliveryState) state at:(NSDate*) datetime forMessage:(Message *) message via:(Peer*) via isOutgoing:(BOOL) isOutgoing {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:nil];
}
-(void) xmppService:(XMPPService *) service didReceiveMuteConversation:(NSString *) conversationID {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:nil];
}
-(void) xmppService:(XMPPService *) service didReceiveUnMuteConversation:(NSString *) conversationID {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:nil];
}

-(void) xmppService:(XMPPService *) service didRetreiveNewLastMessageFromArchives:(Message *) lastMessage {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:lastMessage];
}
-(void) xmppService:(XMPPService *) service didReceiveDeleteConversationID:(NSString *) conversationID {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:nil];
}
-(void) xmppService:(XMPPService *) service didRemoveAllMessagesForPeer:(Peer *) peer {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:nil];
}

-(void) xmppService:(XMPPService *) service didReceiveDeleteAllMessagesInConversationWithPeerJID:(NSString *) peerJID {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:nil];
}
-(void) xmppService:(XMPPService *) service didReceiveDeleteMessageID:(NSString *) messageID inConversationWithPeerJID:(NSString *) peerJID {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:nil];
}

-(void) xmppService:(XMPPService *)service didReceiveChangedInConversationWithPeer:(Peer *) peer {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:nil];
}

@end
