/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import "RainbowSDKTestAbstract.h"
#import "XMPPMessageArchiveManagement.h"
#import "XMPPMessageArchiveManagementCoreDataStorage.h"
#import "Peer+Internal.h"
#import "XMPPDateTimeProfiles.h"
#import "NSDate+Equallity.h"
#import "XMPPIDTracker.h"
#import "ContactsManagerService+Internal.h"

@interface XMPPService ()
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message;
@end

@implementation Contact (DisplayName)
-(NSString *) displayName {
    @synchronized (self) {
        if(self.fullName.length > 0) {
            return self.fullName;
        } else if (self.emailAddresses.count > 0){
            NSString *firstEmail = ((EmailAddress *)self.emailAddresses[0]).address;
            return firstEmail;
        } else if(self.companyName.length > 0){
            return self.companyName;
        } else if(self.phoneNumbers.count > 0){
            NSString *firstPhoneNumber = ((PhoneNumber*)self.phoneNumbers[0]).number;
            return firstPhoneNumber;
        }
        else {
            return NSLocalizedString(@"Unknown", nil);
        }
    }
}
@end

@interface MyXMPP_stream : XMPPStream
@property XMPPJID *myJID;
@end
@implementation MyXMPP_stream
@end


@interface XMPPMessageArchiveManagement (Blabla)<XMPPStreamDelegate>
@property (nonatomic, strong) XMPPIDTracker *xmppIDTracker;
-(void)handleFetchCallLogQueryIQ:(XMPPIQ *)iq withInfo:(XMPPBasicTrackingInfo *)basicTrackingInfo;
-(void)handleFetchMamQueryIQ:(XMPPIQ *)iq withInfo:(XMPPBasicTrackingInfo *)basicTrackingInfo;
@end


@interface XMPPMAMUnitTests : XCTestCase
@property XMPPMessageArchiveManagement *module;
@property XMPPMessageArchiveManagementCoreDataStorage *storage;
@property MyXMPP_stream *stream;
@property XMPPService *service;
@property ContactsManagerService *cms;
@property MyUser *myUser;
@property dispatch_queue_t moduleQueue;
@end

@implementation XMPPMAMUnitTests

- (void)setUp {
    [super setUp];

    _stream = [MyXMPP_stream new];
    _stream.myJID = [XMPPJID jidWithString:@"me@domain.com"];
    
    _storage = [XMPPMessageArchiveManagementCoreDataStorage sharedInstance];
    _moduleQueue = dispatch_queue_create("testModuleQueue", NULL);
    _module = [[XMPPMessageArchiveManagement alloc] initWithMessageArchiveManagementStorage:_storage dispatchQueue:_moduleQueue];
    [_module activate:_stream];
    [_module addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    // flush cache
    PageData *pageData = [PageData new];
    pageData.to = [NSDate date];
    pageData.fromJid = @"friend@domain.com";
    [_storage deleteCachedMessageNotInPageData:pageData xmmpStream:_stream];
    _myUser = [[MyUser alloc] init];
    _cms = [[ContactsManagerService alloc] initWithDownloadManager:nil myUser:_myUser apiUrlManagerService:nil];
    
    _service = [[XMPPService alloc] initWithCertificateManager:nil contactManagerService:_cms];
    _storage.xmppService = _service;
    
}

/**
 *  Check the right notification is generated when starting a MAM request
 */
-(void) testNotifGeneratedOnBeginFetch {
    
    [self expectationForNotification:GET_NOTIF_NAME(@selector(xmppMessageArchiveManagement:didBeginFetchingArchiveForUUID:)) object:nil handler:^(NSNotification *notification) {
        NSString *uuid = notification.object;
        XCTAssert([uuid isEqualToString:@"getMam_me@domain.com_UUID-1111"]);
        return YES;
    }];
    
    // make a MAM request to server
    Peer *peer = [Peer new];
    peer.jid = @"friend@domain.com";
    [_module fetchArchivedMessagesWithPeer:peer withUUID:@"getMam_me@domain.com_UUID-1111" maxSize:@(5) beforeDate:nil lastMessageID:nil];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  Check the right notification is generated when receiving a MAM forwarded <message>
 */
-(void) testNotifGeneratedOnMamMessageReceived {
    
    // make a MAM request to server
    Peer *peer = [Peer new];
    peer.jid = @"friend@domain.com";
    [_module fetchArchivedMessagesWithPeer:peer withUUID:@"getMam_me@domain.com_UUID-1111" maxSize:@(5) beforeDate:nil lastMessageID:nil];
    
    [self expectationForNotification:GET_NOTIF_NAME(@selector(xmppMessageArchiveManagement:didEndArchiveMessage:)) object:nil handler:^(NSNotification *notification) {
        XMPPMessage *message = notification.object;
        // TODO: add Assert on message content
        return YES;
    }];
    
    NSError *error = nil;
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc'><result xmlns='urn:xmpp:mam:1' id='mam_message_id' queryid='mam_query_id'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='friend@domain.com/rc' to='me@domain.com' type='chat' id='message_id'><body>Hello there !</body><request xmlns='urn:xmpp:receipts'/><ack received='true' read='true'/></message><delay xmlns='urn:xmpp:delay' from='domain.com' stamp='2016-06-30T12:00:00.000Z'/></forwarded></result></message>" error:&error];
    XMPPMessage *message = [XMPPMessage messageFromElement:element];
    
    [_module xmppStream:nil didReceiveMessage:message];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testDeleteAllMsgs {
    
    //first insert 2 messages in cache.
    NSError *error = nil;
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc'><result xmlns='urn:xmpp:mam:1' id='mam_message_id' queryid='mam_query_id'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='friend@domain.com/rc' to='me@domain.com' type='chat' id='message_id'><body>Hello there !</body><request xmlns='urn:xmpp:receipts'/><ack received='true' read='true'/></message><delay xmlns='urn:xmpp:delay' from='domain.com' stamp='2016-06-30T12:00:00.000Z'/></forwarded></result></message>" error:&error];
    XMPPMessage *message = [XMPPMessage messageFromElement:element];
    [_storage archiveMessage:message outgoing:NO xmppStream:_stream];
    
    error = nil;
    element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc'><result xmlns='urn:xmpp:mam:1' id='mam_message_id_2' queryid='mam_query_id'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='friend@domain.com/rc' to='me@domain.com' type='chat' id='message_id_2'><body>How are you ?</body><request xmlns='urn:xmpp:receipts'/><ack received='true' read='true'/></message><delay xmlns='urn:xmpp:delay' from='domain.com' stamp='2016-06-30T12:01:00.000Z'/></forwarded></result></message>" error:&error];
    message = [XMPPMessage messageFromElement:element];
    [_storage archiveMessage:message outgoing:NO xmppStream:_stream];
    
    // Make sure we have 2 elements in cache
    NSArray *result = [_storage fetchMessagesForJID:[XMPPJID jidWithString:@"friend@domain.com"] xmmpStream:_stream maxSize:@(10) offset:@(0)];
    XCTAssertEqual([result count], 2);
    
    
    
    // Now make a MAM request to server Which return no messages.
    Peer *peer = [Peer new];
    peer.jid = @"friend@domain.com";
    [_module fetchArchivedMessagesWithPeer:peer withUUID:@"getMam_me@domain.com_UUID-1111" maxSize:@(5) beforeDate:nil lastMessageID:nil];
    
    error = nil;
    element = [[NSXMLElement alloc] initWithXMLString:@"<iq xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc' id='getMam_me@domain.com_UUID-1111' type='result'><fin xmlns='urn:xmpp:mam:1' queryid='getMam_me@domain.com_UUID-1111' complete='true'><set xmlns='http://jabber.org/protocol/rsm'><first></first><last></last><count>0</count></set></fin></iq>" error:&error];
    XMPPIQ *iq = [XMPPIQ iqFromElement:element];
    [_module xmppStream:nil didReceiveIQ:iq];
    
    // And now, we should have an empty cache.
    result = [_storage fetchMessagesForJID:[XMPPJID jidWithString:@"friend@domain.com"] xmmpStream:_stream maxSize:@(10) offset:@(0)];
    XCTAssertEqual([result count], 0);
}

-(void) testReplaceMsgs {
    
    //first insert 2 messages in cache.
    NSError *error = nil;
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc'><result xmlns='urn:xmpp:mam:1' id='mam_message_id' queryid='mam_query_id'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='friend@domain.com/rc' to='me@domain.com' type='chat' id='message_id'><body>Hello there !</body><request xmlns='urn:xmpp:receipts'/><ack received='true' read='true'/></message><delay xmlns='urn:xmpp:delay' from='domain.com' stamp='2016-06-30T12:00:00.000Z'/></forwarded></result></message>" error:&error];
    XMPPMessage *message = [XMPPMessage messageFromElement:element];
    [_storage archiveMessage:message outgoing:NO xmppStream:_stream];
    
    error = nil;
    element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc'><result xmlns='urn:xmpp:mam:1' id='mam_message_id_2' queryid='mam_query_id'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='friend@domain.com/rc' to='me@domain.com' type='chat' id='message_id_2'><body>How are you ?</body><request xmlns='urn:xmpp:receipts'/><ack received='true' read='true'/></message><delay xmlns='urn:xmpp:delay' from='domain.com' stamp='2016-06-30T12:01:00.000Z'/></forwarded></result></message>" error:&error];
    message = [XMPPMessage messageFromElement:element];
    [_storage archiveMessage:message outgoing:NO xmppStream:_stream];
    
    // Make sure we have 2 elements in cache
    NSArray *result = [_storage fetchMessagesForJID:[XMPPJID jidWithString:@"friend@domain.com"] xmmpStream:_stream maxSize:@(10) offset:@(0)];
    XCTAssertEqual([result count], 2);
    
    
    
    // Now make a MAM request to server Which return 1 new messages (with complete = true)
    Peer *peer = [Peer new];
    peer.jid = @"friend@domain.com";
    [_module fetchArchivedMessagesWithPeer:peer withUUID:@"getMam_me@domain.com_UUID-1111" maxSize:@(5) beforeDate:nil lastMessageID:nil];
    
    error = nil;
    element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc'><result xmlns='urn:xmpp:mam:1' id='mam_message_id_3' queryid='getMam_me@domain.com_UUID-1111'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='friend@domain.com/rc' to='me@domain.com' type='chat' id='message_id_3'><body>New message</body><request xmlns='urn:xmpp:receipts'/><ack received='true' read='true'/></message><delay xmlns='urn:xmpp:delay' from='domain.com' stamp='2016-06-30T13:00:00.000Z'/></forwarded></result></message>" error:&error];
    message = [XMPPMessage messageFromElement:element];
    [_module xmppStream:_stream didReceiveMessage:message];
    
    
    error = nil;
    element = [[NSXMLElement alloc] initWithXMLString:@"<iq xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc' id='getMam_me@domain.com_UUID-1111' type='result'><fin xmlns='urn:xmpp:mam:1' queryid='getMam_me@domain.com_UUID-1111' complete='true'><set xmlns='http://jabber.org/protocol/rsm'><first>mam_message_id_3</first><last>mam_message_id_3</last><count>1</count></set></fin></iq>" error:&error];
    XMPPIQ *iq = [XMPPIQ iqFromElement:element];
    [_module xmppStream:_stream didReceiveIQ:iq];
    
    
    // And now, we should have an empty cache.
    result = [_storage fetchMessagesForJID:[XMPPJID jidWithString:@"friend@domain.com"] xmmpStream:_stream maxSize:@(10) offset:@(0)];
    XCTAssertEqual([result count], 1);
    XMPPMessageArchiveManagement_Message_CoreDataObject *cachedMessage = result[0];
    XCTAssert([cachedMessage.mamMessageId isEqualToString:@"mam_message_id_3"]);
    XCTAssert([cachedMessage.messageId isEqualToString:@"message_id_3"]);
    XCTAssert([cachedMessage.body isEqualToString:@"New message"]);
    XCTAssert([cachedMessage.peerJid isEqualToString:@"friend@domain.com"]);
    XCTAssert([cachedMessage.viaJid isEqualToString:@"friend@domain.com"]);
    XCTAssertFalse(cachedMessage.isOutgoing);
}

-(void) testAddNewMsg {
    
    //first insert 2 messages in cache.
    NSError *error = nil;
    
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc'><result xmlns='urn:xmpp:mam:1' id='mam_message_id' queryid='mam_query_id'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='friend@domain.com/rc' to='me@domain.com' type='chat' id='message_id'><body>Hello there !</body><request xmlns='urn:xmpp:receipts'/><ack received='true' read='true'/></message><delay xmlns='urn:xmpp:delay' from='domain.com' stamp='2016-06-30T12:00:00.000Z'/></forwarded></result></message>" error:&error];
    XMPPMessage *message = [XMPPMessage messageFromElement:element];
    [_storage archiveMessage:message outgoing:NO xmppStream:_stream];
    
    error = nil;
    element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc'><result xmlns='urn:xmpp:mam:1' id='mam_message_id_2' queryid='mam_query_id'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='friend@domain.com/rc' to='me@domain.com' type='chat' id='message_id_2'><body>How are you ?</body><request xmlns='urn:xmpp:receipts'/><ack received='true' read='true'/></message><delay xmlns='urn:xmpp:delay' from='domain.com' stamp='2016-06-30T12:01:00.000Z'/></forwarded></result></message>" error:&error];
    message = [XMPPMessage messageFromElement:element];
    [_storage archiveMessage:message outgoing:NO xmppStream:_stream];
    
    // Make sure we have 2 elements in cache
    NSArray *result = [_storage fetchMessagesForJID:[XMPPJID jidWithString:@"friend@domain.com"] xmmpStream:_stream maxSize:@(10) offset:@(0)];
    XCTAssertEqual([result count], 2);
    
    
    // Now make a MAM request to server Which return 1 new messages (with complete = false)
    Peer *peer = [Peer new];
    peer.jid = @"friend@domain.com";
    [_module fetchArchivedMessagesWithPeer:peer withUUID:@"getMam_me@domain.com_UUID-1111" maxSize:@(5) beforeDate:nil lastMessageID:nil];
    
    error = nil;
    element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc'><result xmlns='urn:xmpp:mam:1' id='mam_message_id_3' queryid='getMam_me@domain.com_UUID-1111'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='friend@domain.com/rc' to='me@domain.com' type='chat' id='message_id_3'><body>New message</body><request xmlns='urn:xmpp:receipts'/><ack received='true' read='true'/></message><delay xmlns='urn:xmpp:delay' from='domain.com' stamp='2016-06-30T13:00:00.000Z'/></forwarded></result></message>" error:&error];
    message = [XMPPMessage messageFromElement:element];
    [_module xmppStream:_stream didReceiveMessage:message];
    
    
    error = nil;
    element = [[NSXMLElement alloc] initWithXMLString:@"<iq xmlns='jabber:client' from='me@domain.com' to='me@domain.com/rc' id='getMam_me@domain.com_UUID-1111' type='result'><fin xmlns='urn:xmpp:mam:1' queryid='getMam_me@domain.com_UUID-1111' complete='false'><set xmlns='http://jabber.org/protocol/rsm'><first>mam_message_id_3</first><last>mam_message_id_3</last><count>1</count></set></fin></iq>" error:&error];
    XMPPIQ *iq = [XMPPIQ iqFromElement:element];
    [_module xmppStream:_stream didReceiveIQ:iq];
    
    
    // And now, we should have an empty cache.
    result = [_storage fetchMessagesForJID:[XMPPJID jidWithString:@"friend@domain.com"] xmmpStream:_stream maxSize:@(10) offset:@(0)];
    XCTAssertEqual([result count], 3);
    
    NSLog(@"Result %@", result);
}


-(void) testInjectMessageInMam {
    
    [_storage cleanAllMessages];
    
    [self expectationForNotification:GET_NOTIF_NAME(@selector(xmppMessageArchiveManagement:didEndArchiveMessage:)) object:nil handler:^(NSNotification *notification) {
        XMPPJID *from = [XMPPJID jidWithString:@"b4fa66e056c646bba992b66ae0846c00@cmazard-all-in-one-dev-1.opentouch.cloud"];
        NSArray *result = [_storage fetchMessagesForJID:from xmmpStream:_stream maxSize:@(10) offset:@(0)];
        XCTAssertEqual([result count], 1);
        Message *message = [result firstObject];
        XCTAssert([message.body isEqualToString:@"16"]);
        XCTAssert([message.timestamp isEqualToDateAndTime:[XMPPDateTimeProfiles parseDateTime:@"2018-03-01T14:23:16.470631Z"]]);
        return YES;
    }];
    
    
    NSError *error = nil;
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' from='b4fa66e056c646bba992b66ae0846c00@cmazard-all-in-one-dev-1.opentouch.cloud' to='b4fa66e056c646bba992b66ae0846c00@cmazard-all-in-one-dev-1.opentouch.cloud/mobile_ios_FE2A6D3D-9F05-49B5-8200-3C1288053E1A' type='chat'><sent xmlns='urn:xmpp:carbons:2'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='b4fa66e056c646bba992b66ae0846c00@cmazard-all-in-one-dev-1.opentouch.cloud/web_win_1.36.6_JTv8K4nF' to='d220abfd3117400092d506ce448f16ed@cmazard-all-in-one-dev-1.opentouch.cloud' type='chat' id='web_cb8e1724-843c-4ad9-95f4-90d0154eb31c10' lang='fr'><archived xmlns='urn:xmpp:mam:tmp' by='cmazard-all-in-one-dev-1.opentouch.cloud' id='1519914196469246' stamp='2018-03-01T14:23:16.470631Z'/><stanza-id xmlns='urn:xmpp:sid:0' by='cmazard-all-in-one-dev-1.opentouch.cloud' id='1519914196469246'/><body lang='fr'>16</body><request xmlns='urn:xmpp:receipts'/><active xmlns='http://jabber.org/protocol/chatstates'/></message></forwarded></sent><delay xmlns='urn:xmpp:delay' from='cmazard-all-in-one-dev-1.opentouch.cloud' stamp='2018-03-01T14:23:16.472Z'>Resent</delay></message>" error:&error];
    XMPPMessage *message = [XMPPMessage messageFromElement:element];
    [_module xmppStream:_stream didReceiveMessage:message];
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testCallLogMessage {
    _stream.myJID = [XMPPJID jidWithString:@"03c2a6da4fca41be9dd425117c930059@openrainbow.com"];
    [self expectationForNotification:@"XCT-xmppMessageArchiveManagement:didEndFetchingCallLogsWithAnswer:-Delegate" object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSArray<CallLog*> *archive = [_storage fetchCallLogsForJid:_stream.myJID xmppStream:_stream];
        NSLog(@"CALLLOGS %@", archive);
        XCTAssert(archive.count == 21);
        for (CallLog *aCallLog in archive) {
            XCTAssert(aCallLog.peer.displayName);
            NSLog(@"ACallLog %@", aCallLog);
        }
        return YES;
    }];
    XMPPIQ *iqStart = [XMPPIQ iqFromElement:[[NSXMLElement alloc] initWithXMLString:@"<iq xmlns='jabber:client' type='set' to='03c2a6da4fca41be9dd425117c930059@openrainbow.com' id='8453DEBC-8B4C-4F46-950D-9B7725EF8EE9'><query xmlns='jabber:iq:telephony:call_log' queryid='0249014C-C13B-421C-BBFD-A0345C516872'><set xmlns='http://jabber.org/protocol/rsm'><max>75</max><before/></set></query></iq>" error:nil]];
    dispatch_async(_moduleQueue, ^{
        [_module.xmppIDTracker addElement:iqStart target:_module selector:@selector(handleFetchCallLogQueryIQ:withInfo:) timeout:60];
    });
    NSString* fileRoot = [[NSBundle bundleForClass:[self class]] pathForResource:@"callLogs" ofType:@"xml"];
    NSString* fileContents = [NSString stringWithContentsOfFile:fileRoot encoding:NSUTF8StringEncoding error:nil];
    NSArray* allLinedStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    for (NSString *line in allLinedStrings) {
        NSError *error = nil;
        NSXMLElement *messageElement = [[NSXMLElement alloc] initWithXMLString:line error:&error];
        XMPPMessage *xmppMessage = [XMPPMessage messageFromElement:messageElement];
        [_module xmppStream:_stream didReceiveMessage:xmppMessage];
    }
    
    NSError *error = nil;
    NSXMLElement *iqElement = [[NSXMLElement alloc] initWithXMLString:@"<iq xmlns='jabber:client' from='03c2a6da4fca41be9dd425117c930059@openrainbow.com' to='03c2a6da4fca41be9dd425117c930059@openrainbow.com/mobile_ios_302082E7-D593-493A-BE2E-9A1F1AF7FE6B' id='8453DEBC-8B4C-4F46-950D-9B7725EF8EE9' type='result'><query xmlns='jabber:iq:telephony:call_log' queryid='8453DEBC-8B4C-4F46-950D-9B7725EF8EE9'><set xmlns='http://jabber.org/protocol/rsm'><first>1519318247741639</first><last>1520006616821385</last><count>2073</count></set></query></iq>" error:&error];
    XMPPIQ *iq = [XMPPIQ iqFromElement:iqElement];
    dispatch_async(_moduleQueue, ^{
        [_module xmppStream:_stream didReceiveIQ:iq];
    });
    
    
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testInjectMamPaytrend {
    _stream.myJID = [XMPPJID jidWithString:@"b1c9616fd19845c3b6bc8f0407cc03b0@openrainbow.com"];
    [self expectationForNotification:@"XCT-xmppMessageArchiveManagement:didEndFetchingArchiveWithAnswer:-Delegate" object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        XMPPJID *from = [XMPPJID jidWithString:@"room_9337840a7dff4279bc2637d7ad935ac3@muc.openrainbow.com"];
        NSArray *result = [_storage fetchMessagesForJID:from xmmpStream:_stream maxSize:@(10) offset:@(0)];
        XCTAssertEqual([result count], 10);
        for (Message *message in result) {
            if([message.messageID isEqualToString:@"C8760407-15EE-4695-B9A8-129AFAC2F64B"]){
                XCTAssertTrue(message.isOutgoing);
            }
        }
        NSLog(@"RESULT %@", result);
        return YES;
    }];
    XMPPIQ *iqStart = [XMPPIQ iqFromElement:[[NSXMLElement alloc] initWithXMLString:@"<iq xmlns='jabber:client' type='set' to='room_9337840a7dff4279bc2637d7ad935ac3@muc.openrainbow.com' id='64EDED25-7985-44FC-8D22-766342AA9B01'><query xmlns='urn:xmpp:mam:1:bulk' queryid='64EDED25-7985-44FC-8D22-766342AA9B01'><x xmlns='jabber:x:data' type='submit'><field var='FORM_TYPE' type='hidden'><value>urn:xmpp:mam:1</value></field><field var='with'><value>b1c9616fd19845c3b6bc8f0407cc03b0@openrainbow.com</value></field></x><set xmlns='http://jabber.org/protocol/rsm'><max>20</max><before/></set></query></iq>" error:nil]];
    dispatch_async(_moduleQueue, ^{
        [_module.xmppIDTracker addElement:iqStart target:_module selector:@selector(handleFetchMamQueryIQ:withInfo:) timeout:60];
    });
    NSString* fileRoot = [[NSBundle bundleForClass:[self class]] pathForResource:@"mam_paytren" ofType:@"xml"];
    NSString* fileContents = [NSString stringWithContentsOfFile:fileRoot encoding:NSUTF8StringEncoding error:nil];
    NSArray* allLinedStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    for (NSString *line in allLinedStrings) {
        NSError *error = nil;
        NSXMLElement *messageElement = [[NSXMLElement alloc] initWithXMLString:line error:&error];
        XMPPMessage *xmppMessage = [XMPPMessage messageFromElement:messageElement];
        [_module xmppStream:_stream didReceiveMessage:xmppMessage];
    }
    
    NSError *error = nil;
    NSXMLElement *iqElement = [[NSXMLElement alloc] initWithXMLString:@"<iq xmlns='jabber:client' from='room_9337840a7dff4279bc2637d7ad935ac3@muc.openrainbow.com' to='b1c9616fd19845c3b6bc8f0407cc03b0@openrainbow.com/mobile_ios_D19DE786-5368-4583-9715-38A51DD8F033' id='64EDED25-7985-44FC-8D22-766342AA9B01' type='result'><fin xmlns='urn:xmpp:mam:1' queryid='64EDED25-7985-44FC-8D22-766342AA9B01' complete='true'><set xmlns='http://jabber.org/protocol/rsm'><first>1516183096021015</first><last>1517836036437908</last><count>18</count></set></fin></iq>" error:&error];
    XMPPIQ *iq = [XMPPIQ iqFromElement:iqElement];
    dispatch_async(_moduleQueue, ^{
        [_module xmppStream:_stream didReceiveIQ:iq];
    });
    
    
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testInjectCallLog {
    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:@"<message xmlns='jabber:client' to='a91bdb6dd6464c83a9a664ed2a551c4e@openrainbow.net/mobile_ios_C3BAF21F-3CFA-42B2-BF8C-315ABF16EC3E' from='openrainbow.net' type='chat' id='18074395342695112705'><updated_call_log xmlns='jabber:iq:notification:telephony:call_log' id='1520932357041089'><forwarded xmlns='urn:xmpp:forward:0'><call_log xmlns='jabber:iq:notification:telephony:call_log' type='phone'><caller_info number='2708' jid='7327c329eeca4d05ac047568e882c4bd@openrainbow.net'><identity firstName='Kroes' lastName='Doutzen'/></caller_info><duration>4899</duration><ack read='false'/><callee>a91bdb6dd6464c83a9a664ed2a551c4e@openrainbow.net</callee><state>answered</state><call_id>1621EA234BD#37#827</call_id><caller>7327c329eeca4d05ac047568e882c4bd@openrainbow.net</caller><media>audio</media></call_log><delay xmlns='urn:xmpp:delay' stamp='2018-03-13T09:12:37.041089Z'/></forwarded></updated_call_log></message>" error:nil];
    XMPPMessage *message = [XMPPMessage messageFromElement:element];
    [_service xmppStream:nil didReceiveMessage:message];
}

#pragma mark - MAM delegates

- (void)xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didBeginFetchingArchiveForUUID:(NSString *) UUID {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:UUID];
}

- (void)xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndFetchingArchiveWithAnswer:(NSDictionary *) answer {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:answer];
}

- (void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndArchiveMessage:(XMPPMessage *)message {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:message];
}

- (void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndFetchingCallLogsWithAnswer:(NSDictionary *)answer {
    [[NSNotificationCenter defaultCenter] postNotificationName:GENERATE_NOTIF_NAME object:answer];
}

@end
