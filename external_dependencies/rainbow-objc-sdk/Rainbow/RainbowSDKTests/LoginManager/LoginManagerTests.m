/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowSDKTestAbstract.h"
#import "XMPPStub.h"

@interface LoginManagerTests : RainbowSDKTestAbstract
@end


/**
 *  This test class will check the behavior of the Login phases of the SDK.
 *  It will mainly test the failure cases because the success case is always used 
 *  in all the other tests.
 */
@implementation LoginManagerTests

- (void)setUp {
    [super setUp];
    [super doLogout:NO];
}

- (void)tearDown {
    [super tearDown];
}


/**
 *  The purpose of this test is to check the application behavior
 *  when the login fails at first step : REST login,
 *  because of a bad password
 */
- (void)testLoginRestAuthRefused {
    
    // Add our stubs
    // - for the REST Api request, respond with an error
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/login"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"login-failed-wrong-passwd.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:401 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // - we also expect a NSNotification when the login phase fails
    [self expectationForNotification:kLoginManagerDidFailedToAuthenticate object:nil handler:^(NSNotification *notification) {
        NSError *error = notification.object;
        XCTAssertNotNil(error);
        return YES;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to check the application behavior
 *  when the login fails because of a major internal server error
 */
- (void)testLoginRestAuthInternalServerError {
    
    // Add our stubs
    // - for the REST Api request, respond with an error
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/login"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        
        // We respond with "non-json" error, on a 503 Service Unavailable (can be answered by HAproxy)
        NSData* fixture = [@"503 Service Unavailable !" dataUsingEncoding:NSUTF8StringEncoding];
        return [OHHTTPStubsResponse responseWithData:fixture statusCode:503 headers:@{@"Content-Type":@"text/plain"}];
    }];
    
    // - we also expect a NSNotification when the login phase fails
    [self expectationForNotification:kLoginManagerDidFailedToAuthenticate object:nil handler:^(NSNotification *notification) {
        NSError *error = notification.object;
        XCTAssertNotNil(error);
        return YES;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to check the application behavior
 *  when the login fails because we have no internet connection
 */
- (void)testLoginRestAuthNotConnected {
    
    // Add our stubs
    // - for the REST Api request, respond with a "system error"
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/login"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        return [OHHTTPStubsResponse responseWithError:[NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorNotConnectedToInternet userInfo:nil]];
    }];
    
    // - we also expect a NSNotification when the login phase fails
    [self expectationForNotification:kLoginManagerDidFailedToAuthenticate object:nil handler:^(NSNotification *notification) {
        NSError *error = notification.object;
        XCTAssertNotNil(error);
        return YES;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to check the application behavior
 *  when the login fails because the request never reaches the server
 */
- (void)testLoginRestAuthRequestTimeout {
    
    // Add our stubs
    // - for the REST Api request, respond with correct data, but request take a long time to arrive to server
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/login"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"login.json", self.class);
        return [[OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}] requestTime:60 responseTime:1];
    }];
    
    // - we also expect a NSNotification when the login phase fails
    [self expectationForNotification:kLoginManagerDidFailedToAuthenticate object:nil handler:^(NSNotification *notification) {
        NSError *error = notification.object;
        XCTAssertNotNil(error);
        return YES;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:35.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to check the application behavior
 *  when the XMPP auth fails (which should never happen ?)
 */
- (void)testXMPPAuthRefused {
    
    // Add our custom stubs
    // - respond with a failure for XMPP auth
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"response"][@"xmlns"] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-sasl"]) {
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        return @[@"<failure xmlns='urn:ietf:params:xml:ns:xmpp-sasl'><temporary-auth-failure/></failure>"];
    }];
    
    // - we also expect a NSNotification when the login phase fails
    [self expectationForNotification:kLoginManagerDidFailedToAuthenticate object:nil handler:^(NSNotification *notification) {
        NSError *error = notification.object;
        XCTAssertNotNil(error);
        return YES;
    }];
    
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to check the application behavior
 *  when the XMPP websocket is closed by the server (receive a <close> on stream)
 */
- (void)testXMPPWSConnectionClosed {
    
    // - we expect a NSNotification when the login phase succeed
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^(NSNotification *notification) {
        return YES;
    }];
    
    // - we also expect a NSNotification when the connection is lost
    [self expectationForNotification:kLoginManagerDidLostConnection object:nil handler:^(NSNotification *notification) {
        return YES;
    }];
    
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait 2 seconds before simulate the closing of the stream.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [XMPPStub addIncomingMessage:@"<close xmlns='urn:ietf:params:xml:ns:xmpp-framing'/>"];
    });
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

@end
