/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import "ContactsManagerService+Internal.h"
#import "ContactsManagerServiceToolKit.h"
#import "ContactInternal.h"
#import "EmailAddressInternal.h"
#import "NSDictionary+ChangedKeys.h"
#import "Invitation+Internal.h"
#import <AddressBook/AddressBook.h>

#ifndef TARGET_IPHONE_SIMULATOR
#error "Simulator only !"
#endif

/////////// DUMMY MOCKS /////////////////

@interface DownloadManagerMock : NSObject
-(NSURLSessionUploadTask *) putRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler;
-(NSURLSessionUploadTask *) postRequestWithURL:(NSURL *) url NSDataBody:(NSData *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler;
-(BOOL) putSynchronousRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error;
-(BOOL) postSynchronousRequestWithURL:(NSURL *) url data:(NSData *) data requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error;
-(void) getRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler;

@end
@implementation DownloadManagerMock
-(NSURLSessionUploadTask *) putRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    if (completionHandler)
        completionHandler(nil, nil, nil);
    return nil;
}
-(NSURLSessionUploadTask *) postRequestWithURL:(NSURL *) url NSDataBody:(NSData *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    if (completionHandler)
        completionHandler(nil, nil, nil);
    return nil;
}
-(void) getRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    if (completionHandler)
        completionHandler(nil, nil, nil);
}
-(BOOL) putSynchronousRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error {
    return YES;
}
-(BOOL) postSynchronousRequestWithURL:(NSURL *) url data:(NSData *) data requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error{
    return YES;
}
@end

@interface MyUserMock : NSObject
@property NSObject *source;
@property NSObject *contact;
@property NSString *jid_im;
@end
@implementation MyUserMock
@end

@interface ApiServiceMock : NSObject
-(NSURL *) getURLForService:(ApiServices) service;
-(NSDictionary *) getHTTPHeaderParametersForService:(ApiServices) service;
@end
@implementation ApiServiceMock
-(NSURL *) getURLForService:(ApiServices) service {
    return nil;
}
-(NSDictionary *) getHTTPHeaderParametersForService:(ApiServices) service {
    return nil;
}
@end

@interface DirectoryServiceMock : NSObject
@property NSDictionary *response;
@property NSError *error;
-(void) searchRainbowContactsWithJids:(NSArray<NSString *> *) jids fromOffset:(NSUInteger) offset withLimit:(NSUInteger) limit withCompletionBlock:(SearchCompletionHandler) completionBlock;
-(void) searchRainbowContactsWithLoginEmails:(NSArray<NSString *> *) loginEmails fromOffset:(NSUInteger) offset withLimit:(NSUInteger) limit withCompletionBlock:(SearchCompletionHandler) block;
@end
@implementation DirectoryServiceMock
-(void) searchRainbowContactsWithJids:(NSArray<NSString *> *) jids fromOffset:(NSUInteger) offset withLimit:(NSUInteger) limit withCompletionBlock:(SearchCompletionHandler) completionBlock{
    if (completionBlock) {
        completionBlock(@"", _response[@"users"], _error);
    }
}
-(void) searchRainbowContactsWithLoginEmails:(NSArray<NSString *> *) loginEmails fromOffset:(NSUInteger) offset withLimit:(NSUInteger) limit withCompletionBlock:(SearchCompletionHandler) block {
    if (block) {
        block(@"", _response, _error);
    }
}

-(NSDictionary *) searchRainbowContactWithRainbowID:(NSString *) rainbowID {
    return _response;
}
@end

@interface XMPPUserMemoryStorageObjectMock : NSObject
@property XMPPJID *jid;
@property NSMutableArray<NSString*> *groups;
@property NSString *nickname;
@property BOOL isPendingApproval;
@property NSString *subscription;
@end
@implementation XMPPUserMemoryStorageObjectMock
@end
//////////////////////////////////


//////// Private iOS C-API ////////
// Do not remove this. This allows use to access the iOS private C-API to link/unlink local contacts.
// It is forbidden to access such private APIs in the apps, but not in the tests ;-)
bool ABPersonLinkPerson(ABRecordRef personA, ABRecordRef personB);
bool ABPersonUnlink(ABRecordRef person);
//////////////////////////////////


@interface ContactsManagerServiceUnitTests : XCTestCase
@property ContactsManagerService *cms;
@property DirectoryServiceMock *ds;
@property ABAddressBookRef addressBook;
@end

@implementation ContactsManagerServiceUnitTests

- (void)setUp {
    [super setUp];
    
    // Request access to local contacts. First run of tests can fail due to this.
    // Once this is allowed, it should be OK.
    CFErrorRef error = NULL;
    _addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    ABAddressBookRequestAccessWithCompletion(_addressBook, ^(bool granted, CFErrorRef error) {});
    
    // #if to prevent catastrophe.
#if TARGET_IPHONE_SIMULATOR
    // Clean all the local contacts
    NSArray *peopleArray = (__bridge NSArray *) _addressBook;
    for (id people in peopleArray) {
        ABAddressBookRemoveRecord(_addressBook, (__bridge ABRecordRef)(people), &error);
        ABAddressBookSave(_addressBook, &error);
    }
#endif
    
    DownloadManagerMock *dlm = [DownloadManagerMock new];
    MyUserMock *myum = [MyUserMock new];
    ApiServiceMock *apim = [ApiServiceMock new];
    _ds = [DirectoryServiceMock new];
    
    _cms = [[ContactsManagerService alloc] initWithDownloadManager:(DownloadManager *)dlm myUser:(MyUser *)myum apiUrlManagerService:(ApiUrlManagerService *)apim];
    _cms.directoryService = (DirectoryService*)_ds;
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidChangeUser object:nil];
    
    XCTAssertTrue(_cms.localDeviceAccessAlreadyRequired, @"You must allow access to addressbook to make those test works");
}

- (void)tearDown {
    [super tearDown];
    CFAutorelease(_addressBook);
    _cms.directoryService = nil;
    _ds = nil;
    _cms = nil;
}

- (void)testCreateAndUpdateNewRainbowContactFromJID {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // - Notification emitted on creation
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssertEqual([contact.locals count], 0);
        XCTAssertNotNil(contact.rainbow);
        XCTAssert([contact.jid isEqualToString:@"123@test.com"]);
        
        return YES;
    }];
    
    // Create one new contact.
    Contact *contact = [_cms createOrUpdateRainbowContactWithJid:@"123@test.com"];
    
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"123@test.com"]);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    // Re-create (=update) the same contact
    // But this function does not emit didUpdateContact,
    // because it did not update the contact as the only value is the jid.
    Contact *contact2 = [_cms createOrUpdateRainbowContactWithJid:@"123@test.com"];
    
    XCTAssertEqual(contact, contact2);
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"123@test.com"]);
    
    // still only 1 in contacts.
    XCTAssertEqual([_cms.contacts count], 1);
    
    NSLog(@"Contacts : %@", _cms.contacts);
}

- (void)testParallelCreationOfNewRainbowContactFromPresence {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // 100 parallels call to the function with the same jid.
    for (NSInteger i = 0; i < 100; i++) {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            NSString *jid = @"1@test.com";
            
            // Default is available
            XMPPPresence *presence = [XMPPPresence new];
            [presence addAttributeWithName:@"from" stringValue:jid];
            
            // Create one new contact.
            Contact *contact = [_cms createOrUpdateRainbowContactFromPresence:presence];
            
            
            XCTAssertEqual([contact.locals count], 0);
            XCTAssertNotNil(contact.rainbow);
            XCTAssert([contact.jid isEqualToString:jid]);
        });
    }
    
    [NSThread sleepForTimeInterval:1];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    NSLog(@"Contacts : %@", _cms.contacts);
}


- (void)NOtestMergeLocalWithRainbowFromPresence {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Add local contact
    CFErrorRef error = nil;
    
    ABRecordRef person1 = ABPersonCreate();
    ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &error);
    ABRecordSetValue(person1, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &error);
    
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(person1, kABPersonEmailProperty, multiEmail, &error);
    
    ABAddressBookAddRecord(_addressBook, person1, &error);
    ABAddressBookSave(_addressBook, &error);
    
    // - expect a didAddContact !
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssertEqual([contact.locals count], 1);
        XCTAssertNil(contact.rainbow);
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST"]);
        XCTAssert([contact.lastName isEqualToString:@"LOCALLAST"]);
        
        return YES;
    }];
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    // Now create rainbow from presence
    XMPPPresence *presence = [XMPPPresence new];
    [presence addAttributeWithName:@"from" stringValue:@"123@test.com"];
    
    Contact *contact = [_cms createOrUpdateRainbowContactFromPresence:presence];
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"123@test.com"]);
    
    XCTAssertEqual([_cms.contacts count], 2);
    
    
    // Now act like when roster received
    XMPPUserMemoryStorageObjectMock *user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"123@test.com"];
    user.nickname = @"MyNick";
    user.isPendingApproval = NO;
    user.groups = [NSMutableArray arrayWithObjects:@"GROUP 1", nil];
    
    // Create one new contact.
    contact = [_cms createOrUpdateRainbowContactFromXMPPUser:(XMPPUserMemoryStorageObject*)user];
    
    XCTAssertEqual([_cms.contacts count], 2);
    
    NSDictionary *searchedUserDict = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN",
                                       @"lastName": @"FAMILY",
                                       @"jid_im": @"123@test.com",
                                       @"emails": @[@{@"email":@"user@address.com", @"type":@"home"}],
                                       @"loginEmail": @"user@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict]};
    _ds.error = nil;
    
    [_cms populateVcardForContactsJids:@[contact.jid]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    NSLog(@"Contacts : %@", _cms.contacts);
}

- (void)testCreateNewRainbowContactFromXMPPUser {
    
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssertEqual([contact.locals count], 0);
        XCTAssertNotNil(contact.rainbow);
        XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
        XCTAssert([contact.nickName isEqualToString:@"MyNick"]);
        XCTAssertTrue(contact.isInRoster);
        XCTAssertFalse(contact.isPresenceSubscribed);
        XCTAssertEqual([contact.groups count], 1);
        XCTAssert([contact.groups[0] isEqualToString:@"GROUP 1"]);
        
        return YES;
    }];
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    
    XMPPUserMemoryStorageObjectMock *user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"234@test.com"];
    user.nickname = @"MyNick";
    user.isPendingApproval = NO;
    user.groups = [NSMutableArray arrayWithObjects:@"GROUP 1", nil];
    
    // Create one new contact.
    Contact *contact = [_cms createOrUpdateRainbowContactFromXMPPUser:(XMPPUserMemoryStorageObject*)user];
    
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
    XCTAssert([contact.nickName isEqualToString:@"MyNick"]);
    XCTAssertTrue(contact.isInRoster);
    XCTAssertFalse(contact.isPresenceSubscribed);
    XCTAssertEqual([contact.groups count], 1);
    XCTAssert([contact.groups[0] isEqualToString:@"GROUP 1"]);
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    NSLog(@"Contacts : %@", _cms.contacts);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

- (void)NOtestCreateNewRainbowContactWithPresenceSubscriptionFromXMPPUser {
    
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
        XCTAssertTrue(contact.isInRoster);
        XCTAssertTrue(contact.isPresenceSubscribed);
        
        return YES;
    }];
    
    XMPPUserMemoryStorageObjectMock *user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"234@test.com"];
    user.subscription = @"both";
    
    // Create one new contact.
    Contact *contact = [_cms createOrUpdateRainbowContactFromXMPPUser:(XMPPUserMemoryStorageObject*)user];
    
    XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
    XCTAssertTrue(contact.isInRoster);
    XCTAssertTrue(contact.isPresenceSubscribed);
    
    NSLog(@"Contacts : %@", _cms.contacts);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    
    // Now test when the subscription if just "to"
    // which can happend sometimes..
    
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssert([contact.jid isEqualToString:@"555@test.com"]);
        XCTAssertTrue(contact.isInRoster);
        XCTAssertTrue(contact.isPresenceSubscribed);
        
        return YES;
    }];
    
    
    user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"555@test.com"];
    user.subscription = @"to";
    
    // Create one new contact.
    contact = [_cms createOrUpdateRainbowContactFromXMPPUser:(XMPPUserMemoryStorageObject*)user];
    
    XCTAssert([contact.jid isEqualToString:@"555@test.com"]);
    XCTAssertTrue(contact.isInRoster);
    XCTAssertTrue(contact.isPresenceSubscribed);
    
    NSLog(@"Contacts : %@", _cms.contacts);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


- (void)testNewReceivedPendingInvitation {
    XCTestExpectation *test = [self expectationWithDescription:@"test"];
    // Should not be executed on main thread...
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSDictionary *invitationDict = @{
                                         @"id": @"invitID",
                                         @"invitedUserId": @"56e6bc34c219157cb207e813",
                                         @"invitedUserEmail": @"me@company.com",
                                         @"invitingUserId": @"newfriendRainbowID",
                                         @"invitingUserEmail": @"alice@company.com",
                                         @"invitingDate": @"2016-09-28T16:31:36.881Z",
                                         @"acceptationDate": @"2016-09-28T16:31:36.881Z",
                                         @"status": @"pending",
                                         @"type": @"visibility"
                                         };
        
        _ds.response = @{@"id": @"newfriendRainbowID",
                         @"companyId": @"compID",
                         @"firstName": @"GIVEN",
                         @"lastName": @"FAMILY",
                         @"nickName": @"NICK",
                         @"jid_im": @"234@test.com",
                         //@"emails": @[@{@"email":@"alice@company.com", @"type":@"work"}],
                         @"loginEmail": @"alice@company.com"};
        
        Invitation *invitation = [_cms createOrUpdateInvitation:InvitationDirectionReceived fromDictionary:invitationDict];
        
        XCTAssertEqual(invitation.direction, InvitationDirectionReceived);
        XCTAssert([invitation.invitationID isEqualToString:@"invitID"]);
        XCTAssert([invitation.email isEqualToString:@"alice@company.com"]);
        XCTAssert([invitation.peer.rainbowID isEqualToString:@"newfriendRainbowID"]);
        XCTAssertEqual(invitation.status, InvitationStatusPending);
        
        [test fulfill];
    });
    
    [self waitForExpectationsWithTimeout:3.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

- (void)NOtestNewSentPendingBecomesAcceptedInvitationOnInvisibleRainbowUserExistingInLocal {
    XCTestExpectation *test = [self expectationWithDescription:@"test"];
    // Should not be executed on main thread...
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        XCTAssertEqual([_cms.contacts count], 0);
        
        // Add new local contact
        CFErrorRef error = nil;
        ABRecordRef person1 = ABPersonCreate();
        ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &error);
        ABRecordSetValue(person1, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &error);
        ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(multiEmail, CFSTR("alice@company.com"), kABWorkLabel, NULL);
        ABRecordSetValue(person1, kABPersonEmailProperty, multiEmail, &error);
        ABAddressBookAddRecord(_addressBook, person1, &error);
        ABAddressBookSave(_addressBook, &error);
        
//        [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
        
        XCTAssertEqual([_cms.contacts count], 1);
        XCTAssertFalse([_cms.contacts firstObject].isRainbowUser);
        
        // Invisible user invited -> we don't have the invitedUserId !
        NSDictionary *invitationDict = @{
                                         @"id": @"invitID",
                                         // @"invitedUserId": @"",
                                         @"invitedUserEmail": @"alice@company.com",
                                         @"invitingUserId": @"56e6bc34c219157cb207e813",
                                         @"invitingUserEmail": @"me@company.com",
                                         @"invitingDate": @"2016-09-28T16:31:36.881Z",
                                         @"acceptationDate": @"2016-09-28T16:31:36.881Z",
                                         @"status": @"pending",
                                         @"type": @"visibility"
                                         };
        
        // This will create a rainbow contact with rainbowID : tmp_invited_invitID
        
        Invitation *invitation = [_cms createOrUpdateInvitation:InvitationDirectionSent fromDictionary:invitationDict];
        
        XCTAssertEqual(invitation.direction, InvitationDirectionSent);
        XCTAssert([invitation.invitationID isEqualToString:@"invitID"]);
        XCTAssert([invitation.email isEqualToString:@"alice@company.com"]);
        XCTAssert([invitation.peer.rainbowID isEqualToString:@"tmp_invited_invitID"]);
        XCTAssertEqual(invitation.status, InvitationStatusPending);
        
        
        XCTAssertEqual([_cms.contacts count], 1);
        XCTAssertTrue([_cms.contacts firstObject].isRainbowUser);
        XCTAssert([[_cms.contacts firstObject].rainbowID isEqualToString:@"tmp_invited_invitID"]);
        
        
        
        // Now the invitation becomes accepted -> We have the invitedUserId !
        
        invitationDict = @{
                           @"id": @"invitID",
                           @"invitedUserId": @"newRainbowFriendId",
                           @"invitedUserEmail": @"alice@company.com",
                           @"invitingUserId": @"56e6bc34c219157cb207e813",
                           @"invitingUserEmail": @"me@company.com",
                           @"invitingDate": @"2016-09-28T16:31:36.881Z",
                           @"acceptationDate": @"2016-09-28T16:31:36.881Z",
                           @"status": @"accepted",
                           @"type": @"visibility"
                           };
        
        _ds.response = @{@"id": @"newRainbowFriendId",
                         @"companyId": @"compID",
                         @"firstName": @"GIVEN",
                         @"lastName": @"FAMILY",
                         @"nickName": @"NICK",
                         @"jid_im": @"234@test.com",
                         @"emails": @[@{@"email":@"alice@company.com", @"type":@"work"}],
                         @"loginEmail": @"alice@company.com"};
        
        invitation = [_cms createOrUpdateInvitation:InvitationDirectionSent fromDictionary:invitationDict];
        
        XCTAssertEqual(invitation.direction, InvitationDirectionSent);
        XCTAssert([invitation.invitationID isEqualToString:@"invitID"]);
        XCTAssert([invitation.email isEqualToString:@"alice@company.com"]);
        XCTAssert([invitation.peer.rainbowID isEqualToString:@"newRainbowFriendId"]);
        XCTAssertEqual(invitation.status, InvitationStatusAccepted);
        
        XCTAssertEqual([_cms.contacts count], 1);
        XCTAssertTrue([_cms.contacts firstObject].isRainbowUser);
        XCTAssert([[_cms.contacts firstObject].rainbowID isEqualToString:@"newRainbowFriendId"]);
        
        
        [test fulfill];
    });
    
    [self waitForExpectationsWithTimeout:3.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

- (void)testCreateNewRainbowContactFromXMPPPresence {
    
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssertEqual([contact.locals count], 0);
        XCTAssertNotNil(contact.rainbow);
        XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
        
        return YES;
    }];
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    
    // Default is available
    XMPPPresence *presence = [XMPPPresence new];
    [presence addAttributeWithName:@"from" stringValue:@"234@test.com"];
    
    // Create one new contact.
    Contact *contact = [_cms createOrUpdateRainbowContactFromPresence:presence];
    
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    NSLog(@"Contacts : %@", _cms.contacts);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

- (void)testCreateContactAndUpdateItsPresenceContact {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // - Notification emitted on creation
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssertEqual([contact.locals count], 0);
        XCTAssertNotNil(contact.rainbow);
        XCTAssert([contact.jid isEqualToString:@"123@test.com"]);
        
        return YES;
    }];
    
    // Create one new contact.
    Contact *contact = [_cms createOrUpdateRainbowContactWithJid:@"123@test.com"];
    
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"123@test.com"]);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    // Update its presence
    // This will not trigger didUpdateContact
    // Because the function does only extract the contact jid.
    XMPPPresence *presence = [XMPPPresence new];
    [presence addAttributeWithName:@"from" stringValue:@"123@test.com"];
    
    Contact *contact2 = [_cms createOrUpdateRainbowContactFromPresence:presence];
    
    XCTAssertEqual(contact, contact2);
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"123@test.com"]);

    
    // still only 1 in contacts.
    XCTAssertEqual([_cms.contacts count], 1);
    
    NSLog(@"Contacts : %@", _cms.contacts);
}

- (void)NOtestUpdateRainbowContactWithvCard {
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        XCTAssertEqual([contact.locals count], 0);
        XCTAssertNotNil(contact.rainbow);
        XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
        
        XCTAssert([contact.firstName isEqualToString:@"GIVEN"]);
        XCTAssert([contact.lastName isEqualToString:@"FAMILY"]);
        XCTAssert([contact.nickName isEqualToString:@"NICK"]);
        
        return YES;
    }];
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    
    XMPPUserMemoryStorageObjectMock *user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"234@test.com"];
    user.nickname = @"MyNick";
    user.groups = [NSMutableArray arrayWithObjects:@"GROUP 1", nil];
    
    // Create one new contact.
    Contact *contact = [_cms createOrUpdateRainbowContactFromXMPPUser:(XMPPUserMemoryStorageObject*)user];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    NSDictionary *searchedUserDict = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN",
                                       @"lastName": @"FAMILY",
                                       @"nickName": @"NICK",
                                       @"jid_im": @"234@test.com",
                                       @"emails": @[@{@"email":@"user-perso@address.com", @"type":@"home"}],
                                       @"loginEmail": @"user-pro@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict]};
    _ds.error = nil;
    
    [_cms populateVcardForContactsJids:@[contact.jid]];
    
    // We should still have only one contact !
    XCTAssertEqual([_cms.contacts count], 1);
    
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
    XCTAssert([contact.firstName isEqualToString:@"GIVEN"]);
    XCTAssert([contact.lastName isEqualToString:@"FAMILY"]);
    XCTAssert([contact.nickName isEqualToString:@"NICK"]);
    
    NSLog(@"Contacts : %@", _cms.contacts);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

- (void)NOtestContactKVOUpdateRainbowContactWithvCard {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    
    XMPPUserMemoryStorageObjectMock *user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"234@test.com"];
    user.nickname = @"MyNick";
    user.groups = [NSMutableArray arrayWithObjects:@"GROUP 1", nil];
    
    // Create one new contact.
    Contact *contact = [_cms createOrUpdateRainbowContactFromXMPPUser:(XMPPUserMemoryStorageObject*)user];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    // KVO expectations.
    [self keyValueObservingExpectationForObject:contact keyPath:kContactFirstNameKey expectedValue:@"GIVEN"];
    [self keyValueObservingExpectationForObject:contact keyPath:kContactLastNameKey expectedValue:@"FAMILY"];
    
    NSDictionary *searchedUserDict = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN",
                                       @"lastName": @"FAMILY",
                                       @"jid_im": @"234@test.com",
                                       @"emails": @[@{@"email":@"user-perso@address.com", @"type":@"home"}],
                                       @"loginEmail": @"user-pro@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict]};
    _ds.error = nil;
    
    [_cms populateVcardForContactsJids:@[contact.jid]];
    
    // We should still have only one contact !
    XCTAssertEqual([_cms.contacts count], 1);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    NSLog(@"Contacts : %@", _cms.contacts);
}

- (void)NOtestContactKVOUpdateLocalContact {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Add local contact
    CFErrorRef error = nil;
    
    ABRecordRef person1 = ABPersonCreate();
    ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &error);
    ABRecordSetValue(person1, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &error);
    ABAddressBookAddRecord(_addressBook, person1, &error);
    ABAddressBookSave(_addressBook, &error);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    
    // Update local contact
    ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("LOCALFIRST2"), &error);
    ABRecordSetValue(person1, kABPersonLastNameProperty, CFSTR("LOCALLAST2"), &error);
    ABAddressBookAddRecord(_addressBook, person1, &error);
    ABAddressBookSave(_addressBook, &error);
    
    // KVO expectations.
    [self keyValueObservingExpectationForObject:_cms.contacts[0] keyPath:kContactFirstNameKey expectedValue:@"LOCALFIRST2"];
    [self keyValueObservingExpectationForObject:_cms.contacts[0] keyPath:kContactLastNameKey expectedValue:@"LOCALLAST2"];
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    
    // We should still have only one contact !
    XCTAssertEqual([_cms.contacts count], 1);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    NSLog(@"Contacts : %@", _cms.contacts);
}

- (void)NOtestUpdateRainbowContactWithXMPPUser {
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        XCTAssertEqual([contact.locals count], 0);
        XCTAssertNotNil(contact.rainbow);
        XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
        XCTAssert([contact.nickName isEqualToString:@"MyNick"]);
        XCTAssertTrue(contact.isInRoster);
        XCTAssertFalse(contact.isPresenceSubscribed);
        XCTAssertEqual([contact.groups count], 1);
        XCTAssert([contact.groups[0] isEqualToString:@"GROUP 1"]);
        
        return YES;
    }];
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    
    // Create a pending contact
    Contact *contact = [_cms createOrUpdateRainbowContactWithJid:@"234@test.com"];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    // Now update it with vCard info
    XMPPUserMemoryStorageObjectMock *user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"234@test.com"];
    user.nickname = @"MyNick";
    user.groups = [NSMutableArray arrayWithObjects:@"GROUP 1", nil];
    
    contact = [_cms createOrUpdateRainbowContactFromXMPPUser:(XMPPUserMemoryStorageObject*)user];
    
    // Still only one contact !
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
    XCTAssert([contact.nickName isEqualToString:@"MyNick"]);
    XCTAssertTrue(contact.isInRoster);
    XCTAssertFalse(contact.isPresenceSubscribed);
    XCTAssertEqual([contact.groups count], 1);
    XCTAssert([contact.groups[0] isEqualToString:@"GROUP 1"]);
    
    NSLog(@"Contacts : %@", _cms.contacts);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) assertFail {
    XCTFail(@"Should not happend");
}

- (void)NOtestAddUpdateRemoveLocalContact {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Add new local contact
    CFErrorRef error = nil;
    ABRecordRef person1 = ABPersonCreate();
    ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &error);
    ABRecordSetValue(person1, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &error);
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(person1, kABPersonEmailProperty, multiEmail, &error);
    ABAddressBookAddRecord(_addressBook, person1, &error);
    ABAddressBookSave(_addressBook, &error);
    
    // - expect a didAddContact !
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssertEqual([contact.locals count], 1);
        XCTAssertNil(contact.rainbow);
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST"]);
        XCTAssert([contact.lastName isEqualToString:@"LOCALLAST"]);
        return YES;
    }];
    
    // - expect a didAddLocalContact !
    [self expectationForNotification:kContactsManagerServiceDidAddLocalContact object:nil handler:^(NSNotification *notification) {
        LocalContact *contact = notification.object;
        
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST"]);
        XCTAssert([contact.lastName isEqualToString:@"LOCALLAST"]);
        return YES;
    }];
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    
    // Update, without any change.
    // We should not have the update local notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(assertFail) name:kContactsManagerServiceDidUpdateLocalContact object:nil];
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    // Let's update the local contact
    ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("LOCALFIRST2"), &error);
    // NO lastname !
    ABRecordRemoveValue(person1, kABPersonLastNameProperty, &error);
    multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user2@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(person1, kABPersonEmailProperty, multiEmail, &error);
    CFRelease(multiEmail);
    ABAddressBookAddRecord(_addressBook, person1, &error);
    ABAddressBookSave(_addressBook, &error);
    
    // - expect a didUpdateContact !
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        XCTAssertEqual([contact.locals count], 1);
        XCTAssertNil(contact.rainbow);
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST2"]);
        XCTAssertNil(contact.lastName);
        
        return YES;
    }];
    
    // - expect a didUpdateLocalContact !
    [self expectationForNotification:kContactsManagerServiceDidUpdateLocalContact object:nil handler:^(NSNotification *notification) {
        LocalContact *contact = notification.object;
        
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST2"]);
        XCTAssertNil(contact.lastName);
        
        return YES;
    }];
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    
    // now lets remove the local contact.
    // This is done by sync-ing the localcontact, without this contact in the list.
    // - expect a didRemoveContact !
    [self expectationForNotification:kContactsManagerServiceDidRemoveContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssertEqual([contact.locals count], 0);
        XCTAssertNil(contact.rainbow);
        XCTAssertNil(contact.firstName);
        XCTAssertNil(contact.lastName);
        
        return YES;
    }];
    
    // - expect a didRemoveLocalContact !
    [self expectationForNotification:kContactsManagerServiceDidRemoveLocalContact object:nil handler:^(NSNotification *notification) {
        LocalContact *contact = notification.object;
        
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST2"]);
        XCTAssertNil(contact.lastName);
        
        return YES;
    }];
    
//    [_cms syncLocalContactsWithNSArrayOfContact:@[]];
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

- (void)NOtestRemoveLocalContactInformation {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Add local contact
    ABRecordRef localRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &anError);
    ABRecordSetValue(localRecord, kABPersonJobTitleProperty, CFSTR("LOCALJOB"), &anError);
    ABRecordSetValue(localRecord, kABPersonOrganizationProperty, CFSTR("LOCALORG"), &anError);
    
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    
    ABMutableMultiValueRef multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiPhone, CFSTR("0123456789"), kABWorkLabel, NULL);
    ABMultiValueAddValueAndLabel(multiPhone, CFSTR("9876543210"), kABHomeLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonPhoneProperty, multiPhone, &anError);
    
    ABMutableMultiValueRef multiURL = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiURL, CFSTR("http://aaa.fr"), kABWorkLabel, NULL);
    ABMultiValueAddValueAndLabel(multiURL, CFSTR("http://bbb.fr"), kABHomeLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonURLProperty, multiURL, &anError);
    
    ABMultiValueRef addresses = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
    NSDictionary *values = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"1 rue Dupont", (NSString *)kABPersonAddressStreetKey,
                            @"67400", (NSString *)kABPersonAddressZIPKey,
                            @"Illkirch", (NSString *)kABPersonAddressCityKey,
                            nil];
    ABMultiValueAddValueAndLabel(addresses, (__bridge CFDictionaryRef)values, kABHomeLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonAddressProperty, addresses, &anError);
    
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
    
    // - expect a didAddContact !
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssertEqual([contact.locals count], 1);
        XCTAssertNil(contact.rainbow);
        
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST"]);
        XCTAssert([contact.lastName isEqualToString:@"LOCALLAST"]);
        XCTAssert([contact.jobTitle isEqualToString:@"LOCALJOB"]);
        XCTAssert([contact.companyName isEqualToString:@"LOCALORG"]);
        XCTAssertEqual([contact.emailAddresses count], 1);
        XCTAssertEqual([contact.phoneNumbers count], 2);
        XCTAssertEqual([contact.webSitesURL count], 2);
        XCTAssertEqual([contact.addresses count], 1);
        
        return YES;
    }];
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    
    // Let's update the local contact
    ABRecordRemoveValue(localRecord, kABPersonFirstNameProperty, &anError);
    ABRecordRemoveValue(localRecord, kABPersonLastNameProperty, &anError);
    ABRecordRemoveValue(localRecord, kABPersonJobTitleProperty, &anError);
    ABRecordRemoveValue(localRecord, kABPersonOrganizationProperty, &anError);
    ABRecordRemoveValue(localRecord, kABPersonPhoneProperty, &anError);
    ABRecordRemoveValue(localRecord, kABPersonURLProperty, &anError);
    ABRecordRemoveValue(localRecord, kABPersonAddressProperty, &anError);
    
    multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
    
    // - expect a didUpdateContact !
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        XCTAssertEqual([contact.locals count], 1);
        XCTAssertNil(contact.rainbow);
        
        XCTAssertNil(contact.firstName);
        XCTAssertNil(contact.lastName);
        XCTAssertNil(contact.title);
        XCTAssertNil(contact.companyName);
        XCTAssertEqual([contact.emailAddresses count], 1);
        XCTAssertEqual([contact.phoneNumbers count], 0);
        XCTAssertEqual([contact.webSitesURL count], 0);
        XCTAssertEqual([contact.addresses count], 0);
        
        return YES;
    }];
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

- (void)testAddLocalContactWithoutEmailsNorPhonenumber {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Add local contact without any email not phone number
    // We don't ignore them !
    // They might be not shown in UI, but keep them in the SDK.
    ABRecordRef localRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &anError);
    
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    XCTAssertEqual([_cms.contacts count], 1);
}

- (void)NOtestUpdateLocalContactRemoveEmailsAndNumbers {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Add local contact
    ABRecordRef localRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &anError);
    
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
    // - expect a didAddContact !
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssertEqual([contact.locals count], 1);
        XCTAssertNil(contact.rainbow);
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST"]);
        XCTAssert([contact.lastName isEqualToString:@"LOCALLAST"]);
        
        return YES;
    }];
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    
    // Let's update the local contact and remove its email/phones
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST2"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST2"), &anError);
    
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
    // - expect a didUpdateContact !
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        XCTAssertEqual([contact.locals count], 1);
        XCTAssertNil(contact.rainbow);
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST2"]);
        XCTAssert([contact.lastName isEqualToString:@"LOCALLAST2"]);
        
        return YES;
    }];
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // no more contacts !
    XCTAssertEqual([_cms.contacts count], 1);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

- (void)NOtestMergeNewRainbowContactWithExistingLocal {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Add a local contact
    ABRecordRef localRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &anError);
    
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];

    XCTAssertEqual([_cms.contacts count], 1);
    
    
    // Create a new Rainbow user (this will trigger a didAddContact Notif, not checked here)
    XMPPUserMemoryStorageObjectMock *user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"234@test.com"];
    user.nickname = @"MyNick";
    user.groups = [NSMutableArray arrayWithObjects:@"GROUP 1", nil];
    Contact *contact = [_cms createOrUpdateRainbowContactFromXMPPUser:user];
    
    // Here the contact has no local part !
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    
    XCTAssertEqual([_cms.contacts count], 2);
    
    NSDictionary *searchedUserDict = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN",
                                       @"lastName": @"FAMILY",
                                       @"nickName" : @"NICK",
                                       @"jid_im": @"234@test.com",
                                       @"emails": @[@{@"email":@"user@address.com", @"type":@"home"}],
                                       @"loginEmail": @"user@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict]};
    _ds.error = nil;
    
    // - We expect a didUpdate, after the merge of the 2 contacts
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        if ([contact.locals count] && contact.rainbow) {
            XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
            
            // Mixed of local and rainbow data :
            XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST"]);
            XCTAssert([contact.lastName isEqualToString:@"LOCALLAST"]);
            XCTAssert([contact.nickName isEqualToString:@"NICK"]);
            return YES;
        }

        return NO;
    }];
    
    // - We expect a didRemove of the old contact (with local part) after merge.
    [self expectationForNotification:kContactsManagerServiceDidRemoveContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        // and we have a remove of the contact with only the rainbow part.
        XCTAssertEqual([contact.locals count], 0);
        XCTAssertNil(contact.rainbow);
        
        XCTAssertNil(contact.firstName);
        XCTAssertNil(contact.lastName);
        XCTAssertNil(contact.nickName);
        
        return YES;
    }];
    
    // Update this contact with its vcard (email gets applied !)
    // Contacts should be merged.
    [_cms populateVcardForContactsJids:@[contact.jid]];
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    // We should have only one contact now !
    XCTAssertEqual([_cms.contacts count], 1);
    
    // Now the contact has a local part !
    XCTAssertEqual([contact.locals count], 1);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
    XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST"]);
    XCTAssert([contact.lastName isEqualToString:@"LOCALLAST"]);
    XCTAssert([contact.nickName isEqualToString:@"NICK"]);
    
    NSLog(@"Contacts : %@", _cms.contacts);
}


- (void)NOtestMergeNewLocalContactWithExistingRainbow {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    
    // Add new Rainbow user (this will trigger a didAddContact Notif, not checked here)
    XMPPUserMemoryStorageObjectMock *user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"234@test.com"];
    user.nickname = @"MyNick";
    user.groups = [NSMutableArray arrayWithObjects:@"GROUP 1", nil];
    Contact *contact = [_cms createOrUpdateRainbowContactFromXMPPUser:user];
    
    // Here the contact has no local part !
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    NSDictionary *searchedUserDict = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN",
                                       @"lastName": @"FAMILY",
                                       @"nickName" : @"NICK",
                                       @"jid_im": @"234@test.com",
                                       @"emails": @[@{@"email":@"user@address.com", @"type":@"home"}],
                                       @"loginEmail": @"user@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict]};
    _ds.error = nil;
    
    // - We expect a didUpdate
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        // The updated contact stil have no local part
        XCTAssertEqual([contact.locals count], 0);
        XCTAssertNotNil(contact.rainbow);
        XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
        
        // Mixed of local and rainbow data :
        XCTAssert([contact.firstName isEqualToString:@"GIVEN"]);
        XCTAssert([contact.lastName isEqualToString:@"FAMILY"]);
        XCTAssert([contact.nickName isEqualToString:@"NICK"]);
        
        return YES;
    }];
    
    // Update this contact with its vcard (email gets applied !)
    // Contacts should be merged.
    
    [_cms populateVcardForContactsJids:@[contact.jid]];
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    // We should have only one contact now !
    XCTAssertEqual([_cms.contacts count], 1);
    
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
    XCTAssert([contact.firstName isEqualToString:@"GIVEN"]);
    XCTAssert([contact.lastName isEqualToString:@"FAMILY"]);
    XCTAssert([contact.nickName isEqualToString:@"NICK"]);
    
    // Now add a new local contact which match with the rainbow email
    ABRecordRef localRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &anError);
    
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
    // - We expect a didUpdate
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        // The updated contact now have a local part !
        XCTAssertEqual([contact.locals count], 1);
        XCTAssertNotNil(contact.rainbow);
        XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
        
        // Mixed of local and rainbow data :
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST"]);
        XCTAssert([contact.lastName isEqualToString:@"LOCALLAST"]);
        XCTAssert([contact.nickName isEqualToString:@"NICK"]);
        
        return YES;
    }];
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    NSLog(@"Contacts : %@", _cms.contacts);
}


- (void)NOtestRemoveMergedLocalContact {
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Add a local contact
    ABRecordRef localRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &anError);
    
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    // Create a new Rainbow user (this will trigger a didAddContact Notif, not checked here)
    XMPPUserMemoryStorageObjectMock *user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"234@test.com"];
    user.nickname = @"MyNick";
    user.groups = [NSMutableArray arrayWithObjects:@"GROUP 1", nil];
    Contact *contact = [_cms createOrUpdateRainbowContactFromXMPPUser:user];
    
    // Here the contact has no local part !
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    
    XCTAssertEqual([_cms.contacts count], 2);
    
    NSDictionary *searchedUserDict = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN2",
                                       @"lastName": @"FAMILY2",
                                       @"nickName" : @"NICK",
                                       @"jid_im": @"234@test.com",
                                       @"emails": @[@{@"email":@"user@address.com", @"type":@"home"}],
                                       @"loginEmail": @"user@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict]};
    _ds.error = nil;
    
    // - We expect a didUpdate, after the merge of the 2 contacts
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        // The first update of the rainbow only
        if ([contact.locals count] == 0 && contact.rainbow) {
            XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
            
            // rainbow data updated.
            XCTAssert([contact.firstName isEqualToString:@"GIVEN2"]);
            XCTAssert([contact.lastName isEqualToString:@"FAMILY2"]);
            XCTAssert([contact.nickName isEqualToString:@"NICK"]);
            return YES;
        }
        return NO;
    }];
    
    // we expect another update for the merge.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        // The second update is for the merge
        if ([contact.locals count] && contact.rainbow) {
            XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
            
            // Mixed of local and rainbow data :
            XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST"]);
            XCTAssert([contact.lastName isEqualToString:@"LOCALLAST"]);
            XCTAssert([contact.nickName isEqualToString:@"NICK"]);
            return YES;
        }
        return NO;
    }];
    
    // - We expect a didRemove of the old contact (with local part) after merge.
    [self expectationForNotification:kContactsManagerServiceDidRemoveContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssertEqual([contact.locals count], 0);
        XCTAssertNil(contact.rainbow);
        XCTAssertNil(contact.jid);
        
        XCTAssertNil(contact.firstName);
        XCTAssertNil(contact.lastName);
        XCTAssertNil(contact.nickName);
        
        return YES;
    }];
    
    // Update this contact with its vcard (email gets applied !)
    // Contacts should be merged.
    [_cms populateVcardForContactsJids:@[contact.jid]];
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    // We should have only one contact now !
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    
    // now lets remove the local contact.
    // This is done by sync-ing the localcontact, without this contact in the list.
    // - expect a didRemoveContact !
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        // No more local part of contact
        XCTAssertEqual([contact.locals count], 0);
        XCTAssertNotNil(contact.rainbow);
        XCTAssert([contact.firstName isEqualToString:@"GIVEN2"]);
        XCTAssert([contact.lastName isEqualToString:@"FAMILY2"]);
        
        return YES;
    }];
    
//    [_cms syncLocalContactsWithNSArrayOfContact:@[]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

- (void)NOtestUnMergedLocalContactFromRainbowContact {
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Add a local contact
    ABRecordRef localRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &anError);
    
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    // Create a new Rainbow user (this will trigger a didAddContact Notif, not checked here)
    XMPPUserMemoryStorageObjectMock *user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"234@test.com"];
    user.nickname = @"MyNick";
    user.groups = [NSMutableArray arrayWithObjects:@"GROUP 1", nil];
    Contact *contact = [_cms createOrUpdateRainbowContactFromXMPPUser:user];
    
    // Here the contact has no local part !
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    
    XCTAssertEqual([_cms.contacts count], 2);
    
    
    NSDictionary *searchedUserDict = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN",
                                       @"lastName": @"FAMILY",
                                       @"nickName" : @"NICK",
                                       @"jid_im": @"234@test.com",
                                       @"emails": @[@{@"email":@"user@address.com", @"type":@"home"}],
                                       @"loginEmail": @"user@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict]};
    _ds.error = nil;

    
    // Update this contact with its vcard (email gets applied !)
    // Contacts should be merged.
    [_cms populateVcardForContactsJids:@[contact.jid]];
    
    // We should have only one contact now !
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    
    // now lets update the local contact with another email address.
    // - expect a didUpdateContact for the merged contact (update of data)
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        if ([contact.locals count] && contact.rainbow) {
            XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST2"]);
            XCTAssert([contact.lastName isEqualToString:@"LOCALLAST2"]);
            return YES;
        }
        return NO;
    }];
    
    // - expect another didUpdateContact for after the unmerge. (the rainbow part remains)
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        if ([contact.locals count] == 0 && contact.rainbow) {
            XCTAssert([contact.firstName isEqualToString:@"GIVEN"]);
            XCTAssert([contact.lastName isEqualToString:@"FAMILY"]);
            XCTAssert([contact.nickName isEqualToString:@"NICK"]);
            return YES;
        }
        return NO;
    }];
    
    // - expect a didAddContact ! (for the newly separated local part)
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        // with just the local part.
        XCTAssertEqual([contact.locals count], 1);
        XCTAssertNil(contact.rainbow);
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST2"]);
        XCTAssert([contact.lastName isEqualToString:@"LOCALLAST2"]);
        
        return YES;
    }];
    
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST2"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST2"), &anError);
    
    multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("different-email@address.com"), kABWorkLabel, NULL); // <<<--- another email addr
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // Now we should have 2 separated contacts
    XCTAssertEqual([_cms.contacts count], 2);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

- (void)NOtestUnMergeRainbowContactFromExistingLocal {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Add a local contact (this will trigger a didAddContact Notif, not checked here)
    ABRecordRef localRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &anError);
    
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    // Create a new Rainbow user (this will trigger a didAddContact Notif, not checked here)
    XMPPUserMemoryStorageObjectMock *user = [XMPPUserMemoryStorageObjectMock new];
    user.jid = [XMPPJID jidWithString:@"234@test.com"];
    Contact *contact = [_cms createOrUpdateRainbowContactFromXMPPUser:(XMPPUserMemoryStorageObject*)user];
    
    // Here the contact has no local part !
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    
    XCTAssertEqual([_cms.contacts count], 2);
    
    
    NSDictionary *searchedUserDict = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN",
                                       @"lastName": @"FAMILY",
                                       @"nickName" : @"NICK",
                                       @"jid_im": @"234@test.com",
                                       @"emails": @[@{@"email":@"user@address.com", @"type":@"home"}],
                                       @"loginEmail": @"user@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict]};
    _ds.error = nil;
    
    [_cms populateVcardForContactsJids:@[contact.jid]];
    
    // We should have only one contact now !
    XCTAssertEqual([_cms.contacts count], 1);
    
    // Now the contact has a local part !
    XCTAssertEqual([contact.locals count], 1);
    XCTAssertNotNil(contact.rainbow);
    
    
    // - We expect a didUpdate, when the data is updated (on the still merged contact)
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        if ([contact.locals count] && contact.rainbow) {
            // Data still mixed but up to date
            XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST"]);
            XCTAssert([contact.lastName isEqualToString:@"LOCALLAST"]);
            return YES;
        }
        return NO;
    }];
    
    // Another update for the local part to be removed from the merged contact.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        if ([contact.locals count] == 0 && contact.rainbow) {
            // No more local data !
            XCTAssert([contact.firstName isEqualToString:@"GIVEN"]);
            XCTAssert([contact.lastName isEqualToString:@"FAMILY"]);
            XCTAssert([contact.nickName isEqualToString:@"NICK"]);
            return YES;
        }
        return NO;
    }];
    
    // - We expect a didAdd of the new separated local contact
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        XCTAssertEqual([contact.locals count], 1);
        XCTAssertNil(contact.rainbow);
        XCTAssertNil(contact.jid);
        
        XCTAssert([contact.firstName isEqualToString:@"LOCALFIRST"]);
        XCTAssert([contact.lastName isEqualToString:@"LOCALLAST"]);
        XCTAssertNil(contact.nickName);
        
        return YES;
    }];
    
    
    NSDictionary *searchedUserDict2 = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN",
                                       @"lastName": @"FAMILY",
                                       @"nickName" : @"NICK",
                                       @"jid_im": @"234@test.com",
                                       @"emails": @[@{@"email":@"another-email@address.com", @"type":@"home"}],
                                       @"loginEmail": @"another-email@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict2]};
    _ds.error = nil;
    
    [_cms populateVcardForContactsJids:@[contact.jid]];
    
    // And now we should have 2 contacts separated.
    XCTAssertEqual([_cms.contacts count], 2);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    // Now the contact have lost its a local part !
    XCTAssertEqual([contact.locals count], 0);
    XCTAssertNotNil(contact.rainbow);
    XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
    XCTAssert([contact.firstName isEqualToString:@"GIVEN"]);
    XCTAssert([contact.lastName isEqualToString:@"FAMILY"]);
    XCTAssert([contact.nickName isEqualToString:@"NICK"]);
    
    NSLog(@"Contacts : %@", _cms.contacts);
}


-(void) testUpdateMyvCard {
    
    // Create fake myContact
    MyUserMock *mum = [MyUserMock new];
    _cms.myUser = (MyUser*)mum;
    [_cms createMyRainbowContactForJid:@"me@server.com" withCompanyId:@"compid" withRainbowId:@"1" companyName:@"Default"];
    mum.contact = [_cms getContactWithJid:@"me@server.com"];
    
    
    // try to update it 1st time
    NSDictionary* fields = @{@"firstname": @"first",
                             @"lastname": @"last",
                             @"nickname": @"nick",
                             @"jobrole": @"job",
                             @"title": @"Dr",
                             @"personal_email": @"me@perso.com",
                             @"professional_landphone": @"1111",
                             @"professional_mobilephone": @"2222",
                             @"personal_landphone": @"3333",
                             @"personal_mobilephone": @"4444"};
    
    [_cms updateUserWithFields:fields withCompletionBlock:^(NSError* error){
        XCTAssertNil(error);
    }];
    
    XCTAssert([_cms.myContact.firstName isEqualToString:@"first"]);
    XCTAssert([_cms.myContact.lastName isEqualToString:@"last"]);
    XCTAssert([_cms.myContact.nickName isEqualToString:@"nick"]);
    XCTAssert([_cms.myContact.jobTitle isEqualToString:@"job"]);
    XCTAssert([_cms.myContact.title isEqualToString:@"Dr"]);
    XCTAssertEqual([_cms.myContact.emailAddresses count], 1);
    XCTAssert([_cms.myContact.emailAddresses[0].address isEqualToString:@"me@perso.com"]);
    XCTAssertEqual([_cms.myContact.phoneNumbers count], 4);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeWork withDeviceType:PhoneNumberDeviceTypeLandline].number isEqualToString:@"1111"]);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeWork withDeviceType:PhoneNumberDeviceTypeMobile].number isEqualToString:@"2222"]);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeHome withDeviceType:PhoneNumberDeviceTypeLandline].number isEqualToString:@"3333"]);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeHome withDeviceType:PhoneNumberDeviceTypeMobile].number isEqualToString:@"4444"]);
    
    
    // Second update (change values)
    
    fields = @{@"firstname": @"first2",
               @"lastname": @"last2",
               @"nickname": @"nick2",
               @"jobrole": @"job2",
               @"title": @"Dr2",
               @"personal_email": @"me@perso2.com",
               @"professional_landphone": @"11110",
               @"professional_mobilephone": @"22220",
               @"personal_landphone": @"33330",
               @"personal_mobilephone": @"44440"};
    
    [_cms updateUserWithFields:fields withCompletionBlock:^(NSError* error){
        XCTAssertNil(error);
    }];
    
    XCTAssert([_cms.myContact.firstName isEqualToString:@"first2"]);
    XCTAssert([_cms.myContact.lastName isEqualToString:@"last2"]);
    XCTAssert([_cms.myContact.nickName isEqualToString:@"nick2"]);
    XCTAssert([_cms.myContact.jobTitle isEqualToString:@"job2"]);
    XCTAssert([_cms.myContact.title isEqualToString:@"Dr2"]);
    XCTAssertEqual([_cms.myContact.emailAddresses count], 1);
    XCTAssert([_cms.myContact.emailAddresses[0].address isEqualToString:@"me@perso2.com"]);
    XCTAssertEqual([_cms.myContact.phoneNumbers count], 4);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeWork withDeviceType:PhoneNumberDeviceTypeLandline].number isEqualToString:@"11110"]);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeWork withDeviceType:PhoneNumberDeviceTypeMobile].number isEqualToString:@"22220"]);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeHome withDeviceType:PhoneNumberDeviceTypeLandline].number isEqualToString:@"33330"]);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeHome withDeviceType:PhoneNumberDeviceTypeMobile].number isEqualToString:@"44440"]);
    
    
    // Third update (do not touch)
    
    fields = @{};
    
    [_cms updateUserWithFields:fields withCompletionBlock:^(NSError* error){
        XCTAssertNil(error);
    }];
    
    XCTAssert([_cms.myContact.firstName isEqualToString:@"first2"]);
    XCTAssert([_cms.myContact.lastName isEqualToString:@"last2"]);
    XCTAssert([_cms.myContact.nickName isEqualToString:@"nick2"]);
    XCTAssert([_cms.myContact.jobTitle isEqualToString:@"job2"]);
    XCTAssert([_cms.myContact.title isEqualToString:@"Dr2"]);
    XCTAssertEqual([_cms.myContact.emailAddresses count], 1);
    XCTAssert([_cms.myContact.emailAddresses[0].address isEqualToString:@"me@perso2.com"]);
    XCTAssertEqual([_cms.myContact.phoneNumbers count], 4);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeWork withDeviceType:PhoneNumberDeviceTypeLandline].number isEqualToString:@"11110"]);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeWork withDeviceType:PhoneNumberDeviceTypeMobile].number isEqualToString:@"22220"]);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeHome withDeviceType:PhoneNumberDeviceTypeLandline].number isEqualToString:@"33330"]);
    XCTAssert([[_cms.myContact getPhoneNumberOfType:PhoneNumberTypeHome withDeviceType:PhoneNumberDeviceTypeMobile].number isEqualToString:@"44440"]);
    
    
    // Fourth update (set values to null)
    
    fields = @{@"firstname": @"",
               @"lastname": @"",
               @"nickname": @"",
               @"jobrole": @"",
               @"title": @"",
               @"personal_email": @"",
               @"professional_landphone": @"",
               @"professional_mobilephone": @"",
               @"personal_landphone": @"",
               @"personal_mobilephone": @""};
    
    [_cms updateUserWithFields:fields withCompletionBlock:^(NSError* error){
        XCTAssertNil(error);
    }];
    
    XCTAssert([_cms.myContact.firstName isEqualToString:@""]);
    XCTAssert([_cms.myContact.lastName isEqualToString:@""]);
    XCTAssert([_cms.myContact.nickName isEqualToString:@""]);
    XCTAssert([_cms.myContact.jobTitle isEqualToString:@""]);
    XCTAssert([_cms.myContact.title isEqualToString:@""]);
    XCTAssertEqual([_cms.myContact.emailAddresses count], 0);
    XCTAssertEqual([_cms.myContact.phoneNumbers count], 0);
    
}

- (void)NOtestMergeSearchedRainbowContactForLocalContactsWithExistingRainbowContact {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Create a new Rainbow user without any email.
    // This happens when creating a new contact from the GET conversation for example.
    NSDictionary *userDict = @{@"id": @"123",
                               @"companyId": @"compID",
                               @"firstName": @"GIVEN",
                               @"lastName": @"FAMILY",
                               @"jid_im": @"234@test.com"};
    
    Contact *contact = [_cms createOrUpdateRainbowContactFromJSON:userDict];
    
    XCTAssert([contact.jid isEqualToString:@"234@test.com"]);
    XCTAssertEqual([contact.emailAddresses count], 0);
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    // Prepare the DirectoryServiceMock (will be called by SDK after LocalContacts sync)
    // It will return 1 contact with SAME JID !! and 2 emails -> The pro and the perso emails.
    NSDictionary *searchedUserDict = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN",
                                       @"lastName": @"FAMILY",
                                       @"jid_im": @"234@test.com",
                                       @"emails": @[@{@"email":@"user-perso@address.com", @"type":@"home"}],
                                       @"loginEmail": @"user-pro@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict]};
    _ds.error = nil;
    
    // Now add a new local contact which does not match the current Rainbow one.
    // It is the "same person" but in our local it has just a personal email.
    ABRecordRef localRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &anError);
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user-perso@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // We expect 1 merged contact.
    // The local and the first rainbow does not merge.
    // But after that, we try to find a rainbow contact on the server for this local contact.
    // We get one, which happends to be the same as our current RainbowContact (same JID)
    XCTAssertEqual([_cms.contacts count], 1);
    
    // Mix of local and rainbow data
    XCTAssert([_cms.contacts[0].firstName isEqualToString:@"LOCALFIRST"]);
    XCTAssert([_cms.contacts[0].lastName isEqualToString:@"LOCALLAST"]);
    XCTAssert([_cms.contacts[0].jid isEqualToString:@"234@test.com"]);
    XCTAssertEqual([_cms.contacts[0].emailAddresses count], 2);
    
    NSLog(@"Contacts : %@", _cms.contacts);
}

#pragma mark - MyUser tests.

- (void)NOtestNOMergeNewLocalContactWithMyUser {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Create fake myContact
    MyUserMock *mum = [MyUserMock new];
    _cms.myUser = (MyUser*)mum;
    [_cms createMyRainbowContactForJid:@"me@server.com" withCompanyId:@"compid" withRainbowId:@"1" companyName:@"Defaut"];
    mum.contact = [_cms getContactWithJid:@"me@server.com"];
    mum.jid_im = @"me@server.com";
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    NSDictionary *searchedUserDict = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN",
                                       @"lastName": @"FAMILY",
                                       @"jid_im": @"234@test.com",
                                       @"emails": @[@{@"email":@"user@address.com", @"type":@"home"}],
                                       @"loginEmail": @"user-pro@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict]};
    _ds.error = nil;
    Contact *contact = (Contact *) mum.contact;
    [_cms populateVcardForContactsJids:@[contact.jid]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    // Now add a new local contact which match with the rainbow email
    ABRecordRef localRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &anError);
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // We expect 2 non-merged contacts.
    // Even if the email match, because, there is no merge
    // with MyUser.
    XCTAssertEqual([_cms.contacts count], 2);
    
    NSLog(@"Contacts : %@", _cms.contacts);
}

- (void)NOtestNOMergeLocalContactWithNewMyUser {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Now add a new local contact
    ABRecordRef localRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(localRecord, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &anError);
    ABRecordSetValue(localRecord, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &anError);
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
    ABRecordSetValue(localRecord, kABPersonEmailProperty, multiEmail, &anError);
    ABAddressBookAddRecord(_addressBook, localRecord, &anError);
    ABAddressBookSave(_addressBook, &anError);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    XCTAssertEqual([_cms.contacts count], 1);
    
    
    // Create fake myContact
    MyUserMock *mum = [MyUserMock new];
    _cms.myUser = (MyUser*)mum;
    [_cms createMyRainbowContactForJid:@"me@server.com" withCompanyId:@"compid" withRainbowId:@"1" companyName:@"Default"];
    mum.contact = [_cms getContactWithJid:@"me@server.com"];
    mum.jid_im = @"me@server.com";
    
    XCTAssertEqual([_cms.contacts count], 2);
    
    
    NSDictionary *searchedUserDict = @{@"id": @"123",
                                       @"companyId": @"compID",
                                       @"firstName": @"GIVEN",
                                       @"lastName": @"FAMILY",
                                       @"jid_im": @"234@test.com",
                                       @"emails": @[@{@"email":@"user@address.com", @"type":@"home"}],
                                       @"loginEmail": @"user-pro@address.com"};
    
    _ds.response = @{@"users": @[searchedUserDict]};
    _ds.error = nil;
    
    Contact *contact = (Contact *) mum.contact;
    [_cms populateVcardForContactsJids:@[contact.jid]];
    
    
    /*
     TODO: Replace this by vcard in REST
    // Update the rainbow user vCard with same email as local one.
    XMPPvCardTemp *vCard = [XMPPvCardTemp vCardTemp];
    vCard.givenName = @"GIVEN";
    vCard.familyName = @"FAMILY";
    
    // <EMAIL><WORK/><USERID>user@address.com</USERID></EMAIL>
    NSXMLElement *emailelement = [NSXMLElement elementWithName:@"EMAIL"];
    NSXMLElement *workelement = [NSXMLElement elementWithName:@"WORK"];
    [emailelement addChild:workelement];
    NSXMLElement *useridelement = [NSXMLElement elementWithName:@"USERID"];
    [useridelement setStringValue:@"user@address.com"];
    [emailelement addChild:useridelement];
    [vCard addChild:emailelement];
    
    Contact *contact = (Contact *) mum.contact;
    
    // Update this contact with its vcard (email gets applied !)
    // Contacts should be merged.
    [_cms updateRainbowContact:&contact withVCard:vCard];
     */
    
    
    // We expect 2 non-merged contacts.
    // Even if the email match, because, there is no merge
    // with MyUser.
    XCTAssertEqual([_cms.contacts count], 2);
    
    NSLog(@"Contacts : %@", _cms.contacts);
}

- (void)testDeleteMyContactOnLogout {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Create fake myContact
    Contact *firstMyContact = [_cms createMyRainbowContactForJid:@"me@server.com" withCompanyId:@"compid" withRainbowId:@"1" companyName:@"Default"];
    
    XCTAssertEqual(_cms.myContact, firstMyContact);
    XCTAssertEqual([_cms.contacts count], 1);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidLogoutSucceeded object:nil];
    
    XCTAssertEqual(_cms.myContact, nil);
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Create another myContact
    Contact *secMyContact = [_cms createMyRainbowContactForJid:@"me@server.com" withCompanyId:@"compid" withRainbowId:@"1" companyName:@"Default"];
    
    XCTAssertNotEqual(firstMyContact, secMyContact);
    XCTAssertEqual(_cms.myContact, secMyContact);
    XCTAssertEqual([_cms.contacts count], 1);
    
    NSLog(@"Contacts : %@", _cms.contacts);
}

#pragma mark - Linked contacts tests

-(void) NOtestCreate2LocalContactsLinked {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Create 2 linked contacts directly.
    CFErrorRef error = nil;
    
    ABRecordRef person1 = ABPersonCreate();
    ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("FIRST"), &error);
    ABRecordSetValue(person1, kABPersonLastNameProperty, CFSTR("LAST"), &error);
    ABAddressBookAddRecord(_addressBook, person1, &error);
    ABAddressBookSave(_addressBook, &error);
    
    ABRecordRef person2 = ABPersonCreate();
    ABRecordSetValue(person2, kABPersonFirstNameProperty, CFSTR("FIRST2"), &error);
    ABRecordSetValue(person2, kABPersonLastNameProperty, CFSTR("LAST2"), &error);
    ABAddressBookAddRecord(_addressBook, person2, &error);
    ABAddressBookSave(_addressBook, &error);
    
    // Links this 2 persons.
    ABPersonLinkPerson(person1, person2);
    ABAddressBookSave(_addressBook, &error);
    
    // We should have 4 Notif :
    // - didAdd for the first local contact
    // - didAdd for the second local contact
    // - didUpdate for the merge (with 2 local parts)
    // - didRemove for the un-used merged contact
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // We should have only 1 contact.
    XCTAssertEqual([_cms.contacts count], 1);
    
    // But with 2 local parts
    XCTAssertEqual([_cms.contacts[0].locals count], 2);
}

-(void) NOtestCreate2LocalContactsThenLinkThem {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Create 2 linked contacts directly.
    CFErrorRef error = nil;
    
    ABRecordRef person1 = ABPersonCreate();
    ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("FIRST"), &error);
    ABRecordSetValue(person1, kABPersonLastNameProperty, CFSTR("LAST"), &error);
    ABAddressBookAddRecord(_addressBook, person1, &error);
    ABAddressBookSave(_addressBook, &error);
    
    ABRecordRef person2 = ABPersonCreate();
    ABRecordSetValue(person2, kABPersonFirstNameProperty, CFSTR("FIRST2"), &error);
    ABRecordSetValue(person2, kABPersonLastNameProperty, CFSTR("LAST2"), &error);
    ABAddressBookAddRecord(_addressBook, person2, &error);
    ABAddressBookSave(_addressBook, &error);
    
    // We should have 2 Notif :
    // - didAdd for the first local contact
    // - didAdd for the second local contact
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // We should have 2 contacts.
    XCTAssertEqual([_cms.contacts count], 2);
    
    // Both with 1 part
    XCTAssertEqual([_cms.contacts[0].locals count], 1);
    XCTAssertEqual([_cms.contacts[1].locals count], 1);
    
    
    
    // Then Links this 2 persons.
    ABPersonLinkPerson(person1, person2);
    ABAddressBookSave(_addressBook, &error);
    
    // We should have 2 Notif :
    // - didUpdate for the merge (with 2 local parts)
    // - didRemove for the un-used merged contact
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // We should have only 1 contact.
    XCTAssertEqual([_cms.contacts count], 1);
    
    // But with 2 local parts
    XCTAssertEqual([_cms.contacts[0].locals count], 2);
}

-(void) NOtestUnlink2LocalContacts {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Create 2 linked contacts directly.
    CFErrorRef error = nil;
    
    ABRecordRef person1 = ABPersonCreate();
    ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("FIRST"), &error);
    ABRecordSetValue(person1, kABPersonLastNameProperty, CFSTR("LAST"), &error);
    ABAddressBookAddRecord(_addressBook, person1, &error);
    ABAddressBookSave(_addressBook, &error);
    
    ABRecordRef person2 = ABPersonCreate();
    ABRecordSetValue(person2, kABPersonFirstNameProperty, CFSTR("FIRST2"), &error);
    ABRecordSetValue(person2, kABPersonLastNameProperty, CFSTR("LAST2"), &error);
    ABAddressBookAddRecord(_addressBook, person2, &error);
    ABAddressBookSave(_addressBook, &error);
    
    // Links this 2 persons.
    ABPersonLinkPerson(person1, person2);
    ABAddressBookSave(_addressBook, &error);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // We should have only 1 contact.
    XCTAssertEqual([_cms.contacts count], 1);
    
    // But with 2 local parts
    XCTAssertEqual([_cms.contacts[0].locals count], 2);
    
    
    // Now unlink these persons
    NSLog(@"Unlink the contacts now.");
    ABPersonUnlink(person1);
    ABAddressBookSave(_addressBook, &error);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // We should have 2 contact again.
    XCTAssertEqual([_cms.contacts count], 2);
    
    // But with 1 local parts each
    XCTAssertEqual([_cms.contacts[0].locals count], 1);
    XCTAssertEqual([_cms.contacts[1].locals count], 1);
}

-(void) NOtestReLinkLocalWithMultipleLinkedToAnother {
    // We had a crash with this setup of linked contacts,
    // Due to a delete an element while iterating an array.
    // This is a non-reg test.
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Create 4 linked contacts directly.
    CFErrorRef error = nil;
    
    ABRecordRef person1 = ABPersonCreate();
    ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("FIRST"), &error);
    ABRecordSetValue(person1, kABPersonLastNameProperty, CFSTR("LAST"), &error);
    ABAddressBookAddRecord(_addressBook, person1, &error);
    ABAddressBookSave(_addressBook, &error);
    
    ABRecordRef person2 = ABPersonCreate();
    ABRecordSetValue(person2, kABPersonFirstNameProperty, CFSTR("FIRST2"), &error);
    ABRecordSetValue(person2, kABPersonLastNameProperty, CFSTR("LAST2"), &error);
    ABAddressBookAddRecord(_addressBook, person2, &error);
    ABAddressBookSave(_addressBook, &error);
    
    ABRecordRef person3 = ABPersonCreate();
    ABRecordSetValue(person3, kABPersonFirstNameProperty, CFSTR("FIRST3"), &error);
    ABRecordSetValue(person3, kABPersonLastNameProperty, CFSTR("LAST3"), &error);
    ABAddressBookAddRecord(_addressBook, person3, &error);
    ABAddressBookSave(_addressBook, &error);
    
    ABRecordRef person4 = ABPersonCreate();
    ABRecordSetValue(person4, kABPersonFirstNameProperty, CFSTR("FIRST4"), &error);
    ABRecordSetValue(person4, kABPersonLastNameProperty, CFSTR("LAST4"), &error);
    ABAddressBookAddRecord(_addressBook, person4, &error);
    ABAddressBookSave(_addressBook, &error);
    
    // Then Links persons.
    ABPersonLinkPerson(person1, person2);
    ABAddressBookSave(_addressBook, &error);
    
    ABPersonLinkPerson(person1, person3);
    ABAddressBookSave(_addressBook, &error);
    
    ABPersonLinkPerson(person4, person1);
    ABAddressBookSave(_addressBook, &error);
    
    // This will try to steal the person1 from the first created Contact when processing person4
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // We should have 1 contacts.
    XCTAssertEqual([_cms.contacts count], 1);
    
    // Both with 1 part
    XCTAssertEqual([_cms.contacts[0].locals count], 4);
    
    /*
    // Then Links this 2 persons.
    ABPersonLinkPerson(person2, person3);
    ABAddressBookSave(_addressBook, &error);
    
    // We should have 2 Notif :
    // - didUpdate for the merge (with 2 local parts)
    // - didRemove for the un-used merged contact
    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // We should have only 1 contact.
    XCTAssertEqual([_cms.contacts count], 1);
    
    // But with 2 local parts
    XCTAssertEqual([_cms.contacts[0].locals count], 2);*/
}

-(void) NOtestRemove2LinkedContactsWhile1Remains {
    
    XCTAssertEqual([_cms.contacts count], 0);
    
    // Create 2 linked contacts directly.
    CFErrorRef error = nil;
    
    ABRecordRef person1 = ABPersonCreate();
    ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("FIRST"), &error);
    ABRecordSetValue(person1, kABPersonLastNameProperty, CFSTR("LAST"), &error);
    ABAddressBookAddRecord(_addressBook, person1, &error);
    ABAddressBookSave(_addressBook, &error);
    
    ABRecordRef person2 = ABPersonCreate();
    ABRecordSetValue(person2, kABPersonFirstNameProperty, CFSTR("FIRST2"), &error);
    ABRecordSetValue(person2, kABPersonLastNameProperty, CFSTR("LAST2"), &error);
    ABAddressBookAddRecord(_addressBook, person2, &error);
    ABAddressBookSave(_addressBook, &error);
    
    // Links this 2 persons.
    ABPersonLinkPerson(person1, person2);
    ABAddressBookSave(_addressBook, &error);
    
    // Add a third contact which will survive.
    ABRecordRef person3 = ABPersonCreate();
    ABRecordSetValue(person3, kABPersonFirstNameProperty, CFSTR("FIRST3"), &error);
    ABRecordSetValue(person3, kABPersonLastNameProperty, CFSTR("LAST3"), &error);
    ABAddressBookAddRecord(_addressBook, person3, &error);
    ABAddressBookSave(_addressBook, &error);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // We should have only 2 contact.
    XCTAssertEqual([_cms.contacts count], 2);
    
    // remove 1 & 2
    ABAddressBookRemoveRecord(_addressBook, person1, &error);
    ABAddressBookRemoveRecord(_addressBook, person2, &error);
    ABAddressBookSave(_addressBook, &error);
    
//    [_cms syncLocalContactsWithNSArrayOfContact:[NSArray arrayWithArray:(__bridge NSArray *)_addressBook]];
    
    // We should have 1
    XCTAssertEqual([_cms.contacts count], 1);
}


#pragma mark - didUpdateContact Tests

-(void) failNotification:(NSNotification *) notification {
    XCTAssert(NO, @"This notification should NOT happen : %@", notification);
}


-(void) testDontTriggerDidAdd2Times {
    // Create a new contact with JID
    Contact *contact = [_cms createOrUpdateRainbowContactWithJid:@"toto@domain.com"];
    
    // This notification should NOT be triggered !
    // if triggered, then generate a failure
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(failNotification:) name:kContactsManagerServiceDidAddContact object:nil];
    
    Contact *contact2 = [_cms createOrUpdateRainbowContactWithJid:@"toto@domain.com"];
    
    XCTAssertEqual(contact, contact2);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void) testDidUpdateContactWithJSONDict {
    
    // Create a new contact with JID
    Contact *contact = [_cms createOrUpdateRainbowContactWithJid:@"toto@domain.com"];
    
    
    // Now add multiple information with JSON dict
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        XCTAssert([contact.jid isEqualToString:@"toto@domain.com"]);
        
        // it should contain these fields updated
        XCTAssert([changedKeys containsObject:@"companyName"]);
        XCTAssert([changedKeys containsObject:@"firstName"]);
        XCTAssert([changedKeys containsObject:@"lastName"]);
        
        // and ONLY these ones
        XCTAssertEqual([changedKeys count], 3);
        
        return YES;
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(failNotification:) name:kContactsManagerServiceDidAddContact object:nil];
    
    NSDictionary *dict = @{@"jid_im": @"toto@domain.com",
                           @"companyName": @"COMP",
                           @"firstName": @"FIRST",
                           @"lastName": @"LAST"};
    
    Contact *contact2 = [_cms createOrUpdateRainbowContactFromJSON:dict];
    
    XCTAssertEqual(contact, contact2);
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void) testDidUpdateContactNoTriggeredIfNoUpdate {
    
    // Create a new contact with JID
    Contact *contact = [_cms createOrUpdateRainbowContactWithJid:@"toto@domain.com"];
    
    NSDate *date = [NSDate date];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        XCTAssert([contact.jid isEqualToString:@"toto@domain.com"]);
        
        // it should contain these fields updated
        XCTAssert([changedKeys containsObject:@"lastActivityDate"]);
        
        // and ONLY these ones
        XCTAssertEqual([changedKeys count], 1);
        
        return YES;
    }];
    
    [_cms updateLastActivityForContact:contact withSinceDate:date];
    
    [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
    
    // Make sure we dont catch the previous notification
    [NSThread sleepForTimeInterval:0.2f];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(failNotification:) name:kContactsManagerServiceDidUpdateContact object:nil];
    
    // Reupdate with same date, no update should be triggered
    [_cms updateLastActivityForContact:contact withSinceDate:date];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// The function -(NSArray*) changedKeysIn:(NSDictionary*) of NSDictionary is highly used
// so test it too.

-(void) testChangedKeysInEmptyDict {
    NSDictionary *dict1 = @{};
    NSDictionary *dict2 = @{};
    
    NSArray<NSString *> *result = [dict1 changedKeysIn:dict2];
    
    XCTAssertEqual([result count], 0);
}

-(void) testChangedKeysInValueHasNotChanged {
    NSDictionary *dict1 = @{@"key 1": @"value 1"};
    NSDictionary *dict2 = @{@"key 1": @"value 1"};
    
    NSArray<NSString *> *result = [dict1 changedKeysIn:dict2];
    
    XCTAssertEqual([result count], 0);
}

-(void) testChangedKeysInValueHasChanged {
    NSDictionary *dict1 = @{@"key 1": @"value 1"};
    NSDictionary *dict2 = @{@"key 1": @"value 2"};
    
    NSArray<NSString *> *result = [dict1 changedKeysIn:dict2];
    
    XCTAssertEqual([result count], 1);
    XCTAssert([result containsObject:@"key 1"]);
}

-(void) testChangedKeysInNewKeyAppeared {
    NSDictionary *dict1 = @{};
    NSDictionary *dict2 = @{@"key 1": @"value 1"};
    
    NSArray<NSString *> *result = [dict1 changedKeysIn:dict2];
    
    XCTAssertEqual([result count], 1);
    XCTAssert([result containsObject:@"key 1"]);
}

-(void) testChangedKeysInKeyHasDisepeard {
    NSDictionary *dict1 = @{@"key 1": @"value 1"};
    NSDictionary *dict2 = @{};
    
    NSArray<NSString *> *result = [dict1 changedKeysIn:dict2];
    
    XCTAssertEqual([result count], 1);
    XCTAssert([result containsObject:@"key 1"]);
}

@end
