/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowSDKTestAbstract.h"
#import "XMPPStub.h"
#import "NSDictionary+JSONString.h"

@interface ContactsManagerServiceTests : RainbowSDKTestAbstract
@end

@implementation ContactsManagerServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _contactsManagerService.delegate = self;

    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    contact.presence = @"Online";
    
    [self.server addContact:contact];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    _contactsManagerService.delegate = nil;
    [super tearDown];
}

/**
 *  The purpose of this test is to check if the function getContactWithJid:
 *  return correctly the contact, once it has been added to the stack.
 */
- (void)testGetContactByJid {
    // Add our custom actions on events
    // - Once the user is ready, check if get contact return the correct contact info
    [self expectationForNotification:GET_NOTIF_NAME(@selector(contactsService:didUpdateContact:withChangedKeys:)) object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        NSLog(@"TEST DID UPDATE CONTACT %@ WITH KEYS %@", contact, changedKeys);
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"firstName"]) {
            Contact *found = [_contactsManagerService getContactWithJid:contact.jid];
            
            XCTAssertNotNil(found);
            XCTAssert([found.firstName isEqualToString:@"Marcel"]);
            XCTAssert([found.lastName isEqualToString:@"Dupont"]);
            // TODO : this fails ? why
            // XCTAssert([found.role isEqualToString:@"SysAdmin"]);
            
            return YES;
        }
    }];
    
    // - we also expect a NSNotification when the user is ready.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        NSLog(@"DID UPDATE CONTACT %@ %@", contact, changedKeys);
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"firstName"]) {
            // We don't send a message here,
            // but just expect this NSNotification to arrive.
            return YES;
        }
        return NO;
    }];
    
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to check if the function getContactWithJid:
 *  return correctly nil, for an unknown contact.
 */
- (void)testGetUnknownContactByJid {
    
    // Wait 2 seconds
    XCTestExpectation *waited = [self expectationWithDescription:@"Waited"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        Contact *found = [_contactsManagerService getContactWithJid:@"blablabla@openrainbow.net"];
        XCTAssertNil(found);
        [waited fulfill];
    });
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  The purpose of this test is to change my presence and check the outgoing
 *  XMPP message
 */
- (void)testSendPresenceMessage {
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^(NSNotification *notification) {
        
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        NSLog(@"DID UPDATE MY CONTACT %@ %@", contact, changedKeys);
        if([contact isEqual:_contactsManagerService.myContact] && [changedKeys containsObject:@"presence"]){
            if(contact.presence.presence == ContactPresenceDoNotDisturb)
                return YES;
        }
        
        return NO;
    }];
    
    // Add our custom actions on events :
    // - Once XMPP is connected, change my presence
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^(NSNotification *notification) {
        
        [_contactsManagerService changeMyPresence:[Presence presenceDoNotDisturb]];
        return YES;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to get the last-activity for a contact
 *  check the sent message, and check the KVO update on the contact after answer.
 */
- (void)testGetLastActivityForContact {
    __block BOOL contactAdded = NO;
    __block Contact *contact = nil;
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *aContact = (Contact *)notification.object;
        if ([aContact.jid isEqualToString:@"1234@openrainbow.com"]) {
            contactAdded = YES;
            contact = aContact;
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^(NSNotification *notification) {
        if(contactAdded)
            [_contactsManagerService getLastActivityForContact:contact];
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        NSLog(@"Did update contact %@ changed keys %@", contact, changedKeys);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"lastActivityDate"]){
            return YES;
        }
        return NO;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to update my own vCard infos
 *  Update my contact info in contactManagerService, and call
 *  -[XMPPService updateMyvCard] to see if the correct XMPP
 *  message is sent on the network.
 */
- (void)testUpdateMyvCard {
    
    __block BOOL vcardUpdated = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"1 - DID UPDATE MY CONTACT %@", contact);
        if(contact.vcardPopulated){
            NSDictionary *fields = @{@"firstname": @"AAA",
                                     @"lastname": @"BBB",
                                     @"jobrole": @"CCC",
                                     @"title": @"DDD",
                                     @"personal_email": @"EEE",
                                     @"photo": @"FFF"};
            
            NSLog(@"Update user vcard");
            // We must dispatch this action to avoid doing with in the thread used by notification (that cause a bad instruction error because this method will also send a notification)
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                vcardUpdated = YES;
                [_contactsManagerService updateUserWithFields:fields withCompletionBlock:^(NSError *error) {
                    if (error) {
                        XCTFail(@"vCard update failed : %@", [error localizedDescription]);
                    }
                }];
            });
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^(NSNotification *notification) {
        
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        NSLog(@"DID UPDATE MY CONTACT %@ %@", contact, changedKeys);
        if(vcardUpdated && [contact isEqual:_contactsManagerService.myContact] && contact.vcardPopulated && [changedKeys containsObject:@"firstName"] && [changedKeys containsObject:@"lastName"]){
            XCTAssert([contact.firstName isEqualToString:@"AAA"]);
            XCTAssert([contact.lastName isEqualToString:@"BBB"]);
            XCTAssert([contact.title isEqualToString:@"DDD"]);
            return YES;
        }
        
        return NO;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to make a contact search request
 *  on the server and to make sure the results are correctly used.
 */
- (void)testSearchRequestWithPattern {
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"he_rainbow_1234";
    contact.jid = @"he_1234@openrainbow.com";
    contact.firstname = @"Heymonet";
    contact.lastname = @"Jerome";
    contact.loginEmail = @"he_1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    contact.presence = @"Online";
    
    [self.server addContact:contact];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Search complete"];
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^(NSNotification *notification) {

        [_contactsManagerService searchRemoteContactsWithPattern:@"he" withCompletionHandler:^(NSString *searchPattern, NSArray<Contact *>* foundContacts) {
            XCTAssertTrue([searchPattern isEqualToString:@"he"]);
            XCTAssertEqual(foundContacts.count, 1);
            [expectation fulfill];
        }];
        
        return YES;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  The purpose of this test is to remove a contact
 *  from my network (roster) and check the behavior
 *  of the SDK
 */
- (void)NOtestRemoveContactFromRoster {
    
    // Add our custom stubs for this test
    // - we expect a roster update for un-subscription.
    XCTestExpectation *messageSent = [self expectationWithDescription:@"Un-subscribe message sent"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"iq"][@"type"] isEqualToString:@"set"] &&
            [request[@"iq"][@"query"][@"xmlns"] isEqualToString:@"jabber:iq:roster"] &&
            [request[@"iq"][@"query"][@"item"][@"jid"] isEqualToString:@"1234@openrainbow.com"]){
            
            NSDictionary *item = request[@"iq"][@"query"][@"item"];
            XCTAssert([item[@"subscription"] isEqualToString:@"remove"]);
            XCTAssert([item[@"jid"] isEqualToString:@"1234@openrainbow.com"]);
            
            [messageSent fulfill];
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='%@' type='set'><query xmlns='jabber:iq:roster'><item ask='subscribe' subscription='remove' jid='1234@openrainbow.com'/></query></iq>", request[@"iq"][@"to"], [self getFull], request[@"iq"][@"id"]]];;
    }];
    XCTestExpectation *messageSent_tel_jid = [self expectationWithDescription:@"Un-subscribe tel jid message sent"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"iq"][@"type"] isEqualToString:@"set"] &&
            [request[@"iq"][@"query"][@"xmlns"] isEqualToString:@"jabber:iq:roster"] &&
            [request[@"iq"][@"query"][@"item"][@"jid"] isEqualToString:@"tel_1234@openrainbow.com"]){
            
            NSDictionary *item = request[@"iq"][@"query"][@"item"];
            XCTAssert([item[@"subscription"] isEqualToString:@"remove"]);
            XCTAssert([item[@"jid"] isEqualToString:@"tel_1234@openrainbow.com"]);
            
            [messageSent_tel_jid fulfill];
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='%@' type='set'><query xmlns='jabber:iq:roster'><item ask='subscribe' subscription='remove' jid='tel_1234@openrainbow.com'/></query></iq>", request[@"iq"][@"to"], [self getFull], request[@"iq"][@"id"]]];;
    }];
    
    // Add our custom actions on events
    // - Once the user is ready, remove him from my network
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.vcardPopulated) {
            [_contactsManagerService removeContactFromMyNetwork:contact];
            return YES;
        }
        return NO;
    }];
    
    // - we also expect a didRemoveContact Notification
    [self expectationForNotification:kContactsManagerServiceDidRemoveContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
            return YES;
        }
        return NO;
    }];

    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

@end
