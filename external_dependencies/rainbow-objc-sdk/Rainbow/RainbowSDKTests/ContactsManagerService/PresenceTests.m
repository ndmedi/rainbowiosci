/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowSDKTestAbstract.h"
#import "XMPPStub.h"

@interface PresenceTests : RainbowSDKTestAbstract

@end

@implementation PresenceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - Presence update vcard or avatar
/**
 * For vcard
 <presence xmlns="jabber:client"><x xmlns="vcard-temp:x:update"><data/></x><actor xmlns="jabber:iq:configuration"/></presence>
 */
-(void) testPresenceUpdateVcard {
    __block BOOL didSendPresenceStanza = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_abcd' to='%@'><x xmlns='vcard-temp:x:update'><data/></x><actor xmlns='jabber:iq:configuration'/></presence>", [self getFull]]];
            didSendPresenceStanza = YES;
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && didSendPresenceStanza){
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}
/**
 * For Avatar
 <presence xmlns="jabber:client"><x xmlns="vcard-temp:x:update"><avatar/></x><actor xmlns="jabber:iq:configuration"/></presence>
 */
-(void) testPresenceUpdateAvatar {
    // Online presence
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"presence"][@"priority"][@"text"] isEqualToString:@"5"]){
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[];
    }];
    
    __block BOOL didSendPresenceStanza = NO;
    
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"avatar"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        if (didSendPresenceStanza)
            return [OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:@{}];
        return [OHHTTPStubsResponse responseWithData:[NSData data] statusCode:404 headers:@{}];
    }];
    
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            didSendPresenceStanza = YES;
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_abcd' to='%@'><x xmlns='vcard-temp:x:update'><avatar/></x><actor xmlns='jabber:iq:configuration'/></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && didSendPresenceStanza) {
            XCTAssert([changedKeys containsObject:@"photoData"]);
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK without loading stubs
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

#pragma mark - Presence algo
/**
 *  <presence xmlns='jabber:client' from='tel_c29a429ffa4446d48e81bbebbeac8bcf@openrainbow.net/phone' to='c29a429ffa4446d48e81bbebbeac8bcf@openrainbow.net/web_win_1.14.4_LmDxZARb' xmlns:ns2='jabber:client'> <show>chat</show> <status>EVT_CONNECTION_CLEARED</status> </presence>
 
 
 si tu recois un status
 EVT_SERVICE_INITIATED
 ou EVT_ESTABLISHED
 et bien tu est occupé
 sinon tu es libre (au sens téléphonique)

 */
-(void) testPresenceBusyPhoneTelephony {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='tel_1234@openrainbow.com/phone' to='%@' xmlns:ns2='jabber:client'> <show>chat</show> <status>EVT_ESTABLISHED</status> </presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence){
            XCTAssertTrue(contact.presence.presence == ContactPresenceBusy, @"Contact presence must be busy");
            XCTAssertTrue([contact.presence.status isEqualToString:@"phone"], @"Contact status must be phone");
            XCTAssertFalse(contact.isConnectedWithMobile, @"Contact is not connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceOnline {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence){
            XCTAssertTrue(contact.presence.presence == ContactPresenceAvailable, @"Contact presence must be available");
            XCTAssertFalse(contact.isConnectedWithMobile, @"Contact is not connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceOnlineTwoDesktop {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'></presence>", [self getFull]]];
            
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_abcd' to='%@'></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence){
            XCTAssertTrue(contact.presence.presence == ContactPresenceAvailable, @"Contact presence must be available");
            XCTAssertFalse(contact.isConnectedWithMobile, @"Contact is not connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  There is at least one online ressource so we must have a online presence and we are not isConnectedMobile set to false
 */
-(void) testPresenceOnlineOneDesktopOneMobile {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'></presence>", [self getFull]]];
            
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence){
            XCTAssertTrue(contact.presence.presence == ContactPresenceAvailable, @"Contact presence must be available");
            XCTAssertFalse(contact.isConnectedWithMobile, @"Contact is not connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceOnlineMobileOneMobile {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.isConnectedWithMobile){
            XCTAssertTrue(contact.presence.presence == ContactPresenceAvailable, @"Contact presence must be available");
            XCTAssertTrue(contact.isConnectedWithMobile, @"Contact is not connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceOnlineMobileOneMobileOneDesktopAway {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self getFull]]];
            
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'><show>away</show></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.isConnectedWithMobile){
            XCTAssertTrue(contact.presence.presence == ContactPresenceAvailable, @"Contact presence must be available");
            XCTAssertTrue(contact.isConnectedWithMobile, @"Contact is not connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testOneDeviceOnlinePhonePresenceBusy {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='tel_1234@openrainbow.com/phone' to='%@' xmlns:ns2='jabber:client'> <show>chat</show> <status>EVT_ESTABLISHED</status> </presence>", [self getFull]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.isConnectedWithMobile){
            XCTAssertTrue(contact.presence.presence == ContactPresenceBusy, @"Contact presence must be available");
            XCTAssertTrue([contact.presence.status isEqualToString:@"phone"], @"Contact status mut be phone");
            XCTAssertTrue(contact.isConnectedWithMobile, @"Contact is not connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceOnlineThisDeviceOtherRessourceUnavailable {
    __block BOOL didSendStanza = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='%@' to='%@' xmlns:ns2='jabber:client'></presence>", [self getFull], [self getFull]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='%@/mobile_iOS_1234' to='%@' type='unavailable'><delay xmlns='urn:xmpp:delay' from='openrainbow.com' stamp='2016-10-06T16:02:27.640Z'>Resent</delay></presence>",[self getBare], [self getFull]]];
            didSendStanza = YES;
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        NSLog(@"DID UPDATE MY CONTACT %@", contact);
        if(didSendStanza && [contact isEqual:_myUser.contact] && contact.presence){
            XCTAssertTrue(contact.presence.presence == ContactPresenceAvailable, @"Contact presence must be available");
            XCTAssertTrue(contact.isConnectedWithMobile, @"Contact is connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresentationMode {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self getFull]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>dnd</show><status>presentation</status></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceBusy){
            XCTAssertTrue(contact.presence.presence == ContactPresenceBusy, @"Contact presence must be busy");
            XCTAssertTrue([contact.presence.status isEqualToString:@"presentation"], @"Contact status must be presentation");
            XCTAssertTrue(contact.isConnectedWithMobile, @"Contact is connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testAudioWebRTCCall {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self getFull]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>dnd</show><status>audio</status></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceBusy){
            XCTAssertTrue(contact.presence.presence == ContactPresenceBusy, @"Contact presence must be busy");
            XCTAssertTrue([contact.presence.status isEqualToString:@"audio"], @"Contact status must be audio");
            XCTAssertTrue(contact.isConnectedWithMobile, @"Contact is connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testVideoWebRTCCall {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self getFull]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>dnd</show><status>video</status></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceBusy){
            XCTAssertTrue(contact.presence.presence == ContactPresenceBusy, @"Contact presence must be busy");
            XCTAssertTrue([contact.presence.status isEqualToString:@"video"], @"Contact status must be video");
            XCTAssertTrue(contact.isConnectedWithMobile, @"Contact is connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testSharingWebRTCCall {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self getFull]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>dnd</show><status>sharing</status></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceBusy){
            XCTAssertTrue(contact.presence.presence == ContactPresenceBusy, @"Contact presence must be busy");
            XCTAssertTrue([contact.presence.status isEqualToString:@"sharing"], @"Contact status must be sharing");
            XCTAssertTrue(contact.isConnectedWithMobile, @"Contact is connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testDesktopAwayAuto {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>away</show></presence>", [self getFull]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceAway){
            XCTAssertTrue(contact.presence.presence == ContactPresenceAway, @"Contact presence must be away");
            XCTAssertFalse(contact.isConnectedWithMobile, @"Contact is connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceDND {
    __block BOOL didSendPresenceStanza = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>dnd</show></presence>", [self getFull]]];
            didSendPresenceStanza = YES;
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceDoNotDisturb){
            XCTAssertTrue(contact.presence.presence == ContactPresenceDoNotDisturb, @"Contact presence must be in DND");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testDNDOneRessourceXAEarlier {
    __block BOOL didSendPresenceStanza = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'><delay xmlns='urn:xmpp:delay' from='cccc61ca12504f039cd142c5121b5a8e@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' stamp='2016-09-27T12:30:01.811Z'/><show>xa</show></presence>", [self getFull]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>dnd</show></presence>", [self getFull]]];
            didSendPresenceStanza = YES;
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceDoNotDisturb){
            XCTAssertTrue(contact.presence.presence == ContactPresenceDoNotDisturb, @"Contact presence must be in DND");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceXA {
    __block BOOL didSendPresenceStanza = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'></presence>", [self getFull]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>xa</show></presence>", [self getFull]]];
            didSendPresenceStanza = YES;
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceUnavailable){
            XCTAssertTrue(contact.presence.presence == ContactPresenceUnavailable, @"Contact presence must be in XA");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceXAOneRessourceDNDEarlier {
    __block BOOL didSendPresenceStanza = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'></presence>", [self getFull]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'><delay xmlns='urn:xmpp:delay' from='cccc61ca12504f039cd142c5121b5a8e@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' stamp='2016-09-27T12:30:01.811Z'/><show>dnd</show></presence>", [self getFull]]];

            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>xa</show></presence>", [self getFull]]];
                didSendPresenceStanza = YES;
            });
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceUnavailable){
            XCTAssertTrue(contact.presence.presence == ContactPresenceUnavailable, @"Contact presence must be in Unavailable");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testAway {
    __block BOOL didSendPresenceStanza = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'><show>away</show></presence>", [self getFull]]];
                didSendPresenceStanza = YES;
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceAway){
            XCTAssertTrue(contact.presence.presence == ContactPresenceAway, @"Contact presence must be in AWAY");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testMyPresenceDNDReceiveOnlineManual {
    __block BOOL didSendPresenceStanza = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='%@' to='%@'><show>dnd</show></presence>", [self getFull], [self getFull]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='%@' to='%@'><show></show><status>mode=auto</status></presence>", [self getFull], [self getFull]]];
            didSendPresenceStanza = YES;
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        NSLog(@"DID UPDATE CONTACT DND %@", contact);
        if(contact.presence && contact.presence.presence == ContactPresenceDoNotDisturb){
            XCTAssertTrue(contact.presence.presence == ContactPresenceDoNotDisturb, @"Contact presence must be in DND");
            return YES;
        }
        
        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        NSLog(@"DID UPDATE CONTACT ONLINE %@", contact);
        if(contact.presence && contact.presence.presence == ContactPresenceAvailable && didSendPresenceStanza){
            XCTAssertTrue(contact.presence.presence == ContactPresenceAvailable, @"Contact presence must be Available");
            XCTAssertTrue(contact.presence.status.length == 0, @"No status must be set");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testMyPresenceDNDReceiveAwayManual {
    __block BOOL didSendPresenceStanza = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='%@' to='%@'><show>dnd</show></presence>", [self getFull], [self getFull]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='%@' to='%@'><show>xa</show><status>away</status></presence>", [self getFull], [self getFull]]];
            didSendPresenceStanza = YES;
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        NSLog(@"DID UPDATE CONTACT DND %@", contact);
        if(contact.presence && contact.presence.presence == ContactPresenceDoNotDisturb){
            XCTAssertTrue(contact.presence.presence == ContactPresenceDoNotDisturb, @"Contact presence must be in DND");
            return YES;
        }
        
        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        NSLog(@"DID UPDATE CONTACT AWAY MANUAL %@", contact);
        if(contact.presence && contact.presence.presence == ContactPresenceAway && didSendPresenceStanza){
            XCTAssertTrue(contact.presence.presence == ContactPresenceAway, @"Contact presence must be Away");
             XCTAssertTrue(contact.presence.status.length == 0, @"No status must be set");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}
@end
