/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowSDKTestAbstract.h"

@interface InvitationsTests : RainbowSDKTestAbstract
@property CNContactStore *addressBook;
@end

@implementation InvitationsTests

- (void)setUp {
    [super setUp];
    // #if to prevent catastrophe.
#if TARGET_IPHONE_SIMULATOR
    // Request access to local contacts. First run of tests can fail due to this.
    // Once this is allowed, it should be OK.
    NSError *error = [[NSError alloc] init];
    _addressBook = [[CNContactStore alloc] init];
    [_addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[_addressBook.defaultContainerIdentifier]] error:&error];
    //ABAddressBookRequestAccessWithCompletion(_addressBook, ^(bool granted, CFErrorRef error) {});
    
    // Clean all the local contacts
//    NSArray *peopleArray = (__bridge NSArray *) [_addressBook ;
//    for (id people in peopleArray) {
//        ABAddressBookRemoveRecord(_addressBook, (__bridge ABRecordRef)(people), &error);
//        ABAddressBookSave(_addressBook, &error);
//    }
#endif
}

- (void)tearDown {
    _addressBook = nil;
    [super tearDown];
}

-(void) loadTwoReceivedAndTwoSentInvitations {
    [OHHTTPStubs removeStub:stubGetInvitationsEmpty];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && ([request.URL.path hasSuffix:@"/invitations/received"]) && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"get_2_received_invitations.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && ([request.URL.path hasSuffix:@"/invitations/sent"]) && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"get_2_sent_invitations.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}


// send : <iq type="set"><query xmlns="jabber:iq:roster"><item jid="j_4710913479@openrainbow.net" subscription="remove"/></query></iq>
// rcv : <iq xmlns="jabber:client" from="j_4652674331@openrainbow.net" to="j_4652674331@openrainbow.net/mobile_ios_DEBUG 1.03.180_AA950895-5F17-47BF-9457-DF295C47E6FD" id="push11942056138113818872" type="set"><query xmlns="jabber:iq:roster"><item subscription="remove" jid="j_4710913479@openrainbow.net"/></query></iq>

/**
 *  The purpose of this test is to receive a subscription request
 *  and to accept it.
 *  We are in the same company, so it should be auto-accepted.
 */
- (void)testReceiveSubscRequestAndAccept {
    
    // - As we are in auto-accept mode, the roster update should be emitted without confirmation.
    XCTestExpectation *rosterAdded = [self expectationWithDescription:@"New user added to roster"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"iq"][@"type"] isEqualToString:@"set"] &&
            [request[@"iq"][@"query"][@"xmlns"] isEqualToString:@"jabber:iq:roster"] &&
            [request[@"iq"][@"query"][@"item"][@"jid"] isEqualToString:@"new_friend@openrainbow.com"]) {
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        [rosterAdded fulfill];
        return @[];
    }];
    
    // - As we are in auto-accept mode, the client should subscribe to new_friend presence
    XCTestExpectation *presenceAck = [self expectationWithDescription:@"ACK Subscribe from new friend presence"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"presence"][@"type"] isEqualToString:@"subscribed"]) {
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        [presenceAck fulfill];
        return @[];
    }];
    
    // - As we are in auto-accept mode, the client should subscribe to new_friend presence
    XCTestExpectation *presenceSubscr = [self expectationWithDescription:@"Subscribe to new friend presence"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"presence"][@"type"] isEqualToString:@"subscribe"] &&
            [request[@"presence"][@"to"] isEqualToString:@"new_friend@openrainbow.com"]) {
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        [presenceSubscr fulfill];
        return @[];
    }];
    
    
    // - The new friend should be added.
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        if ([contact.jid isEqualToString:@"new_friend@openrainbow.com"]) {
            return YES;
        }
        return NO;
    }];
    
    // - The new friend should be updated too.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        
        if ([contact.jid isEqualToString:@"new_friend@openrainbow.com"]) {
            return YES;
        }
        return NO;
    }];
    
    
    // Wait 2 seconds before sending the subscr. request
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='new_friend@openrainbow.com' to='%@' type='subscribe'/>", [self getFull]]];
    });
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  The purpose of this test is invite a contact to Rainbow
 *  which should send a REST request to the server. Because the server
 *  is in charge of sending the email/sms/whatever..
 */
- (void)testInviteLocalContactToRainbowWithSuccess {
    
    // Add local contact
    NSError *error = [[NSError alloc] init];
    
//    ABRecordRef person1 = ABPersonCreate();
//    ABRecordSetValue(person1, kABPersonFirstNameProperty, CFSTR("LOCALFIRST"), &error);
//    ABRecordSetValue(person1, kABPersonLastNameProperty, CFSTR("LOCALLAST"), &error);
//
//    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
//    ABMultiValueAddValueAndLabel(multiEmail, CFSTR("user@address.com"), kABWorkLabel, NULL);
//    ABRecordSetValue(person1, kABPersonEmailProperty, multiEmail, &error);
//    ABAddressBookAddRecord(_addressBook, person1, &error);
//    ABAddressBookSave(_addressBook, &error);
    CNMutableContact *person1 = [[CNMutableContact alloc] init];
    person1.givenName = @"LOCALFIRST";
    person1.familyName = @"LOCALLAST";
    CNLabeledValue *email = [[CNLabeledValue alloc] initWithLabel:CNLabelWork value:@"user@address.com"];
    person1.emailAddresses = @[email];
    
    CNSaveRequest *request = [[CNSaveRequest alloc] init];
    [_addressBook executeSaveRequest:request error:&error];
    
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"";
    contact.firstname = @"New";
    contact.lastname = @"Friend";
    contact.loginEmail = @"newFriend@openrainbow.com";
    contact.inRoster = NO;
    contact.isPending = NO;
    
    [self.server addContact:contact];
    
    [self expectationForNotification:kContactsManagerServiceDidInviteContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        NSLog(@"Did invite contact %@", contact);
        if ([contact.jid isEqualToString:@"tmp_invited_Sent_invitation_123"]) {
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^BOOL(NSNotification * notification) {
        Contact *contact = (Contact *) notification.object;
        if([contact.firstName isEqualToString:@"LOCALFIRST"]){
            [_contactsManagerService inviteContact:contact];
            return YES;
        }
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  The purpose of this test is invite a contact to Rainbow
 *  which should send a REST request to the server. But in this case,
 *  the server will respond with an error.
 */
- (void)testInviteContactToRainbowWithFailure {
    
    // Add our custom stubs for this test
    // - a REST request should be done to the server
    XCTestExpectation *messageSent = [self expectationWithDescription:@"Invite message sent"];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/invitations"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        [messageSent fulfill];
        
        // In this case, we return an error !
        NSString* fixture = OHPathForFile(@"invite-error.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:400 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    XCTestExpectation *searchReq = [self expectationWithDescription:@"Search request"];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.query containsString:@"user=test"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        [searchReq fulfill];
        
        // return user json response.
        NSDictionary *user = @{@"jid_im": @"newFriend@openrainbow.com", @"id": @"99887766", @"loginEmail": @"alice@company.com"};
        NSDictionary *users = @{@"users": @[user]};
        
        NSData* fixture = [[users jsonStringWithPrettyPrint:NO] dataUsingEncoding:NSUTF8StringEncoding];
        return [OHHTTPStubsResponse responseWithData:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events
    // - We expect a notification after invitation is done
    [self expectationForNotification:kContactsManagerServiceDidFailedToInviteContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        if ([contact.jid isEqualToString:@"newFriend@openrainbow.com"]) {
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * notification) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [_contactsManagerService searchRemoteContactsWithPattern:@"test" withCompletionHandler:^(NSString *searchPattern, NSArray<Contact *> *foundContacts) {
                
                XCTAssertGreaterThanOrEqual([foundContacts count], 1);
                
                // Now we can invite the first user
                [_contactsManagerService inviteContact:[foundContacts firstObject]];
            }];
        });
        return YES;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testSubscribeAcceptedFromOtherDevice {
    NSString *newFriendJid = @"new_friend_not_in_same_company@openrainbow.com";
    
    XCTestExpectation *simulateSubscribeReceived = [self expectationWithDescription:@"Receive subscribe"];
    XCTestExpectation *simulateSubscribedReceived = [self expectationWithDescription:@"Receive subscribeD"];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * notification) {
        // Wait 5 seconds before sending the subscr. request
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='%@' to='%@' type='subscribe'/>",newFriendJid, [self getFull]]];
            
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='id1' type='set'><query xmlns='jabber:iq:roster' ver='%d'><item subscription='from' jid='%@'/></query></iq>", [self getBare], [self getFull], 123 ,newFriendJid]];
            
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='id1' type='set'><query xmlns='jabber:iq:roster' ver='%d'><item ask='subscribe' subscription='from' jid='%@'/></query></iq>", [self getBare], [self getFull], 2232 ,newFriendJid]];
            
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='id1' type='set'><query xmlns='jabber:iq:roster' ver='%d'><item ask='subscribe' subscription='none' jid='tel_%@'/></query></iq>", [self getBare], [self getFull], 2232 ,newFriendJid]];
            
            [simulateSubscribeReceived fulfill];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='id1' type='set'><query xmlns='jabber:iq:roster' ver='%d'><item subscription='both' jid='%@'/></query></iq>", [self getBare], [self getFull], 5432 ,newFriendJid]];
                
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='%@' to='%@' type='subscribed'/>",newFriendJid, [self getFull]]];
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='%@/web_win_1.15.1_jF6KdPy' to='%@'><priority xmlns='jabber:client'>5</priority></presence>",newFriendJid, [self getFull]]];
                [simulateSubscribedReceived fulfill];
            });
        });
        
        return YES;
    }];
    
    // - The new friend should be added.
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        if ([contact.jid isEqualToString:newFriendJid]) {
            return YES;
        }
        return NO;
    }];
    
    // - The new invitation should be added.
    [self expectationForNotification:kContactsManagerServiceDidAddInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.peer.jid isEqualToString:newFriendJid]) {
            XCTAssertEqual(invitation.direction, InvitationDirectionReceived);
            XCTAssertEqual(invitation.status, InvitationStatusPending);
            return YES;
        }
        return NO;
    }];
    
    // - The new friend should be updated too.
    // Will be triggered after fetching vCard
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if ([contact.jid isEqualToString:newFriendJid] && [changedKeys containsObject:@"vcardPopulated"]) {
            XCTAssertEqual(contact.requestedInvitation.direction, InvitationDirectionReceived);
            XCTAssertEqual(contact.requestedInvitation.status, InvitationStatusPending);
            
            NSLog(@"1st is fullfill");
            return YES;
        }
        return NO;
    }];
    
    // - The new friend should be updated again after the subscribed is received
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if ([contact.jid isEqualToString:newFriendJid] && [changedKeys containsObject:@"isPresenceSubscribed"]) {
            XCTAssertEqual(contact.presence.presence, ContactPresenceAvailable);
            XCTAssertEqual(contact.requestedInvitation.direction, InvitationDirectionReceived);
            XCTAssertEqual(contact.requestedInvitation.status, InvitationStatusAccepted);
            
            NSLog(@"2nd is fullfill");
            return YES;
        }
        return NO;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testAcceptReceivedRosterInvitation {
    NSString *newFriendJid = @"new_friend_not_in_same_company@openrainbow.com";
    
    XCTestExpectation *simulateSubscribeReceived = [self expectationWithDescription:@"Receive subscribe"];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * notification) {
        
        // we are waiting for answer from newFriend
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='id1' type='set'><query xmlns='jabber:iq:roster' ver='%d'><item ask='subscribe' subscription='none' jid='%@'/></query></iq>", [self getBare], [self getFull], 2232, newFriendJid]];
            [simulateSubscribeReceived fulfill];
        });
        
        return YES;
    }];
    
    XCTestExpectation *simulateAcceptReceived = [self expectationWithDescription:@"Receive accept"];
    
    [self expectationForNotification:kContactsManagerServiceDidAddInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        
        // Fake invitation is created, with jid as email
        // and sent_jid as id
        if ([invitation.email isEqualToString:newFriendJid]) {
            
            XCTAssertEqual(invitation.status, InvitationStatusPending);
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                // Add roster update with
                // subscription=both (or to)
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='id1' type='set'><query xmlns='jabber:iq:roster' ver='%d'><item subscription='to' jid='tel_%@'/></query></iq>", [self getBare], [self getFull], 2233 ,newFriendJid]];
                [simulateAcceptReceived fulfill];
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:newFriendJid]) {
            XCTAssertEqual(invitation.status, InvitationStatusAccepted);
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testTwoReceivedAndTwoSentInvitations {
    [self loadTwoReceivedAndTwoSentInvitations];
    
    __block int pendingCounter = 0;
    
    [self expectationForNotification:kContactsManagerServiceDidAddInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:@"alice@my-company.com"]) {
            XCTAssertEqual(invitation.direction, InvitationDirectionReceived);
            XCTAssertEqual(invitation.status, InvitationStatusPending);
            
            // This is the only *received pending* invitation.
            pendingCounter++;
            XCTAssertEqual(_contactsManagerService.totalNbOfPendingInvitations, pendingCounter);
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidAddInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:@"carol@other-company.com"]) {
            XCTAssertEqual(invitation.direction, InvitationDirectionReceived);
            XCTAssertEqual(invitation.status, InvitationStatusAccepted);
            
            XCTAssertEqual(_contactsManagerService.totalNbOfPendingInvitations, pendingCounter);
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidAddInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:@"bob@company.com"]) {
            XCTAssertEqual(invitation.direction, InvitationDirectionSent);
            XCTAssertEqual(invitation.status, InvitationStatusPending);
            
            XCTAssertEqual(_contactsManagerService.totalNbOfPendingInvitations, pendingCounter);
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidAddInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:@"daniel@other-company.com"]) {
            XCTAssertEqual(invitation.direction, InvitationDirectionSent);
            XCTAssertEqual(invitation.status, InvitationStatusAccepted);
            
            XCTAssertEqual(_contactsManagerService.totalNbOfPendingInvitations, pendingCounter);
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testAcceptReceivedInvitation {
    [self loadTwoReceivedAndTwoSentInvitations];
    
    [self expectationForNotification:kContactsManagerServiceDidAddInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:@"alice@my-company.com"]) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [_contactsManagerService acceptInvitation:invitation];
            });
            
            return YES;
        }
        return NO;
    }];
    
    XCTestExpectation *invitationAccept = [self expectationWithDescription:@"Invitation accepted"];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        // Alice invitation ID is 111
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"invitations/111/accept"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        [invitationAccept fulfill];
        
        NSDictionary *dict = @{@"data": @{
                                       @"id": @"111",
                                       @"invitedUserId": @"56e6bc34c219157cb207e813",
                                       @"invitedUserEmail": @"me@company.com",
                                       @"invitingUserId": @"friend1RainbowID",
                                       @"invitingUserEmail": @"alice@my-company.com",
                                       @"invitingDate": @"2016-09-28T16:31:36.881Z",
                                       @"acceptationDate": @"2016-09-28T16:31:36.881Z",
                                       @"status": @"accepted",
                                       @"type": @"visibility"
                                       } };
        
        return [OHHTTPStubsResponse responseWithData:[[dict jsonStringWithPrettyPrint:NO] dataUsingEncoding:NSUTF8StringEncoding] statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:@"alice@my-company.com"]) {
            XCTAssertEqual(invitation.status, InvitationStatusAccepted);
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateInvitationPendingNumber object:nil handler:^(NSNotification *notification) {
        // notification.object for this notif is nil.
        return YES;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testDeclineReceivedInvitation {
    [self loadTwoReceivedAndTwoSentInvitations];
    
    [self expectationForNotification:kContactsManagerServiceDidAddInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:@"alice@my-company.com"]) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [_contactsManagerService declineInvitation:invitation];
            });
            
            return YES;
        }
        return NO;
    }];
    
    XCTestExpectation *invitationDecline = [self expectationWithDescription:@"Invitation declined"];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        // Alice invitation ID is 111
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"invitations/111/decline"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        [invitationDecline fulfill];
        
        NSDictionary *dict = @{@"data": @{
                                       @"id": @"111",
                                       @"invitedUserId": @"56e6bc34c219157cb207e813",
                                       @"invitedUserEmail": @"me@company.com",
                                       @"invitingUserId": @"friend1RainbowID",
                                       @"invitingUserEmail": @"alice@my-company.com",
                                       @"invitingDate": @"2016-09-28T16:31:36.881Z",
                                       @"acceptationDate": @"2016-09-28T16:31:36.881Z",
                                       @"status": @"declined",
                                       @"type": @"visibility"
                                       } };
        
        return [OHHTTPStubsResponse responseWithData:[[dict jsonStringWithPrettyPrint:NO] dataUsingEncoding:NSUTF8StringEncoding] statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:@"alice@my-company.com"]) {
            XCTAssertEqual(invitation.status, InvitationStatusDeclined);
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateInvitationPendingNumber object:nil handler:^(NSNotification *notification) {
        // notification.object for this notif is nil.
        return YES;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testInvitationReceivedEvent {
    /*
     <message xmlns="jabber:client" from="pcloud10@jerome-all-in-one-dev-1.opentouch.cloud/22417636629952626711478624307115300" to="00934009cb83452f82a368a35d525298@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="627fa1b5-ef94-4ff1-9175-aa31c2021b2d_209"><userinvite xmlns="jabber:iq:configuration" id="5822e4242489e1056f691858" action="create" type="received" status="pending"/></message>
     */
    
    XCTestExpectation *simulateInvitationEventReceived = [self expectationWithDescription:@"Receive invitation event"];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * notification) {
        // Wait 5 seconds before sending the invitation request event
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud10@jerome-all-in-one-dev-1.opentouch.cloud/22417636629952626711478624307115300' to='%@' type='management' id='54545'><userinvite xmlns='jabber:iq:configuration' id='invitID' action='create' type='received' status='pending'/></message>", [self getFull]]];
            
            [simulateInvitationEventReceived fulfill];
        });
        
        return YES;
    }];
    
    // App will get details of the new invitation
    XCTestExpectation *invitationDetails = [self expectationWithDescription:@"Invitation details"];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        // new invitation ID is invitID
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"invitations/invitID"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        [invitationDetails fulfill];
        
        NSDictionary *dict = @{@"data": @{
                                       @"id": @"invitID",
                                       @"invitedUserId": @"56e6bc34c219157cb207e813",
                                       @"invitedUserEmail": @"me@company.com",
                                       @"invitingUserId": @"newfriendRainbowID",
                                       @"invitingUserEmail": @"alice@company.com",
                                       @"invitingDate": @"2016-09-28T16:31:36.881Z",
                                       @"acceptationDate": @"2016-09-28T16:31:36.881Z",
                                       @"status": @"pending",
                                       @"type": @"visibility"
                                       } };
        
        return [OHHTTPStubsResponse responseWithData:[[dict jsonStringWithPrettyPrint:NO] dataUsingEncoding:NSUTF8StringEncoding] statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    
    
    [self expectationForNotification:kContactsManagerServiceDidAddInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:@"alice@company.com"]) {
            XCTAssertEqual(invitation.direction, InvitationDirectionReceived);
            XCTAssertEqual(invitation.status, InvitationStatusPending);
            XCTAssert([invitation.peer.rainbowID isEqualToString:@"newfriendRainbowID"]);
            XCTAssertEqual(_contactsManagerService.totalNbOfPendingInvitations, 1);
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testInvitationSentOnAnotherDevice {
    /*
     <message type='management' id='122' from='jid_from@openrainbow.com' to='jid_to@openrainbow.com' xmlns='jabber:client'><userinvite action="create" id='57cd5922d341df5812bbcb72' type='sent' status='pending' xmlns='jabber:iq:configuration'/></message>
     */
    
    XCTestExpectation *simulateInvitationEventReceived = [self expectationWithDescription:@"Receive invitation event"];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * notification) {
        // Wait 5 seconds before sending the invitation sent from another device event
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud10@jerome-all-in-one-dev-1.opentouch.cloud/22417636629952626711478624307115300' to='%@' type='management' id='54545'><userinvite xmlns='jabber:iq:configuration' id='invitID' action='create' type='sent' status='pending'/></message>", [self getFull]]];
            
            [simulateInvitationEventReceived fulfill];
        });
        
        return YES;
    }];
    
    // App will get details of the new invitation
    XCTestExpectation *invitationDetails = [self expectationWithDescription:@"Invitation details"];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        // new invitation ID is invitID
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"invitations/invitID"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        [invitationDetails fulfill];
        
        NSDictionary *dict = @{@"data": @{
                                       @"id": @"invitID",
                                       @"invitedUserId": @"newfriendRainbowID",
                                       @"invitedUserEmail": @"alice@company.com",
                                       @"invitingUserId": @"56e6bc34c219157cb207e813",
                                       @"invitingUserEmail": @"me@company.com",
                                       @"invitingDate": @"2016-09-28T16:31:36.881Z",
                                       @"acceptationDate": @"2016-09-28T16:31:36.881Z",
                                       @"status": @"pending",
                                       @"type": @"visibility"
                                       } };
        
        return [OHHTTPStubsResponse responseWithData:[[dict jsonStringWithPrettyPrint:NO] dataUsingEncoding:NSUTF8StringEncoding] statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    
    
    [self expectationForNotification:kContactsManagerServiceDidAddInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:@"alice@company.com"]) {
            XCTAssertEqual(invitation.direction, InvitationDirectionSent);
            XCTAssertEqual(invitation.status, InvitationStatusPending);
            XCTAssert([invitation.peer.rainbowID isEqualToString:@"newfriendRainbowID"]);
            // Sent invitation are not 'pending'
            XCTAssertEqual(_contactsManagerService.totalNbOfPendingInvitations, 0);
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testInvitationAcceptedOnAnotherDevice {
    /*
     <message type='management' id='122' from='jid_from@openrainbow.com' to='jid_to@openrainbow.com' xmlns='jabber:client'><userinvite id='57cd5922d341df5812bbcb72' action="update" type='received/sent' status='accepted' xmlns='jabber:iq:configuration'/></message>
     */
    
    [self loadTwoReceivedAndTwoSentInvitations];
    
    XCTestExpectation *simulateInvitationAcceptedEventReceived = [self expectationWithDescription:@"Receive invitation accepted event"];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * notification) {
        // Wait 5 seconds before sending the invitation accepted event
        // The invitation (id 111) with alice@my-company.com is a pending received one.
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message type='management' id='13222' from='pcloud10@jerome-all-in-one-dev-1.opentouch.cloud/22417636629952626711478624307115300' to='%@' xmlns='jabber:client'><userinvite id='111' action='update' type='received' status='accepted' xmlns='jabber:iq:configuration'/></message>", [self getFull]]];
            
            [simulateInvitationAcceptedEventReceived fulfill];
        });
        
        return YES;
    }];
    
    // App will get details of the updated invitation
    XCTestExpectation *invitationDetails = [self expectationWithDescription:@"Invitation details"];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        // new invitation ID is invitID
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"invitations/111"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        [invitationDetails fulfill];
        
        NSDictionary *dict = @{@"data": @{
                                       @"id": @"111",
                                       @"invitedUserId": @"56e6bc34c219157cb207e813",
                                       @"invitedUserEmail": @"me@company.com",
                                       @"invitingUserId": @"friend1RainbowID",
                                       @"invitingUserEmail": @"alice@my-company.com",
                                       @"invitingDate": @"2016-09-28T16:31:36.881Z",
                                       @"acceptationDate": @"2016-09-28T16:31:36.881Z",
                                       @"status": @"accepted",
                                       @"type": @"visibility"
                                       } };
        
        return [OHHTTPStubsResponse responseWithData:[[dict jsonStringWithPrettyPrint:NO] dataUsingEncoding:NSUTF8StringEncoding] statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateInvitation object:nil handler:^(NSNotification *notification) {
        Invitation *invitation = notification.object;
        if ([invitation.email isEqualToString:@"alice@my-company.com"]) {
            
            XCTAssertEqual(invitation.status, InvitationStatusAccepted);
            
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testSendInvitationToUserAndUserCreateHisAccount {
    [self loadTwoReceivedAndTwoSentInvitations];
    __block Contact *contactAssociatedToTheInvitation = nil;
    [self expectationForNotification:kContactsManagerServiceDidAddInvitation object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Invitation *invitation = notification.object;
        if([invitation.email isEqualToString:@"bob@company.com"]){
            contactAssociatedToTheInvitation = (Contact *)invitation.peer;
            XCTAssert(contactAssociatedToTheInvitation,@"No contact associated");
            
            // Now simulate bob create his account and accept the invitation
            /**
             <message xmlns="jabber:client" from="pcloud3@jerome-all-in-one-dev-1.opentouch.cloud/96449482362178211591481635146083193" to="c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="d5635314-9f6f-46be-a0c6-c658452d546b_662"><userinvite xmlns="jabber:iq:configuration" id="585018fa1ab5901cbb1b8912" action="update" type="sent" status="auto-accepted"/></message>
             */
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message type='management' id='13222' from='pcloud10@jerome-all-in-one-dev-1.opentouch.cloud/22417636629952626711478624307115300' to='%@' xmlns='jabber:client'><userinvite id='333' action='update' type='sent' status='auto-accepted' xmlns='jabber:iq:configuration'/></message>", [self getBare]]];
            
            return YES;
        }
        return NO;
    }];
    
    
    // App will get details of the updated invitation
    XCTestExpectation *invitationDetails = [self expectationWithDescription:@"Invitation details"];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        // new invitation ID is invitID
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"invitations/333"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        [invitationDetails fulfill];
        
        NSDictionary *dict = @{@"data": @{
                                       @"id": @"333",
                                       @"invitedUserId": @"56e6bc34c219157cb207e813",
                                       @"invitedUserEmail": @"bob@company.com",
                                       @"invitingUserId": @"56e6bc34c219157cb207e813",
                                       @"invitingUserEmail": @"me@company.com",
                                       @"invitingDate": @"2016-09-27T16:31:36.881Z",
                                       @"acceptationDate": @"2016-09-28T16:31:36.881Z",
                                       @"status": @"auto-accepted",
                                       @"type": @"visibility"
                                       } };
        
        return [OHHTTPStubsResponse responseWithData:[[dict jsonStringWithPrettyPrint:NO] dataUsingEncoding:NSUTF8StringEncoding] statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateInvitation object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Invitation *invitation = notification.object;
        if([invitation.email isEqualToString:@"bob@company.com"]){
            XCTAssert(contactAssociatedToTheInvitation == invitation.peer, @"The contact must be the same");
            return YES;
        }
        return NO;
    }];
    
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:300.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

@end
