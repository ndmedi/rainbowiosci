/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

// Test Framework
#import <XCTest/XCTest.h>
#import "OHHTTPStubs.h"
#import "OHPathHelpers.h"
#import "OHHTTPStubsResponse+JSON.h"
#import "NSMutableURLRequest+HTTPBodyTesting.h"
#import "XMPPStub.h"

//Rainbow service
#import "ServicesManager.h"
#import "DownloadManager.h"
#import "DirectoryService.h"
#import "MyUser.h"
#import "ApiUrlManagerService.h"
#import "ContactsManagerService.h"
#import "ConversationsManagerService.h"
#import "LocalContacts.h"
#import "XMPPService.h"
#import "LoginManager.h"
#import "MessagesBrowser.h"
#import "Tools.h"

#import <RainbowUnitTestFakeServer/RainbowUnitTestFakeServer.h>
// Use these 2 macros to generate Notification names, based on the calling method name.
#define GET_NOTIF_NAME(sel) [NSString stringWithFormat:@"XCT-%@-Delegate", NSStringFromSelector(sel)]
#define GENERATE_NOTIF_NAME GET_NOTIF_NAME(_cmd)

@interface RainbowSDKTestAbstract : XCTestCase <ContactsManagerServiceDelegate> {
    ServicesManager *_serviceManager;
    MyUser *_myUser;
    LoginManager *_loginManager;
    ContactsManagerService *_contactsManagerService;
    ConversationsManagerService *_conversationsManagerService;
    id<OHHTTPStubsDescriptor> stubGetGroupsEmpty;
    id<OHHTTPStubsDescriptor> stubGetInvitationsEmpty;
}

-(void) doLoginAndStartXMPP;
-(void) doLogout;

-(NSString*) getFull;
-(NSString *) getBare;
-(void) doLogout:(BOOL) doit;

-(Class) server;
@end
