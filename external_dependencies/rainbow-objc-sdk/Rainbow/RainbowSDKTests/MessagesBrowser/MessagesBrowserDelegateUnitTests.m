/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import "MessageInternal.h"
#import "MessagesBrowser+Internal.h"
#import "NSDate+Utilities.h"
#import "ContactInternal.h"


/*
  This file test some function we had issues with but on the UI part.
 
  This is why there is a lot of copy/pasted functions before the tests, and the tests
  are testing these copy/pasted functions..
 
  The day we'll have some tests on the UI (and in the RainbowUI project)
  We'll move these tests here.
 */


//  NSDateCategory.h
@interface NSDate (MBDateCat)
+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second;
+ (NSDate *)dateWithHour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second;
- (NSInteger) distanceInSecondsToDate:(NSDate *)anotherDate;
@end
@implementation NSDate (MBDateCat)
+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    [components setHour:hour];
    [components setMinute:minute];
    [components setSecond:second];
    return [calendar dateFromComponents:components];
}
+ (NSDate *)dateWithHour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:2016];
    [components setMonth:11];
    [components setDay:1];
    [components setHour:hour];
    [components setMinute:minute];
    [components setSecond:second];
    return [calendar dateFromComponents:components];
}
- (NSInteger) distanceInSecondsToDate:(NSDate *) anotherDate {
    if([self isEarlierThanDate:anotherDate]){
        NSTimeInterval distanceBetweenDates = [anotherDate timeIntervalSinceDate:self];
        return distanceBetweenDates;
    } else {
        NSTimeInterval distanceBetweenDates = [self timeIntervalSinceDate:anotherDate];
        return distanceBetweenDates;
    }
}
@end

@interface GroupedMessages : NSObject
@property (nonatomic, readonly) NSArray<Message *> * allGroupedMessages;
-(instancetype) initWithMessage:(Message *) message;
-(void) addMessage:(Message *) message;
-(void) removeMessage:(Message *) message;
-(Message *) lastMessage;
-(Message *) firstMessage;
@end

@interface GroupedMessages ()
@property (nonatomic, strong, readwrite) NSMutableArray<Message *> * allGroupedMessages;
@end

@implementation GroupedMessages

-(instancetype) initWithMessage:(Message *) message {
    self = [super init];
    if(self){
        _allGroupedMessages = [NSMutableArray array];
        [self insertMessageAtProperIndex:message];
    }
    return self;
}

-(void) dealloc {
    [_allGroupedMessages removeAllObjects];
    _allGroupedMessages = nil;
}

-(void) addMessage:(Message *) message {
    [self insertMessageAtProperIndex:message];
}

-(void) removeMessage:(Message *) message {
    [_allGroupedMessages removeObject:message];
}

-(NSInteger) insertMessageAtProperIndex:(Message *) message {
    NSInteger idxForInsertion = NSNotFound;
    if ([_allGroupedMessages indexOfObject:message]==NSNotFound) {
        idxForInsertion = [self properIndexForItem:message];
        [_allGroupedMessages insertObject:message atIndex:idxForInsertion];
    }
    return idxForInsertion;
}

-(NSInteger) properIndexForItem:(Message *) message {
    __block NSInteger insertIndex = [_allGroupedMessages count];
    // if we don't find a proper place in the list, this means we should add it at the end.
    [_allGroupedMessages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        GroupedMessages *item = obj;
        // if item.date is not later than aNewItem.date
        if (item.date!=nil && ![item isEqual:message] && [item.date compare:message.date]!=NSOrderedAscending) {
            *stop = YES;
            insertIndex = idx;
        }
    }];
    return insertIndex;
}

-(Message *) lastMessage {
    return [_allGroupedMessages lastObject];
}

-(Message *) firstMessage {
    return [_allGroupedMessages firstObject];
}

-(NSString *) description {
    return [NSString stringWithFormat:@"Grouped Messages : %lu content: %@",(unsigned long)_allGroupedMessages.count, [_allGroupedMessages componentsJoinedByString:@"\n"] ];
}

#pragma mark - JSQMessageData protocol
-(NSString *)senderId {
    if (self.lastMessage.isOutgoing)
        return @"me@domain.com";
    return [_allGroupedMessages lastObject].peer.jid;
}

-(NSString *) senderDisplayName {
    if (self.lastMessage.isOutgoing)
        return @"me@domain.com";
    return [_allGroupedMessages lastObject].peer.displayName;
}

-(BOOL) isMediaMessage {
    return NO;
}

-(NSUInteger) messageHash {
    // The JSQ doc says :
    // "This value is used to cache layout information in the collection view."
    // This is used as the message bubble-size cache identifier.
    // But our outgoing-bubble and incomming-bubble does not have the same size (due to the received/read icon)
    return [[NSString stringWithFormat:@"%d%@", (int)[_allGroupedMessages lastObject].isOutgoing, self.text] hash];
}

-(NSString *) text {
    NSMutableString *body = [NSMutableString string];
    BOOL shouldReturnToNewLine = _allGroupedMessages.count > 1;
    for (Message *message in _allGroupedMessages) {
        [body appendString:message.body];
        if(shouldReturnToNewLine)
            [body appendString:@"\n"];
    }
    
    return [body stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
}

-(NSDate *) date {
    return [_allGroupedMessages lastObject].date;
}
@end



@interface MessagesBrowserDelegateUnitTests : XCTestCase
@property (nonatomic, strong) NSMutableArray<GroupedMessages *> *messages;
@end

@implementation MessagesBrowserDelegateUnitTests

-(NSInteger) insertMessageAtProperIndex:(GroupedMessages *) groupedMessages {
    NSInteger idxForInsertion = NSNotFound;
    if ([_messages indexOfObject:groupedMessages]==NSNotFound) {
        idxForInsertion = [self properIndexForItem:groupedMessages];
        [_messages insertObject:groupedMessages atIndex:idxForInsertion];
    }
    return idxForInsertion;
}

-(NSInteger) properIndexForItem:(GroupedMessages *) groupedMessages {
    __block NSInteger insertIndex = [_messages count];
    // if we don't find a proper place in the list, this means we should add it at the end.
    [_messages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        GroupedMessages *item = obj;
        // if item.date is not later than aNewItem.date
        if (item.date!=nil && ![item isEqual:groupedMessages] && [item.date compare:groupedMessages.date]!=NSOrderedDescending) {
            *stop = YES;
            insertIndex = idx;
        }
    }];
    return insertIndex;
}

-(void) setUp {
    _messages = [NSMutableArray new];
}

-(BOOL) shouldGroupMessage:(Message *) message1 with:(Message *) message2 {
    // Never group things different from chat and groupchat
    if (message1.type != MessageTypeChat && message1.type != MessageTypeGroupChat)
        return NO;
    
    if (message2.type != MessageTypeChat && message2.type != MessageTypeGroupChat)
        return NO;
    
    NSInteger distance = [message1.date distanceInSecondsToDate:message2.date];
    if (distance < 0 || distance > 60)
        return NO;
    
    if([message1.peer isEqual:message2.peer] && message1.type == message2.type && message1.isOutgoing == message2.isOutgoing)
        return YES;
    return NO;
}

-(void) itemsBrowser:(CKItemsBrowser*)browser didAddCacheItems:(NSArray*)newItems atIndexes:(NSIndexSet*)indexes {
    NSAssert(_messages, @"should have messages");
    
    for (Message *message in newItems) {
        // Each one on these new messages could be at a different index
        // Which could be anywhere inside our current grouped messages.
        // For example we could have a new message from A which should appear between 2 messages (already grouped) from B.
        // Then we have to split the grouped message from B in 2 grouped messages and add the message from A in
        // the middle.
        Message *previousMessage = nil;
        GroupedMessages *previousGroupedMessages = nil;
        Message *nextMessage = nil;
        GroupedMessages *nextGroupedMessages = nil;
        
        for (GroupedMessages *groupedMessages in [_messages reverseObjectEnumerator]) {
            for (Message *msg in groupedMessages.allGroupedMessages) {
                // Dont forget to handle the equal case !
                if ([msg.date compare:message.date] == NSOrderedAscending || [msg.date compare:message.date] == NSOrderedSame) {
                    previousMessage = msg;
                    previousGroupedMessages = groupedMessages;
                }
                if ([msg.date compare:message.date] == NSOrderedDescending && !nextMessage) {
                    nextMessage = msg;
                    nextGroupedMessages = groupedMessages;
                }
            }
        }

        if (previousMessage) {
            if (nextMessage) {
                // we arrive here when we have a previous AND a next message.
                // This new message is somewhere in the middle of our list.
                // This is the most complicated case, we might have to un-merge already grouped messages, etc.
                
                // First case is when the previous and next are in the same groupedmessage
                if (previousGroupedMessages == nextGroupedMessages) {
                    // We can add the new message to the existing grouped if same peer, etc.
                    if ([self shouldGroupMessage:previousGroupedMessages.firstMessage with:message]) {
                        [previousGroupedMessages addMessage:message];
                    } else {
                        // HERE we have to un-merge the grouped message and add a new between.
                        GroupedMessages *previousGroup = [[GroupedMessages alloc] initWithMessage:previousGroupedMessages.firstMessage];
                        GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                        GroupedMessages *nextGroup = [[GroupedMessages alloc] initWithMessage:nextMessage];
                        
                        for (Message *m in [previousGroupedMessages allGroupedMessages]) {
                            // Dont forget the equal case !
                            if ([m.date compare:message.date] == NSOrderedAscending || [m.date compare:message.date] == NSOrderedSame) {
                                [previousGroup addMessage:m];
                            }
                            if ([m.date compare:message.date] == NSOrderedDescending) {
                                [nextGroup addMessage:m];
                            }
                        }
                        [_messages removeObject:previousGroupedMessages];
                        [self insertMessageAtProperIndex:previousGroup];
                        [self insertMessageAtProperIndex:newGroup];
                        [self insertMessageAtProperIndex:nextGroup];
                    }
                } else {
                    // prev and next are not the same grouped, then its easy, we can have 3 cases :
                    // put the new message in the previous, in the next, or create a new one.
                    if ([self shouldGroupMessage:previousGroupedMessages.firstMessage with:message]) {
                        [previousGroupedMessages addMessage:message];
                    } else if ([self shouldGroupMessage:nextGroupedMessages.firstMessage with:message]) {
                        [nextGroupedMessages addMessage:message];
                    } else {
                        // create a new grouped message
                        GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                        [self insertMessageAtProperIndex:newGroup];
                    }
                }
                
            } else {
                // This is the most common case, we have a previous but no next
                // This is a new message at the bottom of the list.
                if ([self shouldGroupMessage:previousGroupedMessages.firstMessage with:message]) {
                    [previousGroupedMessages addMessage:message];
                } else {
                    // create a new grouped message
                    GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                    [self insertMessageAtProperIndex:newGroup];
                }
            }
        } else {
            if (nextMessage) {
                // We have no previous message but a next message
                // This message goes to the top of the list (it is the new oldest message)
                if ([self shouldGroupMessage:nextGroupedMessages.firstMessage with:message]) {
                    [nextGroupedMessages addMessage:message];
                } else {
                    // create a new grouped message
                    GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                    [self insertMessageAtProperIndex:newGroup];
                }
            } else {
                // No previous and no next message ?
                // This is the first message of the list.
                GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                [self insertMessageAtProperIndex:newGroup];
            }
        }
    }
}

-(void) testNewToMiddleOfGroupedWithUnmerge {
    NSObject *peer1 = [NSObject new];
    NSObject *peer2 = [NSObject new];
    
    // Create 1 existing groupedmessage with 3 messages
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:0 second:10];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    
    Message *m2 = [Message new];
    m2.timestamp = [NSDate dateWithHour:12 minute:0 second:20];
    m2.body = @"M2";
    m2.peer = (Peer*) peer1;
    m2.type = MessageTypeChat;
    
    Message *m3 = [Message new];
    m3.timestamp = [NSDate dateWithHour:12 minute:0 second:30];
    m3.body = @"M3";
    m3.peer = (Peer*) peer1;
    m3.type = MessageTypeChat;
    
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [g1 addMessage:m2];
    [g1 addMessage:m3];
    [self insertMessageAtProperIndex:g1];
    
    // Add 1 new that happend sometimes during the grouped message.
    // At the exact same time of M2 (to check how is handled the equality)
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:0 second:20];
    a1.body = @"A1";
    a1.peer = (Peer*) peer2;
    a1.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // They have been unmerged in 2 grouped + the new group = 3
    XCTAssertEqual([_messages count], 3);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[1].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[2].allGroupedMessages count], 2);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"M3"]);
    XCTAssert([_messages[1].allGroupedMessages[0].body isEqualToString:@"A1"]);
    XCTAssert([_messages[2].allGroupedMessages[0].body isEqualToString:@"M1"]);
    XCTAssert([_messages[2].allGroupedMessages[1].body isEqualToString:@"M2"]);
}

-(void) testNewToMiddleOfGroupedWithoutUnmerge {
    NSObject *peer1 = [NSObject new];
    
    // Create one existing groupedmessage with 2 messages
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:0 second:10];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    
    Message *m2 = [Message new];
    m2.timestamp = [NSDate dateWithHour:12 minute:0 second:30];
    m2.body = @"M2";
    m2.peer = (Peer*) peer1;
    m2.type = MessageTypeChat;
    
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [g1 addMessage:m2];
    [self insertMessageAtProperIndex:g1];
    
    // Add 1 new that happend between the existing 2 messages.
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:0 second:20];
    a1.body = @"A1";
    a1.peer = (Peer*) peer1;
    a1.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // The new msg has been put in middle of the grouped message
    XCTAssertEqual([_messages count], 1);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 3);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"M1"]);
    XCTAssert([_messages[0].allGroupedMessages[1].body isEqualToString:@"A1"]);
    XCTAssert([_messages[0].allGroupedMessages[2].body isEqualToString:@"M2"]);
}

-(void) testNewToMiddleOfNonGroupedWithMergeInPrevious {
    NSObject *peer1 = [NSObject new];
    NSObject *peer2 = [NSObject new];
    
    // Create 2 existing groupedmessage with 1 messages
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:0 second:0];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [self insertMessageAtProperIndex:g1];
    
    Message *m2 = [Message new];
    m2.timestamp = [NSDate dateWithHour:12 minute:2 second:0];
    m2.body = @"M2";
    m2.peer = (Peer*) peer2;
    m2.type = MessageTypeChat;
    GroupedMessages *g2 = [[GroupedMessages alloc] initWithMessage:m2];
    [self insertMessageAtProperIndex:g2];
    
    // Add 1 new that happend between the existing 2 messages,
    // which should merge with previous one. (so enough close in time to the previous message)
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:0 second:30];
    a1.body = @"A1";
    a1.peer = (Peer*) peer1;
    a1.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // Still 2 grouped. new message in the previous one.
    XCTAssertEqual([_messages count], 2);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[1].allGroupedMessages count], 2);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"M2"]);
    XCTAssert([_messages[1].allGroupedMessages[0].body isEqualToString:@"M1"]);
    XCTAssert([_messages[1].allGroupedMessages[1].body isEqualToString:@"A1"]);
}

-(void) testNewToMiddleOfNonGroupedWithMergeInNext {
    NSObject *peer1 = [NSObject new];
    NSObject *peer2 = [NSObject new];
    
    // Create 2 existing groupedmessage with 1 messages
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:0 second:0];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [self insertMessageAtProperIndex:g1];
    
    Message *m2 = [Message new];
    m2.timestamp = [NSDate dateWithHour:12 minute:2 second:0];
    m2.body = @"M2";
    m2.peer = (Peer*) peer2;
    m2.type = MessageTypeChat;
    GroupedMessages *g2 = [[GroupedMessages alloc] initWithMessage:m2];
    [self insertMessageAtProperIndex:g2];
    
    // Add 1 new that happend between the existing 2 messages,
    // which should merge with next one. (so enough close in time to the next message)
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:1 second:30];
    a1.body = @"A1";
    a1.peer = (Peer*) peer2;
    a1.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // Still 2 grouped. new message in the previous one.
    XCTAssertEqual([_messages count], 2);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 2);
    XCTAssertEqual([_messages[1].allGroupedMessages count], 1);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"A1"]);
    XCTAssert([_messages[0].allGroupedMessages[1].body isEqualToString:@"M2"]);
    XCTAssert([_messages[1].allGroupedMessages[0].body isEqualToString:@"M1"]);
}

-(void) testNewToMiddleOfNonGroupedWithNewGroup {
    NSObject *peer1 = [NSObject new];
    NSObject *peer2 = [NSObject new];
    NSObject *peer3 = [NSObject new];
    
    // Create 2 existing groupedmessage with 1 messages
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:0 second:0];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [self insertMessageAtProperIndex:g1];
    
    Message *m2 = [Message new];
    m2.timestamp = [NSDate dateWithHour:12 minute:2 second:0];
    m2.body = @"M2";
    m2.peer = (Peer*) peer2;
    m2.type = MessageTypeChat;
    GroupedMessages *g2 = [[GroupedMessages alloc] initWithMessage:m2];
    [self insertMessageAtProperIndex:g2];
    
    // Add 1 new that happend between the existing 2 messages,
    // which should NOT merge with the other ones
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:1 second:0];
    a1.body = @"A1";
    a1.peer = (Peer*) peer3;
    a1.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // Now 3 grouped. new message in the new group one.
    XCTAssertEqual([_messages count], 3);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[1].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[2].allGroupedMessages count], 1);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"M2"]);
    XCTAssert([_messages[1].allGroupedMessages[0].body isEqualToString:@"A1"]);
    XCTAssert([_messages[2].allGroupedMessages[0].body isEqualToString:@"M1"]);
}

-(void) testNewToBottomGrouped {
    NSObject *peer1 = [NSObject new];
    
    // Create one existing message
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:2 second:0];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [self insertMessageAtProperIndex:g1];
    
    // Add 1 new that happend after the existing one. (close enough in time to be grouped)
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:2 second:30];
    a1.body = @"A1";
    a1.peer = (Peer*) peer1;
    a1.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // They have been merge in one grouped message;
    XCTAssertEqual([_messages count], 1);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 2);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"M1"]);
    XCTAssert([_messages[0].allGroupedMessages[1].body isEqualToString:@"A1"]);
}

-(void) testNewToBottomNotGrouped {
    NSObject *peer1 = [NSObject new];
    NSObject *peer2 = [NSObject new];

    // Create one existing message
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:2 second:0];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [self insertMessageAtProperIndex:g1];
    
    // Add 1 new that happend after the existing one.
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:2 second:30];
    a1.body = @"A1";
    a1.peer = (Peer*) peer2;
    a1.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // They have been merge in one grouped message;
    XCTAssertEqual([_messages count], 2);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[1].allGroupedMessages count], 1);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"A1"]);
    XCTAssert([_messages[1].allGroupedMessages[0].body isEqualToString:@"M1"]);
}

-(void) testNewToTopGrouped {
    NSObject *peer1 = [NSObject new];
    
    // Create one existing message
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:2 second:0];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [self insertMessageAtProperIndex:g1];
    
    // Add 1 new that happend before the existing one. (close enough in time to be grouped)
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:1 second:30];
    a1.body = @"A1";
    a1.peer = (Peer*) peer1;
    a1.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // They have been merge in one grouped message;
    XCTAssertEqual([_messages count], 1);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 2);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"A1"]);
    XCTAssert([_messages[0].allGroupedMessages[1].body isEqualToString:@"M1"]);
}

-(void) testNewToTopNotGrouped {
    NSObject *peer1 = [NSObject new];
    NSObject *peer2 = [NSObject new];
    
    // Create one existing message
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:2 second:0];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [self insertMessageAtProperIndex:g1];
    
    // Add 1 new that happend before the existing one
    // But from another peer
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:1 second:30];
    a1.body = @"A1";
    a1.peer = (Peer*) peer2;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // We should have 2 grouped messages
    XCTAssertEqual([_messages count], 2);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[1].allGroupedMessages count], 1);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"M1"]);
    XCTAssert([_messages[1].allGroupedMessages[0].body isEqualToString:@"A1"]);
}

-(void) testNewFirst {
    
    // Add 1 new message which is the first one in our list.
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:1 second:0];
    a1.body = @"A1";
    a1.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // 1 group created with the first message.
    XCTAssertEqual([_messages count], 1);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 1);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"A1"]);
}

-(void) testShouldNotGroupDueToTime {
    NSObject *peer1 = [NSObject new];
    
    // Create one existing message
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:0 second:0];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [self insertMessageAtProperIndex:g1];
    
    // Add 1 new that match in all point (same peer, same type, etc. but too far in time)
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:1 second:30];
    a1.body = @"A1";
    a1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // We should have 2 grouped messages
    XCTAssertEqual([_messages count], 2);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[1].allGroupedMessages count], 1);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"A1"]);
    XCTAssert([_messages[1].allGroupedMessages[0].body isEqualToString:@"M1"]);
}

-(void) testShouldNotGroupDueToTimeOpposite {
    NSObject *peer1 = [NSObject new];
    
    // Create one existing message
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:1 second:30];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [self insertMessageAtProperIndex:g1];
    
    // Add 1 new that match in all point (same peer, same type, etc. but too far in time)
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:0 second:0];
    a1.body = @"A1";
    a1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // We should have 2 grouped messages
    XCTAssertEqual([_messages count], 2);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[1].allGroupedMessages count], 1);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"M1"]);
    XCTAssert([_messages[1].allGroupedMessages[0].body isEqualToString:@"A1"]);
}

-(void) testShouldNotGroupDueToType {
    NSObject *peer1 = [NSObject new];
    
    // Create one existing message
    Message *m1 = [Message new];
    m1.timestamp = [NSDate dateWithHour:12 minute:0 second:0];
    m1.body = @"M1";
    m1.peer = (Peer*) peer1;
    m1.type = MessageTypeChat;
    GroupedMessages *g1 = [[GroupedMessages alloc] initWithMessage:m1];
    [self insertMessageAtProperIndex:g1];
    
    // Add 1 new that match in all point (same peer, correct time, etc. but different type)
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:0 second:30];
    a1.body = @"A1";
    a1.peer = (Peer*) peer1;
    m1.type = MessageTypeWebRTC;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1] atIndexes:nil];
    
    // We should have 2 grouped messages
    XCTAssertEqual([_messages count], 2);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[1].allGroupedMessages count], 1);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"A1"]);
    XCTAssert([_messages[1].allGroupedMessages[0].body isEqualToString:@"M1"]);
}

-(void) testCorrectComputeOfNextMessage {
    NSObject *peer1 = [NSObject new];
    NSObject *peer2 = [NSObject new];
    
    Message *a1 = [Message new];
    a1.timestamp = [NSDate dateWithHour:12 minute:0 second:40];
    a1.body = @"A1";
    a1.peer = (Peer*) peer1;
    a1.type = MessageTypeChat;
    
    Message *b1 = [Message new];
    b1.timestamp = [NSDate dateWithHour:12 minute:0 second:30];
    b1.body = @"B1";
    b1.peer = (Peer*) peer2;
    b1.type = MessageTypeChat;
    
    Message *a2 = [Message new];
    a2.timestamp = [NSDate dateWithHour:12 minute:0 second:20];
    a2.body = @"A2";
    a2.peer = (Peer*) peer1;
    a2.type = MessageTypeChat;
    
    Message *b2 = [Message new];
    b2.timestamp = [NSDate dateWithHour:12 minute:0 second:10];
    b2.body = @"B2";
    b2.peer = (Peer*) peer2;
    b2.type = MessageTypeChat;
    
    [self itemsBrowser:nil didAddCacheItems:@[a1, b1, a2, b2] atIndexes:nil];
    
    // The new msg has been put in middle of the grouped message
    XCTAssertEqual([_messages count], 4);
    XCTAssertEqual([_messages[0].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[1].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[2].allGroupedMessages count], 1);
    XCTAssertEqual([_messages[3].allGroupedMessages count], 1);
    XCTAssert([_messages[0].allGroupedMessages[0].body isEqualToString:@"A1"]);
    XCTAssert([_messages[1].allGroupedMessages[0].body isEqualToString:@"B1"]);
    XCTAssert([_messages[2].allGroupedMessages[0].body isEqualToString:@"A2"]);
    XCTAssert([_messages[3].allGroupedMessages[0].body isEqualToString:@"B2"]);
}

@end
