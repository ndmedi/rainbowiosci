/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import "MessageInternal.h"
#import "MessagesBrowser+Internal.h"
#import "NSDate+Utilities.h"
#import "ContactInternal.h"

// MOCK XMPP SERVICE

@interface XMPPServiceMock : XMPPService
@property (nonatomic, strong) NSArray <Message *> * returnedMessages;
@property (nonatomic) BOOL isCompletedRequest;
-(NSArray *) getArchivedMessageWithContactJid:(NSString *) contactJid maxSize:(NSNumber *) maxSize offset:(NSNumber *) offset;
-(void)loadArchivedMessagesWith:(Contact *)contact maxSize:(NSNumber *)maxSize beforeDate:(NSDate *)beforeDate lastMessageID:(NSString *)lastMessageID offset:(NSNumber *) offset withCompletionHandler:(XmppServiceLoadArchivedMessagesCompletionHandler) completionHandler;
@end

@implementation XMPPServiceMock
-(NSArray *) getArchivedMessageWithContactJid:(NSString *) contactJid maxSize:(NSNumber *) maxSize offset:(NSNumber *) offset {
    NSLog(@"GET ARCHIVE IN MOCK");
    NSInteger offsetInt = [offset integerValue];
    NSInteger maxSizeInt = [maxSize integerValue];
    
    NSInteger arraySize = [_returnedMessages count];
    NSRange range;
    
    if(offsetInt+maxSizeInt > arraySize){
        range = NSMakeRange(offsetInt, arraySize - offsetInt);
    } else {
        range = NSMakeRange(offsetInt, maxSizeInt);
    }
    
    return [_returnedMessages subarrayWithRange:range];
}

-(void)loadArchivedMessagesWith:(Contact *)contact maxSize:(NSNumber *)maxSize beforeDate:(NSDate *)beforeDate lastMessageID:(NSString *)lastMessageID offset:(NSNumber *) offset withCompletionHandler:(XmppServiceLoadArchivedMessagesCompletionHandler) completionHandler {
    NSLog(@"LOAD ARCHIVE IN MOCK %@ %@ %@ %@", maxSize, beforeDate, lastMessageID, offset);
    
    NSInteger offsetInt = [offset integerValue];
    NSInteger maxSizeInt = [maxSize integerValue];
    
    NSInteger arraySize = [_returnedMessages count];
    NSRange range;
    
    if(offsetInt+maxSizeInt > arraySize){
        range = NSMakeRange(offsetInt, arraySize - offsetInt);
    } else {
        range = NSMakeRange(offsetInt, maxSizeInt);
    }
    
    NSArray *filteredArray = [_returnedMessages subarrayWithRange:range];
    
    Message *firstMessage = [filteredArray firstObject];
    Message *lastMessage = [filteredArray lastObject];
    if(completionHandler){
        NSLog(@"COMPLETION HANDLER result %@", filteredArray);
        completionHandler(filteredArray, nil, firstMessage.messageID, lastMessage.messageID, _isCompletedRequest, [NSNumber numberWithInteger:[_returnedMessages count]]);
    }
}

@end

@interface MessagesBrowserUnitTests : XCTestCase
@property (nonatomic, strong) XMPPServiceMock *xmppMock;
@property (nonatomic, strong) Conversation *conversation;
@end

@implementation MessagesBrowserUnitTests

-(void) setUp {
    [super setUp];
    
    _xmppMock = [[XMPPServiceMock alloc] init];
    _conversation = [Conversation new];
}

-(void) tearDown {
    [super tearDown];
    _xmppMock = nil;
    _conversation = nil;
}

-(void) testGetArchivedMessages {
    [self populateResponseWithSize:10];
    MessagesBrowser *browser = [[MessagesBrowser alloc] initWithConversation:_conversation withXMPPService:_xmppMock withPageSize:5];
    [browser getArchivedMessages];
    [self loopForAWhile:0.5];
    XCTAssertEqual([browser.browsingCache count], 5);
    XCTAssertTrue([browser hasMorePages]);
}

-(void) testGetArchivedMessagesAndAddTwoMessages {
    [self populateResponseWithSize:5];
    MessagesBrowser *browser = [[MessagesBrowser alloc] initWithConversation:_conversation withXMPPService:_xmppMock withPageSize:5];
    [browser getArchivedMessages];
    [self loopForAWhile:0.5];
    XCTAssertEqual([browser.browsingCache count], 5);
    XCTAssertTrue([browser hasMorePages]);
    
    [self addMessageInResponse:2];
    
    [browser resyncBrowsingCacheWithCompletionHandler:^(NSArray *addedCacheItems, NSArray *removedCacheItems, NSArray *updatedCacheItems, NSError *error) {
        NSLog(@"NEXT PAGE");
        XCTAssertEqual([addedCacheItems count], 2);
    }];

    [self loopForAWhile:0.5];
    XCTAssertEqual([browser.browsingCache count], 7);
    XCTAssertTrue([browser hasMorePages]);
}

-(void) testGetArchivedMessagesAndResync {
    [self populateResponseWithSize:5];
    MessagesBrowser *browser = [[MessagesBrowser alloc] initWithConversation:_conversation withXMPPService:_xmppMock withPageSize:5];
    [browser getArchivedMessages];
    [self loopForAWhile:0.5];
    XCTAssertEqual([browser.browsingCache count], 5);
    XCTAssertTrue([browser hasMorePages]);
    
    [browser resyncBrowsingCacheWithCompletionHandler:^(NSArray *addedCacheItems, NSArray *removedCacheItems, NSArray *updatedCacheItems, NSError *error) {
        NSLog(@"RESYNC DONE");
        XCTAssertEqual([addedCacheItems count], 0);
        XCTAssertEqual([updatedCacheItems count], 0);
        XCTAssertEqual([removedCacheItems count], 0);
    }];
    [self loopForAWhile:0.5];
    XCTAssertEqual([browser.browsingCache count], 5);
}

-(void) testGetArchivedMessagesAndResyncReturnZeroMessages {
    [self populateResponseWithSize:5];
    MessagesBrowser *browser = [[MessagesBrowser alloc] initWithConversation:_conversation withXMPPService:_xmppMock withPageSize:5];
    [browser getArchivedMessages];
    [self loopForAWhile:0.5];
    XCTAssertEqual([browser.browsingCache count], 5);
    XCTAssertTrue([browser hasMorePages]);
    
    [browser resyncBrowsingCacheWithCompletionHandler:^(NSArray *addedCacheItems, NSArray *removedCacheItems, NSArray *updatedCacheItems, NSError *error) {
        NSLog(@"RESYNC DONE");
        XCTAssertEqual([addedCacheItems count], 0);
        XCTAssertEqual([updatedCacheItems count], 0);
        XCTAssertEqual([removedCacheItems count], 0);
    }];
    [self loopForAWhile:0.5];
    XCTAssertEqual([browser.browsingCache count], 5);
    
    NSLog(@"SECOND GET ARCHIVED");
    // simulate closing the conversation.
    [browser reset];
    [browser getArchivedMessages];
    [self loopForAWhile:0.5];
    XCTAssertEqual([browser.browsingCache count], 5);
    
    [browser resyncBrowsingCacheWithCompletionHandler:^(NSArray *addedCacheItems, NSArray *removedCacheItems, NSArray *updatedCacheItems, NSError *error) {
        NSLog(@"RESYNC 2 DONE");
        XCTAssertEqual([addedCacheItems count], 0);
        XCTAssertEqual([updatedCacheItems count], 0);
        XCTAssertEqual([removedCacheItems count], 0);
    }];
    [self loopForAWhile:0.5];
    XCTAssertEqual([browser.browsingCache count], 5);
}


-(void) populateResponseWithSize:(int) size {
    NSMutableArray *messages = [NSMutableArray array];
    for (int i = 0; i<size; i++) {
        Message *msg = [Message new];
        msg.body = [NSString stringWithFormat:@"TEST message %d",i];
        msg.type = MessageTypeChat;
        msg.timestamp = [NSDate dateWithHoursFromNow:i];
        msg.peer = nil;
        msg.via = nil;
        msg.messageID = [NSString stringWithFormat:@"%d",i];
        msg.mamMessageID = msg.messageID;
        [messages addObject:msg];
    }
    _xmppMock.returnedMessages = messages;
}

-(void) addMessageInResponse:(int) nbMessage {
    NSMutableArray *response = [_xmppMock.returnedMessages mutableCopy];
    for (int i = 0; i < nbMessage; i++) {
        Message *msg = [Message new];
        msg.body = [NSString stringWithFormat:@"TEST ADDED message %d",i];
        msg.type = MessageTypeChat;
        msg.timestamp = [NSDate dateWithHoursFromNow:i];
        msg.peer = nil;
        msg.via = nil;
        msg.messageID = [NSString stringWithFormat:@"ADDED%d",i];
        msg.mamMessageID = msg.messageID;
        [response addObject:msg];
    }
    _xmppMock.returnedMessages = response;
}

-(void) loopForAWhile:(NSTimeInterval)loopTime {
    NSDate* secLater = [[NSDate date] dateByAddingTimeInterval:loopTime];
    NSLog(@"[U-TEST] Running run loop for %.1fs ...", loopTime);
    while ([(NSDate*)[NSDate date] compare:secLater]==NSOrderedAscending) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:secLater];
    }
    NSLog(@"[U-TEST] End of running run loop");
}

@end
