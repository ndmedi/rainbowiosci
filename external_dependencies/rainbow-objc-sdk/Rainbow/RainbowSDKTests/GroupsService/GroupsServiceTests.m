/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowSDKTestAbstract.h"
#import "NSDictionary+JSONString.h"
#import "ContactInternal.h"
#import "RainbowContact.h"
#import "Group+Internal.h"
#import "ContactsManagerService+Internal.h"

@interface GroupsServiceTests : RainbowSDKTestAbstract

@end

@implementation GroupsServiceTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}


-(void) loadGroupsNotEmpty {
    [OHHTTPStubs removeStub:stubGetGroupsEmpty];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/groups"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"getGroups.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}

-(void) loadGroupsWithTwoUsers {
    [OHHTTPStubs removeStub:stubGetGroupsEmpty];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/groups"] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"getGroups2Users.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
}


-(void) testLoginNoGroups {
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        XCTAssertTrue(_serviceManager.groupsService.groups.count == 0, @"We must have no group");
        return YES;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testGetGroups {
    [self loadGroupsNotEmpty];
    [self expectationForNotification:kGroupsServiceDidAddGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        Group *group = (Group *)notification.object;
        XCTAssertTrue([group.name isEqualToString:@"Colleagues"], @"Group must have name Colleagues");
        XCTAssertTrue(group.users.count == 0, @"We must not have any users in this group");
        XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
        XCTAssertTrue(_serviceManager.groupsService.groups.count == 1, @"We must have no group");
        return YES;
    }];
    
    // This will also start the xmpp connection.
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testCreateGroup {
    [self loadGroupsNotEmpty];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/groups"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString* fixture = OHPathForFile(@"createGroup.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kGroupsServiceDidAddGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *)notification.object;
        if([group.name isEqualToString:@"Colleagues"]){
            XCTAssertTrue([group.name isEqualToString:@"Colleagues"], @"Group must have name Colleagues");
            XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
            XCTAssertTrue(group.users.count == 0, @"We must not have any users in this group");
            XCTAssertTrue(_serviceManager.groupsService.groups.count == 1, @"We must have one group");
            [_serviceManager.groupsService createGroupWithName:@"MyTestGroup" andComment:@"MyTestComment"];
        } else {
            XCTAssertTrue([group.name isEqualToString:@"MyTestGroup"], @"Group must have name MyTestGroup");
            XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
            XCTAssertTrue([group.comment isEqualToString:@"MyTestComment"], @"Group comment must be MyTestComment");
            XCTAssertTrue(group.users.count == 0, @"We must not have any users in this group");
            XCTAssertTrue(_serviceManager.groupsService.groups.count == 2, @"We must have two groups");
            return YES;
        }
        return NO;
    }];
    
    // This will also start the xmpp connection.
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testCreateGroupOtherDevice {
    [self loadGroupsNotEmpty];
    NSString *groupID = @"57b44e9c0c32e0b425252f9f";
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"/groups/%@",groupID]] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString* fixture = OHPathForFile(@"getGroupDetails1User.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message type='management' id='0' from='admin@mycompany.com' to='%@' xmlns='jabber:client'><group id='%@' action='create' scope='group' xmlns='jabber:iq:configuration'/></message>", [self getBare], groupID]];
        });
        return YES;
    }];
    
    [self expectationForNotification:kGroupsServiceDidAddGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *)notification.object;
        if([group.rainbowID isEqualToString:groupID]){
            XCTAssertTrue([group.rainbowID isEqualToString:groupID], @"Group must have the good rainbow ID");
            XCTAssertTrue(_serviceManager.groupsService.groups.count == 2, @"We must have two groups");
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kGroupsServiceDidUpdateGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *)notification.object;
        if([group.rainbowID isEqualToString:groupID]){
            XCTAssertTrue([group.name isEqualToString:@"GroupOtherDevice"], @"Group must have name GroupOtherDevice");
            XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
            XCTAssertTrue(group.users.count == 1, @"We must not have any users in this group");
            XCTAssertTrue(_serviceManager.groupsService.groups.count == 2, @"We must have two groups");
            return YES;
        }
        return NO;
    }];

    // This will also start the xmpp connection.
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testDeleteGroup {
    [self loadGroupsNotEmpty];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/groups"] && [request.HTTPMethod isEqualToString:@"DELETE"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        return [OHHTTPStubsResponse responseWithJSONObject:@{@"status":@"Deleted", @"data":@[]} statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kGroupsServiceDidAddGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *)notification.object;
        if([group.name isEqualToString:@"Colleagues"]){
            XCTAssertTrue([group.name isEqualToString:@"Colleagues"], @"Group must have name Colleagues");
            XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
            XCTAssertTrue(group.users.count == 0, @"We must not have any users in this group");
            XCTAssertTrue(_serviceManager.groupsService.groups.count == 1, @"We must have one group");
            [_serviceManager.groupsService deleteGroup:group];
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kGroupsServiceDidRemoveGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *) notification.object;
        XCTAssertTrue([group.name isEqualToString:@"Colleagues"], @"Deleted group must have Colleagues as name");
        XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
        XCTAssertTrue(_serviceManager.groupsService.groups.count == 0, @"We must have no groups");
        return YES;
    }];
    
    // This will also start the xmpp connection.
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testDeleteGroupOtherDevice {
    NSString *groupID = @"57cd4346d8793a0d124aa21e";
    [self loadGroupsNotEmpty];
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
       
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message type='management' id='0' from='admin@mycompany.com' to='%@' xmlns='jabber:client'><group id='%@' action='delete' scope='group' xmlns='jabber:iq:configuration'/></message>", [self getBare], groupID]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kGroupsServiceDidRemoveGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *) notification.object;
        XCTAssertTrue([group.name isEqualToString:@"Colleagues"], @"Deleted group must have Colleagues as name");
        XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
        XCTAssertTrue(_serviceManager.groupsService.groups.count == 0, @"We must have no groups");
        return YES;
    }];
    
    // This will also start the xmpp connection.
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testUpdateGroupName {
    [self loadGroupsNotEmpty];
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/groups"] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString* fixture = OHPathForFile(@"updateGroup.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kGroupsServiceDidAddGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *)notification.object;
        if([group.name isEqualToString:@"Colleagues"]){
            XCTAssertTrue([group.name isEqualToString:@"Colleagues"], @"Group must have name Colleagues");
            XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
            XCTAssertTrue(group.users.count == 0, @"We must not have any users in this group");
            XCTAssertTrue(_serviceManager.groupsService.groups.count == 1, @"We must have one group");
            [_serviceManager.groupsService updateGroup:group withNewGroupName:@"Friends"];
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kGroupsServiceDidUpdateGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *) notification.object;
        XCTAssertTrue([group.name isEqualToString:@"Friends"], @"Updated group must have Friends as name");
        XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
        XCTAssertTrue(group.users.count == 0, @"We must not have any users in this group");
        XCTAssertTrue(_serviceManager.groupsService.groups.count == 1, @"We must have one group");
        return YES;
    }];
    
    // This will also start the xmpp connection.
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testUpdateGroupNameOtherDevice {
    NSString *groupID = @"57cd4346d8793a0d124aa21e";
    [self loadGroupsNotEmpty];
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message type='management' id='0' from='admin@mycompany.com' to='%@' xmlns='jabber:client'><group id='%@' action='update' scope='group' xmlns='jabber:iq:configuration' name='Friends' comment='Group with by best friends'/></message>", [self getBare], groupID]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kGroupsServiceDidUpdateGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *) notification.object;
        XCTAssertTrue([group.name isEqualToString:@"Friends"], @"Group name must be Friends");
        XCTAssertTrue([group.comment isEqualToString:@"Group with by best friends"], @"Group name must be Group with by best friends");
        XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
        XCTAssertTrue(_serviceManager.groupsService.groups.count == 1, @"We must have one groups");
        return YES;
    }];
    
    // This will also start the xmpp connection.
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testAddContactInGroup {
    [self loadGroupsNotEmpty];
    NSString *groupID = @"57cd4346d8793a0d124aa21e";
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"%@/%@/users/",@"/groups", groupID]] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString* fixture = OHPathForFile(@"addContactInGroup.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    Contact *contactToAdd = [Contact new];
    RainbowContact *rainbow = [RainbowContact new];
    rainbow.rainbowID = @"5703d6619ccf39843c7ef89d";
    rainbow.jid = @"jid_5703d6619ccf39843c7ef89d@test.com";
    rainbow.vcardPopulated = YES;
    contactToAdd.rainbow = rainbow;
    
    [self expectationForNotification:kGroupsServiceDidAddGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *)notification.object;
        if([group.name isEqualToString:@"Colleagues"]){
            XCTAssertTrue([group.name isEqualToString:@"Colleagues"], @"Group must have name Colleagues");
            XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
            XCTAssertTrue(group.users.count == 0, @"We must not have any users in this group");
            XCTAssertTrue(_serviceManager.groupsService.groups.count == 1, @"We must have one group");
            [_serviceManager.groupsService addContact:contactToAdd inGroup:group];
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kGroupsServiceDidUpdateGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *) notification.object;
        XCTAssertTrue(group.users.count == 1, @"We must have one user in this group");
        Contact *theContact = group.users[0];
        XCTAssertTrue([theContact isEqual:contactToAdd]);
        XCTAssertTrue(_serviceManager.groupsService.groups.count == 1, @"We must have one group");
        return YES;
    }];
    
    // This will also start the xmpp connection.
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testAddContactInGroupOtherDevice {
    NSString *groupID = @"57cd4346d8793a0d124aa21e";
    NSString *userRainbowID = @"123456";
    [self loadGroupsNotEmpty];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"/users/%@", userRainbowID]] && [request.HTTPMethod isEqualToString:@"GET"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"searchByRainbowID123456.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message type='management' id='0' from='admin@mycompany.com' to='%@' xmlns='jabber:client'><group id='%@' action='create' scope='user' userId='%@' xmlns='jabber:iq:configuration'/></message>", [self getBare], groupID, userRainbowID]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kGroupsServiceDidUpdateGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *) notification.object;
        XCTAssertTrue(group.users.count == 1, @"We must have one user in this group");
        XCTAssertTrue(_serviceManager.groupsService.groups.count == 1, @"We must have one group");
        
        return YES;
    }];
    
    // This will also start the xmpp connection.
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testRemoveContactFromGroup {
    [self loadGroupsWithTwoUsers];
    NSString *groupID = @"57cd4346d8793a0d124aa21e";
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:[NSString stringWithFormat:@"%@/%@/users/",@"/groups", groupID]] && [request.HTTPMethod isEqualToString:@"DELETE"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString* fixture = OHPathForFile(@"removeContactFromGroup.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    __block Contact *contactToRemove = nil;
    
    [self expectationForNotification:kGroupsServiceDidAddGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *)notification.object;
        if([group.name isEqualToString:@"Colleagues"]){
            XCTAssertTrue([group.name isEqualToString:@"Colleagues"], @"Group must have name Colleagues");
            XCTAssertTrue([group.owner isEqual:_serviceManager.myUser.contact], @"My user must be the group owner");
            XCTAssertEqual(group.users.count, 2);
            XCTAssertEqual(_serviceManager.groupsService.groups.count, 1);
            
            if ([group.users count] > 0) {
                contactToRemove = [group.users objectAtIndex:0];
                [_serviceManager.groupsService removeContact:contactToRemove fromGroup:group];
                return YES;
            }
        }
        return NO;
    }];
    
    [self expectationForNotification:kGroupsServiceDidUpdateGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *) notification.object;
        XCTAssertTrue(group.users.count == 1, @"We must have no user in this group");
        XCTAssertTrue(_serviceManager.groupsService.groups.count == 1, @"We must have one group");
        return YES;
    }];
    
    // This will also start the xmpp connection.
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

-(void) testRemoveContactFromGroupOtherDevice {
    NSString *groupID = @"57cd4346d8793a0d124aa21e";
    NSString *userRainbowID = @"123456";
    [self loadGroupsNotEmpty];
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        Contact *contact = [Contact new];
        RainbowContact *rainbow = [RainbowContact new];
        rainbow.rainbowID = userRainbowID;
        rainbow.jid = @"test@test.com";
        contact.rainbow = rainbow;
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:[contact dictionaryRepresentation:NO]];
        [dic setObject:rainbow.jid forKey:@"jid_im"];
        [dic setObject:rainbow.rainbowID forKey:@"id"];
        
        [_contactsManagerService createOrUpdateRainbowContactFromJSON:dic];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message type='management' id='0' from='admin@mycompany.com' to='%@' xmlns='jabber:client'><group id='%@' action='delete' scope='user' userId='%@' xmlns='jabber:iq:configuration'/></message>", [self getBare], groupID, userRainbowID]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kGroupsServiceDidUpdateGroup object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Group *group = (Group *) notification.object;
        XCTAssertTrue(group.users.count == 0, @"We must have 0 user in this group");
        XCTAssertTrue(_serviceManager.groupsService.groups.count == 1, @"We must have one group");
        
        return YES;
    }];
    
    // This will also start the xmpp connection.
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}
@end
