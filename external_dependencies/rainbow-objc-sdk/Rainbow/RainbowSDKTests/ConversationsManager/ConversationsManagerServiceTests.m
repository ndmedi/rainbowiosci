/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowSDKTestAbstract.h"
#import "XMPPStub.h"
#import "ConversationsManagerService+Internal.h"
#import "Conversation+Internal.h"

@interface ConversationsManagerServiceTests : RainbowSDKTestAbstract
@end

@implementation ConversationsManagerServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _contactsManagerService.delegate = self;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    _contactsManagerService.delegate = nil;
    [super tearDown];
}

// After startup of application we must have one converstaion retreived from REST api
-(void) testStartGetConversations {
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *theConversation = (Conversation *) notification.object;
        if(theConversation.type == ConversationTypeUser){
            XCTAssertTrue(theConversation.type == ConversationTypeUser, @"We must have one conversation of type user");
            XCTAssertFalse(theConversation.isMuted, @"The conversation must not be muted");
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *theConversation = (Conversation *) notification.object;
        if(theConversation.type == ConversationTypeRoom){
            XCTAssertTrue(theConversation.type == ConversationTypeRoom, @"We must have one conversation of type room");
            XCTAssertFalse(theConversation.isMuted, @"The conversation must not be muted");
            return YES;
        }
        return NO;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to start a conversation with
 *  with one of our contacts
 */
- (void)testStartConversation {
    // Add our custom stubs
//    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
//        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
//    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
//        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
//        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
//    }];
    
    ConversationAPI *conversation1 = [ConversationAPI new];
    conversation1.peerID = @"rainbow_123";
    conversation1.type = @"user";
    conversation1.jid = @"fake_conversations_at_startup@openrainbow.com";
    conversation1.lastMessage = @"Hello There";
    conversation1.lastMessageDate = @"2016-10-11T12:10:03.000Z";
    conversation1.unread = @(1);
    conversation1.mute = NO;
    [self.server addConversation:conversation1];
    
    ContactAPI *contact1 = [ContactAPI new];
    contact1.jid = @"1234@openrainbow.com";
    contact1.firstname = @"FirstName";
    contact1.lastname = @"lastName";
    [self.server addContact:contact1];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            return YES;
        }
        return NO;
    }];
    
    // - We receive once a didAddConversation after didLogin event, so we must treat it
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"fake_conversations_at_startup@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // - we expect a didAddConversation notification because its a new conv
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];

}


/**
 *  The purpose of this test is to start and then stop a conversation with
 *  with one of our contacts
 */
- (void)testStopConversation {
    // Add our custom stubs
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations/fake_conversation_id"] && [request.HTTPMethod isEqualToString:@"DELETE"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSData* fixture = [@"{}" dataUsingEncoding:NSUTF8StringEncoding];
        return [OHHTTPStubsResponse responseWithData:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            return YES;
        }
        return NO;
    }];
    
    // - We receive once a didAddConversation after didLogin event, so we must treat it
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"fake_conversations_at_startup@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // - we expect a didAddConversation notification because its a new conv
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [_conversationsManagerService stopConversation:conversation];
                return YES;
            }
        }
        return NO;
    }];

    // - we expect a didStop conversation.
    [self expectationForNotification:kConversationsManagerDidStopConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // - we expect a didRemove conversation.
    [self expectationForNotification:kConversationsManagerDidRemoveConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to start a conversation and then receive an event
 *  because the conversation has been stopped on another device.
 */
- (void)testRemoteStopConversationEvent {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            return YES;
        }
        return NO;
    }];
    
    // - We receive once a didAddConversation after didLogin event, so we must treat it
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"fake_conversations_at_startup@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // - we expect a didAddConversation notification because its a new conv
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // - we expect a didRemove conversation.
    [self expectationForNotification:kConversationsManagerDidRemoveConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSLog(@"Simulate receiving a management message");
        // Event received when a conversation is closed by another of my devices
        [XMPPStub addIncomingMessage:@"<message type='management' id='8413b42e-563c-4437-9a53-06f638b5ab69_0' from='pcloud@openrainbow.com/172440802160413612281463752830017532' to='85a456023ad249bea7a0cb1d5b4fb34a@openrainbow.com' xmlns='jabber:client'> <conversation id='fake_conversation_id' action='delete' xmlns='jabber:iq:configuration'/></message>"];
    });
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  The purpose of this test is to receive an incoming message
 *  for a contact which is not known.
 *  We can check the contact is correctly created in contactsManagerService
 *  and all the required notifications are posted.
 */
- (void)testReceiveMessageFromUnknownContact {
    // Add our custom stubs for this test :
    // - The client will ask for our unknown-user vCard.
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"iq"][@"type"] isEqualToString:@"get"] &&
            [request[@"iq"][@"vCard"][@"xmlns"] isEqualToString:@"vcard-temp"] &&
            [request[@"iq"][@"to"] isEqualToString:@"unknown_jid@openrainbow.net"]) {
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        return @[[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='%@' type='result'><vCard xmlns='vcard-temp' version='2.0'><N><FAMILY>Doe</FAMILY><GIVEN>John</GIVEN></N><ORG><ORGNAME>AL-Enterprise</ORGNAME></ORG><NICKNAME>John</NICKNAME><ROLE>Wizard</ROLE><TEL><WORK/><VOICE/><NUMBER>+33333333</NUMBER></TEL><EMAIL><WORK/><USERID>john.doe@al-enterprise.com</USERID></EMAIL><FN>John Doe</FN></vCard></iq>", request[@"iq"][@"to"], _myUser.contact.jid, request[@"iq"][@"id"]]];
    }];
    
    
    // - We receive once a didAddConversation after didLogin event, so we must treat it
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"fake_conversations_at_startup@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // Add our custom actions on events :
    // - Once SDK is connected, simulate a receive message from unknown-user
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^(NSNotification *notification) {
        [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='unknown_jid@openrainbow.net/resource' to='%@' type='chat' id='11223344'><archived by='openrainbow.net' xmlns='urn:xmpp:mam:tmp' id='1460552810601722'/><stanza-id by='openrainbow.net' xmlns='urn:xmpp:sid:0' id='1460552810601722'/><body>Hello There !</body><request xmlns='urn:xmpp:receipts'/></message>", _myUser.contact.jid]];
        return YES;
    }];
    
    
    // - The app should call the delegate didAddContact for the new contact
    [self expectationForNotification:GET_NOTIF_NAME(@selector(contactsService:didAddContact:)) object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        if ([contact.jid isEqualToString:@"unknown_jid@openrainbow.net"]) {
            return YES;
        }
        return NO;
    }];
    
    
    // - The app should also trigger the didAddContact notification
    [self expectationForNotification:kContactsManagerServiceDidAddContact object:nil handler:^(NSNotification *notification) {
        Contact *contact = notification.object;
        
        if ([contact.jid isEqualToString:@"unknown_jid@openrainbow.net"]) {
            return YES;
        }
        return NO;
    }];
    
    
    // - The app should say it did added a new conversation
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"unknown_jid@openrainbow.net"]) {
                XCTAssertTrue(conversation.unreadMessagesCount == 1,@"Counter must be set to 1");
                return YES;
            }
        }
        return YES;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  The purpose of this test is to send a chat message to
 *  one of our contacts (which is in our roster).
 *  The XMPPService should send a <message> on the network
 *  to the correct jid, with right content.
 */
- (void)testSendMessageToContactInRoster {
    // Add our custom stubs for this test
    // - when a message is sent to the user, check it
    XCTestExpectation *messageSent = [self expectationWithDescription:@"Chat message sent"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"message"][@"type"] isEqualToString:@"chat"] &&
            [request[@"message"][@"to"] isEqualToString:@"1234@openrainbow.com"]) {
            // Check the message content
            XCTAssert([request[@"message"][@"body"][@"text"] isEqualToString:@"Hi my friend"]);
            
            [messageSent fulfill];
            
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // No need to respond.
        return @[];
    }];
    
    // Add our custom stubs
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            
            static dispatch_once_t onceToken;
            dispatch_once (&onceToken, ^{
                [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            });
            return YES;
        }
        return NO;
    }];
    
    // - We receive once a didAddConversation after didLogin event, so we must treat it
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"fake_conversations_at_startup@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // - we expect a didAddConversation notification because its a new conv
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                return YES;
            }
        }
        return NO;
    }];
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [_conversationsManagerService sendMessage:@"Hi my friend" fileAttachment:nil to:conversation completionHandler:nil attachmentUploadProgressHandler:nil];
                return YES;
            }
        }
        return NO;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testMuteConversation {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/conversations"] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString *conversationID = [request.URL.pathComponents lastObject];
        NSDictionary *jsonData = @{@"data": @{@"id": conversationID, @"mute": @YES, @"type":@"user"}};
        return [OHHTTPStubsResponse responseWithJSONObject:jsonData statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    __block BOOL sendMuteRequest = NO;
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *theConversation = (Conversation *) notification.object;
        if(theConversation.type == ConversationTypeUser){
            XCTAssertTrue(theConversation.type == ConversationTypeUser, @"We must have one conversation of type user");
            XCTAssertFalse(theConversation.isMuted, @"The conversation must not be muted");
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [_conversationsManagerService muteConversation:theConversation];
                sendMuteRequest = YES;
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *theConversation = (Conversation *) notification.object;
        if(theConversation.type == ConversationTypeRoom){
            XCTAssertTrue(theConversation.type == ConversationTypeRoom, @"We must have one conversation of type room");
            XCTAssertFalse(theConversation.isMuted, @"The conversation must not be muted");
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kConversationsManagerDidUpdateConversation object:nil handler:^BOOL(NSNotification * notification) {
        if(sendMuteRequest){
            Conversation *theConversation = (Conversation *) notification.object;
            XCTAssertTrue(theConversation.isMuted,@"This conversation must be muted");
            return YES;
        }
        return NO;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testUnMuteConversation {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path containsString:@"/conversations"] && [request.HTTPMethod isEqualToString:@"PUT"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString *conversationID = [request.URL.pathComponents lastObject];
        NSDictionary *jsonData = @{@"data": @{@"id": conversationID, @"mute": @NO, @"type":@"user"}};
        return [OHHTTPStubsResponse responseWithJSONObject:jsonData statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    __block BOOL sendUnMuteRequest = NO;
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *theConversation = (Conversation *) notification.object;
        // Force conversation to be muted just for the test purpose
        theConversation.isMuted = YES;
        if(theConversation.type == ConversationTypeUser){
            XCTAssertTrue(theConversation.type == ConversationTypeUser, @"We must have one conversation of type user");
            XCTAssertTrue(theConversation.isMuted, @"The conversation must be muted");
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [_conversationsManagerService unmuteConversation:theConversation];
                sendUnMuteRequest = YES;
            });
            
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        Conversation *theConversation = (Conversation *) notification.object;
        if(theConversation.type == ConversationTypeRoom){
            XCTAssertTrue(theConversation.type == ConversationTypeRoom, @"We must have one conversation of type room");
            XCTAssertFalse(theConversation.isMuted, @"The conversation must not be muted");
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kConversationsManagerDidUpdateConversation object:nil handler:^BOOL(NSNotification * notification) {
        if(sendUnMuteRequest){
            Conversation *theConversation = (Conversation *) notification.object;
            XCTAssertFalse(theConversation.isMuted,@"This conversation must be unmuted");
            return YES;
        }
        return NO;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testDidReceiveMuteConversation {
    __block BOOL sendMuteMessage = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud2@openrainbow.com/1234' to='%@' type='management' id='abdc'><mute xmlns='jabber:iq:configuration' conversation='56d0000a0261b53142a5c022'/></message>", [self getFull]]];
            sendMuteMessage = YES;
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kConversationsManagerDidUpdateConversation object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Conversation *theConversation = (Conversation *)notification.object;
        if(sendMuteMessage){
            XCTAssertTrue(theConversation.isMuted,@"This conversation must be muted");
            return YES;
        }
        return NO;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testDidReceiveUnMuteConversation {
    __block BOOL sendUnMuteMessage = NO;
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Conversation *theConversation = (Conversation *) notification.object;
        if([theConversation.conversationId isEqualToString:@"56d0000a0261b53142a5c022"]){
            theConversation.isMuted = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='pcloud2@openrainbow.com/1234' to='%@' type='management' id='abdc'><unmute xmlns='jabber:iq:configuration' conversation='56d0000a0261b53142a5c022'/></message>", [self getFull]]];
                sendUnMuteMessage = YES;
            });
            return YES;
        }
        
        return NO;
    }];
    
    [self expectationForNotification:kConversationsManagerDidUpdateConversation object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        Conversation *theConversation = (Conversation *)notification.object;
        if(sendUnMuteMessage){
            XCTAssertFalse(theConversation.isMuted,@"This conversation must not be muted");
            return YES;
        }
        return NO;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

#pragma mark - XEP-184 ACK messages

/**
 *  We send a message to a contact.
 *  Check we have the ACK request in the outgoing message, and
 *  Check the message is currently marked as :
 *  - non received by server
 *  - non received by client
 *  - non read by server
 */
- (void)testSendMessageWithACKRequest {
    
    // - We send a message, with the request for ACK
    XCTestExpectation *MessageSent = [self expectationWithDescription:@"Chat message with ACK request sent"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"message"][@"type"] isEqualToString:@"chat"] &&
            [request[@"message"][@"to"] isEqualToString:@"1234@openrainbow.com"]) {
            
            XCTAssert([request[@"message"][@"request"][@"xmlns"] isEqualToString:@"urn:xmpp:receipts"]);
            
            [MessageSent fulfill];
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[];
    }];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            return YES;
        }
        return NO;
    }];
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [_conversationsManagerService sendMessage:@"Hi my friend" fileAttachment:nil to:conversation completionHandler:^(Message *message, NSError *error) {
                    XCTAssertEqual(message.state, MessageDeliveryStateSent);
                } attachmentUploadProgressHandler:nil];
                return YES;
            }
        }
        return NO;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We receive a carbon-copy of a message
 *  we sent to one of our contact from another device.
 *
 *  Check the message is currently marked as :
 *  - received by server
 *  - non received by client
 *  - non read by server
 */
- (void)testReceiveCarbonMessageFromAnotherDevice {
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            return YES;
        }
        return NO;
    }];
    
    __block Contact *remoteContact = nil;
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                remoteContact = contact;
                
                [XMPPStub addIncomingMessage:@"<message from='fake_iostester_jid@openrainbow.com' to='fake_iostester_jid@openrainbow.com/mobile_rc_fake' xmlns='jabber:client' type='chat'><received xmlns='urn:xmpp:carbons:2'><forwarded xmlns='urn:xmpp:forward:0'><message from='fake_iostester_jid@openrainbow.com/mobile_rc_fake' to='1234@openrainbow.com' type='chat' id='fake_message_id' xmlns='jabber:client'><body>Hi my friend</body><request xmlns='urn:xmpp:receipts'/></message></forwarded></received></message>"];
                
                return YES;
            }
        }
        return NO;
    }];
    
    // We will receive a did receive new incomming message notification
    [self expectationForNotification:kConversationsManagerDidReceiveCarbonCopyMessage object:nil handler:^BOOL(NSNotification * notification) {
        Message *message = (Message *)notification.object;
        if([message.messageID isEqualToString:@"fake_message_id"]) {
            
            XCTAssertEqual(message.state, MessageDeliveryStateDelivered);
            
            return YES;
        }
        
        return NO;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  We receive a new message from a remote contact.
 *
 *  Check that we automatically send a received-ACK and
 *  that we have 1 un-read message in counter
 */
- (void)testReceiveMessageFromContactAutoSendReceivedACK {

    // - The client send a Received ACK message
    XCTestExpectation *ReceivedMessageSent = [self expectationWithDescription:@"Received ACK message sent"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"message"][@"type"] isEqualToString:@"chat"] &&
            [request[@"message"][@"received"][@"xmlns"] isEqualToString:@"urn:xmpp:receipts"]){
            
            NSDictionary *received = request[@"message"][@"received"];
            XCTAssert([received[@"entity"] isEqualToString:@"client"]);
            XCTAssert([received[@"event"] isEqualToString:@"received"]);
            XCTAssert([received[@"id"] isEqualToString:@"fake_received_message_id"]);
            
            [ReceivedMessageSent fulfill];
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[];
    }];
    
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            return YES;
        }
        return NO;
    }];
    
    // - on the didStartConversation, send a message to the app.
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [XMPPStub addIncomingMessage:@"<message xmlns='jabber:client' from='1234@openrainbow.com/xxx_fake_id' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_received_message_id'><body xmlns='jabber:client'>Hello jerome 2</body><request xmlns='urn:xmpp:receipts'/></message>"];
                return YES;
            }
        }
        return NO;
    }];
    
    // We will receive a did receive new incomming message notification
    [self expectationForNotification:kConversationsManagerDidReceiveNewMessageForConversation object:nil handler:^BOOL(NSNotification * notification) {
        Conversation *conversation = (Conversation *)notification.object;
        if([conversation.lastMessage.messageID isEqualToString:@"fake_received_message_id"]){
            XCTAssertEqual(conversation.unreadMessagesCount, 1);
            return YES;
        }
        
        return NO;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We receive a new message from a remote contact.
 *
 *  Check that we send a read-ACK, when marking the
 *  message as read.
 */
- (void)testReceiveMessageFromContactSendReadACK {
    
    // - The client send a Read ACK message
    XCTestExpectation *ReadMessageSent = [self expectationWithDescription:@"Read ACK message sent"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"message"][@"type"] isEqualToString:@"chat"] &&
            [request[@"message"][@"received"][@"xmlns"] isEqualToString:@"urn:xmpp:receipts"] &&
            [request[@"message"][@"received"][@"event"] isEqualToString:@"read"]){
            
            NSDictionary *received = request[@"message"][@"received"];
            XCTAssert([received[@"entity"] isEqualToString:@"client"]);
            XCTAssert([received[@"event"] isEqualToString:@"read"]);
            XCTAssert([received[@"id"] isEqualToString:@"fake_received_message_id"]);
            
            [ReadMessageSent fulfill];
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[];
    }];
    
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            return YES;
        }
        return NO;
    }];
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [XMPPStub addIncomingMessage:@"<message xmlns='jabber:client' from='1234@openrainbow.com/xxx_fake_id' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_received_message_id'><body xmlns='jabber:client'>Hello jerome 2</body><request xmlns='urn:xmpp:receipts'/></message>"];
                return YES;
            }
        }
        return NO;
    }];
    
    // We will receive a did receive new incomming message notification
    [self expectationForNotification:kConversationsManagerDidReceiveNewMessageForConversation object:nil handler:^BOOL(NSNotification * notification) {
        Conversation *conversation = (Conversation *)notification.object;
        if([conversation.lastMessage.messageID isEqualToString:@"fake_received_message_id"]){
            XCTAssertEqual(conversation.unreadMessagesCount, 1);
            // We can send a ack
            [_conversationsManagerService markAsReadByMeAllMessageForConversation:conversation];
            
            XCTAssertEqual(conversation.unreadMessagesCount, 0);
            return YES;
        }
        
        return NO;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We receive a new carbon-message from a remote contact.
 *
 *  Check that we automatically send a received-ACK and
 *  that we have 1 un-read message in counter
 */
- (void)testReceiveCarbonMessageFromContactAutoSendReceivedACK {
    
    // - The client send a Received ACK message
    XCTestExpectation *ReceivedMessageSent = [self expectationWithDescription:@"Received ACK message sent"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"message"][@"type"] isEqualToString:@"chat"] &&
            [request[@"message"][@"received"][@"xmlns"] isEqualToString:@"urn:xmpp:receipts"]){
            
            NSDictionary *received = request[@"message"][@"received"];
            XCTAssert([received[@"entity"] isEqualToString:@"client"]);
            XCTAssert([received[@"event"] isEqualToString:@"received"]);
            XCTAssert([received[@"id"] isEqualToString:@"fake_received_message_id"]);
            
            [ReceivedMessageSent fulfill];
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[];
    }];
    
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            return YES;
        }
        return NO;
    }];
    
    __block Conversation *conversation = nil;
    // - on the didStartConversation, send a carbon-message to the app.
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [XMPPStub addIncomingMessage:@"<message from='fake_iostester_jid@openrainbow.com' to='fake_iostester_jid@openrainbow.com/mobile_rc_fake' xmlns='jabber:client' type='chat'><received xmlns='urn:xmpp:carbons:2'><forwarded xmlns='urn:xmpp:forward:0'><message from='1234@openrainbow.com/xxx_fake_id' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_received_message_id' xmlns='jabber:client'><body>BONJOUR</body><request xmlns='urn:xmpp:receipts'/></message></forwarded></received></message>"];
                return YES;
            }
        }
        return NO;
    }];
    
    // We will receive a did receive new incomming message notification
    [self expectationForNotification:kConversationsManagerDidReceiveCarbonCopyMessage object:nil handler:^BOOL(NSNotification * notification) {
        Message *message = (Message *)notification.object;
        if([message.messageID isEqualToString:@"fake_received_message_id"]){
            XCTAssertEqual(conversation.unreadMessagesCount, 1);
            return YES;
        }
        
        return NO;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We receive a new carbon-message from a remote contact.
 *
 *  Check that we send a read-ACK, when marking the
 *  message as read.
 */
- (void)testReceiveCarbonMessageFromContactSendReadACK {
    
    // - The client send a Read ACK message
    XCTestExpectation *ReadMessageSent = [self expectationWithDescription:@"Read ACK message sent"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"message"][@"type"] isEqualToString:@"chat"] &&
            [request[@"message"][@"received"][@"xmlns"] isEqualToString:@"urn:xmpp:receipts"] &&
            [request[@"message"][@"received"][@"event"] isEqualToString:@"read"]){
            
            NSDictionary *received = request[@"message"][@"received"];
            XCTAssert([received[@"entity"] isEqualToString:@"client"]);
            XCTAssert([received[@"event"] isEqualToString:@"read"]);
            XCTAssert([received[@"id"] isEqualToString:@"fake_received_message_id"]);
            
            [ReadMessageSent fulfill];
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[];
    }];
    
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            return YES;
        }
        return NO;
    }];
    
    __block Conversation *conversationStarted = nil;
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        conversationStarted = conversation;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [XMPPStub addIncomingMessage:@"<message from='fake_iostester_jid@openrainbow.com' to='fake_iostester_jid@openrainbow.com/mobile_rc_fake' xmlns='jabber:client' type='chat'><received xmlns='urn:xmpp:carbons:2'><forwarded xmlns='urn:xmpp:forward:0'><message from='1234@openrainbow.com/xxx_fake_id' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_received_message_id' xmlns='jabber:client'><body>BONJOUR</body><request xmlns='urn:xmpp:receipts'/></message></forwarded></received></message>"];
                return YES;
            }
        }
        return NO;
    }];
    
    // We will receive a did receive new incomming message notification
    [self expectationForNotification:kConversationsManagerDidReceiveCarbonCopyMessage object:nil handler:^BOOL(NSNotification * notification) {
        Message *message = (Message *)notification.object;
        if([message.messageID isEqualToString:@"fake_received_message_id"]){
            XCTAssertEqual(conversationStarted.unreadMessagesCount, 1);
            // We can send a ack
            [_conversationsManagerService markAsReadByMeAllMessageForConversation:conversationStarted];
            
            XCTAssertEqual(conversationStarted.unreadMessagesCount, 0);
            return YES;
        }
        
        return NO;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We receive a received-by-server ACK for a message we sent.
 *
 *  Check the message is marked as received-by-server and
 *  the correct notification is triggered
 *
 *  NB: this kind of ACK is never carboned, no need to test this case.
 */
- (void)testReceiveReceivedByServerACK {

    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            
            static dispatch_once_t onceToken;
            dispatch_once (&onceToken, ^{
                [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            });
            return YES;
        }
        return NO;
    }];
    
    __block NSString *sentMessageID = nil;
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [_conversationsManagerService sendMessage:@"Hi my friend" fileAttachment:nil to:conversation completionHandler:^(Message *message, NSError *error) {
                    sentMessageID = message.messageID;
                    [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='openrainbow.com' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_rcvd_ack_message_id'><received xmlns='urn:xmpp:receipts' event='received' entity='server' id='%@' /><timestamp xmlns='urn:xmpp:receipts' value='2016-09-23T12:00:01.000000Z'/></message>", sentMessageID]];
                } attachmentUploadProgressHandler:nil];
                return YES;
            }
        }
        return NO;
    }];
    
    [self expectationForNotification:kConversationsManagerDidAckMessageNotification object:nil handler:^(NSNotification *notification) {
        // @{@"messageID": (NSString*) messageID, @"state": (NSNumber*) state, @"datetime": (NSDate*) datetime};
        NSDictionary *userInfo = notification.object;
        
        XCTAssert([[userInfo objectForKey:@"messageID"] isEqualToString:sentMessageID]);
        XCTAssertEqual([[userInfo objectForKey:@"state"] intValue], MessageDeliveryStateDelivered);
        
        return YES;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We receive a received-by-client ACK for a message we sent.
 *
 *  Check the message is marked as received-by-client and
 *  the correct notification is triggered
 */
- (void)testReceiveReceivedByClientACK {

    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            
            static dispatch_once_t onceToken;
            dispatch_once (&onceToken, ^{
                [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            });
            return YES;
        }
        return NO;
    }];
    
    __block NSString *sentMessageID = nil;
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [_conversationsManagerService sendMessage:@"Hi my friend" fileAttachment:nil to:conversation completionHandler:^(Message *message, NSError *error) {
                    sentMessageID = message.messageID;
                    [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='1234@openrainbow.com/xxx_fake_id' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_rcvd_ack_message_id'><received xmlns='urn:xmpp:receipts' event='received' entity='client' id='%@' /><timestamp xmlns='urn:xmpp:receipts' value='2016-09-23T12:00:01.000000Z'/></message>", sentMessageID]];
                } attachmentUploadProgressHandler:nil];
                return YES;
            }
        }
        return NO;
    }];
    
    [self expectationForNotification:kConversationsManagerDidAckMessageNotification object:nil handler:^(NSNotification *notification) {
        // @{@"messageID": (NSString*) messageID, @"state": (NSNumber*) state, @"datetime": (NSDate*) datetime};
        NSDictionary *userInfo = notification.object;
        
        XCTAssert([[userInfo objectForKey:@"messageID"] isEqualToString:sentMessageID]);
        XCTAssertEqual([[userInfo objectForKey:@"state"] intValue], MessageDeliveryStateReceived);
        
        return YES;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We receive a read-by-client ACK for a message we sent.
 *
 *  Check the message is marked as read-by-client and
 *  the correct notification is triggered
 */
- (void)testReceiveReadByClientACK {
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            
            static dispatch_once_t onceToken;
            dispatch_once (&onceToken, ^{
                [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            });
            
            return YES;
        }
        return NO;
    }];
    
    __block NSString *sentMessageID = nil;
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [_conversationsManagerService sendMessage:@"Hi my friend" fileAttachment:nil to:conversation completionHandler:^(Message *message, NSError *error) {
                    sentMessageID = message.messageID;
                    [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='1234@openrainbow.com/xxx_fake_id' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_read_ack_message_id'><received xmlns='urn:xmpp:receipts' event='read' entity='client' id='%@' /><timestamp xmlns='urn:xmpp:receipts' value='2016-09-23T12:00:01.000000Z'/></message>", sentMessageID]];
                } attachmentUploadProgressHandler:nil];
                
                return YES;
            }
        }
        return NO;
    }];
    
    [self expectationForNotification:kConversationsManagerDidAckMessageNotification object:nil handler:^(NSNotification *notification) {
        // @{@"messageID": (NSString*) messageID, @"state": (NSNumber*) state, @"datetime": (NSDate*) datetime};
        NSDictionary *userInfo = notification.object;
        
        XCTAssert([[userInfo objectForKey:@"messageID"] isEqualToString:sentMessageID]);
        XCTAssertEqual([[userInfo objectForKey:@"state"] intValue], MessageDeliveryStateRead);
        
        return YES;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We receive a read-by-client ACK for a message we sent -> status is read.
 *  We then receive a received-by-client ACK (from a late other device) -> status remains read !
 */
- (void)testReceiveReadByClientACKThenReReceived {
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            
            static dispatch_once_t onceToken;
            dispatch_once (&onceToken, ^{
                [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            });
            
            return YES;
        }
        return NO;
    }];
    
    __block NSString *sentMessageID = nil;
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [_conversationsManagerService sendMessage:@"Hi my friend" fileAttachment:nil to:conversation completionHandler:^(Message *message, NSError *error) {
                    sentMessageID = message.messageID;
                    [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='1234@openrainbow.com/xxx_fake_id' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_read_ack_message_id'><received xmlns='urn:xmpp:receipts' event='read' entity='client' id='%@' /><timestamp xmlns='urn:xmpp:receipts' value='2016-09-23T12:00:00.000000Z'/></message>", sentMessageID]];
                } attachmentUploadProgressHandler:nil];
                
                return YES;
            }
        }
        return NO;
    }];
    
    __block BOOL firsttime = YES;
    
    [self expectationForNotification:kConversationsManagerDidAckMessageNotification object:nil handler:^(NSNotification *notification) {
        // @{@"messageID": (NSString*) messageID, @"state": (NSNumber*) state, @"datetime": (NSDate*) datetime};
        NSDictionary *userInfo = notification.object;
        
        if (firsttime) {
            XCTAssert([[userInfo objectForKey:@"messageID"] isEqualToString:sentMessageID]);
            XCTAssertEqual([[userInfo objectForKey:@"state"] intValue], MessageDeliveryStateRead);
            
            // Then we receive a received-by-client ack from a late other device.
            [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message xmlns='jabber:client' from='1234@openrainbow.com/xxx_fake_id' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_read_ack_message_id'><received xmlns='urn:xmpp:receipts' event='received' entity='client' id='%@' /><timestamp xmlns='urn:xmpp:receipts' value='2016-09-23T13:00:00.000000Z'/></message>", sentMessageID]];
            firsttime = NO;
            return NO;
        } else {
            // The notification is posted with the received state -> it's normal
            // It informs of what happened.
            XCTAssert([[userInfo objectForKey:@"messageID"] isEqualToString:sentMessageID]);
            XCTAssertEqual([[userInfo objectForKey:@"state"] intValue], MessageDeliveryStateReceived);
            return YES;
        }
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We receive a received-by-client *CARBON* ACK for a message we sent.
 *
 *  Check the message is marked as received-by-client and
 *  the correct notification is triggered
 */
- (void)testReceiveCarbonReceivedByClientACK {
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            
            static dispatch_once_t onceToken;
            dispatch_once (&onceToken, ^{
                [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            });
            return YES;
        }
        return NO;
    }];
    
    __block NSString *sentMessageID = nil;
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [_conversationsManagerService sendMessage:@"Hi my friend" fileAttachment:nil to:conversation completionHandler:^(Message *message, NSError *error) {
                    sentMessageID = message.messageID;
                    [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message from='fake_iostester_jid@openrainbow.com' to='fake_iostester_jid@openrainbow.com/mobile_rc_fake' xmlns='jabber:client' type='chat'><received xmlns='urn:xmpp:carbons:2'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='1234@openrainbow.com/xxx_fake_id' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_rcvd_ack_message_id'><received xmlns='urn:xmpp:receipts' event='received' entity='client' id='%@' /><timestamp xmlns='urn:xmpp:receipts' value='2016-09-23T12:00:01.000000Z'/></message></forwarded></received></message>", sentMessageID]];
                } attachmentUploadProgressHandler:nil];
                
                return YES;
            }
        }
        return NO;
    }];
    
    [self expectationForNotification:kConversationsManagerDidAckMessageNotification object:nil handler:^(NSNotification *notification) {
        // @{@"messageID": (NSString*) messageID, @"state": (NSNumber*) state, @"datetime": (NSDate*) datetime};
        NSDictionary *userInfo = notification.object;
        
        XCTAssert([[userInfo objectForKey:@"messageID"] isEqualToString:sentMessageID]);
        XCTAssertEqual([[userInfo objectForKey:@"state"] intValue], MessageDeliveryStateReceived);
        
        return YES;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We receive a read-by-client *CARBON* ACK for a message we sent.
 *
 *  Check the message is marked as read-by-client and
 *  the correct notification is triggered
 */
- (void)testReceiveCarbonReadByClientACK {
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            
            static dispatch_once_t onceToken;
            dispatch_once (&onceToken, ^{
                [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            });
            return YES;
        }
        return NO;
    }];
    
    __block NSString *sentMessageID = nil;
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                [_conversationsManagerService sendMessage:@"Hi my friend" fileAttachment:nil to:conversation completionHandler:^(Message *message, NSError *error) {
                    sentMessageID = message.messageID;
                    [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<message from='fake_iostester_jid@openrainbow.com' to='fake_iostester_jid@openrainbow.com/mobile_rc_fake' xmlns='jabber:client' type='chat'><received xmlns='urn:xmpp:carbons:2'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='1234@openrainbow.com/xxx_fake_id' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_read_ack_message_id'><received xmlns='urn:xmpp:receipts' event='read' entity='client' id='%@' /><timestamp xmlns='urn:xmpp:receipts' value='2016-09-23T12:00:01.000000Z'/></message></forwarded></received></message>", sentMessageID]];
                } attachmentUploadProgressHandler:nil];
                
                return YES;
            }
        }
        return NO;
    }];
    
    [self expectationForNotification:kConversationsManagerDidAckMessageNotification object:nil handler:^(NSNotification *notification) {
        // @{@"messageID": (NSString*) messageID, @"state": (NSNumber*) state, @"datetime": (NSDate*) datetime};
        NSDictionary *userInfo = notification.object;
        
        XCTAssert([[userInfo objectForKey:@"messageID"] isEqualToString:sentMessageID]);
        XCTAssertEqual([[userInfo objectForKey:@"state"] intValue], MessageDeliveryStateRead);
        
        return YES;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We receive a read-by-me *CARBON* ACK that has been sent by ME on another device.
 *  i.e. when I read a message on another of my devices.
 *
 *  Check the un-read count is decreased and
 *  the correct notification is triggered
 */
- (void)testReceiveCarbonReadByMeACK {

    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            
            static dispatch_once_t onceToken;
            dispatch_once (&onceToken, ^{
                [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            });
            
            return YES;
        }
        return NO;
    }];
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if (conversation.type == ConversationTypeUser) {
            Contact *contact = (Contact *) conversation.peer;
            if ([contact.jid isEqualToString:@"1234@openrainbow.com"]) {
                // First add a message from remote contact
                [XMPPStub addIncomingMessage:@"<message xmlns='jabber:client' from='1234@openrainbow.com/xxx_fake_id' to='fake_iostester_jid@openrainbow.com' type='chat' id='fake_received_message_id'><body xmlns='jabber:client'>Hello jerome 2</body><request xmlns='urn:xmpp:receipts'/></message>"];
                
                // then receive a readACK from another of my device.
                [XMPPStub addIncomingMessage:@"<message from='fake_iostester_jid@openrainbow.com' to='fake_iostester_jid@openrainbow.com/mobile_rc_fake' xmlns='jabber:client' type='chat'><received xmlns='urn:xmpp:carbons:2'><forwarded xmlns='urn:xmpp:forward:0'><message xmlns='jabber:client' from='fake_iostester_jid@openrainbow.com/mobile_rc_fake' to='1234@openrainbow.com' type='chat' id='fake_read_ack_message_id'><received xmlns='urn:xmpp:receipts' event='read' entity='client' id='fake_received_message_id' /><timestamp xmlns='urn:xmpp:receipts' value='2016-09-23T12:00:01.000000Z'/></message></forwarded></received></message>"];
                return YES;
            }
        }
        return NO;
    }];
    
    // This notification will be triggered 2 times.
    // the first time to say, we have 1 unread message, the second time to say we have no more unread messages.
    [self expectationForNotification:kConversationsManagerDidUpdateMessagesUnreadCount object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if ([conversation.peer.jid isEqualToString:@"1234@openrainbow.com"] && conversation.unreadMessagesCount == 1) {
            XCTAssertEqual(conversation.type, ConversationTypeUser);
            return YES;
        }
        return NO;
    }];
    [self expectationForNotification:kConversationsManagerDidUpdateMessagesUnreadCount object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        if ([conversation.peer.jid isEqualToString:@"1234@openrainbow.com"] && conversation.unreadMessagesCount == 0) {
            XCTAssertEqual(conversation.type, ConversationTypeUser);
            return YES;
        }
        return NO;
    }];
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


#pragma mark - XEP-085 chat-state tests

/**
 *  We change the status of the conversation to active
 *
 *  Check the chat-state right message is sent
 */
- (void)testSendActiveChatState {
    
    // - The client send a chat-state active message
    XCTestExpectation *ChatStateMessageSent = [self expectationWithDescription:@"Chat state message sent"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"message"][@"type"] isEqualToString:@"chat"] &&
            [request[@"message"][@"active"][@"xmlns"] isEqualToString:@"http://jabber.org/protocol/chatstates"]){
            
            [ChatStateMessageSent fulfill];
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[];
    }];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            return YES;
        }
        return NO;
    }];
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        
        [_conversationsManagerService setStatus:ConversationStatusActive forConversation:conversation];
        // We call it 2 times, but expect only 1 outgoing message.
        // because the SDK don't send the message if the status has not changed
        [_conversationsManagerService setStatus:ConversationStatusActive forConversation:conversation];
        return YES;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

/**
 *  We change the status of the conversation to composing
 *
 *  Check the chat-state right message is sent
 */
- (void)testSendComposingChatState {
    
    // - The client send a chat-state active message
    XCTestExpectation *ChatStateMessageSent = [self expectationWithDescription:@"Chat state message sent"];
    [XMPPStub stubRequestsPassingTest:^BOOL(NSDictionary *request) {
        if ([request[@"message"][@"type"] isEqualToString:@"chat"] &&
            [request[@"message"][@"composing"][@"xmlns"] isEqualToString:@"http://jabber.org/protocol/chatstates"]){
            
            [ChatStateMessageSent fulfill];
            return YES;
        }
        return NO;
    } withStubResponse:^NSArray<NSString*>*(NSDictionary *request) {
        // no need to answer.
        return @[];
    }];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * request) {
        return [request.URL.host isEqualToString:@"openrainbow.com"] && [request.URL.path hasSuffix:@"/conversations"] && [request.HTTPMethod isEqualToString:@"POST"];
    } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest * request) {
        NSString* fixture = OHPathForFile(@"success_create_conversation.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    // Add our custom actions on events :
    // - Once the user is ready, start a conversation with him.
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^(NSNotification *notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        if ([contact.jid isEqualToString:@"1234@openrainbow.com"] && [changedKeys containsObject:@"vcardPopulated"]) {
            [_conversationsManagerService startConversationWithPeer:contact withCompletionHandler:nil];
            return YES;
        }
        return NO;
    }];
    
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidStartConversation object:nil handler:^(NSNotification *notification) {
        Conversation *conversation = notification.object;
        
        [_conversationsManagerService setStatus:ConversationStatusComposing forConversation:conversation];
        // We call it 2 times, but expect only 1 outgoing message.
        // because the SDK don't send the message if the status has not changed
        [_conversationsManagerService setStatus:ConversationStatusComposing forConversation:conversation];
        return YES;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testReceiveComposingMessageAfterReconnection {
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        // then receive a readACK from another of my device.
        [XMPPStub addIncomingMessage:@"<message xmlns='jabber:client' from='remote@domain.com/resource' to='fake_iostester_jid@openrainbow.com' type='chat'><composing xmlns='http://jabber.org/protocol/chatstates'/><delay xmlns='urn:xmpp:delay' from='domain.com' stamp='2016-09-13T14:05:24.184Z'>Resent</delay></message>"];
        
        return YES;
    }];
    
    [self expectationForNotification:kConversationsManagerDidReceiveComposingMessage object:nil handler:^BOOL(NSNotification * notification) {
        Message *message = (Message *) notification.object;
        NSLog(@"Message %@", message);
        XCTAssertEqual(message.type, MessageTypeChat);
        XCTAssertFalse(message.isOutgoing);
        XCTAssertTrue([message.peer.jid isEqualToString:@"remote@domain.com"]);
        XCTAssertTrue([message.via.jid isEqualToString:@"remote@domain.com"]);
        XCTAssertTrue(message.isComposing);
        return YES;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testReceiveNewMessageAfterReconnection {
    // - we also expect a didStartConversation notification
    [self expectationForNotification:kConversationsManagerDidAddConversation object:nil handler:^(NSNotification *notification) {
        // then receive a readACK from another of my device.
        [XMPPStub addIncomingMessage:@"<message xmlns='jabber:client' from='fake_conversations_at_startup@openrainbow.com/ressource' to='fake_iostester_jid@openrainbow.com' type='chat' id='web_T9ws9ddiCBAE49'><archived xmlns='urn:xmpp:mam:tmp' by='openrainbow.net' id='1473775534702146'/><stanza-id xmlns='urn:xmpp:sid:0' by='openrainbow.net' id='1473775534702146'/><body xmlns='jabber:client'>je t'envoie un message lors que tu es hors connexion</body><request xmlns='urn:xmpp:receipts'/><active xmlns='http://jabber.org/protocol/chatstates'/><delay xmlns='urn:xmpp:delay' from='openrainbow.net' stamp='2016-09-13T14:05:34.709Z'>Resent</delay></message>"];
        
        return YES;
    }];
    
    [self expectationForNotification:kConversationsManagerDidReceiveNewMessage object:nil handler:^BOOL(NSNotification * notification) {
        Message *message = (Message *) notification.object;
        NSLog(@"Message %@", message);
        XCTAssertEqual(message.type, MessageTypeChat);
        XCTAssertFalse(message.isOutgoing);
        XCTAssertTrue([message.peer.jid isEqualToString:@"fake_conversations_at_startup@openrainbow.com"]);
        XCTAssertTrue([message.via.jid isEqualToString:@"fake_conversations_at_startup@openrainbow.com"]);
        XCTAssertFalse(message.isComposing);
        XCTAssertTrue([message.body isEqualToString:@"je t'envoie un message lors que tu es hors connexion"]);
        return YES;
    }];
    
    
    // Start the service
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:35.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

@end
