/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


#import "GenericMaciOS.h"

#if (TARGET_OS_MAC && !(TARGET_OS_EMBEDDED || TARGET_OS_IPHONE))
CFIndex ABMultiValueGetCount (ABMultiValueRef multiValue) {
    return ABMultiValueCount(multiValue);
}

CFStringRef ABRecordGetRecordID(ABRecordRef record) {
    CFStringRef recordid = ABRecordCopyUniqueId(record);
    CFAutorelease(recordid);
    return recordid;
}
#endif
