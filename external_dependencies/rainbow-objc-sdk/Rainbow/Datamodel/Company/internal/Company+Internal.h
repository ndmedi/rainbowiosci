/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "Company.h"

@interface Company ()

@property (nonatomic, strong) NSString *companyId;
@property (nonatomic, strong, readwrite) NSString *adminEmail;
@property (nonatomic, strong, readwrite) NSString *name;
@property (nonatomic, strong, readwrite) NSString *country;
@property (nonatomic, strong, readwrite) NSData *logo;
@property (nonatomic, strong, readwrite) NSData *banner;
@property (nonatomic, strong, readwrite) NSString *websiteURL;
@property (nonatomic, strong, readwrite) NSString *supportEmail;
@property (nonatomic, strong, readwrite) NSString *companyDescription;
@property (nonatomic, strong, readwrite) NSString *companySize;
@property (nonatomic, strong, readwrite) NSString *slogan;
@property (nonatomic, strong, readwrite) NSDate *lastAvatarUpdateDate;
@property (nonatomic, strong, readwrite) NSDate *lastBannerUpdateDate;
@property (nonatomic, strong, readwrite) NSString *economicActivityClassification;
@property (nonatomic, strong, readwrite) Contact *companyContact;
@property (nonatomic, readwrite) BOOL alreadyRequestToJoin;
@property (nonatomic, readwrite) BOOL logoIsCircle;
@end
