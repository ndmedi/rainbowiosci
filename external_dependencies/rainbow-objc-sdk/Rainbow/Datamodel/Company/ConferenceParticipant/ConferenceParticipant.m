/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConferenceParticipant.h"
#import "ConferenceParticipant+Internal.h"
#import "ServicesManager.h"
#import "ContactsManagerService.h"
#import "ContactsManagerService+Internal.h"
#import "Tools.h"

@implementation ConferenceParticipant

-(void) updateParticipantWithNewParticipant:(ConferenceParticipant *) newParticipant {
    if(_role != newParticipant.role){
        _role = newParticipant.role;
    }
    if(![_phoneNumber isEqualToString:newParticipant.phoneNumber]){
        _phoneNumber = newParticipant.phoneNumber;
    }
    if(_state != newParticipant.state){
        _state = newParticipant.state;
    }
    if(_muted != newParticipant.muted){
        _muted = newParticipant.muted;
    }
    if(_hold != newParticipant.hold){
        _hold = newParticipant.hold;
    }
}

-(BOOL) isEqual:(ConferenceParticipant *)object {
    return [_contact isEqual:object.contact];
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"Conference participant %p : <ID %@ Contact %@ phone number %@ Role %@ state %@ muted %@ hold %@ isTalking %@>", self, _participantId, _contact, _phoneNumber, [ConferenceParticipant stringFromParticipantRole:_role], [ConferenceParticipant stringFromParticipantState:_state], NSStringFromBOOL(_muted), NSStringFromBOOL(_hold), NSStringFromBOOL(_isTalking)];
    }
}

-(NSString *) debugDescription {
    @synchronized(self){
        return [NSString stringWithFormat:@"Conference participant %p : <ID %@ Contact %@ phone number %@ Role %@ state %@ muted %@ hold %@ isTalking %@>", self, _participantId, [_contact debugDescription], _phoneNumber, [ConferenceParticipant stringFromParticipantRole:_role], [ConferenceParticipant stringFromParticipantState:_state], NSStringFromBOOL(_muted), NSStringFromBOOL(_hold), NSStringFromBOOL(_isTalking)];
    }
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary new];
    if(_participantId)
        [dic setObject:_participantId forKey:@"id"];
    if(_phoneNumber)
        [dic setObject:_phoneNumber forKey:@"phoneNumber"];
    if(_jidIM)
        [dic setObject:_jidIM forKey:@"jidIM"];
    
    [dic setObject:[ConferenceParticipant stringFromParticipantState:_state] forKey:@"state"];
    [dic setObject:[ConferenceParticipant stringFromParticipantRole:_role] forKey:@"role"];
    [dic setObject:[NSNumber numberWithBool:_muted] forKey:@"muted"];
    [dic setObject:[NSNumber numberWithBool:_hold] forKey:@"hold"];
    [dic setObject:[NSNumber numberWithBool:_isTalking] forKey:@"isTalking"];
    
    return dic;
}

+(ParticipantRole) participantRoleFromNSString:(NSString *)roleStr{
    return [roleStr isEqualToString:@"moderator"] ? ParticipantRoleModerator : ParticipantRoleMember;
}

+(ParticipantState) participantStateFromNSString:(NSString *)stateStr {
    if([stateStr isEqualToString:@"ringing"])
        return ParticipantStateRinging;
    else if ([stateStr isEqualToString:@"connected"])
        return ParticipantStateConnected;
    else if ([stateStr isEqualToString:@"disconnected"])
        return ParticipantStateDisconnected;
    else
        return ParticipantStateUnknown;
}

+(NSString *) stringFromParticipantRole:(ParticipantRole) participantRole {
    switch (participantRole) {
        case ParticipantRoleModerator:
            return @"moderator";
            break;
        case ParticipantRoleMember:
            return @"member";
        default:
            return @"unknown";
    }
}

+(NSString *) stringFromParticipantState:(ParticipantState) participantState {
    switch (participantState) {
        case ParticipantStateRinging:
            return @"ringing";
            break;
        case ParticipantStateConnected:
            return @"connected";
        case ParticipantStateDisconnected:
            return @"disconnected";
        case ParticipantStateUnknown:
        default:
            return @"unknown";
            break;
    }
}

@end
