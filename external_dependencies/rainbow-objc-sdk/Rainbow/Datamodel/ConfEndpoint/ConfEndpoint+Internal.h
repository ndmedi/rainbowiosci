/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


@interface ConfEndpoint ()
@property (nonatomic, strong) NSString *userId;
@property (nonatomic) ConferenceEndPointMediaType mediaType;
@property (nonatomic, strong) NSString *providerUserId;
@property (nonatomic, strong) NSString *confUserId;
@property (nonatomic, strong) NSString *providerType;
@property (nonatomic, strong) NSString *confEndpointId;
@property (nonatomic, strong) NSString *providerConfId;
@property (nonatomic, strong) NSString *companyId;
@property (nonatomic, readwrite) BOOL isScheduled;
@property (nonatomic, readwrite) BOOL isDialOutDisabled;

@property (nonatomic, readwrite, strong) NSString *attachedRoomID;
@property (nonatomic, readwrite, strong) NSMutableArray<PhoneNumber *> *phoneNumbers;

@property (nonatomic, readwrite, strong) NSString *participantCode;
@property (nonatomic, readwrite, strong) NSString *moderatorCode;
@property (nonatomic, readwrite, strong) NSString *listenOnlyCode;

+(ConferenceEndPointMediaType) conferenceEndPointMediaTypeFromString:(NSString *) mediaType;
+(NSString *) stringFromConferenceEndPointMediaType:(ConferenceEndPointMediaType) mediaType;

-(NSDictionary *) dictionaryRepresentation;
@end
