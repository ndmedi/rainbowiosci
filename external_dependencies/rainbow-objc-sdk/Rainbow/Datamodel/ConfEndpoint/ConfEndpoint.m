/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConfEndpoint.h"
#import "ConfEndpoint+Internal.h"
#import "Tools.h"

@implementation ConfEndpoint

-(instancetype)init {
    self = [super init];
    if(self){
        _phoneNumbers = [NSMutableArray array];
    }
    return self;
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"%p : id %@ isScheduled %@ attachedToRoom %@ mediaType %@ userId %@", self, _confEndpointId, NSStringFromBOOL(_isScheduled), _attachedRoomID, [ConfEndpoint stringFromConferenceEndPointMediaType:_mediaType], _userId];
    }
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setObject:[ConfEndpoint stringFromConferenceEndPointMediaType:_mediaType] forKey:@"mediaType"];
    if(_confEndpointId)
        [dic setObject:_confEndpointId forKey:@"confEndpointId"];
    if(_userId)
        [dic setObject:_userId forKey:@"userId"];
    [dic setObject:[NSNumber numberWithBool:_isScheduled] forKey:@"scheduled"];
    return dic;
}

+(ConferenceEndPointMediaType) conferenceEndPointMediaTypeFromString:(NSString *) mediaType {
    if([mediaType isEqualToString:@"pstnAudio"])
        return ConferenceEndPointMediaTypePSTNAudio;
    else if ([mediaType isEqualToString:@"webrtc"])
        return ConferenceEndPointMediaTypeWebRTC;
    else if ([mediaType isEqualToString:@"webrtcSharingOnly"])
        return ConferenceEndPointMediaTypeWebRTCSharingOnly;
    else
        return ConferenceEndPointMediaTypeUnknown;
}

+(NSString *) stringFromConferenceEndPointMediaType:(ConferenceEndPointMediaType) mediaType {
    switch (mediaType) {
        case ConferenceEndPointMediaTypeWebRTC:
            return @"webrtc";
            break;
        case ConferenceEndPointMediaTypeWebRTCSharingOnly:
            return @"webrtcSharingOnly";
            break;
        case ConferenceEndPointMediaTypePSTNAudio:
            return @"pstnAudio";
        case ConferenceEndPointMediaTypeUnknown:
        default:
            return @"";
            break;
    }
}

@end
