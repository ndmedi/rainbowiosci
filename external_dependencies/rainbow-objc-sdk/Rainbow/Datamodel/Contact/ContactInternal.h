/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Contact.h"
#import "RainbowContact.h"
#import "LocalContact.h"

@interface Contact ()

// A contact is _really_ composed of a rainbow contact AND/OR multiple local contacts.
// All the other public properies are just stubs which returns the values
// of the previous rainbow or local contact values.

/**
 *  The real rainbow part of this contact
 */
@property (nonatomic, strong) RainbowContact *rainbow;

/**
 *  The real local parts of this contact
 */
@property (nonatomic, strong, readonly) NSArray<LocalContact *> *locals;

@property (nonatomic, readonly) NSString *externalObjectID;


// To be able to propagate the KVO changes of local contacts,
// we have to handle insertion ourself.
// This override the addObject:
-(void) addLocalContact:(LocalContact *) localContact;
// This creates some moveContact function
-(void) transfertLocalsFrom:(Contact *) otherContact;
// This override the removeObject:
-(void) removeLocalContact:(LocalContact *) localContact;
// This override the removeAllObjects
-(void) removeAllLocalContacts;

@end
