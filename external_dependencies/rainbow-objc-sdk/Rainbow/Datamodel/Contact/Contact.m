/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Contact.h"
#import "ContactInternal.h"
#import "Message.h"
#import "EmailAddress.h"
#import "Tools.h"
#import "NSDictionary+JSONString.h"
#import "NSArray+JSONString.h"
#import "NSDate+JSONString.h"
#import "ExternalContact.h"

@implementation Contact

-(instancetype) init {
    if (self = [super init]) {
        _locals = [NSMutableArray array];
    }
    return self;
}

-(void) dealloc {
    [self removeKVOForObject:_rainbow];
    _rainbow = nil;
    [self removeAllLocalContacts];
    _locals = nil;
}


#pragma mark - GenericContact stubs getters

// All the following getters are returning data
// from the local OR the rainbow contact.
// Most of the time, the local contact has priority

-(NSData *) photoData {
    @synchronized(self){
        if(self.rainbow.userPhoto.photoData)
            return self.rainbow.userPhoto.photoData;
        else
            for (LocalContact *local in self.locals) {
                if (local.userPhoto.photoData)
                    return local.userPhoto.photoData;
            }
    }
    return nil;
}

-(NSString *) firstName {
    if(self.rainbow.firstName.length)
        return self.rainbow.firstName;
    else {
        for (LocalContact *local in self.locals) {
            if ([local.firstName length])
                return local.firstName;
        }
    }
    return nil;
}

-(NSString *) lastName {
    if(self.rainbow.lastName.length)
        return self.rainbow.lastName;
    else {
        for (LocalContact *local in self.locals) {
            if ([local.lastName length])
                return local.lastName;
        }
    }
    return nil;
}

-(NSString *) nickName {
    @synchronized (self) {
        if(self.rainbow.nickName.length){
            return self.rainbow.nickName;
        } else {
            for (LocalContact *local in self.locals) {
                if ([local.nickName length])
                    return local.nickName;
            }
        }
    }
    return nil;
}

-(NSArray<PhoneNumber *> *) phoneNumbers {
    @synchronized (self) {
        // For arrays, we can return an array
        // Containing both the Local contact and Rainbow contact phoneNumbers.
        NSMutableSet *set = [NSMutableSet set];
        for (LocalContact *local in self.locals) {
            if (local.phoneNumbers)
                [set addObjectsFromArray:local.phoneNumbers];
        }
        if (self.rainbow.phoneNumbers)
            [set addObjectsFromArray:self.rainbow.phoneNumbers];
        return [set allObjects];
    }
}

-(NSArray<EmailAddress *> *) emailAddresses {
    @synchronized (self) {
        // For arrays, we can return an array
        // Containing both the Local contact and Rainbow contact emailAddresses.
        NSMutableSet *set = [NSMutableSet set];
        for (LocalContact *local in self.locals) {
            if (local.emailAddresses)
                [set addObjectsFromArray:local.emailAddresses];
        }
        if (self.rainbow.emailAddresses)
            [set addObjectsFromArray:self.rainbow.emailAddresses];
        return [set allObjects];
    }
}

-(NSString *) title {
    @synchronized (self) {
        if(self.rainbow.title.length)
            return self.rainbow.title;
        else {
            for (LocalContact *local in self.locals) {
                if ([local.title length])
                    return local.title;
            }
        }
    }
    return nil;
}

-(NSString *) jobTitle {
    @synchronized (self) {
        if(self.rainbow.jobTitle.length)
            return self.rainbow.jobTitle;
        else {
            for (LocalContact *local in self.locals) {
                if ([local.jobTitle length])
                    return local.jobTitle;
            }
        }
    }
    return nil;
}

-(NSTimeZone *) timeZone {
    @synchronized (self) {
        if(self.rainbow.timeZone){
            return self.rainbow.timeZone;
        } else {
            for (LocalContact *local in self.locals) {
                if (local.timeZone)
                    return local.timeZone;
            }
        }
    }
    return nil;
}

-(NSString *) companyId {
    @synchronized (self) {
        if(self.rainbow.companyId.length)
            return self.rainbow.companyId;
        else {
            for (LocalContact *local in self.locals) {
                if ([local.companyId length])
                    return local.companyId;
            }
        }
    }
    return nil;
}

-(NSString *) companyName {
    if(self.rainbow.companyName.length)
        return self.rainbow.companyName;
    else {
        for (LocalContact *local in self.locals) {
            if ([local.companyName length])
                return local.companyName;
        }
    }
    return nil;
}

-(NSString *) rtcJid {
    return self.jid;
}

#pragma mark - LocalContact stubs getters

-(NSArray<PostalAddress*> *) addresses {
    @synchronized (self) {
        // For arrays, we can return an array
        // Containing both the Local contact and Rainbow contact addresses.
        NSMutableSet *set = [NSMutableSet set];
        for (LocalContact *local in self.locals) {
            if (local.addresses)
                [set addObjectsFromArray:local.addresses];
        }
        // Rainbow does not handle postal addresses yet.
        return [set allObjects];
    }
}

-(NSArray<NSString *> *) webSitesURL {
    @synchronized (self) {
        NSMutableSet *set = [NSMutableSet set];
        for (LocalContact *local in self.locals) {
            if (local.webSitesURL)
                [set addObjectsFromArray:local.webSitesURL];
        }
        // Rainbow does not handle websites yet.
        return [set allObjects];
    }
}


#pragma mark - RainbowContact stubs getters

-(BOOL) vcardPopulated {
    return self.rainbow.vcardPopulated;
}

-(NSString *) jid {
    return self.rainbow.jid;
}

-(NSString *) jid_tel {
    return self.rainbow.jid_tel;
}

-(NSString *) rainbowID {
    return self.rainbow.rainbowID;
}

-(BOOL) isInRoster {
    return self.rainbow.isInRoster;
}

-(BOOL) isPresenceSubscribed {
    return self.rainbow.isPresenceSubscribed;
}

-(Invitation *) requestedInvitation {
    return self.rainbow.requestedInvitation;
}

-(Invitation *) sentInvitation {
    return self.rainbow.sentInvitation;
}

-(CompanyInvitation *) companyInvitation {
    return self.rainbow.companyInvitation;
}

-(NSArray<NSString *> *) groups {
    return self.rainbow.groups;
}

-(Presence *) presence {
    return self.rainbow.presence;
}

-(CalendarPresence *) calendarPresence {
    return self.rainbow.calendarPresence;
}

-(BOOL) isConnectedWithMobile {
    return self.rainbow.isConnectedWithMobile;
}

-(BOOL) hasPBXAccess {
    return self.rainbow.hasPBXAccess;
}

-(NSDate *) lastUpdateDate {
    return self.rainbow.lastUpdateDate;
}

-(BOOL) isBot {
    return self.rainbow.isBot;
}

-(BOOL) isTerminated {
    return self.rainbow.isTerminated;
}

-(NSDate *) lastActivityDate {
    return self.rainbow.lastActivityDate;
}


#pragma mark - Easy-to-use computed properties

// Default implementation for display name of contact
-(NSString *) displayName {
    return self.fullName;
}

-(NSString *) fullName {
    @synchronized (self) {
        NSMutableString *format = [[NSMutableString alloc] init];
        NSInteger nbParams = 0;
        if(self.firstName) {
            [format appendString:@"%@"];
            nbParams++;
        }
        if(self.firstName && self.lastName)
            [format appendString:@" "];
        if(self.lastName){
            [format appendString:@"%@"];
            nbParams++;
        }
        
        if(nbParams == 1) {
            if(self.firstName)
                return [NSString stringWithFormat:format, [self.firstName capitalizedString]];
            else
                return [NSString stringWithFormat:format, [self.lastName uppercaseString]];
        } else {
            return [NSString stringWithFormat:format, [self.firstName capitalizedString], [self.lastName uppercaseString]];
        }
    }
}

-(BOOL) isRainbowUser {
    return self.rainbow && self.rainbow.jid ? YES : NO;
}

-(BOOL) isInvitedUser {
    return [self.rainbow.rainbowID hasPrefix:@"tmp_invited"] ? YES : NO;
}

-(BOOL) canChatWith {
    if(self.rainbow.sentInvitation)
        if((self.rainbow.sentInvitation.status != InvitationStatusAccepted && self.rainbow.sentInvitation.status != InvitationStatusAutoAccepted))
            return NO;
    
    if(self.rainbow.requestedInvitation)
        if(self.rainbow.requestedInvitation.status != InvitationStatusAccepted && self.rainbow.requestedInvitation.status != InvitationStatusAutoAccepted)
            return NO;
    
    if(self.rainbowID == nil )
        return NO;
    
    return [self.rainbow.jid length] > 0;
}

-(BOOL) isVisible {
    // Local contacts are always visible.
    @synchronized (self) {
        if ([self.locals count])
            return YES;
    }
    
    // Rainbow contacts are visible if their JID is not a tel_ jid.
    if (self.rainbow)
        if ([self.rainbow.rainbowID length])
            return YES;
    return NO;
}

-(BOOL) isMuted {
    return [self.groups containsObject:@"muted"];
}

-(NSString *) countryCode {
    @synchronized (self) {
        if(self.rainbow)
            return self.rainbow.countryCode;
        else
            return nil;
    }
}

-(NSString *) language {
    @synchronized (self) {
        if(self.rainbow)
            return self.rainbow.language;
        else
            return nil;
    }
}

-(BOOL) isExternalContact {
    @synchronized (self) {
        if(self.rainbow)
            return NO;
        if(self.locals.count > 0){
            for (id contact in self.locals) {
                if([contact isKindOfClass:[ExternalContact class]]){
                    return YES;
                }
            }
        } else {
            return NO;
        }
    }
    return NO;
}

-(BOOL) isPBXContact {
    @synchronized (self) {
        if(self.rainbow)
            return NO;
        if(self.locals.count > 0){
            for (id contact in self.locals) {
                if([contact isKindOfClass:[ExternalContact class]] && [contact isPBXExternalContact]){
                    return YES;
                }
            }
        } else {
            return NO;
        }
    }
    return NO;
}

-(NSString *) externalObjectID {
    @synchronized (self) {
        if(self.isExternalContact){
            return ((ExternalContact*)[self.locals firstObject]).externalObjectID;
        }
    }
    return nil;
}

#pragma mark - KVO handling functions

-(void) addKVOForObject:(id) object {
    [object addObserver:self forKeyPath:kContactFirstNameKey options:NSKeyValueObservingOptionNew context:nil];
    [object addObserver:self forKeyPath:kContactLastNameKey options:NSKeyValueObservingOptionNew context:nil];
    [object addObserver:self forKeyPath:kContactLastActivityDateKey options:NSKeyValueObservingOptionNew context:nil];
    [object addObserver:self forKeyPath:kContactPresenceKey options:NSKeyValueObservingOptionNew context:nil];
    [object addObserver:self forKeyPath:kCalendarPresenceKey options:NSKeyValueObservingOptionNew context:nil];
}

-(void) removeKVOForObject:(id) object {
    [object removeObserver:self forKeyPath:kContactFirstNameKey];
    [object removeObserver:self forKeyPath:kContactLastNameKey];
    [object removeObserver:self forKeyPath:kContactLastActivityDateKey];
    [object removeObserver:self forKeyPath:kContactPresenceKey];
    [object removeObserver:self forKeyPath:kCalendarPresenceKey];
}

-(void) emitKVOChange:(NSString*) keyPath {
    [self willChangeValueForKey:keyPath];
    [self didChangeValueForKey:keyPath];
}

-(BOOL) isEqual:(Contact *) contact {
    if (self == contact)
        return YES;
    
    if (![contact isKindOfClass:[Contact class]])
        return NO;
    
    if (self.jid && contact && contact.jid && [self.jid isEqualToString:contact.jid])
        return YES;
   
    if (self.rainbowID && contact && contact.rainbowID && [self.rainbowID isEqualToString:contact.rainbowID])
        return YES;
    
    return NO;
}

-(void) setRainbow:(RainbowContact*) rainbow {
    [self removeKVOForObject:_rainbow];
    
    NSString *currentFirstName = self.firstName;
    NSString *currentLastName = self.lastName;
    Presence *currentPresence = self.presence;
    CalendarPresence *currentCalPresence = self.calendarPresence;
    NSDate *currentLastActivityDate = self.lastActivityDate;
    
    _rainbow = nil;
    _rainbow = rainbow;
    [self addKVOForObject:_rainbow];
    
    if (![self.firstName isEqualToString:currentFirstName]) {
        [self emitKVOChange:kContactFirstNameKey];
    }
    if (![self.lastName isEqualToString:currentLastName]) {
        [self emitKVOChange:kContactLastNameKey];
    }
    if (![self.presence isEqual:currentPresence]) {
        [self emitKVOChange:kContactPresenceKey];
    }
    if (![self.calendarPresence isEqual:currentCalPresence]) {
        [self emitKVOChange:kCalendarPresenceKey];
    }
    if (![self.lastActivityDate isEqualToDate:currentLastActivityDate]) {
        [self emitKVOChange:kContactLastActivityDateKey];
    }
}

-(void) addLocalContact:(LocalContact *) localContact {
    if (!localContact)
        return;
    
    [self addKVOForObject:localContact];
    
    NSString *currentFirstName = self.firstName;
    NSString *currentLastName = self.lastName;
    
    @synchronized (self) {
        [((NSMutableArray*) _locals) addObject:localContact];
    }
    
    if (![self.firstName isEqualToString:currentFirstName]) {
        [self emitKVOChange:kContactFirstNameKey];
    }
    if (![self.lastName isEqualToString:currentLastName]) {
        [self emitKVOChange:kContactLastNameKey];
    }
}

-(void) transfertLocalsFrom:(Contact *) otherContact {
    [self removeAllLocalContacts];
    if(!otherContact.isExternalContact){
        for (LocalContact *localContact in otherContact.locals) {
            [self addLocalContact:localContact];
        }
    }
}

-(void) removeLocalContact:(LocalContact *) localContact {
    if (!localContact || ![_locals containsObject:localContact])
        return;
    
    [self removeKVOForObject:localContact];
    
    NSString *currentFirstName = self.firstName;
    NSString *currentLastName = self.lastName;
    
    @synchronized (self) {
        [((NSMutableArray*) _locals) removeObject:localContact];
    }
    
    if (![self.firstName isEqualToString:currentFirstName]) {
        [self emitKVOChange:kContactFirstNameKey];
    }
    if (![self.lastName isEqualToString:currentLastName]) {
        [self emitKVOChange:kContactLastNameKey];
    }
}

-(void) removeAllLocalContacts {
    
    [_locals enumerateObjectsUsingBlock:^(LocalContact *localContact, NSUInteger idx, BOOL *stop) {
        [self removeKVOForObject:localContact];
    }];
    
    NSString *currentFirstName = self.firstName;
    NSString *currentLastName = self.lastName;
    
    @synchronized (self) {
        [((NSMutableArray *)_locals) removeAllObjects];
    }
    
    if (![self.firstName isEqualToString:currentFirstName]) {
        [self emitKVOChange:kContactFirstNameKey];
    }
    if (![self.lastName isEqualToString:currentLastName]) {
        [self emitKVOChange:kContactLastNameKey];
    }
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    // reflect changes of the wrapped contact to the KVO observers of this Contact.
    [self emitKVOChange:keyPath];
}

-(NSString *) description {
    @synchronized(self){
#if DEBUG
        return [NSString stringWithFormat:@"Contact %p : Firstname %@ Lastname %@ emailAdress %@ phonenumbers %@ jid: %@ Presence : %@ isRainbowUser %@ isVisible %@ isInRoster %@ isPresSubscr %@ isConnectedWithMobile %@ hasPBXAccess %@ requestedInvitation %@ sentInvitation %@" ,self, self.firstName, self.lastName, self.emailAddresses, self.phoneNumbers, self.jid, self.presence, NSStringFromBOOL(self.isRainbowUser), NSStringFromBOOL(self.isVisible), NSStringFromBOOL(self.isInRoster), NSStringFromBOOL(self.isPresenceSubscribed), NSStringFromBOOL(self.isConnectedWithMobile), NSStringFromBOOL(self.hasPBXAccess), self.requestedInvitation, self.sentInvitation];
#else
        return [NSString stringWithFormat:@"Contact %p : jid: %@ Presence : %@ isRainbowUser %@ isVisible %@ isInRoster %@ isPresSubscr %@ isConnectedWithMobile %@ hasPBXAccess %@ requestedInvitation %@ sentInvitation %@" ,self, self.jid, self.presence, NSStringFromBOOL(self.isRainbowUser), NSStringFromBOOL(self.isVisible), NSStringFromBOOL(self.isInRoster), NSStringFromBOOL(self.isPresenceSubscribed), NSStringFromBOOL(self.isConnectedWithMobile), NSStringFromBOOL(self.hasPBXAccess), self.requestedInvitation, self.sentInvitation];
#endif
    }
}

-(NSString *) debugDescription {
    @synchronized(self){
        return [NSString stringWithFormat:@"\tContact %p jid %@ Firstname %@ Lastname %@ isRainbowUser %@ isVisible %@ isInRoster %@ isPresSubscr %@ isConnectedWithMobile %@ hasPBXAccess %@",self, self.jid, self.firstName, self.lastName, NSStringFromBOOL(self.isRainbowUser), NSStringFromBOOL(self.isVisible), NSStringFromBOOL(self.isInRoster), NSStringFromBOOL(self.isPresenceSubscribed), NSStringFromBOOL(self.isConnectedWithMobile), NSStringFromBOOL(self.hasPBXAccess)];
    }
}

-(NSString *) jsonRepresentation {
    return [[self dictionaryRepresentation:NO] jsonStringWithPrettyPrint:NO];
}

-(NSDictionary *) dictionaryRepresentation {
    return [self dictionaryRepresentation:YES];
}

-(NSDictionary *) dictionaryRepresentation:(BOOL) fullRepresensation {
    @synchronized (self) {
        //
        // Make sure we pass only values of types : NSString, NSNumber, NSArray, NSDictionary, or NSNull
        //
        NSMutableDictionary *jsonDic = [NSMutableDictionary dictionary];
        if(self.photoData && fullRepresensation)
            [jsonDic setObject:[NSString stringWithFormat:@"%lu", (unsigned long)[self.photoData hash]] forKey:@"photoData"];
        if(self.firstName)
            [jsonDic setObject:self.firstName forKey:@"firstName"];
        if(self.lastName)
            [jsonDic setObject:self.lastName forKey:@"lastName"];
        if(self.nickName)
            [jsonDic setObject:self.nickName forKey:@"nickName"];
        else
            [jsonDic setObject:@"" forKey:@"nickName"];
        
        NSMutableArray *phoneNumbers = [NSMutableArray array];
        if(self.phoneNumbers.count > 0){
            NSArray *phoneNumbersList = self.phoneNumbers;
            for (PhoneNumber *phoneNumber in phoneNumbersList) {
                [phoneNumbers addObject:[phoneNumber dictionaryRepresentation]];
            }
        }
        [jsonDic setObject:phoneNumbers forKey:@"phoneNumbers"];
        
        NSMutableArray *emailAddresses = [NSMutableArray array];
        if(self.emailAddresses.count > 0) {
            for (EmailAddress *emailAddress in self.emailAddresses) {
                [emailAddresses addObject:[emailAddress dictionaryRepresentation]];
            }
        }
        // Attention ! the field is named emails on the server
        // And this key name is used to be sent to the server when we update our
        // own profile
        [jsonDic setObject:emailAddresses forKey:@"emails"];
        
        if(self.title)
            [jsonDic setObject:self.title forKey:@"title"];
        else
            [jsonDic setObject:@"" forKey:@"title"];
        if(self.jobTitle)
            [jsonDic setObject:self.jobTitle forKey:@"jobTitle"];
        else
            [jsonDic setObject:@"" forKey:@"jobTitle"];
        if(self.timeZone)
            [jsonDic setObject:self.timeZone.name forKey:@"timezone"];
        if(self.companyId)
            [jsonDic setObject:self.companyId forKey:@"companyId"];
        if(self.companyName)
            [jsonDic setObject:self.companyName forKey:@"companyName"];
        if(self.countryCode)
            [jsonDic setObject:self.countryCode forKey:@"country"];
        if(self.language)
            [jsonDic setObject:self.language forKey:@"language"];
        
        NSMutableArray *addresses = [NSMutableArray array];
        if(self.addresses.count > 0){
            for (PostalAddress *postalAddress in self.addresses) {
                [addresses addObject:[postalAddress dictionaryRepresentation]];
            }
        }
        [jsonDic setObject:addresses forKey:@"addresses"];
        
        NSMutableArray *webSitesURL = [NSMutableArray array];
        if(self.webSitesURL.count > 0){
            for (NSString *webSiteURL in self.webSitesURL) {
                [webSitesURL addObject:webSiteURL];
            }
        }
        [jsonDic setObject:webSitesURL forKey:@"webSitesURL"];
        
        if(fullRepresensation){
            [jsonDic setObject:NSStringFromBOOL(self.vcardPopulated) forKey:@"vcardPopulated"];
            if(self.jid)
                [jsonDic setObject:self.jid forKey:@"jid"];
            if(self.jid_tel)
                [jsonDic setObject:self.jid_tel forKey:@"jid_tel"];
            if(self.rainbowID)
                [jsonDic setObject:self.rainbowID forKey:@"rainbowID"];
            
            [jsonDic setObject:NSStringFromBOOL(self.isInRoster) forKey:@"isInRoster"];
            [jsonDic setObject:NSStringFromBOOL(self.isPresenceSubscribed) forKey:@"isPresenceSubscribed"];
            
            if (self.requestedInvitation)
                [jsonDic setObject:[self.requestedInvitation description] forKey:@"requestedInvitation"];
            
            if (self.sentInvitation)
                [jsonDic setObject:[self.sentInvitation description] forKey:@"sentInvitation"];
            
            if(self.companyInvitation)
                [jsonDic setObject:[self.companyInvitation description] forKey:@"companyInvitation"];
            
            if(self.groups)
                [jsonDic setObject:self.groups forKey:@"groups"];
            
            if(self.presence)
                [jsonDic setObject:[self.presence description] forKey:@"presence"];
            [jsonDic setObject:NSStringFromBOOL(self.isConnectedWithMobile) forKey:@"isConnectedWithMobile"];
            [jsonDic setObject:NSStringFromBOOL(self.hasPBXAccess) forKey:@"hasPBXAccess"];
            if(self.lastUpdateDate)
                [jsonDic setObject:[NSDate jsonStringFromDate:self.lastUpdateDate] forKey:@"lastUpdateDate"];
            [jsonDic setObject:NSStringFromBOOL(self.isBot) forKey:@"isBot"];
            [jsonDic setObject:NSStringFromBOOL(self.isTerminated) forKey:@"isTerminated"];
            if(self.lastActivityDate)
                [jsonDic setObject:[NSDate jsonStringFromDate:self.lastActivityDate] forKey:@"lastActivityDate"];
            
            // Add only come computed properties.
            
            if (self.displayName)
                [jsonDic setObject:self.displayName forKey:@"displayName"];
        }
        return jsonDic;
    }
}


#pragma mark - helpers

-(PhoneNumber *) getPhoneNumberOfType:(PhoneNumberType) type {
    __block PhoneNumber *phoneNumberToReturn = nil;
    [self.phoneNumbers enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
        if(aPhoneNumber.type == type){
            phoneNumberToReturn = aPhoneNumber;
            *stop = YES;
        }
    }];
    return phoneNumberToReturn;
}

-(BOOL) hasPhoneNumberOfType:(PhoneNumberType) type {
    __block BOOL result = NO;
    [self.phoneNumbers enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
        if(aPhoneNumber.type == type){
            result = YES;
            *stop = YES;
        }
    }];
    return result;
}

-(PhoneNumber *) getPhoneNumberOfType:(PhoneNumberType) type withDeviceType:(PhoneNumberDeviceType)deviceType {
    __block PhoneNumber *phoneNumberToReturn = nil;
    [self.phoneNumbers enumerateObjectsUsingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
        if(aPhoneNumber.type == type && aPhoneNumber.deviceType == deviceType) {
            phoneNumberToReturn = aPhoneNumber;
            *stop = YES;
        }
    }];
    return phoneNumberToReturn;
}

-(BOOL) hasPhoneNumberOfType:(PhoneNumberType) type withDeviceType:(PhoneNumberDeviceType)deviceType {
    __block BOOL result = NO;
    [self.phoneNumbers enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
        if(aPhoneNumber.type == type && aPhoneNumber.deviceType == deviceType) {
            result = YES;
            *stop = YES;
        }
    }];
    return result;
}

-(EmailAddress *) getEmailAdressOfType:(EmailAddressType) type {
    __block EmailAddress *emailAddressToReturn = nil;
    [self.emailAddresses enumerateObjectsUsingBlock:^(EmailAddress * emailAdress, NSUInteger idx, BOOL * stop) {
        if(emailAdress.type == type){
            emailAddressToReturn = emailAdress;
            *stop = YES;
        }
            
    }];
    return emailAddressToReturn;
}

-(BOOL) hasEmailAddressOfType:(EmailAddressType) type {
    __block BOOL result = NO;
    [self.emailAddresses enumerateObjectsUsingBlock:^(EmailAddress * emailAddress, NSUInteger idx, BOOL * stop) {
        if(emailAddress.type == type){
            result = YES;
            *stop = YES;
        }
    }];
    return result;
}

@end
