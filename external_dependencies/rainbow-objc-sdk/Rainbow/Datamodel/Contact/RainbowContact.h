/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "GenericContact.h"
#import "Presence.h"
#import "CalendarPresence.h"
#import "Invitation.h"
#import "CompanyInvitation.h"

/**
 *  This class adds the properties
 *  that exists only for rainbow contacts and
 *  never for local contacts.
 */
@interface RainbowContact : GenericContact

/**
 *  Whether the information of the user has been retrieved from the server
 */
@property (nonatomic) BOOL vcardPopulated;

/**
 *  The user's JID
 */
@property (nonatomic, strong) NSString *jid;

/**
 *  The user's telephone JID
 */
@property (nonatomic, strong) NSString *jid_tel;

/**
 *  The user's Rainbow ID
 */
@property (nonatomic, strong) NSString *rainbowID;

/**
 *  Whether the user is in our roster
 */
@property (nonatomic) BOOL isInRoster;

/**
 *  Whether we have subscribed to the user's presence
 */
@property (nonatomic) BOOL isPresenceSubscribed;

/**
 *  New invitation system. When this user send us an invitation
 *  through REST api (or old xmpp roster) it is linked here.
 */
@property (nonatomic, strong) Invitation *requestedInvitation;

/**
 *  New invitation system. When we send an invitation to this user
 *  through REST api (or old xmpp roster) it is linked here.
 */
@property (nonatomic, strong) Invitation *sentInvitation;

/**
 *  The user's XMPP groups
 */
@property (nonatomic, strong) NSMutableArray<NSString*> *groups;

/**
 *  The user's current presence
 */
@property (nonatomic, strong) Presence *presence;

/**
 *  The user's current calendar presence
 */
@property (nonatomic, strong) CalendarPresence *calendarPresence;

/**
 *  Whether the user is connected with at least one mobile or not
 */
@property (nonatomic) BOOL isConnectedWithMobile;

/**
 *  Whether the user has a configured PBX access
 */
@property (nonatomic) BOOL hasPBXAccess;

/**
 *  Time of the last user profile update
 */
@property (nonatomic, strong) NSDate *lastUpdateDate;

/**
 *  Whether the user is a bot (like Emily) or not
 */
@property (nonatomic) BOOL isBot;

/**
 *  Whether the user has closed his account or not
 */
@property (nonatomic) BOOL isTerminated;

/**
 *  The last time this user has been active (its a server information)
 */
@property (nonatomic, strong) NSDate *lastActivityDate;

/**
 *  The user's country code following ISO 3166-1 alpha3 format
 */
@property (nonatomic, strong, readwrite) NSString *countryCode;

/**
 *  The user's language in ISO 639 alpha-1 format
 */
@property (nonatomic, strong, readwrite) NSString *language;

/**
 *  An invitation sent or received to join a company
 */
@property (nonatomic, strong, readwrite) CompanyInvitation *companyInvitation;


@end
