/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "LocalContact.h"

@implementation LocalContact

-(instancetype) init {
    if (self = [super init]) {
        _addresses = [NSMutableArray array];
        _webSitesURL = [NSMutableArray array];
    }
    return self;
}

-(void) dealloc {
    [_addresses removeAllObjects];
    _addresses = nil;
    [_webSitesURL removeAllObjects];
    _webSitesURL = nil;
}

-(NSString *) contactLocalID {
    if(self.addressBookRecordID > 0)
        return [NSString stringWithFormat:@"%ld", (long)self.addressBookRecordID];
    else
        return nil;
}

@end
