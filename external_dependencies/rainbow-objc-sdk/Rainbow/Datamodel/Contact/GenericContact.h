/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "PostalAddress.h"
#import "PhoneNumber.h"
#import "EmailAddress.h"
#import "UserPhoto.h"

/**
 *  This class contains all the generic properties 
 *  which can exists for both Rainbow Contacts and 
 *  local contacts
 */
@interface GenericContact : NSObject

/**
 *  The user's Avatar
 */
@property (nonatomic, strong) UserPhoto *userPhoto;

/**
 *  The user's first name
 */
@property (nonatomic, strong) NSString *firstName;

/**
 *  The user's last name
 */
@property (nonatomic, strong) NSString *lastName;

/**
 *  The user's nick name
 */
@property (nonatomic, strong) NSString *nickName;

/**
 *  The user's phone numbers
 */
@property (nonatomic, strong, readonly) NSArray<PhoneNumber*> *phoneNumbers;

-(BOOL) phoneNumbersContainsObject:(PhoneNumber *) phoneNumber;
-(void) addPhoneNumberObject:(PhoneNumber *)phoneNumber;
-(void) removePhoneNumberObject:(PhoneNumber *)phoneNumber;
-(void) removePhoneNumbersObjectFromArray:(NSArray<PhoneNumber*>*) phoneNumbers;
-(void) removeAllPhoneNumbers;

/**
 *  The user's email addresses
 */
@property (nonatomic, strong, readonly) NSArray<EmailAddress*> *emailAddresses;

-(BOOL) emailAddressesContainsObject:(EmailAddress *) emailAddress;
-(void) addEmailAddressObject:(EmailAddress *)emailAdress;
-(void) removeEmailAddressObject:(EmailAddress *)emailAdress;
-(void) removeEmailAddressesObjectFromArray:(NSArray<EmailAddress*>*) emails;
-(void) removeAllEmailAddresses;

/**
 *  The user's title (honorifics title, like Mr, Mrs, Sir, Lord, Lady, Dr, Prof,...)
 */
@property (nonatomic, strong) NSString *title;

/**
 *  The user's job title
 */
@property (nonatomic, strong) NSString *jobTitle;

/**
 *  The user's time zone
 */
@property (nonatomic, strong) NSTimeZone *timeZone;

/**
 *  The user's company rainbowID
 */
@property (nonatomic, strong) NSString *companyId;

/**
 *  The user's company name
 */
@property (nonatomic, strong) NSString *companyName;

/**
 *  This creates a JSON representation of the GenericContact
 *
 *  @return the JSON representation as string
 */
-(NSString *) jsonRepresentation;

@end
