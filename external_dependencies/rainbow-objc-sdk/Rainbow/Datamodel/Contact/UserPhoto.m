/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UserPhoto.h"
#import "Tools.h"

@interface UserPhoto ()

@end

@implementation UserPhoto

-(instancetype) initWithPhotoData:(NSData *) photoData {
    self = [super init];
    if(self){
        _photoData = photoData;
        _lastUpdateDate = [NSDate date];
    }
    return self;
}

-(instancetype) initWithPhotoData:(NSData *) photoData lastUpdateDate:(NSDate *) lastUpdateDate {
    self = [super init];
    if(self){
        _photoData = photoData;
        _lastUpdateDate = lastUpdateDate;
    }
    return self;
}

-(instancetype) initWithLastUpdateDate:(NSDate *) lastUpdateDate {
    self = [super init];
    if(self){
        _photoData = nil;
        _lastUpdateDate = lastUpdateDate;
    }
    return self;
}

-(void) updateUserPhotoWith:(UserPhoto *) userPhoto {
    if(userPhoto.photoData)
        self.photoData = [userPhoto.photoData copy];
    if(userPhoto.lastUpdateDate)
        self.lastUpdateDate = [userPhoto.lastUpdateDate copy];
}

-(void) dealloc {
    _photoData = nil;
    _lastUpdateDate = nil;
}

#pragma mark - NSCoding implementation
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_photoData forKey:@"photoData"];
    [aCoder encodeObject:_lastUpdateDate forKey:@"lastUpdateDate"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if(self){
        _photoData = [aDecoder decodeObjectForKey:@"photoData"];
        _lastUpdateDate = [aDecoder decodeObjectForKey:@"lastUpdateDate"];
    }
    return self;
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"UserPhoto %p have data %@ with update date %@",self, NSStringFromBOOL(_photoData!=nil), _lastUpdateDate];
    }
}

-(BOOL)isEqual:(id)object {
    if(self == object){
        return YES;
    }
    if(self.photoData == _photoData)
        return YES;
    
    return NO;
}

-(NSUInteger)hash {
    return [_photoData hash];
}
@end
