/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "PhoneNumber.h"
#import "PhoneNumberInternal.h"
#import "Tools.h"
#import "NSDictionary+JSONString.h"

@implementation PhoneNumber

-(instancetype) initWithPhoneNumberString:(NSString *) phoneNumberString {
    self = [super init];
    if(self){
        self.number = phoneNumberString;
    }
    return self;
}

-(instancetype) initWithPhoneNumber:(PhoneNumber *) phoneNumber {
    self = [super init];
    if(self){
        [self updateWith:phoneNumber];
    }
    return self;
}

-(void) updateWith:(PhoneNumber*)pNumber {
    self.number = pNumber.number?[NSString stringWithString:pNumber.number]:nil;
    self.numberE164 = pNumber.numberE164?[NSString stringWithString:pNumber.numberE164]:nil;
    self.type = pNumber.type?pNumber.type:PhoneNumberTypeHome;
    self.deviceType = pNumber.deviceType?pNumber.deviceType:PhoneNumberDeviceTypeLandline;
    self.isPrefered = pNumber.isPrefered?pNumber.isPrefered:NO;
    self.countryCode = pNumber.countryCode?[NSString stringWithString:pNumber.countryCode]:nil;
    self.isMonitored = pNumber.isMonitored?pNumber.isMonitored:NO;
}

-(NSString *) label {
    return [PhoneNumber stringForPhoneNumberType:_type];
}

-(NSString *) number {
    if(!_number)
        return _numberE164;
    return _number;
}

+(NSString *) stringForPhoneNumberType:(PhoneNumberType) type {
    switch (type) {
        case PhoneNumberTypeHome: {
            return @"Home";
            break;
        }
        case PhoneNumberTypeWork:
        default:{
            return @"Work";
            break;
        }
        case PhoneNumberTypeOther:{
            return @"Other";
            break;
        }
        case PhoneNumberTypeConference:{
            return @"Conference";
            break;
        }
    }
}

+(PhoneNumberType) typeFromPhoneNumberLabel:(NSString *) label {
    if([label isEqualToString:@"home"])
        return PhoneNumberTypeHome;
    if([label isEqualToString:@"other"])
        return PhoneNumberTypeOther;
    if([label isEqualToString:@"Conference"])
        return PhoneNumberTypeConference;
    return PhoneNumberTypeWork;
}

+(PhoneNumberDeviceType) deviceTypeFromDeviceTypeLabel:(NSString *) label {
    //landline, mobile, fax, other
    if([label isEqualToString:@"mobile"])
        return PhoneNumberDeviceTypeMobile;
    if([label isEqualToString:@"fax"])
        return PhoneNumberDeviceTypeFax;
    if([label isEqualToString:@"other"])
        return PhoneNumberDeviceTypeOther;
    return PhoneNumberDeviceTypeLandline;
}

+(NSString *) stringForPhoneNumberDeviceType:(PhoneNumberDeviceType) deviceType {
    switch (deviceType) {
        case PhoneNumberDeviceTypeLandline: {
            return @"Landline";
            break;
        }
        case PhoneNumberDeviceTypeMobile: {
            return @"Mobile";
            break;
        }
        case PhoneNumberDeviceTypeFax: {
            return @"Fax";
            break;
        }
        case PhoneNumberDeviceTypeOther: {
            return @"Other";
            break;
        }
    }
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"Phone number : < %@ (E164: %@) - %@ - %@ (Prefered: %@, CountryCode: %@)>", _number, _numberE164, [PhoneNumber stringForPhoneNumberType:_type], [PhoneNumber stringForPhoneNumberDeviceType:_deviceType], NSStringFromBOOL(_isPrefered), _countryCode];
    }
}

-(BOOL) isEqual:(PhoneNumber *) phoneNumber {
    if([phoneNumber isKindOfClass:[PhoneNumber class]]){
        if(_numberE164 && phoneNumber.numberE164)
            return [[_numberE164 stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:[phoneNumber.numberE164 stringByReplacingOccurrencesOfString:@" " withString:@""]];
        else
            return [[self.number stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:[phoneNumber.number stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    return NO;
}

-(NSUInteger)hash {
    return [_number hash];
}

- (id)copyWithZone:(nullable NSZone *)zone {
    return [[PhoneNumber allocWithZone:zone] initWithPhoneNumber:self];
}

-(NSString *) jsonRepresentation {
    return [[self dictionaryRepresentation] jsonStringWithPrettyPrint:NO];
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *jsonDic = [NSMutableDictionary dictionary];
    if(_number)
        [jsonDic setObject:_number forKey:@"number"];
    if(_numberE164)
        [jsonDic setObject:_numberE164 forKey:@"numberE164"];
    
    [jsonDic setObject:[[PhoneNumber stringForPhoneNumberType:_type] lowercaseString] forKey:@"type"];
    [jsonDic setObject:[[PhoneNumber stringForPhoneNumberDeviceType:_deviceType] lowercaseString] forKey:@"deviceType"];
    
    [jsonDic setObject:NSStringFromBOOL(_isMonitored) forKey:@"isMonitored"];
    
    return jsonDic;
}

@end
