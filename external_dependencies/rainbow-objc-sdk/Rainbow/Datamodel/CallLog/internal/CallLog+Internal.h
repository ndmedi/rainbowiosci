/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CallLog.h"
#import "Contact.h"

@interface CallLog ()
@property (nonatomic, readwrite) CallLogType type;
@property (nonatomic, readwrite) CallLogService service;
@property (nonatomic, readwrite) CallLogState state;
@property (nonatomic, readwrite, strong) NSDate *date;
// Duration in millisecond retreived
@property (nonatomic, readwrite, strong) NSNumber *realDuration;
@property (nonatomic, readwrite, strong) Contact *caller;
@property (nonatomic, readwrite, strong) Contact *callee;
@property (nonatomic, readwrite) CallLogMedia media;

@property (nonatomic, strong) NSString *callLogId;
@property (nonatomic, readwrite) BOOL isOutgoing;
@property (nonatomic) BOOL isRead;

+(CallLogState) callLogStateFromEventTypeString:(NSString *) eventType;
-(NSDictionary *) dictionaryDescription;
+(CallLogMedia) callLogMediaFromString:(NSString *) mediaString;
+(CallLogType) callLogTypeFromString:(NSString *) type;
+(CallLogService) callLogServiceFromString:(NSString *) service;
@end
