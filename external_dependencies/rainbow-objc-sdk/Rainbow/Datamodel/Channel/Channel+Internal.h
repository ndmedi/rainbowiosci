/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Channel.h"

@interface Channel ()
@property (nonatomic, strong, readwrite) NSString *name;
@property (nonatomic, strong, readwrite) NSString *topic;
@property (nonatomic, readwrite) ChannelVisibility visibility;
@property (nonatomic, readwrite) int maxItems;
@property (nonatomic, readwrite) int maxPayloadSize;
@property (nonatomic, strong, readwrite) NSString *id;
@property (nonatomic, strong, readwrite) NSString *companyId;
@property (nonatomic, strong, readwrite) NSString *creatorId;
@property (nonatomic, strong, readwrite) NSDate *creationDate;
@property (nonatomic, readwrite) int usersCount;

+(NSString *)stringFromChannelVisibility:(ChannelVisibility)visibility;
+(ChannelVisibility)channelVisibilityFromString:(NSString *)str;
+(NSString *)stringFromChannelUserType:(ChannelUserType)userType;
+(ChannelUserType)channelUserTypeFromString:(NSString *)str;
@end
