/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Channel.h"
#import "Channel+Internal.h"
#import "ChannelPayload+Internal.h"

@interface Channel ()
@property (nonatomic, strong, readwrite) NSMutableArray<ChannelPayload *> *items;
@end

@implementation Channel

+(NSString *)stringFromChannelVisibility:(ChannelVisibility)visibility {
    NSString *visibilityStr = nil;
    switch(visibility){
        case ChannelVisibilityPublic:
            visibilityStr = @"public";
            break;
        case ChannelVisibilityCompany:
            visibilityStr = @"company";
            break;
        case ChannelVisibilityPrivate:
            visibilityStr = @"private";
            break;
    }
    return visibilityStr;
}

+(ChannelVisibility)channelVisibilityFromString:(NSString *)str {
    ChannelVisibility visibility = ChannelVisibilityPublic;
    if([str isEqualToString:@"private"]){
        visibility = ChannelVisibilityPrivate;
    } else if([str isEqualToString:@"company"]){
        visibility = ChannelVisibilityCompany;
    }
    return visibility;
}


+(NSString *)stringFromChannelUserType:(ChannelUserType)userType {
    NSString *userTypeStr = nil;
    switch(userType){
        case ChannelUserTypeNone:
            userTypeStr = @"none";
            break;
        case ChannelUserTypeMember:
            userTypeStr = @"member";
            break;
        case ChannelUserTypePublisher:
            userTypeStr = @"publisher";
            break;
        case ChannelUserTypeOwner:
            userTypeStr = @"owner";
            break;
    }
    return userTypeStr;
}

+(ChannelUserType)channelUserTypeFromString:(NSString *)str {
    ChannelUserType userType = ChannelUserTypeNone;
    if([str isEqualToString:@"member"]){
        userType = ChannelUserTypeMember;
    } else if([str isEqualToString:@"publisher"]){
        userType = ChannelUserTypePublisher;
    } else if([str isEqualToString:@"owner"]){
        userType = ChannelUserTypeOwner;
    }
    return userType;
}

-(instancetype)init {
    self = [super init];
    if(self){
        _items = [[NSMutableArray alloc] init];
    }
    return self;
}

-(ChannelPayload *)findItemById:(NSString *)id {
    for(ChannelPayload *item in self.items){
        if([id isEqualToString:item.id]){
            return item;
        }
    }
    return nil;
}

-(void)addOrUpdateItem:(ChannelPayload *)item {
    ChannelPayload *oldItem = [self findItemById:item.id];
    if(oldItem){
        oldItem.title = item.title;
        oldItem.message = item.message;
        oldItem.url = item.url;
    } else {
        [(NSMutableArray *)self.items addObject:item];
    }
}

-(void)removeItem:(ChannelPayload *)item {
    ChannelPayload *oldItem = [self findItemById:item.id];
    if(oldItem){
        [(NSMutableArray *)self.items removeObject:oldItem];
    }
}

@end
