/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CKBrowserResyncJob.h"
#import "CKBrowsableItem.h"
#import "NSDate+Equallity.h"
#import "NSDate+Utilities.h"

@implementation CKBrowserResyncJob

@synthesize untilDate = _untilDate;

-(id) initWithNextPageOperation:(CKBrowsingOperation*)nextPageOp {
    self = [super init];
    if (self) {
        _nextPageOperation = [nextPageOp retain];
    }
    return self;
}

-(void) dealloc {
    [_untilDate release];
    [_nextPageOperation release];
    [super dealloc];
}

/**
 The time frame of a resync starts at the limit "until" date and goes to "NOW".
 [_untilDate; nil]
 */
-(NSDate*) timeFrameStart {
    return _untilDate;
}

-(NSArray*) performJobWithError:(NSError **)error {
//    NSLog(@"[BROWSER] performing %@", self);
    BOOL resyncDone = NO;
    [_jobResultPage release];
    _jobResultPage = [[NSMutableArray alloc] init];
    _nextPageOperation.startIndex=0;
    _nextPageOperation.itemsCount = _itemsCount;
    while (!resyncDone && !_outdated) {
        NSError* pageError = nil;
        NSArray* currentPage = [_nextPageOperation performJobWithError:&pageError];
        self.hasMorePages = _nextPageOperation.hasMorePages;
//        NSLog(@"[BROWSER] resyncing from item #%ld for %ld items", (long)_nextPageOperation.startIndex, (long)_nextPageOperation.itemsCount);
        if (!pageError) {
            BOOL lastPage = [currentPage count]<_itemsCount;
            id<CKBrowsableItem> last = [currentPage lastObject];
            // add only callLog instance that are before the untilDate
            for (id<CKBrowsableItem> item in currentPage) {
                if ([item.date isLaterThanDate:self.untilDate] || [item.date isEqualToDateAndTime:self.untilDate]) { // if it is descending or equals
                    [(NSMutableArray*)_jobResultPage addObject:item];
                }
            }
            if ([last.date compare:self.untilDate]!=NSOrderedDescending || lastPage) { 
                resyncDone = YES;
            } else {
                _nextPageOperation.startIndex+=_itemsCount;
            }
        } else {
            resyncDone = YES;
            if (error)
                *error = [NSError errorWithDomain:@"resync" code:-1 userInfo:nil];
        }
    }
//    NSLog(@"[BROWSER] RESYNC until %@ (%li items): %@", self.untilDate, (unsigned long)[_jobResultPage count], _jobResultPage);
    return _jobResultPage;
}

-(NSString*) description {
    return [NSString stringWithFormat:@"ResyncJob%@ (until %@)", (_outdated)?@"[!OUTDATED!]":@"", _untilDate];
}

@end
