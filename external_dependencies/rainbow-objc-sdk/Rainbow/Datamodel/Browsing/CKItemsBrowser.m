/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CKItemsBrowser.h"
#import "CKBrowsingOperation.h"
#import "CKBrowserResyncJob.h"
#import "Tools.h"

@interface CKItemsBrowser(internal)
-(NSInteger) properIndexForItem:(id<CKBrowsableItem>)item;
-(NSIndexSet*) browsingCacheIndexesForItems:(NSArray*)items ;
+(NSArray*) idsFromItems:(NSArray*)items;
-(NSArray*) cacheItemsBetweenStartDate:(NSDate*)startDate andEndDate:(NSDate*)endDate;
-(CKBrowsingOperation*) nextPageOperation;
@end

@interface CKItemsBrowser ()
@end


@implementation CKItemsBrowser

@synthesize delegate = _delegate;

-(instancetype) initWithItemsCountPerPage:(NSInteger)itemsPerPage {
    self = [super init];
    if (self) {
        _hasMorePages = YES;
        _itemsCountPerPage = itemsPerPage;
        _browsingCache = [[NSMutableArray alloc] init];
        _itemsMutex = [[NSObject alloc] init];
        _pendingBrowsingJobs = [[NSMutableArray alloc] init];
        _browsingJobsQueue = [[NSOperationQueue alloc] init];
        [_browsingJobsQueue setMaxConcurrentOperationCount:1];
    }
    return self;
}

-(void) dealloc {
    [_itemsMutex release];
    [_browsingJobsQueue release];
    [_browsingCache release];
    [_pendingBrowsingJobs release];
    [super dealloc];
}

#pragma mark - public API

-(BOOL) hasMorePages {
    @synchronized(_itemsMutex) {
        return _hasMorePages;
    }
}

-(NSArray*) browsingCache {
    NSArray* browsingCache = nil;
    @synchronized (_itemsMutex) {
        browsingCache = [NSArray arrayWithArray:_browsingCache];
    }
    return browsingCache;
}

-(void) reset {
//    NSLog(@"[BROWSER] resetting browser ...");
    //if there's an ongoing next logs pages request -> invalidate it
    @synchronized (_itemsMutex) {
        [self invalidatePendingJobs];
        [self removeCacheItems:_browsingCache];
        _hasMorePages=YES;
    }
}

-(void) resyncBrowsingCacheWithCompletionHandler:(CKItemsBrowsingCompletionHandler)completionHandler {
    @synchronized (_itemsMutex) {
        if ([_browsingCache count]>0) {
            NSDate* dateLimit = ((id<CKBrowsableItem>)[_browsingCache lastObject]).date;
            [self resyncBrowsingCacheUntilDate:dateLimit withCompletionHandler:completionHandler];
        } else {
//            NSLog(@"[BROWSER] resyncing %@ by asking 1st page...", self);
            [self nextPageWithCompletionHandler:completionHandler];
        }
    }
}

-(void) resyncBrowsingCacheUntilDate:(NSDate*)date withCompletionHandler:(CKItemsBrowsingCompletionHandler)completionHandler {
//    NSLog(@"[BROWSER] resyncing %@ until %@ ...", self, date);
    CKBrowserResyncJob* resyncJob= nil;
    @synchronized (_itemsMutex) {
        //oudate all previous jobs
        [self invalidatePendingJobs];
        // we do a resync
        CKBrowsingOperation* nextPageOperation = [self nextPageOperation];
        resyncJob =  [[CKBrowserResyncJob alloc] initWithNextPageOperation:nextPageOperation];
        resyncJob.untilDate = date;
        resyncJob.itemsCount = _itemsCountPerPage;
        [self performJob:resyncJob withCompletionHandler:completionHandler];
        [resyncJob release];
//        NSLog(@"[BROWSER] jobs queue is now %@", _pendingBrowsingJobs);
    }
}

-(void) nextPageWithCompletionHandler:(CKItemsBrowsingCompletionHandler)completionHandler {
    CKBrowsingOperation* localJob= nil;
    NSInteger startIndex = -1;
    @synchronized (_itemsMutex) {
        //oudate all previous jobs
        [self invalidatePendingJobs];
        startIndex = [_browsingCache count];
        localJob = [self nextPageOperation];
        localJob.startIndex = startIndex;
        localJob.itemsCount= _itemsCountPerPage;
        [self performJob:localJob withCompletionHandler:completionHandler];
//        NSLog(@"[BROWSER] jobs queue is now %@", _pendingBrowsingJobs);
    }
}

/**
 this selector should be overriden by subclassing to return the concrete way a next page operation
 can be achieved.
 */
-(CKBrowsingOperation*) nextPageOperation {
    return nil;
}

-(id<CKBrowsableItem>) removeCacheItemAtIndex:(NSInteger)idx {
    id<CKBrowsableItem> removedObject = nil;
    @synchronized(_itemsMutex) {
        NSIndexSet* removedIndexes = [NSIndexSet indexSetWithIndex:idx];
        removedObject = [_browsingCache objectAtIndex:idx];
        [_browsingCache removeObjectAtIndex:idx];
        __unsafe_unretained CKItemsBrowser *weakSelf = self;
        dispatch_block_t block = ^{
            [_delegate itemsBrowser:weakSelf didRemoveCacheItems:[NSArray arrayWithObject:removedObject] atIndexes:removedIndexes];
        };
        if([NSThread isMainThread])
            block();
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                block();
            });
        }
    }
    return removedObject;
}

-(NSArray*) removeCacheItemsFromIndex:(NSInteger)idx {
    NSArray* removedItems = nil;
    @synchronized(_itemsMutex) {
        if (idx<[_browsingCache count]) {
            NSIndexSet* removedIndexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(idx, [_browsingCache count]-idx)];
            removedItems = [[_browsingCache objectsAtIndexes:removedIndexes] retain];
            [_browsingCache removeObjectsAtIndexes:removedIndexes];
            __unsafe_unretained CKItemsBrowser *weakSelf = self;
            dispatch_block_t block = ^{
                [_delegate itemsBrowser:weakSelf didRemoveCacheItems:removedItems atIndexes:removedIndexes];
            };
            if([NSThread isMainThread])
                block();
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    block();
                });
            }
            
            [removedItems release];
        }
        if ([removedItems count]>0)
            _hasMorePages = YES;
    }
    return removedItems;
}

-(NSArray*) removeCacheItemsBeforeDate:(NSDate*)limitDate {
    NSArray* removedObjects = nil;
    __block NSInteger idxLimit = 0;
    @synchronized(_itemsMutex) {
        [_browsingCache enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            id<CKBrowsableItem> item = obj;
            // if item date is earlier than limitDate 
            if ([item.date compare:limitDate]==NSOrderedAscending) {
                *stop = YES;
                idxLimit = idx;
            }
        }];
        [self removeCacheItemsFromIndex:idxLimit];
    }
    return removedObjects;
}

-(NSArray*) removeCacheItems:(NSArray*)itemsToBeRemoved {
    // update the browsing cache;
    NSMutableArray* foundDeletedItems = [NSMutableArray array];
    NSMutableArray* foundDeletedItemsIDs= [NSMutableArray array];
    NSMutableIndexSet* foundDeletedItemsIndexes = [NSMutableIndexSet indexSet];
    @synchronized (_itemsMutex) {
        for (id<CKBrowsableItem> deletedItem in itemsToBeRemoved) {
            // update logs structs
            NSUInteger index = [_browsingCache indexOfObject:deletedItem];
            if (index != NSNotFound) {
                [foundDeletedItems addObject:[_browsingCache objectAtIndex:index]];
                [foundDeletedItemsIndexes addIndex:index];
                [foundDeletedItemsIDs addObject:deletedItem.referenceIdentifier];
            }
            else {
//                NSLog (@"[BROWSER] deleted item not found %@", deletedItem);
            }
        }
        NSAssert([foundDeletedItemsIDs count]==[foundDeletedItemsIndexes count], nil);
        [_browsingCache removeObjectsAtIndexes:foundDeletedItemsIndexes];
    }
    if ([foundDeletedItems count]>0){
        __unsafe_unretained CKItemsBrowser *weakSelf = self;
        dispatch_block_t block = ^{
            [_delegate itemsBrowser:weakSelf didRemoveCacheItems:foundDeletedItems atIndexes:foundDeletedItemsIndexes];
        };
        
        if([NSThread isMainThread])
            block();
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                block();
            });
        }
    }
    return foundDeletedItems;
}

-(id<CKBrowsableItem>) getCacheItemWithReferenceIdentifier:(NSString*)refIdentifier {
    __block id<CKBrowsableItem> foundItem = nil;
    @synchronized (_itemsMutex) {
        [_browsingCache enumerateObjectsUsingBlock:^(id<CKBrowsableItem> obj, NSUInteger idx, BOOL *stop) {
            if ([obj.referenceIdentifier isEqualToString:refIdentifier]) {
                foundItem = obj;
                *stop = YES;
            }
        }];
    }    
    return foundItem;
}

#pragma mark - Browsing cache management

+(NSArray*) idsFromItems:(NSArray*)items {
    NSMutableArray* ids = [NSMutableArray array];
    for (id<CKBrowsableItem> item in items)
        [ids addObject:item.referenceIdentifier];
    return ids;
}

-(NSInteger) properIndexForItem:(id<CKBrowsableItem>)aNewItem {
    __block NSInteger insertIndex = [_browsingCache count];
    @synchronized (_itemsMutex) {
        // if we don't find a proper place in the list, this means we should add it at the end.
        [_browsingCache enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            id<CKBrowsableItem> item = obj;
            // if item.date is not later than aNewItem.date
            if (item.date!=nil && ![item isEqual:aNewItem] && [item.date compare:aNewItem.date]!=NSOrderedDescending) {
                *stop = YES;
                insertIndex = idx;
            }
        }];
    }
    return insertIndex;
}

/**
 Returns `YES` if the move changed something to the ordering of items, `NO` if 
 the cache is left unchanged.
 */
- (BOOL) moveItemFromIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex {
    if (fromIndex < toIndex) {
        toIndex--; // Optional 
    }
    
    if (fromIndex!=toIndex) {
        id<CKBrowsableItem> object = [_browsingCache objectAtIndex:fromIndex];
        [_browsingCache removeObjectAtIndex:fromIndex];
        [_browsingCache insertObject:object atIndex:toIndex];
        return YES;
    } else {
        return NO;
    }
}

-(NSInteger) insertItemAtProperIndex:(id<CKBrowsableItem>)aNewItem {
    NSInteger idxForInsertion = NSNotFound;
    NSInteger idxForDeletion = NSNotFound;
    @synchronized (_itemsMutex) {
        if ([_browsingCache indexOfObject:aNewItem]==NSNotFound) {
            idxForInsertion = [self properIndexForItem:aNewItem];
            [_browsingCache insertObject:aNewItem atIndex:idxForInsertion];
        } else {
//            idxForDeletion = [_browsingCache indexOfObject:aNewItem];
//            [_browsingCache removeObjectAtIndex:idxForDeletion];
//            idxForInsertion = [self properIndexForItem:aNewItem];
//            [_browsingCache insertObject:aNewItem atIndex:idxForInsertion];

        }
    }
    return idxForInsertion;
}

-(NSIndexSet*) browsingCacheIndexesForItems:(NSArray*)items {
    NSMutableIndexSet* addedIndexes = [NSMutableIndexSet indexSet];
    for (NSObject* item in items) {
        NSInteger idx = [_browsingCache indexOfObject:item];
        if (idx==NSNotFound)
            return nil;
        else
            [addedIndexes addIndex:idx];
    }
    return addedIndexes;
}

/**
 Returns cache items that are between [startDate; endDate] (included) . 
 endDate is more recent than startDate.
 If endDate is `nil`, only items fresher than startDate are returned.
 @param startDate This is a date in the past where the time frame start. CANNOT BE `nil`.
 @param endDate This is date that can be past or means "NOW". `nil` is used to mean "NOW" (NB: we don't use the date "NOW" 
 from the platform as the items date comes from the server and can be slightly different from the NOW on the iOS device...) 
 */
-(NSArray*) cacheItemsBetweenStartDate:(NSDate*)startDate andEndDate:(NSDate*)endDate {
    NSMutableArray* itemsInTimeFrame = [[NSMutableArray alloc] init];
    [_browsingCache enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id<CKBrowsableItem> c = obj;
        if (startDate!=nil && [c.date compare:startDate]!=NSOrderedAscending 
            && ((endDate!=nil && [c.date compare:endDate]!=NSOrderedDescending) || endDate==nil)) {
            [itemsInTimeFrame addObject:c];
        }
    }];
//    NSLog(@"[BROWSER] Items between %@ and %@ (%ld items) : %@", startDate, endDate, (unsigned long)[itemsInTimeFrame count], itemsInTimeFrame);
    return [itemsInTimeFrame autorelease];
}

-(void) updateBrowsingCacheWithPage:(NSArray*)newPage withCompletionHandler:(CKItemsBrowsingCompletionHandler)hdlr {
    [self updateBrowsingCacheWithPage:newPage startsAtHead:NO withCompletionHandler:hdlr];
} 

/**
 This selector is invoked during browsing cache updates and can be overriden to detect changes between
 current cache items and their most recent update.
 @return items that have been updated.
 @see updateBrowsingCacheWithPage:startsAtHead:withCompletionHandler:
 */
-(NSArray*) applyChangesToCurrentCacheItems:(NSArray*)cachedItems fromFreshItems:(NSArray*)updatedItems {
    return nil;
}

-(void) updateBrowsingCacheWithPage:(NSArray*)newPage correspondingToTimeFrameStart:(NSDate*)timeFrameStart timeFrameEnd:(NSDate*)timeFrameEnd withCompletionHandler:(CKItemsBrowsingCompletionHandler)hdlr {
    // ensure sorting of the new page is OK because the added / updated / removed algorithm relies on the asumption that
    // the page items are sorted (fresh ones at the beginning of the page)
    NSMutableArray * sortedArray = [NSMutableArray arrayWithArray:newPage];
    [sortedArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [((id<CKBrowsableItem>)obj2).date compare:((id<CKBrowsableItem>)obj1).date]; 
    }];
    newPage = sortedArray;
    
    NSMutableArray* addedItems = [NSMutableArray array];
    NSMutableArray* removedItems = [NSMutableArray array];
    NSMutableArray* updatedItems = [NSMutableArray array];
    // Items from the cache that have a matching update in the `newPage`.
    NSMutableArray* alreadyHereMatchingCacheItems = [NSMutableArray array];
    // Items from the `newPage` that correspond to an already existing item in the cache. 
    NSMutableArray* alreadyHereMatchingFreshItems = [NSMutableArray array];
    
    @synchronized(_itemsMutex) {
        // for each item of the new page, checks wether or not it was alread part of the cache.
        for (id<CKBrowsableItem> item in newPage) {
            NSUInteger idx = [_browsingCache indexOfObject:item];
            id<CKBrowsableItem> existingItem = idx==NSNotFound?nil:[_browsingCache objectAtIndex:idx]; 
            if (existingItem) {
                [alreadyHereMatchingCacheItems addObject:existingItem];
                [alreadyHereMatchingFreshItems addObject:item];
            } else {
                // this is a new item, it needs to be added
                [addedItems addObject:item];
            }
        }
        
        NSMutableArray* unchangedItems = [NSMutableArray arrayWithArray:alreadyHereMatchingCacheItems];
        NSArray* changedItems = nil;
        // We first invoke the resync of the alreayd existing items as their date may have changed !
        // This is important as the new date may (or not) be in the timeframe that is being checked 
        // later on in the process.
        if ([alreadyHereMatchingCacheItems count]>0)
            changedItems = [self applyChangesToCurrentCacheItems:alreadyHereMatchingCacheItems fromFreshItems:alreadyHereMatchingFreshItems];
        
        // for each item of the cache in the timeframe of the `newPage`, checks if it's still part of the new page.
        NSArray* existingItemsInTimeFrame = [self cacheItemsBetweenStartDate:timeFrameStart andEndDate:timeFrameEnd];
        for (id<CKBrowsableItem> existingItem in existingItemsInTimeFrame) {
            if ([newPage indexOfObject:existingItem]==NSNotFound) {
                [removedItems addObject:existingItem];
            }
        }
//        NSLog(@"[BROWSER] ======= MERGE RESULT======");
        if ([addedItems count]>0) {
            for (id<CKBrowsableItem> item in addedItems) {
                [self insertItemAtProperIndex:item];
            }
            NSIndexSet* addedIndexes = [self browsingCacheIndexesForItems:addedItems];
//            NSLog(@"[BROWSER][ADDED] (%ld items) INDEXES %@ : %@", (unsigned long)[addedIndexes count], addedIndexes, addedItems);
            __unsafe_unretained CKItemsBrowser *weakSelf = self;
            dispatch_block_t block = ^{
                [_delegate itemsBrowser:weakSelf didAddCacheItems:addedItems atIndexes:addedIndexes];
            };
            if([NSThread isMainThread])
                block();
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    block();
                });
            }
        }
        if ([removedItems count]>0) {
            NSIndexSet* removedIndexes = [self browsingCacheIndexesForItems:removedItems];
            [_browsingCache removeObjectsAtIndexes:removedIndexes];
//            NSLog(@"[BROWSER][REMOVED] (%ld items) INDEXES %@ : %@", (unsigned long)[removedIndexes count], removedIndexes, removedItems);
            __unsafe_unretained CKItemsBrowser *weakSelf = self;
            dispatch_block_t block = ^{
                [_delegate itemsBrowser:weakSelf didRemoveCacheItems:removedItems atIndexes:removedIndexes];
            };
            if([NSThread isMainThread])
                block();
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    block();
                });
            }
        }
        
        if ([changedItems count]>0) {
            // Checks what happened to those updated items: just some "classic" properties or even the date property ? (in case, a reordering
            // of the cache is required).
            [self checkForMisplacedItemsAndReorderIfAny:changedItems];
            __unsafe_unretained CKItemsBrowser *weakSelf = self;
            dispatch_block_t block = ^{
                [_delegate itemsBrowser:weakSelf didUpdateCacheItems:changedItems atIndexes:[self browsingCacheIndexesForItems:changedItems]];
            };
            if([NSThread isMainThread])
                block();
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    block();
                });
            };
            [unchangedItems removeObjectsInArray:changedItems];
            [updatedItems addObjectsFromArray:changedItems];
//            NSLog(@"[BROWSER][UPDATED] (%ld items) %@", (unsigned long)[changedItems count], changedItems);
        }
//        NSLog(@"[BROWSER][UNCHANGED] (%ld items) %@", (unsigned long)[unchangedItems count], unchangedItems);
    }
    //NSLog(@"[BROWSER] AFTER MERGE, cache is (%ld items)%@", (unsigned long)[_browsingCache count], _browsingCache);
    if (hdlr){
        if(![NSThread isMainThread]){
            dispatch_async(dispatch_get_main_queue(), ^{
                hdlr(addedItems, removedItems, updatedItems, nil);
            });
        } else {
            hdlr(addedItems, removedItems, updatedItems, nil);
        }
    }
    
}

/**
 Updates the browsing cache with items from the given page.
 @param startsAtHead `YES` if the given page represents the browsing cache head. (this means, that the first item of this new page
 must also be the first item of the cache **without any other item before it ! **
 */
-(void) updateBrowsingCacheWithPage:(NSArray*)newPage startsAtHead:(BOOL)startsAtHead withCompletionHandler:(CKItemsBrowsingCompletionHandler)hdlr {
    // ensure sorting of the new page is OK because the added / updated / removed algorithm relies on the asumption that
    // the page items are sorted (fresh ones at the beginning of the page)
    
    NSMutableArray * sortedArray = [NSMutableArray arrayWithArray:newPage];
    [sortedArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [((id<CKBrowsableItem>)obj2).date compare:((id<CKBrowsableItem>)obj1).date]; 
    }];
    newPage = sortedArray;
    
    NSDate* timeFrameStart = [newPage count]>0?((NSObject<CKBrowsableItem>*)[newPage lastObject]).date:nil;
    NSDate* timeFrameEnd = [newPage count]>0 && !startsAtHead?((NSObject<CKBrowsableItem>*)[newPage firstObject]).date:nil;
    
    [self updateBrowsingCacheWithPage:newPage correspondingToTimeFrameStart:timeFrameStart timeFrameEnd:timeFrameEnd withCompletionHandler:hdlr];
}

/**
 
 */
-(NSArray*) checkForMisplacedItemsAndReorderIfAny :(NSArray*) items {
    NSMutableArray* misplacedItems = [NSMutableArray array];
    NSMutableArray* movedIndexes = [NSMutableArray array];
    NSMutableArray* movedIndexesNew = [NSMutableArray array];
    @synchronized (_itemsMutex) {
        for (id<CKBrowsableItem> item in items) {
            NSInteger currentIndex = [_browsingCache indexOfObject:item];
            NSInteger rightTheoricalIndex = [self properIndexForItem:item];
            if (currentIndex!=rightTheoricalIndex && [self moveItemFromIndex:currentIndex toIndex:rightTheoricalIndex]) {
                [misplacedItems addObject:item];
                [movedIndexes addObject:[NSNumber numberWithInteger:currentIndex]];
                [movedIndexesNew addObject:[NSNumber numberWithInteger:rightTheoricalIndex]];
            }
        }
        if ([movedIndexes count]>0) {
            // not really safe to notify from the synchronized block but what are the alternatives here ? :/
            __unsafe_unretained CKItemsBrowser *weakSelf = self;
            dispatch_block_t block = ^{
                [_delegate itemsBrowser:weakSelf didReorderCacheItemsAtIndexes:movedIndexes toIndexes:movedIndexesNew];
            };
            if([NSThread isMainThread])
                block();
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    block();
                });
            }
        }
    }
    return misplacedItems;
}

#pragma mark - Completion Handlers invocation.

-(void) errorFeedbackWithParams:(NSDictionary*)params {
    NSError* error = [params objectForKey:@"error"];
    CKItemsBrowsingCompletionHandler hdlr = [params objectForKey:@"completionHandler"];
    if (hdlr){
        if(![NSThread isMainThread]){
            dispatch_async(dispatch_get_main_queue(), ^{
                hdlr(nil, nil, nil, error);
            });
        }
    }
}

-(void) updateBrowsingCacheWithParams:(NSDictionary*)params {
    NSArray* page = params[@"page"];
    BOOL startsAtFirstPage = [params[@"isHead"] boolValue];
    NSDate* timeFrameStart = params[@"timeFrameStart"];
    NSDate* timeFrameEnd = params[@"timeFrameEnd"];
    CKItemsBrowsingCompletionHandler hdlr = params[@"completionHandler"];
    if (timeFrameStart) {
        [self updateBrowsingCacheWithPage:page correspondingToTimeFrameStart:timeFrameStart timeFrameEnd:timeFrameEnd withCompletionHandler:hdlr];
    } else {
        [self updateBrowsingCacheWithPage:page startsAtHead:startsAtFirstPage  withCompletionHandler:hdlr];
    }
}

#pragma mark - jobs performing

-(void) invalidatePendingJobs {
    @synchronized (_itemsMutex) {
        for (CKBrowsingOperation* job in _pendingBrowsingJobs)
            job.outdated = YES;
//        NSLog(@"[BROWSER] pending jobs invalidated %@", _pendingBrowsingJobs);
    }
}

-(void) performJob:(CKBrowsingOperation*)localJob withCompletionHandler:(CKItemsBrowsingCompletionHandler)completionHandler {
    // normally the caller is already in a synchronized 
    @synchronized (_itemsMutex) {
        //NSThread* callerThread = [NSThread currentThread];
        [_pendingBrowsingJobs addObject:localJob];
        // So that there's no cross reference between localJob and its completion block (with __block, the tmp is not retained)
        __block CKBrowsingOperation* tmp = localJob;
        [localJob setCompletionBlock:^{
            NSArray* page = tmp.jobResultPage;
            NSError* error = tmp.jobResultError;
            if(!error) {
                // Everything went fine
                if([tmp respondsToSelector:@selector(hasMorePages)]){
                    _hasMorePages = tmp.hasMorePages;
                } else {
                    BOOL hasMore = ([page count]==_itemsCountPerPage);
                    _hasMorePages = hasMore;
                }
//                NSLog(@"[BROWSER] %@ performed: %lu items found. More pages:%@", tmp, (unsigned long)[page count], NSStringFromBOOL(_hasMorePages));
                NSMutableDictionary* params = [NSMutableDictionary dictionaryWithDictionary:@{@"page":page, @"isHead":[NSNumber numberWithBool:(tmp.startIndex==0)]}];
                
                // the time frame start can be nil if a valid page (no error returned while browsing) contains 0 items.
                if (tmp.timeFrameStart)
                    [params setObject:tmp.timeFrameStart forKey:@"timeFrameStart"];
                if (tmp.timeFrameEnd)
                    [params setObject:tmp.timeFrameEnd forKey:@"timeFrameEnd"];
                if (completionHandler)
                    [params setObject:[[completionHandler copy] autorelease] forKey:@"completionHandler"];
                [self updateBrowsingCacheWithParams:params];
            } else {
//                NSLog(@"[BROWSER] %@ error while performed: %@", tmp, [error localizedDescription]);
                if (completionHandler){
                    [self errorFeedbackWithParams:@{@"error":error, @"completionHandler":[[completionHandler copy] autorelease]}];
                } else {
                    [self errorFeedbackWithParams:@{@"error":error}];
                }
            }
            [_pendingBrowsingJobs removeObject:tmp];
        }];
        [_browsingJobsQueue addOperation:localJob];
    }
}

@end
