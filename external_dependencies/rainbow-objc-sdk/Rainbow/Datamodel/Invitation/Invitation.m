/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Invitation.h"
#import "Invitation+Internal.h"
#import "Tools.h"

@implementation Invitation

+(NSString *) stringFromInvitationDirection:(InvitationDirection) direction {
    switch (direction) {
        case InvitationDirectionSent:
            return @"sent";
        case InvitationDirectionReceived:
            return @"received";
    }
    // If we don't return a default value here, the usage of this function in [NSString stringWithFormat:]
    // (for example in our [Invitation description] function)
    // might crash when giving an unknown direction !
    return @"unknown";
}

+(InvitationDirection) invitationDirectionFromString:(NSString *) direction {
    if ([direction isEqualToString:@"sent"])
        return InvitationDirectionSent;
    if ([direction isEqualToString:@"received"])
        return InvitationDirectionReceived;
    
    NSLog(@"Unknown invitation direction %@", direction);
    NSAssert(NO, @"Unknown invitation direction");
    return -1;
}

+(NSString *) stringFromInvitationStatus:(InvitationStatus) status {
    switch (status) {
        case InvitationStatusPending:
            return @"pending";
        case InvitationStatusAccepted:
            return @"accepted";
        case InvitationStatusAutoAccepted:
            return @"auto-accepted";
        case InvitationStatusDeclined:
            return @"declined";
        case InvitationStatusDeleted:
            return @"deleted";
        case InvitationStatusCanceled:
            return @"canceled";
        case InvitationStatusFailed:
            return @"Failed";
    }
    // If we don't return a default value here, the usage of this function in [NSString stringWithFormat:]
    // (for example in our [Invitation description] function)
    // might crash when giving an unknown status !
    return @"unknown";
}

+(InvitationStatus) invitationStatusFromString:(NSString *) status {
    if ([status isEqualToString:@"pending"])
        return InvitationStatusPending;
    if ([status isEqualToString:@"accepted"])
        return InvitationStatusAccepted;
    if ([status isEqualToString:@"auto-accepted"])
        return InvitationStatusAutoAccepted;
    if ([status isEqualToString:@"declined"])
        return InvitationStatusDeclined;
    if([status isEqualToString:@"deleted"])
        return InvitationStatusDeleted;
    if([status isEqualToString:@"canceled"])
        return InvitationStatusCanceled;
    if([status isEqualToString:@"failed"])
        return InvitationStatusFailed;
    
    NSLog(@"Unknown invitation status %@", status);
    NSAssert(NO, @"Unknown invitation status");
    return -1;
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"Invitation %p <id: %@ email: %@ direction: %@ status: %@ peer: %p>", self, _invitationID, _email, [Invitation stringFromInvitationDirection:_direction], [Invitation stringFromInvitationStatus:_status], _peer];
    }
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary new];
    if ([Invitation stringFromInvitationStatus:_status] ) {
         [dic setObject:[Invitation stringFromInvitationStatus:_status] forKey:@"status"];
    }
    if (_invitationID) {
        [dic setObject:_invitationID forKey:@"id"];
    }
   
    if (_date) {
        [dic setObject:_date forKey:@"invitingDate"];
    }
    if ([Invitation stringFromInvitationDirection:_direction] ) {
        [dic setObject:[Invitation stringFromInvitationDirection:_direction]  forKey:@"direction"];
    }
    
    if(_direction == InvitationDirectionSent){
        if (_email) {
             [dic setObject:_email forKey:@"invitedUserEmail"];
        }
       
        if(_peer && _peer.rainbowID && ![_peer.rainbowID containsString:@"tmp_invited"])
            [dic setObject:_peer.rainbowID forKey:@"invitedUserId"];
    } else {
        if (_email) {
            [dic setObject:_email forKey:@"invitingUserEmail"];
        }
        
        if(_peer.rainbowID)
            [dic setObject:_peer.rainbowID forKey:@"invitingUserId"];
    }
    if(_peer.jid)
        [dic setObject:_peer.jid forKey:@"peerJid"];
    
    return dic;
}

@end
