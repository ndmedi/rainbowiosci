/*
 * Rainbow
 *
 * Copyright (c) 2016-2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RTCCall.h"
#import <WebRTC/WebRTC.h>
#import "XMPPJID.h"
#import <CallKit/CallKit.h>
#import "CallProtocol.h"


@interface RTCCall () <CallProtocol>

@property (nonatomic, readwrite, strong) NSUUID *callID;

@property(nonatomic, readwrite, strong) NSString *jingleSessionID;

@property(nonatomic, readwrite, strong) NSString *webRTCMetricsID;

@property(nonatomic, readwrite) RTCCallFeatureFlags features;

@property (nonatomic, readwrite) BOOL isRecording;

// Pure internal properties

-(NSString *) fullJID;

@property(nonatomic, strong) NSString *resource;

@property(nonatomic, strong) RTCPeerConnection *peerConnection;

@property(nonatomic, strong) NSMutableArray<RTCIceCandidate *> *candidates;

@property(nonatomic, strong) NSTimer *outgoingCallTimer;

@property (nonatomic, strong) CXCallAction *actionToFullFill;

@property (nonatomic, strong) RTCSessionDescription *offer;

@property (nonatomic, strong) RTCSessionDescription *answer;

@property (nonatomic) BOOL iceFailed;

// Property use to determine if the call is a janus fake call used by desktop sharing
@property (nonatomic) BOOL fakeCall;
@property (nonatomic, strong) NSUUID* realCallID;

// List of linked calls
// The ID of a linked call, a linked call is an other RTCCall that is linked to that object, it's used for janus call when desktop sharing is added
@property (nonatomic, strong) NSMutableArray <RTCCall *> *linkedCalls;

@property (nonatomic, strong) RTCMediaStream *stream;

@property (nonatomic) BOOL isModifingMedia;
@end
