/*
 * Rainbow
 *
 * Copyright (c) 2016-2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RTCCall+Internal.h"
#import "Tools.h"
#import "Call+Internal.h"

@implementation RTCCall

-(instancetype) init {
    self = [super init];
    if (self) {
        _candidates = [NSMutableArray new];
        _features = 0;
        _isRecording = NO;
        _linkedCalls = [NSMutableArray new];
        self.capabilities = CallCapabilitiesAll;
    }
    return self;
}

-(void) dealloc {
    [_peerConnection close];
    _peerConnection = nil;
    [_outgoingCallTimer invalidate];
    _outgoingCallTimer = nil;
}

-(BOOL) isAudioEnabled {
    return (self.features & RTCCallFeatureAudio) != 0;
}

-(BOOL) isVideoEnabled {
    return [self isRemoteVideoEnabled] || [self isLocalVideoEnabled];
}

-(BOOL) isRemoteVideoEnabled {
    return (self.features & RTCCallFeatureRemoteVideo) != 0;
}

-(BOOL) isSharingEnabled {
    return (self.features & RTCCallFeatureRemoteSharing) != 0;
}

-(BOOL) isLocalVideoEnabled {
    return (self.features & RTCCallFeatureLocalVideo) != 0;
}

-(NSString *) fullJID {
    return [[XMPPJID jidWithString:self.peer.rtcJid resource:self.resource] full];
}

- (BOOL) isMediaPillarCall {
    return (self.features & RTCCallFeatureMediaPillar);
}


- (NSString *)description {
    return [NSString stringWithFormat:@"RTCCall %p : <CallId %@ (sessionId %@) with %@, resource %@, isIncoming %@, status %@, audio %@, video %@, sharing %@, linkedCallIdCount %ld>",
            self,
            self.callID,
            self.jingleSessionID,
            self.peer.rtcJid,
            self.resource,
            NSStringFromBOOL(self.isIncoming),
            [Call stringForStatus:self.status],
            NSStringFromBOOL(self.isAudioEnabled),
            NSStringFromBOOL(self.isVideoEnabled),
            NSStringFromBOOL(self.isSharingEnabled),
            [self.linkedCalls count]];
}

@end
