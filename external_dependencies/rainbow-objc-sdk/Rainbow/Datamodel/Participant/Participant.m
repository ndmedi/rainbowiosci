/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Participant.h"
#import "Participant+Internal.h"
#import "NSDate+JSONString.h"

@implementation Participant


#pragma mark - enum parser

+(ParticipantPrivilege) stringToPrivilege:(NSString *) privilege {
    if ([privilege isEqualToString:@"user"])
        return ParticipantPrivilegeUser;
    if ([privilege isEqualToString:@"moderator"])
        return ParticipantPrivilegeModerator;
    if ([privilege isEqualToString:@"owner"])
        return ParticipantPrivilegeOwner;
    if ([privilege isEqualToString:@"guest"])
        return ParticipantPrivilegeGuest;
    return ParticipantPrivilegeUnknown;
}

-(NSString *) privilegeString {
    switch(_privilege) {
        case ParticipantPrivilegeOwner:
            return @"owner";
        case ParticipantPrivilegeUser:
            return @"user";
        case ParticipantPrivilegeModerator:
            return @"moderator";
        case ParticipantPrivilegeGuest:
            return @"guest";
        case ParticipantPrivilegeUnknown:
            return @"unknown";
    }
    return nil;
}

+(ParticipantStatus) stringToStatus:(NSString *) status {
    if ([status isEqualToString:@"invited"])
        return ParticipantStatusInvited;
    if ([status isEqualToString:@"rejected"])
        return ParticipantStatusRejected;
    if ([status isEqualToString:@"accepted"])
        return ParticipantStatusAccepted;
    if ([status isEqualToString:@"unsubscribed"])
        return ParticipantStatusUnsubscribed;
    if([status isEqualToString:@"deleted"])
        return ParticipantStatusDeleted;
    return ParticipantStatusUnknown;
}

-(NSString *) statusString {
    switch(_status) {
        case ParticipantStatusInvited:
            return @"invited";
        case ParticipantStatusRejected:
            return @"rejected";
        case ParticipantStatusAccepted:
            return @"accepted";
        case ParticipantStatusUnsubscribed:
            return @"unsubscribed";
        case ParticipantStatusDeleted:
            return @"deleted";
        case ParticipantStatusUnknown:
            return @"unknown";
    }
    return nil;
}

+(NSString *) stringForPrivilege:(ParticipantPrivilege) privilege {
    switch (privilege) {
        case ParticipantPrivilegeUnknown: {
            return @"unknown";
            break;
        }
        case ParticipantPrivilegeUser: {
            return @"user";
            break;
        }
        case ParticipantPrivilegeModerator: {
            return @"moderator";
            break;
        }
        case ParticipantPrivilegeGuest: {
            return @"guest";
            break;
        }
        // For owner we return moderator, little internal ack
        case ParticipantPrivilegeOwner: {
            return @"moderator";
            break;
        }
    }
    // If we don't return a default value here, the usage of this function in [NSString stringWithFormat:]
    // might crash when giving an unknown privilege !
    return @"unknown";
}

+(NSString *) stringForStatus:(ParticipantStatus) status {
    switch (status) {
        case ParticipantStatusUnknown: {
            return @"unknown";
            break;
        }
        case ParticipantStatusInvited: {
            return @"invited";
            break;
        }
        case ParticipantStatusRejected: {
            return @"rejected";
            break;
        }
        case ParticipantStatusAccepted: {
            return @"accepted";
            break;
        }
        case ParticipantStatusUnsubscribed: {
            return @"unsubscribed";
            break;
        }
        case ParticipantStatusDeleted: {
            return @"deleted";
            break;
        }
    }
    // If we don't return a default value here, the usage of this function in [NSString stringWithFormat:]
    // might crash when giving an unknown status !
    return @"unknown";
}

-(BOOL) isEqual:(Participant *) aParticipant {
    if(self == aParticipant)
        return YES;
    if([aParticipant isKindOfClass:[Participant class]])
        return [self.contact.rainbowID isEqualToString:aParticipant.contact.rainbowID];
    
    return NO;
}

-(NSUInteger)hash {
    return [self.contact.rainbowID hash];
}

# pragma mark description

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"<Participant %p: %@ privilege %@ status %@>", self, _contact, [self privilegeString], [self statusString]];
    }
}

-(NSString *) debugDescription {
    @synchronized(self){
        return [NSString stringWithFormat:@"\t<Participant %p: %@ privilege %@ status %@>", self, [_contact debugDescription], [self privilegeString], [self statusString]];
    }
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *participantsDic = [NSMutableDictionary dictionary];
    
    if(self.contact.rainbowID)
        [participantsDic setObject:self.contact.rainbowID forKey:@"userId"];
    if(self.contact.jid)
        [participantsDic setObject:self.contact.jid forKey:@"jid_im"];
    
    [participantsDic setObject:[Participant stringForPrivilege:self.privilege] forKey:@"privilege"];
    [participantsDic setObject:[Participant stringForStatus:self.status] forKey:@"status"];
    [participantsDic setObject:[NSDate jsonStringFromDate:self.addedDate] forKey:@"additionDate"];
    return participantsDic;
}

@end
