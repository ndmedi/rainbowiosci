/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Message.h"
#import "MessageInternal.h"
#import "Tools.h"
#import "NSString+Emoji.h"
#import "Tools.h"
#import "NSDate+JSONString.h"

@interface Message ()
@property (nonatomic, strong) NSMutableDictionary *deliveryDateForState;
@property (nonatomic, readwrite) MessageDeliveryState state;
@end

@implementation Message

-(instancetype) init {
    self = [super init];
    if(self){
        _timestamp = [NSDate date];
        _deliveryDateForState = [NSMutableDictionary new];
        _hasBeenPresentedInPush = NO;
    }
    return self;
}

-(void) dealloc {
    _timestamp = nil;
    [_deliveryDateForState removeAllObjects];
    _deliveryDateForState = nil;
}

-(NSString *) description {
    @synchronized(self){
#if DEBUG
        return [NSString stringWithFormat:@"Message %p <with: %@  via: %@ Body: %@ (Outgoing %@) msgId %@ timestamp %@ deliveryState %@ deliveriesInfo %@ message  presentedInPush %@ isResentMessage %@ attachment %@ callLog %@>",self, _peer.jid, _via.jid, _body, NSStringFromBOOL(_isOutgoing), _messageID, _timestamp, [Message stringForDeliveryState:_state], _deliveryDateForState, NSStringFromBOOL(_hasBeenPresentedInPush), NSStringFromBOOL(_isResentMessage), _attachment, _callLog];
#else
        return [NSString stringWithFormat:@"Message %p <with: %@  via: %@ (Outgoing %@) msgId %@ timestamp %@ deliveryState %@ presentedInPush %@  isResentMessage %@ attachment %@ callLog %@>",self, _peer.jid, _via.jid, NSStringFromBOOL(_isOutgoing), _messageID, _timestamp, [Message stringForDeliveryState:_state], NSStringFromBOOL(_hasBeenPresentedInPush), NSStringFromBOOL(_isResentMessage), _attachment, _callLog];
#endif
    }
}

-(NSString *) debugDescription {
    @synchronized(self){
        return [NSString stringWithFormat:@"Message %p <with %@ via %@ Body : %@ (Outgoing %@) msgId %@>", self, _peer.jid, _via.jid, _body, NSStringFromBOOL(_isOutgoing), _messageID];
    }
}

-(BOOL) isEqual:(Message *) message {
    if([message isKindOfClass:[Message class]]){
        return [_messageID isEqualToString:message.messageID];
    }
    return NO;
}

-(NSDate *) date {
    return self.timestamp;
}

-(NSString *) body {
    switch(self.type) {
        case MessageTypeUnknown:
        case MessageTypeChat:
        case MessageTypeGroupChat:
        case MessageTypeGroupChatEvent:
        case MessageTypeCallRecordingStart:
        case MessageTypeCallRecordingStop: {
            return [_body stringByReplacingEmojiCheatCodesWithUnicode];
            break;
        }
        case MessageTypeWebRTC: {
            if(_body.length > 0){
                NSArray<NSString *> *parts = [_body componentsSeparatedByString:@"||"];
                // Example of webrtc body : "missedCall||21 juin 2016 10:44"
                if ([parts count] >= 2) {
                    return [NSString stringWithFormat:@"%@\n%@", NSLocalizedString(parts[0], nil), parts[1]];
                }
            } else {
                return [self bodyForWebRTCCallLog];
            }
            break;
        }
        case MessageTypeFileTransfer: {
            NSArray<NSString *> *parts = [_body componentsSeparatedByString:@"||"];
            // Example of file transfer body : "Hello||Capture d’écran 2016-06-06 à 13.42.45.jpg||188808||image/jpeg||refused||12345"
            // The last param seems to be a transferID
            if ([parts count] >= 5) {
                NSString *message = [parts[0] stringByReplacingEmojiCheatCodesWithUnicode];
                NSString *fileSize = [Tools byteSizeToFormattedString:parts[2]];
                return [NSString stringWithFormat:@"%@\n%@ (%@)\n%@", message, parts[1], fileSize, NSLocalizedString(parts[4], nil)];
            }
            break;
        }
    }
    return [_body stringByReplacingEmojiCheatCodesWithUnicode];
}

-(NSString *) bodyForWebRTCCallLog {
    if(!self.callLog)
        return nil;
    NSString *messageText = @"";
    switch (self.callLog.state) {
        case CallLogStateMissed:{
            if(self.callLog.isOutgoing){
                messageText = [NSString stringWithFormat:NSLocalizedString(@"You tried calling %@", nil), self.peer.displayName];
            } else {
                messageText = [NSString stringWithFormat:NSLocalizedString(@"%@ tried calling you", nil), self.peer.displayName];
            }
            break;
        }
        case CallLogStateAnswered:{
            if(self.callLog.isOutgoing){
                messageText = [NSString stringWithFormat:NSLocalizedString(@"You called %@", nil), self.peer.displayName];
            } else {
                messageText = [NSString stringWithFormat:NSLocalizedString(@"%@ called you", nil), self.peer.displayName];
            }
            break;
        }
        case CallLogStateUnknown:
        default:
            messageText = NSLocalizedString(@"Unknown event",nil);
            break;
    }
    
    return messageText;
    
}

+(MessageType) messageTypeForBody:(NSString *) body {
    NSArray<NSString *> *parts = [body componentsSeparatedByString:@"||"];
    if ([parts count] >= 2)
        return MessageTypeWebRTC;
    else if ([parts count] >= 5)
        return MessageTypeFileTransfer;
    else
        return MessageTypeChat;
}

-(void) resetDeliveryStateAndDate {
    _state = MessageDeliveryStateSent;
    [_deliveryDateForState removeAllObjects];
}

/**
 *  This function check the state before setting it.
 *  If the sequence is not authorized, nothing is done 
 *  (for example set state to received when its already read)
 *  In addition, the timestamp is only stored if we don't already
 *  have it for the given state.
 *  (if we set 2 times for read state, the first timestamp is kept)
 *
 *  @param state The new state desired
 *  @param date  The datetime when it happened
 */
-(void) setDeliveryState:(MessageDeliveryState) state atTimestamp:(NSDate *) date {
    
    // Finite State Machine of message delivery
    switch(_state) {
        case MessageDeliveryStateSent: {
            // current state is sent, we can go to [failed,delivered,received,read]
            if (state == MessageDeliveryStateFailed || state == MessageDeliveryStateDelivered || state == MessageDeliveryStateReceived || state == MessageDeliveryStateRead) {
                _state = state;
            }
            break;
        }
        case MessageDeliveryStateDelivered: {
            // current state is delivered, we can go to [received,read]
            if (state == MessageDeliveryStateReceived || state == MessageDeliveryStateRead) {
                _state = state;
            }
            break;
        }
        case MessageDeliveryStateReceived: {
            // current state is received, we can go to [read]
            if (state == MessageDeliveryStateRead) {
                _state = state;
            }
            break;
        }
        case MessageDeliveryStateRead: {
            break;
        }
    }
    
    // We store the date, if new date is not nil and we don't have already a date for this state.
    if (date && ![_deliveryDateForState objectForKey:[NSNumber numberWithInt:_state]]) {
        [_deliveryDateForState setObject:date forKey:[NSNumber numberWithInt:_state]];
    }
}

-(NSDate *) deliveryDateForState:(MessageDeliveryState) state {
    return [_deliveryDateForState objectForKey:[NSNumber numberWithInt:state]];
}

+(NSString *) stringForDeliveryState:(MessageDeliveryState) deliveryState {
    switch (deliveryState) {
        case MessageDeliveryStateSent: {
            return @"Sent";
            break;
        }
        case MessageDeliveryStateDelivered: {
            return @"Delivered";
            break;
        }
        case MessageDeliveryStateReceived: {
            return @"Received";
            break;
        }
        case MessageDeliveryStateRead: {
            return @"Read";
            break;
        }
        case MessageDeliveryStateFailed: {
            return @"Failed";
            break;
        }
    }
    // If we don't return a default value here, the usage of this function in [NSString stringWithFormat:]
    // might crash when giving an unknown state !
    return @"Unknown";
}

+(MessageGroupChatEventType) messageGroupChatEventTypeFromString:(NSString *)eventName {
    MessageGroupChatEventType eventType = MessageGroupChatEventNone;
    
    if([eventName isEqualToString:@"invitation"]){
        eventType = MessageGroupChatEventInvitation;
    } else if([eventName isEqualToString:@"welcome"]){
        eventType = MessageGroupChatEventWelcome;
    } else if([eventName isEqualToString:@"join"]){
        eventType = MessageGroupChatEventJoin;
    } else if([eventName isEqualToString:@"leave"]){
        eventType = MessageGroupChatEventLeave;
    } else if([eventName isEqualToString:@"close"]){
        eventType = MessageGroupChatEventClose;
    } else if([eventName isEqualToString:@"conferenceAdd"]){
        eventType = MessageGroupChatEventConferenceAdd;
    } else if([eventName isEqualToString:@"conferenceRemove"]){
        eventType = MessageGroupChatEventConferenceRemove;
    }
    
    return eventType;
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    if(_messageID)
        [dic setObject:_messageID forKey:@"id"];
    
//    if (_timestamp) {
//        [dic setObject:_timestamp  forKey:@"lastMessageDate"];
//    }
    
    if(_body){
        if(_body.length > 0)
            [dic setObject:_body forKey:@"lastMessageText"];
        
        if(_attachment){
            [dic setObject:_attachment.url  forKey:@"attachmentURL"];
            [dic setObject:_attachment.fileName  forKey:@"fileName"];
            if (_attachment.data) {
                [dic setObject:_attachment.data  forKey:@"attachmentData"];
            }
        }
    }
  
    return dic;
}

@end
