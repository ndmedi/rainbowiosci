/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Message.h"

@interface Message ()
@property (nonatomic, readwrite) MessageType type;
@property (nonatomic, readwrite, strong) Peer *peer;
@property (nonatomic, readwrite, strong) Peer *via;
@property (nonatomic, readwrite, strong) NSString *messageID;
@property (nonatomic, readwrite, strong) NSString *mamMessageID;
@property (nonatomic, readwrite, strong) NSString *body;
@property (nonatomic, readwrite, strong) NSDate *timestamp;
@property (nonatomic, readwrite) BOOL isOutgoing;
@property (nonatomic, readwrite) BOOL isComposing;
@property (nonatomic, readwrite) BOOL isCarbonned;
@property (nonatomic, readwrite) BOOL isReceivedCarbon;
@property (nonatomic, readwrite) BOOL isSentCarbon;
@property (nonatomic, readwrite) BOOL hasBeenPresentedInPush;
@property (nonatomic, readwrite) BOOL isOfflineMessage;
@property (nonatomic, readwrite) BOOL isResentMessage;
@property (nonatomic, readwrite) MessageGroupChatEventType groupChatEventType;
@property (nonatomic, readwrite, strong) Peer *groupChatEventPeer;
@property (nonatomic, readwrite, strong) CallLog *callLog;
@property (nonatomic, readwrite, strong) File *attachment;
@property (nonatomic, readwrite, strong) NomadicStatus *nomadicStatus;

-(void) setDeliveryState:(MessageDeliveryState) state atTimestamp:(NSDate *) date;

+(MessageType) messageTypeForBody:(NSString *) body;

// Internal private method that must not be used !!
// There is only one class that is allowed to use it! This is xmpp service in one specific case when we create a message from mam  message
-(void) resetDeliveryStateAndDate;

-(NSDictionary *) dictionaryRepresentation;
@end
