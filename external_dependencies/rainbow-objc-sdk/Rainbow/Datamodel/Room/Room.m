/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Room+Internal.h"
#import "Tools.h"
#import "NSDate+JSONString.h"
#import "Participant+Internal.h"
#import "ConfEndpoint+Internal.h"

@implementation Room
-(instancetype) init {
    if(self = [super init]){
        _participants = [NSMutableArray new];
        _isMyRoom = NO;
        _isAdmin = NO;
        _customData = [NSDictionary new];
    }
    return self;
}

-(NSString *) rtcJid {
    return _conference.jingleJid;
}

-(void) dealloc {
    [self removeAllParticipants];
    _participants = nil;
}

-(BOOL) isEqual:(Room *) aRoom {
    if(self == aRoom)
        return YES;
    if(![[self class] isEqual:[aRoom class]])
        return NO;
    return [self.rainbowID isEqualToString:aRoom.rainbowID];
}

-(NSUInteger)hash {
    return [self.rainbowID hash];
}

-(void) addParticipant:(Participant *) participant {
    if (!participant)
        return;
    [self willChangeValueForKey:kRoomParticipantsKey];
    @synchronized (_participants) {
        [(NSMutableArray *)_participants addObject:participant];
    }
    [self didChangeValueForKey:kRoomParticipantsKey];
}

-(void) removeParticipant:(Participant *) participant {
    if (!participant)
        return;
    [self willChangeValueForKey:kRoomParticipantsKey];
    @synchronized (_participants) {
        [(NSMutableArray *)_participants removeObject:participant];
    }
    [self didChangeValueForKey:kRoomParticipantsKey];
}

-(void) removeAllParticipants {
    [self willChangeValueForKey:kRoomParticipantsKey];
    @synchronized (_participants) {
        [(NSMutableArray *)_participants removeAllObjects];
    }
    [self didChangeValueForKey:kRoomParticipantsKey];
}

-(NSArray <Participant*> *) participants {
    @synchronized (_participants) {
        NSMutableSet *set = [NSMutableSet set];
        [set addObjectsFromArray:_participants];
        return [set allObjects];
    }
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"<Room %p: roomJID :%@   [OWNER %@] Name: %@ topic: %@ creator: %@\n\tparticipants %@ conference %@ myStatusInRoom %@>", self, self.jid, NSStringFromBOOL(_isMyRoom), self.displayName, _topic, _creator, _participants, _conference, [Participant stringForStatus:self.myStatusInRoom]];
    }
}


-(NSString *) debugDescription {
    @synchronized(self){
        return [NSString stringWithFormat:@"\tRoom <%p roomJID :%@ [OWNER %@] Name: %@ topic: %@ \n\tcreator: %@\n\tConference %@ myStatusInRoom %@>", self, self.jid, NSStringFromBOOL(_isMyRoom), self.displayName, _topic, [_creator debugDescription], [_conference debugDescription], [Participant stringForStatus:self.myStatusInRoom]];
    }
}

-(Participant *) participantFromContact:(Contact *) contact {
    @synchronized (_participants) {
        __block Participant *theParticipant = nil;
        [_participants enumerateObjectsUsingBlock:^(Participant * aParticipant, NSUInteger idx, BOOL * stop) {
            if([aParticipant.contact isEqual:contact]){
                theParticipant = aParticipant;
                *stop = YES;
            }
        }];
        
        return theParticipant;
    }
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(self.jid)
        [dic setObject:self.jid forKey:@"jid"];
    if(self.rainbowID)
        [dic setObject:self.rainbowID forKey:@"id"];
    if(self.displayName)
        [dic setObject:self.displayName forKey:@"name"];
    if(self.topic)
        [dic setObject:_topic forKey:@"topic"];
    if(_creationDate)
        [dic setObject:[NSDate jsonStringFromDate:_creationDate] forKey:@"creationDate"];
    if(self.lastAvatarUpdateDate)
        [dic setObject:[NSDate jsonStringFromDate:self.lastAvatarUpdateDate] forKey:@"lastAvatarUpdateDate"];
    else
        [dic setObject:@"" forKey:@"lastAvatarUpdateDate"];
    
    [dic setObject:[Participant stringForStatus:self.myStatusInRoom] forKey:@"myStatusInRoom"];
    
    if(_conference){
        if(_conference.endpoint && _conference.status != ConferenceStatusUnknown)
            [dic setObject:@[[_conference.endpoint dictionaryRepresentation]] forKey:@"confEndpoints"];
        [dic setObject:[_conference dictionaryRepresentation] forKey:@"conference"];
    }

    
    if(_participants.count > 0){
        NSMutableArray *participants = [NSMutableArray array];
        @synchronized (_participants) {
            for (Participant *aParticipant in _participants) {
                if(aParticipant.contact) {
                    if(aParticipant.contact.rainbowID.length > 0 && aParticipant.contact.jid.length > 0){
                        [participants addObject:[aParticipant dictionaryRepresentation]];
                    }
                }
            }
        }
        
        [dic setObject:participants forKey:@"users"];
    }
    
    if(_visibility == RoomVisibilityPrivate)
        [dic setObject:@"private" forKey:@"visibility"];
    else
        [dic setObject:@"public" forKey:@"visibility"];
    
    if(_creator && _creator.rainbowID)
        [dic setObject:_creator.rainbowID forKey:@"creator"];
    
    if(_customData)
       [dic setObject:_customData forKey:@"customData"];
    
    if (_guestEmails) {
        [dic setObject:_guestEmails forKey:@"guestEmails"];
    }
    
    return dic;
}

-(BOOL) isPGIConferenceRoom {
    return _conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio;
}

-(BOOL) isWebRTCConferenceRoom {
    return _conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC;
}

@end
