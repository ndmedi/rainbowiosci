/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Peer+Internal.h"
#import "Room.h"
#import "Participant+Internal.h"
#import "Conference+Internal.h"

@interface Room ()
@property (nonatomic, readwrite, strong) NSString *topic;
@property (nonatomic, readwrite) RoomVisibility visibility;
@property (nonatomic, readwrite, strong) NSArray<Participant*> *participants;
@property (nonatomic, readwrite, strong) NSArray<NSString*> *guestEmails;
@property (nonatomic, readwrite, strong) Contact *creator;
@property (atomic, readwrite, strong) NSDate *creationDate;
@property (nonatomic, readwrite) BOOL isMyRoom;
@property (nonatomic, readwrite) BOOL isAdmin;
@property (nonatomic, readwrite) ParticipantStatus myStatusInRoom;

@property (nonatomic, readwrite, strong) NSString *associatedConferenceId;
@property (nonatomic, readwrite) NSData *photoData;
@property (atomic, readwrite) NSDate *lastAvatarUpdateDate;
@property (nonatomic, readwrite) NSDictionary *customData;

@property (nonatomic, strong, readwrite) Conference *conference;

-(void) addParticipant:(Participant *) participant;
-(void) removeParticipant:(Participant *) participant;
-(void) removeAllParticipants;

-(NSDictionary *) dictionaryRepresentation;
@end
