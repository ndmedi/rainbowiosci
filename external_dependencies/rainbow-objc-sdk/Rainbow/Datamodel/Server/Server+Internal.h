/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Server.h"

@interface Server ()
@property (nonatomic, readwrite, strong) NSString *serverHostname;
@property (nonatomic, readonly) BOOL useExternalAuthentication;
@property (nonatomic, readonly) NSString *externalAuthenticationPath;
@property (nonatomic, readonly) BOOL requireWebsockets;
@property (nonatomic, readonly) NSString *websocketPath;
@property (nonatomic, readwrite) BOOL isSecure;
@property (nonatomic, readwrite) BOOL defaultServer;
@property (nonatomic, readonly) BOOL isAllInOne;

+(Server *) serverWithServerName:(NSString *) serverName;
+(Server *) opentouchRainbowServer;

@end
