/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConfEndpoint.h"
#import "Contact.h"
#import "ConferenceParticipant+Internal.h"

typedef NS_ENUM(NSInteger, ConferenceStatus){
    ConferenceStatusUnknown = 0,
    ConferenceStatusAttached,
    ConferenceStatusStarted,
    ConferenceStatusJoined
};

@interface Conference ()
@property (nonatomic, readwrite) ConferenceType type;
@property (nonatomic ,readwrite) ConferenceStatus status;
@property (nonatomic, strong, readwrite) NSString *confId;
@property (nonatomic, strong, readwrite) Contact *owner;
@property (nonatomic, readwrite) BOOL isMyConference;
@property (nonatomic, strong, readwrite) NSString *organizerId;
@property (atomic, strong, readwrite) NSDate *start;
@property (atomic, strong, readwrite) NSDate *end;
@property (nonatomic, strong, readwrite) NSMutableArray<ConferenceParticipant*> *participants;
@property (nonatomic, strong, readwrite) NSMutableArray<ConferencePublisher*> *publishers;
@property (nonatomic, strong, readwrite) ConfEndpoint *endpoint;
@property (nonatomic, strong, readwrite) NSArray<ConferenceParticipantId *> *activeTalkers;
@property (nonatomic, strong, readwrite) NSString *jingleJid;
@property (nonatomic, strong, readwrite) ConferenceParticipant *myConferenceParticipant;

@property (nonatomic, readwrite) BOOL canJoin;
@property (nonatomic, readwrite) BOOL isActive;
@property (nonatomic, readwrite) BOOL isRecording;
@property (nonatomic, readwrite) BOOL isTalkerActive;
@property (nonatomic, readwrite) BOOL allParticipantsMuted;

@property (nonatomic, readwrite) BOOL endedConference;

+(NSString *) stringFromConferenceType:(ConferenceType) type;
+(ConferenceType) conferenceTypeFromNSString:(NSString *) type;

-(NSDictionary *)dictionaryRepresentation;

-(void) resetParticipants;
@end
