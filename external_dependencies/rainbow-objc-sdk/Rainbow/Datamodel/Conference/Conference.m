/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Conference.h"
#import "Conference+Internal.h"
#import "ConfEndpoint+Internal.h"
#import "Tools.h"
#import "ConferenceParticipant+Internal.h"
#import "NSDate+JSONString.h"
#import "ConferencePublisher+Internal.h"

@implementation Conference

-(instancetype) init {
    self = [super init];
    if(self){
        _participants = [NSMutableArray new];
        _publishers = [NSMutableArray new];
        _isMyConference = NO;
    }
    return self;
}

-(BOOL)isEqual:(Conference *)object {
    return self == object;
}

-(void) setOwner:(Contact *)owner {
    _owner = owner;
}

-(void) setIsMyConference:(BOOL)isMyConference {
    _isMyConference = isMyConference;
}

-(void) resetParticipants {
    [_participants removeAllObjects];
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"Conference %p : confId %@ type %@ isActive %@ start %@ end %@ participants %@ endpoint %@ myParticipantInConf %@ publishers %@", self, _confId, [Conference stringFromConferenceType:_type], NSStringFromBOOL(_isActive), _start, _end, _participants, _endpoint, _myConferenceParticipant, _publishers];
    }
}

-(NSString *) debugDescription {
    @synchronized(self){
        return [NSString stringWithFormat:@"Conference %p : confId %@ type %@ isActive %@ start %@ end %@ participants %@ endpoint %@ myParticipantInConf %@, publishers %@", self, _confId, [Conference stringFromConferenceType:_type], NSStringFromBOOL(_isActive), _start, _end, [_participants debugDescription], _endpoint, _myConferenceParticipant, _publishers];
    }
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary new];
    if(_confId)
        [dic setObject:_confId forKey:@"id"];
    
    [dic setObject:[Conference stringFromConferenceType:_type] forKey:@"type"];
    
    if(_type == ConferenceTypeScheduled){
        if(_start)
            [dic setObject:[NSDate jsonStringFromDate:_start] forKey:@"scheduledStartDate"];
        if(_end)
            [dic setObject:[NSDate jsonStringFromDate:_end] forKey:@"scheduledEndDate"];
    } else if(_start)
        [dic setObject:[NSDate jsonStringFromDate:_start] forKey:@"creationDate"];
    
    if(_owner)
        [dic setObject:_owner.rainbowID forKey:@"creator"];
    
    if(_endpoint){
        [dic setObject:[ConfEndpoint stringFromConferenceEndPointMediaType:_endpoint.mediaType] forKey:@"mediaType"];
    }
    
    if(_participants.count > 0){
        NSMutableArray *participants = [NSMutableArray array];
        for (ConferenceParticipant *aParticipant in _participants) {
            [participants addObject:[aParticipant dictionaryRepresentation]];
        }
        [dic setObject:participants forKey:@"participants"];
    }
    
    if(_publishers.count > 0){
        NSMutableArray *publishers = [NSMutableArray array];
        for (ConferencePublisher *aPublisher in _publishers) {
            [publishers addObject:[aPublisher dictionaryRepresentation]];
        }
        [dic setObject:publishers forKey:@"publishers"];
    }
    
    if(_jingleJid)
        [dic setObject:_jingleJid forKey:@"jingleID"];
    
    [dic setObject:[NSNumber numberWithBool:_isActive] forKey:@"isActive"];
    [dic setObject:[NSNumber numberWithBool:_isRecording] forKey:@"isRecording"];
    [dic setObject:[NSNumber numberWithBool:_isTalkerActive] forKey:@"isTalkerActive"];
    [dic setObject:[NSNumber numberWithBool:_allParticipantsMuted] forKey:@"allParticipantsMuted"];
    [dic setObject:[NSNumber numberWithBool:_canJoin] forKey:@"canJoin"];
    [dic setObject:[NSNumber numberWithBool:(_type == ConferenceTypeScheduled)] forKey:@"scheduled"];
    
    return dic;
}

+(NSString *) stringFromConferenceType:(ConferenceType) type {
    switch (type) {
        case ConferenceTypeInstant:
            return @"instant";
            break;
        case ConferenceTypeScheduled:
            return @"scheduled";
        case ConferenceTypeUnknown:
        default:
            return @"unknonw";
            break;
    }
}

+(ConferenceType) conferenceTypeFromNSString:(NSString *) type {
    if([type isEqualToString:@"instant"])
        return  ConferenceTypeInstant;
    if([type isEqualToString:@"scheduled"])
        return ConferenceTypeScheduled;
    return ConferenceTypeUnknown;

}

@end
