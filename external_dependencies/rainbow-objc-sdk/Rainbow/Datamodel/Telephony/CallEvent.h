/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "CallParticipant.h"

typedef NS_ENUM(NSInteger, CallMediaState) {
    
    /**
     * Unknown state : aims to support state evolutions for compatibility.
     */
    CallStateUnknown = 0,
    /**
     * The device is off-hook.
     */
    CallStateOffHook,
    
    /**
     * Call is in idle state.
     */
    CallStateIdle,
    
    /**
     * Call release is in progress.
     */
    CallStateReleasing,
    
    /**
     * An attempt to make a call is in progress.
     */
    CallStateDialing,
    
    /**
     * The call has been placed on hold.
     */
    CallStateHeld,
    
    /**
     * The incoming call is ringing.
     */
    CallStateRingingIncoming,
    
    /**
     * The outgoing call is ringing.
     */
    CallStateRingingOutgoing,
    
    /**
     * The call is active.
     */
    CallStateActive
    
};

@interface CallEvent : NSObject

/** Represent the call state */
@property (nonatomic, readonly) CallMediaState state;
/** Represent the call reference */
@property (nonatomic, readonly) NSString *callReference;
@property (nonatomic, readonly) NSString *shortCallReference;
/** Represent the call cause */
@property (nonatomic, readonly) NSString *callCause;
/** Represent the call participant list */
@property (nonatomic, readonly) NSMutableArray<CallParticipant *> *callParticipants;
/** Represent the device type : "MAIN" or "SECONDARY" */
@property (nonatomic, readonly) NSString *deviceType;

@end
