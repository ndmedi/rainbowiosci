/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CallEvent+Internal.h"

@implementation CallEvent

-(instancetype) init {
    self = [super init];
    if(self){
        _callParticipants = [NSMutableArray new];
    }
    return self;
}

-(NSString *) description {
    return [NSString stringWithFormat:@"CallEvent %p <CallReference '%@', callCause '%@', state '%@', for '%@', callParticipants %@>", self, _callReference, _callCause, [CallEvent stringFromCallMediaState:_state], _deviceType, _callParticipants];
}

+(NSString *) stringFromCallMediaState:(CallMediaState) state {
    switch (state) {
        case CallStateUnknown:
            return @"Unknown";
            break;
        case CallStateOffHook:
            return @"Off Hook";
            break;
        case CallStateIdle:
            return @"Idle";
            break;
        case CallStateReleasing:
            return @"Releasing";
            break;
        case CallStateDialing:
            return @"Dialing";
            break;
        case CallStateHeld:
            return @"Held";
            break;
        case CallStateRingingIncoming:
            return @"Ringing incoming";
            break;
        case CallStateRingingOutgoing:
            return @"Ringing outgoing";
            break;
        case CallStateActive:
            return @"Active";
            break;
    }
}
@end
