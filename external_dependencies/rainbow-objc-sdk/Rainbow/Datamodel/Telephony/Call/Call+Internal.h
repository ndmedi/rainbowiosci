/*
 * Rainbow
 *
 * Copyright (c) 2016-2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CallProtocol.h"
#import "Call.h"
#import "TelephonyService+Internal.h"

@interface Call () <CallProtocol>
@property (nonatomic, readwrite, strong) NSString *callRef;
@property (nonatomic, readwrite, strong) NSString *shortCallRef;
@property (nonatomic, readwrite, strong) Peer *peer;
@property (nonatomic, readwrite) BOOL isIncoming;
@property (nonatomic, readwrite) BOOL isMediaPillarCall;
@property (nonatomic, readwrite) CallStatus status;
@property (nonatomic, readwrite) NSDate *connectionDate;
@property (nonatomic, readwrite) CallCapabilities capabilities;
@property (nonatomic, readwrite, strong) NSString *callCause;
@property (nonatomic, readwrite) BOOL readyToDisplay;
+(NSString *)stringForStatus:(CallStatus) status;
@end
