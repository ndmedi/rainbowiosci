/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CallEvent.h"


@interface CallEvent()

@property (nonatomic, readwrite) CallMediaState state;
@property (nonatomic, readwrite, strong) NSString *callReference;
@property (nonatomic, readwrite, strong) NSString *shortCallReference;
@property (nonatomic, readwrite, strong) NSString *callCause;
@property (nonatomic, readwrite, strong) NSMutableArray<CallParticipant *> *callParticipants;
@property (nonatomic, readwrite, strong) NSString *deviceType;

@end
