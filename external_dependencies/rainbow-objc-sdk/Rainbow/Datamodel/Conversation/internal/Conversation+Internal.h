/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Conversation.h"
#import "Contact.h"
#import "Room.h"

@interface Conversation ()
@property (nonatomic, readwrite, strong) NSString *conversationId;
@property (nonatomic, readwrite, strong) Peer *peer;
@property (nonatomic, readwrite) NSInteger unreadMessagesCount;
@property (nonatomic, readwrite, strong) Message *lastMessage;
@property (nonatomic, readwrite, strong) NSDate *creationDate;
@property (readwrite) ConversationStatus status;

@property (nonatomic, readwrite) BOOL hasActiveCall;

// We have to save all the non-yet acknownledged messageIDs
// to quickly be able to ack them.
@property (nonatomic, readwrite) NSMutableSet<NSString *> *unReadMessageIDs;

-(NSString*) stringForConversationStatus;

// remove if useless :
@property (nonatomic, readonly) Contact *contactCast;
@property (nonatomic, readonly) Room *roomCast;

@property (nonatomic, readwrite) BOOL isMuted;

@property (nonatomic, readwrite) BOOL isSynchronized;

-(NSDictionary *) dictionaryRepresentation;

@end
