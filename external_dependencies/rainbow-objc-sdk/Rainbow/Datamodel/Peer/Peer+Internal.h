/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Peer.h"

typedef NS_ENUM(NSInteger, PeerMediaType) {
    PeerMediaTypeAudio = 0,
    PeerMediaTypeVideo,
    PeerMediaTypeSharing,
    PeerMediaTypeVideoSharing
};

@interface Peer ()
@property (nonatomic, readwrite, strong) NSString *rainbowID;
@property (nonatomic, readwrite, strong) NSString *jid;
@property (nonatomic, readwrite, strong) NSString *displayName;
@property (nonatomic, readwrite) NSString *rtcJid;

// Media type currently used by the peer during webrtc calls
@property (nonatomic) PeerMediaType mediaType;
@property (nonatomic, strong) NSString *publisherID;
-(NSString *) getPeerDisplayName;
@end
