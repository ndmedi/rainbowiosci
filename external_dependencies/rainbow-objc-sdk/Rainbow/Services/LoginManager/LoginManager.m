/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "LoginManager.h"
#import "LoginManager+Internal.h"
#import "defines.h"
#import "XMPPService.h"
#import "MyUser+Internal.h"
#import "ContactsManagerService+Internal.h"
#import "Tools.h"
#import "NSDictionary+JSONString.h"
#import "NSData+JSON.h"
#import "DownloadManager.h"
#import "Server+Internal.h"
#import "RainbowUserDefaults.h"

NSString *const kLoginManagerDidLoginSucceeded = @"didLoginSucceeded";
NSString *const kLoginManagerInternalDidLoginSucceeded = @"internalDidLoginSucceeded";
NSString *const kLoginManagerDidLogoutSucceeded = @"didLogoutSucceeded";
NSString *const kLoginManagerDidLostConnection = @"didLostConnection";
NSString *const kLoginManagerInternalDidLostConnection = @"internalDidLostConnection";
NSString *const kLoginManagerDidReconnect = @"didReconnect";
NSString *const kLoginManagerDidFailedToAuthenticate = @"didFailedToConnect";
NSString *const kLoginManagerDidChangeServer = @"didChangeServer";
NSString *const kLoginManagerDidChangeUser = @"didChangeUser";
NSString *const kLoginManagerWillLogin = @"willLogin";
NSString *const kLoginManagerInternalWillLogin = @"InternalWillLogin";
NSString *const kLoginManagerDidDisconnect = @"didDisconnect";
// This notification is sent by services manager after a delay if the connection is too long to establish
NSString *const kLoginManagerTryToReconnect = @"tryToReconnect";

@interface LoginManager () <XMPPServiceLoginDelegate>
@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property (nonatomic, strong) ContactsManagerService *contactsManagerService;
@property (nonatomic, strong) MyUser *myUser;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
@property (nonatomic) UIBackgroundTaskIdentifier ongoingLoginID;
#endif
@property (nonatomic) BOOL hasAlreadyNotifyLostConnection;
@property (nonatomic) NSInteger nbOfConnectionTentative;

// Set to yes when the change password in done on this device
// We will not treat that case like if we changed it on another device
@property (nonatomic) BOOL changePasswordRequested;
@end

@implementation LoginManager
-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager withXmppService:(XMPPService *) xmppService myUser:(MyUser *) myUser apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService {
    self = [super init];
    if(self){
        _downloadManager = downloadManager;
        _xmppService = xmppService;
        _myUser = myUser;
        _apiUrlManagerService = apiUrlManagerService;
        _contactsManagerService = contactsManagerService;
        _xmppService.loginDelegate = self;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        _ongoingLoginID = UIBackgroundTaskInvalid;
#endif
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeServerURL:) name:kChangeServerURLNotification object:nil];
        [self extractServerInfo];
        
        _autoLogin = YES;
        _hasAlreadyNotifyLostConnection = NO;
        _nbOfConnectionTentative = 0;
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChangeServerURLNotification object:nil];
    [_contactsManagerService deleteMyContact];
    _xmppService.loginDelegate = nil;
    _xmppService = nil;
    _downloadManager = nil;
    _myUser = nil;
    _apiUrlManagerService = nil;
    _contactsManagerService = nil;
}

-(BOOL) isConnected {
    return _xmppService.isXmppConnected;
}

-(void) setUsername:(NSString *) username andPassword:(NSString *) password {
    // Compare new user name to the current user name, if different clean every things
    BOOL changeUserName = NO;
    if(![_myUser.username isEqualToString:username]){
        NSLog(@"Username is not the same so reset all infos");
        [self resetAllCredentials];
        changeUserName = YES;
    }

    _myUser.username = username;
    _myUser.password = password;
    if(changeUserName)
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidChangeUser object:nil];
}

-(void) resetAllCredentials {
    if(self.isConnected)
        [self performSelectorInBackground:@selector(logout) withObject:nil];
    
    [_contactsManagerService resetMyContact];
    [_myUser reset];
    [_xmppService reset];
}

-(void) didChangeServerURL:(NSNotification *) notification {
    NSDictionary *newServerParameters = (NSDictionary *)notification.object;
    
    // Set login & password
    NSString *newLogin = [newServerParameters objectForKey:@"login"];
    NSString *newPassword = [newServerParameters objectForKey:@"password"];
    
    if(newLogin.length > 0)
        [self setUsername:newLogin andPassword:nil];
    if(newPassword.length > 0)
        [self setUsername:nil andPassword:newPassword];
    
    if( newLogin.length > 0 && newPassword.length > 0) {
        [self setUsername: newLogin andPassword: newPassword];
    }
    
    if([[newServerParameters objectForKey:@"serverURL"] length] > 0) {
        [[RainbowUserDefaults sharedInstance] setObject: [newServerParameters objectForKey:@"serverURL"] forKey:@"serverURL"];
        [[NSUserDefaults standardUserDefaults] setObject:[newServerParameters objectForKey:@"serverURL"] forKey:@"serverURL"];
    } else {
        [[RainbowUserDefaults sharedInstance] removeObjectForKey:@"serverURL"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"serverURL"];
    }
    
    if(_loginDidSucceed)
        [self performSelectorInBackground:@selector(disconnect) withObject:nil];
    
    [self extractServerInfo];
}

-(void) extractServerInfo {
    NSString *serverURL = [[RainbowUserDefaults sharedInstance] objectForKey:@"serverURL"];
    NSArray *split = [serverURL componentsSeparatedByString:@"/"];
    Server *server = nil;
    if(split.count == 1){
        if(serverURL.length > 0)
            server = [Server serverWithServerName:serverURL];
    } else {
        NSString *serverURL = split[0];
        if(serverURL.length > 0){
            server = [Server serverWithServerName:serverURL];
            if([split objectAtIndex:1] && [split[1] isEqualToString:@"unsecure"])
                server.isSecure = NO;
        }
    }
    
    if(!server)
        server = [Server opentouchRainbowServer];
    
    [self setServer:server];
}

-(void) setServer:(Server *)server {
    if([_server isEqual:server])
        return;
    BOOL shouldNotify = YES;
    if(!_server){
        shouldNotify = NO;
    }
    _server = nil;
    _myUser.server = nil;
    _server = server;
    _myUser.server = _server;
    if(shouldNotify)
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidChangeServer object:_server];
}

-(BOOL) loginToServer:(Server *) server {
    NSAssert(![[NSThread currentThread] isEqual:[NSThread mainThread]], @"Login method must *NOT* be invoked on main thread");
    __block BOOL loginSucceed = NO;
    if(server.useExternalAuthentication){
        //Authenticate through REST Api
        NSURL *loginUrl = [_apiUrlManagerService getURLForService:ApiServicesAuthenticationLogin];
        NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesAuthenticationLogin];
        NSData *receivedData = nil;
        NSError *requestError = nil;
        NSURLResponse *response = nil;
        NSLog(@"Login to server %@", server);
        BOOL requestSucceed = [_downloadManager getSynchronousRequestWithURL:loginUrl requestHeadersParameter:headersParameters returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
        NSLog(@"Login request sent, analysing response ... request Error %@", requestError);
        if(requestSucceed && !requestError){
            NSDictionary *response = [receivedData objectFromJSONData];
            if(response) {
                // Handle errors if we have some.
                if (response[@"errorCode"]) {
                    NSLog(@"Login request detail error %@",response);
                    NSString *errorMessage = @"";
                    // Generic message
                    switch ([response[@"errorCode"] intValue]) {
                        case 400:
                            errorMessage = NSLocalizedString(@"Bad request", nil);
                            break;
                        case 401:
                            errorMessage = NSLocalizedString(@"Unauthorized", nil);
                            break;
                        case 403:
                            errorMessage = NSLocalizedString(@"Forbidden", nil);
                            break;
                        case 500:
                            errorMessage = NSLocalizedString(@"Internal server error", nil);
                            break;
                        default:
                            errorMessage = [NSString stringWithFormat:@"Login Error : %@", response[@"errorCode"]];
                            break;
                    }
                    
                    // We have a more specific error code ?
                    switch ([response[@"errorDetailsCode"] intValue]) {
                        case 400020:
                            // This should not happend
                            NSLog(@"Login Error : Missing header X-Rainbow-Client or X-Rainbow-Client-Version");
                            errorMessage = [NSString stringWithFormat:@"Internal Error : %@", response[@"errorDetailsCode"]];
                            break;
                        case 401500:
                            errorMessage = NSLocalizedString(@"Unknown login or wrong password", nil);
                            break;
                        case 401501:
                            errorMessage = NSLocalizedString(@"Account temporary locked", nil);
                            break;
                        case 401520:
                            errorMessage = NSLocalizedString(@"User not activated", nil);
                            break;
                        case 401521:
                            errorMessage = NSLocalizedString(@"Company not activated", nil);
                            break;
                        case 403020:
                            errorMessage = NSLocalizedString(@"Your application version is outdated, you have to update it", nil);
                            break;
                        case 403021:
                            // This should not happend
                            NSLog(@"Login Error : Application is not recognized");
                            errorMessage = [NSString stringWithFormat:@"Internal Error : %@", response[@"errorDetailsCode"]];
                            break;
                        default:
                            // This should not happend
                            NSLog(@"Login Error : Unknown errorDetailsCode %@ for errorCode %@", response[@"errorDetailsCode"], response[@"errorCode"]);
                            break;
                    }
                    
                    NSError *error = [NSError errorWithDomain:@"Authentication" code:[response[@"errorDetailsCode"] intValue] userInfo:@{NSLocalizedDescriptionKey:errorMessage}];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidFailedToAuthenticate object:error];
                    loginSucceed = NO;
                    _autoLogin = NO;
                    // In case of login error we consider we are not logged in anymore
                    _loginDidSucceed = NO;
                } else {
                    NSMutableDictionary *user = [NSMutableDictionary dictionaryWithDictionary:response[@"loggedInUser"]];
                    if([user isKindOfClass:[NSDictionary class]] && user.count >= 2){
                        NSLog(@"User is logged in in REST");
                        if(response[@"token"] )
                            _myUser.token = response[@"token"];
                        else
                            NSLog(@"warning : login with no authorization token");
                        
                        _myUser.userID = user[@"id"];
                        
                        _myUser.jid_im = [user objectForKey:@"jid_im"];
                        _myUser.jid_password = [user objectForKey:@"jid_password"];
                        _myUser.jid_tel = [user objectForKey:@"jid_tel"];
                        _myUser.isInitialized = ((NSNumber *)[user objectForKey:@"isInitialized"]).boolValue;
                        _myUser.isInDefaultCompany = ((NSNumber *)[user objectForKey:@"isInDefaultCompany"]).boolValue;
                        _myUser.companyID = [user objectForKey:@"companyId"];
                        _myUser.companyName = [user objectForKey:@"companyName"];
                        _myUser.isADSearchAvailable = ((NSNumber *)[user objectForKey:@"isADSearchAvailable"]).boolValue;
                        _myUser.isGuest = ((NSNumber *)[user objectForKey:@"guestMode"]).boolValue;
                        
                        if(user[@"roles"])
                            [(NSMutableArray *)_myUser.roles addObjectsFromArray:user[@"roles"]];
                        
                        if(user[@"profiles"]){
                            NSMutableArray *profiles = [NSMutableArray new];
                            BOOL isReadyToCreateConference = NO;
                            for (NSDictionary *profile in user[@"profiles"]) {
                                NSString *profileName = profile[@"profileName"];
                                
                                if([profileName isEqualToString:@"Conference"] && profile[@"provisioningNeeded"]) {
                                    for (NSDictionary *profileProvisioning in profile[@"provisioningNeeded"]) {
                                        if([profileProvisioning[@"providerType"] isEqualToString:@"PGI"]){
                                            NSNumber* ongoing = [profileProvisioning objectForKey:@"provisioningOngoing"];
                                            if(![ongoing boolValue]) {
                                                isReadyToCreateConference = YES;
                                            }
                                        }
                                    }
                                }
                                
                                NSString *offerName = profile[@"offerName"];
                                if (offerName)
                                    [profiles addObject:offerName];
                            }
                            
                            [_myUser setIsReadyToCreateConference:isReadyToCreateConference];
                            [_myUser setProfilesName:profiles];
                        }
                        
                        loginSucceed = YES;
                        
                        [self getServerApiVersion];
                    } else {
                        NSError *error = [NSError errorWithDomain:@"Authentication" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Failed to authenticate user, check your login or password"}];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidFailedToAuthenticate object:error];
                        loginSucceed = NO;
                    }
                }
            } else {
                NSLog(@"Failed to read server answer %@", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
                NSError *error = [NSError errorWithDomain:@"Authentication" code:500 userInfo:@{NSLocalizedDescriptionKey:NSLocalizedString(@"Could not read server answer", nil)}];
                [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidFailedToAuthenticate object:error];
            }
        } else {
            NSError *error = requestError;
            if(receivedData.length > 0){
                NSDictionary *response = [receivedData objectFromJSONData];
                if(response[@"errorMsg"] && response[@"errorDetails"]){
                    error = [NSError errorWithDomain:@"Authentication" code:500 userInfo:@{NSLocalizedDescriptionKey:response[@"errorDetails"]}];
                }
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidFailedToAuthenticate object:error];
            
            loginSucceed = NO;
        }
    } else {
        // Standard XMPP connection
        _myUser.jid_im = _myUser.username;
        _myUser.jid_password = _myUser.password;
        loginSucceed = YES;
    }
    
    if(!_loginDidSucceed)
        _loginDidSucceed = loginSucceed;
    
    return loginSucceed;
}

-(BOOL) logout {
    NSAssert(![[NSThread currentThread] isEqual:[NSThread mainThread]], @"Logout method must *NOT* be invoked on main thread");
    BOOL logoutSucceed = NO;
    //Authenticate through REST Api
    NSLog(@"Invoke logout api");
    _loginDidSucceed = NO;
    NSURL *loginUrl = [_apiUrlManagerService getURLForService:ApiServicesAuthenticationLogout];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesAuthenticationLogout];
    NSData *receivedData = nil;
    NSError *requestError = nil;
    NSURLResponse *response = nil;
    BOOL requestSucceed = [_downloadManager getSynchronousRequestWithURL:loginUrl requestHeadersParameter:headersParameters returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
    logoutSucceed = (requestSucceed && !requestError);
    NSLog(@"Logout done %@",NSStringFromBOOL(logoutSucceed));

    return logoutSucceed;
}

-(void) connect {
    _autoLogin = YES;
    [self extractServerInfo];
    _nbOfConnectionTentative = 0;
    [self connectToServer:_server];
}

-(void) connectToServer:(Server *) server {
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    UIApplication* app = [UIApplication sharedApplication];
    if(_ongoingLoginID == UIBackgroundTaskInvalid){
        _ongoingLoginID = [app beginBackgroundTaskWithExpirationHandler:^{
            NSLog(@"*** [LoginManager] Login backgroud task expired %ld! ***", (unsigned long)_ongoingLoginID);
            [app endBackgroundTask:_ongoingLoginID];
            _ongoingLoginID = UIBackgroundTaskInvalid;
        }];
        
        if(_ongoingLoginID != UIBackgroundTaskInvalid){
            NSLog(@"*** [LoginManager] Start login long running task (id : %ld)", (unsigned long)_ongoingLoginID);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                [self internalConnectToServer:server];
            });
        } else {
            NSLog(@"Could not create a background task");
        }
    } else {
        NSLog(@"*** [LoginManager] ongoing login long running task %ld",_ongoingLoginID);
    }
#endif
}

-(void) endOngoingLoginLongRunningTask {
    if(_ongoingLoginID != UIBackgroundTaskInvalid){
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        NSLog(@"************ [LoginManager] End of long running task id : %lu",(unsigned long)_ongoingLoginID);
        [[UIApplication sharedApplication] endBackgroundTask:_ongoingLoginID];
#endif
        _ongoingLoginID = UIBackgroundTaskInvalid;
    }
}

-(void) internalConnectToServer:(Server *) server {
    NSAssert(![[NSThread currentThread] isEqual:[NSThread mainThread]], @"Connect must *NOT* be invoked on main thread");
    _myUser.server = nil;
    _myUser.server = server;
    if(_myUser.haveMinimalInformationsToStartWithOutToken)
        [_contactsManagerService createMyRainbowContactForJid:_myUser.jid_im withCompanyId:_myUser.companyID withRainbowId:_myUser.userID companyName:_myUser.companyName];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerInternalWillLogin object:nil];
    // We post this notification synchronously on main thread to let time to the UI to display something before starting connection
    dispatch_sync(dispatch_get_main_queue(), ^{
        NSLog(@"Will Login notification");
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerWillLogin object:nil];
    });
    
    BOOL didLogin = NO;
    if(!_myUser.haveMinimalInformationsToStart)
        didLogin = [self loginToServer:server];
    else {
        didLogin = _myUser.haveMinimalInformationsToStart;
        // Because we will not relogin again we have to consider that the login succeed at least one time.
        _loginDidSucceed = didLogin;
    }
    if(didLogin){
        if(!_myUser.contact)
            [_contactsManagerService createMyRainbowContactForJid:_myUser.jid_im withCompanyId:_myUser.companyID withRainbowId:_myUser.userID companyName:_myUser.companyName];
        [_contactsManagerService refreshMyVcard];
        
        if (![_xmppService isXmppConnected])
            [self connectToXMPPWithUserName:_myUser.jid_im password:_myUser.jid_password];
        else
            [self endOngoingLoginLongRunningTask];
    } else
        [self endOngoingLoginLongRunningTask];
}

-(BOOL) connectToXMPPWithUserName:(NSString *) username password:(NSString *) password {
    // start connection to xmpp
    NSError *connectionError = nil;
    return [_xmppService connectToServer:_myUser.server withJID:username andPassword:password withError:&connectionError];
}

-(void) disconnect {
    NSAssert(![[NSThread currentThread] isEqual:[NSThread mainThread]], @"Disconnect must *NOT* be invoked on main thread");
    [self logout];
    _autoLogin = NO;
    [_xmppService disconnect];
    [_contactsManagerService deleteMyContact];
    NSLog(@"post notif LOGOUT SUCCESS");
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidLogoutSucceeded object:nil];
    _nbOfConnectionTentative = 0;
}

-(void) xmppService:(XMPPService *)xmppService failedToAuthenticateWithError:(NSError *)error {
    NSLog(@"[LoginManager] failed to authenticate %@", error);
    [self endOngoingLoginLongRunningTask];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidFailedToAuthenticate object:error];
    NSLog(@"[LoginManager] authentication failed, reset token and perform a full login");
    // In case of authentication failed we want to reconnect with a full login so the token is put to nil (to invalidate it).
    _myUser.token = nil;
    [self connectToServer:_server];
    
    _nbOfConnectionTentative = 0;
}

-(void) xmppServiceDidConnect:(XMPPService *)xmmpService {
    NSLog(@"[LoginManager] did connect");
    [self endOngoingLoginLongRunningTask];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidLoginSucceeded object:nil];
    _nbOfConnectionTentative = 0;
}

-(void) xmppServiceDidDisconnect:(XMPPService *)xmmpService withError:(NSError *)error {
    NSLog(@"[LoginManager] did disconnect");
    [self endOngoingLoginLongRunningTask];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidDisconnect object:nil];
    
    
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    dispatch_async(dispatch_get_main_queue(), ^{
        if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground){
            return;
        }
    });
#endif
    NetworkStatus status = [_reachability currentReachabilityStatus];
     if(_autoLogin && error.code != -987 && error.code != -998 && error.code != -999 && _xmppService.autoReconnectEnabled && status != NotReachable){
        NSLog(@"We receive a disconnect and the autoLogin is still active so we must reconnect automatically");
        // Autologin is still active and the error code is not one of the excepted one so we must restart a connection
        if(_nbOfConnectionTentative < 5){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                [self connectToServer:_server];
            });
            _nbOfConnectionTentative ++;
        } else {
            NSLog(@"Max number of connection tentation reached, stop trying an return an error");
            if (_loginDidSucceed && error.code == 401) {//unautherized error code
                _loginDidSucceed = NO;
                NSError *error = [NSError errorWithDomain:@"Authentication" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Failed to connect after 5 tentative"}];
                [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidFailedToAuthenticate object:error];
            }
        }
    } else {
        _nbOfConnectionTentative = 0;
    }
}

-(void) xmppServiceDidLostConnection:(XMPPService *)xmmpService {
    NSLog(@"[LoginManager] did lost connection");
    [self endOngoingLoginLongRunningTask];
    
    // We must send in all cases an internal notification about lost connection to allow ServiceManager to end is long running task
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerInternalDidLostConnection object:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(!_hasAlreadyNotifyLostConnection ){
            UIApplicationState state = [[UIApplication sharedApplication] applicationState];
            if (state != UIApplicationStateBackground && state != UIApplicationStateInactive)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidLostConnection object:nil];
                _hasAlreadyNotifyLostConnection = YES;
            }
        }
    });
}

-(void) xmppServiceDidResumeStream:(XMPPService *)xmppService {
    NSLog(@"[LoginManager] Did resume stream");
    [self endOngoingLoginLongRunningTask];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidReconnect object:nil];
    _hasAlreadyNotifyLostConnection = NO;
    _nbOfConnectionTentative = 0;
}

-(void) xmppServiceDidReceiveChangePasswordMessage:(XMPPService *)xmppService {
    NSLog(@"[LoginManager] didChangePassword received");
    _myUser.token = nil;
    if(!_changePasswordRequested){
        _loginDidSucceed = NO;
        [self setUsername:_myUser.username andPassword:@""];
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidLogoutSucceeded object:nil];
    }
    _changePasswordRequested = NO;
}

#pragma mark - Auto-Enrollment
-(void) sendNotificationForEnrollmentTo:(NSString *) emailAddress completionHandler:(LoginManagerRegistrationCompletionHandler) completionHandler {
    NSLog(@"Sending Notification for enrolment");
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesEndUserNotificationsEmailsSelfRegister];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesEndUserNotificationsEmailsSelfRegister];
    NSDictionary *body = @{@"email": emailAddress, @"lang":[[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode]};
    [_downloadManager postRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO]  requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        NSLog(@"Sending Notification for enrolment, return data ...");
        if(completionHandler)
            completionHandler(jsonResponse, error);
    }];
}

-(void) sendSelfRegisterRequestWithLoginEmail:(NSString *) loginEmail password:(NSString *) password temporaryCode:(NSString *) temporaryCode completionHandler:(LoginManagerRegistrationCompletionHandler) completionHandler {
    [self sendSelfRegisterRequestWithLoginEmail:loginEmail password:password temporaryCode:temporaryCode invitationId:nil joinCompanyInvitationId:nil visibility:nil completionHandler:completionHandler];
}

-(void) sendSelfRegisterRequestWithLoginEmail:(NSString *) loginEmail password:(NSString *) password invitationId:(NSString *) invitationId completionHandler:(LoginManagerRegistrationCompletionHandler) completionHandler {
    [self sendSelfRegisterRequestWithLoginEmail:loginEmail password:password temporaryCode:nil invitationId:invitationId joinCompanyInvitationId:nil visibility:nil completionHandler:completionHandler];
}

-(void) sendSelfRegisterRequestWithLoginEmail:(NSString *) loginEmail password:(NSString *) password invitationId:(NSString *) invitationId visibility:(NSString *) visibility completionHandler:(LoginManagerRegistrationCompletionHandler) completionHandler {
    [self sendSelfRegisterRequestWithLoginEmail:loginEmail password:password temporaryCode:nil invitationId:invitationId joinCompanyInvitationId:nil visibility:visibility completionHandler:completionHandler];
}

-(void) sendSelfRegisterRequestWithLoginEmail:(NSString *) loginEmail password:(NSString *) password joinCompanyInvitationId:(NSString *) joinCompanyInvitationId completionHandler:(LoginManagerRegistrationCompletionHandler) completionHandler {
    [self sendSelfRegisterRequestWithLoginEmail:loginEmail password:password temporaryCode:nil invitationId:nil joinCompanyInvitationId:joinCompanyInvitationId visibility:nil  completionHandler:completionHandler];
}

-(void) sendSelfRegisterRequestWithLoginEmail:(NSString *)loginEmail password:(NSString *)password temporaryCode:(NSString *)temporaryCode invitationId:(NSString *) invitationId joinCompanyInvitationId:(NSString *) joinCompanyInvitationId visibility:(NSString *) visibility completionHandler:(LoginManagerRegistrationCompletionHandler)completionHandler {
    if(temporaryCode.length > 0 && invitationId.length > 0 && joinCompanyInvitationId.length > 0){
        NSLog(@"Temporary code, invitationID and joinCompanyInvitationId cannot be used in the same time");
        if(completionHandler)
            completionHandler(nil,[NSError errorWithDomain:@"Bad parameters" code:500 userInfo:nil]);
        return;
    }
    if(temporaryCode.length == 0 && invitationId.length == 0 && joinCompanyInvitationId.length == 0){
        NSLog(@"Temporary code, invitationID and joinCompanyInvitationId cannot be empty all in the same time");
        if(completionHandler)
            completionHandler(nil,[NSError errorWithDomain:@"Bad parameters" code:500 userInfo:nil]);
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesUsersSelfRegister];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesUsersSelfRegister];
    NSMutableDictionary *body = [NSMutableDictionary dictionaryWithDictionary:@{@"loginEmail": loginEmail, @"password": password}];
    if(visibility.length > 0 && ([visibility isEqualToString:@"public"] || [visibility isEqualToString:@"private"] || [visibility isEqualToString:@"none"]))
        [body setObject:visibility forKey:@"visibility"];
    
    if(temporaryCode.length > 0)
        [body setValue:temporaryCode forKey:@"temporaryToken"];
    if(invitationId.length > 0)
        [body setValue:invitationId forKey:@"invitationId"];
    if(joinCompanyInvitationId.length > 0)
        [body setObject:joinCompanyInvitationId forKey:@"joinCompanyInvitationId"];
    NSLog(@"Sending Register request, invitationID %@ companyInvitationID %@", invitationId, joinCompanyInvitationId);
    [_downloadManager postRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO]  requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        NSLog(@"Sending Register request return data ...");
        if(completionHandler)
            completionHandler(jsonResponse, error);
    }];
}

#pragma mark - reset password
-(void) sendResetPasswordEmailWithLoginEmail:(NSString *) loginEmail completionHandler:(LoginManagerRegistrationCompletionHandler) completionHandler {
    NSLog(@"Sending Reset password");
    if(!loginEmail){
        NSLog(@"No login email");
        if(completionHandler){
            NSError *error = [NSError errorWithDomain:@"LoginManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Invalid parameters"}];
            completionHandler(nil, error);
        }
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesEndUserNotificationEmailsResetPassword];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesEndUserNotificationEmailsResetPassword];
    NSDictionary *body = @{@"email": loginEmail, @"lang": [[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode]};
    [_downloadManager postRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO]  requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        NSLog(@"Sending Reset password, return data ...");
        if(completionHandler)
            completionHandler(jsonResponse, error);
    }];
}

-(void) sendResetPasswordWithLoginEmail:(NSString *) loginEmail password:(NSString *) password temporaryCode:(NSString *) temporaryCode completionHandler:(LoginManagerRegistrationCompletionHandler) completionHandler {
    NSLog(@"Sending Reset password with temporary code");
    if(!loginEmail || !password || !temporaryCode){
        NSLog(@"No loginEmail or password or temporaryCode");
        if(completionHandler){
            NSError *error = [NSError errorWithDomain:@"LoginManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Invalid parameters"}];
            completionHandler(nil, error);
        }
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesEndUserResetPassword];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesEndUserResetPassword];
    NSDictionary *body = @{@"loginEmail": loginEmail, @"newPassword": password, @"temporaryToken": temporaryCode};
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        NSLog(@"Sending Reset password with temporary code, return data");
        if(completionHandler)
            completionHandler(jsonResponse, error);
    }];
}
#pragma mark - change password
-(void) sendChangePassword:(NSString *) oldPassword newPassword:(NSString *) password completionHandler:(LoginManagerRegistrationCompletionHandler) completionHandler {
    NSLog(@"Sending change password");
    if(!oldPassword || !password){
        NSLog(@"No old or new password");
        if(completionHandler){
            NSError *error = [NSError errorWithDomain:@"LoginManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Invalid parameters"}];
            completionHandler(nil, error);
        }
        return;
    }
        
    // request url
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesEndUserChangePassword];
    // request headers
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesEndUserChangePassword];
    // request parameters
    NSDictionary *body = @{@"oldPassword": oldPassword, @"newPassword": password};
    _changePasswordRequested = YES;
    
    // execute request
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        NSLog(@"Sending change password, return data");
        if(completionHandler)
            completionHandler(jsonResponse, error);
    }];
}

#pragma mark - other
-(void) getServerApiVersion {
    // Get Api version
    NSLog(@"Get server api version");
    NSURL *aboutURL = [_apiUrlManagerService getURLForService:ApiServicesAbout];
    NSDictionary *aboutHeaders = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesAbout];
    
    [_downloadManager getRequestWithURL:aboutURL requestHeadersParameter:aboutHeaders completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSLog(@"Get server api version done");
        if(error){
            NSLog(@"Failed to get server api version error %@", error);
            return;
        }

        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(jsonResponse){
            NSString *version = [jsonResponse[@"version"] stringByReplacingOccurrencesOfString:@"1." withString:@""];
            _serverApiVersion = [NSNumber numberWithFloat:[version floatValue]];
        }
    }];
}


-(void) deleteMyAccount {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesMyUsers];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesMyUsers];
    NSLog(@"--- USER SELF DELETE ---");
    
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot self delete in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot self delete in REST due to JSON parsing failure");
            return;
        }
        
        NSArray *data = [jsonResponse objectForKey:@"data"];
        
        if(data){
            _autoLogin = NO;
            [_contactsManagerService deleteMyContact];
            NSLog(@"User details deleted successfully.");
            NSError *logoutError = [NSError errorWithDomain:@"LoginManager" code:-998 userInfo:@{NSLocalizedDescriptionKey:@"User self deleted his account"}];
            [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidLogoutSucceeded object: logoutError];
            _nbOfConnectionTentative = 0;
        }
    }];
}

@end
