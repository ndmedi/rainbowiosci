/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "GroupsService.h"
#import "GroupsService+Internal.h"
#import "LoginManager.h"
#import "Tools.h"
#import "NSDate+JSONString.h"
#import "NSData+JSON.h"
#import "NSDictionary+JSONString.h"
#import "Group+Internal.h"
#import "NSObject+NotNull.h"
#import "LoginManager+Internal.h"
#import "PINCache.h"

NSString *const kGroupsServiceDidAddGroup = @"didAddGroup";
NSString *const kGroupsServiceDidUpdateGroup = @"didUpdateGroup";
NSString *const kGroupsServiceDidRemoveGroup = @"didRemoveGroup";
NSString *const kGroupsServiceDidRemoveAllGroups = @"didRemoveAllGroups";
NSString *const kGroupsServiceDidUpdateGroupsForContact = @"didUpdateGroupsForContact";

@interface GroupsService ()
@property (nonatomic, strong) PINCache *groupsCache;
@end

@implementation GroupsService
-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService xmppService:(XMPPService *) xmppService {
    self = [super init];
    if(self){
        _cacheLoaded = NO;
        _downloadManager = downloadManager;
        _apiUrlManagerService = apiUrlManagerService;
        _contactsManagerService = contactsManagerService;
        _xmppService = xmppService;
        _xmppService.groupsService = self;
        
        _groups = [NSMutableArray array];
        _groupsMutex = [NSObject new];
        
        _groupsCache = [[PINCache alloc] initWithName:@"groupCache"];
        _groupsCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willLogin:) name:kLoginManagerInternalWillLogin object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeServer:) name:kLoginManagerDidChangeServer object:nil];
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalWillLogin object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    
    
    @synchronized (_groupsMutex) {
        [_groups removeAllObjects];
        _groups = nil;
    }
    _groupsMutex = nil;
    
    _downloadManager = nil;
    _apiUrlManagerService = nil;
    _contactsManagerService = nil;
    _xmppService = nil;
    _groupsCache = nil;
}

#pragma mark - Login manager notifications

-(void) willLogin:(NSNotification *) notification {
    if (!_cacheLoaded) {
        _cacheLoaded = NO;
        [self loadGroupsFromCache];
        
    }
}

-(void) didLogin:(NSNotification *) notification {
    [self fetchGroups];
}

-(void) didLogout:(NSNotification *) notification {
    @synchronized (_groupsMutex) {
        [_groups removeAllObjects];
        [[NSNotificationCenter defaultCenter] postNotificationName:kGroupsServiceDidRemoveAllGroups object:nil];
    }
}

-(void) didReconnect:(NSNotification *) notification {
    // Do not fetch groups on reconnect it's not needed
    // TODO: Resynchronize the groups using the xmpp messages
}

-(void) didChangeUser:(NSNotification *) notification {
    [_groupsCache removeAllObjects];
}

-(void) didChangeServer:(NSNotification *) notification {
    [_groupsCache removeAllObjects];
}

-(void) loadGroupsFromCache {
    __weak __typeof__(self) weakSelf = self;
    NSMutableArray *exisintKeys = [NSMutableArray array];
    [_groupsCache.diskCache enumerateObjectsWithBlockAsync:^(NSString * key, NSURL * fileURL, BOOL * stop) {
        [exisintKeys addObject:key];
    } completionBlock:^(id<PINCaching> cache) {
        NSLog(@"Groups cache : Load cache completion block start");
        for (NSString *key in exisintKeys) {
            NSDictionary *object = [_groupsCache objectForKey:key];
            @synchronized(_groupsMutex){
                [weakSelf createOrUpdateGroupFromJSON:object];
            }
        }
        NSLog(@"Groups cache : Load cache completion block end");
    }];
}

-(void) fetchGroups {
    NSURL *service_url = [_apiUrlManagerService getURLForService:ApiServicesGroups, _contactsManagerService.myContact.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?format=full",service_url]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRooms];
    
    NSLog(@"Get groups list");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get groups in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get groups in REST due to JSON parsing failure");
            return;
        }
        NSLog(@"Get groups response data ...");

        NSDictionary *data = [jsonResponse objectForKey:@"data"];
        for (NSDictionary *groupDict in data) {
            [self createOrUpdateGroupFromJSON:groupDict];
        }
        
        // Request all missing vcard
        NSMutableArray<NSString *> *jidToSearch = [NSMutableArray array];
        @synchronized (_groupsMutex) {
            for (Group *group in _groups) {
                for (Contact *aContact in group.users) {
                    if(!aContact.vcardPopulated && aContact.jid.length > 0)
                        [jidToSearch addObject:aContact.jid];
                }
            }
        }
        if(jidToSearch.count > 0)
            [_contactsManagerService populateVcardForContactsJids:jidToSearch];
    }];
}

#pragma mark - Groups public api
-(Group *) createGroupWithName:(NSString *) name andComment:(NSString *) comment{
    if (!name || [name length] == 0) {
        NSLog(@"Error: cannot create new group without a name.");
        return nil;
    }
    
    NSAssert(![NSThread isMainThread], @"Create group must not be invoked on mainThread");
    
    // Create the group through REST api.
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesGroups, _contactsManagerService.myContact.rainbowID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRooms];
    NSString *bodyValue = comment;
    if(comment.length == 0)
        bodyValue = @"";
    
    NSDictionary *bodyContent = @{@"name":name, @"comment":bodyValue};
    NSData *receivedData = nil;
    NSError *requestError = nil;
    NSURLResponse *response = nil;
    
    BOOL requestSucceed = [_downloadManager postSynchronousRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
    
    if(!requestSucceed || requestError) {
        NSLog(@"Error: Cannot create the group in REST : %@ %@", requestError, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        return nil;
    }
    
    NSDictionary *jsonResponse = [receivedData objectFromJSONData];
    if(!jsonResponse) {
        NSLog(@"Error: Cannot create the group in REST due to JSON parsing failure");
        return nil;
    }
    
    NSDictionary *data = [jsonResponse objectForKey:@"data"];
    if(data){
        NSLog(@"Did create group");
        Group *group = [self createOrUpdateGroupFromJSON:data];
        return group;
    } else
        return nil;
}

-(void) deleteGroup:(Group *) group {
    if(!group.rainbowID || group.rainbowID.length == 0){
        NSLog(@"The given group don't have a rainbow ID, that is not normal %@", group);
        return;
    }
    
    NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesGroups, _contactsManagerService.myContact.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", url_base, group.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesGroups];
    
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot delete the group in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(!jsonResponse) {
                NSLog(@"Error: Cannot delete the group in REST due to JSON parsing failure");
            } else {
                NSLog(@"Delete group response");
                if(jsonResponse[@"status"]){
                    [self removeGroup:group];
                }
            }
        }
    }];
}

-(void) updateGroup:(Group *) group withNewGroupName:(NSString *) newGroupName {
    if (!newGroupName || [newGroupName length] == 0) {
        NSLog(@"Error: cannot update group without a name.");
        return;
    }
    if(!group.rainbowID || group.rainbowID.length == 0){
        NSLog(@"The given group don't have a rainbow ID, that is not normal %@", group);
        return;
    }
        
    NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesGroups, _contactsManagerService.myContact.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", url_base, group.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesGroups];
    
    NSDictionary *bodyContent = @{@"name":newGroupName};
    [_downloadManager putRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot update the group in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(!jsonResponse) {
                NSLog(@"Error: Cannot update the group in REST due to JSON parsing failure");
            } else {
                NSDictionary *data = [jsonResponse objectForKey:@"data"];
                if(data){
                    NSLog(@"Did update group");
                    [self createOrUpdateGroupFromJSON:data];
                }
            }
        }
    }];
}

-(void) updateGroup:(Group *) group withNewComment:(NSString *) newComment {
    if(!group.rainbowID || group.rainbowID.length == 0){
        NSLog(@"The given group don't have a rainbow ID, that is not normal %@", group);
        return;
    }
    
    NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesGroups, _contactsManagerService.myContact.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", url_base, group.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesGroups];
    NSString *bodyValue = newComment;
    if(newComment.length == 0)
        bodyValue = @"";
    
    NSDictionary *bodyContent = @{@"name":group.name, @"comment":bodyValue};
    [_downloadManager putRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot update the group in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(!jsonResponse) {
                NSLog(@"Error: Cannot update the group in REST due to JSON parsing failure");
            } else {
                NSLog(@"Update group response");
                NSDictionary *data = [jsonResponse objectForKey:@"data"];
                if(data){
                    NSLog(@"Did update group");
                    [self createOrUpdateGroupFromJSON:data];
                }
            }
        }
    }];
}

-(void) addContact:(Contact *) contact inGroup:(Group *) group {
    if(!group.rainbowID || group.rainbowID.length == 0){
        NSLog(@"The given group don't have a rainbow ID, that is not normal %@", group);
        return;
    }
    
    if(!contact.rainbowID || contact.rainbowID.length == 0){
        NSLog(@"The given contact don't have a rainbow ID, that is not good %@", contact);
        return;
    }
    
    NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesGroups, _contactsManagerService.myContact.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/users/%@", url_base, group.rainbowID, contact.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesGroups];
    
    [_downloadManager postRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot add contact in group in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(!jsonResponse) {
                NSLog(@"Error: Cannot add contact in group in REST due to JSON parsing failure");
            } else {
                NSLog(@"Add contact in group response");
                NSDictionary *data = [jsonResponse objectForKey:@"data"];
                if(data){
                    [self insertContact:contact inGroup:group];
                }
            }
        }
    }];
}

-(void) removeContact:(Contact *) contact fromGroup:(Group *) group {
    if(!group.rainbowID || group.rainbowID.length == 0){
        NSLog(@"The given group don't have a rainbow ID, that is not normal %@", group);
        return;
    }
    
    if(!contact.rainbowID || contact.rainbowID.length == 0){
        NSLog(@"The given contact don't have a rainbow ID, that is not good %@", contact);
        return;
    }
    
    NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesGroups, _contactsManagerService.myContact.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/users/%@", url_base, group.rainbowID, contact.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesGroups];
    
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot remove contact in group in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(!jsonResponse) {
                NSLog(@"Error: Cannot remove contact in group in REST due to JSON parsing failure");
            } else {
                NSLog(@"Remove contact in group response");
                NSDictionary *data = [jsonResponse objectForKey:@"data"];
                if(data){
                    [self deleteContact:contact inGroup:group];
                }
            }
        }
    }];
}

-(NSArray <Group*> *) groupsForContact:(Contact *) contact {
    @synchronized (_groupsMutex) {
        NSMutableArray<Group*> *groupList = [NSMutableArray new];
        [_groups enumerateObjectsUsingBlock:^(Group * group, NSUInteger idx, BOOL * stopGroup) {
            [group.users enumerateObjectsUsingBlock:^(Contact * member, NSUInteger idx, BOOL *  stopMember) {
                if([member isEqual:contact]){
                    [groupList addObject:group];
                    *stopMember = YES;
                }
            }];
        }];
        
        [groupList sortUsingDescriptors:@[[GroupsService sortDescriptorGroupByName]]];
        return groupList;
    }
}

#pragma mark - Groups facilitors
+(NSSortDescriptor*) sortDescriptorGroupByName {
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
        return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
    }];
    return sortByName;
}

-(Group *) createGroupWithRainbowID:(NSString *) rainbowID {
    Group *group = nil;
    BOOL newGroup = NO;
    @synchronized (_groupsMutex) {
        group = [self getGroupByRainbowID:rainbowID];
        if (!group) {
            newGroup = YES;
            group = [Group new];
            group.rainbowID = rainbowID;
            [_groups addObject:group];
        }
    }

    [self fetchGroupDetails:group];
    if (newGroup) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kGroupsServiceDidAddGroup object:group];
    }
    return group;
}

-(Group *) createOrUpdateGroupFromJSON:(NSDictionary *) dict {
    Group *group = nil;
    @synchronized (_groupsMutex) {
        group = [self getGroupByRainbowID:[dict objectForKey:@"id"]];
        BOOL newGroup = NO;
        if (!group) {
            group = [Group new];
            newGroup = YES;
        }
        
        group.rainbowID = [dict objectForKey:@"id"];
        group.name = [dict objectForKey:@"name"];
        if([[dict objectForKey:@"creationDate"] length] > 0 && [[dict objectForKey:@"creationDate"] isKindOfClass:[NSString class]])
            group.creationDate = [NSDate dateFromJSONString:dict[@"creationDate"]];
        group.owner = [_contactsManagerService getContactWithRainbowID:[dict objectForKey:@"owner"]];
        
        if([dict objectForKey:@"comment"])
            group.comment = [[dict objectForKey:@"comment"] notNull];
        
        NSArray *users = dict[@"users"];
        
        for (id userObj in users) {            
            Contact *contact = nil;
            if([userObj isKindOfClass:[NSString class]]){
                contact = [_contactsManagerService getContactWithRainbowID:userObj];
                if (!contact){
                    __weak typeof(self) weakSelf = self;
                    __weak typeof(group) weakGroup = group;
                    [_contactsManagerService createRainbowContactAsynchronouslyWithRainbowID:userObj withCompletionHandler:^(Contact *theContact) {
                        // We must send the group updated notification so reuse the method that already do that
                        [weakSelf insertContact:theContact inGroup:weakGroup];
                    }];
                } else {
                    @synchronized (_groupsMutex) {
                        if(![group.users containsObject:contact])
                            [((NSMutableArray*)group.users) addObject:contact];
                    }
                }
            } else if([userObj isKindOfClass:[NSDictionary class]]){
                contact = [_contactsManagerService getContactWithRainbowID:userObj[@"id"]];
                if (!contact)
                    contact = [_contactsManagerService createOrUpdateRainbowContactFromJSON:userObj];
                @synchronized (_groupsMutex) {
                    if(![group.users containsObject:contact])
                        [((NSMutableArray*)group.users) addObject:contact];
                }
            } else {
                NSLog(@"Not supported object structure in users array %@", users);
                break;
            }
        }

        [_groupsCache setObjectAsync:[group dictionarayRepresentation] forKey:group.rainbowID completion:nil];
        
        if (newGroup) {
            [_groups addObject:group];
            [[NSNotificationCenter defaultCenter] postNotificationName:kGroupsServiceDidAddGroup object:group];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kGroupsServiceDidUpdateGroup object:group];
        }
    }
    return group;
    
}

-(Group *) getGroupByRainbowID:(NSString *) rainbowID {
    @synchronized (_groupsMutex) {
        for (Group *group in _groups) {
            if ([group.rainbowID isEqualToString:rainbowID]) {
                return group;
            }
        }
        return nil;
    }
}

-(void) removeGroup:(Group *) group {
    @synchronized (_groupsMutex){
        if([_groups containsObject:group]){
            [_groups removeObject:group];
            [_groupsCache removeObjectForKeyAsync:group.rainbowID completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kGroupsServiceDidRemoveGroup object:group];
        }
    }
}

-(void) fetchGroupDetails:(Group *) group {
    if (!group.rainbowID) {
        NSLog(@"Error: unable to get group details from server, group has no rainbowID..");
        return;
    }
    
    NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesGroups, _contactsManagerService.myContact.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", url_base, group.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesGroups];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get group %@ details in REST : %@ %@", group.rainbowID, error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get group %@ details in REST due to JSON parsing failure", group.rainbowID);
            return;
        }
        
        NSDictionary *groupDict = [jsonResponse objectForKey:@"data"];
        if(groupDict)
            [self createOrUpdateGroupFromJSON:groupDict];
    }];
}

-(void) insertContact:(Contact *) contact inGroup:(Group *) group {
    @synchronized (_groupsMutex) {
        @synchronized (_groupsMutex) {
            if(![group.users containsObject:contact]){
                [((NSMutableArray*)group.users) addObject:contact];
                [_groupsCache setObjectAsync:[group dictionarayRepresentation] forKey:group.rainbowID completion:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:kGroupsServiceDidUpdateGroup object:group];
                [[NSNotificationCenter defaultCenter] postNotificationName:kGroupsServiceDidUpdateGroupsForContact object:contact];
            } else
                NSLog(@"Contact %@ is already added in the group %@", contact, group);
        }
    }
}

-(void) deleteContact:(Contact *) contact inGroup:(Group *) group {
    @synchronized (_groupsMutex) {
        if([group.users containsObject:contact]){
            [((NSMutableArray*)group.users) removeObject:contact];
            [_groupsCache setObjectAsync:[group dictionarayRepresentation] forKey:group.rainbowID completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kGroupsServiceDidUpdateGroup object:group];
            [[NSNotificationCenter defaultCenter] postNotificationName:kGroupsServiceDidUpdateGroupsForContact object:contact];
        } else
            NSLog(@"Contact %@ not found in group %@", contact, group);
    }
}

-(void) updateGroup:(Group *) group withName:(NSString *) name andComment:(NSString *) comment {
    @synchronized (_groupsMutex) {
        group.name = name;
        group.comment = comment;
        [_groupsCache setObjectAsync:[group dictionarayRepresentation] forKey:group.rainbowID completion:nil];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kGroupsServiceDidUpdateGroup object:group];
}

#pragma mark - Search
-(NSArray<Group *> *) searchGroupsWithPattern:(NSString *) pattern {
    NSString *trimmedPattern = [pattern stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSMutableArray *subPredicates = [NSMutableArray array];
    NSArray *terms = [trimmedPattern componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    for (NSString *term in terms) {
        if(term.length == 0)
            continue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"name BEGINSWITH[cd] %@", term];
        [subPredicates addObject:predicate];
    }
    
    NSMutableSet<Group *> *result = [NSMutableSet set];
    @synchronized (_groupsMutex) {
        NSPredicate *filter = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
        [result addObjectsFromArray:[_groups filteredArrayUsingPredicate:filter]];
    }
    
    return [result allObjects];
}


@end
