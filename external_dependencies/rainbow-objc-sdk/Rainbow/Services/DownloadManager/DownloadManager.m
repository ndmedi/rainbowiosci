/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "DownloadManager.h"
#import "NSURLSession+SynchronousTask.h"
#import "Tools.h"
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
#import <UIKit/UIKit.h>
#endif

#import "PINCache.h"
#import "NSDate+Utilities.h"
#import "ServicesManager+Internal.h"
#import "NSDictionary+JSONString.h"
#import "PINCache+OperationQueue.h"
#import "NSData+JSON.h"

@interface DownloadManager () <NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate>
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSOperationQueue *downloadOperationQueue;
@property (nonatomic) UIBackgroundTaskIdentifier downloadBackgroundTaskId;
@property (nonatomic, strong) NSMutableDictionary<NSURLRequest*, DownloadManagerUploadTaskProgressionHandler> *uploadRequestWithProgressHandler;
@property (nonatomic, strong) PINCache *downloadManagerCache;
@property (nonatomic, strong) NSPredicate *filter;
@end

#define kHTTPRequestTimeoutValue 60

@implementation DownloadManager
-(instancetype) init {
    if(self = [super init]) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        __weak id weakSelf = self;
        _session = [NSURLSession sessionWithConfiguration:config delegate:weakSelf delegateQueue:nil];
        
        _downloadOperationQueue = [NSOperationQueue new];
        // If we use a too high value, the appliation become unstable and tends to freeze a lot !
        // tested on iPhone 5s, with value of 500 it freeze in 90% of cases, with a value of 20 it does not freeze.
        // We don't fix the maxConcurrentOperationCount because if there is not enought system ressource that will cause a SIGARBT crash.
        _downloadOperationQueue.maxConcurrentOperationCount = 20;
        _downloadOperationQueue.name = @"DownloadManagerOperationQueue";
        _downloadOperationQueue.qualityOfService = NSQualityOfServiceBackground;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        _downloadBackgroundTaskId = UIBackgroundTaskInvalid;
#endif
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newNetworkConnectionDetected:) name:kServicesManagerNewNetworkConnectionDetected object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        
        _uploadRequestWithProgressHandler = [NSMutableDictionary dictionary];
        
        _downloadManagerCache = [[PINCache alloc] initWithName:@"downloadManagerCache"];
        _downloadManagerCache.memoryCache.removeAllObjectsOnEnteringBackground = NO;
        
        NSArray* matchingURLsToFilter = @[@"jids", @"loginemails"];
        NSMutableArray *subPredicates = [NSMutableArray array];
        [matchingURLsToFilter enumerateObjectsUsingBlock:^(NSString * obj, NSUInteger idx, BOOL * stop) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS %@", obj];
            [subPredicates addObject:predicate];
        }];
        _filter = [NSCompoundPredicate orPredicateWithSubpredicates:subPredicates];
    }
    return self;
}

-(void) dealloc {
    _session = nil;
    [_downloadOperationQueue cancelAllOperations];
    _downloadOperationQueue = nil;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidFinishLaunchingNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kServicesManagerNewNetworkConnectionDetected object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    
    [_uploadRequestWithProgressHandler removeAllObjects];
    _uploadRequestWithProgressHandler = nil;
    
    _downloadManagerCache = nil;
    
    _filter = nil;
}


#pragma mark - Notifications

#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
-(void) applicationDidFinishLaunching:(NSNotification *) notification{
    NSLog(@"[DownloadManager] application did finish launching");
    [self newNetworkConnectionDetected:nil];
}

-(void) applicationDidBecomeActive:(NSNotification *) notification{
    NSLog(@"[DownloadManager] application did Become Active");
    if (_downloadBackgroundTaskId != UIBackgroundTaskInvalid) {
        NSLog(@"Download background task ended (id : %ld)", (unsigned long)_downloadBackgroundTaskId);
        UIApplication *application = [UIApplication sharedApplication];
        [application endBackgroundTask: _downloadBackgroundTaskId];
        _downloadBackgroundTaskId = UIBackgroundTaskInvalid;
    }
}

-(void) applicationWillResignActive:(NSNotification *) notification {
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground && _downloadBackgroundTaskId == UIBackgroundTaskInvalid){
        
        if(_downloadOperationQueue.operationCount > 0){
            NSLog(@"Application will enter in background, start long running task for all pending operations %@", [_downloadOperationQueue operations]);
            UIApplication *application = [UIApplication sharedApplication];
            _downloadBackgroundTaskId = [application beginBackgroundTaskWithExpirationHandler:^{
                NSLog(@"Download background task expired (id :%ld), cleanning everythings", (unsigned long)_downloadBackgroundTaskId);
                [_downloadOperationQueue cancelAllOperations];
                [application endBackgroundTask: _downloadBackgroundTaskId];
                _downloadBackgroundTaskId = UIBackgroundTaskInvalid;
                
            }];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSLog(@"Download background task started (id : %ld), waiting for the end of all enqueued operations", (unsigned long)_downloadBackgroundTaskId);
                // Wait until the pending operations finish
                [_downloadOperationQueue waitUntilAllOperationsAreFinished];
                
                [application endBackgroundTask:_downloadBackgroundTaskId];
                NSLog(@"Download background task done (id : %ld)", (unsigned long)_downloadBackgroundTaskId);
                _downloadBackgroundTaskId = UIBackgroundTaskInvalid;
            });
        }
    
}
}
#endif

-(void) newNetworkConnectionDetected:(NSNotification *) notification {
    NSMutableArray *existingKeys = [NSMutableArray array];
    [_downloadManagerCache.diskCache enumerateObjectsWithBlockAsync:^(NSString *key, NSURL *fileURL, BOOL *stop) {
        [existingKeys addObject:key];
    } completionBlock:^(id<PINCaching> cache) {
        for (NSString *uniqueKey in existingKeys) {
            [_downloadManagerCache objectForKeyAsync:uniqueKey completion:^(id<PINCaching> cache, NSString *key, NSDictionary *object) {
                NSURLRequest *request = [object objectForKey:@"request"];
                NSData *data = [object objectForKey:@"data"];
                [self replayRequest:request data:data uniqueRequestID:key];
            }];
        }
    }];
}

-(void) didChangeUser:(NSNotification *) notification {
    [_downloadManagerCache removeAllObjects];
}

-(void) didLogin:(NSNotification *) notification {
    _downloadOperationQueue.suspended = NO;
}

-(void) didLogout:(NSNotification *) notification {
    _downloadOperationQueue.suspended = YES;
    [_downloadOperationQueue cancelAllOperations];
    [_downloadManagerCache.operationQueue cancelAllOperations];
}

#pragma mark - Get with long running task in operation queue

-(void) getRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    [_downloadOperationQueue addOperationWithBlock:^{
        NSData *receivedData = nil;
        NSError *requestError = nil;
        NSURLResponse *response = nil;
        BOOL requestSucceed = [self performSynchronousRequestWithURL:url httpMethod:@"GET" body:nil requestHeadersParameter:requestHeadersParameter returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
        if(requestSucceed && !requestError){
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
            if (!requestError && httpResp.statusCode >= 400) {
                requestError = [NSError errorWithDomain:@"Network" code:httpResp.statusCode userInfo:nil];
            }
            
            if(completionHandler)
                completionHandler(receivedData, requestError, response);
        }
        else {
            completionHandler(nil, requestError, response);
        }
    }];
    [self checkIfLongRunningTaskMustBeStarted];
}

-(void) postRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    NSData *dataBody = [body dataUsingEncoding:NSUTF8StringEncoding];
    [self postRequestWithURL:url NSDataBody:dataBody requestHeadersParameter:requestHeadersParameter completionBlock:completionHandler];
}

-(void) postRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter shouldSaveRequest:(BOOL)shouldSaveRequest completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    NSData *dataBody = [body dataUsingEncoding:NSUTF8StringEncoding];
    [self postRequestWithURL:url NSDataBody:dataBody requestHeadersParameter:requestHeadersParameter shouldSaveRequest:shouldSaveRequest completionBlock:completionHandler];
}

-(void) postRequestWithURL:(NSURL *) url NSDataBody:(NSData *) dataBody requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    [self postRequestWithURL:url NSDataBody:dataBody requestHeadersParameter:requestHeadersParameter shouldSaveRequest:YES completionBlock:completionHandler];
}

-(void) postRequestWithURL:(NSURL *) url NSDataBody:(NSData *) dataBody requestHeadersParameter:(NSDictionary *) requestHeadersParameter shouldSaveRequest:(BOOL)shouldSaveRequest completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    [_downloadOperationQueue addOperationWithBlock:^{
        NSData *receivedData = nil;
        NSError *requestError = nil;
        NSURLResponse *response = nil;
        
        BOOL requestSucceed = [self performSynchronousRequestWithURL:url httpMethod:@"POST" data:dataBody requestHeadersParameter:requestHeadersParameter returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError progressionHandler:nil  shouldSaveRequest:shouldSaveRequest];
        if(requestSucceed && !requestError){
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
            if (!requestError && httpResp.statusCode >= 400) {
                requestError = [NSError errorWithDomain:@"Network" code:httpResp.statusCode userInfo:nil];
            }
            
        }
        if(completionHandler)
            completionHandler(receivedData, requestError, response);
    }];
    [self checkIfLongRunningTaskMustBeStarted];
}

-(void) checkIfLongRunningTaskMustBeStarted {
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    
    // [UIApplication applicationState] should be used from main thread
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self checkIfLongRunningTaskMustBeStarted];
        });
        return;
    }
    
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground && _downloadBackgroundTaskId == UIBackgroundTaskInvalid){
        if(_downloadOperationQueue.operationCount > 0){
            NSLog(@"Some download operations have been enqueued but there is no long running task for them, so start it");
            [self applicationWillResignActive:nil];
        }
    }
#endif
}

#pragma mark - Synchronous methods
-(BOOL) getSynchronousRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error {
    return [self performSynchronousRequestWithURL:url httpMethod:@"GET" body:nil requestHeadersParameter:requestHeadersParameter returningResponseData:responseData returningNSURLResponse:returnedResponse returningError:error];
}

-(BOOL) postSynchronousRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error shouldSaveRequest:(BOOL) shouldSaveRequest{
    return [self performSynchronousRequestWithURL:url httpMethod:@"POST" body:body requestHeadersParameter:requestHeadersParameter returningResponseData:responseData returningNSURLResponse:returnedResponse returningError:error shouldSaveRequest:NO];
}

-(BOOL) postSynchronousRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error {
    return [self performSynchronousRequestWithURL:url httpMethod:@"POST" body:body requestHeadersParameter:requestHeadersParameter returningResponseData:responseData returningNSURLResponse:returnedResponse returningError:error];
}

-(BOOL) postSynchronousRequestWithURL:(NSURL *) url data:(NSData *) data requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error {
    return [self performSynchronousRequestWithURL:url httpMethod:@"POST" data:data requestHeadersParameter:requestHeadersParameter returningResponseData:responseData returningNSURLResponse:returnedResponse returningError:error progressionHandler:nil];
}

-(BOOL) putSynchronousRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error {
    return [self performSynchronousRequestWithURL:url httpMethod:@"PUT" body:body requestHeadersParameter:requestHeadersParameter returningResponseData:responseData returningNSURLResponse:returnedResponse returningError:error];
}

-(BOOL) putSynchronousRequestWithURL:(NSURL *) url data:(NSData *) data requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error {
    return [self performSynchronousRequestWithURL:url httpMethod:@"PUT" data:data requestHeadersParameter:requestHeadersParameter returningResponseData:responseData returningNSURLResponse:returnedResponse returningError:error progressionHandler:nil];
}

-(BOOL) putSynchronousRequestWithURL:(NSURL *) url data:(NSData *) data requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error progressionHandler:(DownloadManagerUploadTaskProgressionHandler) progress {
    return [self performSynchronousRequestWithURL:url httpMethod:@"PUT" data:data requestHeadersParameter:requestHeadersParameter returningResponseData:responseData returningNSURLResponse:returnedResponse returningError:error progressionHandler:progress];
}

-(BOOL) deleteSynchronousRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error {
    return [self performSynchronousRequestWithURL:url httpMethod:@"DELETE" body:nil requestHeadersParameter:requestHeadersParameter returningResponseData:responseData returningNSURLResponse:returnedResponse returningError:error];
}

-(BOOL) deleteSynchronousRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error shouldSaveRequest :(BOOL) shouldSaveRequest {
    return [self performSynchronousRequestWithURL:url httpMethod:@"DELETE" body:nil requestHeadersParameter:requestHeadersParameter returningResponseData:responseData returningNSURLResponse:returnedResponse returningError:error shouldSaveRequest:shouldSaveRequest];
}

#pragma mark - Asynchronous methods
-(NSURLSessionUploadTask *) putRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    NSData *databody = [body dataUsingEncoding:NSUTF8StringEncoding];
    return [self performRequestWithURL:url httpMethod:@"PUT" body:databody requestHeadersParameter:requestHeadersParameter uniqueRequestID:nil completionBlock:completionHandler];
}

-(NSURLSessionUploadTask *) deleteRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    NSData *databody = [body dataUsingEncoding:NSUTF8StringEncoding];
    return [self performRequestWithURL:url httpMethod:@"DELETE" body:databody requestHeadersParameter:requestHeadersParameter uniqueRequestID:nil completionBlock:completionHandler];
}

-(NSURLSessionUploadTask *) deleteRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    return [self performRequestWithURL:url httpMethod:@"DELETE" body:[NSData data] requestHeadersParameter:requestHeadersParameter uniqueRequestID:nil completionBlock:completionHandler];
}

#pragma mark - Internal methods
-(NSURLSessionUploadTask *) performRequestWithURL:(NSURL *) url httpMethod:(NSString *) httpMethod body:(NSData *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter uniqueRequestID:(NSString *) uniqueRequestID completionBlock:(DownloadManagerCompletionHandler) completionHandler {
    
    if(body.length == 0 && ([httpMethod isEqualToString:@"POST"] || [httpMethod isEqualToString:@"PUT"] || [httpMethod isEqualToString:@"DELETE"])){
        body = [NSData data];
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:kHTTPRequestTimeoutValue];
    request.HTTPMethod = httpMethod;
#if DEBUG
    if([httpMethod isEqualToString:@"POST"] || [httpMethod isEqualToString:@"PUT"])
        request.HTTPBody = body;
#endif
    request.networkServiceType = NSURLNetworkServiceTypeBackground;
    [requestHeadersParameter enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL * stop) {
        [request addValue:value forHTTPHeaderField:key];
    }];
    
    NSString *requestKey = nil;
    if(!uniqueRequestID){
        [self saveRequestWithRequest:request httpMethod:httpMethod data:body];
    } else
        requestKey = uniqueRequestID;
    
    NSUInteger sendedBytes = request.HTTPMethod.length;
    sendedBytes += request.URL.description.length;
    sendedBytes += request.allHTTPHeaderFields.description.length;
    sendedBytes += request.HTTPBody.length;
    NSLog(@"[DownloadManager] performing request : %@ %@ sendsize %ld",httpMethod, request.URL, sendedBytes);
    NSURLSessionUploadTask *uploadTask = [_session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
        if (!error && httpResp.statusCode >= 400) {
            error = [NSError errorWithDomain:@"Network" code:httpResp.statusCode userInfo:nil];
        }
        if(error && [error.domain isEqualToString:NSURLErrorDomain] && error.code == NSURLErrorCancelled){
            NSLog(@"REQUEST CANCELED !!!");
            error = [NSError errorWithDomain:@"Network" code:NSURLErrorCancelled userInfo:@{NSLocalizedDescriptionKey:@"Request canceled due to something wrong"}];
        } else {
            if(requestKey){
                // This request has been performed without error remove it from the cache
                [_downloadManagerCache removeObjectForKeyAsync:requestKey completion:^(id<PINCaching>  _Nonnull cache, NSString * _Nonnull key, id  _Nullable object) {
                    NSLog(@"Remove saved object for key %@", key);
                }];
            }
        }
        
        NSUInteger receivedBytes = response.description.length;
        receivedBytes += data.length;
        [self logResponse:httpMethod url: request size: receivedBytes response: data error: error ];
        if(completionHandler)
            completionHandler(data, error, response);
    }];
    
    [uploadTask resume];
    return uploadTask;
}

-(BOOL) performSynchronousRequestWithURL:(NSURL *) url httpMethod:(NSString *) httpMethod data:(NSData *) data requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error progressionHandler:(DownloadManagerUploadTaskProgressionHandler) progress {
    return [self performSynchronousRequestWithURL:url httpMethod:httpMethod data:data requestHeadersParameter:requestHeadersParameter returningResponseData:responseData returningNSURLResponse:returnedResponse returningError:error progressionHandler:progress shouldSaveRequest:YES];
}

-(BOOL) performSynchronousRequestWithURL:(NSURL *) url httpMethod:(NSString *) httpMethod data:(NSData *) data requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error progressionHandler:(DownloadManagerUploadTaskProgressionHandler) progress shouldSaveRequest:(BOOL) shouldSaveRequest {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:kHTTPRequestTimeoutValue];
    request.HTTPMethod = httpMethod;
    request.HTTPBody = data;
    request.networkServiceType = NSURLNetworkServiceTypeBackground;
    
    [requestHeadersParameter enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL * stop) {
        [request addValue:value forHTTPHeaderField:key];
    }];
    
    if(progress){
        [_uploadRequestWithProgressHandler setObject:progress forKey:request];
    }
    
    NSString *requestKey = nil;
    if (shouldSaveRequest) {
        requestKey = [self saveRequestWithRequest:request httpMethod:httpMethod data:data];
    }
    
    NSURLResponse *response = nil;
    NSError *errorResult = nil;
    NSData *dataReceived = nil;
    
    NSUInteger sendedBytes = request.HTTPMethod.length;
    sendedBytes += request.URL.description.length;
    sendedBytes += request.allHTTPHeaderFields.description.length;
    sendedBytes += request.HTTPBody.length;
    NSLog(@"[DownloadManager] performing request : %@ %@ sendsize %ld",httpMethod, request.URL, sendedBytes);
    
    if(data.length > 0){
        dataReceived = [_session sendSynchronousUploadTaskWithRequest:request fromData:data returningResponse:&response error:&errorResult];
    } else {
        dataReceived = [_session sendSynchronousDataTaskWithRequest:request returningResponse:&response error:&errorResult];
    }
    *returnedResponse = response;
    *responseData = [NSData dataWithData:dataReceived];
    if (error)
        *error = errorResult;
    
    if(!errorResult) {
        if(requestKey){
            // This request has been performed without error remove it from the cache
            [_downloadManagerCache removeObjectForKeyAsync:requestKey completion:^(id<PINCaching>  _Nonnull cache, NSString * _Nonnull key, id  _Nullable object) {
                NSLog(@"Remove saved object for key %@", key);
            }];
        }
    }
    
    NSUInteger receivedBytes = response.description.length;
    receivedBytes += data.length;

    [self logResponse:httpMethod url: request size: receivedBytes response: dataReceived error: errorResult ];
    
    if(progress){
        [_uploadRequestWithProgressHandler removeObjectForKey:request];
    }
    return !errorResult;
}

-(BOOL) performSynchronousRequestWithURL:(NSURL *) url httpMethod:(NSString *) httpMethod body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error {
    return [self performSynchronousRequestWithURL:url httpMethod:httpMethod body:body requestHeadersParameter:requestHeadersParameter returningResponseData:responseData returningNSURLResponse:returnedResponse returningError:error shouldSaveRequest:YES];
}

-(BOOL) performSynchronousRequestWithURL:(NSURL *) url httpMethod:(NSString *) httpMethod body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error shouldSaveRequest:(BOOL) shouldSaveRequest{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:kHTTPRequestTimeoutValue];
    request.HTTPMethod = httpMethod;
    if([httpMethod isEqualToString:@"POST"] || [httpMethod isEqualToString:@"PUT"])
        request.HTTPBody = [body dataUsingEncoding:NSUTF8StringEncoding];
    request.networkServiceType = NSURLNetworkServiceTypeBackground;
    [requestHeadersParameter enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL * stop) {
        [request addValue:value forHTTPHeaderField:key];
    }];
    
    NSData *sendData = [body dataUsingEncoding:NSUTF8StringEncoding];
    NSString *requestKey = nil;
    if (shouldSaveRequest) {
        requestKey = [self saveRequestWithRequest:request httpMethod:httpMethod data:sendData];
    }
    NSURLResponse *response = nil;
    NSError *errorResult = nil;
    NSData *data = nil;
    
    NSUInteger sendedBytes = request.HTTPMethod.length;
    sendedBytes += request.URL.description.length;
    sendedBytes += request.allHTTPHeaderFields.description.length;
    sendedBytes += request.HTTPBody.length;
    NSLog(@"[DownloadManager] performing request : %@ %@ sendsize %ld",httpMethod, request.URL, sendedBytes);
    
    if(body.length > 0){
         data = [_session sendSynchronousUploadTaskWithRequest:request fromData:sendData returningResponse:&response error:&errorResult];
    } else {
        NSURL *location = [_session sendSynchronousDownloadTaskWithRequest:request returningResponse:&response error:&errorResult];
        data = [NSData dataWithContentsOfURL:location];
    }
    *returnedResponse = response;
    *responseData = [NSData dataWithData:data];
    if (error)
        *error = errorResult;

    if(!errorResult){
        if(requestKey){
        // This request has been performed without error remove it from the cache
            [_downloadManagerCache removeObjectForKeyAsync:requestKey completion:^(id<PINCaching>  _Nonnull cache, NSString * _Nonnull key, id  _Nullable object) {
                NSLog(@"Remove saved object for key %@", key);
            }];
        }
    }
    NSUInteger receivedBytes = response.description.length;
    receivedBytes += data.length;
    [self logResponse:httpMethod url: request size: receivedBytes response: data error: errorResult ];
    return !errorResult;
}

-(void) replayRequest:(NSURLRequest *) request data:(NSData *) data uniqueRequestID:(NSString *) uniqueRequestID {
    NSLog(@"Replay uniqueRequestID %@ http method %@ request %@, data %@", uniqueRequestID, request.HTTPMethod, request, data);
    [self performRequestWithURL:request.URL httpMethod:request.HTTPMethod body:data requestHeadersParameter:request.allHTTPHeaderFields uniqueRequestID:uniqueRequestID completionBlock:nil];
}

#pragma mark URLSession protocol
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler {
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        // Check certificate
        BOOL evaluationResult = NO;
        evaluationResult = [self evaluateServerTrust:challenge.protectionSpace.serverTrust];
        if(evaluationResult){
            // Accept certificate
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        } else {
            // Cancel request
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
        }
    } else {
        NSLog(@"Error didReceiveChallenge method=%@",challenge.protectionSpace.authenticationMethod);
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    NSLog(@"Warning didCompleteWithError error=%@",error);
}
- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error {
    NSLog(@"Warning didBecomeInvalidWithError error=%@",error);
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend {    
    if([_uploadRequestWithProgressHandler objectForKey:task.originalRequest]){
        DownloadManagerUploadTaskProgressionHandler progress = [_uploadRequestWithProgressHandler objectForKey:task.originalRequest];
        progress((double)totalBytesSent, (double)totalBytesExpectedToSend);
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    NSLog(@"didFinishDownloadingToURL");
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    NSLog(@"DID WRITE DATA %lld %lld",bytesWritten, totalBytesExpectedToWrite);
}

#pragma mark - certificate validation
// This method is used by Socket Rocket for certificate validation
-(BOOL) evaluateServerTrust:(SecTrustRef) serverTrust forDomain:(NSString *) domain {
    NSLog(@"Evaluating server trust %@ for domain %@", serverTrust, domain);
    return [self evaluateServerTrust:serverTrust];
}

-(BOOL) evaluateServerTrust:(SecTrustRef) serverTrust {
    NSLog(@"Evaluating server trust %@", serverTrust);
    SecPolicyRef policy = SecPolicyCreateBasicX509();
    
    NSMutableArray *policies = [NSMutableArray arrayWithObject:(__bridge id) policy];
    // Configure policies
    SecTrustSetPolicies(serverTrust, (__bridge CFTypeRef)(policies));
    
    // Check if certificate is valid
    SecTrustResultType result;
    OSStatus errorStatus = 0;
    BOOL res = [self serverTrustIsValid:serverTrust withError:errorStatus withTrustResultErrorCode:&result];
    if(policy)
        CFRelease(policy);
    NSLog(@"Evaluation return %@ for server trust %@", serverTrust, NSStringFromBOOL(res));
    return res;
}

/*
 * Check if the certificate is already trusted
 */
-(BOOL) serverTrustIsValid:(SecTrustRef) serverTrust withError:(OSStatus) status withTrustResultErrorCode:(SecTrustResultType*) result{
    status = SecTrustEvaluate(serverTrust, result);
    NSCAssert(status == errSecSuccess, @"SecTrustEvaluate error: %ld", (long int)status);
    
    if (*result == kSecTrustResultInvalid)
        NSLog(@"SSL Challenge Result: Invalid");
    else if (*result == kSecTrustResultProceed)
        NSLog(@"SSL Challenge Result: Proceed");
    else if (*result == kSecTrustResultDeny)
        NSLog(@"SSL Challenge Result: Deny");
    else if (*result == kSecTrustResultUnspecified)
        NSLog(@"SSL Challenge Result: Unspecified");
    else if (*result == kSecTrustResultRecoverableTrustFailure)
        NSLog(@"SSL Challenge Result: Recoverable Trust Failure");
    else if (*result == kSecTrustResultFatalTrustFailure)
        NSLog(@"SSL Challenge Result: Fatal Trust Failure");
    else if (*result == kSecTrustResultOtherError)
        NSLog(@"SSL Challenge Result: Other Error");
    
    return (*result == kSecTrustResultUnspecified || *result == kSecTrustResultProceed);
}

-(NSString *) saveRequestWithRequest:(NSURLRequest *) request httpMethod:(NSString *) httpMethod data:(NSData *) data {
    NSString *requestKey = nil;
    BOOL shouldSaveRequest = NO;
    if([httpMethod isEqualToString:@"POST"] || [httpMethod isEqualToString:@"PUT"] || [httpMethod isEqualToString:@"DELETE"]){
        shouldSaveRequest = ![_filter evaluateWithObject:request.URL.absoluteString];
    }
    
    if(shouldSaveRequest){
        requestKey = [Tools generateUniqueID];
        NSMutableDictionary *savedInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:request,@"request", nil];
        if(data)
            [savedInfo setObject:data forKey:@"data"];
        [_downloadManagerCache setObjectAsync:savedInfo forKey:requestKey completion:nil];
    }
    return requestKey;
}

- (void) logResponse:(NSString *) requestType url: (NSMutableURLRequest *) request size: (NSInteger)size response:(NSData *) receivedData error: (NSError * )error {
    
    if (!error)
        NSLog(@"[DownloadManager] performing request : %@ %@ receivedsize %ld",requestType, request.URL, size);
    else {
        NSDictionary *userInfo = [error userInfo];
        NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
            NSDictionary *response = [receivedData objectFromJSONData];
            if(response){
                NSInteger errorDetailsCode = [response[@"errorDetailsCode"] integerValue];
                NSLog(@"[DownloadManager] performing request : %@ %@ receivedsize %ld error description %@  error detail %ld",requestType, request.URL, size, errorString , errorDetailsCode);
            } else {
                NSLog(@"[DownloadManager] performing request : %@ %@ receivedsize %ld error description %@",requestType, request.URL, size, errorString);
            }
    }
}

@end
