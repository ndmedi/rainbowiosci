/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

typedef void (^DownloadManagerCompletionHandler)(NSData *receivedData, NSError *error, NSURLResponse *response);
typedef void(^DownloadManagerUploadTaskProgressionHandler) (double totalBytesSent, double totalBytesExpectedToSend);

@interface DownloadManager : NSObject

-(BOOL) evaluateServerTrust:(SecTrustRef) serverTrust forDomain:(NSString *) domain;

#pragma mark - Synchronous methods
-(BOOL) getSynchronousRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error;

-(BOOL) postSynchronousRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error;

-(BOOL) postSynchronousRequestWithURL:(NSURL *) url data:(NSData *) data requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error;

-(BOOL) postSynchronousRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error shouldSaveRequest:(BOOL) shouldSaveRequest;
-(BOOL) putSynchronousRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error;

-(BOOL) putSynchronousRequestWithURL:(NSURL *) url data:(NSData *) data requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error progressionHandler:(DownloadManagerUploadTaskProgressionHandler) progress;

-(BOOL) putSynchronousRequestWithURL:(NSURL *) url data:(NSData *) data requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error;

-(BOOL) deleteSynchronousRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error;

-(BOOL) deleteSynchronousRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter returningResponseData:(NSData **) responseData returningNSURLResponse:(NSURLResponse **) returnedResponse returningError:(NSError **) error shouldSaveRequest :(BOOL) shouldSaveRequest;
#pragma mark - Asynchronous methods
-(void) postRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler;

-(void) postRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter shouldSaveRequest:(BOOL)shouldSaveRequest completionBlock:(DownloadManagerCompletionHandler) completionHandler;

-(NSURLSessionUploadTask *) putRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler;

-(void) getRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler;

-(NSURLSessionUploadTask *) deleteRequestWithURL:(NSURL *) url body:(NSString *) body requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler;

-(NSURLSessionUploadTask *) deleteRequestWithURL:(NSURL *) url requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler;

#pragma mark - async method with NSData body
-(void) postRequestWithURL:(NSURL *) url NSDataBody:(NSData *) dataBody requestHeadersParameter:(NSDictionary *) requestHeadersParameter completionBlock:(DownloadManagerCompletionHandler) completionHandler;

@end
