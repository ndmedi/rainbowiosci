/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Tools.h"
#import "RainbowUserDefaults.h"
#import "UserSettings.h"

#import "ServicesManager.h"

@implementation Tools

NSString* NSStringFromBOOL(BOOL aBoolean) {
    return aBoolean?@"YES":@"NO";
}

+ (NSString *) applicationName {
    static NSString *_appName;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        _appName = [bundle objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        if (!_appName) {
            _appName = [bundle objectForInfoDictionaryKey:@"CFBundleName"];
        }
    });
    return _appName;
}

+ (NSString *) applicationVersion {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
#if DEBUG
    return [NSString stringWithFormat:@"DEBUG_%@.%@", majorVersion, minorVersion];
#else
    return [NSString stringWithFormat:@"%@.%@", majorVersion, minorVersion];
#endif
}

+ (NSString *) currentOS {
    NSString *os = @"";
#if (TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE)
    os = @"iOS";
#else
    os = @"OSX";
#endif
    return os;
}

+ (NSString *) currentOSVersion {
    return [[NSProcessInfo processInfo] operatingSystemVersionString];
}

// Encode a string to embed in an URL.
+(NSString*) encodeToPercentEscapeString:(NSString *) string {
    return [string stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
}

+ (NSString *)generateUniqueID {
    NSString *result = nil;
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    if (uuid) {
        result = (__bridge_transfer NSString *)CFUUIDCreateString(NULL, uuid);
        CFRelease(uuid);
    }
    
    return result;
}

+ (BOOL) isValidEmailAddress:(NSString *) emailAddress {
    NSString *emailValidationRegExp = @"^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.([a-zA-Z]{2,4})$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailValidationRegExp];
    
    if ([predicate evaluateWithObject:emailAddress])
        return YES;

    return NO;
}

// http://stackoverflow.com/a/7846956
+ (NSString *) byteSizeToFormattedString:(id)value {
    double convertedValue = [value doubleValue];
    int multiplyFactor = 0;
    NSArray *tokens = [NSArray arrayWithObjects:@"bytes", @"KB", @"MB", @"GB", @"TB", @"PB", @"EB", @"ZB", @"YB",nil];
    while (convertedValue > 1024) {
        convertedValue /= 1024;
        multiplyFactor++;
    }
    return [NSString stringWithFormat:@"%4.1f %@",convertedValue, [tokens objectAtIndex:multiplyFactor]];
}

+ (NSDate *) dateWithLastModifiedHeaderString:(NSString *) dateString {
    NSDate *date = [self dateFromString:dateString withFormat: @"E, dd MMM yyyy HH:mm:ss Z"];
    return date;
}

+ (NSDate *) dateFromString:(NSString *) dateString withFormat: (NSString *) formatString {
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormat setDateFormat:formatString];
    [dateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *date = [dateFormat dateFromString:dateString];
    return date;
}

+(NSString *) anonymizeString:(NSString *) string {
#if DEBUG
    return string;
#endif
    NSString *newBody = @"";
    NSError *regExpError = nil;
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:@"<body.*?>(.*?)</body>" options:NSRegularExpressionDotMatchesLineSeparators error:&regExpError];
    
    NSRange stringRange = NSMakeRange(0, string.length);
    NSString *first, *last;
    NSTextCheckingResult *matchText = [regExp firstMatchInString:string options:NSMatchingReportProgress range:stringRange];
    if(matchText){
        NSRange matchRange = [matchText rangeAtIndex:1];
        first = [NSString stringWithFormat:@"%c",[string characterAtIndex:matchRange.location]];
        last = [NSString stringWithFormat:@"%c",[string characterAtIndex:(matchRange.location + (matchRange.length -1))]];
    }
    NSString *template = [NSString stringWithFormat:@"<body>%@********%@</body>",first,last];
    newBody = [regExp stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, string.length) withTemplate:template];
    return newBody;
}

+(NSString *) anonymizeNotificationRequest:(NSString *) request {
    NSString *bodyPart = @"";
    NSRange start = [request rangeOfString:@"body: "];
    if (start.location != NSNotFound)
    {
        bodyPart = [request substringFromIndex:start.location + start.length];
        NSRange end = [bodyPart rangeOfString:@", categoryIdentifier:"];
        if (end.location != NSNotFound)
        {
            bodyPart = [bodyPart substringToIndex:end.location];
        }
    }
    return [request stringByReplacingOccurrencesOfString:bodyPart withString:@"******"];;
}

+ (NSMutableDictionary *) anonymizeNotificationUserInfo:(NSDictionary *) userInfo {
    NSMutableDictionary *mutableUserInfo = [NSMutableDictionary new];
    NSMutableDictionary *aps = [userInfo[@"aps"] mutableCopy];;
    NSMutableDictionary *alert = [aps[@"alert"] mutableCopy];
    if (alert)
        [alert setValue:@"******" forKey:@"body"];
    if (aps) {
        [aps setObject:alert forKey:@"alert"];
        [mutableUserInfo setObject:aps forKey:@"aps"];
    }
    for (NSString* key in userInfo) {
        if (![key isEqualToString:@"aps"]) {
            if ([key isEqualToString:@"last-message-body"]) {
                [mutableUserInfo setValue:@"******" forKey:key];
            } else {
                [mutableUserInfo setValue:userInfo[key] forKey:key];
            }
        }
    }
    return mutableUserInfo;
}

+(NSString *)valueForKey:(NSString *)key fromURL:(NSURL *)url {
    NSURLQueryItem *queryItem = [self containsQueryItem:key fromURL:url];
    if (queryItem) {
        return queryItem.value;
    } else return @"";
}

+ (NSURLQueryItem *)containsQueryItem:(NSString *)key fromURL:(NSURL *)url {
    NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:url resolvingAgainstBaseURL:NO];
    NSArray *queryItems = urlComponents.queryItems;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", key];
    NSURLQueryItem *queryItem = [[queryItems filteredArrayUsingPredicate:predicate] firstObject];
    return queryItem;
}


+(BOOL) isUsingSDK {
    return YES;
}

+(NSString *) resourceName {
    // Use resource name like "mobile_ios_D22C5EE0-40DC-4CD2-B849-324D2648E7B5"
    NSString *resourceName = [UserSettings sharedInstance].resourceUniqueID;
    NSString *sdkName = @"mobile_ios_sdk";
    NSString *noSdkName = @"mobile_ios";
    NSString *prefix;
    BOOL isSDK = [Tools isUsingSDK];
    if(isSDK){
        prefix = sdkName;
    } else {
        prefix = noSdkName;
    }
    if(resourceName.length == 0){
        resourceName = [NSString stringWithFormat:@"%@_%@", prefix, [Tools generateUniqueID]];
        NSLog(@"Saving resource name for this device %@", resourceName);
        [UserSettings sharedInstance].resourceUniqueID = resourceName;
    } else {
        NSLog(@"Reuse already saved resource name %@", resourceName);
        NSInteger resourceNameParts = [resourceName componentsSeparatedByString:@"_"].count;
        NSInteger nameParts = [prefix componentsSeparatedByString:@"_"].count;
        // Check if the first part has changed (with or without "_sdk")
        if(resourceNameParts - 1 != nameParts){
            NSLog(@"Resource name is invalid -> generate a new resource name");
            resourceName = [NSString stringWithFormat:@"%@_%@", prefix, [Tools generateUniqueID]];
            [UserSettings sharedInstance].resourceUniqueID = resourceName;
            NSLog(@"New resource name is %@", resourceName);
        }
    }
    return resourceName;
}

+ (NSString *) rainbowAppGroupPath {
    NSString *appGroupDirectoryPath = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:[RainbowUserDefaults suiteName]].path;
    return appGroupDirectoryPath;
}

@end
