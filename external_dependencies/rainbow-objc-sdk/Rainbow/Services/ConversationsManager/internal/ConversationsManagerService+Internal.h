/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConversationsManagerService.h"
#import "RoomsService.h"
#import "ContactsManagerService.h"
#import "DownloadManager.h"
#import "ApiUrlManagerService.h"
#import "XMPPService.h"
#import "FileSharingService.h"

// This one is used in internal for message browser
#define kConversationsManagerDidReceiveNewMessage @"didReceiveNewMessage"
#define kConversationsManagerDidReceiveCarbonCopyMessage @"didReceiveCarbonCopyMessage"
#define kConversationsManagerDidRemoveAllMessagesForConversation @"didRemoveAllMessagesForConversation"
#define kConversationsManagerDidRemoveMessageForConversation @"didRemoveMessageForConversation"
#define kConversationsManagerWillSendNewMessage @"willSendNewMessage"

#define kConversationsManagerDidUpdateMessage @"didUpdateMessage"

@interface ConversationsManagerService ()

@property (nonatomic) BOOL cacheLoaded;

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService roomsService:(RoomsService *) roomsService xmppService:(XMPPService *) xmppService fileSharingService:(FileSharingService *) fileSharingService;

-(Message *) createMessageFromJSON:(NSDictionary *) jsonDictionary;


#pragma mark - XEP-0184 unread messages

-(void) addMessageUnReadByMeFromPeer:(Peer *) peer message:(Message *) message;
-(void) markAsReadByMeMessageFromPeer:(Peer *) peer messageID:(NSString*) messageID;
-(void) sendReadByMeACKForAllMessagesTo:(Conversation *) conversation;
-(void) forceRefreshCache;

@end
