/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConversationsManagerService.h"
#import "ConversationsManagerService+Internal.h"
#import "ContactsManagerService+Internal.h"
#import "RoomsService+Internal.h"
#import "LoginManager+Internal.h"
#import "Contact.h"
#import "Conversation.h"
#import "Conversation+internal.h"
#import "Tools.h"
#import "ServicesManager.h"
#import "NSDictionary+JSONString.h"
#import "NSData+JSON.h"
#import "MessageInternal.h"
#import "NSDate+JSONString.h"
#import "MessagesBrowser+Internal.h"
#import "Room+Internal.h"
#import "RoomsService+Internal.h"
#import "defines.h"
#import "PINCache.h"
#import "NSDate+Utilities.h"
#import "UIImage+Thumbnail.h"
#import "XMPPMessage+XEP_0066Rainbow.h"    // XMPP messages with out of band data
#import "NSData+MimeType.h"
#import "File+Internal.h"
#import "MessageInternal.h"
#import "FileSharingService+Internal.h"
#import "NSObject+NotNull.h"
#import "CallLog+Internal.h"
#import "RTCService.h"
#import <Intents/Intents.h>
#import "PINCache+OperationQueue.h"
#import "RainbowUserDefaults.h"
#import "PINDiskCache+forceRefresh.h"
#import <Rainbow/RainbowUserDefaults.h>

#define kAttachmentFileLimit 1000000
NSString *const kConversationsManagerDidEndLoadingConversations = @"didEndLoadingConversations";

NSString *const kConversationsManagerDidAddConversation = @"didAddConversation";
NSString *const kConversationsManagerDidRemoveConversation = @"didRemoveConversation";
NSString *const kConversationsManagerDidRemoveAllConversations = @"didRemoveAllConversations";
NSString *const kConversationsManagerDidUpdateConversation = @"didUpdateConversation";
NSString *const kConversationsManagerDidStartConversation = @"didStartConversation";
NSString *const kConversationsManagerDidStopConversation = @"didStopConversation";
NSString *const kConversationsManagerDidChangeConversation = @"didChangeConversation";

NSString *const kConversationsManagerDidReceiveNewMessageForConversation = @"didReceiveNewMessageForConversation";
NSString *const kConversationsManagerDidReceiveComposingMessage = @"didReceiveComposingMessage";
NSString *const kConversationsManagerDidAckMessageNotification = @"didAckMessage";
NSString *const kConversationsManagerDidUpdateMessagesUnreadCount = @"didUpdateMessagesUnreadCount";

NSString *const kConversationsManagerDidReceiveConferenceReminderInConversation = @"didReceiveConferenceReminderInConversation";

NSString *const kConversationsFullyLoaded = @"conversationsFullyLoaded";

NSString *const kIsAbleToRefreshUIChanged = @"isAbleToRefreshUIChanged";
NSString *const kEndReceiveXMPPRequestsAfterResume = @"endReceiveXMPPRequestsAfterResume";


typedef void (^ConversationsManagerServiceDownloadFileCompletionHandler)(NSData *receivedData, NSError *error);

@interface ConversationsManagerService () <XMPPServiceConversationDelegate>
@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property (nonatomic, strong) ContactsManagerService *contactsManagerService;
@property (nonatomic, strong) RoomsService *roomsService;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) NSMutableArray<Conversation *> *conversations;
@property (nonatomic, strong) NSObject *conversationsMutex;
@property (nonatomic, strong) NSMutableArray<Message *> *pendingMessages;
@property (nonatomic, strong) NSObject *pendingMessagesMutex;
@property (nonatomic, strong) NSObject *pendingConversationsMutex;
@property (nonatomic, strong) NSMutableArray <Conversation*> *conversationsLoadedFromCache;
@property (nonatomic, strong) NSMutableDictionary * failedFilesDictionary;
@property (nonatomic, strong) FileSharingService *fileSharingService;
@property (nonatomic, strong) NSObject *cacheMutex;
@property (nonatomic, strong) PINCache *conversationCache;
@property (nonatomic, strong) PINCache *failedFilesCache;
@property (nonatomic) BOOL conversationFullyLoaded;
@end

@implementation ConversationsManagerService
-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService roomsService:(RoomsService *) roomsService xmppService:(XMPPService *) xmppService fileSharingService:(FileSharingService *) fileSharingService {
    self = [super init];
    if(self){

        _downloadManager = downloadManager;
        _apiUrlManagerService = apiUrlManagerService;
        _contactsManagerService = contactsManagerService;
        _roomsService = roomsService;
        _xmppService = xmppService;
        _xmppService.conversationDelegate = self;
        _fileSharingService = fileSharingService;
        _conversationsMutex = [NSObject new];
        _conversations = [NSMutableArray array];
        _pendingMessages = [NSMutableArray array];
        _pendingMessagesMutex = [NSObject new];
        _cacheMutex = [NSObject new];
        _conversationsLoadedFromCache = [NSMutableArray array];
        _cacheLoaded = NO;
        _conversationFullyLoaded = [[RainbowUserDefaults sharedInstance] boolForKey:kConversationsFullyLoaded];;
        _failedFilesDictionary = [NSMutableDictionary dictionary];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willLogin:) name:kLoginManagerInternalWillLogin object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didDisconnect:) name:kLoginManagerDidDisconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddCall:) name:kTelephonyServiceDidAddCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateCall:) name:kTelephonyServiceDidUpdateCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveCall:) name:kTelephonyServiceDidRemoveCallNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRoomInvitationStatusChanged:) name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
        
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
    
        _conversationCache = [[PINCache alloc] initWithName:@"conversationCache"];
        _conversationCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        
        [_conversationCache trimToDate:[NSDate dateWithDaysBeforeNow:30]];
        
        _failedFilesCache = [[PINCache alloc] initWithName:@"failedFielsCache"];
        _failedFilesCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        [_failedFilesCache trimToDate:[NSDate dateWithDaysBeforeNow:30]];
        
        _cacheLoaded = YES;
        [self loadConversationsFromCache];
      
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalWillLogin object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidDisconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidAddCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidUpdateCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidRemoveCallNotification object:nil];
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
    
    @synchronized (_conversationsMutex) {
        [(NSMutableArray*)_conversations removeAllObjects];
        _conversations = nil;
    }
    
    @synchronized (_pendingMessagesMutex) {
        [_pendingMessages removeAllObjects];
        _pendingMessages = nil;
    }
    
    [_conversationsLoadedFromCache removeAllObjects];
    _conversationsLoadedFromCache = nil;
    
    _cacheMutex = nil;

    [_failedFilesDictionary removeAllObjects];
    _failedFilesDictionary = nil;
    
    _downloadManager = nil;
    _apiUrlManagerService = nil;
    _contactsManagerService = nil;
    _roomsService = nil;
    _xmppService.conversationDelegate = nil;
    _xmppService = nil;
    _conversationsMutex = nil;
    _pendingConversationsMutex = nil;
    _pendingMessagesMutex = nil;
    _conversationCache = nil;
    _failedFilesCache = nil;
}

-(void) willLogin:(NSNotification *) notification {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if(!_cacheLoaded){
            [self loadConversationsFromCache];
            _cacheLoaded = NO;
        }
        
    });
}

-(void) didChangeUser:(NSNotification *) notification {
    [_conversationCache removeAllObjects];
    [_failedFilesCache removeAllObjects];
}

/**
 *  On login event, we fetch the conversation list from the server.
 */
-(void) didLogin:(NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRoom:) name:kRoomsServiceDidRemoveRoom object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didJoinRoom:) name:kRoomsServiceDidJoinRoom object:nil];
    
    [self fetchConversations];
}

-(void) didLogout:(NSNotification *) notification {
    @synchronized (_conversationsMutex) {
        [((NSMutableArray *)_conversations) removeAllObjects];
    }
    @synchronized (_pendingMessages) {
        [_pendingMessages removeAllObjects];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidRemoveAllConversations object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRemoveRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidJoinRoom object:nil];
    _cacheLoaded = NO;
    [_conversationCache.operationQueue cancelAllOperations];
}

-(void) didReconnect:(NSNotification *) notification {
    // Do not fetch conversations on reconnect it not needed
    if (_conversationFullyLoaded) {
        [self resyncFakeConversations];
    } else {
        NSLog(@"Conversation not Fully loaded => reload");
        [self fetchConversations];
    }
}


-(void) resyncFakeConversations {
    NSLog(@"Resynchronizing fake conversations");
    __block Peer *peer =nil;
    @synchronized(_conversationsMutex){
        [_conversations enumerateObjectsUsingBlock:^(Conversation * aConversation, NSUInteger idx, BOOL * stop) {
            if([aConversation.conversationId containsString:@"fake_"]){
                if([aConversation.peer.rainbowID isEqualToString:@"fake_rainbow_id"]){
                    ConversationType convType = [aConversation type] ;
                    if (convType == ConversationTypeRoom) {
                        Room *room = [_roomsService getOrCreateRainbowRoomSynchronouslyWithJid:aConversation.peer.jid];
                        if (room)
                            peer = room;
                        
                    } else if (convType == ConversationTypeUser) {
                        Contact *contact = [_contactsManagerService getOrCreateRainbowContactSynchronouslyWithJid:aConversation.peer.jid];
                        if(contact){
                            peer = contact;
                        }
                    } else if(convType == ConversationTypeBot) {
                        Contact *bot = [_contactsManagerService getContactWithJid:aConversation.peer.jid];
                        if(bot){
                            peer = bot;
                        }
                    }
                    [self updateFakeConversation:aConversation forPeer:peer];                      
                }
                else {
                    [self updateFakeConversation:aConversation forPeer:aConversation.peer];
                }
            }
        }];
    }
}

-(void)updateFakeConversation:(Conversation *)aConversation forPeer:(Peer *)peer {
    if (peer != nil) {
        // It's a fake conversation create on server side
        [_conversationCache removeObjectForKey:aConversation.conversationId];
        [self removeConversation:aConversation];
        NSError *error = nil;
        Conversation *theConversation = [self internalStartConversationWithPeer:peer error:&error];
        if(!error && theConversation){
            [self updateLastMessageForConversation:theConversation withLastMessage:aConversation.lastMessage];
        } else {
            NSLog(@"Error while creating conversation on server side %@", error);
        }
    }
    else {
        NSLog(@"Cannot sync fake conversation with jid : %@", aConversation.peer.jid);
    }
}
 
-(void) didDisconnect:(NSNotification *) notification {
}

-(void) loadFailedFilesFromCacheforConversation :(Conversation *) conversation {
   
    _failedFilesCache = [[PINCache alloc] initWithName:@"failedFielsCache"];
    _failedFilesCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
    [_failedFilesCache trimToDate:[NSDate dateWithDaysBeforeNow:30]];
    
    __weak __typeof__(self) weakSelf = self;
    NSMutableArray *existingKeys = [NSMutableArray array];
    NSLog(@"Failed Files cache : Start loading cache");
    [_failedFilesDictionary removeAllObjects];
    
    
    [_failedFilesCache.diskCache enumerateObjectsWithBlockAsync:^(NSString *key, NSURL *fileURL, BOOL *stop) {
        [existingKeys addObject:key];
    } completionBlock:^(id<PINCaching> cache) {
        NSDictionary *objDictionary = [_failedFilesCache objectForKey:conversation.conversationId];
        NSLog(@"Failed cache : Load cache Completion block start");
        for (NSString *key in objDictionary) {
            @synchronized (_failedFilesDictionary) {
                if (![_failedFilesDictionary objectForKey:key]) {
                    [_failedFilesDictionary setObject:[objDictionary objectForKey:key] forKey:key];
                }
            }
        }
        
        for (NSString *key in _failedFilesDictionary) {
            Message * message = [weakSelf createMessageFromJSON:[_failedFilesDictionary objectForKey:key]];
            message.attachment.tag = message.messageID;
            [self addFailedMessage:message.body forFileAttachment:message.attachment to:conversation completionHandler:^(Message *message, NSError *error) {
                NSLog(@"sucess ading failed message to UI");
            } attachmentUploadProgressHandler:^(Message *message, double totalBytesSent, double totalBytesExpectedToSend) {
                
            }];
        }
        
        NSLog(@"Failed File cache : Load cache Completion block end");
       
    }];

}


-(void) loadConversationsFromCache {
    // Load conversations from cache
    __weak __typeof__(self) weakSelf = self;
    NSMutableArray *existingKeys = [NSMutableArray array];
    NSLog(@"Conversation cache : Start loading cache");
    [_conversationsLoadedFromCache removeAllObjects];
    
    
    [_conversationCache.diskCache enumerateObjectsWithBlockAsync:^(NSString *key, NSURL *fileURL, BOOL *stop) {
        [existingKeys addObject:key];
    } completionBlock:^(id<PINCaching> cache) {
        NSLog(@"Conversation cache : Load cache Completion block start");
        for (NSString *key in existingKeys) {
            NSDictionary *obj = [_conversationCache objectForKey:key];
            Conversation *theConversation = [weakSelf createOrUpdateConversationFromDictionary:obj postNotifications:NO];
            theConversation.conversationId = key;
            @synchronized (_cacheMutex) {
                if(![_conversationsLoadedFromCache containsObject:theConversation])
                    [_conversationsLoadedFromCache addObject:theConversation];
            }
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidEndLoadingConversations object:nil];
        NSLog(@"Conversation cache : Load cache Completion block end");
    }];
}

-(void) loadConversationsFromCacheSynchrounsly {
    __weak __typeof__(self) weakSelf = self;
    NSMutableArray *existingKeys = [NSMutableArray array];
    NSLog(@"Conversation cache : Start loading cache");
    [_conversationsLoadedFromCache removeAllObjects];
    [_conversationCache.diskCache enumerateObjectsWithBlock:^(NSString * _Nonnull key, NSURL * _Nullable fileURL, BOOL * _Nonnull stop) {
        [existingKeys addObject:key];
    }];
    NSLog(@"Conversation cache : Load cache Completion block start");
    for (NSString *key in existingKeys) {
        NSDictionary *obj = [_conversationCache objectForKey:key];
        Conversation *theConversation = [weakSelf createOrUpdateConversationFromDictionary:obj postNotifications:NO];
        theConversation.conversationId = key;
        @synchronized (_cacheMutex) {
            if(![_conversationsLoadedFromCache containsObject:theConversation])
                [_conversationsLoadedFromCache addObject:theConversation];
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidEndLoadingConversations object:nil];
    NSLog(@"Conversation cache : Load cache Completion block end");
}

-(void) fetchConversations {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConversations];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConversations];
    NSLog(@"Get conversations list");
    _conversationFullyLoaded = NO;
    [[RainbowUserDefaults sharedInstance] setBool:_conversationFullyLoaded forKey:kConversationsFullyLoaded];
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *urlResponse) {
        if (error) {
            NSLog(@"Get conversations request returned an error: %@", error);
            return;
        }
        
        NSDictionary *response = [receivedData objectFromJSONData];
        if(!response) {
            NSLog(@"Get conversations JSON parsing failed");
            return;
        }
        
        NSDictionary *data = [response objectForKey:@"data"];
        NSLog(@"Get conversations return data ...");
        
        // List of conversations returned by server.
        NSMutableArray<Conversation*> *listOfConversations = [NSMutableArray array];
        
        @synchronized (_conversationsMutex) {
            for (NSDictionary *serverConversation in data) {
                Conversation *theConversation = [self createOrUpdateConversationFromDictionary:serverConversation];
                
                if(theConversation.peer)
                    [listOfConversations addObject:theConversation];
                else
                    NSLog(@"No peer found ignoring conversation %@", serverConversation);
            }
            // Now resync fake conversations
            [self resyncFakeConversations];
            
            // Check if there is some conversation that must be removed (because loaded from cache)
            [_conversationsLoadedFromCache enumerateObjectsUsingBlock:^(Conversation * aConversation, NSUInteger idx, BOOL * stop) {
                if([aConversation isKindOfClass:[Conversation class]]){
                    if(![listOfConversations containsObject:aConversation]){
                        NSLog(@"The conversation has been removed since last cache update : %@", aConversation);
                        [self removeConversation:aConversation];
                    }
                }
            }];
            
            _conversationFullyLoaded = YES;
            [[RainbowUserDefaults sharedInstance] setBool:_conversationFullyLoaded forKey:kConversationsFullyLoaded];
        }
        
        for (Conversation *aConversation in listOfConversations) {
            // Notify of unread messages
            aConversation.isSynchronized = NO;
            if(aConversation.unreadMessagesCount > 0)
                [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessagesUnreadCount object:aConversation];
            // Request all missing vcard
            if([aConversation.peer isKindOfClass:[Contact class]]){
                Contact *contact = (Contact *)aConversation.peer;
                if(!contact.vcardPopulated)
                    [_contactsManagerService addJidToFetch:contact.jid];
            }
        }

        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidEndLoadingConversations object:nil];
        NSLog(@"Get conversations end of treatment");
        [_conversationsLoadedFromCache removeAllObjects];
    }];
}

-(Message *) createMessageFromJSON:(NSDictionary *) jsonDictionary {
    Message *newMessage = [Message new];

    NSString *messageBody = jsonDictionary[@"lastMessageText"];
    NSDate *messageDate = [NSDate dateFromJSONString: jsonDictionary[@"lastMessageDate"]];
    newMessage.type = [Message messageTypeForBody:messageBody];
    newMessage.body = messageBody;
    newMessage.timestamp = messageDate;
    NSDictionary *call = [[jsonDictionary objectForKey:@"call"] notNull];
    if(call.count > 0){
        // This is a call log
        newMessage.type = MessageTypeWebRTC;
        NSString *callLogState = call[@"state"];
        if([callLogState isEqualToString:@"missed"]){
            newMessage.callLog = [CallLog new];
            newMessage.callLog.state = CallLogStateMissed;
            newMessage.callLog.date = messageDate;
            newMessage.callLog.type = CallLogTypeWebRTC;
        }
        if([callLogState isEqualToString:@"answered"]){
            NSInteger duration = [((NSString *)call[@"duration"]) integerValue];
            newMessage.callLog = [CallLog new];
            newMessage.callLog.state = CallLogStateAnswered;
            newMessage.callLog.date = messageDate;
            newMessage.callLog.realDuration = [NSNumber numberWithInteger:duration];
            newMessage.callLog.type = CallLogTypeWebRTC;
            
        }
    }
    
    if([jsonDictionary objectForKey:@"type"] && [jsonDictionary[@"type"] isEqualToString:@"room"]){
        newMessage.via = [_roomsService getRoomByJid:jsonDictionary[@"jid_im"]];
    }
    
    if(jsonDictionary[@"lastMessageSender"] && [jsonDictionary[@"lastMessageSender"] isKindOfClass:[NSString class]]){
        NSString *lastMessageSender = jsonDictionary[@"lastMessageSender"];
        // Last message sender can be the room itself (event message) don't create a peer with wrong lastmessagesenderjid for that event
        if([lastMessageSender containsString:@"room_"]){
            newMessage.peer = nil;
            newMessage.isOutgoing = NO;
        } else {
            if(jsonDictionary[@"jid_im"] && [jsonDictionary[@"jid_im"] isKindOfClass:[NSString class]]){
                NSString *lastMessageSenderID = lastMessageSender;
                NSArray *divArray = [lastMessageSender componentsSeparatedByString:@"/"];
                if (divArray.count > 1) {
                    lastMessageSenderID = divArray[0];
                }
                newMessage.peer = [_contactsManagerService createOrUpdateRainbowContactWithJid:lastMessageSenderID];
                newMessage.isOutgoing = ([_contactsManagerService.myContact.jid isEqualToString:jsonDictionary[@"jid_im"]]) ? YES : NO;
            if (newMessage.callLog.type == CallLogTypeWebRTC){
                newMessage.callLog.isOutgoing = newMessage.isOutgoing;
            }
            }
        }
    }
    
    // this is for offline attachment
    if (jsonDictionary[@"attachmentURL"]) {
        
        if (jsonDictionary[@"id"]) {
            newMessage.messageID = jsonDictionary[@"id"];
        }
        
        NSURL * url = (NSURL *)jsonDictionary[@"attachmentURL"];
        NSData *data = (NSData *)jsonDictionary[@"attachmentData"];
        
        newMessage.attachment = [_fileSharingService createFileWithFileName:jsonDictionary[@"fileName"]?jsonDictionary[@"fileName"]:[url lastPathComponent] size:[data length] mimeType:[data mimeTypeByGuessing] data:data url:url];
        
    }
    
    return newMessage;
}

-(void)simulateDidReceiveMessage:(Message *)message {
    [self xmppService:_xmppService didReceiveMessage:message];        
}

#pragma mark - Conversation delegate from XMPPService
-(void) xmppService:(XMPPService *) service willSendMessage:(Message *) message {
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerWillSendNewMessage object:message];
}

-(void) xmppService:(XMPPService *) service didSendMessage:(Message *) message {
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:message];
}

-(void) xmppService:(XMPPService *) service didReceiveMessage:(Message *) message {
    NSLog(@"Did receive message %@", message);
    // Don't create conversation with mediapillar
    if ([message.via.jid containsString:@"mp_"])
        return;
        
    Conversation *theConversation = nil;
    BOOL isNewConversation = NO;
    @synchronized (_conversationsMutex) {
        theConversation = [self getConversationWithPeer:message.via];
        // Conversation does not exist ?

        // If we are not connected in xmpp we create a fake conversation like that we will be notified of the update of the conversation
        if(!theConversation && !_xmppService.isXmppConnected) {
            Peer *peer = message.via;
            
            NSString *type;
            
            if (peer == nil) {
                //push message received on unknown peer do not update conversation and wait resume to resync
                return;
            }else if ([peer isKindOfClass:[Contact class]]) {
                type = @"user";
                if(((Contact*)peer).isBot)
                    type = @"bot";
            } else if ([peer isKindOfClass:[Room class]]) {
                type = @"room";
            } 
            
            NSMutableDictionary * data = [[NSMutableDictionary alloc] init];
            [data setObject:[NSString stringWithFormat:@"fake_%@",peer.jid] forKey:@"id"];
            if (peer.rainbowID != nil) {
                [data setObject:peer.rainbowID forKey:@"peerId"];
            }
            else {
                [data setObject:@"fake_rainbow_id" forKey:@"peerId"];
            }
            [data setObject:peer.jid forKey:@"jid_im"];
            [data setObject:type forKey:@"type"];
            NSString *displayName = [peer getPeerDisplayName];
            if (displayName == nil) {
                displayName = peer.displayName;
            }
            if (displayName != nil) {
                [data setObject:displayName forKey:@"displayName"];
                NSArray* nameArray = [displayName componentsSeparatedByString:@" "];
                if (nameArray.count) {
                    [data setObject:[nameArray objectAtIndex:0] forKey:@"firstName"];
                }
                if (nameArray.count > 1) {
                    [data setObject:[nameArray objectAtIndex:1] forKey:@"lastName"];
                }
            }
            
            theConversation = [self createOrUpdateConversationFromDictionary:data];
        }
        
        if(!theConversation) {
            if([message.via isKindOfClass:[Contact class]] && !message.via.rainbowID) {
                NSLog(@"We don't have any information on the sender of this message %@", message);
                @synchronized (_pendingMessagesMutex) {
                    [_pendingMessages addObject:message];
                }
                [_contactsManagerService populateVcardForContactsJids:@[message.via.jid]];
                return;
            }
            if([message.via isKindOfClass:[Room class]]){
                NSLog(@"We got a message for a room, check our status in this room");
                Room *aRoom = [_roomsService getRoomByJid:message.via.jid];
                if(aRoom){
                    if(aRoom.myStatusInRoom != ParticipantStatusAccepted){
                      NSLog(@"Don't treat that message our status in not ParticipantStatusAccepted");
                        return;
                    }
                }
            }
            // start a new conversation
            NSError *error = nil;
            theConversation = [self internalStartConversationWithPeer:message.via error:&error];
            if(error){
                NSLog(@"Error while creating conversation on server side %@", error);
                return;
            }
            
            isNewConversation = YES;
        }
        
        if (message.attachment.isDownloadAvailable) {
            [_fileSharingService fetchDataForFile:message.attachment sizeLimit:kAttachmentFileLimit completionHandler:^(File *file, NSError *error) {
                if(!error){
                    message.attachment.thumbnailData = file.thumbnailData;
                    NSLog(@"success fetching data for message %@", message.messageID);
                    
                } else {
                    message.attachment.thumbnailData = nil;
                    NSLog(@"Error fetching data for message %@", message.messageID);
                }
                 [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:message];
            }];
        }        
        
        BOOL didReceiveMessageInNotifications = [theConversation.unReadMessageIDs containsObject:message.messageID];
        if (!didReceiveMessageInNotifications) {
            [self updateLastMessageForConversation:theConversation withLastMessage:message];
            [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:message];
        }
        
        // Group chat events
        if(message.type == MessageTypeGroupChatEvent){
            // TODO : is it needed ?
            if(message.groupChatEventType == MessageGroupChatEventConferenceReminder){
                [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidReceiveConferenceReminderInConversation object:theConversation];
            }
        } else {
            // XEP-184
            // When we receive a new message, add it to our list of message not read by ME.
            if((!message.isOutgoing || [message.via isKindOfClass:[Room class]])) {
                [self addMessageUnReadByMeFromPeer:message.via message:message];
            }
        }
        
        // We must not increase unreadMessageCount for message received in push because it will be done by synchronization done at login, BUT in case of new conversation, the fetch mecanism will not return it (because the conversation doesn't exist yet) so we have to increase it only if it's a new conversation and for message received in push !
        if(isNewConversation){
            if(message.hasBeenPresentedInPush){
                theConversation.unreadMessagesCount = [theConversation.unReadMessageIDs count];
                [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessagesUnreadCount object:theConversation];
            }
        } else {
            /*
             When resume succeed the message are presented with a "Resent" element so we have to increment the counter because it a resume
             Message presented in push "Retransmission" element are presented only when session failed to resume so it will be a full login and in that case we don't increment the counters
             */
            if(message.callLog && message.callLog.state != CallLogStateMissed) {
                // Don't increase counter for call log not missed
            } else {
                if(!message.hasBeenPresentedInPush && !message.isOutgoing && [message isEqual:theConversation.lastMessage] && !didReceiveMessageInNotifications){
                    if (theConversation.unreadMessagesCount < [theConversation.unReadMessageIDs count]) {
                        theConversation.unreadMessagesCount = [theConversation.unReadMessageIDs count];
                    }
                    else {
                        theConversation.unreadMessagesCount += 1;
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessagesUnreadCount object:theConversation];
                }
            }
        }
        
        [_conversationCache setObjectAsync:[theConversation dictionaryRepresentation] forKey:theConversation.conversationId completion:nil];
        NSLog(@"Notify new message %@ in conversation %@", message, theConversation);
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidReceiveNewMessage object:message];
        // We don't notify that we have a new message for a conversation if that message is not the last message of the conversation
        // That case occurs when the application is killed during a stream resume, at next startup the message will be again presented in stream resume, but bevause we already have treated it the message will not be the last message.
        if([message isEqual:theConversation.lastMessage]){
            [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidReceiveNewMessageForConversation object:theConversation];
        }
    }
}

-(void) xmppService:(XMPPService *) service didReceiveCarbonCopyMessage:(Message *)message {
    Conversation *theConversation = nil;
    @synchronized (_conversationsMutex) {
        theConversation = [self getConversationWithPeer:message.via];
        
        // Conversation does not exist ?
        if(!theConversation){
            if([message.via isKindOfClass:[Contact class]] && !message.via.rainbowID) {
                NSLog(@"We don't have any information on the sender of this message %@", message);
                [_contactsManagerService populateVcardForContactsJids:@[message.via.jid]];
                @synchronized (_pendingMessagesMutex) {
                    [_pendingMessages addObject:message];
                }
                return;
            }
            
            NSError *error = nil;
            theConversation = [self internalStartConversationWithPeer:message.via error:&error];
            if(error){
                NSLog(@"Error while creating conversation on server side %@", error);
                return;
            }
        }
        
        [self updateLastMessageForConversation:theConversation withLastMessage:message];
        
        // XEP-0184 unreceived & unread messages.
        if(message.isOutgoing && [message.via isKindOfClass:[Contact class]]) {
            // It's a carbon message sent by ME on another of my devices.
            // This message :
            // - has been received by the server obviously (as it forwarded it to me)
            // - has not been received and read by the contact.
            //[self addMessageUnReceivedByPeer:message.via messageID:message.messageID];
            //[self addMessageUnReadByPeer:message.via messageID:message.messageID];
        }
        else {
            // It's a carbon message sent by one of my contacts.
            // Add it to our list of unReadByMe messageIDs
            [self addMessageUnReadByMeFromPeer:message.via message:message];
        }
    }
    
    if (message.attachment.url) {
        if (message.attachment.isDownloadAvailable) {
            [_fileSharingService fetchDataForFile:message.attachment sizeLimit:kAttachmentFileLimit completionHandler:^(File *file, NSError *error) {
                if(!error){
                    message.attachment.thumbnailData = file.thumbnailData;
                    NSLog(@"success fetching data for message %@", message.messageID);
                } else {
                    message.attachment.thumbnailData = nil;
                    NSLog(@"Error fetching data for message %@", message);
                }
                 [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:message];
            }];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidReceiveCarbonCopyMessage object:message];
}

-(void) xmppService:(XMPPService *) service didReceiveComposingMessage:(Message *)message {
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidReceiveComposingMessage object:message];
}

-(void) xmppService:(XMPPService *) service didReceiveArchivedMessage:(Message *)message {
    // XEP-0184 unreceived & unread messages.
    if(message.isOutgoing && [message.via isKindOfClass:[Contact class]]) {
        // It's an archived message sent by ME.
        // This message :
        // - has been received by the server obviously (as it forwarded it to me)
        // - might have not been received and read by the contact.
        /*if (!message.isArchivedMessageReceived) {
         [self addMessageUnReceivedByPeer:message.via messageID:message.messageID];
         }
         if (!message.isArchivedMessageRead) {
         [self addMessageUnReadByPeer:message.via messageID:message.messageID];
         }*/
    }
    else {
        // It's a carbon message sent by one of my contacts.
        // Add it to our list of unReadByMe messageIDs
        if (message.state != MessageDeliveryStateRead) {
            [self addMessageUnReadByMeFromPeer:message.via message:message];
        }
    }
    
    Conversation *theConversation = [self getConversationWithPeer:message.via];
    [self updateLastMessageForConversation:theConversation withLastMessage:message];
    
    if (message.attachment.isDownloadAvailable) {
        [_fileSharingService fetchDataForFile:message.attachment sizeLimit:kAttachmentFileLimit completionHandler:^(File *file, NSError *error) {
            if(!error){
                message.attachment.thumbnailData = file.thumbnailData;
                NSLog(@"success fetching data for message %@", message.messageID);
            } else {
                message.attachment.thumbnailData = nil;
                NSLog(@"Error fetching data for message %@", message);
            }
             [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:message];
        }];
    }
}

/**
 *  This is called when we receive an acknowledgement message,
 *  to inform that a "real" message delivery status has changed.
 *
 *  @param service    The xmpp service
 *  @param state      The new state of the message
 *  @param datetime   The datetime when this state has happened
 *  @param message    The message which has changed
 *  @param via        The via peer with which the message has been exchanged
 *  @param isOutgoing Whether the message is incoming or outgoing
 */
-(void) xmppService:(XMPPService *) service didReceiveDeliveryState:(MessageDeliveryState) state at:(NSDate*) datetime forMessage:(Message*) message via:(Peer*) via isOutgoing:(BOOL) isOutgoing {
    
    NSAssert(message.messageID, @"MessageID should be set for didReceiveDeliveryState:");
    
    if(state == MessageDeliveryStateRead) {
        Conversation *theConversation = [self getConversationWithPeer:via];
        if(!theConversation){
            NSLog(@"We try to mark as read-by-me the messageID %@ for an unknown conversation with %@, we don't treat it", message.messageID, via);
            return;
        }
        
        // If the message has been read on another device we must remove it from our list of message the mark as read.
        @synchronized (theConversation) {
            [theConversation.unReadMessageIDs removeObject:message.messageID];
            theConversation.unreadMessagesCount = theConversation.unReadMessageIDs.count;
        }
    
        [_conversationCache setObjectAsync:[theConversation dictionaryRepresentation] forKey:theConversation.conversationId completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessagesUnreadCount object:theConversation];
    }
    
    NSMutableDictionary *userInfo = [NSMutableDictionary new];
    [userInfo setObject:message.messageID forKey:@"messageID"];
    [userInfo setObject:[NSNumber numberWithInt:state] forKey:@"state"];
    // Add the datetime if available
    if (datetime) {
        [userInfo setObject:datetime forKey:@"datetime"];
        if (isOutgoing && state == MessageDeliveryStateDelivered) {
            message.timestamp = [userInfo objectForKey:@"datetime"];
            Conversation *theConversation = [self getConversationWithPeer:message.via];
            
            if(theConversation)
                [self updateLastMessageTimestampForConversation:theConversation withLastMessage:message];
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidAckMessageNotification object:userInfo];
    
}

-(void) xmppService:(XMPPService *) service didRetreiveNewLastMessageFromArchives:(Message *) lastMessage {
    //Peer *peer = lastMessage.isOutgoing ? lastMessage.toContact : lastMessage.fromContact;
    
    // TODO : Check this point
    // we can have webrtc messages from us -> to us ! (like ringing notif)
    // prevent to create a conversation with myself..
    
    Conversation *theConversation = [self getConversationWithPeer:lastMessage.via];
    if(theConversation)
        [self updateLastMessageForConversation:theConversation withLastMessage:lastMessage];
}

-(void) xmppService:(XMPPService *)service didReceiveDeleteConversationID:(NSString *) conversationID {
    @synchronized(_conversationsMutex){
        Conversation *theConversation = [self getConversationWithID:conversationID];
        if(theConversation){
            [self removeConversation:theConversation];
        }
    }
}

-(void) xmppService:(XMPPService *)service didRemoveAllMessagesForPeer:(Peer *)peer {
    Conversation *theConversation = [self getConversationWithPeer:peer];
    if(theConversation){
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidRemoveAllMessagesForConversation object:theConversation];
        [self updateLastMessageForConversation:theConversation withLastMessage:nil];
        [_failedFilesDictionary removeAllObjects];
        //[_failedFilesCache removeObjectForKey:theConversation.conversationId];
    }
}

-(void) xmppService:(XMPPService *) service didReceiveDeleteAllMessagesInConversationWithPeerJID:(NSString *) peerJID {
    Conversation *conversation = [self getConversationWithPeerJID:peerJID];
    if (conversation) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidRemoveAllMessagesForConversation object:conversation];
        [self updateLastMessageForConversation:conversation withLastMessage:nil];
    }
}

-(void) xmppService:(XMPPService *) service didReceiveDeleteMessageID:(NSString *) messageID inConversationWithPeerJID:(NSString *) peerJID {
    Conversation *conversation = [self getConversationWithPeerJID:peerJID];
    if (conversation) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidRemoveMessageForConversation object:conversation];
        if ([conversation.lastMessage.messageID isEqualToString:messageID]) {
            [self updateLastMessageForConversation:conversation withLastMessage:nil];
        }
    }
}

-(void) xmppService:(XMPPService *)service didReceiveChangedInConversationWithPeer:(Peer *) peer {
    Conversation *conversation = [self getConversationWithPeer:peer];
    if (conversation) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidChangeConversation object:conversation];
    }
}

-(void) xmppService:(XMPPService *)service didReceiveMarkAllAsReadInConversationWithPeerJID:(NSString *) peerJID andReadByMe: (BOOL) isReadByMe{
    Conversation *theConversation =  [self getConversationWithPeerJID:peerJID];
    if(!theConversation){
        NSLog(@"We try to mark all as read for an unknown conversation with %@, we don't treat it", peerJID);
        return;
    }
    //Mark All message as read in message browser
    @synchronized (theConversation) {
        if (isReadByMe) {
            for(NSString *messageID in theConversation.unReadMessageIDs) {
                NSMutableDictionary *userInfo = [NSMutableDictionary new];
                [userInfo setObject:messageID forKey:@"messageID"];
                [userInfo setObject:[NSNumber numberWithInt:MessageDeliveryStateRead] forKey:@"state"];
                [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidAckMessageNotification object:userInfo];
            }
   
            // If the message has been read on another device we must remove it from our list of message the mark as read.
            
            [theConversation.unReadMessageIDs removeAllObjects];
            theConversation.unreadMessagesCount = 0;
        } else {
            NSDate *now = [[NSDate alloc] init];
             NSArray *pageMessages = [_xmppService getArchivedMessageWithContactJid:peerJID maxSize:[NSNumber numberWithInteger:NSIntegerMax] offset:0];
            [pageMessages enumerateObjectsUsingBlock:^(Message *theMessage, NSUInteger idx, BOOL * stop) {
                if (theMessage.isOutgoing && theMessage.state != MessageDeliveryStateRead) {
                    [theMessage setDeliveryState:MessageDeliveryStateRead atTimestamp:now];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:theMessage];
                }
            }];
            
        }
    }
    
    [_conversationCache setObjectAsync:[theConversation dictionaryRepresentation] forKey:theConversation.conversationId completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessagesUnreadCount object:theConversation];
}

#pragma mark - Contact update notification
-(void) didUpdateContact:(NSNotification *) notification {
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Contact *theContact = [userInfo objectForKey:kContactKey];
    
    if(theContact.rainbowID) {
        __block Message *thePendingMessage = nil;
        @synchronized (_pendingMessagesMutex) {
            [_pendingMessages enumerateObjectsUsingBlock:^(Message * aMessage, NSUInteger idx, BOOL * stop) {
                if([aMessage.via isEqual:theContact]){
                    thePendingMessage = aMessage;
                    *stop = YES;
                }
            }];
            if(thePendingMessage)
                [_pendingMessages removeObject:thePendingMessage];
        }
        
        if(thePendingMessage){
            // Treat this message like a new message
            [self xmppService:_xmppService didReceiveMessage:thePendingMessage];
        }
    } else {
        NSLog(@"No rainbowID for contact, could not treat pending messages %@", theContact);
    }
}

#pragma mark - CRUD of conversations
-(Conversation *) createOrUpdateConversationFromDictionary:(NSDictionary *) dictionary {
    return [self createOrUpdateConversationFromDictionary:dictionary postNotifications:YES];
}

-(Conversation *) createOrUpdateConversationFromDictionary:(NSDictionary *) dictionary postNotifications:(BOOL) notify {
    NSString *conversationID = dictionary[@"id"];
    //    NSLog(@"createOrUpdateConversation %@", conversationID);
    Conversation *theConversation;
    BOOL newConversation = NO;
    Peer *peer;
    // in this dict :
    // "id" is the conversation ID.
    // "type" is the conv type; can be "room", "user" or "bot"
    // "peerId" is the roomID, the userID or the botID
    // "jid_im" is the roomJID, the userJID or the botJID
    
    // First, we look if this conversation already exists
    @synchronized (_conversationsMutex) {
        theConversation = [self getConversationWithID:conversationID];
        
        if(!theConversation) {
            theConversation = [Conversation new];
            theConversation.conversationId = conversationID;
            newConversation = YES;
        }
        
        
        NSString *conversationType = dictionary[@"type"];
        // extract specific information
        if ([conversationType isEqualToString:@"user"]) {
            // We have to replace the "id" data with "peerId" data.
            NSMutableDictionary *contactDict = [dictionary mutableCopy];
            [contactDict setObject:[contactDict objectForKey:@"peerId"] forKey:@"id"];
            peer = [_contactsManagerService createOrUpdateRainbowContactFromJSON:contactDict];
            if(newConversation && [dictionary objectForKey:@"creationDate"]){
                theConversation.creationDate = [NSDate dateFromJSONString:[dictionary objectForKey:@"creationDate"]];
            }
            contactDict = nil;
        } else if ([conversationType isEqualToString:@"room"]) {
            // We have to replace the "id" data with "peerId" data.
            NSMutableDictionary *roomDict = [dictionary mutableCopy];
            [roomDict setObject:[roomDict objectForKey:@"peerId"] forKey:@"id"];
            [roomDict setObject:[roomDict objectForKey:@"jid_im"] forKey:@"jid"];
            // The creationDate is the conversation creation date not the room creation date
            [roomDict removeObjectForKey:@"creationDate"];
            peer = [_roomsService createOrUpdateRoomFromJSON:roomDict];
            roomDict = nil;
        } else if([conversationType isEqualToString:@"bot"]){
            NSMutableDictionary *botDic = [dictionary mutableCopy];
            if([botDic objectForKey:@"name"])
                [botDic setObject:[botDic objectForKey:@"name"] forKey:@"lastName"];
            [botDic setObject:[botDic objectForKey:@"peerId"] forKey:@"id"];
            peer = [_contactsManagerService createOrUpdateRainbowBotContactFromJSON:botDic];
            botDic = nil;
        }
        
        //
        if(newConversation) {
            theConversation.peer = peer;
            
            @synchronized (_conversationsMutex) {
                [(NSMutableArray *)_conversations addObject:theConversation];
            }
        }
        
        // Extract non-specific information
        if([dictionary objectForKey:@"lastMessageText"] &&
           [dictionary objectForKey:@"lastMessageDate"] &&
           [[dictionary objectForKey:@"lastMessageDate"] isKindOfClass:[NSString class]]) {
            
            Message *lastMessage = [self createMessageFromJSON:dictionary];
            theConversation.lastMessage = lastMessage;
        }
        
        if([dictionary objectForKey:@"unreadMessageNumber"]) {
            NSString *unreadMessageNumber = dictionary[@"unreadMessageNumber"];
            theConversation.unreadMessagesCount = unreadMessageNumber.integerValue;
            [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessagesUnreadCount object:theConversation];
        }
        
        if([dictionary objectForKey:@"mute"]){
            theConversation.isMuted = ((NSNumber *)[dictionary objectForKey:@"mute"]).boolValue;
        }
        if([dictionary objectForKey:@"unreadMessagesCount"]){
            theConversation.unreadMessagesCount = ((NSNumber *)[dictionary objectForKey:@"unreadMessagesCount"]).integerValue;
            if(notify)
                [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessagesUnreadCount object:theConversation];
        }
        
        [_conversationCache setObjectAsync:[theConversation dictionaryRepresentation] forKey:theConversation.conversationId completion:nil];
        
        if(notify){
            if(newConversation) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidAddConversation object:theConversation];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateConversation object:theConversation];
            }
        }
    }
    //    NSLog(@"createOrUpdateConversation %@", theConversation);
    return theConversation;
}

/**
 *  This (as opposite to stopConversation:) is called when closing 
 *  a conversation on this device or from another device !
 *
 *  @param theConversation The conversation to close.
 */
-(void) removeConversation:(Conversation *) theConversation {
    @synchronized (_conversationsMutex) {
        NSLog(@"Removing conversation %@", theConversation);
        [(NSMutableArray *)_conversations removeObject:theConversation];
    }
    [_conversationCache removeObjectForKeyAsync:theConversation.conversationId completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidRemoveConversation object:theConversation];
}

-(void) updateLastMessageForConversation:(Conversation *) theConversation withLastMessage:(Message *) lastMessage {
    
    if (theConversation) {
       
        theConversation.lastMessage = lastMessage;
        [_conversationCache setObjectAsync:[theConversation dictionaryRepresentation] forKey:theConversation.conversationId completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateConversation object:theConversation];
    }
}


-(void) updateLastMessageTimestampForConversation:(Conversation *) theConversation withLastMessage:(Message *) lastMessage {
    
    if (theConversation) {
        if ([theConversation.lastMessage.messageID isEqualToString:lastMessage.messageID]) {
            theConversation.lastMessage.timestamp = lastMessage.timestamp;
        }
        [_conversationCache setObjectAsync:[theConversation dictionaryRepresentation] forKey:theConversation.conversationId completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateConversation object:theConversation];
    }
}

-(void) startConversationWithPeer:(Peer *) peer withCompletionHandler:(ConversationsManagerConversationStartedComplionHandler) completionHandler {
    [self startConversationWithPeer:peer notifyStartConversation:YES withCompletionHandler:completionHandler];
}

-(void) startConversationWithPeer:(Peer *) peer notifyStartConversation:(BOOL) notify withCompletionHandler:(ConversationsManagerConversationStartedComplionHandler) completionHandler {
    
    void(^actionCompletion)(Conversation *theConversation, BOOL setDate) = ^(Conversation *theConversation, BOOL setDate) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateConversation object:theConversation];
        if(notify)
            [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidStartConversation object:theConversation];
    };
    
    Conversation *theConversation = [self getConversationWithPeer:peer];
    if(theConversation) {
        actionCompletion(theConversation, NO);
        if(completionHandler)
            completionHandler(theConversation, nil);
    } else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSError *error = nil;
            Conversation *aConversation = [self internalStartConversationWithPeer:peer error:&error];
            if(!error && aConversation){
                actionCompletion(aConversation, YES);
                if(completionHandler)
                    completionHandler(aConversation, nil);
            } else {
                NSLog(@"Error while creating conversation on server side %@", error);
                if(completionHandler)
                    completionHandler(nil, error);
            }
        });
    }
}

-(Conversation *) internalStartConversationWithPeer:(Peer *) peer error:(NSError **) error {
    NSAssert(![NSThread isMainThread], @"internalStartConversationWithPeer must not be invoked on main thread");
    NSLog(@"Creating a new conversation on server with %@", peer);
    // Create a conversation in REST
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConversations];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConversations];
    
    NSString *type;
    if ([peer isKindOfClass:[Contact class]]) { 
        type = @"user";
        if(((Contact*)peer).isBot)
            type = @"bot";
    } else if ([peer isKindOfClass:[Room class]]) {
        type = @"room";
    }
    
    if(!type) {
        NSLog(@"Type of conversation is not set because we don't determine the kind of peer %@", peer);
        *error = [NSError errorWithDomain:@"ConversationsManager" code:500 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Could not determine conversation type for peer %@", peer]}];
        return nil;
    }
        
    
    NSAssert(peer.rainbowID, @"Unable to start conversation with peer which have no rainbowID");
    if (!peer.rainbowID) {
        NSLog(@"Unable to start conversation with peer which have no rainbowID %@", peer);
        *error = [NSError errorWithDomain:@"ConversationsManager" code:500 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Could not create conversation without rainbowID, peer: %@", peer]}];
        return nil;
    }
    
    NSDictionary *bodyContent = @{@"peerId":peer.rainbowID, @"type":type};
    NSData *receivedData = nil;
    NSError *requestError = nil;
    NSURLResponse *response = nil;
    
    BOOL requestSucceed = [_downloadManager postSynchronousRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
    NSLog(@"Answer : Creating a new conversation on server with %@  error: %@  data ...", peer, requestError);
    Conversation *theConversation = nil;
    if(requestSucceed && !requestError) {
        NSDictionary *response = [receivedData objectFromJSONData];
        if(response) {
            if([response objectForKey:@"errorCode"]){
                *error = [NSError errorWithDomain:@"ConversationsManager" code:500 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Server error : %@", response]}];
                NSLog(@"Create conversation : error : %@", response);
                return nil;
            }
            
            NSMutableDictionary *data = [[response objectForKey:@"data"] mutableCopy];
            
            // Room with this topic is for internal use only - don't create or update a conversation for this
            if([[data objectForKey:@"topic"] isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"]){
                NSLog(@"Did not create conversation because his topic value equal Rainbow_OutlookCreation_InternalUseOnly");
                *error = [NSError errorWithDomain:@"ConversationsManager" code:500 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Don't create conversation with Rainbow_OutlookCreation_InternalUseOnly topic"]}];
                return nil;
            }
            
            [data setObject:peer.jid forKey:@"jid_im"];
            theConversation = [self createOrUpdateConversationFromDictionary:data];
        }
    } else {
        NSLog(@"Request Failed : Creating a new conversation on server with %@", peer);
        
        // check if the newtwork conncection appear to be offline
        // TODO: be sure that this is the right code error for offline network
        if (requestError.code == NSURLErrorNotConnectedToInternet) {
            // create fake conversation
            NSArray* nameArray = [peer.displayName componentsSeparatedByString:@" "];
            NSMutableDictionary * data = [[NSMutableDictionary alloc]init];
            [data setObject:[NSString stringWithFormat:@"fake_%@",peer.jid] forKey:@"id"];
            [data setObject:peer.rainbowID forKey:@"peerId"];
            [data setObject:peer.jid forKey:@"jid_im"];
            [data setObject:type forKey:@"type"];
            if(!peer.displayName)
                peer.displayName = @"";
            [data setObject:peer.displayName forKey:@"displayName"];
            [data setObject:[nameArray objectAtIndex:0] forKey:@"firstName"];
            if (nameArray.count > 1) {
                 [data setObject:[nameArray objectAtIndex:1] forKey:@"lastName"];
            }
            theConversation = [self createOrUpdateConversationFromDictionary:data];
        }
        else{
            if(requestError){
                *error = [NSError errorWithDomain:requestError.domain code:requestError.code userInfo:requestError.userInfo];
            } else
                *error = [NSError errorWithDomain:@"ConversationsManager" code:500 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Failed to create new conversation on server with %@", peer]}];
        }

    }
    return theConversation;
}

-(void) stopConversation:(Conversation *) conversation {
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesConversations];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",serviceUrl, conversation.conversationId]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConversations];
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidStopConversation object:conversation];
        [self removeConversation:conversation];
       
    }];
}

#pragma mark - File sharing
-(void) downloadAttachmentForMessage:(Message *) message completionHandler:(ConversationsManagerAttachmentDownloadedComplionHandler) completionHandler {
    [_fileSharingService downloadDataForFile:message.attachment withCompletionHandler:^(File *file, NSError *error) {
        if(!error){
            [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:message];
        } else {
            NSLog(@"Could not download attachment, error %@", error);
        }
        if(completionHandler)
            completionHandler(message, error);
    }];
}

#pragma mark - Send message

-(Message *) addFailedMessage:(NSString *) message forFileAttachment:(File *) file to:(Conversation *) conversation completionHandler:(ConversationsManagerServiceSendMessageCompletionHandler) completionHandler attachmentUploadProgressHandler:(ConversationsManagerAttachmentProgressionHandler) progressHandler {

    NSString *theMessage = [message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    MessageType type = MessageTypeChat;
    if (conversation.type == ConversationTypeRoom) {
        type = MessageTypeGroupChat;
    }
    NSString *mimeType = [file.data mimeTypeByGuessing];
    if(!file.fileName){
        NSString *fileName = [NSString stringWithFormat:@"IMG_%@.%@", [NSDate date], [file.data extension]];
        file.fileName = fileName;
    }
    NSData *data = file.data;
    if(theMessage.length == 0)
        theMessage = file.fileName;
    
    NSLog(@"attempt to show failed file %@",file.fileName);
    
    XMPPMessage *xmppMsg = [_xmppService createXMPPMessage:theMessage ofType:type withOBData:nil desc:nil withId:file.tag to:conversation.peer withCompletionHandler:completionHandler];
    
    // We create a fake File object to display it imediatelly in the UI
    File *attachment = [_fileSharingService createFileWithFileName:file.fileName size:[data length] mimeType:mimeType data:data url:file.url];
    
    Message *aMessage = [_xmppService sendLocalyXMPPMessage:xmppMsg];
    aMessage.messageID = file.tag;
    attachment.tag = aMessage.messageID;
    attachment.thumbnailData = attachment.data;
    aMessage.attachment = attachment;
    aMessage.attachment.isOfflineAttachment = YES;
    [aMessage setDeliveryState:MessageDeliveryStateFailed atTimestamp:[NSDate date]];
    [self updateLastMessageForConversation:conversation withLastMessage:aMessage];
    // we simulate the update message because we add the attachment
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:aMessage];
    return aMessage;
    
}

-(Message *) sendMessage:(NSString *) message fileAttachment:(File *) file to:(Conversation *) conversation completionHandler:(ConversationsManagerServiceSendMessageCompletionHandler) completionHandler attachmentUploadProgressHandler:(ConversationsManagerAttachmentProgressionHandler) progressHandler {
    NSString *theMessage = [message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    MessageType type = MessageTypeChat;
    if (conversation.type == ConversationTypeRoom) {
        type = MessageTypeGroupChat;
    }
    if(!file){
        Message *aMessage = [_xmppService sendMessage:theMessage ofType:type to:conversation.peer withCompletionHandler:^(Message *message, NSError *error) {
            if (!error) {
                // Update the lastMessage of the conversation
                [self updateLastMessageForConversation:conversation withLastMessage:message];
                [_xmppService removeMessageWithMessageIDfromCache:message.messageID];
            } else {
                [message setDeliveryState:MessageDeliveryStateFailed atTimestamp:[NSDate date]];
                [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:message];
            }
            if(completionHandler){
                completionHandler(message, error);
            }
        }];
        return aMessage;
    } else {
        NSString *mimeType = (file.mimeType)?file.mimeType:[file.data mimeTypeByGuessing];
        if(!file.fileName){
            NSString *fileName = [NSString stringWithFormat:@"IMG_%@.%@", [NSDate date], [file.data extension]];
            file.fileName = fileName;
        }
        NSData *data = file.data;
        if(theMessage.length == 0)
            theMessage = file.fileName;
        
        NSLog(@"attempt to send file %@",file.fileName);
        
        XMPPMessage *xmppMsg = [_xmppService createXMPPMessage:theMessage ofType:type withOBData:nil desc:nil withId:nil to:conversation.peer withCompletionHandler:completionHandler];
        // We create a fake File object to display it imediatelly in the UI
        File *attachment = [_fileSharingService createFileWithFileName:file.fileName size:([data length])?[data length]:file.size mimeType:mimeType data:data url:file.url];
        
        Message *aMessage = [_xmppService sendLocalyXMPPMessage:xmppMsg];
        attachment.tag = aMessage.messageID;
        attachment.thumbnailData = attachment.data;
        aMessage.attachment = attachment;
        [aMessage setDeliveryState:MessageDeliveryStateSent atTimestamp:[NSDate date]];
        [self updateLastMessageForConversation:conversation withLastMessage:aMessage];
        
        // we simulate the update message because we add the attachment
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:aMessage];
        
        [[NSFileManager defaultManager] removeItemAtURL:file.url error:nil];
        
        if(file.rainbowID){
            __weak typeof(_xmppService) weakXMPPService = _xmppService;
            __weak typeof(self) weakSelf = self;
            
            [_fileSharingService addViewer:conversation.peer forFile:file withCompletionHandler:^(File *file, NSError *error) {
                 NSLog(@"The operation is done, send the message");
                [xmppMsg addOutOfBandURI:[file.url absoluteString] fileName:file.fileName mimeType:file.mimeType size:([data length])?[data length]:file.size ownerId:[ServicesManager sharedInstance].myUser.contact.rainbowID];
              
                [weakXMPPService sendXMPPMessage:xmppMsg];
                NSLog(@"Send message with attachment uploaded: url=%@ mimeType=%@",[file.url absoluteString], mimeType);
                [weakSelf updateLastMessageForConversation:conversation withLastMessage:aMessage];
                [weakXMPPService removeXMPPMessagefromCache:xmppMsg];
                
                if(completionHandler)
                    completionHandler(aMessage, error);
            }];
        } else {
            [_fileSharingService uploadFile:attachment forConversation:conversation completionHandler:^(File *file, NSError *error) {
                if(error) {
                    NSLog(@"Failed to send message with attachment uploaded: url=%@ with error %@",[file.url absoluteString], error);
                    [aMessage setDeliveryState:MessageDeliveryStateFailed atTimestamp:[NSDate date]];
                    aMessage.attachment.isOfflineAttachment = YES;
                    [_xmppService removeXMPPMessagefromCache:xmppMsg];
                    [_failedFilesDictionary setObject:[aMessage dictionaryRepresentation] forKey:aMessage.attachment.fileName];
                    [_failedFilesCache setObject:_failedFilesDictionary  forKey:conversation.conversationId];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:aMessage];
                    
                } else {
                    [xmppMsg addOutOfBandURI:[file.url absoluteString] fileName:file.fileName mimeType:file.mimeType size:([data length])?[data length]:file.size ownerId:[ServicesManager sharedInstance].myUser.contact.rainbowID];
                   
                    [_xmppService sendXMPPMessage:xmppMsg];
                    [self updateLastMessageForConversation:conversation withLastMessage:aMessage];
                    NSLog(@"Send message with attachment uploaded: url=%@ mimeType=%@",[file.url absoluteString], mimeType);
                    [_xmppService removeXMPPMessagefromCache:xmppMsg];
                }
                
                if(completionHandler)
                    completionHandler(aMessage, error);
            }];
        }
        return aMessage;
    }
}

-(void) deleteFailedMessage:(Message *) message to:(Conversation *) conversation {
    NSLog(@"remove message ID %@ from failedFilesDictionary",message.messageID);
    [_failedFilesDictionary removeObjectForKey:message.attachment.fileName];
    [_failedFilesCache setObject:_failedFilesDictionary forKey:conversation.conversationId];
}

-(Message *) reSendMessage:(Message *) message to:(Conversation *) conversation completionHandler:(ConversationsManagerServiceSendMessageCompletionHandler) completionHandler attachmentUploadProgressHandler:(ConversationsManagerAttachmentProgressionHandler) progressHandler {
    NSString *theMessage = [message.body stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    MessageType type = MessageTypeChat;
    if (conversation.type == ConversationTypeRoom) {
        type = MessageTypeGroupChat;
    }
    NSString *mimeType = [message.attachment.data mimeTypeByGuessing];
    if(!message.attachment.fileName){
        NSString *fileName = [NSString stringWithFormat:@"IMG_%@.%@", [NSDate date], [message.attachment.data extension]];
        message.attachment.fileName = fileName;
    }
    NSData *data = message.attachment.data;
    if(theMessage.length == 0)
        theMessage = message.attachment.fileName;
    
    
    XMPPMessage *xmppMsg = [_xmppService createXMPPMessage:theMessage ofType:type withOBData:nil desc:nil withId:message.messageID to:conversation.peer withCompletionHandler:completionHandler];
    Message *aMessage = [_xmppService messageFromXmppMessage:xmppMsg];
    // Fake the sent state
    [aMessage resetDeliveryStateAndDate];
    [aMessage setDeliveryState:MessageDeliveryStateSent atTimestamp:aMessage.date];
    
    // We create a fake File object to display it imediatelly in the UI
    File *attachment = [_fileSharingService createFileWithFileName:message.attachment.fileName size:[data length] mimeType:mimeType data:data url:message.attachment.url];
    attachment.tag = aMessage.messageID;
    aMessage.attachment = attachment;
    
    [_xmppService sendLocalyXMPPMessage:xmppMsg];
  //  [self updateLastMessageForConversation:conversation withLastMessage:aMessage];
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:aMessage];
   
    [_fileSharingService uploadFile:attachment forConversation:conversation completionHandler:^(File *file, NSError *error) {
        if(error) {
            [aMessage setDeliveryState:MessageDeliveryStateFailed atTimestamp:[NSDate date]];
            aMessage.attachment.isOfflineAttachment = YES;
            [_xmppService removeXMPPMessagefromCache:xmppMsg];
            [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:aMessage];
            
        } else {
           
            [xmppMsg addOutOfBandURI:[file.url absoluteString] fileName:file.fileName mimeType:file.mimeType size:[data length] ownerId:[ServicesManager sharedInstance].myUser.contact.rainbowID];
            [_xmppService sendXMPPMessage:xmppMsg];
            [self updateLastMessageForConversation:conversation withLastMessage:aMessage];
            NSLog(@"remove message ID %@ from failedFilesDictionary",attachment.fileName);
            [_failedFilesDictionary removeObjectForKey:attachment.fileName];
            [_failedFilesCache setObject:_failedFilesDictionary forKey:conversation.conversationId];

            NSLog(@"Send message with attachment uploaded: url=%@ mimeType=%@",[file.url absoluteString], mimeType);
        }
        
        if(completionHandler)
            completionHandler(aMessage, error);
    }];
    
    return aMessage;
}

-(Conversation *) getConversationForContact:(Contact *) contact {
    __block Conversation *theConversation = nil;
    @synchronized (_conversationsMutex) {
        [_conversations enumerateObjectsUsingBlock:^(Conversation * aConversation, NSUInteger idx, BOOL * stop) {
            if(aConversation.type == ConversationTypeUser && aConversation.peer == contact) {
                theConversation = aConversation;
                *stop = YES;
            }
        }];
    }
    
    return theConversation;
}

-(Conversation *) getConversationWithPeer:(Peer *) peer {
    __block Conversation *theConversation = nil;
    @synchronized (_conversationsMutex) {
        [_conversations enumerateObjectsUsingBlock:^(Conversation * aConversation, NSUInteger idx, BOOL * stop) {
            if(aConversation.peer == peer) {
                theConversation = aConversation;
                *stop = YES;
            }
        }];
    }
    
    return theConversation;
}

-(Conversation *) getConversationWithPeerJID:(NSString *) peerJID {
    __block Conversation *theConversation = nil;
    @synchronized (_conversationsMutex) {
        [_conversations enumerateObjectsUsingBlock:^(Conversation * aConversation, NSUInteger idx, BOOL * stop) {
            if([aConversation.peer.jid isEqualToString:peerJID] && ![aConversation.conversationId containsString:@"fake_"]) {
                theConversation = aConversation;
                *stop = YES;
            }
        }];
    }
    
    return theConversation;
}

-(Conversation *) getConversationWithID:(NSString *) conversationID {
    __block Conversation *theConversation = nil;
    @synchronized (_conversationsMutex) {
        [_conversations enumerateObjectsUsingBlock:^(Conversation * aConversation, NSUInteger idx, BOOL * stop) {
            if([aConversation.conversationId isEqualToString:conversationID]){
                theConversation = aConversation;
                *stop = YES;
            }
        }];
    }
    return theConversation;
}

-(NSInteger) totalNbOfUnreadMessagesInAllConversations {
    __block NSInteger total = 0;
    @synchronized (_conversationsMutex) {
        [_conversations enumerateObjectsUsingBlock:^(Conversation * conversation, NSUInteger idx, BOOL * stop) {
                total+=conversation.unreadMessagesCount;
        }];
    }
    return total;
}

#pragma mark - XEP-85 Chat state

-(void) setStatus:(ConversationStatus) status forConversation:(Conversation *) conversation; {

    if (conversation.status != status) {
        conversation.status = status;
        NSLog(@"Set new status %@ for conversation with %@", [conversation stringForConversationStatus], conversation.peer.jid);
        [_xmppService sendChatStateMessageTo:conversation.peer status:conversation.status];
    }
}

#pragma mark - XEP-0184 Mark as read and send read ACKs methods

/**
 *  This adds a messageID to our set of unread by ME message.
 *  This is called in 2 cases :
 *  - When we receive a normal chat <message> stanza from somebody else.
 *  - When we receive a carbon copy of a message, sent by somebody else. (not sent by ME)
 *
 *  @param peer   The remote peer
 *  @param message The message
 */
-(void) addMessageUnReadByMeFromPeer:(Peer *) peer message:(Message *) message {
     // messageID appear to be null sometimes
    if (!message.messageID)
        return;
    
    Conversation *theConversation = [self getConversationWithPeer:peer];
    if(!theConversation){
        NSLog(@"We try to increase unread count for an unknown conversation with %@, we don't treat it", peer);
        return;
    }
    
    NSLog(@"addMessageUnReadByMeFromContact %@ %@", peer.jid, message.messageID);
    // Unread message count must be incremented only for message not received in push, because the synchronisation done at login will provide us the good counter and we don't want to change it manually with messages already counted.
    @synchronized (theConversation) {
        [theConversation.unReadMessageIDs addObject:message.messageID];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessagesUnreadCount object:theConversation];
}

/**
 *  This removes a messageID from our set of un-read message by ME.
 *  This is called in 2 cases :
 *  - After we sent the ACK on XMPP, because we opened the conversation (read on this device)
 *  - When we receive a carbon read-by-me ACK (read on another of my devices)
 *
 *  @param contact   The remote contact
 *  @param messageID the message ID
 */
-(void) markAsReadByMeMessageFromPeer:(Peer *) peer messageID:(NSString*) messageID {
    // messageID appear to be null sometimes
    if (!messageID)
        return;
    
    Conversation *theConversation = [self getConversationWithPeer:peer];
    if(!theConversation){
        NSLog(@"We try to mark as read-by-me a message for an unknown conversation with %@, we don't treat it", peer);
        return;
    }
    
    NSLog(@"markAsReadByMeMessageFromContact %@ %@", peer.jid, messageID);
    if (theConversation.unreadMessagesCount > 0)
        theConversation.unreadMessagesCount--;
    @synchronized (theConversation) {
        [theConversation.unReadMessageIDs removeObject:messageID];
    }
    [_conversationCache setObjectAsync:[theConversation dictionaryRepresentation] forKey:theConversation.conversationId completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessagesUnreadCount object:theConversation];
}

/**
 *  This sends a read-ACK message for ALL the un-read messages to a remote contact, to inform him
 *  we have opened and read ALL the messages.
 *
 *  @param contact   The remote contact
 */
-(void) sendReadByMeACKForAllMessagesTo:(Conversation *) conversation {
    @synchronized (conversation) {
        
        for(NSString *messageID in conversation.unReadMessageIDs) {
            NSLog(@"sendReadByMeACKForAllMessagesTo %@ for message ID %@", conversation.peer.jid, messageID);
            [_xmppService sendReadByMeACKMessageTo:conversation.peer.jid messageID:messageID];
        }
        
        conversation.unreadMessagesCount = 0;
        [conversation.unReadMessageIDs removeAllObjects];
    }
    [_conversationCache setObjectAsync:[conversation dictionaryRepresentation] forKey:conversation.conversationId completion:nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessagesUnreadCount object:conversation];
    });
}

/**
 *  This is a public method of the SDK.
 *  Mark all the un-read by ME messages as read and send
 *  the ACK messages.
 *
 *  @param conversation The conversation to mark as read.
 */
-(void) markAsReadByMeAllMessageForConversation:(Conversation *) conversation {
    // For a conversation, mark messages as read means
    // send an ACK for all the unread messages and decrease the unread counter
    [self sendReadByMeACKForAllMessagesTo:conversation];
}

#pragma mark - Send read ACKs methods in REST

/**
 *  This sends a read-ACK message for ALL the un-read messages to a remote contact in REST, to inform him
 *  we have opened and read ALL the messages.
 *
 *  @param conversation the conversation to mark as read
 */
-(void) sendMarkAllMessagesAsReadFromConversation:(Conversation *) conversation {
    @synchronized (conversation) {
        //Request to mark all messages as read at one request
        if (conversation.unreadMessagesCount > 0) {
            NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesMarkAllMessagesAsReadFroConversation, conversation.conversationId];
            NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesMarkAllMessagesAsReadFroConversation];
            [_downloadManager putRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *urlResponse) {
                NSDictionary *response = [receivedData objectFromJSONData];
                if(!response) {
                    NSLog(@"Mark all messages as read JSON parsing failed");
                }
                NSString *status = [response objectForKey:@"status"];
                NSLog(@"Mark all messages as read status: %@", status);
            }];
        }
        
        conversation.unreadMessagesCount = 0;
    }
}

#pragma mark - message browsing

-(MessagesBrowser *) messagesBrowserForConversation:(Conversation *) conversation withPageSize:(NSInteger) pageSize preloadMessages:(BOOL) preload {
    MessagesBrowser *browser = [[MessagesBrowser alloc] initWithConversation:conversation withXMPPService:_xmppService withPageSize:pageSize];
    if(preload){
        [browser getArchivedMessages];
    }
     [self performSelector:@selector(loadFailedFilesFromCacheforConversation:) withObject:conversation afterDelay:1.0];
    return browser;
}

#pragma mark - Delete messages

-(void) deleteAllMessagesForConversation:(Conversation *) conversation {
    [_xmppService deleteAllMessagesFromConversation:conversation];
}

-(void) deleteMessage:(Message *) message inConversation:(Conversation *) conversation {
    if (message.attachment) {
        [_fileSharingService deleteFile:message.attachment withCompletionHandler:^(File *file, NSError *error) {
            if (file && !error) {
                 [[NSNotificationCenter defaultCenter] postNotificationName:kFileSharingServiceDidRemoveFile object:file];
                if ([conversation.lastMessage isEqual:message]) {
                    NSLog(@"this message %@ is deleted,so should update it",message);
                   Message *theMessage = [_xmppService getLastArchivedMessageWithContactJid:conversation.peer.jid];                    if (theMessage) {
                       
                        [self updateLastMessageForConversation:conversation withLastMessage:theMessage];
                    }
                }
            }
            else{
                 [[NSNotificationCenter defaultCenter] postNotificationName:kFileSharingServiceDidRemoveFile object:nil];
            }
        }];
    }
}

#pragma mark - RoomsService notifications
-(void) didJoinRoom:(NSNotification *) notification {
}

-(void) didRemoveRoom:(NSNotification *) notification {
    Room *room = (Room *) notification.object;
    Conversation *theConversation = [self getConversationWithPeer:room];
    if(theConversation){
        [self stopConversation:theConversation];
    }
}

-(void) didRoomInvitationStatusChanged:(NSNotification *) notification {
    Room *room = (Room *) notification.object;
    if(room.myStatusInRoom == ParticipantStatusAccepted){
        if(![self getConversationWithPeer:room]){
            // didRoomInvitationStatusChanged notification is sent on mainThread and internalStartConversationWithPeer use a synchronous post request so the mainThread is lock during this time and that is not good !!
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSError *error = nil;
                [self internalStartConversationWithPeer:room error:&error];
            });
        }
    }
}

#pragma mark - Mute/Unmute conversation

-(void) muteConversation:(Conversation *) conversation {
    if(conversation.isMuted){
        NSLog(@"Cannot mute a conversation already muted");
        return;
    }
    [self updateConversation:conversation withMuteState:YES];
}

-(void) unmuteConversation:(Conversation *) conversation {
    if(!conversation.isMuted){
        NSLog(@"Cannot unmute a conversation already unmuted");
        return;
    }
    [self updateConversation:conversation withMuteState:NO];
}


-(void) updateConversation:(Conversation *) conversation withMuteState:(BOOL) muteState {
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesConversations];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",serviceUrl, conversation.conversationId]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConversations];
    
    NSDictionary *bodyContent = @{@"mute":[NSNumber numberWithBool:muteState]};
    [_downloadManager putRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO]  requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"Error: Cannot update the conversation in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(!jsonResponse && error.code != NSURLErrorNotConnectedToInternet) {
                NSLog(@"Error: Cannot update the conversation in REST due to JSON parsing failure");
            } else {
                NSDictionary *data = [jsonResponse objectForKey:@"data"];
                if(data){
                    NSLog(@"Did update conversation");
                    if([data objectForKey:@"mute"]){
                        Conversation *theConversation = [self getConversationWithID:data[@"id"]];
                        [self updateMuteStateForConversation:theConversation withNewMuteState:((NSNumber *)[data objectForKey:@"mute"]).boolValue];
                    }
                } else if(error.code == NSURLErrorNotConnectedToInternet){
                    [self updateMuteStateForConversation:conversation withNewMuteState:muteState];
                }
            }
        }

    }];
}

-(void) updateMuteStateForConversation:(Conversation *) conversation withNewMuteState:(BOOL) muteState {
    if(conversation.isMuted == muteState){
        NSLog(@"Conversation is already in the same mute state");
        return;
    }
    conversation.isMuted = muteState;
    [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateConversation object:conversation];
}

-(void) xmppService:(XMPPService *) service didReceiveMuteConversation:(NSString *) conversationID {
    Conversation *theConversation = [self getConversationWithID:conversationID];
    [self updateMuteStateForConversation:theConversation withNewMuteState:YES];
}

-(void) xmppService:(XMPPService *) service didReceiveUnMuteConversation:(NSString *) conversationID {
    Conversation *theConversation = [self getConversationWithID:conversationID];
    [self updateMuteStateForConversation:theConversation withNewMuteState:NO];
}

#pragma mark - Send conversation by email
-(BOOL) sendConversationByMail:(Conversation *) conversation {
    if([NSThread isMainThread]){
        NSAssert([NSThread isMainThread], @"Send conversation by mail must not be invoked on main thread");
        NSLog(@"Send conversation by mail must not be invoked on main thread");
        return NO;
    }
    
    if(!conversation.conversationId){
        NSLog(@"The conversation must have a conversationID");
        return NO;
    }
    
    if(conversation.type == ConversationTypeBot){
        NSLog(@"Send conversation by mail is not working with bots");
        return NO;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConversationDownload, conversation.conversationId];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConversationDownload];
    
    NSData *receivedData = nil;
    NSError *requestError = nil;
    NSURLResponse *response = nil;
    NSLog(@"Invoke send by email");
    BOOL requestSucceed = [_downloadManager postSynchronousRequestWithURL:url body:nil requestHeadersParameter:headers returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
    NSLog(@"Send by email, error ? %@", requestError);
    if(requestError && requestError.code == NSURLErrorNotConnectedToInternet){
        // we are not connected the action will be replay later, so we can consider it as done.
        return YES;
    }
    if(requestSucceed && !requestError){
        NSError *jsonError = nil;
        NSDictionary *response = [receivedData objectFromJSONData];
        if(!jsonError){
            NSLog(@"Send by email response %@", response);
            return YES;
        }
    }
    return NO;
}

-(NSArray<Conversation *> *) searchConversationWithPattern:(NSString *) pattern {
    NSString *trimmedPattern = [pattern stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSMutableArray *subPredicates = [NSMutableArray array];
    NSArray *terms = [trimmedPattern componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    for (NSString *term in terms) {
        if(term.length == 0)
            continue;
        
        NSString *theTerm = [term stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
        
        // verification failed because of that.
        NSString *goodTerm = [theTerm stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        // Avoid bad regex by escaping any special character, for example '+' becomes '//+' but 'A' does not change
        NSString *escapedPattern = [NSRegularExpression escapedPatternForString:goodTerm];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"peer != nil AND (peer isMemberOfClass:%@ and ((peer.firstName BEGINSWITH[cd] %@) OR (peer.lastName MATCHES[cd] %@)))", [Contact class], goodTerm, [NSString stringWithFormat:@"(?w:).*\\b%@.*", escapedPattern]];
        [subPredicates addObject:predicate];
    }
    
    NSMutableSet<Conversation *> *result = [NSMutableSet set];
    @synchronized (_conversationsMutex) {
        NSPredicate *filter = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
        [result addObjectsFromArray:[_conversations filteredArrayUsingPredicate:filter]];
    }
    
    return [result allObjects];
}

#pragma mark - RTC Call notifications
-(void) didAddCall:(NSNotification *) notification {
    Call *call = (Call *) notification.object;
    Conversation *theConversation = [self getConversationWithPeer:call.peer];
    if(theConversation){
        theConversation.hasActiveCall = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateConversation object:theConversation];
    }
}

-(void) didUpdateCall:(NSNotification *) notification {
    Call *call = (Call *) notification.object;
    Conversation *theConversation = [self getConversationWithPeer:call.peer];
    if(theConversation){
        theConversation.hasActiveCall = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateConversation object:theConversation];
    }
}

-(void) didRemoveCall:(NSNotification *) notification {
    Call *call = (Call *) notification.object;
    Conversation *theConversation = [self getConversationWithPeer:call.peer];
    if(theConversation){
        theConversation.hasActiveCall = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateConversation object:theConversation];
    }
}

-(void) handleContinueUserActivity:(NSUserActivity *)userActivity error:(NSError **)errorPointer waitingForResponse:(void (^_Nullable)(BOOL receivedResponse))waitingForResponseBlock restorationHandler:(void (^_Nullable)(NSArray * _Nullable))restorationHandler {
    if([userActivity.interaction.intent isKindOfClass:[INSendMessageIntent class]]){
        INInteraction *interaction = userActivity.interaction;
        INSendMessageIntent *startMessageIntent = (INSendMessageIntent *)interaction.intent;
        INPerson *contact = startMessageIntent.recipients[0];
        
        INPersonHandle *personHandle = contact.personHandle;
        NSString *emailString = personHandle.value;
        
        // Try to get the contact by jid
        Contact *rainbowContact = [_contactsManagerService getContactWithJid: emailString];
        
        // If no rainbow contact match, try a search by email
        if(!rainbowContact)
            rainbowContact = [_contactsManagerService searchRainbowContactWithEmailString: emailString];
        
        // No local contact found. Do a search on server
        if(!rainbowContact) {
            if(waitingForResponseBlock)
                waitingForResponseBlock(YES);
            
            NSArray *users = [_contactsManagerService.directoryService searchRainbowContactsWithLoginEmails:@[emailString] fromOffset:0 withLimit:10];
            if(users != nil && users.count > 0)
                rainbowContact = [_contactsManagerService createOrUpdateRainbowContactFromJSON: users[0]];
            
            if(waitingForResponseBlock)
                waitingForResponseBlock(NO);
        }
        
        // Finally no rainbow contact found
        if(!rainbowContact) {
            if (errorPointer)
                *errorPointer = [NSError errorWithDomain:@"UserActivity" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"User not found"}];
        }
        
        if(rainbowContact && rainbowContact.canChatWith) {
            [self startConversationWithPeer:(Peer*)rainbowContact withCompletionHandler:nil];
        } else {
            if (errorPointer){
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{NSLocalizedDescriptionKey:@"User not found"}];
                if(rainbowContact)
                    [dic setObject:rainbowContact forKey:@"object"];
                *errorPointer = [NSError errorWithDomain:@"UserActivity" code:-1 userInfo:dic];
            }
        }
    }
}

#pragma mark - Application notification
-(void) applicationDidBecomeActive:(NSNotification *) notification {
    BOOL shouldRefresh = [[RainbowUserDefaults sharedInstance] boolForKey:@"shouldRefreshConversationUI"];
    if(shouldRefresh){
        [_conversationCache.diskCache forceRefresh];
        [self loadConversationsFromCache];
        
    }
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:@"shouldRefreshConversationUI"];
    if ([ServicesManager sharedInstance].loginManager.loginDidSucceed && ![ServicesManager sharedInstance].loginManager.isConnected) {
        [self fetchConversations];
    }
}

-(void) forceRefreshCache {
    [_conversationCache.diskCache forceRefresh];
    [self loadConversationsFromCacheSynchrounsly];   
    
}
@end
