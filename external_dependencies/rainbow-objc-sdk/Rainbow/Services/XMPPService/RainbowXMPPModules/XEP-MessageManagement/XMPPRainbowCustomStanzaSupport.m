/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPRainbowCustomStanzaSupport.h"
#import "XMPP.h"
#import "XMPPMessage+RainbowCustomMessageTypes.h"
#import "ContactsManagerService+Internal.h"
#import "RoomsService+Internal.h"
#import "Participant+Internal.h"
#import "GroupsService+Internal.h"
#import "XMPPPresence+RainbowCustomType.h"
#import "CompaniesService+Internal.h"
#import "ConferencesManagerService+Internal.h"
#import "NSDate+JSONString.h"
#import "NSData+JSON.h"
#import "Tools.h"
#import "File+Internal.h"
#import "MyUser+Internal.h"
#import "ContactInternal.h"
#import "NSXMLElement+XEP_0203.h"

#if ! __has_feature(objc_arc)
#warning This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

@implementation XMPPRainbowCustomStanzaSupport

#pragma mark - Init/Dealloc
- (id)initWithDispatchQueue:(dispatch_queue_t)queue {
    if((self = [super initWithDispatchQueue:queue])) {
    }
    
    return self;
}

#pragma mark - XMPPModule
- (BOOL)activate:(XMPPStream *)aXmppStream {
    if ([super activate:aXmppStream]) {
        return YES;
    }
    
    return NO;
}

- (void)deactivate {
    [super deactivate];
}

#pragma mark - Message with type configuration NS
- (void) treatManagementXMPPMessage:(XMPPMessage *) message {
    
#pragma mark - Conversation deleted event
    // Deal with management message (from admin) for deleting conversation
    if([message isManagementMessage]){
        // This is a management message.
        NSXMLElement *conversationElement = [message elementForName:@"conversation" xmlns:kXMPPJabberConfigurationNS];
        if(conversationElement){
            //NSString *action = [conversationElement attributeForName:@"action"].stringValue;
            NSString *conversationID = [conversationElement attributeForName:@"id"].stringValue;
            [_conversationDelegate xmppService:_xmppService didReceiveDeleteConversationID:conversationID];
            return;
        }
    }
    
    
#pragma mark - Thumbnail  Event
    /**
     * Create a Thumbnail event
     <message xmlns="jabber:client" from="pcloud_previewworker_5@jerome-all-in-one-dev-1.opentouch.cloud/1597780096528435018851072" to="12a788a4e5894505bece59c225052002@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="fb989d88-47a9-4277-afd1-1b068b552b41_14"><thumbnail xmlns="jabber:iq:configuration" action="create"><url>https://openrainbow.org/api/rainbow/fileserver/v1.0/files/5a69a834775f45f4fcd7c801?thumbnail=true</url><mime>application/octet-stream</mime><filename>IMG_0005.JPG</filename><size>15629</size><md5sum>94c97b675a7a9fb30bf6bb632ea1a737</md5sum><fileid>5a69a834775f45f4fcd7c801</fileid></thumbnail></message>
     **/
    
    if([message isManagementMessage]){
        NSXMLElement *thumbnailElement = [message elementForName:@"thumbnail" xmlns:kXMPPJabberConfigurationNS];
        if(thumbnailElement){
           // NSString *url = [thumbnailElement elementForName:@"url"].stringValue;
            NSString *fileId = [thumbnailElement elementForName:@"fileid"].stringValue;
           // NSString *size = [thumbnailElement elementForName:@"size"].stringValue;
           // NSString *filename = [thumbnailElement elementForName:@"filename"].stringValue;
            
            File *afile = [_fileService getFileByRainbowID:fileId];
            
            if (afile) {
                afile.canDownloadThumbnail = YES;
                //download thumbnail
                if (afile.isDownloadAvailable) {
                    [_fileService fetchDataForFile:afile sizeLimit:100000 completionHandler:^(File *file, NSError *error) {
                        if(!error){
                            afile.thumbnailData = file.thumbnailData;
                            NSDictionary *jsonDic = @{@"thumbnailData":file.thumbnailData};
                            [_fileService updateFile:file withJsonDic:jsonDic];
                            NSLog(@"[XMPPRainbowCustomStanzaSupport] Success fetching data for message %@", message);
                            
                        } else {
                        
                            NSLog(@"[XMPPRainbowCustomStanzaSupport] Error fetching data for message %@", message);
                        }
                    }];
                }
               
            }
          
        }
    }
    
    
#pragma mark - File Events
    /**
     * Create a File event
     <message xmlns="jabber:client" from="pcloud_filestorage_4@jerome-all-in-one-dev-1.opentouch.cloud/4001759734787197297236" to="c2673e1952ef40178b68cb4ae2c6c2ed@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="3338fb5e-e5c8-4fc2-bcb3-a49934416ab7_3"><file xmlns="jabber:iq:configuration" action="create"><fileid>5a6b34987ab03f3155c4bb02</fileid></file></message>
     
     * Update a File event
     <message xmlns="jabber:client" from="pcloud_filestorage_2@jerome-all-in-one-dev-1.opentouch.cloud/16876922984549581137378" to="c2673e1952ef40178b68cb4ae2c6c2ed@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="3338fb5e-e5c8-4fc2-bcb3-a49934416ab7_1"><file xmlns="jabber:iq:configuration" action="update"><fileid>5a6b34317ab03f3155c4bb01</fileid></file></message>
     
     * Delete a File event
     <message xmlns="jabber:client" from="pcloud_filestorage_3@jerome-all-in-one-dev-1.opentouch.cloud/6597419619376709558268" to="c2673e1952ef40178b68cb4ae2c6c2ed@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="3338fb5e-e5c8-4fc2-bcb3-a49934416ab7_2"><file xmlns="jabber:iq:configuration" action="delete"><fileid>59edd88d56e620cfe628e3db</fileid></file></message>
     
     **/
    if([message isManagementMessage]){
        NSXMLElement *fileElement = [message elementForName:@"file" xmlns:kXMPPJabberConfigurationNS];
        if(fileElement){
            NSString *action = [fileElement attributeForName:@"action"].stringValue;
            NSString *fileId = [fileElement elementForName:@"fileid"].stringValue;
            
            File *aFile = [_fileService getFileByRainbowID:fileId];
            
            if(!aFile){
                aFile = [_fileService getOrCreateFileWithRainbowID:fileId withInfos:nil];
            }
            
            if ([action isEqualToString:@"create"] || [action isEqualToString:@"update"]) {
                //request details for this file
                [_fileService fetchFileInformation:aFile completionHandler:^(File *file, NSError *error) {
                    NSLog(@"[%@] end fetching file info for file %@",[self class], file);
                }];

            }
          
            else  if ([action isEqualToString:@"delete"]) {
                // mark the file as isDownloadAvailable to NO
                
                NSDictionary *jsonDic = @{@"isDownloadAvailable":@"NO"};
                [_fileService updateFile:aFile withJsonDic:jsonDic];
                [[NSNotificationCenter defaultCenter] postNotificationName:kFileSharingServiceDidRemoveFile object:aFile];
                // get jid from owner or viewers!
                [_xmppMessageArchiveManagementStorage updateFileWithURL:aFile.url.absoluteString withFileStatus:NO];

            }
        }
    }
    
#pragma mark - Room Events
    /**
     * Create a room event
     <message xmlns="jabber:client" from="pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/77200815533808837211473065423766542" to="c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="d7c6c2f3-4c16-42b2-853f-73359dda8e68_175"><room xmlns="jabber:iq:configuration" roomid="57cd34b18da16718a4c3ae25" roomjid="room_13e40808931e442ea2b18f7c33b1ac54@muc.jerome-all-in-one-dev-1.opentouch.cloud" userjid="c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud" status="accepted"/></message>
     
     * Add participant
     <message xmlns="jabber:client" from="pcloud4@jerome-all-in-one-dev-1.opentouch.cloud/152301706776434368761473085877757626" to="c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="607a75b9-fa19-45eb-a4e9-706ab54ef3f6_226"><room xmlns="jabber:iq:configuration" roomid="57cd876375de90b67e2f3e0c" roomjid="room_49bd0b4ad24e4ae3a7663566bcc12910@muc.jerome-all-in-one-dev-1.opentouch.cloud" userjid="0emilyf2b4cb24d4c93a0d8b1bae86dd@jerome-all-in-one-dev-1.opentouch.cloud" status="invited"/></message>
     
     * Delete a participant
     <message xmlns="jabber:client" from="pcloud5@jerome-all-in-one-dev-1.opentouch.cloud/32553569824586666671473066011676852" to="c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="d7c6c2f3-4c16-42b2-853f-73359dda8e68_426"><room xmlns="jabber:iq:configuration" roomid="57cd39588da16718a4c3ae2d" roomjid="room_37e9fc6e1d3347fa99465775d4ffaace@muc.jerome-all-in-one-dev-1.opentouch.cloud" userjid="9ffcf8eb59ba4c5f9da9e1606cc9b2d6@jerome-all-in-one-dev-1.opentouch.cloud" status="deleted"/></message>
     
     * Participant accept invitation
     <message xmlns="jabber:client" from="pcloud1@jerome-all-in-one-dev-1.opentouch.cloud/116576736161587204281473085877741119" to="c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="607a75b9-fa19-45eb-a4e9-706ab54ef3f6_43"><room xmlns="jabber:iq:configuration" roomid="57cd824875de90b67e2f3e06" roomjid="room_0bf633c01eed4499a3c141fa9946273f@muc.jerome-all-in-one-dev-1.opentouch.cloud" userjid="9ffcf8eb59ba4c5f9da9e1606cc9b2d6@jerome-all-in-one-dev-1.opentouch.cloud" status="accepted"/></message>
     
     * Participant reject invitation
     <message xmlns="jabber:client" from="pcloud7@jerome-all-in-one-dev-1.opentouch.cloud/161365117216749249611473085877719512" to="c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="607a75b9-fa19-45eb-a4e9-706ab54ef3f6_109"><room xmlns="jabber:iq:configuration" roomid="57cd835875de90b67e2f3e0a" roomjid="room_e0a4da25384f481eba9d529216d82571@muc.jerome-all-in-one-dev-1.opentouch.cloud" userjid="9ffcf8eb59ba4c5f9da9e1606cc9b2d6@jerome-all-in-one-dev-1.opentouch.cloud" status="rejected"/></message>
     
     * Change topic
     <message xmlns="jabber:client" from="pcloud10@openrainbow.net/149603920580980919851473424085565802" to="j_5169769963@openrainbow.net" type="management" id="949cfefc-02dd-4a64-be61-7cc8319460f3_409"><room xmlns="jabber:iq:configuration" roomid="57c06745293d40712179b5b1" roomjid="room_fd7760d16e3a40cabd29ec3d9958d1b2@muc.openrainbow.net" topic="salons de test"/></message>
     
     * Change name
     <message xmlns="jabber:client" from="pcloud_enduser_1@openrainbow.com/9541282100335198819948072" to="cb65455cb5d84ce38d6a9d52898157d7@openrainbow.com" type="management" id="76ffe35b-59f0-4939-8bda-14f075be927c_49002"><room xmlns="jabber:iq:configuration" roomid="58a1c6e76a6ef405493151ca" roomjid="room_ac6c7cf490ff40d38620e5147b335e79@muc.openrainbow.com" name="Bubble !"/></message>
     
     * Avatar update
     <message xmlns="jabber:client" from="pcloud_enduser_8@jerome-all-in-one-dev-1.opentouch.cloud/1424204946368052455431100" to="a62332478ded40e184076392dd0fbdb9@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="7456e9ec-89bf-4bbb-8e0a-81731f48693a_209"><room xmlns="jabber:iq:configuration" roomid="58f77f97c06ec037862a6cb6" roomjid="room_79c4a5c37fef4edcbd9278b8cfb0746d@muc.jerome-all-in-one-dev-1.opentouch.cloud" lastAvatarUpdateDate="2017-09-05T12:08:47.694Z"/>
        <avatar action="update | delete"/>
     </message>
     
     * Conference update
     <message xmlns="jabber:client" from="pcloud_enduser_10@demo-all-in-one-dev-1.opentouch.cloud/145457761695291481901094063" to="9f46475b676c449eadb6268747f7da77@demo-all-in-one-dev-1.opentouch.cloud" type="management" id="77027865-85c1-4ef2-aab0-633979266ed5_157"><room xmlns="jabber:iq:configuration" roomid="59e60f8cc10f02905389424d" roomjid="room_e8ffc4f0666e4a05bec71822d7772da9@muc.demo-all-in-one-dev-1.opentouch.cloud" scheduledStartDate="2017-10-17T14:20:00.000Z" scheduledEndDate="2017-10-17T15:50:00.000Z" scheduledTimezone="Europe/Paris"/></message>
     
     * CustomData update
     <message xmlns="jabber:client" from="pcloud_enduser_6@jerome-all-in-one-dev-1.opentouch.cloud/5651275247767646857786" to="d598767f16944aee942e3c8eb7811c30@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="8ed77aa4-9b64-41a2-afee-ef2610d0da13_1389"><room xmlns="jabber:iq:configuration" roomid="5a145a08236a9c01b2696443" roomjid="room_b953b0a14d67414f8c945987d52e8fee@muc.jerome-all-in-one-dev-1.opentouch.cloud" customData="{&quot;MyCustomKey&quot;:&quot;MyCustomValue&quot;}"/></message>
     
     * Moderator update
     <message xmlns="jabber:client" from="pcloud_enduser_8@jerome-all-in-one-dev-1.opentouch.cloud/66933146245540747446172" to="d598767f16944aee942e3c8eb7811c30@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="d2f706f1-1130-4ccb-9e44-58a949c7cb64_5220"><room xmlns="jabber:iq:configuration" roomid="59df35f868b4a55457245f15" roomjid="room_c14626da118f431398e191d9f07f8851@muc.jerome-all-in-one-dev-1.opentouch.cloud" userjid="d598767f16944aee942e3c8eb7811c30@jerome-all-in-one-dev-1.opentouch.cloud" privilege="moderator"/>
     
     * Owner change
     <message xmlns="jabber:client" from="pcloud_enduser_2@jerome-all-in-one-dev-1.opentouch.cloud/18432095315625666561318" to="fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud/mobile_ios_F6BE3F34-447C-43C4-86B9-8B3A117FF972" type="management" id="609afa61-836e-4377-bf24-87b277a43283_8959"><room xmlns="jabber:iq:configuration" roomid="59db796468b4a55457245ef5" roomjid="room_e30bd2ab14934319a539b02233488838@muc.jerome-all-in-one-dev-1.opentouch.cloud" userjid="fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud" privilege="owner"/>
     </message>
     */
    
    if([message isManagementMessage]){
        NSXMLElement *roomElement = [message elementForName:@"room" xmlns:kXMPPJabberConfigurationNS];
        if(roomElement){
            NSString *roomJid = [roomElement attributeForName:@"roomjid"].stringValue;
            NSString *roomid = [roomElement attributeForName:@"roomid"].stringValue;
            NSString *userJid = [roomElement attributeForName:@"userjid"].stringValue;
            NSString *status = [roomElement attributeForName:@"status"].stringValue;
            NSString *topic = [roomElement attributeForName:@"topic"].stringValue;
            NSString *name = [roomElement attributeForName:@"name"].stringValue;
            NSString *privilege = [roomElement attributeForName:@"privilege"].stringValue;
            NSString *lastAvatarUpdateDate = [roomElement attributeForName:@"lastAvatarUpdateDate"].stringValue;
            NSString *avatarAction = [[roomElement elementForName:@"avatar"] attributeForName:@"action"].stringValue;
            NSString *scheduledStartDateString = [roomElement attributeForName:@"scheduledStartDate"].stringValue;
            NSString *scheduledEndDateString = [roomElement attributeForName:@"scheduledEndDate"].stringValue;
            NSString *customData = [roomElement attributeForName:@"customData"].stringValue;

            Room *room = [_roomsService getRoomByJid:roomJid];
            if(!room){
                room = [_roomsService createOrUpdateRoomFromJID:roomJid withRainbowID:roomid];
                NSLog(@"We create a room because we don't known it %@", room);
            }
            if(userJid.length > 0){
                if([userJid isEqualToString:_contactsManagerService.myContact.jid]){
                    // This event concern my user
                    if([status isEqualToString:@"accepted"]){
                        // If we are in accepted status that means another device accept an invitation or create a room
                        [_xmppService joinRoom:room];
                        [_roomsService updateParticipant:userJid withStatus:[Participant stringToStatus:status] inRoom:room];
                        [_roomsDelegate xmppService:_xmppService didCreateRoom:room];
                    }
                    if([status isEqualToString:@"unsubscribed"]){
                        [_roomsService updateParticipant:userJid withStatus:[Participant stringToStatus:status] inRoom:room];
                        [_xmppService leaveRoom:room];
                    }
                    if([status isEqualToString:@"deleted"] || [status isEqualToString:@"rejected"]){
                        // If we are in deleted status that means another device reject an invitation or destroy a room
                        [_roomsService updateParticipant:userJid withStatus:[Participant stringToStatus:status] inRoom:room];
                        [_xmppService leaveRoom:room];
                        [_roomsService removeRoom:room];
                        [_roomsDelegate xmppService:_xmppService didDestroyRoom:room];
                    }
                    // Is this case possible ?
                    if([privilege isEqualToString:@"user"] || [privilege isEqualToString:@"moderator"] || [privilege isEqualToString:@"owner"]){
                        // update our privilege in room
                        [_roomsService internalUpdateParticipant:userJid withPrivilege:[Participant stringToPrivilege:privilege] inRoom:room];
                    }
                } else if ([privilege isEqualToString:@"user"] || [privilege isEqualToString:@"moderator"] || [privilege isEqualToString:@"owner"]){
                    // This event concern a participant of the room
                    // Just update his privilege in the room
                    [_roomsService internalUpdateParticipant:userJid withPrivilege:[Participant stringToPrivilege:privilege] inRoom:room];
                } else {
                    // This event concern a participant of the room
                    // Just update his status in the room
                    [_roomsService updateParticipant:userJid withStatus:[Participant stringToStatus:status] inRoom:room];
                    
                    // This might be a brand-new participant we don't know. Fetch its information if needed.
                    Contact *contact = [_contactsManagerService getContactWithJid:userJid];
                    if (!contact.vcardPopulated) {
                        // Use the jid because we might not have the rainbowID yet.
                        [_contactsManagerService populateVcardForContactsJids:@[userJid]];
                    }
                }
            } else {
                NSLog(@"No user JID, certainly a new topic '%@' or a new name '%@' or a new avatar update '%@' or a new schedule '%@'-'%@' or a customData update '%@'", topic, name, lastAvatarUpdateDate,
                      scheduledStartDateString, scheduledEndDateString, customData);
                
                if(topic.length > 0){
                    if(room)
                        [_roomsDelegate xmppService:_xmppService didChangeTopic:topic forRoom:room];
                    else
                        NSLog(@"We receive a new topic for a room that we don't known, do nothing");
                }
                if(name.length > 0){
                    if(room)
                        [_roomsDelegate xmppService:_xmppService didChangeName:name forRoom:room];
                    else
                        NSLog(@"We receive a new name for a room that we don't known, do nothing");
                }
                if(customData.length > 0){
                    if(room) {
                        NSDictionary *customDataDic = [[customData dataUsingEncoding:NSUTF8StringEncoding] objectFromJSONData];
                        [_roomsDelegate xmppService:_xmppService didChangeCustomData:customDataDic forRoom:room];
                    }
                    else
                        NSLog(@"We receive custom data for a room that we don't known, do nothing");
                }
                if(avatarAction.length > 0){
                    if(room) {
                        if([avatarAction isEqualToString:@"update"] && lastAvatarUpdateDate.length > 0) {
                            NSDate *date = [Tools dateFromString:lastAvatarUpdateDate withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
                            [_roomsDelegate xmppService:_xmppService didUpdateAvatarInRoom:room withDate:date];
                        }
                        else if([avatarAction isEqualToString:@"delete"]) {
                            [_roomsDelegate xmppService:_xmppService didDeleteAvatarInRoom:room];
                        }
                    }
                    else
                        NSLog(@"We receive a new avatar update for a room that we don't known, do nothing");
                }
                if(scheduledStartDateString.length > 0 || scheduledEndDateString.length > 0){
                    if(room) {
                        NSDate *scheduledStartDate = [Tools dateFromString:scheduledStartDateString withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
                        NSDate *scheduledEndDate = [Tools dateFromString:scheduledEndDateString withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
                        [_roomsDelegate xmppService:_xmppService didChangedConferenceScheduleInRoom: room startDate:scheduledStartDate endDate:scheduledEndDate];
                    }
                    else
                        NSLog(@"We receive a new schedule update for a conference in a room that we don't known, do nothing");
                }
            }
            return;
        }
    }
#pragma mark - Group Events
    // Deal with managment message for groups
    
    /**
     *
     * Create a group
     <message type="management" id="8413b42e-563c-4437-9a53-06f638b5ab69_0"
     from="pcloud@openrainbow.com/172440802160413612281463752830017532"
     to="85a456023ad249bea7a0cb1d5b4fb34a@openrainbow.com" xmlns="jabber:client">
     <group id="57b44e9c0c32e0b425252f9e" action="create" scope="group" xmlns="jabber:iq:configuration"/>
     </message>
     
     * Delete a group
     <message type="management" id="8413b42e-563c-4437-9a53-06f638b5ab69_0"
     from="pcloud@openrainbow.com/172440802160413612281463752830017532"
     to="85a456023ad249bea7a0cb1d5b4fb34a@openrainbow.com" xmlns="jabber:client">
     <group id="57b44e9c0c32e0b425252f9e" action="delete" scope="group" xmlns="jabber:iq:configuration"/>
     </message>
     
     *  Add a user in a group
     <message type="management" id="8413b42e-563c-4437-9a53-06f638b5ab69_0"
     from="pcloud@openrainbow.com/172440802160413612281463752830017532"
     to="85a456023ad249bea7a0cb1d5b4fb34a@openrainbow.com" xmlns="jabber:client">
     <group id="57b44e9c0c32e0b425252f9e" action="create" scope="user" userId="574ff5153448af6c2940f908" xmlns="jabber:iq:configuration"/>
     </message>
     
     * Remove a user from a group
     <message type="management" id="8413b42e-563c-4437-9a53-06f638b5ab69_0"
     from="pcloud@openrainbow.com/172440802160413612281463752830017532"
     to="85a456023ad249bea7a0cb1d5b4fb34a@openrainbow.com" xmlns="jabber:client">
     <group id="57b44e9c0c32e0b425252f9e" action="delete" scope="user" userId="574ff5153448af6c2940f908" xmlns="jabber:iq:configuration"/>
     </message>
     
     * Update a group
     <message type="management" id="8413b42e-563c-4437-9a53-06f638b5ab69_0"
     from="pcloud@openrainbow.com/172440802160413612281463752830017532"
     to="85a456023ad249bea7a0cb1d5b4fb34a@openrainbow.com" xmlns="jabber:client">
     <group id="57b44e9c0c32e0b425252f9e" action="update" scope="group" name="Friends" comment="Group with by best friends" xmlns="jabber:iq:configuration"/>
     </message>
     */
    if([message isManagementMessage]) {
        NSXMLElement *groupElement = [message elementForName:@"group" xmlns:kXMPPJabberConfigurationNS];
        if(groupElement){
            NSString *groupID = [groupElement attributeForName:@"id"].stringValue;
            NSString *action = [groupElement attributeForName:@"action"].stringValue;
            NSString *scope = [groupElement attributeForName:@"scope"].stringValue;
            NSString *userId = [groupElement attributeForName:@"userId"].stringValue;
            Group *group = [_groupsService getGroupByRainbowID:groupID];
            
            if([action isEqualToString:@"create"]){
                if([scope isEqualToString:@"group"]){
                    NSLog(@"We create a group because we don't known it %@", group);
                    group = [_groupsService createGroupWithRainbowID:groupID];
                }
                if([scope isEqualToString:@"user"]){
                    NSLog(@"We add the user %@ in group %@", userId, group);
                    Contact *contact = [_contactsManagerService getContactWithRainbowID:userId];
                    if(!contact){
                        contact = [_contactsManagerService createOrUpdateRainbowContactWithRainbowId:userId];
                    }
                    [_groupsService insertContact:contact inGroup:group];
                }
            }
            
            if([action isEqualToString:@"delete"]){
                if([scope isEqualToString:@"group"]){
                    NSLog(@"We delete a group %@", group);
                    [_groupsService removeGroup:group];
                }
                if([scope isEqualToString:@"user"]){
                    NSLog(@"We remove the user %@ in group %@", userId, group);
                    Contact *contact = [_contactsManagerService getContactWithRainbowID:userId];
                    [_groupsService deleteContact:contact inGroup:group];
                }
            }
            
            if([action isEqualToString:@"update"]){
                if([scope isEqualToString:@"group"]){
                    NSString *groupName = [groupElement attributeForName:@"name"].stringValue;
                    NSString *comment = [groupElement attributeForName:@"comment"].stringValue;
                    [_groupsService updateGroup:group withName:groupName andComment:comment];
                }
            }
        }
    }
    
#pragma mark - Mute Events
    /**
     *  Mute a conversation
     <message xmlns="jabber:client" from="pcloud2@jerome-all-in-one-dev-1.opentouch.cloud/72962844697413454711476089818742109" to="9ffcf8eb59ba4c5f9da9e1606cc9b2d6@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="7ac193da-b4ec-4d7c-806c-c21c5c9c93af_2611"><mute xmlns="jabber:iq:configuration" conversation="57fb9ecc5fb5d043585bd9fa"/></message>
     */
    if([message isManagementMessage]){
        NSXMLElement *muteElement = [message elementForName:@"mute" xmlns:kXMPPJabberConfigurationNS];
        if(muteElement){
            // We found a mute element we must have a conversation ID
            NSString *conversationID = [muteElement attributeForName:@"conversation"].stringValue;
            [_conversationDelegate xmppService:_xmppService didReceiveMuteConversation:conversationID];
        }
    }
    
#pragma mark - Unmute Events
    /**
     *  Unmute conversation
     <message xmlns="jabber:client" from="pcloud10@jerome-all-in-one-dev-1.opentouch.cloud/167735223009863261831476089818791788" to="9ffcf8eb59ba4c5f9da9e1606cc9b2d6@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="7ac193da-b4ec-4d7c-806c-c21c5c9c93af_2669"><unmute xmlns="jabber:iq:configuration" conversation="57fb5be55fb5d043585bd9c4"/></message>
     */
    
    if([message isManagementMessage]){
        NSXMLElement *unmuteElement = [message elementForName:@"unmute" xmlns:kXMPPJabberConfigurationNS];
        if(unmuteElement){
            // We found a mute element we must have a conversation ID
            NSString *conversationID = [unmuteElement attributeForName:@"conversation"].stringValue;
            [_conversationDelegate xmppService:_xmppService didReceiveUnMuteConversation:conversationID];
        }
    }
#pragma mark - User settings update
    /**
     *  User setting update event
     *  <message type="management" id="8413b42e-563c-4437-9a53-06f638b5ab69_0"
     from="pcloud@openrainbow.com/172440802160413612281463752830017532"
     to="85a456023ad249bea7a0cb1d5b4fb34a@openrainbow.com" xmlns="jabber:client">
     <usersettings action="update" xmlns="jabber:iq:configuration"/>
     </message>
     */
    if([message isManagementMessage]){
        NSXMLElement *usersettingsElement = [message elementForName:@"usersettings" xmlns:kXMPPJabberConfigurationNS];
        if(usersettingsElement){
            // We found a usersettings element, we must update our presence settings
            [_contactsManagerService didChangeUserSettings];
        }
    }
    
#pragma mark - User conference activated
    /**
     *  User conference creation actived
        <message type="management" id="2c11e9f1-3e59-4c7d-b1b7-1d38d31ab458_0" to="ae55432b2d064d38be333204591daca9@francky-all-in-one-dev-1.opentouch.cloud" xmlns="jabber:client">
            <confuseractivated action="create" xmlns="jabber:iq:configuration"/>
        </message>
     */
    if([message isManagementMessage]){
        NSXMLElement *confuseractivated = [message elementForName:@"confuseractivated" xmlns:kXMPPJabberConfigurationNS];
        if(confuseractivated){
            NSString *action = [confuseractivated attributeForName:@"action"].stringValue;
            
            if([action isEqualToString:@"create"]) {
                [_contactsManagerService didReceiveCreateConfUserActivated:YES];
            }
            else if([action isEqualToString:@"delete"]) {
                [_contactsManagerService didReceiveCreateConfUserActivated:NO];
            }
        }
    }
    
#pragma mark - User deleted
    /**
     *  A user has been deleted
     <message id="8413b42e-563c-4437-9a53-06f638b5ab69_0" type="management"
     from="pcloud_enduser_1@openrainbow.com/172440802160413612281463752830017532"
     to="5abb735b2d3c4e50adde276c50ec489c@@openrainbow.com"
     xmlns="jabber:client">
     <useraccount id="56c5c19f94141765119f896c" action="update" xmlns="jabber:iq:configuration"/>
     </message>
     */
    if([message isManagementMessage]){
        NSXMLElement *useraccountElement = [message elementForName:@"useraccount" xmlns:kXMPPJabberConfigurationNS];
        if(useraccountElement){
            // We found a useraccount element
            NSString *action = [useraccountElement attributeForName:@"action"].stringValue;
            
            if([action isEqualToString:@"update"]) {
            //    [_contactsManagerService didReceiveUserDelete];
            }
            else if([action isEqualToString:@"delete"]) {
            //    [_contactsManagerService didReceiveUserDelete];
            }
        }
    }
    
#pragma mark - licence changed
    /*
     * <message type="management" id="27f3b13e-f141-46b5-9149-e55f05ba1711_0" to="b35492ddbbb4474faa485b1d1dac00c7@nico-all-in-one-dev-1.opentouch.cloud" xmlns="jabber:client">
     <userprofile action="delete" subscriptionId="58cfdd63b4d087d2b5595c5b" offerReference="RB-Business"
     </message>
     */
    
    if([message isManagementMessage]){
        NSXMLElement *userProfile = [message elementForName:@"userprofile" xmlns:kXMPPJabberConfigurationNS];
        if(userProfile){
            [_contactsManagerService.myUser fetchProfiles];
        }
    }

#pragma mark - password changed
    /*
     * <message xmlns='jabber:client' from='pcloud_admin_1@nico-all-in-one-dev-1.opentouch.cloud/485534143633809932837502' to='425fc895cac04b079261cee2fdc4f8ad@nico-all-in-one-dev-1.opentouch.cloud' type='management' id='a1d68cef-df19-4da9-9054-8df4a92a54e5_15'><no-store xmlns='urn:xmpp:hints'/><userpassword xmlns='jabber:iq:configuration' action='update'/></message>

     */
    if([message isManagementMessage]){
        NSXMLElement *element = [message elementForName:@"userpassword" xmlns:kXMPPJabberConfigurationNS];
        if(element){
            NSLog(@"WE DETECT A CHANGE PASSWORD");
            [_loginDelegate xmppServiceDidReceiveChangePasswordMessage:_xmppService];
            [_xmppService disconnect];
        }
    }
    
#pragma mark - User Invite events
    /**
     *  I've received an invitation
     <message xmlns="jabber:client" from="pcloud10@jerome-all-in-one-dev-1.opentouch.cloud/22417636629952626711478624307115300" to="00934009cb83452f82a368a35d525298@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="627fa1b5-ef94-4ff1-9175-aa31c2021b2d_209"><userinvite xmlns="jabber:iq:configuration" id="5822e4242489e1056f691858" action="create" type="received" status="pending"/></message>
     
     *  I've sent an invitation (on another device)
     <message type='management' id='122' from='jid_from@openrainbow.com' to='jid_to@openrainbow.com' xmlns='jabber:client'><userinvite action="create" id='57cd5922d341df5812bbcb72' type='sent' status='pending' xmlns='jabber:iq:configuration'/></message>
     
     *  Invitation accepted (type = received or sent)
     <message type='management' id='122' from='jid_from@openrainbow.com' to='jid_to@openrainbow.com' xmlns='jabber:client'><userinvite id='57cd5922d341df5812bbcb72' action="update" type='received/sent' status='accepted' xmlns='jabber:iq:configuration'/></message>
     */
    
    if([message isManagementMessage]) {
        NSXMLElement *userInviteElement = [message elementForName:@"userinvite" xmlns:kXMPPJabberConfigurationNS];
        if(userInviteElement) {
            NSString *invitationID = [userInviteElement attributeForName:@"id"].stringValue;
            NSString *type = [userInviteElement attributeForName:@"type"].stringValue;
            NSString *action = [userInviteElement attributeForName:@"action"].stringValue;
            
            if([action isEqualToString:@"create"] || [action isEqualToString:@"re-send"]) {
                [_contactsManagerService createOrUpdateInvitation:[type isEqualToString:@"sent"]?InvitationDirectionSent:InvitationDirectionReceived withID:invitationID];
            }
            if([action isEqualToString:@"delete"]){
                [_contactsManagerService removeInvitationWithID:invitationID];
            }
            if([action isEqualToString:@"update"]){
                [_contactsManagerService createOrUpdateInvitation:[type isEqualToString:@"sent"]?InvitationDirectionSent:InvitationDirectionReceived withID:invitationID];
            }
        }
    }
#pragma mark - Company invitation message
    if([message isManagementMessage]){
        /*
         * I receive a company invitation
         * <message xmlns="jabber:client" from="pcloud_admin_2@jerome-all-in-one-dev-1.opentouch.cloud/34378525980941883643252" to="fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="3b6fa8c5-a50c-4b4b-a597-f2a736543f51_1"><joincompanyinvite xmlns="jabber:iq:configuration" id="58a6324d45c568028fb23262" action="create" type="received" status="pending"/></message>
         
         *
         *  I accept a company invitation (from another device)
         *  <message type='management' id='122' from='jid_from@openrainbow.com' to='jid_to@openrainbow.com' xmlns='jabber:client'> <joincompanyinvite action="update" id='582048dfe2e68a79f4979624' status='accepted' type='received' xmlns='jabber:iq:configuration'/>
         </message>
         
         * I decline a company invitation (from another device)
         * <message type='management' id='122' from='jid_from@openrainbow.com' to='jid_to@openrainbow.com' xmlns='jabber:client'> <joincompanyinvite action="update" id='582048dfe2e68a79f4979624' status='declined' type='received' xmlns='jabber:iq:configuration'/>
         </message>
         
         * A company invitation has been canceled by sender
         * <message xmlns="jabber:client" from="pcloud_admin_8@jerome-all-in-one-dev-1.opentouch.cloud/999596716757238456215894" to="1d4d4a0d6c3a4457a281f3fb22ccdb2b@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="3b6fa8c5-a50c-4b4b-a597-f2a736543f51_17"><joincompanyinvite xmlns="jabber:iq:configuration" id="58a63b9445c568028fb23265" action="update" type="received" status="canceled"/></message>
         
         * A company invitation is resent because it has been refused first
         *  <message xmlns="jabber:client" from="pcloud_admin_4@jerome-all-in-one-dev-1.opentouch.cloud/309894290249460910016656" to="1d4d4a0d6c3a4457a281f3fb22ccdb2b@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="3b6fa8c5-a50c-4b4b-a597-f2a736543f51_53"><joincompanyinvite xmlns="jabber:iq:configuration" id="58a63e1b45c568028fb2326b" action="re-send" type="received" status="pending"/></message>
         */
        
        NSXMLElement *joinCompanyInviteElement = [message elementForName:@"joincompanyinvite" xmlns:kXMPPJabberConfigurationNS];
        if(joinCompanyInviteElement) {
            NSString *companyInvitationID = [joinCompanyInviteElement attributeForName:@"id"].stringValue;
            NSString *direction = [joinCompanyInviteElement attributeForName:@"type"].stringValue;
            NSString *action = [joinCompanyInviteElement attributeForName:@"action"].stringValue;
            NSString *status = [joinCompanyInviteElement attributeForName:@"status"].stringValue;
            if([action isEqualToString:@"create"] || [action isEqualToString:@"update"] || [action isEqualToString:@"re-send"]) {
                [_companiesService createOrUpdateCompanyInvitationWithId:companyInvitationID direction:direction status:status];
            }
        }
    }
#pragma mark - Company join request message
    if([message isManagementMessage]){
        /*
         * I receive a company join accepted
         * <message xmlns="jabber:client" from="pcloud_admin_3@jerome-all-in-one-dev-1.opentouch.cloud/166795374288783934285048" to="03cf60fd1a4a4c94b5fecda8a30434b6@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="5e170953-861c-4b1f-901c-905bd27aa2b4_12"><joincompanyrequest xmlns="jabber:iq:configuration" id="58da2302c13e371091f31cd6" action="update" type="sent" status="accepted"/></message>
         *
         * I receive a company join rejected
         * <message xmlns="jabber:client" from="pcloud_admin_9@jerome-all-in-one-dev-1.opentouch.cloud/142290794486032080233308" to="03cf60fd1a4a4c94b5fecda8a30434b6@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="5e170953-861c-4b1f-901c-905bd27aa2b4_8"><joincompanyrequest xmlns="jabber:iq:configuration" id="58da2302c13e371091f31cd6" action="update" type="sent" status="declined"/></message>
         *
         *  I receive a company delete invitation
         *  <message xmlns="jabber:client" from="pcloud_enduser_5@jerome-all-in-one-dev-1.opentouch.cloud/81427921316199853224888" to="caabb03975324d068f49f003fca06588@jerome-all-in-one-dev-1.opentouch.cloud" type="management" id="43657b9f-3bef-4382-8c6b-23c75e11fc6e_3968"><joincompanyrequest xmlns="jabber:iq:configuration" id="594008761b0323f8b335a797" action="delete" type="sent" status="pending"/></message>
         */
        NSXMLElement *joinCompanyJoinElement = [message elementForName:@"joincompanyrequest" xmlns:kXMPPJabberConfigurationNS];
        if(joinCompanyJoinElement) {
            NSString *companyInvitationID = [joinCompanyJoinElement attributeForName:@"id"].stringValue;
            NSString *direction = [joinCompanyJoinElement attributeForName:@"type"].stringValue;
            NSString *action = [joinCompanyJoinElement attributeForName:@"action"].stringValue;
            NSString *status = [joinCompanyJoinElement attributeForName:@"status"].stringValue;
            if([action isEqualToString:@"update"]) {
                [_companiesService updateCompanyInvitationJoinWithId:companyInvitationID direction:direction status:status];
            }
            if([action isEqualToString:@"delete"]){
                [_companiesService removeCompanyInvitationWithID:companyInvitationID];
            }
        }
    }
    
#pragma mark - Conference management message
    if([message isConferenceManagementMessage]){
        NSXMLElement *conferenceElement = [message elementForName:@"conference-info" xmlns:kXMPPJabberConferenceNS];
        /**
         * <message xmlns="jabber:client" from="janusgateway.jerome-all-in-one-dev-1.opentouch.cloud" to="fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud/conf" type="chat"><conference-info xmlns="jabber:iq:conference"><conference-id>59b7e2cad556f5cdf0bec468</conference-id><media-type xmlns="urn:xmpp:janus:1">webrtc</media-type><conference-state><active>true</active><talker-active>true</talker-active><recording-started>false</recording-started><participant-count>2</participant-count></conference-state><participants><participant><participant-id>59c3bf686c6cd9d209ec2d39</participant-id><jid-im>fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud</jid-im><role>moderator</role><mute>off</mute><hold>off</hold><cnx-state>connected</cnx-state></participant></participants></conference-info></message>
         
         // A participant join
         <message xmlns="jabber:client" from="janusgateway.jerome-all-in-one-dev-1.opentouch.cloud" to="fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud/conf" type="chat"><conference-info xmlns="jabber:iq:conference"><conference-id>59b7e2cad556f5cdf0bec468</conference-id><media-type xmlns="urn:xmpp:janus:1">webrtc</media-type><added-participants><participant><participant-id>59c535d63077d86a6ccf0140</participant-id><jid-im>c2673e1952ef40178b68cb4ae2c6c2ed@jerome-all-in-one-dev-1.opentouch.cloud</jid-im><role>moderator</role><mute>off</mute><hold>off</hold><cnx-state>connected</cnx-state></participant></added-participants></conference-info></message>

         // A participant join with a phonenumber
         <message xmlns="jabber:client" xmlns:ns2="jabber:client" from="tel_3f5e210b41ff4e0aae5afba660cd277a@openrainbow.com/conf" to="3f5e210b41ff4e0aae5afba660cd277a@openrainbow.com"><conference-info xmlns="jabber:iq:conference"><conference-id>59ef56ddc2d3034d44416319</conference-id><added-participants><participant><participant-id>5a8ad3bebc141b49e47cebff</participant-id><phone-number>44767960402</phone-number><role>member</role><mute>off</mute><hold>off</hold><cnx-state>connected</cnx-state></participant></added-participants></conference-info></message>

         */
        if(conferenceElement){
            ConferenceInfo *conferenceInfo = [_xmppService conferenceInfoMessageFromXmppMessage:message];
            [_conferenceDelegate xmppService:_xmppService didReceiveConferenceInfo:conferenceInfo];
            return;
        }
        /*
         * End of conference messages are sent with jabber:iq:notification namespace :
         * <message xmlns="jabber:client" xmlns:ns2="jabber:client" from="pgiservice.demo-all-in-one-dev-1.opentouch.cloud" to="dd91ef00c2ca4a25b4c4704ca43e1beb@demo-all-in-one-dev-1.opentouch.cloud/conf" type="chat"><conference-info xmlns="jabber:iq:notification"><conference-id>59525429006d8921a0ea91e5</conference-id><conference-state><active>false</active></conference-state></conference-info></message>
         */
        conferenceElement = [message elementForName:@"conference-info" xmlns:kXMPPJabberNotificationNS];
        if(conferenceElement){
            ConferenceInfo *conferenceInfo = [_xmppService conferenceInfoMessageFromXmppMessage:message];
            [_conferenceDelegate xmppService:_xmppService didReceiveConferenceInfo:conferenceInfo];
            return;
        }
    }
}

#pragma mark - Presence
- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
    // Deal with update of avatar or vcard in presence messages
    // For avatar :
    //<presence xmlns="jabber:client"><x xmlns="vcard-temp:x:update"><avatar/></x><actor xmlns="jabber:iq:configuration"/></presence>
    // For PGI Conference right update
    //<presence xmlns="jabber:client"><x xmlns='vcard-temp:x:update'><data/></x><actor xmlns='jabber:iq:configuration'/><priority>5</priority></presence>
    NSXMLElement *xElement = [presence elementForName:kXMPPvCardAvatarElement xmlns:kXMPPvCardAvatarNS];
    NSXMLElement *delayElement = [presence elementForName:KXMPPvCardDelayElement xmlns:kXMPPAvatarDelay];
    if (xElement) {
        NSLog(@"We found a x element in presence %@", presence);
        NSXMLElement *photoElement = [xElement elementForName:kXMPPvCardAvatarPhotoElement];
        NSXMLElement *dataElement = [xElement elementForName:kXMPPvCardDataElement];
        
        if (photoElement || dataElement) {
            NSLog(@"We found a avatar tag, or a data tag, we must update the photo or the vcard");
            if([presence isPresenceWithActor]){
                Contact *contact = [_contactsManagerService getContactWithJid:presence.from.bare];
                if(presence.wasDelayed){
                    contact.rainbow.userPhoto.lastUpdateDate = presence.delayedDeliveryDate;
                }
                else{
                   contact.rainbow.userPhoto.lastUpdateDate = [NSDate date];
                    
                }
                NSLog(@"We found an actor element");
                NSLog(@"last update date for userphoto is: %@",contact.rainbow.userPhoto.lastUpdateDate);
                if(photoElement){
                    [_contactsManagerService populateAvatarForRainbowContact:contact forceReload:YES];
                }
                
                if(dataElement) {
                    if( [contact isEqual:_contactsManagerService.myContact] )
                        [_contactsManagerService.myUser fetchProfilesFeatures];
                    
                    [_contactsManagerService populateVcardForContactsJids:@[presence.from.bare]];
                }
            }
        }
    }
    
}

@end

