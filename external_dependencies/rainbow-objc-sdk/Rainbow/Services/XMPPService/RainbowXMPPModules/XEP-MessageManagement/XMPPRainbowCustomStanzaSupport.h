/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPModule.h"
#import "XMPPServiceProtocol.h"
#import "RoomsService.h"
#import "ContactsManagerService.h"
#import "GroupsService.h"
#import "CompaniesService.h"
#import "ConferencesManagerService.h"
#import "XMPPMessage.h"
#import "FileSharingService.h"
#import "FileSharingService+Internal.h"
#import "XMPPMessageArchiveManagementCoreDataStorage.h"

@interface XMPPRainbowCustomStanzaSupport : XMPPModule
@property (nonatomic, assign) id<XMPPServiceConversationDelegate> conversationDelegate;
@property (nonatomic, assign) id<XMPPServiceRoomsDelegate> roomsDelegate;
@property (nonatomic, assign) id<XMPPServiceConferenceDelegate> conferenceDelegate;
@property (nonatomic, assign) id<XMPPServiceLoginDelegate> loginDelegate;
@property (nonatomic, weak) FileSharingService *fileService;
@property (nonatomic, weak) RoomsService *roomsService;
@property (nonatomic, weak) ContactsManagerService *contactsManagerService;
@property (nonatomic, weak) XMPPService *xmppService;
@property (nonatomic, weak) GroupsService *groupsService;
@property (nonatomic, weak) CompaniesService *companiesService;
@property (nonatomic, strong) XMPPMessageArchiveManagementCoreDataStorage *xmppMessageArchiveManagementStorage;
- (void) treatManagementXMPPMessage:(XMPPMessage *) message;

@end
