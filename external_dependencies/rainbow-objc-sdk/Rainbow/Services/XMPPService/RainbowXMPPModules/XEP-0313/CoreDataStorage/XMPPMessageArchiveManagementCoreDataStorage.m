#import "XMPPMessageArchiveManagementCoreDataStorage.h"
#import "XMPPCoreDataStorageProtected.h"
#import "XMPPLogging.h"
#import "NSXMLElement+XEP_0203.h"
#import "XMPPMessage+XEP_0085.h"
#import "NSXMLElement+XEP_0297.h"
#import "NSDate+XMPPDateTimeProfiles.h"
#import "XMPPMessage+XEP_0280.h"
#import "XMPPMessageArchiveManagement.h"
#import "XMPPMessage+XEP_0313.h"
#import "XMPPMessage+XEP0045.h"
#import "XMPPMessage+RainbowCustomMessageTypes.h"
#import "XMPPMessage+XEP_0066.h"
#import "XMPPMessage+XEP_313_CallLogs.h"
#import "MessageInternal.h"
#import "CallLog+Internal.h"

#if ! __has_feature(objc_arc)
#warning This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

// Log levels: off, error, warn, info, verbose
// Log flags: trace
#if DEBUG
static const int xmppLogLevel = XMPP_LOG_LEVEL_VERBOSE; // VERBOSE; // | XMPP_LOG_FLAG_TRACE;
#else
static const int xmppLogLevel = XMPP_LOG_LEVEL_WARN;
#endif


@interface XMPPMessageArchiveManagementCoreDataStorage ()
{
    NSString *messageEntityName;
    NSString *contactEntityName;
}

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation XMPPMessageArchiveManagementCoreDataStorage

static XMPPMessageArchiveManagementCoreDataStorage *sharedInstance;

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[XMPPMessageArchiveManagementCoreDataStorage alloc] initWithDatabaseFilename:nil storeOptions:nil];
    });
    
    return sharedInstance;
}

/**
 * Documentation from the superclass (XMPPCoreDataStorage):
 *
 * If your subclass needs to do anything for init, it can do so easily by overriding this method.
 * All public init methods will invoke this method at the end of their implementation.
 *
 * Important: If overriden you must invoke [super commonInit] at some point.
 **/
- (void)commonInit
{
    [super commonInit];
    
    messageEntityName = @"XMPPMessageArchiveManagement_Message_CoreDataObject";
    contactEntityName = @"XMPPMessageArchiveManagement_Contact_CoreDataObject";
}

/**
 * Documentation from the superclass (XMPPCoreDataStorage):
 *
 * Override me, if needed, to provide customized behavior.
 * For example, you may want to perform cleanup of any non-persistent data before you start using the database.
 *
 * The default implementation does nothing.
 **/
- (void)didCreateManagedObjectContext
{
    // If there are any "composing" messages in the database, delete them (as they are temporary).
    
    NSManagedObjectContext *moc = [self managedObjectContext];
    NSEntityDescription *messageEntity = [self messageEntity:moc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"composing == YES"];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = messageEntity;
    fetchRequest.predicate = predicate;
    fetchRequest.fetchBatchSize = saveThreshold;
    [moc performBlock:^{
        NSError *error = nil;
        NSArray *messages = [moc executeFetchRequest:fetchRequest error:&error];
        
        if (messages == nil)
            {
            XMPPLogError(@"%@: %@ - Error executing fetchRequest: %@", [self class], THIS_METHOD, error);
            return;
            }
        
        NSUInteger count = 0;
        
        for (XMPPMessageArchiveManagement_Message_CoreDataObject *message in messages)
            {
            [moc deleteObject:message];
            
            if (++count > saveThreshold)
                {
                if (![moc save:&error])
                    {
                    XMPPLogWarn(@"%@: Error saving - %@ %@", [self class], error, [error userInfo]);
                    [moc rollback];
                    }
                }
            }
        
        if (count > 0)
            {
            if (![moc save:&error])
                {
                XMPPLogWarn(@"%@: Error saving - %@ %@", [self class], error, [error userInfo]);
                [moc rollback];
                }
            }
    }];
}

-(void) cleanAllMessages {
    NSLog(@"Cleaning all data because of user changed notification");
    [self deleteAllObjects:[self messageEntityName]];
    [self deleteAllObjects:[self contactEntityName]];
}

- (void) deleteAllObjects: (NSString *) entityDescription  {
    dispatch_block_t block = ^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *messageEntity = [self messageEntity:moc];
        [moc performBlock:^{
            fetchRequest.entity = messageEntity;
            
            NSError *error;
            NSArray *items = [moc executeFetchRequest:fetchRequest error:&error];
            
            for (NSManagedObject *managedObject in items) {
                [moc deleteObject:managedObject];
                NSLog(@"%@ object deleted",entityDescription);
            }
            if (![moc save:&error]) {
                NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
            }
        }];
    };
    
    if (dispatch_get_specific(storageQueueTag))
        block();
    else
        dispatch_sync(storageQueue, block);
    
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Internal API
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)willInsertMessage:(XMPPMessageArchiveManagement_Message_CoreDataObject *)message
{
    // Override hook
}

- (void)didUpdateMessage:(XMPPMessageArchiveManagement_Message_CoreDataObject *)message
{
    // Override hook
}

- (void)willDeleteMessage:(XMPPMessageArchiveManagement_Message_CoreDataObject *)message
{
    // Override hook
}

- (void)willInsertContact:(XMPPMessageArchiveManagement_Contact_CoreDataObject *)contact
{
    // Override hook
}

- (void)didUpdateContact:(XMPPMessageArchiveManagement_Contact_CoreDataObject *)contact
{
    // Override hook
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Private API
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(XMPPMessageArchiveManagement_Message_CoreDataObject *) messageWithID:(NSString *) messageID managedObjectContext:(NSManagedObjectContext *)moc entity:(NSEntityDescription *) entity {
    
    __block XMPPMessageArchiveManagement_Message_CoreDataObject *result = nil;
    
    NSString *predicateFrmt = @"messageId == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, messageID];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entity;
    fetchRequest.predicate = predicate;
    fetchRequest.fetchLimit = 1;
    
//    [moc performBlockAndWait:^{
        NSError *error = nil;
        NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
        if (results == nil || error) {
            XMPPLogError(@"%@: %@ - Error executing fetchRequest: %@", THIS_FILE, THIS_METHOD, fetchRequest);
        } else {
            result = (XMPPMessageArchiveManagement_Message_CoreDataObject *)[results lastObject];
        }
//    }];
    
    return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Public API
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (XMPPMessageArchiveManagement_Contact_CoreDataObject *)contactForMessage:(XMPPMessageArchiveManagement_Message_CoreDataObject *)msg
{
    // Potential override hook
    
    return [self contactWithBareJidStr:msg.bareJidStr
                      streamBareJidStr:msg.streamBareJidStr
                  managedObjectContext:msg.managedObjectContext];
}

- (XMPPMessageArchiveManagement_Contact_CoreDataObject *)contactWithJid:(XMPPJID *)contactJid
                                                              streamJid:(XMPPJID *)streamJid
                                                   managedObjectContext:(NSManagedObjectContext *)moc
{
    return [self contactWithBareJidStr:[contactJid bare]
                      streamBareJidStr:[streamJid bare]
                  managedObjectContext:moc];
}

- (XMPPMessageArchiveManagement_Contact_CoreDataObject *)contactWithBareJidStr:(NSString *)contactBareJidStr
                                                              streamBareJidStr:(NSString *)streamBareJidStr
                                                          managedObjectContext:(NSManagedObjectContext *)moc
{
    NSEntityDescription *entity = [self contactEntity:moc];
    
    NSPredicate *predicate;
    if (streamBareJidStr)
        {
        predicate = [NSPredicate predicateWithFormat:@"bareJidStr == %@ AND streamBareJidStr == %@",
                     contactBareJidStr, streamBareJidStr];
        }
    else
        {
        predicate = [NSPredicate predicateWithFormat:@"bareJidStr == %@", contactBareJidStr];
        }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchLimit:1];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    if (results == nil)
        {
        XMPPLogError(@"%@: %@ - Fetch request error: %@", THIS_FILE, THIS_METHOD, error);
        return nil;
        }
    else
        {
        return (XMPPMessageArchiveManagement_Contact_CoreDataObject *)[results lastObject];
        }
}

- (NSString *)messageEntityName
{
    __block NSString *result = nil;
    
    dispatch_block_t block = ^{
        result = messageEntityName;
    };
    
    if (dispatch_get_specific(storageQueueTag))
        block();
    else
        dispatch_sync(storageQueue, block);
    
    return result;
}

- (void)setMessageEntityName:(NSString *)entityName
{
    dispatch_block_t block = ^{
        messageEntityName = entityName;
    };
    
    if (dispatch_get_specific(storageQueueTag))
        block();
    else
        dispatch_async(storageQueue, block);
}

- (NSString *)contactEntityName
{
    __block NSString *result = nil;
    
    dispatch_block_t block = ^{
        result = contactEntityName;
    };
    
    if (dispatch_get_specific(storageQueueTag))
        block();
    else
        dispatch_sync(storageQueue, block);
    
    return result;
}

- (void)setContactEntityName:(NSString *)entityName
{
    dispatch_block_t block = ^{
        contactEntityName = entityName;
    };
    
    if (dispatch_get_specific(storageQueueTag))
        block();
    else
        dispatch_async(storageQueue, block);
}

- (NSEntityDescription *)messageEntity:(NSManagedObjectContext *)moc
{
    // This is a public method, and may be invoked on any queue.
    // So be sure to go through the public accessor for the entity name.
    
    return [NSEntityDescription entityForName:[self messageEntityName] inManagedObjectContext:moc];
}

- (NSEntityDescription *)contactEntity:(NSManagedObjectContext *)moc
{
    // This is a public method, and may be invoked on any queue.
    // So be sure to go through the public accessor for the entity name.
    
    return [NSEntityDescription entityForName:[self contactEntityName] inManagedObjectContext:moc];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Storage Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)configureWithParent:(XMPPMessageArchiveManagement *)aParent queue:(dispatch_queue_t)queue
{
    return [super configureWithParent:aParent queue:queue];
}


- (void)archiveMessage:(XMPPMessage *) originalMessage outgoing:(BOOL)outgoing xmppStream:(XMPPStream *)xmppStream
{
    // Message should either have a body, or be a composing notification
    
    NSXMLElement *result = [originalMessage resultStanza];
    NSXMLElement *forwarded = nil;
    NSXMLElement *callLog = nil;
    NSDate *forwardedDelay = nil;
    NSString *messageID = nil;
    NSString *mamMessageID = [result attributeStringValueForName:@"id"];
    XMPPMessage *message = nil;
    if(result) {
        forwarded = [result forwardedStanza];
        message = [originalMessage resultForwardedMessage];
        forwardedDelay = [forwarded delayedDeliveryDate];
        // Use the correct forwarded messageID and not the IQ ID
        messageID = [message elementID];
    } else {
        // it's certainly an "normal" message
        
        message = originalMessage;

        // Check if this not a carbon copy message
        if([message isMessageCarbon]){
            message = [message messageCarbonForwardedMessage];
            if([message isArchivedMessage])
                forwardedDelay = [message archivedMessageDate];
            else
                forwardedDelay = [originalMessage delayedDeliveryDate];
        }
        
        if([message isArchivedMessage] && mamMessageID.length == 0){
            mamMessageID = [message archivedMamMessageID];
        }
        
        messageID = [message elementID];
    }
    
    // Check if this message is a call log
    if ([message isCallLogMessage] || [message isUpdatedCallLogMessage]){
        result = [message callLogResultStanza];
        if(result){
            forwardedDelay = [message callLogForwardedDate];
            callLog = [message callLogStanza];
        } else {
            forwardedDelay = [[message updatedCallLogForwardedStanza] delayedDeliveryDate];
            callLog = [message updatedCallLogStanza];
        }
        messageID = [callLog elementForName:@"call_id"].stringValue;
    }
    
    NSString *messageBody = [[message elementForName:@"body"] stringValue];
    
    if (([messageBody length] == 0 || messageID.length == 0) && ![message isWebRTCCallWithCallLogInfo] && ![message hasOutOfBandData] && ![message isCallLogMessage] && ![message isUpdatedCallLogMessage] && ![message isChatMessageWithRecordingState]) {
        // Message has no body and no chat state.
        // Nothing to do with it.
        return;
    }
    XMPPJID *myJid = [self myJIDForXMPPStream:xmppStream];
   
    if ([message hasOutOfBandData]) {
        Message * aMessage = [self fetchMessagesForJID:myJid wtihMessageId:messageID xmmpStream:xmppStream];
        if (aMessage) {
            NSXMLElement *attachmentElement = nil;
            attachmentElement = [message elementForName:@"x"];
            if (attachmentElement) {
                if (!aMessage.attachment.isDownloadAvailable) {
                    NSXMLElement *statusElement = [NSXMLElement elementWithName:@"isDownloadAvailable" numberValue:[NSNumber numberWithBool:NO]];
                    [attachmentElement addChild:statusElement];
                }
            }
        }
    }
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [self messageEntity:moc];
        BOOL isOutgoingMessage;
        NSString *peerJid = nil;
        NSString *viaJid = nil;
        
        if ([message isGroupChatMessage]) {
            // For a MUC MAM message, we only have the "from" attribute
            // which looks like : "room_1234@domain.com/toto@domain.com/rc1"
            // we have to look at the toto@domain.com and see if it's me to know the direction of the message.
            XMPPJID *remote = [message from];
            if (remote.resource) {
                peerJid = [XMPPJID jidWithString:remote.resource].bare;
                isOutgoingMessage = [peerJid isEqualToString:myJid.bare];
            } else {
                // Message comes from the room itself !
                isOutgoingMessage = NO;
                peerJid = remote.bare;
            }
            viaJid = remote.bare;
        } else {
            if([message isWebRTCCallWithCallLogInfo]){
                NSArray *callLogs = [message elementsForName:@"call_log"];
                if([callLogs count] > 0){
                    NSXMLElement *callLog = callLogs[0];
                    NSString *caller = ((NSXMLElement*)[callLog elementsForName:@"caller"][0]).stringValue;
                    XMPPJID *callerJid = [XMPPJID jidWithString:caller];
                    NSString *callee = ((NSXMLElement*)[callLog elementsForName:@"callee"][0]).stringValue;
                    
                    BOOL isOutgoing = ([callerJid isEqualToJID:xmppStream.myJID options:XMPPJIDCompareBare]);
                    isOutgoingMessage = isOutgoing;
                    
                    peerJid = isOutgoing?callee:caller;
                    viaJid = isOutgoing?callee:caller;
                } else
                    isOutgoingMessage = NO;
            } else if(callLog){
                NSString *caller = [callLog elementForName:@"caller"].stringValue;
                NSString *callee = [callLog elementForName:@"callee"].stringValue;
                
                // We don't want to see/save the janusgateway call logs
                if([caller containsString:@"janusgateway"] || [callee containsString:@"janusgateway"] || [caller containsString:@"mp_"])
                    return;
                XMPPJID *callerJid = [XMPPJID jidWithString:caller];
                
                BOOL isOutgoing = ([callerJid isEqualToJID:xmppStream.myJID options:XMPPJIDCompareBare]);
                isOutgoingMessage = isOutgoing;
                
                peerJid = isOutgoing?callee:caller;
                viaJid = isOutgoing?callee:caller;
            } else {
                isOutgoingMessage = [[message from].bare isEqualToString:myJid.bare];
                
                // outgoing flag is set only for message that I sent so force this flag
                if(outgoing)
                    isOutgoingMessage = outgoing;
                
                XMPPJID *remote = isOutgoingMessage ? [message to] : [message from];
                peerJid = remote.bare;
                viaJid = remote.bare;
            }
        }
        [moc performBlockAndWait:^{
        // Fetch-n-Update OR Insert new message
        __block XMPPMessageArchiveManagement_Message_CoreDataObject *archivedMessage = [self messageWithID:messageID managedObjectContext:moc entity:entity];
        
            //        XMPPLogVerbose(@"Previous archivedMessage: %@", archivedMessage);
            
            BOOL didCreateNewArchivedMessage = NO;
            if (archivedMessage == nil) {
                archivedMessage = (XMPPMessageArchiveManagement_Message_CoreDataObject *) [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
                didCreateNewArchivedMessage = YES;
            }
            archivedMessage.message = message;
            archivedMessage.body = messageBody;
            
            archivedMessage.peerJid = peerJid;
            archivedMessage.viaJid = viaJid;
            
            archivedMessage.streamBareJidStr = xmppStream.myJID.bare;
            
            NSDate * timestampDate = [NSDate date];
            if ([message timestampMessageDate]) {
                timestampDate = [message timestampMessageDate];
            }
            else if([message archivedMessageDate]){
                timestampDate = [message archivedMessageDate];
            }

            NSDate *dateToUse = ([timestampDate compare:[NSDate date]])?timestampDate:[NSDate date];
            

            if(forwardedDelay)
                dateToUse = forwardedDelay;
            else {
                if([message wasDelayed])
                    dateToUse = [message delayedDeliveryDate];
            }
            
            archivedMessage.mamMessageId = mamMessageID;
            archivedMessage.timestamp = dateToUse;
            
            if(messageID.length > 0)
                archivedMessage.messageId = messageID;
            else
                archivedMessage.messageId = [NSString stringWithFormat:@"%ld", (long)([archivedMessage.timestamp timeIntervalSinceReferenceDate])];
            
            archivedMessage.thread = [[message elementForName:@"thread"] stringValue];
            archivedMessage.isOutgoing = isOutgoingMessage;
            archivedMessage.isCallLog = (callLog!=nil);
            //        XMPPLogVerbose(@"New archivedMessage: %@", archivedMessage);
            
            if (didCreateNewArchivedMessage) // [archivedMessage isInserted] doesn't seem to work
                {
                //            XMPPLogVerbose(@"Inserting message...");
                
                [archivedMessage willInsertObject];       // Override hook
                [self willInsertMessage:archivedMessage]; // Override hook
                [moc insertObject:archivedMessage];
                }
            else
                {
                //            XMPPLogVerbose(@"Updating message...");
                
                [archivedMessage didUpdateObject];       // Override hook
                [self didUpdateMessage:archivedMessage]; // Override hook
                }
        }];
    }];
}

-(Message *) fetchMessagesForJID:(XMPPJID *)jid wtihMessageId:(NSString *)messageId xmmpStream:(XMPPStream *) xmppStream {
    __block Message *returnedResult;
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO];
    
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [self messageEntity:moc];
        [moc performBlockAndWait:^{
            // jid is the *VIA* jid !
            // If we have messages exchanged in a room, _via_ will be the room and _peer_ the "real" remote person talking.
            // Here we want the messages exchanged in the room.
            NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"messageId == %@",messageId];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            fetchRequest.entity = entity;
            fetchRequest.predicate = predicate;
            fetchRequest.includesPendingChanges = YES;
            fetchRequest.sortDescriptors = @[sort];
            
            NSError *error = nil;
            NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
            if(!error){
                // Messages objects are not allowed in outside of the moc so transform them
                for (XMPPMessageArchiveManagement_Message_CoreDataObject *aMessage in results) {
                    //
                    //                    aMessage.timestamp = [NSDate date];
                    //                    [aMessage didUpdateObject];
                    returnedResult = [self messageFromMamXmppMessage:aMessage];
                    
                }
            } else
                XMPPLogError(@"Error while executing fetch request %@", error);
        }];
    }];
    return returnedResult;
}

- (NSArray<Message*> *)fetchLastMessagesForJID:(XMPPJID *)jid xmmpStream:(XMPPStream *) xmppStream {
    NSMutableArray<Message*> *returnedResults = [NSMutableArray new];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO];
   
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [self messageEntity:moc];
        [moc performBlockAndWait:^{
            // jid is the *VIA* jid !
            // If we have messages exchanged in a room, _via_ will be the room and _peer_ the "real" remote person talking.
            // Here we want the messages exchanged in the room.
            NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"viaJid == %@ && isCallLog == NO && NOT (%K CONTAINS %@)", [jid bare],@"messageStr",@"isDownloadAvailable"];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            fetchRequest.entity = entity;
            fetchRequest.predicate = predicate;
            fetchRequest.includesPendingChanges = YES;
            fetchRequest.sortDescriptors = @[sort];
            fetchRequest.fetchOffset = 0;
            fetchRequest.fetchBatchSize = 1;
            fetchRequest.fetchLimit = 1;
            NSError *error = nil;
            NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
            if(!error){
                // Messages objects are not allowed in outside of the moc so transform them
                for (XMPPMessageArchiveManagement_Message_CoreDataObject *aMessage in results) {
                    Message *theMessage = [self messageFromMamXmppMessage:aMessage];
                    if(![returnedResults containsObject:theMessage])
                        [returnedResults addObject:theMessage];
                }
            } else
                XMPPLogError(@"Error while executing fetch request %@", error);
        }];
    }];
    return returnedResults;
}

- (NSArray<Message*> *)fetchMessagesForJID:(XMPPJID *)jid xmmpStream:(XMPPStream *) xmppStream maxSize:(NSNumber *) maxSize offset:(NSNumber *) offset {
    NSMutableArray<Message*> *returnedResults = [NSMutableArray new];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO];
    NSInteger offsetInt = [offset integerValue];
    NSInteger maxSizeInt = [maxSize integerValue];
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [self messageEntity:moc];
        [moc performBlockAndWait:^{
            // jid is the *VIA* jid !
            // If we have messages exchanged in a room, _via_ will be the room and _peer_ the "real" remote person talking.
            // Here we want the messages exchanged in the room.
            NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"viaJid == %@ && isCallLog == NO", [jid bare]];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            fetchRequest.entity = entity;
            fetchRequest.predicate = predicate;
            fetchRequest.includesPendingChanges = YES;
            fetchRequest.sortDescriptors = @[sort];
            fetchRequest.fetchOffset = offsetInt;
            fetchRequest.fetchBatchSize = maxSizeInt;
            fetchRequest.fetchLimit = maxSizeInt;
            NSError *error = nil;
            NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
            if(!error){
                // Messages objects are not allowed in outside of the moc so transform them
                for (XMPPMessageArchiveManagement_Message_CoreDataObject *aMessage in results) {
                    Message *theMessage = [self messageFromMamXmppMessage:aMessage];
                    if(![returnedResults containsObject:theMessage])
                        [returnedResults addObject:theMessage];
                }
            } else
                XMPPLogError(@"Error while executing fetch request %@", error);
        }];
    }];
    return returnedResults;
}

-(Message *) messageFromMamXmppMessage:(XMPPMessageArchiveManagement_Message_CoreDataObject *) aMessage {
    XMPPMessage *message = aMessage.message;
    Message *theMessage = [_xmppService messageFromXmppMessage:message];
    theMessage.timestamp = aMessage.timestamp;
    theMessage.mamMessageID = aMessage.mamMessageId;
    
    // We check if we have a peer and a via, if not determine it with informations from mam message.
    if(!theMessage.peer && !theMessage.via){
        theMessage.isOutgoing = aMessage.isOutgoing;
        if(![aMessage.message isWebRTCCallWithCallLogInfo]){
            if (aMessage.isOutgoing) {
                // Some message from room can have a nil jid (no to for exemple), so we will assume that the to it's me.
                if(aMessage.message.to){
                    theMessage.peer = [_xmppService peerForJID:aMessage.message.to];
                    theMessage.via = [_xmppService viaForJID:aMessage.message.to];
                } else {
                    theMessage.peer = [_xmppService peerForJID:aMessage.message.from];
                    theMessage.via = [_xmppService viaForJID:aMessage.message.from];
                }
            } else {
                theMessage.peer = [_xmppService peerForJID:aMessage.message.from];
                theMessage.via = [_xmppService viaForJID:aMessage.message.from];
            }
        }
    }
    
    if([message isArchivedMessageSent]){
        [theMessage setDeliveryState:MessageDeliveryStateSent atTimestamp:theMessage.timestamp];
    }
    
  
    if([message isArchivedMessageDelivered]){
        [theMessage setDeliveryState:MessageDeliveryStateDelivered atTimestamp:[message archivedMessageDeliveredDate]];
    }
    else {
    if ([message isArchivedMessageSent])
        [theMessage setDeliveryState:MessageDeliveryStateDelivered atTimestamp:theMessage.timestamp];
}
    
    if ([message isArchivedMessageReceived]) {
        NSDate *timestamp = [message archivedMessageReceivedDate];
        [theMessage setDeliveryState:MessageDeliveryStateReceived atTimestamp:timestamp];
    }
    // It's possible that the message is read but not received !!!
    // <ack received='false' read='true' read_timestamp='2018-01-03T09:04:59.230031Z'/>
    // how it is possible, I don't known !!!
    // In that case we will use the message date as delivered date, that is just horrible !
    if([message isArchivedMessageRead] && ![message isArchivedMessageReceived]){
        [theMessage setDeliveryState:MessageDeliveryStateDelivered atTimestamp:aMessage.timestamp];
    }
    
    if ([message isArchivedMessageRead]) {
        [theMessage setDeliveryState:MessageDeliveryStateRead atTimestamp:[message archivedMessageReadDate]];
    }
    
    return theMessage;
}

- (NSArray <XMPPMessage *> *)deleteCachedMessageNotInPageData:(PageData*) pageData xmmpStream:(XMPPStream *) xmppStream {
    
    if (!pageData.to) {
        return nil;
    }
    NSMutableArray *array = [NSMutableArray array];
    
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [self messageEntity:moc];
        [moc performBlockAndWait:^{
            NSPredicate *predicate;
            // from can be null.
            if (pageData.from) {
                // Change the predicate to a strict limit only for the pageData.to, to exclude messages that can be on the previous page and not in the pageData.
                predicate = [NSPredicate predicateWithFormat:@"viaJid == %@ AND timestamp >= %@ AND timestamp < %@", pageData.fromJid, pageData.from, pageData.to];
            } else {
                predicate = [NSPredicate predicateWithFormat:@"viaJid == %@ AND timestamp < %@",pageData.fromJid, pageData.to];
            }
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            fetchRequest.entity = entity;
            fetchRequest.predicate = predicate;
            fetchRequest.includesPendingChanges = YES;
            NSError *error = nil;
            NSArray *messages = [moc executeFetchRequest:fetchRequest error:&error];
            
            NSLog(@"Number of messages found in cache = %lu, count in page data %lu", [messages count], pageData.messageIDs.count);
            NSLog(@"Looking if all messages still exist server side");
            
            for (XMPPMessageArchiveManagement_Message_CoreDataObject *message in messages) {
                if (![pageData.messageIDs containsObject:message.messageId]) {
                    NSLog(@"Remove from cache message : %@", message.messageId);
                    [moc deleteObject:message];
                    [array addObject:message.message];
                }
            }
        }];
    }];
    
    return array;
}


- (void)deleteAllMessagesWithPeerJID:(NSString *) jid xmmpStream:(XMPPStream *) xmppStream {
    if (!jid) {
        return;
    }
    
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        
        NSEntityDescription *entity = [self messageEntity:moc];
        NSPredicate *predicate;
        predicate = [NSPredicate predicateWithFormat:@"viaJid == %@", jid];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = entity;
        fetchRequest.predicate = predicate;
        fetchRequest.includesPendingChanges = YES;
        NSError *error = nil;
        NSArray *messages = [moc executeFetchRequest:fetchRequest error:&error];
        
        NSLog(@"Deleting all messages with %@ in our CoreData", jid);
        
        for (XMPPMessageArchiveManagement_Message_CoreDataObject *message in messages)
            {
            [moc deleteObject:message];
            }
    }];
}

- (void)updateFileWithURL:(NSString *) url  withFileStatus:(BOOL) isDownloadAvailable {
    if (!url) {
        return;
    }
    
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [self messageEntity:moc];
        __block NSError *error = nil;
        [moc performBlockAndWait:^{
            NSPredicate *predicate;
            predicate = [NSPredicate predicateWithFormat:@"messageStr CONTAINS[cd] %@", url];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            fetchRequest.entity = entity;
            fetchRequest.predicate = predicate;
            fetchRequest.includesPendingChanges = YES;
            
            NSArray *messages = [moc executeFetchRequest:fetchRequest error:&error];
            
            for (XMPPMessageArchiveManagement_Message_CoreDataObject *message in messages) {
                NSXMLElement *attachmentElement = nil;
                attachmentElement = [message.message elementForName:@"x"];
            
                if (attachmentElement) {
                    
                    if (!isDownloadAvailable) {
                         NSXMLElement *statusElement = [NSXMLElement elementWithName:@"isDownloadAvailable" numberValue:[NSNumber numberWithBool:NO]];
                        [attachmentElement addChild:statusElement];
                        message.messageStr = [message.message compactXMLString];
                        NSLog(@"archive message updated for file %@ with isDownloadAvailable  NO",url);
                            
                    }
                }
            }
            [moc save:&error];
        }];
    }];
}

- (void)updateMessageID:(NSString *) messageID  atTimeStamp:(NSString *) timestamp {
    if (!messageID || !timestamp) {
        return;
    }
    
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [self messageEntity:moc];
        __block NSError *error = nil;
        [moc performBlockAndWait:^{
            NSPredicate *predicate;
            predicate = [NSPredicate predicateWithFormat:@"messageId == %@", messageID];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            fetchRequest.entity = entity;
            fetchRequest.predicate = predicate;
            fetchRequest.includesPendingChanges = YES;
            
            NSArray *messages = [moc executeFetchRequest:fetchRequest error:&error];
            
            for (XMPPMessageArchiveManagement_Message_CoreDataObject *message in messages) {
                NSXMLElement *retransmissionElement = nil;
                retransmissionElement = [message.message elementForName:@"retransmission"];
                
                if (retransmissionElement) {
                    if ([retransmissionElement attributeForName:@"stamp"]) {
                        [retransmissionElement removeAttributeForName:@"stamp"];
                        
                        if (timestamp) {
                            [retransmissionElement addAttributeWithName:@"stamp" stringValue:timestamp];
                            
                        }
                    }
                }

                
                NSXMLElement *timestampElement = nil;
                timestampElement = [message.message elementForName:@"timestamp"];
                
                if (!timestampElement) {
                    timestampElement = [NSXMLElement elementWithName:@"timestamp"];
                    [message.message addChild:timestampElement];
                }
                
                if ([timestampElement attributeForName:@"value"]) {
                    [timestampElement removeAttributeForName:@"value"];
                }
                
                if (timestamp) {
                    [timestampElement addAttributeWithName:@"value" stringValue:timestamp];
                }

                message.timestamp = [message.message getTimeFromString:timestamp];
                message.messageStr = [message.message compactXMLString];
                NSLog(@"archive message updated for message %@ with date %@",message.messageStr,message.timestamp);
                
                 
            }
            [moc save:&error];
        }];
    }];
}

- (void)deleteMessageID:(NSString *) messageID withPeerJID:(NSString *) jid xmmpStream:(XMPPStream *) xmppStream{
    if (!jid || !messageID) {
        return;
    }
    
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        
        NSEntityDescription *entity = [self messageEntity:moc];
        NSPredicate *predicate;
        predicate = [NSPredicate predicateWithFormat:@"messageId == %@ AND viaJid == %@", messageID, jid];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = entity;
        fetchRequest.predicate = predicate;
        fetchRequest.includesPendingChanges = YES;
        NSError *error = nil;
        NSArray *messages = [moc executeFetchRequest:fetchRequest error:&error];
        
        NSLog(@"Deleting messageID %@ with %@ in our CoreData", messageID, jid);
        
        for (XMPPMessageArchiveManagement_Message_CoreDataObject *message in messages)
            {
            [moc deleteObject:message];
            }
    }];
}

- (NSDate *)getMessageDateForMamMessageId:(NSString *) messageID {
    __block NSDate *date = nil;
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [self messageEntity:moc];
        [moc performBlockAndWait:^{
            NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"mamMessageId == %@", messageID];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            fetchRequest.entity = entity;
            fetchRequest.predicate = predicate;
            fetchRequest.includesPendingChanges = YES;
            fetchRequest.fetchLimit = 1;
            
            NSError *error = nil;
            NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
            if(!error && [results count] > 0) {
                XMPPMessageArchiveManagement_Message_CoreDataObject *message = results[0];
                date = message.timestamp;
            } else
                XMPPLogError(@"Error while executing fetch request %@", error);
        }];
    }];
    return date;
}


- (void)updateMessageID:(NSString *) messageID deliveryState:(NSString *) state at:(NSString *) timestamp {
    if (!messageID || !state) {
        return;
    }
    
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [self messageEntity:moc];
        __block NSError *error = nil;
        [moc performBlockAndWait:^{
            NSPredicate *predicate;
            predicate = [NSPredicate predicateWithFormat:@"messageId == %@", messageID];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            fetchRequest.entity = entity;
            fetchRequest.predicate = predicate;
            fetchRequest.includesPendingChanges = YES;
            
            NSArray *messages = [moc executeFetchRequest:fetchRequest error:&error];
            
            NSLog(@"Updating messageID %@ with delivery status %@ at %@", messageID, state, timestamp);
            
            for (XMPPMessageArchiveManagement_Message_CoreDataObject *message in messages) {
                NSXMLElement *ackElement = nil;
                if(message.isCallLog){
                    NSXMLElement *callLogElement = [message.message callLogStanza];
                    if(!callLogElement && [message.message isUpdatedCallLogMessage]){
                        callLogElement = [message.message updatedCallLogStanza];
                    }
                    ackElement = [callLogElement elementForName:@"ack"];
                } else
                    ackElement = [message.message elementForName:@"ack"];
                
                if (!ackElement) {
                    ackElement = [NSXMLElement elementWithName:@"ack"];
                    [message.message addChild:ackElement];
                }
                if([state isEqualToString:@"delivered"]){
                    [ackElement addAttributeWithName:@"delivered" stringValue:@"true"];
                    if (timestamp) {
                        [ackElement addAttributeWithName:@"delivered_timestamp" stringValue:timestamp];
                    }
                } else if ([state isEqualToString:@"received"]) {
                    [ackElement addAttributeWithName:@"received" stringValue:@"true"];
                    if (timestamp) {
                        [ackElement addAttributeWithName:@"recv_timestamp" stringValue:timestamp];
                    }
                } else if ([state isEqualToString:@"read"]) {
                    [ackElement addAttributeWithName:@"read" stringValue:@"true"];
                    if (timestamp) {
                        [ackElement addAttributeWithName:@"read_timestamp" stringValue:timestamp];
                    }
                }
                // make sure to erase the xml value to be sure
                // it will be stored in CoreData
                message.messageStr = [message.message compactXMLString];
            }
            [moc save:&error];
        }];
    }];
}

#pragma mark - CallLogs
-(NSArray<CallLog *> *) fetchCallLogsForJid:(XMPPJID *)jid xmppStream:(XMPPStream *) xmppStream {
    NSMutableArray *returnedResults = [NSMutableArray new];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO];
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [self messageEntity:moc];
        [moc performBlockAndWait:^{
            NSPredicate *predicate = nil;
            if([jid isEqualToJID:xmppStream.myJID options:XMPPJIDCompareBare])
                predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr == %@ AND isCallLog == YES", jid.bare];
            else
                predicate = [NSPredicate predicateWithFormat:@"viaJid == %@ AND isCallLog == YES", jid.bare];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            fetchRequest.entity = entity;
            fetchRequest.predicate = predicate;
            fetchRequest.includesPendingChanges = YES;
            fetchRequest.sortDescriptors = @[sort];
            NSError *error = nil;
            NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
            if(!error) {
                for (XMPPMessageArchiveManagement_Message_CoreDataObject *aMessage in results) {
                    CallLog *theCallLog = [self callLogFromMamMessage:aMessage];
                    if(theCallLog){
                        if(![returnedResults containsObject:theCallLog])
                            [returnedResults addObject:theCallLog];
                    }
                }
            }
            else
                XMPPLogError(@"Error while executing fetch request %@", error);
        }];
    }];
    return returnedResults;
}

-(CallLog *) callLogFromMamMessage:(XMPPMessageArchiveManagement_Message_CoreDataObject *) aMessage {
    XMPPMessage *theMessage = aMessage.message;
    CallLog *theCallLog = nil;
    NSXMLElement *callLog = nil;
    NSDate *forwardedDelay = nil;
    if([theMessage isCallLogMessage]){
        forwardedDelay = [theMessage callLogForwardedDate];
        callLog = [theMessage callLogStanza];
    } else if ([theMessage isUpdatedCallLogMessage]) {
        callLog = [theMessage updatedCallLogStanza];
        forwardedDelay = [[theMessage updatedCallLogForwardedStanza] delayedDeliveryDate];
    }
    if(callLog){
        theCallLog = [_xmppService callLogFromCallLogStanza:callLog];
        theCallLog.date = forwardedDelay;
        theCallLog.isOutgoing = aMessage.isOutgoing;
    }
    
    return theCallLog;
}

-(void) deleteCachedCallLogsNotInPageData:(PageData *)pageData xmmpStream:(XMPPStream *)xmppStream {
    if (!pageData.to) {
        return;
    }
    
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        NSEntityDescription *entity = [self messageEntity:moc];
        [moc performBlockAndWait:^{
            //        NSPredicate *predicate;
            //
            //        predicate = [NSPredicate predicateWithFormat:@"isCallLog == YES AND streamBareJidStr == %@",pageData.fromJid];
            XMPPJID *jid = [XMPPJID jidWithString:pageData.fromJid];
            NSPredicate *predicate = nil;
            if([jid isEqualToJID:xmppStream.myJID options:XMPPJIDCompareBare])
                predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr == %@ AND isCallLog == YES", jid.bare];
            else
                predicate = [NSPredicate predicateWithFormat:@"viaJid == %@ AND isCallLog == YES", jid.bare];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            fetchRequest.entity = entity;
            fetchRequest.predicate = predicate;
            fetchRequest.includesPendingChanges = YES;
            NSError *error = nil;
            NSArray *messages = [moc executeFetchRequest:fetchRequest error:&error];
            
#if DEBUG
            NSLog(@"Number of messages found in cache = %lu, count in page data %lu", [messages count], pageData.messageIDs.count);
            NSLog(@"Looking if all messages still exist server side");
#endif
            
            for (XMPPMessageArchiveManagement_Message_CoreDataObject *message in messages) {
                if (![pageData.messageIDs containsObject:message.messageId]) {
#if DEBUG
                    NSLog(@"Remove from cache message : %@", message.messageId);
#endif
                    [moc deleteObject:message];
                }
            }
        }];
    }];
}

- (void)deleteAllCallLogWithPeerJID:(NSString *) jid xmmpStream:(XMPPStream *) xmppStream {
    if (!jid) {
        return;
    }
    
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        
        NSEntityDescription *entity = [self messageEntity:moc];
        NSPredicate *predicate;
        predicate = [NSPredicate predicateWithFormat:@"viaJid == %@  AND isCallLog == YES", jid];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = entity;
        fetchRequest.predicate = predicate;
        fetchRequest.includesPendingChanges = YES;
        NSError *error = nil;
        NSArray *messages = [moc executeFetchRequest:fetchRequest error:&error];
        
        NSLog(@"Deleting all call logs with %@ in our CoreData", jid);
        
        for (XMPPMessageArchiveManagement_Message_CoreDataObject *message in messages)
            {
            [moc deleteObject:message];
            }
    }];
}

-(void) deleteAllCallLogsForXmppStream:(XMPPStream *) xmppStream {
    [self executeBlock:^{
        NSManagedObjectContext *moc = [self managedObjectContext];
        
        NSEntityDescription *entity = [self messageEntity:moc];
        NSPredicate *predicate;
        predicate = [NSPredicate predicateWithFormat:@"isCallLog == YES"];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = entity;
        fetchRequest.predicate = predicate;
        fetchRequest.includesPendingChanges = YES;
        NSError *error = nil;
        NSArray *messages = [moc executeFetchRequest:fetchRequest error:&error];
        
        NSLog(@"Deleting all call logs in our CoreData");
        
        for (XMPPMessageArchiveManagement_Message_CoreDataObject *message in messages){
            [moc deleteObject:message];
        }
    }];
}

@end
