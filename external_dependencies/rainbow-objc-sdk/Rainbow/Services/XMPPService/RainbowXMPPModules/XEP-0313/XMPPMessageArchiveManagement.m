 /*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPMessageArchiveManagement.h"
#import "XMPPFramework.h"
#import "XMPPLogging.h"
#import "NSNumber+XMPP.h"
#import "XMPPMessageArchiveManagementCoreDataStorage.h"
#import "NSDate+XMPPDateTimeProfiles.h"
#import "XMPPConstants.h"
#import "NSXMLElement+XEP_0297.h"
#import "NSXMLElement+XEP_0203.h"
#import "XMPPMessage+XEP_0313.h"
#import "XMPPMessage+XEP_0280.h"
#import "XMPPMessage+XEP_0184.h"
#import "XMPPMessage+XEP0045.h"
#import "Contact.h"
#import "Room.h"
#import "XMPPMessage+RainbowCustomMessageTypes.h"
#import "XMPPMessage+XEP_313_CallLogs.h"
#import "XMPPIDTracker.h"

#if ! __has_feature(objc_arc)
#warning This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

// Log levels: off, error, warn, info, verbose
// Log flags: trace
#if DEBUG
static const int xmppLogLevel = XMPP_LOG_LEVEL_VERBOSE; // | XMPP_LOG_FLAG_TRACE;
#else
static const int xmppLogLevel = XMPP_LOG_LEVEL_WARN;
#endif


@implementation PageData
-(instancetype) init {
    if (self = [super init]) {
        _messageIDs = [NSMutableSet new];
    }
    return self;
}
-(void) dealloc {
    [_messageIDs removeAllObjects];
    _messageIDs = nil;
}
@end

@interface XMPPMessageArchiveManagement ()
@property (nonatomic, strong) NSMutableDictionary *messageIDsFromServer;
@property (nonatomic, strong) XMPPIDTracker *xmppIDTracker;
@end

@implementation XMPPMessageArchiveManagement
@synthesize xmppMessageArchiveManagementStorage = _xmppMessageArchiveManagementStorage;

- (id)init {
    // This will cause a crash - it's designed to.
    // Only the init methods listed in XMPPMessageArchiveManagement.h are supported.
    
    return [self initWithMessageArchiveManagementStorage:nil dispatchQueue:NULL];
}

- (id)initWithDispatchQueue:(dispatch_queue_t)queue {
    // This will cause a crash - it's designed to.
    // Only the init methods listed in XMPPMessageArchiveManagement.h are supported.
    
    return [self initWithMessageArchiveManagementStorage:nil dispatchQueue:queue];
}

- (id)initWithMessageArchiveManagementStorage:(id <XMPPMessageArchiveManagementStorage>)storage {
    return [self initWithMessageArchiveManagementStorage:storage dispatchQueue:NULL];
}

- (id)initWithMessageArchiveManagementStorage:(id <XMPPMessageArchiveManagementStorage>)storage dispatchQueue:(dispatch_queue_t)queue {
    NSParameterAssert(storage != nil);
    
    if ((self = [super initWithDispatchQueue:queue])) {
        if ([storage configureWithParent:self queue:moduleQueue]) {
            xmppMessageArchiveManagementStorage = storage;
        } else {
            XMPPLogError(@"%@: %@ - Unable to configure storage!", THIS_FILE, THIS_METHOD);
        }
        
        NSXMLElement *pref = [NSXMLElement elementWithName:@"prefs" xmlns:XMLNS_XMPP_ARCHIVE];
        
        preferences = pref;
        
        _messageIDsFromServer = [NSMutableDictionary new];
        
    }
    return self;
}

- (BOOL)activate:(XMPPStream *)aXmppStream {
    XMPPLogTrace();
    
    if ([super activate:aXmppStream]) {
        XMPPLogVerbose(@"%@: Activated", THIS_FILE);
        
        // Reserved for future potential use
        _xmppIDTracker = [[XMPPIDTracker alloc] initWithStream:xmppStream dispatchQueue:moduleQueue];
        
        return YES;
    }
    
    return NO;
}

- (void)deactivate {
    XMPPLogTrace();
    
    // Reserved for future potential use
    dispatch_block_t block = ^{ @autoreleasepool {
        
        [_xmppIDTracker removeAllIDs];
        _xmppIDTracker = nil;
        
    }};
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_sync(moduleQueue, block);
    [super deactivate];
}

-(void) dealloc {
    [_messageIDsFromServer removeAllObjects];
    _messageIDsFromServer = nil;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Properties
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (id <XMPPMessageArchiveManagementStorage>)xmppMessageArchivingStorage {
    // Note: The XMPPMessageArchiveManagementStorage variable is read-only (set in the init method)
    return xmppMessageArchiveManagementStorage;
}

- (BOOL)clientSideMessageArchivingOnly {
    __block BOOL result = NO;
    
    dispatch_block_t block = ^{
        result = clientSideMessageArchivingOnly;
    };
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_sync(moduleQueue, block);
    
    return result;
}

- (void)setClientSideMessageArchivingOnly:(BOOL)flag {
    dispatch_block_t block = ^{
        clientSideMessageArchivingOnly = flag;
    };
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

- (NSXMLElement *)preferences {
    __block NSXMLElement *result = nil;
    
    dispatch_block_t block = ^{
        
        result = [preferences copy];
    };
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_sync(moduleQueue, block);
    
    return result;
}

- (void)setPreferences:(NSXMLElement *)newPreferences {
    dispatch_block_t block = ^{ @autoreleasepool {
        
        // Update cached value
        
        preferences = [newPreferences copy];
        
        // Update storage
        
        if ([xmppMessageArchiveManagementStorage respondsToSelector:@selector(setPreferences:forUser:)]) {
            XMPPJID *myBareJid = [[xmppStream myJID] bareJID];
            
            [xmppMessageArchiveManagementStorage setPreferences:preferences forUser:myBareJid];
        }
        
        // Todo:
        //
        //  - Send new pref to server (if changed)
    }};
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}


- (void) fetchArchivedMessagesWithPeer:(Peer *) peer withUUID:(NSString *) UUID maxSize:(NSNumber *) maxSize beforeDate:(NSDate *) beforeDate lastMessageID:(NSString *) lastMessageID {
    
    dispatch_block_t block = ^{
        @autoreleasepool {
            //    <iq type="set" id="benjamin.zores.otpc@opentouchcloud.org" xmlns="jabber:client">
            //        <query xmlns="urn:xmpp:mam:1" queryid="benjamin.zores.otpc@opentouchcloud.org">
            //            <x xmlns="jabber:x:data" type="submit">
            //                <field var="FORM_TYPE" type="hidden">
            //                    <value>urn:xmpp:mam:1</value>
            //                </field>
            //                <field var="with">
            //                    <value>benjamin.zores.otpc@opentouchcloud.org</value>
            //                </field>
            //            </x>
            //            <set xmlns="http://jabber.org/protocol/rsm">
            //                <max>30</max>
            //                <before>2015-11-04T10:22:58Z</before>
            //            </set>
            //        </query>
            //    </iq>
            
            /*
             * Custom implementation for MUC+MAM : It's the complete opposite of the standard behavior...
             * We have to send the request IQ like :
             
             <iq ... to="room_jid@domain.com" ...>
             ...
             NO <field var="with"/> markup !
             ...
             </iq>
             */
            
            [multicastDelegate xmppMessageArchiveManagement:self didBeginFetchingArchiveForUUID:UUID];
            
            // For each MAM request, we save all the [messageID, timestamp] we will receive.
            NSAssert(!beforeDate, @"Must NOT have beforeDate");
            
            PageData *pageData = [PageData new];
            if (lastMessageID) {
                pageData.to = [xmppMessageArchiveManagementStorage getMessageDateForMamMessageId:lastMessageID];
                //        NSAssert(pageData.to, @"Shoud know the message !");
            } else {
                // before now.
                pageData.to = [NSDate date];
            }
            
            pageData.fromJid = peer.jid;
            
            XMPPJID *toIQ = nil;
            if ([peer isKindOfClass:[Room class]]) {
                // For MUC + MAM, the room JID is passed in the "to" of the IQ..
                toIQ = [XMPPJID jidWithString:peer.jid];
            }
            
            [_messageIDsFromServer setObject:pageData forKey:UUID];
            
            NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:XMLNS_XMPP_ARCHIVE_BULK];
            [query addAttributeWithName:@"queryid" stringValue:UUID];
            
            NSXMLElement *x = [NSXMLElement elementWithName:@"x" xmlns:@"jabber:x:data"];
            [x addAttributeWithName:@"type" stringValue:@"submit"];
            
            NSXMLElement *field = [NSXMLElement elementWithName:@"field"];
            [field addAttributeWithName:@"var" stringValue:@"FORM_TYPE"];
            [field addAttributeWithName:@"type" stringValue:@"hidden"];
            NSXMLElement *value = [NSXMLElement elementWithName:@"value" stringValue:XMLNS_XMPP_ARCHIVE];
            [field addChild:value];
            
            [x addChild:field];
            
            NSXMLElement *field2 = [NSXMLElement elementWithName:@"field"];
            [field2 addAttributeWithName:@"var" stringValue:@"with"];
            NSXMLElement *value2 = [NSXMLElement elementWithName:@"value"];
            if ([peer isKindOfClass:[Room class]]) {
                [value2 setStringValue:xmppStream.myJID.bare];
            } else {
                [value2 setStringValue:peer.jid];
            }
            [field2 addChild:value2];
            [x addChild:field2];
            
            NSXMLElement *set = [NSXMLElement elementWithName:@"set" xmlns:@"http://jabber.org/protocol/rsm"];
            NSXMLElement *max = [NSXMLElement elementWithName:@"max" numberValue:maxSize];
            NSXMLElement *beforeElement = [NSXMLElement elementWithName:@"before"];
            // http://xmpp.org/extensions/xep-0059.html
            // 2.5 Requesting the Last Page in a Result Set
            //    The requesting entity MAY ask for the last page in a result set by including in its request an empty <before/> element.
            if(lastMessageID.length > 0)
                [beforeElement setStringValue:lastMessageID];
            
            [set addChild:max];
            [set addChild:beforeElement];
            
            [query addChild:x];
            [query addChild:set];
            
            XMPPIQ *iq = [XMPPIQ iqWithType:@"set" to:toIQ elementID:UUID child:query];
            [iq setXmlns:@"jabber:client"];
            
            NSLog(@"FETCH MAM ARCHIVE WITH IQ %@", iq);
            [_xmppIDTracker addElement:iq target:self selector:@selector(handleFetchMamQueryIQ:withInfo:) timeout:5];
            [xmppStream sendElement:iq];
        }
    };
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

-(void) deleteAllMessagesWithPeer:(Peer *) peer withUUID:(NSString *) UUID {
    /*
    <iq type='set' id='1343dc62254f46349e537a51f35d03ee@openrainbow.net' xmlns='jabber:client'>
        <delete xmlns='urn:xmpp:mam:0' deleteid='remove_1343dc62254f46349e537a51f35d03ee@openrainbow.net'>
            <x xmlns='jabber:x:data' type='submit'>
                <field var='FORM_TYPE' type='hidden'>
                    <value>urn:xmpp:mam:0</value>
                </field>
                <field var='with'>
                    <value>1343dc62254f46349e537a51f35d03ee@openrainbow.net</value>
                </field>
            </x>
            <set xmlns='http://jabber.org/protocol/rsm'>
                <before>2016-06-21T08:27:22Z</before>
            </set>
        </delete>
     </iq>
     */
    
    dispatch_block_t block = ^{
        @autoreleasepool {
            
            PageData *pageData = [PageData new];
            pageData.to = [NSDate date];
            pageData.fromJid = peer.jid;
            
            NSArray<Message *> *messagesForJid = [xmppMessageArchiveManagementStorage fetchMessagesForJID:[XMPPJID jidWithString:peer.jid] xmmpStream:xmppStream maxSize:[NSNumber numberWithInteger:NSIntegerMax] offset:[NSNumber numberWithInteger:0]];
            for (Message *msg in messagesForJid) {
                [pageData.messageIDs addObject:msg.messageID];
            }
            
            [_messageIDsFromServer setObject:pageData forKey:UUID];
            
            NSXMLElement *query = [NSXMLElement elementWithName:@"delete" xmlns:XMLNS_XMPP_ARCHIVE];
            [query addAttributeWithName:@"deleteid" stringValue:UUID];
            
            NSXMLElement *x = [NSXMLElement elementWithName:@"x" xmlns:@"jabber:x:data"];
            [x addAttributeWithName:@"type" stringValue:@"submit"];
            
            NSXMLElement *field = [NSXMLElement elementWithName:@"field"];
            [field addAttributeWithName:@"var" stringValue:@"FORM_TYPE"];
            [field addAttributeWithName:@"type" stringValue:@"hidden"];
            NSXMLElement *value = [NSXMLElement elementWithName:@"value" stringValue:XMLNS_XMPP_ARCHIVE];
            [field addChild:value];
            
            NSXMLElement *field2 = [NSXMLElement elementWithName:@"field"];
            [field2 addAttributeWithName:@"var" stringValue:@"with"];
            
            NSXMLElement *value2 = [NSXMLElement elementWithName:@"value"];
            if ([peer isKindOfClass:[Room class]]) {
                [value2 setStringValue:xmppStream.myJID.bare];
            } else {
                [value2 setStringValue:peer.jid];
            }
            [field2 addChild:value2];
            
            [x addChild:field];
            [x addChild:field2];
            
            NSXMLElement *set = [NSXMLElement elementWithName:@"set" xmlns:@"http://jabber.org/protocol/rsm"];
            NSXMLElement *beforeElement = [NSXMLElement elementWithName:@"before"];
            [set addChild:beforeElement];
            
            [query addChild:x];
            [query addChild:set];
            
            XMPPJID *toIQ = nil;
            if ([peer isKindOfClass:[Room class]]) {
                // For MUC + MAM, the room JID is passed in the "to" of the IQ..
                toIQ = [XMPPJID jidWithString:peer.jid];
            }
            
            XMPPIQ *deleteIQ = [XMPPIQ iqWithType:@"set" to:toIQ elementID:UUID child:query];
            [deleteIQ setXmlns:@"jabber:client"];
            
            NSLog(@"DELETE ALL MESSAGE WITH IQ %@", deleteIQ);
            [_xmppIDTracker addElement:deleteIQ target:self selector:@selector(handleDeleteAllMessagesQueryIQ:withInfo:) timeout:60];
            [xmppStream sendElement:deleteIQ];
        }
    };
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

#pragma mark XMPPIDTracker
-(void)handleFetchMamQueryIQ:(XMPPIQ *)iq withInfo:(XMPPBasicTrackingInfo *)basicTrackingInfo {
    dispatch_block_t block = ^{
        @autoreleasepool {
            NSXMLElement *fin = [iq elementForName:@"fin" xmlns:XMLNS_XMPP_ARCHIVE];
            // Check if it's a bulk mam
            if(!fin)
                fin = [iq elementForName:@"fin" xmlns:XMLNS_XMPP_ARCHIVE_BULK];
            if(fin){
                NSLog(@"End of request mam %@", fin);
                BOOL isDeleteRequestAnswer = NO;
                NSString *uuid = nil;
                BOOL isComplete = NO;
                NSMutableDictionary *answered = nil;
                if([fin attributeForName:@"queryid"]){
                    uuid = [fin attributeForName:@"queryid"].stringValue;
                    isComplete = [[fin attributeForName:@"complete"].stringValue isEqualToString:@"true"];
                    NSXMLElement *set = [fin elementForName:@"set" xmlns:@"http://jabber.org/protocol/rsm"];
                    NSXMLElement *firstElement = [set elementForName:@"first"];
                    NSXMLElement *lastElement = [set elementForName:@"last"];
                    NSXMLElement *countElement = [set elementForName:@"count"];
                    
                    answered = [[NSMutableDictionary alloc] initWithDictionary:@{@"requestID": uuid}];
                    
                    if(countElement)
                        [answered addEntriesFromDictionary:@{@"count" : countElement.stringValue}];
                    
                    [answered addEntriesFromDictionary:@{@"complete":[NSNumber numberWithBool:isComplete]}];
                    if(firstElement && lastElement)
                        [answered addEntriesFromDictionary:@{@"first": firstElement.stringValue, @"last": lastElement.stringValue}];
                    
                }
                
                if([fin attributeForName:@"deleteid"]){
                    isComplete = YES;
                    isDeleteRequestAnswer = YES;
                    uuid = [fin attributeForName:@"deleteid"].stringValue;
                    NSLog(@"WE HAVE received a fin of delete action");
                    answered = [[NSMutableDictionary alloc] initWithDictionary:@{@"requestID": uuid}];
                    
                }
                
                // print the set we built
                PageData *pageData = [_messageIDsFromServer objectForKey:uuid];
                
                if (pageData) {
                    // if we delete all messages we must flush the pageData
                    if(isDeleteRequestAnswer)
                        [pageData.messageIDs removeAllObjects];
                    
                    // if complete is true, it's the last page, we have not from date.
                    if (isComplete) {
                        pageData.from = nil;
                    }
#if DEBUG
                    NSLog(@"PAGEDATA = %@ %@ %@", pageData.from, pageData.to, pageData.messageIDs);
#endif
                    // date from might be null.
                    NSArray <XMPPMessage*> *deletedMessages = [xmppMessageArchiveManagementStorage deleteCachedMessageNotInPageData:pageData xmmpStream:xmppStream];
                    for (XMPPMessage *message in deletedMessages) {
                        [multicastDelegate xmppMessageArchiveManagement:self didRemoveMessage:message];
                    }
                }
                if(isDeleteRequestAnswer)
                    [multicastDelegate xmppMessageArchiveManagement:self didEndDeleteAllMessagesForUUID:uuid];
                else
                    if(answered)
                        [multicastDelegate xmppMessageArchiveManagement:self didEndFetchingArchiveWithAnswer:answered];
                
                // Page data for the current query can be removed
                if (uuid) {
                    [_messageIDsFromServer removeObjectForKey:uuid];
                }
            }
            else {
                NSXMLElement *query = [iq elementForName:@"query" xmlns:XMLNS_XMPP_ARCHIVE_BULK];
                if (query != nil) {
                    NSString *uuid = [query attributeForName:@"queryid"].stringValue;
                    if (uuid != nil) {
                        NSMutableDictionary *answered = [[NSMutableDictionary alloc] initWithDictionary:@{@"requestID": uuid}];
                        [multicastDelegate xmppMessageArchiveManagement:self didEndFetchingArchiveWithAnswer:answered];
                    }
                    
                }
            }
        }
    };
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

-(void) handleDeleteAllMessagesQueryIQ:(XMPPIQ *)iq withInfo:(XMPPBasicTrackingInfo *)basicTrackingInfo {
    NSLog(@"End of request delete all message %@", iq);
    [self handleFetchMamQueryIQ:iq withInfo:basicTrackingInfo];
}

-(void)handleFetchCallLogQueryIQ:(XMPPIQ *)iq withInfo:(XMPPBasicTrackingInfo *)basicTrackingInfo {
    dispatch_block_t block = ^{
        @autoreleasepool {
            NSXMLElement *query = [iq elementForName:@"query" xmlns:XMLNS_XMPP_MAM_IQ_CALLLOGS];
            if(query){
                NSString *uuid = @"";
                NSMutableDictionary *answered = nil;
                NSLog(@"End of request for calllogs %@", query);
                
                if([query attributeForName:@"queryid"]){
                    uuid = [query attributeForName:@"queryid"].stringValue;
                    NSXMLElement *set = [query elementForName:@"set" xmlns:@"http://jabber.org/protocol/rsm"];
                    NSXMLElement *firstElement = [set elementForName:@"first"];
                    NSXMLElement *lastElement = [set elementForName:@"last"];
                    NSXMLElement *countElement = [set elementForName:@"count"];
                    
                    answered = [[NSMutableDictionary alloc] initWithDictionary:@{@"requestID": uuid}];
                    
                    if(countElement)
                        [answered addEntriesFromDictionary:@{@"count" : countElement.stringValue}];
                    
                    if(firstElement && lastElement)
                        [answered addEntriesFromDictionary:@{@"first": firstElement.stringValue, @"last": lastElement.stringValue}];
                }
                
                // print the set we built
                PageData *pageData = [_messageIDsFromServer objectForKey:uuid];
                
                if (pageData) {
#if DEBUG
                    NSLog(@"PAGEDATA = %@ %@ %@", pageData.from, pageData.to, pageData.messageIDs);
#endif
                    [xmppMessageArchiveManagementStorage deleteCachedCallLogsNotInPageData:pageData xmmpStream:xmppStream];
                }
                // Page data for the current query can be removed
                if (uuid) {
                    [_messageIDsFromServer removeObjectForKey:uuid];
                }
                
                [multicastDelegate xmppMessageArchiveManagement:self didEndFetchingCallLogsWithAnswer:answered];
            }
        }
    };
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}


-(void)handleDeleteCallLogWithContactJIDQueryIQ:(XMPPIQ *)iq withInfo:(XMPPBasicTrackingInfo *)basicTrackingInfo {
    dispatch_block_t block = ^{
        @autoreleasepool {
            NSLog(@"End of request for delete calllogs %@", iq);
            NSString *uuid = [iq attributeForName:@"id"].stringValue;
            // Notify end of delete
            // No uuid means delete All calllogs
            if(uuid){
                [multicastDelegate xmppMessageArchiveManagement:self didEndDeleteCallLogsForUUID:uuid];
            } else {
                [multicastDelegate didReceiveDeleteAllCallLogsInXmppMessageArchiveManagement:self];
            }
        }
    };
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

#pragma mark - call logs
-(void) fetchCallLogsWithUUID:(NSString *) uniqueID {
    dispatch_block_t block = ^{
        @autoreleasepool {
            [multicastDelegate xmppMessageArchiveManagement:self didBeginFetchingArchiveForUUID:uniqueID];
            
            PageData *pageData = [PageData new];
            // before now.
            pageData.to = [NSDate date];
            pageData.fromJid = xmppStream.myJID.bare;
            
            XMPPJID *toIQ = [XMPPJID jidWithString:xmppStream.myJID.bare];
            [_messageIDsFromServer setObject:pageData forKey:uniqueID];
            
            NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:XMLNS_XMPP_MAM_IQ_CALLLOGS];
            [query addAttributeWithName:@"queryid" stringValue:uniqueID];
            
            NSXMLElement *set = [NSXMLElement elementWithName:@"set" xmlns:@"http://jabber.org/protocol/rsm"];
            
            NSXMLElement *max = [NSXMLElement elementWithName:@"max" numberValue:@75];
            NSXMLElement *before = [NSXMLElement elementWithName:@"before"];
            [set addChild:max];
            [set addChild:before];
            [query addChild:set];
            
            XMPPIQ *iq = [XMPPIQ iqWithType:@"set" to:toIQ elementID:uniqueID child:query];
            [iq setXmlns:@"jabber:client"];
            [_xmppIDTracker addElement:iq target:self selector:@selector(handleFetchCallLogQueryIQ:withInfo:) timeout:60];
            [xmppStream sendElement:iq];
        }
    };
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

-(void) deleteAllCallLogsWithUUID:(NSString *) uniqueID {
    dispatch_block_t block = ^{
        @autoreleasepool {
            NSXMLElement *query = [NSXMLElement elementWithName:@"delete" xmlns:XMLNS_XMPP_MAM_IQ_CALLLOGS];
            
            XMPPIQ *deleteIQ = [XMPPIQ iqWithType:@"set" to:nil elementID:uniqueID child:query];
            [deleteIQ setXmlns:@"jabber:client"];
            
            NSLog(@"DELETE ALL CALLLOGS WITH IQ %@", deleteIQ);
            [_xmppIDTracker addElement:deleteIQ target:self selector:@selector(handleDeleteCallLogWithContactJIDQueryIQ:withInfo:) timeout:60];
            [xmppStream sendElement:deleteIQ];
        }
    };
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

-(void) deleteCallLogsWithContactJID:(NSString *) contactJID withUUID:(NSString *) UUID {
    dispatch_block_t block = ^{
        @autoreleasepool {
            NSXMLElement *query = [NSXMLElement elementWithName:@"delete" xmlns:XMLNS_XMPP_MAM_IQ_CALLLOGS];
            [query addAttributeWithName:@"peer" stringValue:contactJID];
            
            XMPPIQ *deleteIQ = [XMPPIQ iqWithType:@"set" to:nil elementID:UUID child:query];
            [deleteIQ setXmlns:@"jabber:client"];
            
            NSLog(@"DELETE CALLLOGS WITH IQ %@", deleteIQ);
            [_xmppIDTracker addElement:deleteIQ target:self selector:@selector(handleDeleteCallLogWithContactJIDQueryIQ:withInfo:) timeout:60];
            [xmppStream sendElement:deleteIQ];
        }
    };
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Utilities
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)shouldArchiveMessage:(XMPPMessage *)message outgoing:(BOOL)isOutgoing xmppStream:(XMPPStream *)xmppStream {
    
    XMPPMessage *messageToThread = message;
    
    if([message isForwardedStanza])
        messageToThread = [message forwardedMessage];
    else if ([message isMessageCarbon])
        messageToThread = [message messageCarbonForwardedMessage];
    else if ([message isResultStanza])
        messageToThread = [message resultForwardedMessage];

    BOOL hasStoreFlag = [messageToThread elementsForName:@"store"].count > 0;
    
    if([messageToThread isErrorMessage])
        return NO;
    
    if([messageToThread isChatMessageWithBody] || [messageToThread isGroupChatMessageWithBody] || [messageToThread isFileTranfer] || [messageToThread isWebRTCCall] || [messageToThread isCallLogMessage] || [messageToThread isUpdatedCallLogMessage] || hasStoreFlag)
        return YES;
    
    return NO;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPStream Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    XMPPLogTrace();

    if (clientSideMessageArchivingOnly) return;
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
    /*
      IQ when deleting all messages
     <iq xmlns='jabber:client' from='j_8174134054@openrainbow.net' to='j_8174134054@openrainbow.net/web_win_1.10.0_jLMrwbsl' id='1343dc62254f46349e537a51f35d03ee@openrainbow.net' type='result'>
        <fin xmlns='urn:xmpp:mam:0' deleteid='remove_1343dc62254f46349e537a51f35d03ee@openrainbow.net'>
            <set xmlns='http://jabber.org/protocol/rsm'>
                <count>6</count>
            </set>
        </fin>
     </iq>
     */
    
    /*
     IQ for end of loading mam messages from server
         <iq xmlns="jabber:client" from="jerome2.otpc@opentouchcloud.org" to="jerome2.otpc@opentouchcloud.org/Colleagues" id="B68929B5-6A3F-4F54-8066-07C7FF9D878A" type="result">
            <fin xmlns="urn:xmpp:mam:1" queryid="018344B7-A575-4987-82D6-C8787AAED30D" complete="true">
              <set xmlns="http://jabber.org/protocol/rsm">
                  <first>1445503306599735</first>
                  <last>1445868196722907</last>
                  <count>10</count>
              </set>
          </fin>
        </iq>
     */
    
    NSString *type = [iq type];
    if ([type isEqualToString:@"result"]) {
        NSXMLElement *pref = [iq elementForName:@"prefs" xmlns:XMLNS_XMPP_ARCHIVE];
        if (pref) {
            [self setPreferences:pref];
        }
        
        // XEP-0313 v0.4
        NSXMLElement *fin = [iq elementForName:@"fin" xmlns:XMLNS_XMPP_ARCHIVE];
        if(fin) {
            return [_xmppIDTracker invokeForID:[iq elementID] withObject:iq];
        }
        
        NSXMLElement *finBulk = [iq elementForName:@"fin" xmlns:XMLNS_XMPP_ARCHIVE_BULK];
        if(finBulk) {
            return [_xmppIDTracker invokeForID:[iq elementID] withObject:iq];
        }
        
        // Call log
        NSXMLElement *query = [iq elementForName:@"query" xmlns:XMLNS_XMPP_MAM_IQ_CALLLOGS];
        if(query){
            return [_xmppIDTracker invokeForID:[iq elementID]  withObject:iq];
        }
        
        // Call logs deleted
        NSXMLElement *deleted = [iq elementForName:@"deleted" xmlns:XMLNS_XMPP_MAM_IQ_CALLLOGS];
        if(deleted){
            return [_xmppIDTracker invokeForID:[iq elementID] withObject:iq];
        }
    }
    return NO;
}

// We save the messages in mam before sending it because in case of message send in offline we want to display it imediatelly
- (XMPPMessage *)xmppStream:(XMPPStream *)sender willSendMessage:(XMPPMessage *)message {
    if ([self shouldArchiveMessage:message outgoing:YES xmppStream:sender]) {
        NSXMLElement *ackElement = [message elementForName:@"ack"];
        if (!ackElement) {
            ackElement = [NSXMLElement elementWithName:@"ack"];
            [ackElement addAttributeWithName:@"received" stringValue:@"false"];
            [ackElement addAttributeWithName:@"read" stringValue:@"false"];
            [message addChild:ackElement];
        }
        
        NSString *originalTo = message.toStr;
        
        if([message isGroupChatMessage]){
            // We must add the "from" room/jid/ressource to allow mam to save it correctly
            // We must also change the "to" to be conform to a real didSendMessage callback
            // ex :
            //  from="room_6337aa307079488ea85273113e5328e3@muc.jerome-all-in-one-dev-1.opentouch.cloud/c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud/mobile_ios_9956EE3D-79C7-44C1-9179-7B7BAA50A8F5"
            
            //  to="c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud/mobile_ios_9956EE3D-79C7-44C1-9179-7B7BAA50A8F5"
            
            NSString *from = [NSString stringWithFormat:@"%@/%@", originalTo, xmppStream.myJID.full];
            
            [message addAttributeWithName:@"from" stringValue:from];
            [message removeAttributeForName:@"to"];
            [message addAttributeWithName:@"to" stringValue:xmppStream.myJID.full];
        }
        
        [xmppMessageArchiveManagementStorage archiveMessage:message outgoing:YES xmppStream:sender];
        
        [message removeElementForName:@"ack"];
        if([message isGroupChatMessage]){
            [message removeAttributeForName:@"to"];
            [message addAttributeWithName:@"to" stringValue:originalTo];
            [message removeAttributeForName:@"from"];
        }
    }
    
    return message;
}

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
    XMPPLogTrace();
    NSLog(@"%@ : %@",THIS_FILE, THIS_METHOD);
    // Deal with delete call log notification (from another device)
    if([message isDeletedCallLogMessage]){
        NSXMLElement *deletedCallLogElement = [message elementForName:@"deleted_call_log" xmlns:XMLNS_XMPP_MAM_CALLLOGS];
        NSString *peer = [deletedCallLogElement attributeForName:@"peer"].stringValue;
        if(peer.length > 0){
            // Special treatment for call logs with non rainbow user that do not have rainbow JID
            // We receive a peer that looks like "phonenumber@_" ex: 6100@_
            if([peer containsString:@"@_"])
                peer = [peer componentsSeparatedByString:@"@_"].firstObject;
            
            [xmppMessageArchiveManagementStorage deleteAllCallLogWithPeerJID:peer xmmpStream:xmppStream];
            [multicastDelegate xmppMessageArchiveManagement:self didReceiveDeleteCallLogForContactJID:peer];
        } else {
            [xmppMessageArchiveManagementStorage deleteAllCallLogsForXmppStream:xmppStream];
            [multicastDelegate didReceiveDeleteAllCallLogsInXmppMessageArchiveManagement:self];
        }
        return;
    }
    
    // Deal with call log read notification
    if([message isReadCallLogNotificationMessage]){
        NSString *callLogId = [[message elementForName:@"read" xmlns:XMLNS_XMPP_CALLLOGS_RECEIPTS] attributeForName:@"call_id"].stringValue;
        [xmppMessageArchiveManagementStorage updateMessageID:callLogId deliveryState:@"read" at:nil];
        [multicastDelegate xmppMessageArchiveManagement:self didUpdateCallLogReadState:callLogId];
        return;
    }
    
    // Notification for message deleted on another device.
    NSXMLElement *deletedElement = [message elementForName:@"deleted" xmlns:@"jabber:iq:notification"];
    if ([message isChatMessage] && deletedElement) {
        NSString *peerJID = [deletedElement attributeStringValueForName:@"with"];
        NSString *messageID = [deletedElement attributeStringValueForName:@"id"];
        if ([messageID isEqualToString:@"all"]) {
            [xmppMessageArchiveManagementStorage deleteAllMessagesWithPeerJID:peerJID xmmpStream:xmppStream];
            [multicastDelegate xmppMessageArchiveManagement:self didReceiveDeleteAllMessagesInConversationWithPeerJID:peerJID];
        } else {
            [xmppMessageArchiveManagementStorage deleteMessageID:messageID withPeerJID:peerJID xmmpStream:xmppStream];
            [multicastDelegate xmppMessageArchiveManagement:self didReceiveDeleteMessageID:messageID inConversationWithPeerJID:peerJID];
        }
        return;
    }
    
    // Check if it's a bulk message answer
    NSXMLElement *resultsElements = [message elementForName:@"results" xmlns:XMLNS_XMPP_ARCHIVE_BULK];
    if(resultsElements){
        NSArray *results = [resultsElements elementsForName:@"result"];
        if(results){
            for (NSXMLElement *result in results) {
                XMPPMessage *resultMessage = [result forwardedMessage];
                
                XMPPMessage *tmpMessage = [[XMPPMessage alloc] initWithType:nil to:resultMessage.to elementID:nil child:[result copy]];
                [tmpMessage addAttributeWithName:@"from" stringValue:resultMessage.from.full];
                tmpMessage.xmlns = @"jabber:client";
                [self saveMessageInCache:tmpMessage xmppStream:sender];
            }
        }
    } else {
        // Deal with non bulk messages
        [self saveMessageInCache:message xmppStream:sender];
    }

    // Handle XEP-184 ACK messages (and carbon)
    // When we receive an acknowledgment, we must update the message delivery status in our cache.
    XMPPMessage *ackMessage = message;
    if ([ackMessage isMessageCarbon]) {
        ackMessage = [ackMessage messageCarbonForwardedMessage];
    }
    
    // Acks look like :
    // <message from="XXX" to="YYY" type="chat">
    //    <timestamp xmlns="urn:xmpp:receipts" value="2016-10-28T08:47:04.243116Z"/>
    //    <received xmlns="urn:xmpp:receipts" event="read" entity="client" id="101A2766-98D7-4001-AA98-23C5D010F025"/>
    // </message>
    if([ackMessage hasReceiptResponse]) {
        NSXMLElement *receivedElement = [ackMessage elementForName:@"received" xmlns:@"urn:xmpp:receipts"];
        // update only for client events, not server ones
        if ([[receivedElement attributeStringValueForName:@"entity"] isEqualToString:@"server"]) {
            NSString *messageID = [ackMessage receiptResponseID];
            NSXMLElement *timestampElement = [ackMessage elementForName:@"timestamp" xmlns:@"urn:xmpp:receipts"];
            NSString *timestamp = [timestampElement attributeStringValueForName:@"value"];
            if(!timestamp)
                timestamp = [[NSDate date] xmppDateTimeString];
            [xmppMessageArchiveManagementStorage updateMessageID:messageID deliveryState:@"delivered" at:timestamp];
            [self updateMessageWithMessageID:messageID atTimestamp:timestamp];
            
            
            
        }
        if ([[receivedElement attributeStringValueForName:@"entity"] isEqualToString:@"client"]) {
            NSString *messageID = [ackMessage receiptResponseID];
            
            NSXMLElement *timestampElement = [ackMessage elementForName:@"timestamp" xmlns:@"urn:xmpp:receipts"];
            NSString *timestamp = [timestampElement attributeStringValueForName:@"value"];
            
            NSString *state = [receivedElement attributeStringValueForName:@"event"];
            
            [xmppMessageArchiveManagementStorage updateMessageID:messageID deliveryState:state at:timestamp];
            
        }
    }
}


- (void) updateMessageWithMessageID:(NSString *)messageID atTimestamp :(NSString *) timestamp {
    [xmppMessageArchiveManagementStorage updateMessageID:messageID atTimeStamp:timestamp];
}

-(void) saveMessageInCache:(XMPPMessage *) message xmppStream:(XMPPStream *) sender {
    if ([self shouldArchiveMessage:message outgoing:NO xmppStream:sender]) {
        NSXMLElement *result = nil;
        if([message isResultStanza])
            result = [message elementForName:@"result" xmlns:XMLNS_XMPP_ARCHIVE];
        else if ([message isCallLogMessage])
            result = [message callLogResultStanza];
        
        if(!result && [message isUpdatedCallLogMessage]){
            result = [message updatedCallLogStanza];
        }
        
        
        NSString *queryID = [result attributeStringValueForName:@"queryid"];
        PageData *pageData = [_messageIDsFromServer objectForKey:queryID];
        
        if(pageData && [result hasForwardedStanza]) {
            NSDate *forwardedDelay = [[result forwardedStanza] delayedDeliveryDate];
            XMPPMessage *forwardedMessage = [result forwardedMessage];
            NSString *elementID = [forwardedMessage elementID];
            if(elementID.length > 0){
                [pageData.messageIDs addObject:elementID];
                
                if ([forwardedDelay compare:pageData.from] == NSOrderedAscending || !pageData.from) {
                    pageData.from = forwardedDelay;
                }
                if ([forwardedDelay compare:pageData.to] == NSOrderedDescending || !pageData.to) {
                    pageData.to = forwardedDelay;
                }
            } else {
                // check if it's a calllog
                if([message isCallLogMessage]){
                    NSXMLElement *callLogElement = [message callLogStanza];
                    
                    if(callLogElement){
                        NSString *callLogID = [callLogElement elementForName:@"call_id"].stringValue;
                        if(callLogID.length){
                            [pageData.messageIDs addObject:callLogID];
                            if ([forwardedDelay compare:pageData.from] == NSOrderedAscending || !pageData.from) {
                                pageData.from = forwardedDelay;
                            }
                            if ([forwardedDelay compare:pageData.to] == NSOrderedDescending || !pageData.to) {
                                pageData.to = forwardedDelay;
                            }
                        } else {
                            NSLog(@"No callLogID for this message ! %@", message);
                        }
                    }
                } else {
                    NSLog(@"No elementID for this message !! %@", forwardedMessage);
                }
            }
        }
        
        // If we have sent a new message from one of our other devices,
        // we want to store in our cache the message, but marked as non-received and non-read yet.
        // Otherwise, it'll be treated at reload like an "old message" where no <ack/> markup means received and read.
        // So add the markup <ack/> if its a sent carbon
        if ([message isSentMessageCarbon]) {
            NSXMLElement *messageToThread = [message messageCarbonForwardedMessage];
            NSXMLElement *ackElement = [messageToThread elementForName:@"ack"];
            if (!ackElement) {
                ackElement = [NSXMLElement elementWithName:@"ack"];
                [messageToThread addChild:ackElement];
            }
            if (![ackElement attributeForName:@"received"]) {
                [ackElement addAttributeWithName:@"received" stringValue:@"false"];
            }
            if (![ackElement attributeForName:@"read"]) {
                [ackElement addAttributeWithName:@"read" stringValue:@"false"];
            }
        }
        
        [xmppMessageArchiveManagementStorage archiveMessage:message outgoing:NO xmppStream:sender];
        [multicastDelegate xmppMessageArchiveManagement:self didEndArchiveMessage:message];
    } else {
        NSLog(@"Don't save in mam message %@", message);
    }
}

@end
