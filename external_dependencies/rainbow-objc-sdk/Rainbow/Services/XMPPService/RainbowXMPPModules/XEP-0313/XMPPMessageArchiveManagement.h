/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "XMPP.h"
#import "Peer.h"
#import "CallLog.h"
#import "Message.h"

#define _XMPP_MESSAGE_ARCHIVING_MANAGEMENT_H

@protocol XMPPMessageArchiveManagementStorage;


@interface PageData : NSObject
@property (nonatomic, strong) NSDate *from;
@property (nonatomic, strong) NSDate *to;
@property (nonatomic, strong) NSMutableSet *messageIDs;
@property (nonatomic, strong) NSString *fromJid;
@end

/**
 * This class provides support for storing message history.
 * The functionality is formalized in XEP-0313.
 **/

@interface XMPPMessageArchiveManagement : XMPPModule {

@protected
    __strong id <XMPPMessageArchiveManagementStorage> xmppMessageArchiveManagementStorage;
    
@private
    BOOL clientSideMessageArchivingOnly;
    NSXMLElement *preferences;
}
- (id) initWithMessageArchiveManagementStorage:(id <XMPPMessageArchiveManagementStorage>)storage;
- (id) initWithMessageArchiveManagementStorage:(id <XMPPMessageArchiveManagementStorage>)storage dispatchQueue:(dispatch_queue_t)queue;
- (void) fetchArchivedMessagesWithPeer:(Peer *) peer withUUID:(NSString *) UUID maxSize:(NSNumber *) maxSize beforeDate:(NSDate *) beforeDate lastMessageID:(NSString *) lastMessageID;

-(void) saveMessageInCache:(XMPPMessage *) message xmppStream:(XMPPStream *) sender;

-(void) deleteAllMessagesWithPeer:(Peer *) peer withUUID:(NSString *) UUID;

- (void) updateMessageWithMessageID:(NSString *)messageID atTimestamp :(NSString *) timestamp;

@property (readonly, strong) id <XMPPMessageArchiveManagementStorage> xmppMessageArchiveManagementStorage;

#pragma mark - CallLogs
-(void) fetchCallLogsWithUUID:(NSString *) uniqueID;

-(void) deleteAllCallLogsWithUUID:(NSString *) uniqueID;

-(void) deleteCallLogsWithContactJID:(NSString *) contactJID withUUID:(NSString *) UUID;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol XMPPMessageArchiveManagementStorage <NSObject>
@required

- (BOOL)configureWithParent:(XMPPMessageArchiveManagement *)aParent queue:(dispatch_queue_t)queue;

- (void)archiveMessage:(XMPPMessage *)message outgoing:(BOOL)outgoing xmppStream:(XMPPStream *)stream;

- (NSArray <XMPPMessage *> *)deleteCachedMessageNotInPageData:(PageData*) pageData xmmpStream:(XMPPStream *) xmppStream;

- (void)deleteAllMessagesWithPeerJID:(NSString *) jid xmmpStream:(XMPPStream *) xmppStream;

- (void)deleteMessageID:(NSString *) messageID withPeerJID:(NSString *) jid xmmpStream:(XMPPStream *) xmppStream;

- (void)updateMessageID:(NSString *) messageID deliveryState:(NSString *) state at:(NSString *) timestamp;

- (void)updateMessageID:(NSString *) messageID  atTimeStamp:(NSString *) timestamp;

- (NSDate *)getMessageDateForMamMessageId:(NSString *) messageID;

- (NSArray<Message*> *)fetchMessagesForJID:(XMPPJID *)jid xmmpStream:(XMPPStream *) xmppStream maxSize:(NSNumber *) maxSize offset:(NSNumber *) offset;

#pragma mark - CallLogs
-(NSArray<CallLog *> *) fetchCallLogsForJid:(XMPPJID *)jid xmppStream:(XMPPStream *) xmppStream;
-(void) deleteCachedCallLogsNotInPageData:(PageData *)pageData xmmpStream:(XMPPStream *)xmppStream;
-(void) deleteAllCallLogWithPeerJID:(NSString *) jid xmmpStream:(XMPPStream *) xmppStream;
-(void) deleteAllCallLogsForXmppStream:(XMPPStream *) xmppStream;

#pragma mark - preferences
@optional

/**
 * The storage class may optionally persistently store the client preferences.
 **/
- (void)setPreferences:(NSXMLElement *)prefs forUser:(XMPPJID *)bareUserJid;

/**
 * The storage class may optionally persistently store the client preferences.
 * This method is then used to fetch previously known preferences when the client first connects to the xmpp server.
 **/
- (NSXMLElement *)preferencesForUser:(XMPPJID *)bareUserJid;

@end

@protocol XMPPMessageArchiveManagementDelegate <NSObject>
@optional

- (void)xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didBeginFetchingArchiveForUUID:(NSString *) UUID;

/**
 * Sent when the initial roster has been populated into storage.
 **/
- (void)xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndFetchingArchiveWithAnswer:(NSDictionary *) answer;

- (void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndArchiveMessage:(XMPPMessage *)message;

- (void) updateMessageWithMessageID:(NSString *)messageID atTimestamp :(NSString *) timestamp;

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndDeleteAllMessagesForUUID:(NSString *) uuid;

// Sent when we flushed the cached because of a "message deleted on another device" event is received
-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didReceiveDeleteAllMessagesInConversationWithPeerJID:(NSString *) peerJID;

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didReceiveDeleteMessageID:(NSString *) messageID inConversationWithPeerJID:(NSString *) peerJID;

#pragma mark - Call logs
-(void)xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndFetchingCallLogsWithAnswer:(NSDictionary *) answer;
-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndDeleteCallLogsForUUID:(NSString *) uuid;

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didReceiveDeleteCallLogForContactJID:(NSString *) jid;

-(void) didReceiveDeleteAllCallLogsInXmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender;

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *) sender didUpdateCallLogReadState:(NSString *) callLogID;

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *) sender didRemoveMessage:(XMPPMessage *) message;
@end
