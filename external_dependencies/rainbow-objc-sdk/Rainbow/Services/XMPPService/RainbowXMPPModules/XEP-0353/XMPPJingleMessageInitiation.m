/*
 * Rainbow
 *
 * Copyright (c) 2016-2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPJingleMessageInitiation.h"
#import "XMPP.h"
#import "XMPPMessage.h"
#import "NSXMLElement+XEP_0203.h"
#import "NSXMLElement+XEP_0203_Element.h"

#define JINGLE_MESSAGE_XMLNS @"urn:xmpp:jingle-message:0"
#define JINGLE_APPS_RTP_XMLNS @"urn:xmpp:jingle:apps:rtp:1"
#define JINGLE_SUBJECT_XMLNS @"urn:xmpp:jingle-subject:0"

@implementation XMPPJingleMessageInitiation

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
    if ([[message type] length] > 0) {
        // XEP says messages should not have a type.
        // We specially want to ignore type='error'
        return;
    }
    
    if(!_myUser.isAllowedToUseWebRTCMobile){
        NSLog(@"This user is not allowed to use webRTC on mobile");
        return;
    }
    
    // We can receive these elements :
    // - <propose>
    // - <retract>
    // - <accept>
    // - <proceed>
    // - <reject>
    
    // Ignore resent messages,
    // Because we already received these messages through Push.
    if([message wasDelayed]){
        if([[[message delayElement] stringValue] isEqualToString:@"Resent"]) {
            return;
        }
    }
    

    
    NSXMLElement *element = nil;
    
    if ((element = [message elementForName:@"propose" xmlns:JINGLE_MESSAGE_XMLNS])) {
        /*
         <message from='romeo@montague.example/orchard'
                  to='juliet@capulet.example'>
            <propose xmlns='urn:xmpp:jingle-message:0' id='a73sjjvkla37jfea'>
                <description xmlns='urn:xmpp:jingle:apps:rtp:1' media='audio'/>
            </propose>
         </message>
         */
        NSString *sessionID = [element attributeStringValueForName:@"id"];
        NSMutableArray<NSString *> *medias = [NSMutableArray new];
        for (NSXMLElement *description in [element elementsForName:@"description"]) {
            if ([[description xmlns] isEqualToString:JINGLE_APPS_RTP_XMLNS]) {
                NSString *media = [description attributeStringValueForName:@"media"];
                if (media) {
                    [medias addObject:media];
                }
            }
        }
        if([medias containsObject:@"sharing"] && medias.count == 1){
            NSLog(@"It's a sharing sharing request, we must ignore it");
            return;
        }
        [multicastDelegate xmppJingleMessageInitiation:self didReceiveProposeMsg:sessionID medias:medias from:message.from];
    }
    else if ((element = [message elementForName:@"retract" xmlns:JINGLE_MESSAGE_XMLNS])) {
        /*
         <message from='romeo@montague.example/orchard'
                  to='juliet@capulet.example'>
            <retract xmlns='urn:xmpp:jingle-message:0' id='a73sjjvkla37jfea'/>
         </message>
         */
        NSString *sessionID = [element attributeStringValueForName:@"id"];
        [multicastDelegate xmppJingleMessageInitiation:self didReceiveRetractMsg:sessionID from:message.from];
    }
    else if ((element = [message elementForName:@"accept" xmlns:JINGLE_MESSAGE_XMLNS])) {
        /*
         <message from='juliet@capulet.example/phone'
                  to='juliet@capulet.example'>
            <accept xmlns='urn:xmpp:jingle-message:0' id='a73sjjvkla37jfea'/>
         </message>
         */
        NSString *sessionID = [element attributeStringValueForName:@"id"];
        // check from == to.
        if (![message.from.bareJID isEqualToJID:message.to.bareJID]) {
            NSLog(@"Error: we received a <accept> JingleMessageInitiation but its not from me !");
        } else {
            [multicastDelegate xmppJingleMessageInitiation:self didReceiveAcceptMsg:sessionID];
        }
    }
    else if ((element = [message elementForName:@"proceed" xmlns:JINGLE_MESSAGE_XMLNS])) {
        /*
         <message from='juliet@capulet.example/phone'
                  to='romeo@montague.example/orchard'>
            <proceed xmlns='urn:xmpp:jingle-message:0' id='a73sjjvkla37jfea'/>
         </message>
         */
        NSString *sessionID = [element attributeStringValueForName:@"id"];
        [multicastDelegate xmppJingleMessageInitiation:self didReceiveProceedMsg:sessionID from:message.from];
    }
    else if ((element = [message elementForName:@"reject" xmlns:JINGLE_MESSAGE_XMLNS])) {
        /*
         <message from='juliet@capulet.example/tablet'
                  to='juliet@capulet.example'>
            <reject xmlns='urn:xmpp:jingle-message:0' id='a73sjjvkla37jfea'/>
         </message>
         */
        NSString *sessionID = [element attributeStringValueForName:@"id"];
        [multicastDelegate xmppJingleMessageInitiation:self didReceiveRejectMsg:sessionID from:message.from];
    }
}


#pragma mark - send XEP-0353 messages

// I want to call somebody
- (void)sendProposeMsg:(NSString *) sessionID medias:(NSArray<NSString *> *) medias to:(XMPPJID *) bareJID  {
    /*
     <message from='romeo@montague.example/orchard'
              to='juliet@capulet.example'>
        <propose xmlns='urn:xmpp:jingle-message:0' id='a73sjjvkla37jfea'>
            <description xmlns='urn:xmpp:jingle:apps:rtp:1' media='audio'/>
        </propose>
     </message>
     */
    [self sendProposeMsg:sessionID subject:nil medias:medias to:bareJID];
}

// I want to call somebody with a subject
- (void)sendProposeMsg:(NSString *) sessionID subject:(NSString *) subject medias:(NSArray<NSString *> *) medias to:(XMPPJID *) bareJID  {
    /*
     <message from='romeo@montague.example/orchard'
              to='juliet@capulet.example'>
        <propose xmlns='urn:xmpp:jingle-message:0' id='a73sjjvkla37jfea'>
            <description xmlns='urn:xmpp:jingle:apps:rtp:1' media='audio'/>
            <subject xmlns='urn:xmpp:jingle-subject:0'>Subject of the call</subject>
        </propose>
     </message>
     */
    // Only to bareJID
    if (![bareJID isBareWithUser]) {
        NSLog(@"Error: <propose> messages should be sent to bare JID");
        return;
    }
    
    NSXMLElement *proposeElement = [NSXMLElement elementWithName:@"propose" xmlns:JINGLE_MESSAGE_XMLNS];
    [proposeElement addAttributeWithName:@"id" stringValue:sessionID];
    
    for (NSString *media in medias) {
        NSXMLElement *descriptionElement = [NSXMLElement elementWithName:@"description" xmlns:JINGLE_APPS_RTP_XMLNS];
        [descriptionElement addAttributeWithName:@"media" stringValue:media];
        [proposeElement addChild:descriptionElement];
    }
    if (subject) {
        NSXMLElement *subjectElement = [NSXMLElement elementWithName:@"subject" stringValue:subject];
        [subjectElement setXmlns:JINGLE_SUBJECT_XMLNS];
        [proposeElement addChild:subjectElement];
    }
    
    XMPPMessage *message = [XMPPMessage messageWithType:@"" to:[bareJID bareJID] elementID:[xmppStream generateUUID] child:proposeElement];
    [message removeAttributeForName:@"type"];
    if (message) {
        [xmppStream sendElement:message];
    }
}

// I cancel a call not yet answered
- (void)sendRetractMsg:(NSString *) sessionID to:(XMPPJID *) bareJID {
    /*
     <message from='romeo@montague.example/orchard'
              to='juliet@capulet.example'>
        <retract xmlns='urn:xmpp:jingle-message:0' id='a73sjjvkla37jfea'/>
     </message>
     */
    // Only to bareJID
    if (![bareJID isBareWithUser]) {
        NSLog(@"Error: <retract> messages should be sent to bare JID");
        return;
    }
    
    NSXMLElement *retractElement = [NSXMLElement elementWithName:@"retract" xmlns:JINGLE_MESSAGE_XMLNS];
    [retractElement addAttributeWithName:@"id" stringValue:sessionID];
    
    XMPPMessage *message = [XMPPMessage messageWithType:@"" to:[bareJID bareJID] elementID:[xmppStream generateUUID] child:retractElement];
    [message removeAttributeForName:@"type"];
    if (message) {
        [xmppStream sendElement:message];
    }
}

// I accept a call on this device, send carbon to my other devices to stop ringing
- (void)sendAcceptMsg:(NSString *) sessionID {
    /*
     <message from='juliet@capulet.example/phone'
              to='juliet@capulet.example'>
        <accept xmlns='urn:xmpp:jingle-message:0' id='a73sjjvkla37jfea'/>
     </message>
     */
    
    NSXMLElement *acceptElement = [NSXMLElement elementWithName:@"accept" xmlns:JINGLE_MESSAGE_XMLNS];
    [acceptElement addAttributeWithName:@"id" stringValue:sessionID];
    
    XMPPMessage *message = [XMPPMessage messageWithType:@"" to:[xmppStream.myJID bareJID] elementID:[xmppStream generateUUID] child:acceptElement];
    [message removeAttributeForName:@"type"];
    if (message) {
        [xmppStream sendElement:message];
    }
}
// I accept a call, send proceed to a caller
- (void)sendProceedMsg:(NSString *) sessionID to:(XMPPJID *) fullJID {
    /*
     <message from='juliet@capulet.example/phone'
              to='romeo@montague.example/orchard'>
        <proceed xmlns='urn:xmpp:jingle-message:0' id='a73sjjvkla37jfea'/>
     </message>
     */
    // Only to fullJID
    if (![fullJID isFullWithUser]) {
        NSLog(@"Error: <proceed> messages should be sent to full JID");
        return;
    }
    
    NSXMLElement *proceedElement = [NSXMLElement elementWithName:@"proceed" xmlns:JINGLE_MESSAGE_XMLNS];
    [proceedElement addAttributeWithName:@"id" stringValue:sessionID];
    
    XMPPMessage *message = [XMPPMessage messageWithType:@"" to:fullJID elementID:[xmppStream generateUUID] child:proceedElement];
    [message removeAttributeForName:@"type"];
    if (message) {
        [xmppStream sendElement:message];
    }
}

// I refuse an incoming call
- (void)sendRejectMsg:(NSString *) sessionID to:(XMPPJID *) JID {
    /*
     <message from='juliet@capulet.example/tablet'
              to='juliet@capulet.example'>
        <reject xmlns='urn:xmpp:jingle-message:0' id='a73sjjvkla37jfea'/>
     </message>
     */
    
    // Could be to bareJID (for me)
    // or to fullJID (to remote)
    
    NSXMLElement *rejectElement = [NSXMLElement elementWithName:@"reject" xmlns:JINGLE_MESSAGE_XMLNS];
    [rejectElement addAttributeWithName:@"id" stringValue:sessionID];
    
    XMPPMessage *message = [XMPPMessage messageWithType:@"" to:JID elementID:[xmppStream generateUUID] child:rejectElement];
    [message removeAttributeForName:@"type"];
    if (message) {
        [xmppStream sendElement:message];
    }
}

@end
