/*
 * Rainbow
 *
 * Copyright (c) 2016-2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPModule.h"
#import "XMPPJID.h"

#import "MyUser.h"


@interface XMPPJingleMessageInitiation : XMPPModule

@property (nonatomic, weak) MyUser *myUser;

// I want to call somebody
- (void)sendProposeMsg:(NSString *) sessionID medias:(NSArray<NSString *> *) medias to:(XMPPJID *) bareJID;

// I want to call somebody with a subject
- (void)sendProposeMsg:(NSString *) sessionID subject:(NSString *) subject medias:(NSArray<NSString *> *) medias to:(XMPPJID *) bareJID;

// I cancel a call not yet answered
- (void)sendRetractMsg:(NSString *) sessionID to:(XMPPJID *) bareJID;

// I accept a call on this device, send carbon to my other devices to stop ringing
- (void)sendAcceptMsg:(NSString *) sessionID;

// I accept a call, send proceed to a caller
- (void)sendProceedMsg:(NSString *) sessionID to:(XMPPJID *) fullJID;

// I refuse an incoming call
- (void)sendRejectMsg:(NSString *) sessionID to:(XMPPJID *) JID;

@end


@protocol XMPPJingleMessageInitiationDelegate <NSObject>

// Somebody is calling me
- (void)xmppJingleMessageInitiation:(XMPPJingleMessageInitiation *) xmppJMI didReceiveProposeMsg:(NSString *) sessionID medias:(NSArray<NSString *> *) medias from:(XMPPJID *) from;

// Somebody is canceling is call to me
- (void)xmppJingleMessageInitiation:(XMPPJingleMessageInitiation *) xmppJMI didReceiveRetractMsg:(NSString *) sessionID from:(XMPPJID *) from;

// I accepted an incoming call on another device (no from parameter as it's suppsed to be ME)
- (void)xmppJingleMessageInitiation:(XMPPJingleMessageInitiation *) xmppJMI didReceiveAcceptMsg:(NSString *) sessionID;

// Somebody accepted my outgoing call
- (void)xmppJingleMessageInitiation:(XMPPJingleMessageInitiation *) xmppJMI didReceiveProceedMsg:(NSString *) sessionID from:(XMPPJID *) from;

// Somebody rejected my outgoing call
- (void)xmppJingleMessageInitiation:(XMPPJingleMessageInitiation *) xmppJMI didReceiveRejectMsg:(NSString *) sessionID from:(XMPPJID *) from;

@end
