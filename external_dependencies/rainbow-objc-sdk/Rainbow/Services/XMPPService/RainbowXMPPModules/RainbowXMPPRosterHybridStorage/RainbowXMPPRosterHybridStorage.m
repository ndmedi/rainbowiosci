/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */
#import "DDLog.h"
#import "RainbowXMPPRosterHybridStorage.h"
#import "XMPPRosterMemoryStorage.h"
#import "XMPPUserMemoryStorageObject.h"
#import "XMPPRosterMemoryStoragePrivate.h"
#import "PINCache.h"
#import "XMPPUserMemoryStorageObject+HybridStorage.h"
#import "PINCache+OperationQueue.h"
#import "LoginManager.h"

@interface XMPPRoster (PrivateMethods)
-(void)_setHasRoster:(BOOL)flag;
@end

@interface RainbowXMPPRosterHybridStorage ()
@property (nonatomic, strong) PINCache *rosterCache;
@end

@implementation RainbowXMPPRosterHybridStorage
#pragma mark - Init/Dealloc
-(instancetype)initWithDispatchQueue:(dispatch_queue_t)queue {
    if((self = [super initWithDispatchQueue:queue])) {
        _rosterCache = [[PINCache alloc] initWithName:@"rosterCache"];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
    }
    
    return self;
}

#pragma mark - XMPPModule
-(BOOL)activate:(XMPPStream *)stream  {
    if ([super activate:stream]) {
        return YES;
    }
    return NO;
}

-(void)deactivate {
    [super deactivate];
    _memoryStorage = nil;
    _rosterCache = nil;
}

-(void)dealloc {
    _rosterCache = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
}

-(void) clearStorage {
    [_rosterCache removeAllObjects];
}

-(void) didLogout:(NSNotification *) notification {
    [_rosterCache.operationQueue cancelAllOperations];
}

-(void) loadUsersFromCacheForRoster:(XMPPRoster *) roster {
    dispatch_block_t block = ^{
        NSLog(@"Start loading users from cache");
        NSMutableArray *keys = [NSMutableArray array];
        [_rosterCache.diskCache enumerateObjectsWithBlock:^(NSString * _Nonnull key, NSURL * _Nullable fileURL, BOOL * _Nonnull stop) {
            [keys addObject:key];
        }];
        

        dispatch_sync(_memoryStorage.parent.moduleQueue, ^{
            NSMutableArray <XMPPUserMemoryStorageObject*> * allUsers = [NSMutableArray array];
            // BeginPopulation (Stream + Version)
        
            [_memoryStorage beginRosterPopulationForXMPPStream:_stream withVersion:_rosterVersion];
            
            for (NSString *key in keys) {
                XMPPUserMemoryStorageObject *user = [_rosterCache objectForKey:key];
                //                NSLog(@"Loaded user is key %@ %@",key,user);
                [allUsers addObject:user];
            }
        
            [_memoryStorage populateRosterItems:allUsers];
            
            // EndPopulation
            [_memoryStorage endRosterPopulationForXMPPStream:_stream];
            [roster _setHasRoster:YES];
            NSLog(@"End loading users from cache");
            [multicastDelegate xmppRosterDidEndLoadingCache:nil];
        });
    };
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

-(void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
//    NSLog(@"[%@] %@ args %@",THIS_FILE, THIS_METHOD, presence);
}

-(void)xmppRoster:(XMPPRoster *)sender didReceiveRosterItem:(NSXMLElement *)item {
//    NSLog(@"[%@] %@ args : %@",THIS_FILE, THIS_METHOD, item);
}

- (void)xmppRosterDidChange:(XMPPRosterMemoryStorage *)sender {
//    NSLog(@"[%@] %@ args : %@",THIS_FILE, THIS_METHOD, sender);
}

- (void)xmppRosterDidPopulate:(XMPPRosterMemoryStorage *)sender {
    dispatch_block_t block = ^{
        for (XMPPUserMemoryStorageObject *user in sender.unsortedUsers) {
            [_rosterCache setObjectAsync:user forKey:user.jid.bare completion:nil];
        }
    };
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didAddUser:(XMPPUserMemoryStorageObject *)user {
    XMPPUserMemoryStorageObject *tmpUser = [user copy];
    dispatch_block_t block = ^{
        [_rosterCache setObjectAsync:tmpUser forKey:tmpUser.jid.bare completion:nil];
    };
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didUpdateUser:(XMPPUserMemoryStorageObject *)user {
    XMPPUserMemoryStorageObject *tmpUser = [user copy];
    dispatch_block_t block = ^{
        [_rosterCache setObjectAsync:tmpUser forKey:tmpUser.jid.bare completion:nil];
    };
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didRemoveUser:(XMPPUserMemoryStorageObject *)user {
    dispatch_block_t block = ^{
        [_rosterCache removeObjectForKeyAsync:user.jid.bare completion:nil];
    };
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didAddResource:(XMPPResourceMemoryStorageObject *)resource withUser:(XMPPUserMemoryStorageObject *)user {
    XMPPUserMemoryStorageObject *tmpUser = [user copy];
    dispatch_block_t block = ^{
        [_rosterCache setObjectAsync:tmpUser forKey:tmpUser.jid.bare completion:nil];
    };
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didUpdateResource:(XMPPResourceMemoryStorageObject *)resource withUser:(XMPPUserMemoryStorageObject *)user {
    XMPPUserMemoryStorageObject *tmpUser = [user copy];
    dispatch_block_t block = ^{
        [_rosterCache setObjectAsync:tmpUser forKey:tmpUser.jid.bare completion:nil];
    };
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didRemoveResource:(XMPPResourceMemoryStorageObject *)resource withUser:(XMPPUserMemoryStorageObject *)user {
    XMPPUserMemoryStorageObject *tmpUser = [user copy];
    dispatch_block_t block = ^{
        [_rosterCache setObjectAsync:tmpUser forKey:tmpUser.jid.bare completion:nil];
    };
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}

@end
