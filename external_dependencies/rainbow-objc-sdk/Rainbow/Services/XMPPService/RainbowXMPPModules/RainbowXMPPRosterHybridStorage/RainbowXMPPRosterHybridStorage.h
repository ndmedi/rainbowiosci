/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPModule.h"
#import "XMPPUserMemoryStorageObject.h"
#import "XMPPRosterMemoryStorage.h"
#import "XMPPRoster.h"

@interface RainbowXMPPRosterHybridStorage : XMPPModule
@property (nonatomic, weak) XMPPRosterMemoryStorage *memoryStorage;
@property (nonatomic, weak) XMPPStream *stream;
@property (nonatomic, strong) NSString *rosterVersion;
-(void) loadUsersFromCacheForRoster:(XMPPRoster *) roster;
-(void) clearStorage;
@end

@protocol RainbowXMPPRosterHybridStorageDelegate
@required
 -(void) xmppRosterDidEndLoadingCache:(XMPPRoster *) roster;
@end
