/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPStreamWebSockets.h"
#import "SRWebSocket.h"
#import "NSArray+LinqExtensions.h"
#import "NSDictionary+LinqExtensions.h"

#import "XMPPStream+Internal.h"
#import "XMPPStreamInternal.h"
#import "XMPPInternal.h"
#import "XMPPLogging.h"

#import "DDLog.h"
#import "DDTTYLogger.h"

#import "XMPPParser+Copying.h"
#import "XMPP.h"

#import "Tools+Internal.h"
#import "Server+Internal.h"
#import <objc/runtime.h>
#import "RainbowUserDefaults.h"
#import "SRWebSocket+closeNetworkLost.h"
#import "RainbowSecurityPolicy.h"
#import <libxml/parser.h>
#import "ApiUrlManagerService.h"
#import "NSString+URLEncode.h"

@interface XMPPStreamWebSockets () <SRWebSocketDelegate> {
    BOOL didResetConnection;
}

@property (nonatomic) BOOL isStreamFeaturesResponseReceived;
@property (nonatomic, strong) SRWebSocket *webSocket;

@property (nonatomic, strong) NSMutableDictionary *rootNodeForParser;
@property (nonatomic, strong) dispatch_queue_t parserQueue;

@property (nonatomic, strong) NSOperationQueue *networkOperationQueue;

@property (nonatomic, strong) NSOperationQueue *internalQueue;

@property (nonatomic, strong) NSObject *nodeForParserMutex;

@property (nonatomic, strong) NSString *customResourceName;
@end

@implementation XMPPStreamWebSockets

- (void)commonInit {
    [super commonInit];
    if(!_networkOperationQueue){
        _networkOperationQueue = [NSOperationQueue new];
        _networkOperationQueue.name = @"XMPPNetworkOperationQueue";
        _networkOperationQueue.maxConcurrentOperationCount = 1;
    }
    if(!_internalQueue){
        _internalQueue = [NSOperationQueue new];
        _internalQueue.maxConcurrentOperationCount = 1;
    }
    if(!_rootNodeForParser){
        _rootNodeForParser = [NSMutableDictionary new];
    }
    _nodeForParserMutex = [NSObject new];
}

-(void) setResourceName:(NSString *)name {
    _customResourceName = name;
}

-(void) dealloc {
    [self disconnect];
    _webSocket.delegate = nil;
    _webSocket = nil;
    _networkOperationQueue = nil;
    _internalQueue = nil;
}

- (BOOL)connectWithTimeout:(NSTimeInterval)timeout error:(NSError **)errPtr {
    //We try in web socket so we extract the domain directly from the JID, we don't use the DNS to resolv it.
    self->hostName = _server.serverHostname;
    self->hostPort = _server.isSecure?433:80;
    
    return [super connectWithTimeout:timeout error:errPtr];
}

- (BOOL)connectToHost:(NSString *)host onPort:(UInt16)port withTimeout:(NSTimeInterval)timeout error:(NSError **)errPtr {
    NSAssert(dispatch_get_specific(self.xmppQueueTag), @"Invoked on incorrect queue");
    
    _internalQueue.suspended = NO;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@://%@/%@?x-rainbow-xmpp-dom=%@", _server.isSecure?@"wss":@"ws", self->hostName, _server.websocketPath, [XMPPJID jidWithString:_myUser.contact.jid].domain.urlEncode]];
    NSLog(@"Connecting through Web socket to %@", url);
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:timeout];
    NSString *clientName = [Tools isUsingSDK] ? @"sdk_ios" : @"ios";
    [req addValue:clientName forHTTPHeaderField:@"x-rainbow-client"];
    [req addValue:[Tools applicationVersion] forHTTPHeaderField:@"x-rainbow-client-version"];
    
    if([self enableBackgroundingOnSocket])
        req.networkServiceType = NSURLNetworkServiceTypeVoIP;
    
    // https://tools.ietf.org/html/rfc7395
    // During the WebSocket handshake, the client MUST include the value 'xmpp'
    // in the list of protocols for the 'Sec-WebSocket-Protocol'
    NSArray<NSString*> *requestedProtocols = @[@"xmpp"];
    if(_webSocket){
        _webSocket.delegate = nil;
        _webSocket = nil;
    }
    RainbowSecurityPolicy *policy = [RainbowSecurityPolicy new];
    policy.downloadManager = _certificateManager;
    _webSocket = [[SRWebSocket alloc] initWithURLRequest:req protocols:requestedProtocols securityPolicy:policy];
    _webSocket.delegate = self;
    [_webSocket setDelegateDispatchQueue:self.xmppQueue];
    [_webSocket open];
    
    @synchronized (_nodeForParserMutex) {
        [_rootNodeForParser removeAllObjects];
    }
    _parserQueue = dispatch_queue_create("xmpp.parser", NULL);
    
    BOOL result = YES;
    if (result && [self resetByteCountPerConnection]) {
        self->numberOfBytesSent = 0;
        self->numberOfBytesReceived = 0;
    }
    
    if(result) {
        [self startConnectTimeout:timeout];
    }
    
    return result;
}

-(void) forceCloseSocket {
    dispatch_block_t block = ^{ @autoreleasepool {
        NSLog(@"Force closing websocket");
        self->state = STATE_XMPP_DISCONNECTED;
        // Set a custom error, so it will be possible
        // possible to make distinction in the delegate xmppStreamDidDisconnect:withError:
        otherError = [NSError errorWithDomain:@"ForceCloseSocket" code:kForceCloseSocketErrorCode userInfo:nil];
        
        // Sometimes the server cut the websocket (after a ping) so in that case, the websocket is in closing state, and the delegate will not be triggered, we have to force it manually.
        if(_webSocket.readyState == SR_CLOSING || _webSocket.readyState == SR_CLOSED){
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincompatible-pointer-types"
            [self socketDidDisconnect:_webSocket withError:otherError];
#pragma clang diagnostic pop
        }
        
        [_webSocket closeCauseNetworkLost];
        [self cleanInternalParameters];
    }};
    
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_sync(self.xmppQueue, block);
}

-(void) cleanInternalParameters {
    @synchronized (_nodeForParserMutex) {
        [_rootNodeForParser removeAllObjects];
    }
    _parserQueue = nil;
    
    self->hostName = nil;
    self->hostPort = 0;
    _isStreamFeaturesResponseReceived = NO;
}

- (void)disconnect {
    dispatch_block_t block = ^{ @autoreleasepool {
        if (state != STATE_XMPP_DISCONNECTED) {
            [self->multicastDelegate xmppStreamWasToldToDisconnect:self];
            
            if (state == STATE_XMPP_RESOLVING_SRV) {
                state = STATE_XMPP_DISCONNECTED;
                [self->multicastDelegate xmppStreamDidDisconnect:self withError:nil];
            } else {
                self->state = STATE_XMPP_DISCONNECTED;
                NSString *message = @"<close xmlns='urn:ietf:params:xml:ns:xmpp-framing'/>";
                [_webSocket sendString:message error:nil];
                NSLog(@"[WebSocket] SEND [%ld] : %@", [message length], message);
                [_webSocket close];
                [self->multicastDelegate xmppStreamDidSendClosingStreamStanza:self];
                @synchronized (_nodeForParserMutex) {
                    [_rootNodeForParser removeAllObjects];
                }
                
                self->hostName = nil;
                self->hostPort = 0;
                _isStreamFeaturesResponseReceived = NO;
                
                [_networkOperationQueue cancelAllOperations];
                _internalQueue.suspended = YES;
                [_internalQueue cancelAllOperations];
            }
        }
    }};
    
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_sync(self.xmppQueue, block);
}

#pragma mark - Cocoa Async connection delegate
- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port {
}

- (void)socket:(GCDAsyncSocket *)sock didReceiveTrust:(SecTrustRef)trust completionHandler:(void (^)(BOOL shouldTrustPeer))completionHandler {
}

- (void)socketDidSecure:(GCDAsyncSocket *)sock {
}

/**
 * Called when a socket has completed reading the requested data. Not called if there is an error.
 **/
- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
}

/**
 * Called after data with the given tag has been successfully sent.
 **/
- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag {
    
}

/**
 * Called when a socket disconnects with or without error.
 **/
- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err {
    [super socketDidDisconnect:sock withError:err];
}


#pragma mark - WebSocket delegate
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    NSData* rawMessageData = nil;
    if ([message isKindOfClass: [NSData class]]) {
        rawMessageData = (NSData*)message;
    } else if ([message isKindOfClass: [NSString class]]) {
        NSString* strRawMessage = (NSString*)message;
        rawMessageData = [strRawMessage dataUsingEncoding:NSUTF8StringEncoding];
    } else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu-zero-variadic-macro-arguments"
        NSAssert(NO, @"Unknown transport response object");
#pragma clang diagnostic pop
    }
    XMPPParser *aParser = [[XMPPParser alloc] initWithDelegate:self delegateQueue:self.xmppQueue parserQueue:_parserQueue];
    [aParser parseData:rawMessageData];
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    NSLog(@"Websocket %@ didOpen", webSocket);
    
    [self endConnectTimeout];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincompatible-pointer-types"
    [self->multicastDelegate xmppStream:self socketDidConnect:webSocket];
#pragma clang diagnostic pop
    
    [self doPerformPlainAuthentication];
}
- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    NSLog(@"Websocket %@ didFailedWithError %@",webSocket, error);
    dispatch_async(self.xmppQueue, ^{
        @autoreleasepool {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincompatible-pointer-types"
            [super socketDidDisconnect:_webSocket withError:error];
#pragma clang diagnostic pop
            
            [self cleanInternalParameters];
        }
    });
}
- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    NSLog(@"Websocket %@ didCloseWithCode %ld reason %@, wasClean %d", webSocket, (long)code, reason, wasClean);
    dispatch_async(self.xmppQueue, ^{
        @autoreleasepool {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincompatible-pointer-types"
            [super socketDidDisconnect:_webSocket withError:nil];
#pragma clang diagnostic pop
        }
    });
}
- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    NSLog(@"Websocket %@ didReceivePong %@", webSocket, pongPayload);
}

#pragma mark - Action Performed
- (void)doPerformPlainAuthentication {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    // the callbacks will send the presense stanza
    [self sendStreamRequest];
}

- (void)sendStreamRequest {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    dispatch_block_t block = ^{ @autoreleasepool {
        static NSString* messageFormat = @"<open xmlns='urn:ietf:params:xml:ns:xmpp-framing' to='%@' version='1.0'/>";
        NSString* message = [NSString stringWithFormat: messageFormat, self.myJID.domain];
        NSLog(@"[WebSocket] SEND [%ld] : %@", [message length], message);
        [_webSocket sendString:message error:nil];
        if(self.isAuthenticated)
            self->state = STATE_XMPP_NEGOTIATING;
        [self setDidStartNegotiation:YES];
    }};
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_sync(self.xmppQueue, block);
}

- (void)sendAuthElement:(NSXMLElement *)element {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    dispatch_block_t block = ^{ @autoreleasepool {
        if (self->state == STATE_XMPP_AUTH) {
            NSString *outgoingStr = [element compactXMLString];
            NSData *outgoingData = [outgoingStr dataUsingEncoding:NSUTF8StringEncoding];
            
            NSLog(@"[WebSocket] SEND [%ld] : %@", [outgoingData length], outgoingStr);
            self->numberOfBytesSent += [outgoingData length];
            
            [_webSocket sendString:outgoingStr error:nil];
        } else {
            NSLog(@"Unable to send element while not in STATE_XMPP_AUTH: %@", [element compactXMLString]);
        }
    }};
    
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_sync(self.xmppQueue, block);
}

-(void) startBinding {
    SEL selector = @selector(xmppStreamWillBind:);
    self->state = STATE_XMPP_BINDING;
    
    if (![multicastDelegate hasDelegateThatRespondsToSelector:selector]) {
        [self sendBindRequest];
    } else {
        GCDMulticastDelegateEnumerator *delegateEnumerator = [multicastDelegate delegateEnumerator];
        
        dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(concurrentQueue, ^{ @autoreleasepool {
            
            __block id <XMPPCustomBinding> delegateCustomBinding = nil;
            
            id delegate;
            dispatch_queue_t dq;
            
            while ([delegateEnumerator getNextDelegate:&delegate delegateQueue:&dq forSelector:selector]) {
                dispatch_sync(dq, ^{ @autoreleasepool {
                    
                    delegateCustomBinding = [delegate xmppStreamWillBind:self];
                }});
                
                if (delegateCustomBinding) {
                    break;
                }
            }
            
            dispatch_async(self.xmppQueue, ^{ @autoreleasepool {
                
                if (delegateCustomBinding)
                    [self startCustomBinding:delegateCustomBinding];
                else
                    [self sendBindRequest];
            }});
        }});
    }
}

- (void)startCustomBinding:(id <XMPPCustomBinding>)delegateCustomBinding {
    NSLog(@"Start Custom binding %@", delegateCustomBinding);
    
    self->customBinding = delegateCustomBinding;
    
    NSError *bindError = nil;
    XMPPBindResult result = [self->customBinding start:&bindError];
    
    if (result == XMPP_BIND_CONTINUE) {
        // Expected result
        // Wait for reply from server, and forward to customBinding module.
    } else {
        if (result == XMPP_BIND_SUCCESS) {
            // It appears binding isn't needed (perhaps handled via auth)
            
            BOOL skipStartSessionOverride = NO;
            if ([customBinding respondsToSelector:@selector(shouldSkipStartSessionAfterSuccessfulBinding)]) {
                skipStartSessionOverride = [customBinding shouldSkipStartSessionAfterSuccessfulBinding];
            }
            
            [self continuePostBinding:skipStartSessionOverride];
        } else if (result == XMPP_BIND_FAIL_FALLBACK) {
            // Custom binding isn't available for whatever reason,
            // but the module has requested we fallback to standard binding.
            
            [self sendBindRequest];
        } else if (result == XMPP_BIND_FAIL_ABORT) {
            // Custom binding failed,
            // and the module requested we abort.
            
            self->otherError = bindError;
            [self forceCloseSocket];
        }
        
        self->customBinding = nil;
    }
}

- (void)sendBindRequest {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    dispatch_block_t block = ^{ @autoreleasepool {
        // Use resource name like "mobile_ios_1.0.150_D22C5EE0-40DC-4CD2-B849-324D2648E7B5"
        NSString *resourceName = [Tools resourceName];
        if([_customResourceName length] > 0) {
            resourceName = _customResourceName;
        }
        
        NSString *currentJid = self.myJID.bare;
        [self setMyJID:[XMPPJID jidWithString:currentJid resource:resourceName]];
        
        NSString *bindRequest = [NSString stringWithFormat:@"<iq type='set' id='_bind_auth_2' xmlns='jabber:client'><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'><resource>%@</resource></bind></iq>", resourceName];
        NSLog(@"[WebSocket] SEND [%ld] : %@", [bindRequest length], bindRequest);
        [_webSocket sendString:bindRequest error:nil];
    }};
    
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_sync(self.xmppQueue, block);
}

- (void)sendSessionRequest {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    dispatch_block_t block = ^{ @autoreleasepool {
        NSString* sessionRequest = @"<iq type='set' id='_session_auth_2' xmlns='jabber:client'><session xmlns='urn:ietf:params:xml:ns:xmpp-session'/></iq>";
        NSLog(@"[WebSocket] SEND [%ld] : %@", [sessionRequest length], sessionRequest);
        [_webSocket sendString:sessionRequest error:nil];
    }};
    
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_sync(self.xmppQueue, block);
}

#pragma mark - Process response
- (void)processStanza:(NSXMLElement*)element {
    dispatch_block_t block = ^{ @autoreleasepool {
        switch (self->state) {
            case STATE_XMPP_DISCONNECTED:
            case STATE_XMPP_CONNECTING: {
                [self handleAuthStreamOpenResponse:element];
                break;
            }
            case STATE_XMPP_RESOLVING_SRV: {
                break;
            }
            case STATE_XMPP_OPENING: {
                break;
            }
            case STATE_XMPP_NEGOTIATING: {
                [self checkForSecondStreamResponse:element];
                break;
            }
            case STATE_XMPP_STARTTLS_1: {
                break;
            }
            case STATE_XMPP_STARTTLS_2: {
                break;
            }
            case STATE_XMPP_POST_NEGOTIATION: {
                break;
            }
            case STATE_XMPP_REGISTERING: {
                break;
            }
            case STATE_XMPP_AUTH: {
                [self handleAuthResponse:element];
                break;
            }
            case STATE_XMPP_BINDING: {
                [self handleBindResponse:element];
                break;
            }
            case STATE_XMPP_START_SESSION: {
                [self handleSessionResponse:element];
                break;
            }
            case STATE_XMPP_CONNECTED: {
                [self handlePresenseOrMessageElement:element];
                break;
            }
            default: {
                break;
            }
        }
    }};
    
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_sync(self.xmppQueue, block);
}

#pragma mark - Handle Response

- (void)handleAuthStreamOpenResponse:(NSXMLElement *)element {
    NSLog(@"[WebSocket] RCV [%ld] : %@", [element compactXMLString].length, element);
    BOOL isStreamResponse = [[element name] isEqualToString: @"stream:features"];
    if (isStreamResponse) {
        self->rootElement = element;
        self->state = STATE_XMPP_CONNECTED;
        [self->multicastDelegate xmppStreamDidConnect:self];
        didResetConnection = NO;
    } else {
        // IDLE
        BOOL isOpenResponse = [[element name] isEqualToString: @"open"];
        BOOL isCloseResponse = [[element name] isEqualToString: @"close"];
        if (isOpenResponse || isCloseResponse) {
            NSLog(@"Nothing done with open/close stream message : %@", element);
        }
        else {
            if (!didResetConnection) {
                NSLog(@"Expecting auth message -> reset connection");
                didResetConnection = YES;
                [self forceCloseSocket];
            }
        }
    }
}

- (void)handleAuthResponse:(NSXMLElement *)element {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    NSLog(@"[WebSocket] RCV [%ld] : %@", [element compactXMLString].length, element);
    BOOL isChalenge = [[element name] isEqualToString:@"challenge"];
    if(isChalenge){
        [self handleAuth:element];
        return;
    }
    
    BOOL isSuccess = [[element name] isEqualToString: @"success"];
    if (isSuccess) {
        [self setIsAuthenticated:YES];
        self->state = STATE_XMPP_AUTH;
        [self sendStreamRequest];
    } else {
        self->state = STATE_XMPP_DISCONNECTED;
        [self->multicastDelegate xmppStream:self didNotAuthenticate:element];
        
        [self disconnect];
    }
}

- (void)checkForSecondStreamResponse:(NSXMLElement *)element {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    NSLog(@"[WebSocket] RCV [%ld] : %@", [element compactXMLString].length, element);
    if ([[element name] isEqualToString: @"stream:features"] && [element elementForName:@"bind"] != nil) {
        //    <stream:features xmlns="jabber:client" xmlns:stream="http://etherx.jabber.org/streams" version="1.0"><bind xmlns="urn:ietf:params:xml:ns:xmpp-bind"/><session xmlns="urn:ietf:params:xml:ns:xmpp-session"/><sm xmlns="urn:xmpp:sm:2"/><sm xmlns="urn:xmpp:sm:3"/><c xmlns="http://jabber.org/protocol/caps" hash="sha-1" node="http://www.process-one.net/en/ejabberd/" ver="6LZsyp9FYXV9NHsBmxJvPrDLTQs="/><register xmlns="http://jabber.org/features/iq-register"/></stream:features>
        
        _isStreamFeaturesResponseReceived = YES;
    }
    if (_isStreamFeaturesResponseReceived) {
        [self startBinding];
    }
}

- (void)handleBindResponse:(NSXMLElement *)element {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    NSLog(@"[WebSocket] RCV [%ld] : %@", [element compactXMLString].length, element);
    if(self->customBinding){
        [self handleCustomBinding:element];
    } else {
        BOOL invalid = NO;
        if (self->validatesResponses) {
            XMPPIQ *iq = [XMPPIQ iqFromElement:element];
            if (![idTracker invokeForElement:iq withObject:nil]) {
                invalid = YES;
            }
        }
        if (!invalid) {
            // The response from our binding request
            [self handleStandardBinding:element];
        }
    }
}

-(void) handleStandardBinding:(NSXMLElement *) element {
    NSLog(@"handleStandardBinding %@", element);
    if ([self isResponseToSkip: element]) {
        // skip unexpected stanza
        // TODO : fail or notify sentry
        return;
    }
    
    if (![self isSuccessfulBindResponse: element]) {
        self->state = STATE_XMPP_DISCONNECTED;
        [self->multicastDelegate xmppStream:self didNotAuthenticate:element];
        [self disconnect];
        return;
    }
    
    [self setMyJID_setByServer:[XMPPJID jidWithString:[self jidFromBindResponse: element]]];
    
    // Update state
    self->state = STATE_XMPP_START_SESSION;
    [self sendSessionRequest];
}

- (void)handleCustomBinding:(NSXMLElement *)response {
    NSLog(@"handleCustomBinding %@", response);
    
    NSError *bindError = nil;
    XMPPBindResult result = [self->customBinding handleBind:response withError:&bindError];
    
    if (result == XMPP_BIND_CONTINUE) {
        // Binding still in progress
    } else {
        if (result == XMPP_BIND_SUCCESS) {
            // Binding complete. Continue.
            
            BOOL skipStartSessionOverride = NO;
            if ([customBinding respondsToSelector:@selector(shouldSkipStartSessionAfterSuccessfulBinding)]) {
                skipStartSessionOverride = [customBinding shouldSkipStartSessionAfterSuccessfulBinding];
            }
            
            [self continuePostBinding:skipStartSessionOverride];
        } else if (result == XMPP_BIND_FAIL_FALLBACK) {
            // Custom binding failed for whatever reason,
            // but the module has requested we fallback to standard binding.
            
            [self sendBindRequest];
        }
        else if (result == XMPP_BIND_FAIL_ABORT) {
            // Custom binding failed,
            // and the module requested we abort.
            
            self->otherError = bindError;
            [self forceCloseSocket];
        }
        
        self->customBinding = nil;
    }
}

- (void)handleSessionResponse:(NSXMLElement *)element {
    //    <iq
    //        type="result"
    //        xmlns="jabber:client"
    //        id="_session_auth_2"
    //        xmlns:stream="http://etherx.jabber.org/streams"
    //        version="1.0"/>
    NSLog(@"[WebSocket] RCV [%ld] : %@", [element compactXMLString].length, element);
    if ([self isResponseToSkip: element]) {
        
        // skip non mathcing stanza
        // TODO : maybe notify sentry
        return;
    } else if ([self isSuccessfulSessionResponse: element]) {
        self->state = STATE_XMPP_CONNECTED;
        [self->multicastDelegate xmppStreamDidAuthenticate:self];
        _networkOperationQueue.suspended = NO;
    } else {
        
        self->state = STATE_XMPP_DISCONNECTED;
        [self->multicastDelegate xmppStream:self didNotAuthenticate:element];
        [self disconnect];
        return;
    }
}

- (void)handlePresenseOrMessageElement:(NSXMLElement *)element {
    BOOL shouldPrint = YES;
    if([[element name] isEqualToString:@"iq"]){
        XMPPIQ* iqResponse = [XMPPIQ iqFromElement: element];
        NSXMLElement *ping = [iqResponse elementForName:@"ping" xmlns:@"urn:xmpp:ping"];
        if(ping)
            shouldPrint = NO;
        NSXMLElement *vcard = [iqResponse elementForName:@"vCard" xmlns:@"vcard-temp"];
        if(vcard)
            shouldPrint = NO;
    }
    
    NSString *elementName = [element name];
    BOOL isPresense = [elementName isEqualToString: @"presence"];
    BOOL isMessage  = [elementName isEqualToString: @"message" ];
    BOOL isIQResponse = [elementName isEqualToString: @"iq"];
    BOOL isClose = [elementName isEqualToString: @"close"];
    BOOL isXEP198 = [customElementNames countForObject:elementName];
    
    
    if (shouldPrint){
        if(isMessage){
            // We must remove the body only for the log
            NSString *msg = [Tools anonymizeString:[element compactXMLString]];
            NSLog(@"[WebSocket] RCV [%ld] : %@", msg.length, msg);
        } else {
            NSLog(@"[WebSocket] RCV [%ld] : %@", [element compactXMLString].length, element);
        }
    }
    
    if (isPresense) {
        XMPPPresence* presenseResponse = [XMPPPresence presenceFromElement: element];
        [self receivePresence:presenseResponse];
    } else if (isMessage) {
        XMPPMessage* messageResponse = [XMPPMessage messageFromElement: element];
        [self receiveMessage:messageResponse];
    } else if (isIQResponse) {
        XMPPIQ* iqResponse = [XMPPIQ iqFromElement: element];
        [self receiveIQ:iqResponse];
    } else if (isClose && [[element xmlns] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-framing"]) {
        NSLog(@"close received on websocket");
        // We cannot call [self disconnect] because it calls the delegate xmppStreamWasToldToDisconnect
        // Which stop the XMPPReconnect module.
        // And in our case, we expect the reconnection !
        [self forceCloseSocket];
    } else if (isXEP198){
        [self receiveCustomElement:element];
    } else {
        NSLog(@"NOT HANDLED STANZA %@", element);
        // IDLE
        // Skipping other response stanza
        
        /*
         <stream:error xmlns:stream="http://etherx.jabber.org/streams"><policy-violation xmlns="urn:ietf:params:xml:ns:xmpp-streams"/><text xmlns="urn:ietf:params:xml:ns:xmpp-streams" lang="">Too many unacked stanzas</text></stream:error>
         */
        
        if([elementName isEqualToString:@"stream:error"]){
            [self->multicastDelegate xmppStream:self didReceiveError:element];
        }
    }
}

#pragma mark - Some facilitors
-(BOOL) isResponseToSkip:(id) nsXMLElement {
    NSParameterAssert([nsXMLElement isKindOfClass: [NSXMLElement class]]);
    
    NSXMLElement* element = (NSXMLElement*)nsXMLElement;
    NSString* elementName = [element name];
    
    BOOL result = (![elementName isEqualToString: @"iq"]);
    
    return result;
}

-(BOOL)isSuccessfulBindResponse:(id)nsXMLElement {
    NSParameterAssert([nsXMLElement isKindOfClass: [NSXMLElement class]]);
    NSXMLElement* element = (NSXMLElement*)nsXMLElement;
    XMPPIQ* responseIq = [XMPPIQ iqFromElement: element];
    
    if ([responseIq isErrorIQ]) {
        return NO;
    } else if ([responseIq isResultIQ]) {
        NSXMLNode* idAttribute = [element attributeForName: @"id"];
        NSString* idValue = [idAttribute stringValue];
        
        return [idValue isEqualToString: @"_bind_auth_2"];
    } else {
        return NO;
    }
}

- (NSString*)jidFromBindResponse:(id)nsXMLElement {
    NSParameterAssert([nsXMLElement isKindOfClass: [NSXMLElement class]]);
    NSXMLElement* element = (NSXMLElement*)nsXMLElement;
    XMPPIQ* responseIq = [XMPPIQ iqFromElement: element];
    
    NSXMLElement* bindElement = [responseIq childElement];
    NSXMLElement* jidElement = [[bindElement children] firstObject];
    
    NSString* rawJid = [jidElement stringValue];
    
    return rawJid;
}

-(BOOL)isSuccessfulSessionResponse:(id)nsXMLElement {
    NSParameterAssert([nsXMLElement isKindOfClass: [NSXMLElement class]]);
    NSXMLElement* element = (NSXMLElement*)nsXMLElement;
    
    XMPPIQ* parsedIq = [XMPPIQ iqFromElement: element];
    if ([parsedIq isErrorIQ]) {
        return NO;
    } else if ([parsedIq isResultIQ]) {
        return [[parsedIq elementID] isEqualToString: @"_session_auth_2"];
    } else {
        return NO;
    }
}

#pragma mark - Parser delegate
/**
 * Called when the xmpp parser has read in the entire root element.
 **/
- (void)xmppParser:(XMPPParser *)sender didReadRoot:(NSXMLElement *)root {
    @synchronized(_nodeForParserMutex) {
        [_rootNodeForParser setObject:root forKey:sender];
    }
}

- (void)xmppParser:(XMPPParser *)sender didReadElement:(NSXMLElement *)element {
    NSXMLElement *stanzaRoot;
    @synchronized(_nodeForParserMutex) {
        stanzaRoot = _rootNodeForParser[sender];
    }
    [stanzaRoot addChild:element];
    NSString *elementName = [element name];
    if ([elementName isEqualToString:@"stream:error"] || [elementName isEqualToString:@"error"]) {
        NSLog(@"FOUND AN ERROR IN ELEMENT %@", stanzaRoot);
        [self->multicastDelegate xmppStream:self didReceiveError:element];
        
        return;
    }
}

- (void)xmppParserDidParseData:(XMPPParser *)sender {
}

- (void)xmppParserDidEnd:(XMPPParser *)sender {
    
    XMPPParser *retainedSender = sender;
    NSXMLElement *stanzaRoot;
    
    @synchronized(_nodeForParserMutex) {
        stanzaRoot = [_rootNodeForParser[retainedSender] copy];
    }
    // Workaround for challenge answer, the xmppParser don't return the content of the challenge for a unknown reason, but we have it in the context so extract it manually.
    if([[stanzaRoot name] isEqualToString:@"challenge"]){
        NSString *challengeContent = [NSString stringWithCString:(const char *)retainedSender->parserCtxt->node->last->last->content encoding:NSUTF8StringEncoding];
        [stanzaRoot setStringValue:challengeContent];
    }
    @synchronized(_nodeForParserMutex) {
        [_rootNodeForParser removeObjectForKey:retainedSender];
    }
    
    
    [_internalQueue addOperationWithBlock:^{
        [self processStanza:stanzaRoot];
        
    }];
}

- (void)xmppParser:(XMPPParser *)sender didFail:(NSError *)error {
    NSLog(@"Failed to parse stanza %@", [error localizedDescription]);
    self->state = STATE_XMPP_DISCONNECTED;
    [_webSocket close];
}

#pragma mark - Overrided methods

/**
 * This method handles sending an XML stanza.
 * If the XMPPStream is not connected, this method does nothing.
 **/
- (void)sendElement:(NSXMLElement *)element
{
    if (element == nil) return;
    
    dispatch_block_t block = ^{ @autoreleasepool {
        [self sendElement:element withTag:WS_TAG_XMPP_WRITE_STREAM];
    }};
    
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_async(self.xmppQueue, block);
}


- (void)sendIQ:(XMPPIQ *)iq withTag:(long)tag {
    NSAssert(dispatch_get_specific(self.xmppQueueTag), @"Invoked on incorrect queue");
    
    // We're getting ready to send an IQ.
    // Notify delegates to allow them to optionally alter/filter the outgoing IQ.
    
    SEL selector = @selector(xmppStream:willSendIQ:);
    
    if (![multicastDelegate hasDelegateThatRespondsToSelector:selector]) {
        // None of the delegates implement the method.
        // Use a shortcut.
        
        [self continueSendIQ:iq withTag:tag];
    } else {
        // Notify all interested delegates.
        // This must be done serially to allow them to alter the element in a thread-safe manner.
        
        GCDMulticastDelegateEnumerator *delegateEnumerator = [multicastDelegate delegateEnumerator];
        
        dispatch_async(self->willSendIqQueue, ^{ @autoreleasepool {
            
            // Allow delegates to modify and/or filter outgoing element
            
            __block XMPPIQ *modifiedIQ = iq;
            
            id del;
            dispatch_queue_t dq;
            
            while (modifiedIQ && [delegateEnumerator getNextDelegate:&del delegateQueue:&dq forSelector:selector]) {
#if DEBUG
                {
                    char methodReturnType[32];
                    
                    Method method = class_getInstanceMethod([del class], selector);
                    method_getReturnType(method, methodReturnType, sizeof(methodReturnType));
                    
                    if (strcmp(methodReturnType, @encode(XMPPIQ*)) != 0)
                    {
                        NSAssert(NO, @"Method xmppStream:willSendIQ: is no longer void (see XMPPStream.h). "
                                 @"Culprit = %@", NSStringFromClass([del class]));
                    }
                }
#endif
                
                dispatch_sync(dq, ^{ @autoreleasepool {
                    
                    modifiedIQ = [del xmppStream:self willSendIQ:modifiedIQ];
                    
                }});
            }
            
            if (modifiedIQ) {
                dispatch_async(self.xmppQueue, ^{ @autoreleasepool {
                    [self continueSendIQ:modifiedIQ withTag:tag];
                }});
            }
        }});
    }
}

- (void)sendMessage:(XMPPMessage *)message withTag:(long)tag {
    NSAssert(dispatch_get_specific(self.xmppQueueTag), @"Invoked on incorrect queue");
    
    // We're getting ready to send a message.
    // Notify delegates to allow them to optionally alter/filter the outgoing message.
    
    SEL selector = @selector(xmppStream:willSendMessage:);
    
    if (![multicastDelegate hasDelegateThatRespondsToSelector:selector]) {
        // None of the delegates implement the method.
        // Use a shortcut.
        
        [self continueSendMessage:message withTag:tag];
    } else {
        // Notify all interested delegates.
        // This must be done serially to allow them to alter the element in a thread-safe manner.
        
        GCDMulticastDelegateEnumerator *delegateEnumerator = [multicastDelegate delegateEnumerator];
        
        dispatch_async(willSendMessageQueue, ^{ @autoreleasepool {
            
            // Allow delegates to modify outgoing element
            
            __block XMPPMessage *modifiedMessage = message;
            
            id del;
            dispatch_queue_t dq;
            
            while (modifiedMessage && [delegateEnumerator getNextDelegate:&del delegateQueue:&dq forSelector:selector]) {
#if DEBUG
                {
                    char methodReturnType[32];
                    
                    Method method = class_getInstanceMethod([del class], selector);
                    method_getReturnType(method, methodReturnType, sizeof(methodReturnType));
                    
                    if (strcmp(methodReturnType, @encode(XMPPMessage*)) != 0)
                    {
                        NSAssert(NO, @"Method xmppStream:willSendMessage: is no longer void (see XMPPStream.h). "
                                 @"Culprit = %@", NSStringFromClass([del class]));
                    }
                }
#endif
                
                dispatch_sync(dq, ^{ @autoreleasepool {
                    
                    modifiedMessage = [del xmppStream:self willSendMessage:modifiedMessage];
                    
                }});
            }
            
            if (modifiedMessage) {
                dispatch_async(self.xmppQueue, ^{ @autoreleasepool {
                    [self continueSendMessage:modifiedMessage withTag:tag];
                }});
            }
        }});
    }
}

- (void)sendPresence:(XMPPPresence *)presence withTag:(long)tag {
    NSAssert(dispatch_get_specific(self.xmppQueueTag), @"Invoked on incorrect queue");
    
    // We're getting ready to send a presence element.
    // Notify delegates to allow them to optionally alter/filter the outgoing presence.
    
    SEL selector = @selector(xmppStream:willSendPresence:);
    
    if (![multicastDelegate hasDelegateThatRespondsToSelector:selector]) {
        // None of the delegates implement the method.
        // Use a shortcut.
        
        [self continueSendPresence:presence withTag:tag];
    } else {
        // Notify all interested delegates.
        // This must be done serially to allow them to alter the element in a thread-safe manner.
        
        GCDMulticastDelegateEnumerator *delegateEnumerator = [multicastDelegate delegateEnumerator];
        
        dispatch_async(willSendPresenceQueue, ^{ @autoreleasepool {
            
            // Allow delegates to modify outgoing element
            
            __block XMPPPresence *modifiedPresence = presence;
            
            id del;
            dispatch_queue_t dq;
            
            while (modifiedPresence && [delegateEnumerator getNextDelegate:&del delegateQueue:&dq forSelector:selector]) {
#if DEBUG
                {
                    char methodReturnType[32];
                    
                    Method method = class_getInstanceMethod([del class], selector);
                    method_getReturnType(method, methodReturnType, sizeof(methodReturnType));
                    
                    if (strcmp(methodReturnType, @encode(XMPPPresence*)) != 0)
                    {
                        NSAssert(NO, @"Method xmppStream:willSendPresence: is no longer void (see XMPPStream.h). "
                                 @"Culprit = %@", NSStringFromClass([del class]));
                    }
                }
#endif
                
                dispatch_sync(dq, ^{ @autoreleasepool {
                    
                    modifiedPresence = [del xmppStream:self willSendPresence:modifiedPresence];
                    
                }});
            }
            
            if (modifiedPresence) {
                dispatch_async(self.xmppQueue, ^{ @autoreleasepool {
                    [self continueSendPresence:modifiedPresence withTag:tag];
                }});
            }
        }});
    }
}

- (void)continueSendElement:(NSXMLElement *)element withTag:(long)tag {
    NSAssert(dispatch_get_specific(self.xmppQueueTag), @"Invoked on incorrect queue");
    
    NSString *outgoingStr = [element compactXMLString];
    NSData *outgoingData = [outgoingStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"[WebSocket] SEND [%ld] : %@", [outgoingData length], outgoingStr);
    
    self->numberOfBytesSent += [outgoingData length];
    
    [_webSocket sendData:outgoingData error:nil];
    
    if ([self->customElementNames countForObject:[element name]]) {
        [self->multicastDelegate xmppStream:self didSendCustomElement:element];
    }
}

- (void)continueSendIQ:(XMPPIQ *)iq withTag:(long)tag {
    NSAssert(dispatch_get_specific(self.xmppQueueTag), @"Invoked on incorrect queue");
    [_networkOperationQueue addOperationWithBlock:^{
        NSString *outgoingStr = [iq compactXMLString];
        if(!iq.elementID){
            NSLog(@"ERROR !!! IQ without element ID %@", iq);
        }
        NSData *outgoingData = [outgoingStr dataUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"[WebSocket] SEND [%ld] : %@", [outgoingData length], outgoingStr);
        [_webSocket sendData:outgoingData error:nil];
        
        dispatch_sync(self.xmppQueue, ^{
            self->numberOfBytesSent += [outgoingData length];
            [self->multicastDelegate xmppStream:self didSendIQ:iq];
        });
    }];
}

- (void)continueSendMessage:(XMPPMessage *)message withTag:(long)tag {
    NSAssert(dispatch_get_specific(self.xmppQueueTag), @"Invoked on incorrect queue");
    __weak __typeof__(self) weakSelf = self;
    if(!message.elementID){
        NSAssert(message.elementID, @"ERROR !!! Message without element ID, this message must be fixed !");
        NSLog(@"ERROR !!! Message without element ID %@", message);
    }
    [_networkOperationQueue addOperationWithBlock:^{
        NSString *outgoingStr = [message compactXMLString];
        NSData *outgoingData = [outgoingStr dataUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"[WebSocket] SEND [%ld] : %@", [outgoingData length], [Tools anonymizeString:outgoingStr]);
        [_webSocket sendData:outgoingData error:nil];
        
        dispatch_sync(self.xmppQueue, ^{
            self->numberOfBytesSent += [outgoingData length];
            [self->multicastDelegate xmppStream:weakSelf didSendMessage:message];
        });
    }];
}

- (void)continueSendPresence:(XMPPPresence *)presence withTag:(long)tag {
    NSAssert(dispatch_get_specific(self.xmppQueueTag), @"Invoked on incorrect queue");
    [_networkOperationQueue addOperationWithBlock:^{
        NSString *outgoingStr = [presence compactXMLString];
        NSData *outgoingData = [outgoingStr dataUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"[WebSocket] SEND [%ld] : %@", [outgoingData length], outgoingStr);
        [_webSocket sendData:outgoingData error:nil];
        
        dispatch_sync(self.xmppQueue, ^{
            
            self->numberOfBytesSent += [outgoingData length];
            
            // Update myPresence if this is a normal presence element.
            // In other words, ignore presence subscription stuff, MUC room stuff, etc.
            //
            // We use the built-in [presence type] which guarantees lowercase strings,
            // and will return @"available" if there was no set type (as available is implicit).
            
            NSString *type = [presence type];
            if ([type isEqualToString:@"available"] || [type isEqualToString:@"unavailable"]) {
                if ([presence toStr] == nil && self->myPresence != presence){
                    [presence removeElementForName:@"status"];
                    self->myPresence = presence;
                }
            }
            
            [self->multicastDelegate xmppStream:self didSendPresence:presence];
        });
    }];
}

- (BOOL)supportsAuthenticationMechanism:(NSString *)mechanismType {
    __block BOOL result = NO;
    dispatch_block_t block = ^{ @autoreleasepool {
        if (state >= STATE_XMPP_POST_NEGOTIATION) {
            NSXMLElement *mech = [self->rootElement elementForName:@"mechanisms" xmlns:@"urn:ietf:params:xml:ns:xmpp-sasl"];
            NSArray *mechanisms = [mech elementsForName:@"mechanism"];
            for (NSXMLElement *mechanism in mechanisms) {
                if ([[mechanism stringValue] isEqualToString:mechanismType]) {
                    result = YES;
                    break;
                }
            }
        }
    }};
    
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_sync(self.xmppQueue, block);
    
    return result;
}

- (void)continuePostBinding:(BOOL)skipStartSessionOverride {
    NSLog(@"continuePostBinding %@", NSStringFromBOOL(skipStartSessionOverride));
    
    // And we may now have to do one last thing before we're ready - start an IM session
    NSXMLElement *features = [rootElement elementForName:@"stream:features"];
    
    // Check to see if a session is required
    // Don't forget about that NSXMLElement bug you reported to apple (xmlns is required or element won't be found)
    NSXMLElement *f_session = [features elementForName:@"session" xmlns:@"urn:ietf:params:xml:ns:xmpp-session"];
    
    if (f_session && !self->skipStartSession && !skipStartSessionOverride) {
        NSXMLElement *session = [NSXMLElement elementWithName:@"session"];
        [session setXmlns:@"urn:ietf:params:xml:ns:xmpp-session"];
        
        XMPPIQ *iq = [XMPPIQ iqWithType:@"set" elementID:[self generateUUID]];
        [iq addChild:session];
        
        NSString *outgoingStr = [iq compactXMLString];
        NSData *outgoingData = [outgoingStr dataUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"[WebSocket] SEND [%ld] : %@", [outgoingData length], outgoingStr);
        numberOfBytesSent += [outgoingData length];
        
        [_webSocket sendData:outgoingData error:nil];
        
        [self->idTracker addElement:iq
                             target:nil
                           selector:NULL
                            timeout:XMPPIDTrackerTimeoutNone];
        
        // Update state
        self->state = STATE_XMPP_START_SESSION;
    } else {
        // Revert back to connected state (from binding state)
        self->state = STATE_XMPP_CONNECTED;
        _networkOperationQueue.suspended = NO;
        [self->multicastDelegate xmppStreamDidAuthenticate:self];
    }
}

- (void)sendBindElement:(NSXMLElement *)element {
    dispatch_block_t block = ^{ @autoreleasepool {
        if (self->state == STATE_XMPP_BINDING) {
            NSString *outgoingStr = [element compactXMLString];
            NSData *outgoingData = [outgoingStr dataUsingEncoding:NSUTF8StringEncoding];
            
            NSLog(@"[WebSocket] SEND [%ld] : %@", [outgoingData length], outgoingStr);
            numberOfBytesSent += [outgoingData length];
            
            [_webSocket sendData:outgoingData error:nil];
        } else {
            NSLog(@"Unable to send element while not in STATE_XMPP_BINDING: %@", [element compactXMLString]);
        }
    }};
    
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_async(self.xmppQueue, block);
}

-(void) simulateDidReceiveMessage:(XMPPMessage *) message {
    dispatch_block_t block = ^{ @autoreleasepool {
        [self receiveMessage:message];
    }};
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_async(self.xmppQueue, block);
}

- (void)receiveMessage:(XMPPMessage *)message {
    NSAssert(dispatch_get_specific(self.xmppQueueTag), @"Invoked on incorrect queue");
    
    // We're getting ready to receive a message.
    // Notify delegates to allow them to optionally alter/filter the incoming message.
    
    SEL selector = @selector(xmppStream:willReceiveMessage:);
    
    if (![multicastDelegate hasDelegateThatRespondsToSelector:selector]) {
        // None of the delegates implement the method.
        // Use a shortcut.
        
        if (self->willReceiveStanzaQueue) {
            // But still go through the stanzaQueue in order to guarantee in-order-delivery of all received stanzas.
            
            dispatch_async(self->willReceiveStanzaQueue, ^{
                dispatch_async(self.xmppQueue, ^{ @autoreleasepool {
                    [super continueReceiveMessage:message];
                }});
            });
        } else {
            [super continueReceiveMessage:message];
        }
    } else {
        // Notify all interested delegates.
        // This must be done serially to allow them to alter the element in a thread-safe manner.
        
        GCDMulticastDelegateEnumerator *delegateEnumerator = [multicastDelegate delegateEnumerator];
        
        if (self->willReceiveStanzaQueue == NULL)
            self->willReceiveStanzaQueue = dispatch_queue_create("xmpp.willReceiveStanza", DISPATCH_QUEUE_SERIAL);
        
        dispatch_async(self->willReceiveStanzaQueue, ^{ @autoreleasepool {
            
            // Allow delegates to modify incoming element
            
            __block XMPPMessage *modifiedMessage = message;
            
            id del;
            dispatch_queue_t dq;
            
            while (modifiedMessage && [delegateEnumerator getNextDelegate:&del delegateQueue:&dq forSelector:selector]) {
                dispatch_sync(dq, ^{ @autoreleasepool {
                    
                    modifiedMessage = [del xmppStream:self willReceiveMessage:modifiedMessage];
                    
                }});
            }
            
            dispatch_async(self.xmppQueue, ^{ @autoreleasepool {
                if (modifiedMessage)
                    [self continueReceiveMessage:modifiedMessage];
                else
                    [self->multicastDelegate xmppStreamDidFilterStanza:self];
            }});
        }});
    }
}

- (void)receiveCustomElement:(NSXMLElement *)customElement {
    NSAssert(dispatch_get_specific(self.xmppQueueTag), @"Invoked on incorrect queue");
    
    if (self->willReceiveStanzaQueue == NULL)
        self->willReceiveStanzaQueue = dispatch_queue_create("xmpp.willReceiveStanza", DISPATCH_QUEUE_SERIAL);
    
    // Always go through the stanzaQueue in order to guarantee in-order-delivery of all received stanzas.
    dispatch_async(self->willReceiveStanzaQueue, ^{
        dispatch_async(self.xmppQueue, ^{ @autoreleasepool {
            NSString *elementName = [customElement name];
            if ([customElementNames countForObject:elementName]) {
                [self->multicastDelegate xmppStream:self didReceiveCustomElement:customElement];
            }
        }});
    });
}

@end
