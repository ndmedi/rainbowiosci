/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

@class XMPPService;
@class Contact;
@class Message;
@class Room;
@class Group;
@class NomadicStatus;
@class CallForwardStatus;
@class CallEvent;
#import "Message.h"
@class ConferenceInfo;

@protocol XMPPServiceLoginDelegate <NSObject>
-(void) xmppServiceDidConnect:(XMPPService *) xmmpService;
-(void) xmppServiceDidDisconnect:(XMPPService *) xmmpService withError:(NSError *) error;
-(void) xmppServiceDidLostConnection:(XMPPService *) xmmpService;
-(void) xmppService:(XMPPService *) xmppService failedToAuthenticateWithError:(NSError *) error;
-(void) xmppServiceDidResumeStream:(XMPPService *) xmppService;
-(void) xmppServiceDidReceiveChangePasswordMessage:(XMPPService *) xmppService;
@end

@protocol XMPPServiceConversationDelegate <NSObject>
-(void) xmppService:(XMPPService *) service willSendMessage:(Message *) message;
-(void) xmppService:(XMPPService *) service didSendMessage:(Message *) message;
-(void) xmppService:(XMPPService *) service didReceiveMessage:(Message *) message;
-(void) xmppService:(XMPPService *) service didReceiveCarbonCopyMessage:(Message *)message;
-(void) xmppService:(XMPPService *) service didReceiveComposingMessage:(Message *)message;

-(void) xmppService:(XMPPService *) service didReceiveArchivedMessage:(Message *)message;

-(void) xmppService:(XMPPService *) service didReceiveDeliveryState:(MessageDeliveryState) state at:(NSDate*) datetime forMessage:(Message *) message via:(Peer*) via isOutgoing:(BOOL) isOutgoing;
-(void) xmppService:(XMPPService *) service didReceiveMuteConversation:(NSString *) conversationID;
-(void) xmppService:(XMPPService *) service didReceiveUnMuteConversation:(NSString *) conversationID;

-(void) xmppService:(XMPPService *) service didRetreiveNewLastMessageFromArchives:(Message *) lastMessage;
-(void) xmppService:(XMPPService *) service didReceiveDeleteConversationID:(NSString *) conversationID;
-(void) xmppService:(XMPPService *) service didRemoveAllMessagesForPeer:(Peer *) peer;

-(void) xmppService:(XMPPService *) service didReceiveDeleteAllMessagesInConversationWithPeerJID:(NSString *) peerJID;
-(void) xmppService:(XMPPService *) service didReceiveDeleteMessageID:(NSString *) messageID inConversationWithPeerJID:(NSString *) peerJID;

-(void) xmppService:(XMPPService *)service didReceiveChangedInConversationWithPeer:(Peer *) peer;

-(void) xmppService:(XMPPService *) service didReceiveMarkAllAsReadInConversationWithPeerJID:(NSString *) peerJID andReadByMe: (BOOL) isReadByMe;
@end

@protocol XMPPServiceRoomsDelegate <NSObject>
-(void) xmppService:(XMPPService *) service didCreateRoom:(Room *) room;
-(void) xmppService:(XMPPService *) service didJoinRoom:(Room *) room;
-(void) xmppService:(XMPPService *) service didLeaveRoom:(Room *) room;
-(void) xmppService:(XMPPService *) service didDestroyRoom:(Room *) room;
-(void) xmppService:(XMPPService *) service didFailedToDestroyRoom:(Room *) room withError:(NSError *) error;
-(void) xmppService:(XMPPService *) service occupant:(Contact *) occupant didJoinRoom:(Room *) room;
-(void) xmppService:(XMPPService *) service occupant:(Contact *) occupant didLeaveRoom:(Room *) room;
-(void) xmppService:(XMPPService *)service didChangeTopic:(NSString *) newTopic forRoom:(Room *)room;
-(void) xmppService:(XMPPService *)service didChangeName:(NSString *) newName forRoom:(Room *)room;
-(void) xmppService:(XMPPService *)service didUpdateAvatarInRoom:(Room *)room withDate:(NSDate *) newUpdateDate;
-(void) xmppService:(XMPPService *)service didDeleteAvatarInRoom:(Room *)room;
-(void) xmppService:(XMPPService *)service didChangedConferenceScheduleInRoom:(Room *)room startDate:(NSDate *)start endDate:(NSDate *)end;
-(void) xmppService:(XMPPService *)service didChangeCustomData:(NSDictionary *) newData forRoom:(Room *)room;

@end

@protocol XMPPServiceConferenceDelegate <NSObject>
-(void) xmppService:(XMPPService *) service didReceiveConferenceInfo:(ConferenceInfo *) conferenceInfo;
-(void) xmppService:(XMPPService *) service didReceiveMessage:(Message *)message;
@end

@protocol XMPPServiceCallLogsDelegate <NSObject>
-(void) xmppService:(XMPPService *) ŒRTCservice didAddCallLog:(CallLog *) callLog;
-(void) xmppService:(XMPPService *) service didRemoveCallLogsWithPeer:(Peer *) peer;
-(void) didRemoveAllCallLogs:(XMPPService *) service;
-(void) xmppService:(XMPPService *) service didUpdateCallLogReadState:(NSString *) callLogID;
@end

@protocol XMPPServiceChannelsDelegate <NSObject>
-(void) xmppService:(XMPPService *) service didReceiveItems:(NSArray<NSDictionary *> *) conferenceInfo;
-(void) xmppService:(XMPPService *) service didDeleteChannel:(NSString *) channelID;
@end

@protocol XMPPServiceContactsManagerDelegate <NSObject>
-(void) didEndPopulatingRoster;
@end

@protocol XMPPServiceRTCDelegate <NSObject>
-(void) xmppService:(XMPPService *) service didReceiveRemoteRecordingStatusChanged:(NSString *) status forJID:(NSString *) jid;
@end

@protocol XMPPServiceTelephonyDelegate <NSObject>

-(void) xmppService:(XMPPService *) service didReceiveNomadicStatusUpdate:(NomadicStatus *) status;
-(void) xmppService:(XMPPService *) service didReceiveCallForwardStatusUpdate:(CallForwardStatus *) status;
-(void) xmppService:(XMPPService *) service didReceiveCallEvent:(CallEvent *) callEvent;
-(void) xmppService:(XMPPService *) service didReceiveMpCall:(NSString*) sessionID withPeer:(Peer *) mpPeer andResource:(NSString*) resource;
-(void) xmppService:(XMPPService *) service didReceiveVoicemailCountUpdate:(NSInteger) count;

@end
