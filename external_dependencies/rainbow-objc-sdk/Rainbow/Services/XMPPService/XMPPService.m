/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPService.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import "DDASLLogger.h"
#import "XMPPFramework.h"
#import "Tools.h"
#import "Conversation+internal.h"
#import "XMPPMessage+XEP_0184EX.h"
#import "XMPPMessage+XEP_0066Rainbow.h"

#import "XMPPMessageArchiveManagement.h"
#import "XMPPMessageArchiveManagementCoreDataStorage.h"

#import "XMPPStreamWebSockets.h"
#import "defines.h"
#import "ContactsManagerService+Internal.h"
#import "RoomsService+Internal.h"
#import "RTCService+Internal.h"
#import "NSDate+Utilities.h"

#import "Room+Internal.h"
#import "XMPPMessage+RainbowCustomMessageTypes.h"
#import "XMPPRainbowCustomStanzaSupport.h"
#import "XMPPPresence+RainbowCustomType.h"
#import "XMPPPush.h"
#import "XMPPMediaPillar.h"
#import "XMPPIQ+XEP_ClickToCallMobile.h"
#import "XMPPJingleMessageInitiation.h"
#import "XMPPIQ+XEP_0167.h"

#import "MessageInternal.h"
#import "LoginManager+Internal.h"

#import "XMPPRoster+StreamManagement.h"
#import "XMPPMessage+XEP0045Rainbow.h"

#import "NSXMLElement+XEP_0203_Element.h"

#import "File+Internal.h"
#import "XMPPStreamManagement+Rainbow.h"
#import "PINCache.h"
#import "XMPPMessage+Retransmission.h"
#import "CallLog+Internal.h"
#import "XMPPMessage+XEP_313_CallLogs.h"
#import "CallLog+Internal.h"
#import "XMPPStreamManagementDiskStorage.h"

#import "ConferenceInfo+Internal.h"
#import "ConferenceParticipant.h"

#import "NSDate+XMPPDateTimeProfiles.h"
#import "ConferenceParticipant+Internal.h"
#import "NSDate+Seconds.h"

#import "NSXMLElement+XEP_0352.h"
#import "RainbowXMPPRosterHybridStorage.h"
#import "RainbowUserDefaults.h"

#import "ConferencePublisher+Internal.h"

#import "XMPPPubSub.h"
#import "XMPPRoom+ServerShutdown.h"
#import "XMPPIDTracker.h"

#import "PhoneNumberInternal.h"
#import "NomadicStatus+Internal.h"
#import "CallForwardStatus+Internal.h"
#import "CallEvent+Internal.h"
#import "CallParticipant+Internal.h"


NSString *const kRemoveCallLogsKeyPrefex = @"remove-";
NSString *const kRemoveAllCallLogsKeyPrefex = @"removeall-";

@interface XMPPService () <XMPPRosterDelegate, XMPPMessageArchiveManagementDelegate, XMPPRoomDelegate, XMPPJingleMessageInitiationDelegate> {
    dispatch_queue_t xmppDelegateQueue;
    void *xmppDelegateQueueTag;
}

@property (nonatomic, strong) XMPPStreamWebSockets *xmppStream;
@property (nonatomic, strong) DownloadManager *certificateManager;
@property (nonatomic, strong) ContactsManagerService *contactsManager;
@property (nonatomic, strong) XMPPReconnect *xmppReconnect;
@property (nonatomic, strong) RainbowXMPPRosterHybridStorage *rainbowHybridStorage;
@property (nonatomic, strong) XMPPRosterMemoryStorage *xmppRosterStorage;
@property (nonatomic, strong) XMPPRoster *xmppRoster;
@property (nonatomic, strong) XMPPCapabilities *xmppCapabilities;
@property (nonatomic, strong) XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
@property (nonatomic, strong) XMPPMessageCarbons *xmppMessageCarbon;
@property (nonatomic, strong) XMPPPing *xmppPing;
@property (nonatomic, strong) XMPPMessageArchiveManagement *xmppMessageArchiveManagement;
@property (nonatomic, strong) XMPPMessageArchiveManagementCoreDataStorage *xmppMessageArchiveManagementStorage;
@property (nonatomic, strong) XMPPStreamManagementDiskStorage *xmppStreamManagementStorage;
@property (nonatomic, strong) XMPPStreamManagement *xmppStreamManagement;
@property (nonatomic, strong) XMPPSoftwareVersion *xmppSoftwareVersion;
@property (nonatomic, strong) XMPPLastActivity *xmppLastActivity;
@property (nonatomic, strong) XMPPMessageDeliveryReceipts *xmppMessageDeliveryReceipts;
@property (nonatomic, strong) XMPPJingle *xmppJingle;
@property (nonatomic, weak)   id <XMPPJingleDelegate> appDelegate;
@property (nonatomic, strong) XMPPJingleMessageInitiation *xmppJingleMessageInitiation;
@property (nonatomic, strong) XMPPMUC *xmppMUC;
@property (nonatomic, strong) XMPPRainbowCustomStanzaSupport *xmppRainbowCustomStanzaSupport;
@property (nonatomic, strong) XMPPPush *xmppPush;
@property (nonatomic, strong) XMPPMediaPillar *xmppMediaPillar;
@property (nonatomic, strong) NSMutableArray<XMPPRoom *> *xmppRooms;
@property (nonatomic, strong) NSObject *xmppRoomsMutex;
@property (nonatomic) BOOL customCertEvaluation;
@property (nonatomic) NSInteger nbOf503Error;
@property (nonatomic) BOOL wasCleanDisconnect;
@property (nonatomic, strong) NSString *jid;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSMutableDictionary *loadArchiveRequestCompletionHandler;
@property (nonatomic, strong) NSMutableDictionary *deleteRequestCompletionHandler;
@property (nonatomic, strong) NSMutableDictionary *sendMessageRequestsCompletionHandler;
@property (nonatomic, strong) NSMutableDictionary *loadCallLogsRequestCompletionHandler;
@property (nonatomic, strong) NSObject *completionHandlerDictionaryMutex;
@property (nonatomic, strong) PINCache *outgoingStanzaCache;

@property (nonatomic, strong) NSOperationQueue *internalOperationQueue;

@property (nonatomic, strong) NSTimer *timerToRefreshUI;

// This one must be keept
@property (nonatomic, strong) PINCache *savedActionscache;

@property (nonatomic, strong) NSString *rosterVersion;

// We keep the isConnected state manually instead of using the xmppStream state because requesting it may block the calling theard if there is too many action on the xmppQueue
@property (nonatomic) BOOL isConnected;

@property (nonatomic, strong) XMPPIDTracker *mucPresenceTracker;

@property (nonatomic) BOOL isAbleToRefreshUI;

// Set to YES when we did authenticate but not in resume (classic login)
@property (nonatomic) BOOL needToSendFirstPresence;
@end

@implementation XMPPService

-(instancetype) initWithCertificateManager:(DownloadManager *) certificateTrustExtension contactManagerService:(ContactsManagerService *) contactsManager {
    self = [super init];
    if(self){
        [DDLog addLogger:[DDTTYLogger sharedInstance]];
        [DDLog addLogger:[DDASLLogger sharedInstance]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeServer:) name:kLoginManagerDidChangeServer object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRTCCall:) name:kTelephonyServiceDidRemoveCallNotification object:nil];
        
        xmppDelegateQueueTag = &xmppDelegateQueueTag;
        xmppDelegateQueue = dispatch_queue_create("xmppDelegate", DISPATCH_QUEUE_SERIAL);
        dispatch_queue_set_specific(xmppDelegateQueue, xmppDelegateQueueTag, xmppDelegateQueueTag, NULL);
        
        _certificateManager = certificateTrustExtension;
        _contactsManager = contactsManager;
        _xmppStream = [XMPPStreamWebSockets new];
        _xmppStream.certificateManager = _certificateManager;
        
        _xmppReconnect = [XMPPReconnect new];
        _xmppReconnect.reconnectTimerInterval = 10;
        _rainbowHybridStorage = [RainbowXMPPRosterHybridStorage new];
        _xmppRosterStorage = [XMPPRosterMemoryStorage new];
        _rainbowHybridStorage.memoryStorage = _xmppRosterStorage;
        _rainbowHybridStorage.stream = _xmppStream;
        _xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:_xmppRosterStorage];
        // We don't autofetch the roster because with stream management we must NOT retreive it, so we will handle that manually
        _xmppRoster.autoFetchRoster = NO;
        _xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = YES;
        _xmppRoster.allowRosterlessOperation = YES;
        _xmppCapabilitiesStorage = [XMPPCapabilitiesCoreDataStorage sharedInstance];
        _xmppCapabilities = [[XMPPCapabilities alloc] initWithCapabilitiesStorage:_xmppCapabilitiesStorage];
        _xmppCapabilities.autoFetchHashedCapabilities = YES;
        _xmppCapabilities.autoFetchNonHashedCapabilities = NO;
        _xmppMessageCarbon = [[XMPPMessageCarbons alloc] init];
        _xmppMessageCarbon.autoEnableMessageCarbons = YES;
        _xmppMessageCarbon.allowsUntrustedMessageCarbons = YES;
        _xmppPing = [XMPPPing new];
        _xmppPing.respondsToQueries = YES;
        _xmppMessageArchiveManagementStorage = [XMPPMessageArchiveManagementCoreDataStorage sharedInstance];
        _xmppMessageArchiveManagement = [[XMPPMessageArchiveManagement alloc] initWithMessageArchiveManagementStorage:_xmppMessageArchiveManagementStorage];
        _xmppStreamManagementStorage = [XMPPStreamManagementDiskStorage new];
        _xmppStreamManagement = [[XMPPStreamManagement alloc] initWithStorage:_xmppStreamManagementStorage];
        _xmppStreamManagement.autoResume = YES;
        _xmppSoftwareVersion = [[XMPPSoftwareVersion alloc] initWithName:[Tools applicationName] version:[Tools applicationVersion] os:[Tools currentOS] dispatchQueue:xmppDelegateQueue];
        _xmppLastActivity = [XMPPLastActivity new];
        _xmppLastActivity.respondsToQueries = YES;
        _xmppMessageDeliveryReceipts = [XMPPMessageDeliveryReceipts new];
		_xmppJingle = [XMPPJingle new];
        _xmppJingleMessageInitiation = [XMPPJingleMessageInitiation new];
        _xmppJingleMessageInitiation.myUser = _contactsManager.myUser;
        _xmppMUC = [XMPPMUC new];
        _xmppRooms = [NSMutableArray array];
        _xmppRoomsMutex = [NSObject new];
        _xmppRainbowCustomStanzaSupport = [XMPPRainbowCustomStanzaSupport new];
        _xmppRainbowCustomStanzaSupport.contactsManagerService = _contactsManager;
        _xmppRainbowCustomStanzaSupport.xmppService = self;
        _xmppPush = [XMPPPush new];
        _xmppMessageArchiveManagementStorage.xmppService = self;
        _xmppMediaPillar = [XMPPMediaPillar new];
        [self setIsAbleToRefreshUIAndNotify:YES];
        _needToSendFirstPresence = NO;
        
        [_xmppReconnect activate:_xmppStream];
        [_xmppRoster activate:_xmppStream];
        [_xmppMessageCarbon activate:_xmppStream];
        [_xmppMessageArchiveManagement activate:_xmppStream];
        [_xmppPing activate:_xmppStream];
        [_xmppStreamManagement activate:_xmppStream];
        [_xmppSoftwareVersion activate:_xmppStream];
        [_xmppLastActivity activate:_xmppStream];
        [_xmppMessageDeliveryReceipts activate:_xmppStream];
        [_xmppJingle activate:_xmppStream];
        [_xmppJingleMessageInitiation activate:_xmppStream];

        [_xmppMUC activate:_xmppStream];
        _xmppRainbowCustomStanzaSupport.xmppMessageArchiveManagementStorage = _xmppMessageArchiveManagementStorage;
        [_xmppRainbowCustomStanzaSupport activate:_xmppStream];
        [_xmppPush activate:_xmppStream];
        [_xmppMediaPillar activate:_xmppStream];
        
        // Automatically add a "request-for-ack" on all our outgoing messages.
        // But NO automatic ack answer on incoming messages, we handle them ourselves.
        _xmppMessageDeliveryReceipts.autoSendMessageDeliveryRequests = YES;
        _xmppMessageDeliveryReceipts.autoSendMessageDeliveryReceipts = NO;
        
        [_xmppStream addDelegate:self delegateQueue:xmppDelegateQueue];
        [_xmppRoster addDelegate:self delegateQueue:xmppDelegateQueue];
        [_xmppRoster addDelegate:_rainbowHybridStorage delegateQueue:xmppDelegateQueue];
        [_rainbowHybridStorage addDelegate:self delegateQueue:xmppDelegateQueue];
        [_xmppPing addDelegate:self delegateQueue:xmppDelegateQueue];
        [_xmppMessageArchiveManagement addDelegate:self delegateQueue:xmppDelegateQueue];
        [_xmppStreamManagement addDelegate:self delegateQueue:xmppDelegateQueue];
        [_xmppSoftwareVersion addDelegate:self delegateQueue:xmppDelegateQueue];
        [_xmppLastActivity addDelegate:self delegateQueue:xmppDelegateQueue];
        [_xmppReconnect addDelegate:self delegateQueue:xmppDelegateQueue];
		[_xmppJingle addDelegate:self delegateQueue:xmppDelegateQueue];
        [_xmppJingleMessageInitiation addDelegate:self delegateQueue:xmppDelegateQueue];
        [_xmppMUC addDelegate:self delegateQueue:xmppDelegateQueue];
        
        _customCertEvaluation = YES;
        
        _loadArchiveRequestCompletionHandler = [NSMutableDictionary new];
        _loadCallLogsRequestCompletionHandler = [NSMutableDictionary new];
        _deleteRequestCompletionHandler = [NSMutableDictionary new];
        _sendMessageRequestsCompletionHandler = [NSMutableDictionary new];
        _completionHandlerDictionaryMutex = [NSObject new];
        
        _mucPresenceTracker = [[XMPPIDTracker alloc] initWithStream:_xmppStream dispatchQueue:xmppDelegateQueue];
        
        _nbOf503Error = 0;
        
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
       
#endif
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willLogin:) name:kLoginManagerInternalWillLogin object:nil];
        
        _outgoingStanzaCache = [[PINCache alloc] initWithName:@"outgoingStanzaCache"];
        _outgoingStanzaCache.memoryCache.removeAllObjectsOnEnteringBackground = NO;

        _savedActionscache = [[PINCache alloc] initWithName:@"savedActionscache"];
        _savedActionscache.memoryCache.removeAllObjectsOnEnteringBackground = NO;
        
        _rosterVersion = [[RainbowUserDefaults sharedInstance] objectForKey:@"rosterVersion"];
        _rainbowHybridStorage.rosterVersion = _rosterVersion;
        
        _internalOperationQueue = [NSOperationQueue new];
        _internalOperationQueue.maxConcurrentOperationCount = 20;
        _internalOperationQueue.qualityOfService = NSOperationQueuePriorityNormal;
        NSLog(@"INIT XMPP SERVICE");
        if(!_xmppStream.myJID && _contactsManager.myContact.jid){
            NSString *resourceName = [Tools resourceName];
            [_xmppStream setMyJID:[XMPPJID jidWithString:_contactsManager.myContact.jid resource:resourceName]];
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [_rainbowHybridStorage loadUsersFromCacheForRoster:_xmppRoster];
        });
        _xmppStream.myUser = _contactsManager.myUser;
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidRemoveCallNotification object:nil];
    
    [_xmppStream removeDelegate:self];
    [_xmppRoster removeDelegate:self];
    [_xmppRoster removeDelegate:_rainbowHybridStorage];
    [_xmppMessageCarbon removeDelegate:self];
    [_xmppPing removeDelegate:self];
    [_xmppMessageArchiveManagement removeDelegate:self];
    [_xmppStreamManagement removeDelegate:self];
    [_xmppSoftwareVersion removeDelegate:self];
    [_xmppLastActivity removeDelegate:self];
    [_xmppJingle removeDelegate:self];
    [_xmppJingleMessageInitiation removeDelegate:self];
    [_xmppMUC removeDelegate:self];
    [_xmppReconnect removeDelegate:self];
    
    [_xmppSoftwareVersion deactivate];
    [_xmppStreamManagement deactivate];
    [_xmppPing deactivate];
    [_xmppMessageArchiveManagement deactivate];
    [_xmppMessageCarbon deactivate];
    [_xmppCapabilities deactivate];
    [_xmppRoster deactivate];
    [_xmppReconnect deactivate];
    [_xmppLastActivity deactivate];
    [_xmppMessageDeliveryReceipts deactivate];
    [_xmppJingle deactivate];
    [_xmppJingleMessageInitiation deactivate];
    [_xmppMUC deactivate];
    [_xmppRainbowCustomStanzaSupport deactivate];
    [_xmppPush deactivate];
    _xmppMessageArchiveManagementStorage.xmppService = nil;
    
    dispatch_async(xmppDelegateQueue, ^{
        [_mucPresenceTracker removeAllIDs];
        _mucPresenceTracker = nil;
    });
    
    [_xmppMediaPillar deactivate];
    
    _outgoingStanzaCache = nil;
    
    // Clean the rooms.
    @synchronized (_xmppRoomsMutex) {
        for (XMPPRoom *room in _xmppRooms) {
            [room removeDelegate:self];
            [room deactivate];
        }
        [_xmppRooms removeAllObjects];
    }
    _xmppRoomsMutex = nil;
    
    [_xmppStream disconnect];
    _rainbowHybridStorage.memoryStorage = nil;
    _rainbowHybridStorage = nil;
    _xmppPush = nil;
    _xmppMediaPillar = nil;
    _xmppRainbowCustomStanzaSupport.roomsService = nil;
    _xmppRainbowCustomStanzaSupport.contactsManagerService = nil;
    _xmppRainbowCustomStanzaSupport.fileService = nil;
    _xmppRainbowCustomStanzaSupport.xmppService = nil;
    _xmppRainbowCustomStanzaSupport.roomsDelegate = nil;
    _xmppRainbowCustomStanzaSupport.conversationDelegate = nil;
    _xmppRainbowCustomStanzaSupport.xmppMessageArchiveManagementStorage = nil;
    _xmppRainbowCustomStanzaSupport = nil;
    _xmppRooms = nil;
    _xmppMUC = nil;
    _xmppJingle = nil;
    _xmppJingleMessageInitiation = nil;
    _xmppMessageDeliveryReceipts = nil;
    _xmppLastActivity = nil;
    _xmppSoftwareVersion = nil;
    _xmppStreamManagementStorage = nil;
    _xmppStreamManagement = nil;
    _xmppMessageCarbon = nil;
    _xmppCapabilitiesStorage = nil;
    _xmppCapabilities = nil;
    _rainbowHybridStorage = nil;
    _xmppRoster = nil;
    _xmppReconnect = nil;
    _xmppStream = nil;
    
    [_loadArchiveRequestCompletionHandler removeAllObjects];
    _loadArchiveRequestCompletionHandler = nil;
    [_deleteRequestCompletionHandler removeAllObjects];
    _deleteRequestCompletionHandler = nil;
    [_sendMessageRequestsCompletionHandler removeAllObjects];
    _sendMessageRequestsCompletionHandler = nil;
    
    [_loadCallLogsRequestCompletionHandler removeAllObjects];
    _loadCallLogsRequestCompletionHandler = nil;
    
    _contactsManager = nil;
    _certificateManager = nil;
    
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidFinishLaunchingNotification object:nil];
#endif
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalWillLogin object:nil];
}

-(void) reset {
    [self didChangeUser:nil];
}

-(void) setResourceName:(NSString *) name {
    [_xmppStream setResourceName:name];
}

#pragma mark - iOS Application notifications

-(void) applicationDidFinishLaunching:(NSNotification *) notification {

}

-(void) applicationWillResignActive:(NSNotification *) notification {
    NSLog(@"[XMPPService] will resing active");
#ifdef CSI_ACTIVATED
    if(_xmppStream.isConnected){
        [_xmppStream sendElement:[NSXMLElement indicateInactiveElement]];
    }
#endif
    _xmppReconnect.autoReconnect = _rtcService.hasActiveCalls;
    _autoReconnectEnabled = _xmppReconnect.autoReconnect;
}

-(void) applicationDidEnterBackground:(NSNotification *) notification {
    NSLog(@"[XMPPService] Did enter background");
}

-(void) applicationDidBecomeActive:(NSNotification *) notification {
    NSLog(@"[XMPPService] Did become active");
    _xmppReconnect.autoReconnect = YES;
    _autoReconnectEnabled = YES;
    
#ifdef CSI_ACTIVATED
    if(_xmppStream.isConnected)
        [_xmppStream sendElement:[NSXMLElement indicateActiveElement]];
}
#endif

-(void) didRemoveRTCCall:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveRTCCall:notification];
        });
        return;
    }
        
    if(!_rtcService.hasActiveCalls){
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground){
            _xmppReconnect.autoReconnect = NO;
            _autoReconnectEnabled = NO;
        }
#endif
    }
}

#pragma mark - Login manager notifications
-(void) willLogin:(NSNotification *) notification {

}

-(void) loadPresenceFromCache {
    NSLog(@"Start loading presence from cache");
    
    NSMutableArray *presencesJidAlreadyLoadedFromCache = [NSMutableArray new];
    
    // Priority to contacts in the conversations list
    NSArray<Conversation *> *conversations = [ServicesManager sharedInstance].conversationsManagerService.conversations;
    if (conversations.count > 0) {
        OTCLog(@"Load presences for contacts in conversations in priority");

        // Don't allow UI to refresh (to avoid multiple refreshs)
        [[NSNotificationCenter defaultCenter] postNotificationName:kIsAbleToRefreshUIChanged object:nil userInfo:@{@"isAbleToRefreshUI": @(NO)}];

        [conversations enumerateObjectsUsingBlock:^(Conversation * conversation, NSUInteger idx, BOOL * stop) {
            if (conversation.type != ConversationTypeRoom) {
                XMPPUserMemoryStorageObject *user = [_xmppRosterStorage userForJID:[XMPPJID jidWithString:conversation.peer.jid] options:XMPPJIDCompareUser];
                if (user) {
                    for(XMPPResourceMemoryStorageObject *resource in user.allResources){
                        NSLog(@"Loading presence from resource %@", resource);
                        [self xmppStream:_xmppStream didReceivePresence:resource.presence withLogs:NO];
                    }
                    [presencesJidAlreadyLoadedFromCache addObject:user.jid.bare];
                }
            }
        }];

        OTCLog(@"End of loading presences for contacts in conversations - refresh UI");

        // Allow UI to refresh as we end to load presences for contacts in conversations list
        [[NSNotificationCenter defaultCenter] postNotificationName:kIsAbleToRefreshUIChanged object:nil userInfo:@{@"isAbleToRefreshUI": @(YES)}];

        // Ask UI to refresh
        [[NSNotificationCenter defaultCenter] postNotificationName:kEndReceiveXMPPRequestsAfterResume object:nil];
    }
    
    OTCLog(@"Now we load all others presences");
    // Load presences for others (just skip if we've already done)
    for (XMPPUserMemoryStorageObject *user in [_xmppRosterStorage unsortedUsers]) {
        if (![presencesJidAlreadyLoadedFromCache containsObject:user.jid.bare]) {
            for(XMPPResourceMemoryStorageObject *resource in user.allResources){
                NSLog(@"Loading presence from resource %@", resource);
                [self xmppStream:_xmppStream didReceivePresence:resource.presence withLogs:NO];
            }
            [presencesJidAlreadyLoadedFromCache addObject:user.jid.bare];
        }
    }
    
    // Load my presence
    XMPPUserMemoryStorageObject *myUser = _xmppRosterStorage.myUser;
    for (XMPPResourceMemoryStorageObject *resource in myUser.allResources) {
        [self xmppStream:_xmppStream didReceivePresence:resource.presence];
    }
    
    NSLog(@"End of loading presence from cache");
}

#pragma mark - user changed
-(void) didChangeUser:(NSNotification *) notification {
    [_xmppStream setMyJID:nil];
    [_xmppRoster clearRosterContent];
    [_rainbowHybridStorage clearStorage];
    [_xmppStreamManagementStorage cleanCache];
    [_savedActionscache removeAllObjects];
    [_outgoingStanzaCache removeAllObjects];
    [_xmppMessageArchiveManagementStorage cleanAllMessages];
    _rosterVersion = nil;
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:@"rosterVersion"];
}

-(void) didChangeServer:(NSNotification *) notification {
    [_xmppStream setMyJID:nil];
    [_xmppRoster clearRosterContent];
    [_rainbowHybridStorage clearStorage];
    [_xmppStreamManagementStorage cleanCache];
    [_savedActionscache removeAllObjects];
    [_outgoingStanzaCache removeAllObjects];
    [_xmppMessageArchiveManagementStorage cleanAllMessages];
    _rosterVersion = nil;
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:@"rosterVersion"];
}

#pragma mark - Custom setters for delegates
-(void) setLoginDelegate:(id<XMPPServiceLoginDelegate>)loginDelegate {
    _loginDelegate = loginDelegate;
    _xmppRainbowCustomStanzaSupport.loginDelegate = loginDelegate;
}

-(void) setConversationDelegate:(id<XMPPServiceConversationDelegate>)conversationDelegate {
    _xmppRainbowCustomStanzaSupport.conversationDelegate = conversationDelegate;
    _conversationDelegate = conversationDelegate;
}

-(void) setRoomsDelegate:(id<XMPPServiceRoomsDelegate>)roomsDelegate {
    _xmppRainbowCustomStanzaSupport.roomsDelegate = roomsDelegate;
    _roomsDelegate = roomsDelegate;
}

-(void) setRoomsService:(RoomsService *)roomsService {
    _xmppRainbowCustomStanzaSupport.roomsService = roomsService;
    _roomsService = roomsService;
}

-(void) setGroupsService:(GroupsService *)groupsService {
    _xmppRainbowCustomStanzaSupport.groupsService = groupsService;
    _groupsService = groupsService;
}

-(void) setCompaniesService:(CompaniesService *)companiesService {
    _xmppRainbowCustomStanzaSupport.companiesService = companiesService;
    _companiesService = companiesService;
}

-(void) setConferenceDelegate:(id<XMPPServiceConferenceDelegate>)conferenceDelegate {
    _xmppRainbowCustomStanzaSupport.conferenceDelegate = conferenceDelegate;
    _conferenceDelegate = conferenceDelegate;
}

-(void) setApplicationID:(NSString *)applicationID {
    _applicationID = applicationID;
    _xmppPush.applicationID = applicationID;
}

-(void) setPushToken:(NSData *)pushToken {
    _pushToken = pushToken;
    _xmppPush.pushToken = _pushToken;
}

-(void) setFileSharingService:(FileSharingService *)fileSharingService {
    _fileSharingService = fileSharingService;
    _xmppRainbowCustomStanzaSupport.fileService = _fileSharingService;
}

#pragma mark - Connection management
-(BOOL) isXmppConnected {
    return _isConnected;
}

-(BOOL) isXmppConnecting {
    return _xmppStream.isConnecting;
}

-(BOOL) connectToServer:(Server *) server withJID:(NSString *) jid andPassword:(NSString *) password withError:(NSError **) error {
    if (self.isXmppConnected || self.isXmppConnecting) {
        return YES;
    }
    _jid = jid;
    _password = password;
    _xmppStream.server = server;
    
    NSString *resourceName = [Tools resourceName];
    [_xmppStream setMyJID:[XMPPJID jidWithString:_jid resource:resourceName]];
    
    // Auto reconnect must be activated only if push is disabled
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    dispatch_async(dispatch_get_main_queue(), ^{
        _xmppReconnect.autoReconnect = ([UIApplication sharedApplication].applicationState == UIApplicationStateActive);
        _autoReconnectEnabled = _xmppReconnect.autoReconnect;
    });
#endif
    NSLog(@"Connecting to server %@", server);
    _internalOperationQueue.suspended = NO;
    if (![_xmppStream connectWithTimeout:10 error:error]) {
        return NO;
    }
    return YES;
}

-(void) disconnect {
    NSLog(@"XMPP Disconnection");
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
    [_xmppStream sendElement:presence];
    _xmppReconnect.autoReconnect = NO;
    _autoReconnectEnabled = NO;
    [_xmppStream disconnect];
    [_xmppStreamManagement disconnect];
    @synchronized(_completionHandlerDictionaryMutex){
        [_loadArchiveRequestCompletionHandler removeAllObjects];
        [_deleteRequestCompletionHandler removeAllObjects];
        [_sendMessageRequestsCompletionHandler removeAllObjects];
        [_loadCallLogsRequestCompletionHandler removeAllObjects];
    }
    _rosterVersion = nil;
    _xmppStream.myJID = nil;
    dispatch_async(xmppDelegateQueue, ^{
        [_mucPresenceTracker removeAllIDs];
    });
    _internalOperationQueue.suspended = YES;
    [_internalOperationQueue cancelAllOperations];
}

-(void) forceClosingConnection {
    NSLog(@"Force close socket");
    [_xmppStream forceCloseSocket];
}

-(void) forceNextConnectionToNotResumeTheStream {
    [_xmppStreamManagement removeAllForStream:_xmppStream];
    [_xmppStreamManagementStorage cleanCache];
}

-(void) setMyPresenceTo:(Presence *)presence firstPresence:(BOOL) firstPresence {
    NSString *type = nil;
    switch (presence.presence) {
        case ContactPresenceUnavailable: {
            type = @"unavailable";
            break;
        }
        case ContactPresenceAvailable: {
            break;
        }
        case ContactPresenceDoNotDisturb: {
            type = @"dnd";
            break;
        }
        case ContactPresenceAway:
        case ContactPresenceInvisible: {
            type = @"xa";
            break;
        }
        case ContactPresenceBusy:
            break;
    }
    
    //<presence xmlns='jabber:client'><show>away</show></presence>
    XMPPPresence *aPresence = [XMPPPresence presence];
    [aPresence addAttributeWithName:@"from" stringValue:_xmppStream.myJID.full];
    NSXMLElement *show = [NSXMLElement elementWithName:@"show" stringValue:type];
    NSXMLElement *priority = [NSXMLElement elementWithName:@"priority" stringValue:@"5"];
    [aPresence addChild:priority];
    [aPresence addChild:show];
    
    if(presence.status.length > 0){
        NSXMLElement *status = [NSXMLElement elementWithName:@"status" stringValue:presence.status];
        [aPresence addChild:status];
    }
    
    if(presence.presence == ContactPresenceAvailable){
        NSXMLElement *status = [NSXMLElement elementWithName:@"status" stringValue:@"mode=auto"];
        [aPresence addChild:status];
    }
    if(presence.presence == ContactPresenceAway){
        NSXMLElement *status = [NSXMLElement elementWithName:@"status" stringValue:@"away"];
        [aPresence addChild:status];
    }
    
    if(firstPresence){
        NSXMLElement *parameters = [NSXMLElement elementWithName:@"parameters" xmlns:kXMPPJabberConfigurationNS];
        NSXMLElement *pres = [NSXMLElement elementWithName:@"presence_options"];
        [pres addAttributeWithName:@"bulk" stringValue:@"true"];
        [parameters addChild:pres];
        [aPresence addChild:parameters];
        
        
//        <presence xmlns='jabber:client'><application xmlns='jabber:iq:application'><appid>12345abcd</appid><userid>789cbvbfhg</userid><application>
//        <parameters xmlns='jabber:iq:configuration'><presence_options bulk='true'/></parameters>
        
        
//        </presence>
        
        if(_applicationID.length > 0 && _contactsManager.myContact.rainbowID.length > 0){
            NSXMLElement *application = [NSXMLElement elementWithName:@"application" xmlns:@"jabber:iq:application"];
            NSXMLElement *appID = [NSXMLElement elementWithName:@"appid" stringValue:_applicationID];
            NSXMLElement *userID = [NSXMLElement elementWithName:@"userid" stringValue:_contactsManager.myContact.rainbowID];
            [application addChild:appID];
            [application addChild:userID];
            [aPresence addChild:application];
        }
        _needToSendFirstPresence = NO;
    }
    NSLog(@"SEND PRESENCE %@", aPresence);
    [_xmppStream sendElement:aPresence];
}

-(void) setMyPresenceTo:(Presence *) presence {
    [self setMyPresenceTo:presence firstPresence:_needToSendFirstPresence];
}

#pragma mark - Connection delegate
- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStream:(XMPPStream *)sender willSecureWithSettings:(NSMutableDictionary *)settings {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    NSString *expectedCertName = [_xmppStream.myJID domain];
    if (expectedCertName) {
        settings[(NSString *) kCFStreamSSLPeerName] = expectedCertName;
    }
    
    if (_customCertEvaluation){
        settings[GCDAsyncSocketManuallyEvaluateTrust] = @(YES);
    }
}

- (void)xmppStream:(XMPPStream *)sender didReceiveTrust:(SecTrustRef)trust completionHandler:(void (^)(BOOL shouldTrustPeer))completionHandler {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    SecTrustResultType result = kSecTrustResultDeny;
    OSStatus status = SecTrustEvaluate(trust, &result);
    
    if (status == noErr && (result == kSecTrustResultProceed || result == kSecTrustResultUnspecified)) {
        completionHandler(YES);
    } else {
        completionHandler(NO);
    }
}

- (void) xmppStreamDidSecure:(XMPPStream *)sender {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void) xmppStreamDidConnect:(XMPPStream *)sender {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);

    _nbOf503Error = 0;
    _isConnected = NO;
    NSError *error = nil;
    if (![_xmppStream authenticateWithPassword:_password error:&error]) {
        NSLog(@"Error authenticating: %@", error);
        [_xmppStream disconnect];
        [_loginDelegate xmppService:self failedToAuthenticateWithError:error];
    }
}

- (void) xmppStreamDidAuthenticate:(XMPPStream *)sender {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    _isConnected = YES;

    [_xmppStreamManagement automaticallyRequestAcksAfterStanzaCount:0 orTimeout:10.0];
    [_xmppStreamManagement automaticallySendAcksAfterStanzaCount:1 orTimeout:0];
    if(![_xmppStreamManagement didResume]){
        OTCLog(@"Did authenticate but NOT in resume");
        _needToSendFirstPresence = YES;
        [self setIsAbleToRefreshUIAndNotify:YES];
        [_xmppRoster clearAllResources];
        [_xmppStreamManagement enableStreamManagementWithResumption:YES maxTimeout:0];
        [_xmppRoster fetchRosterVersion:_rosterVersion];
        [_loginDelegate xmppServiceDidConnect:self];
        [_outgoingStanzaCache removeAllObjects];
        // We don't need to send the message already cached in case of a resumed stream because the stream management will resend for us the missing stanza
        [self loadSavedActionsCache];
    } else {
        OTCLog(@"Did authenticate in resume");
        [self setIsAbleToRefreshUIAndNotify:NO];
        if(![_xmppRoster hasRoster] || !_rosterVersion)
            [_xmppRoster fetchRosterVersion:_rosterVersion];
        [self saveMessageWithBodyOnlyInOutgoingStanzaCache];
        if(!_contactsManager.myContact.presence)
            [self setMyPresenceTo:[Presence presenceAvailable]];
        NSLog(@"We resume a stream, notify login manager");
        [_loginDelegate xmppServiceDidResumeStream:self];
    }
    
#ifdef CSI_ACTIVATED
    dispatch_async(dispatch_get_main_queue(), ^{
        if(([UIApplication sharedApplication].applicationState == UIApplicationStateActive))
            [_xmppStream sendElement:[NSXMLElement indicateActiveElement]];
        else
            [_xmppStream sendElement:[NSXMLElement indicateInactiveElement]];
    });
#endif
}

- (void) saveMessageWithBodyOnlyInOutgoingStanzaCache {
   NSLog(@"saveMessageWithBodyOnlyInOutgoingStanzaCache");
    NSMutableArray *outgoingStanzas = [NSMutableArray new];
    
    [_outgoingStanzaCache.diskCache enumerateObjectsWithBlockAsync:^(NSString * key, NSURL * fileURL, BOOL * stop) {
        [outgoingStanzas addObject:key];
    } completionBlock:^(id<PINCaching> cache) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            for (NSString *key in outgoingStanzas) {
                id object = [_outgoingStanzaCache objectForKey:key];
                NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:object error:nil];
                if([[element name] isEqualToString:@"message"]){
                    XMPPMessage *message = [XMPPMessage messageFromElement:element];
                    if(![message body]) {
                            [_outgoingStanzaCache removeObjectForKeyAsync:key completion:nil];
                    }
                }else {
                     [_outgoingStanzaCache removeObjectForKeyAsync:key completion:nil];
                }
            }
        });
    }];
}

-(void) loadPendingOutgoingStanzaCache {
    NSLog(@"Load pending outgoing stanza");
    NSMutableArray *outgoingStanzas = [NSMutableArray new];
    NSMutableDictionary <NSString *, NSXMLElement *> *messagesToResend = [NSMutableDictionary dictionary];
    [_outgoingStanzaCache.diskCache enumerateObjectsWithBlockAsync:^(NSString * key, NSURL * fileURL, BOOL * stop) {
        [outgoingStanzas addObject:key];
    } completionBlock:^(id<PINCaching> cache) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            for (NSString *key in outgoingStanzas) {
                id object = [_outgoingStanzaCache objectForKey:key];
                if (object) {
                    NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:object error:nil];
                    [messagesToResend setObject:element forKey:key];
                }
                [_outgoingStanzaCache removeObjectForKeyAsync:key completion:nil];
            }
            
            NSArray *sortedKeys = [messagesToResend keysSortedByValueUsingComparator:^NSComparisonResult(NSXMLElement *obj1, NSXMLElement * obj2) {
                NSString *timestamp1 = [[[obj1 elementForName:@"tmptimestamp"] attributeForName:@"value"] stringValue];
                NSString *timestamp2 = [[[obj2 elementForName:@"tmptimestamp"] attributeForName:@"value"] stringValue];
                NSDate *date1 = [NSDate dateWithXmppDateTimeString:timestamp1];
                NSDate *date2 = [NSDate dateWithXmppDateTimeString:timestamp2];
                return [date1 compare:date2];
            }];
            
            NSMutableArray *sortedValues = [NSMutableArray array];
            for (NSString *key in sortedKeys){
                [sortedValues addObject:[messagesToResend objectForKey:key]];
            }
            
            for (NSXMLElement *message in sortedValues) {
                // Remove timestamp balise !!!
                [message removeElementForName:@"timestamp"];
                NSLog(@"Resending message from outgoing stanza cache %@", message);
                [self resend:message];
            }
        });
    }];
}

-(void) resend:(NSXMLElement *) message {
    [message removeElementForName:@"timestamp"];
    [message removeElementForName:@"tmptimestamp"];
    //if ([message body])
        [_xmppStream sendElement:message];
}

-(void) loadSavedActionsCache {
    NSLog(@"Load saved Actions cache");
    NSMutableArray *savedActionsCachedKeys = [NSMutableArray array];
    NSMutableDictionary<NSString *,XMPPMessage *> *messagesToResend = [NSMutableDictionary dictionary];
    __weak __typeof__(self) weakSelf = self;

    [_savedActionscache.diskCache enumerateObjectsWithBlockAsync:^(NSString * key, NSURL * fileURL, BOOL * stop) {
        [savedActionsCachedKeys addObject:key];
    } completionBlock:^(id<PINCaching> cache) {
        for (NSString *key in savedActionsCachedKeys) {
            XMPPMessage* message = [_savedActionscache objectForKey:key];
            [messagesToResend setObject:message forKey:key];
            [_savedActionscache removeObjectForKeyAsync:key completion:nil];
        }
        
        NSArray *sortedKeys = [messagesToResend keysSortedByValueUsingComparator:^NSComparisonResult(XMPPMessage *obj1, XMPPMessage * obj2) {
            NSString *timestamp1 = [[[obj1 elementForName:@"timestamp"] attributeForName:@"value"] stringValue];
            NSString *timestamp2 = [[[obj2 elementForName:@"timestamp"] attributeForName:@"value"] stringValue];
            NSDate *date1 = [NSDate dateWithXmppDateTimeString:timestamp1];
            NSDate *date2 = [NSDate dateWithXmppDateTimeString:timestamp2];
            return [date1 compare:date2];
        }];

        NSMutableArray *sortedValues = [NSMutableArray array];
        for (NSString *key in sortedKeys){
            [sortedValues addObject:[messagesToResend objectForKey:key]];
        }
        
        for (XMPPMessage *message in sortedValues) {
            // Remove timestamp balise !!!
            [message removeElementForName:@"timestamp"];
            NSLog(@"Resending message from saved actions cache %@", message);
            [weakSelf.xmppStream sendElement:message];
        }
        NSLog(@"Load saved Actions cache end");
    }];
}

- (void) xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error {
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    _isConnected = NO;
    [_xmppStream disconnect];
    [_xmppStream setMyJID:nil];
    NSError *errorToReturn = [NSError errorWithDomain:@"XMPP" code:500 userInfo:@{NSLocalizedDescriptionKey: @"Unable to connect to server, authentication failed"}];
    [_loginDelegate xmppService:self failedToAuthenticateWithError:errorToReturn];
}

- (BOOL) xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
    // We will not create an entire XMPPModule just for the ClickToCallMobile IQ now.
    // If it gets more complete, then we should revise this.
    
    if (!_isAbleToRefreshUI)
        [self resetTimerBeforeAllowingToRefreshUI];
    
    if ([iq isClickToCallMobileIQ]) {
        NSString *numberToCall = [iq getC2CPhoneNumber];
        BOOL directCall = [iq isC2CDirectCall];
        [_contactsManager callPhoneNumber:numberToCall directCall:directCall];
        return YES;
    }
    
    // XEP-167
    if ([iq isHoldJingle]) {
        [_rtcService didReceiveHoldJingleMessage:[iq sessionIDFromJingle]];
        return YES;
    } else if ([iq isUnholdJingle]) {
        [_rtcService didReceiveUnholdJingleMessage:[iq sessionIDFromJingle]];
        return YES;
    }
    
    return NO;
}


- (void)xmppStream:(XMPPStream *)sender didReceiveError:(id)error {
    NSLog(@"%@: %@, error : %@", THIS_FILE, THIS_METHOD, error);
    
    /**
     * <error xmlns="jabber:client" code="503" type="cancel"><service-unavailable xmlns="urn:ietf:params:xml:ns:xmpp-stanzas"/><text xmlns="urn:ietf:params:xml:ns:xmpp-stanzas">Authentication required</text></error>
     
       <stream:error xmlns:stream="http://etherx.jabber.org/streams"><conflict xmlns="urn:ietf:params:xml:ns:xmpp-streams"/><text xmlns="urn:ietf:params:xml:ns:xmpp-streams" lang="en">User disconnected</text></stream:error>
     */
    
    NSXMLElement *element = (NSXMLElement *)error;
    NSString *elementName = [element name];
    if([elementName isEqualToString:@"stream:error"]){
        // reset stream resume for the next login
        [_xmppStreamManagement removeAllForStream:_xmppStream];
    }
    NSString *errorCode = [((NSXMLElement *)error) attributeForName:@"code"].stringValue;
    NSXMLElement *txtElement = [((NSXMLElement *)error) elementForName:@"text"];
    NSString *errorReason = txtElement.stringValue;
    if([errorCode isEqualToString:@"503"] && ([errorReason isEqualToString:@"Authentication required"] || [errorReason isEqualToString:@"No module is handling this query"])){
        if(_nbOf503Error < 5)
            [_xmppStream forceCloseSocket];
        
        _nbOf503Error++;
    }
    
    //
    if([errorReason isEqualToString:@"User disconnected"]) {
        NSError *anError = [NSError errorWithDomain:@"XMPP" code:-998 userInfo:@{NSLocalizedDescriptionKey: @"User disconnected by server"}];
        _xmppReconnect.autoReconnect = NO;
        _autoReconnectEnabled = NO;
        [_loginDelegate xmppServiceDidDisconnect:self withError:anError];
    }
}

- (void)xmppStreamDidSendClosingStreamStanza:(XMPPStream *)sender {
    NSLog(@"Did Send closing stream stanza");

    _wasCleanDisconnect = YES;
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    // here we have to check lost connection or disconnect when login
    NSLog(@"%@: %@", THIS_FILE, THIS_METHOD);
    _isConnected = NO;
    if (![_xmppStream isConnected]) {
        // Important ! trigger lostconn when we force close the socket !
        if (error.code == kForceCloseSocketErrorCode) {
            [_loginDelegate xmppServiceDidLostConnection:self];
        }
        
        if(!error){
            error = [NSError errorWithDomain:@"XMPP" code:500 userInfo:@{NSLocalizedDescriptionKey: @"Unable to connect to server"}];
        }
        [_loginDelegate xmppServiceDidDisconnect:self withError:error];
        [_loginDelegate xmppServiceDidLostConnection:self];
    } else if (error.code == kForceCloseSocketErrorCode) {
        // Important ! trigger lostconn when we force close the socket !
        [_loginDelegate xmppServiceDidLostConnection:self];
        
    } else {
        // We disconnect properly but return the error (if exist) for the case that we lost network connection
        [_loginDelegate xmppServiceDidDisconnect:self withError:error];
    }
    
    [_mucPresenceTracker removeAllIDs];
    _wasCleanDisconnect = NO;
}

- (void)xmppReconnect:(XMPPReconnect *)sender didDetectAccidentalDisconnect:(SCNetworkConnectionFlags)connectionFlags {
    NSLog(@"We detect a network disconnection");
    [_loginDelegate xmppServiceDidLostConnection:self];
}

#pragma mark - Presence

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
    [self xmppStream:sender didReceivePresence:presence withLogs:YES];
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence withLogs:(BOOL)withLogs {
    if (withLogs)
        NSLog(@"[XMPPService] Did receive presence %@", presence);
    
    if (!_isAbleToRefreshUI)
        [self resetTimerBeforeAllowingToRefreshUI];
   
    if(!_xmppRoster.hasRoster && _xmppRoster.isPopulating){
        if (withLogs)
            NSLog(@"We are currently populating the roster so presence cannot be treated yet");
        return;
    }
    // Ignore presence element from MUC
    NSXMLElement *xMUCElement = [presence elementForName:@"x" xmlns:XMPPMUCUserNamespace];
    if (xMUCElement) {
        // extract the element ID for the tracker
        
        
        // Process rainbow status codes.
        // 332 System shutdown, must join again the room
        
        BOOL isServerShutdown = NO;
        BOOL isJoined = NO;
        for (NSXMLElement *status in [xMUCElement elementsForName:@"status"]) {
            switch ([status attributeIntValueForName:@"code"]){
                case 332: isServerShutdown = YES; break;
                case 110: isJoined = YES; break;
            }
        }
        if(isServerShutdown){
            if (withLogs)
                NSLog(@"We detect server shutdown, re-join the given");
            // We must join again this room
            Room *room = [_roomsService getRoomByJid:presence.from.bare];
            if(room){
                XMPPRoom *xmppRoom = [self getXMPPRoomForJID:presence.from];
                [xmppRoom changeLeaveState];
                [self joinRoom:room];
            }
        }
        

        
        
        if(isJoined && presence.elementID){
            if (withLogs)
                NSLog(@"The room has been join remove the presence with id %@ from our tracker", presence.elementID);
            [_mucPresenceTracker removeID:presence.elementID];
        }
        if (withLogs)
            NSLog(@"MUC presence, not treated here");
        return;
    }
    
    if ([presence isErrorPresence]) {
        NSString* cause = [presence errorCause];
        // Room problem on XMPP server => do not join
        if ([cause isEqualToString:@"Room creation failed"] ||
            [cause isEqualToString:@"Room creation is denied by service policy"] ||
            [cause isEqualToString:@"Conference room does not exist"]) {
            Room *room = [_roomsService getRoomByJid:presence.from.bare];
            if(room){
                [_mucPresenceTracker removeID:presence.elementID];
                return;
            }
        }
    }
    // Ignore presence with actor element (they will be treated by custom rainbow module)
    if([presence isPresenceWithActor]){
        if (withLogs)
            NSLog(@"Presence with actor, it's managed by another delegate");
        return;
    }
    
    if([presence.type isEqualToString:@"subscribe"]){
        if (withLogs)
            NSLog(@"Don't treat this kind of presence, it's managed by another delegate %@", presence);
        return;
    }
    
    Contact *contact = [_contactsManager createOrUpdateRainbowContactFromPresence:presence];
    if(!contact) {
        return;
    }
    
    BOOL isTelephonicPresence = NO;
    NSString *jid = presence.from.bare;
    if([presence.from.bare containsString:@"tel_"]){
        isTelephonicPresence = YES;
        [_contactsManager updateRainbowContact:contact withTelephonyRessource:isTelephonicPresence];
        jid = [presence.from.bare stringByReplacingOccurrencesOfString:@"tel_" withString:@""];
    }
    
    BOOL isCalendarPresence = NO;
    if([presence.fromStr containsString:@"/calendar"]){
        isTelephonicPresence = NO;
        isCalendarPresence = YES;
    }
    
    if([presence.type isEqualToString:@"subscribe"]){
        if (withLogs)
            NSLog(@"Don't treat this kind of presence, it's managed by another delegate %@", presence);
        return;
    }
    
    XMPPUserMemoryStorageObject *user = [_xmppRosterStorage userForJID:[XMPPJID jidWithString:jid] options:XMPPJIDCompareUser];
    
    if(!user && ![presence.type isEqualToString:@"unavailable"] && !isCalendarPresence){
        if (withLogs)
            NSLog(@"User not found in roster storage %@", jid);
        return;
    }
    
    if([presence.type isEqualToString:@"unsubscribed"]){
        [_contactsManager updateRainbowContactNotInRoster:contact];
    }
    
    if([presence.type isEqualToString:@"subscribed"]){
        [_contactsManager updateRainbowContactIsInRoster:contact];
    }
    
    // Calendar presence.
    if(isCalendarPresence) {
        [_contactsManager updateRainbowContact:contact withCalendarPresence:presence];
        return;
    }
    
    //Implement something for PBX telephonic states. PBX Presence override everything
    if(isTelephonicPresence && presence.status.length > 0 && ![presence.status isEqualToString:@"EVT_CONNECTION_CLEARED"]){
        [_contactsManager updateRainbowContact:contact withPresence:presence];
        return;
    }
    
    // If we are treating our own presence and if there is a status with mode=auto and our current presence is DND or XA, we must sent a presence message to online !
//    if([contact isEqual:_contactsManager.myContact] && !isTelephonicPresence){
//        if([presence.status isEqualToString:@"mode=auto"] && (_contactsManager.myContact.presence.presence == ContactPresenceDoNotDisturb || _contactsManager.myContact.presence.presence == ContactPresenceInvisible)) {
//            if (withLogs)
//                NSLog(@"We receive mode auto, so we force our presence to available");
//            [self setMyPresenceTo:[Presence presenceAvailable]];
//        }
//    }
    
    XMPPResourceMemoryStorageObject *dndResource = nil;
    XMPPResourceMemoryStorageObject *xaResource = nil;
    XMPPResourceMemoryStorageObject *onlineResource = nil;
    XMPPResourceMemoryStorageObject *awayResource = nil;
    XMPPResourceMemoryStorageObject *awayManualResource = nil;
    
    XMPPPresence *presenceStanzaToUse = presence;
    
    if (withLogs)
        NSLog(@"Treating presence for %@",contact.jid);
    
    for (XMPPResourceMemoryStorageObject *aResource in user.allResources) {
        if (withLogs)
            NSLog(@"Searching good presence in all user resources %@", aResource);
        if(aResource.presence.intShow == 0){
            if (withLogs) {
                if([aResource.presence.status isEqualToString:@"phone"])
                    NSLog(@"Ressource %@ is busy on phone", aResource);
                else if([aResource.presence.status isEqualToString:@"video"])
                    NSLog(@"Ressource %@ is busy in video", aResource);
                else if([aResource.presence.status isEqualToString:@"presentation"])
                    NSLog(@"Ressource %@ is busy in presentation", aResource);
                else if([aResource.presence.status isEqualToString:@"sharing"])
                    NSLog(@"Ressource %@ is busy in sharing", aResource);
                else
                    NSLog(@"Resource %@ is dnd", aResource);
            }
            
            dndResource = aResource;
        } else if (aResource.presence.intShow == 1){
            // XA
            if([aResource.presence.status isEqualToString:@"away"]){
                if (withLogs)
                    NSLog(@"Resource %@ is away (manual)", aResource);
                awayManualResource = aResource;
            } else {
                if (withLogs)
                    NSLog(@"Resource %@ is invisible", aResource);
                xaResource = aResource;
            }
        } else if (aResource.presence.intShow == 3) {
            // Online
            if (withLogs)
                NSLog(@"Resource %@ is Online", aResource);
            onlineResource = aResource;
        } else if (aResource.presence.intShow == 2){
            // Away
            if (withLogs)
                NSLog(@"Resource %@ is Away", aResource);
            awayResource = aResource;
        }
    }
    
    if(dndResource){
        presenceStanzaToUse = dndResource.presence;
        if(xaResource){
            // Compare the dnd stanza presence date and the xa stanza date
            // use the more recent stanza
            NSDate *dndPresenceDate = [self presenceDateForResource:dndResource];
            NSDate *xaPresenceDate = [self presenceDateForResource:xaResource];
            if (withLogs)
                NSLog(@"Comparing stanza delivery date : dnd %@  xa %@",dndPresenceDate, xaPresenceDate );
            if([xaPresenceDate isLaterThanDate:dndPresenceDate])
                presenceStanzaToUse = xaResource.presence;
        }
    } else if(xaResource){
        presenceStanzaToUse = xaResource.presence;
        if(dndResource){
            // Compare the dnd stanza presence date and the xa stanza date
            // use the more recent stanza
            NSDate *dndPresenceDate = [self presenceDateForResource:dndResource];
            NSDate *xaPresenceDate = [self presenceDateForResource:xaResource];
            if (withLogs)
                NSLog(@"Comparing stanza delivery date : dnd %@  xa %@",dndPresenceDate, xaPresenceDate );
            if([dndPresenceDate isLaterThanDate:xaPresenceDate])
                presenceStanzaToUse = dndResource.presence;
        }
    } else if (awayManualResource) {
        presenceStanzaToUse = awayManualResource.presence;
    } else if(onlineResource){
        [self searchMobileRessourceForUser:user];
        presenceStanzaToUse = onlineResource.presence;
    } else if(awayResource){
        presenceStanzaToUse = awayResource.presence;
    }
    
    if(!isTelephonicPresence){
        if (withLogs)
            NSLog(@"We must check if the user have a telephony presence");
        // Check if the user have a telephony presence
        if(contact.jid_tel.length > 0){
            XMPPUserMemoryStorageObject *user_tel = [_xmppRosterStorage userForJID:[XMPPJID jidWithString:contact.jid_tel]];
            if(user_tel.allResources.count > 0) {
                for (XMPPResourceMemoryStorageObject *tel_ressource in user_tel.allResources) {
                    if(([tel_ressource.jid.full containsString:@"/phone"] || [tel_ressource.jid.full containsString:@"/conf"]) && ![tel_ressource.presence.status isEqualToString:@"EVT_CONNECTION_CLEARED"]) {
                        if (withLogs)
                            NSLog(@"We found a telephony ressource not in cleared state so we use this presence");
                        presenceStanzaToUse = tel_ressource.presence;
                        break;
                    }
                }
            }
        }
    }
    
    if (withLogs)
        NSLog(@"Computed presence for contact : %@ use the presence stanza %@", contact.jid, presenceStanzaToUse);
    [_contactsManager updateRainbowContact:contact withPresence:presenceStanzaToUse withLogs:withLogs];
}

-(NSDate *) presenceDateForResource:(XMPPResourceMemoryStorageObject *) resource {
    NSDate *dateToReturn = nil;
    if (resource.presence.wasDelayed) {
        dateToReturn = resource.presence.delayedDeliveryDate;
    } else {
        dateToReturn = resource.presenceDate;
    }
    return dateToReturn;
}


#pragma mark - XMPPRosterDelegate
-(void) xmppRosterDidEndLoadingCache:(XMPPRoster *) roster {
    NSLog(@"DID END LOADING CACHE");
    [self loadPresenceFromCache];
    [self xmppRosterDidEndPopulating:roster fromCache:YES];
}

- (XMPPIQ *)xmppStream:(XMPPStream *)sender willSendIQ:(XMPPIQ *)iq {
    // We patch the roster iq to force it to request the version to the server ver="" to request it.
    NSXMLElement *element = [iq elementForName:@"query" xmlns:@"jabber:iq:roster"];
    if(element){
        if(![element attributeForName:@"ver"]){
            [element addAttributeWithName:@"ver" stringValue:_rosterVersion];
        }
        if(!iq.elementID){
            [iq addAttributeWithName:@"id" stringValue:[XMPPStream generateUUID]];
        }
    }

    return iq;
}

- (XMPPPresence *)xmppStream:(XMPPStream *)sender willSendPresence:(XMPPPresence *)presence {
    // Start a tracker for muc presence ??
    if([presence.to.bare containsString:@"room_"]){
//        <presence to="room_8b302d50eacb4fbabc5e5bfe67f24466@muc.fdie-all-in-one-dev-2.opentouch.cloud/b32b77b4816e4d4e9003bd8e25aae96b@fdie-all-in-one-dev-2.opentouch.cloud/mobile_ios_85239ECE-FDAA-4FD0-A129-EE9516304ADF"><x xmlns="http://jabber.org/protocol/muc"><history maxchars="0"/></x></presence>
        
        // We must add an ID for the tracker
        [presence addAttributeWithName:@"id" stringValue:[Tools generateUniqueID]];
        
        __weak __typeof(self) weakSelf = self;
        __weak __typeof(_roomsService) weakRoomsService = _roomsService;
        [_mucPresenceTracker addElement:presence block:^(id obj, id<XMPPTrackingInfo> info) {
            // Retry if timeout ?
            // search for the room and treat it as and error
            Room *room = [weakRoomsService getRoomByJid:info.element.to.bare];
            XMPPRoom *xmppRoom = [weakSelf getXMPPRoomForJID:info.element.to];
            if(room){
                [xmppRoom changeLeaveState];
                [weakSelf joinRoom:room];
            }
        } timeout:10];
    }
    return presence;
}

- (void)xmppRoster:(XMPPRoster *)sender didReceivePresenceSubscriptionRequest:(XMPPPresence *)presence {
    NSLog(@"didReceivePresenceSubscriptionRequest %@", presence);
}

-(void) acceptBuddyRequestFrom:(Contact *) contact {
    // with AddToRoster:YES, we will auto subscribe to the contact's presence.
    [_xmppRoster acceptPresenceSubscriptionRequestFrom:[XMPPJID jidWithString:contact.jid] andAddToRoster:YES];
    // We also want to subscribe to telephony presence update of this user
    [_xmppRoster subscribePresenceToUser:[XMPPJID jidWithString:contact.jid_tel]];
}

-(void) rejectBuddyRequestFrom:(Contact *) contact {
    [_xmppRoster rejectPresenceSubscriptionRequestFrom:[XMPPJID jidWithString:contact.jid]];
}

-(void)xmppRosterDidBeginPopulating:(XMPPRoster *)sender withVersion:(NSString *)version {
    // Save the version
    _rosterVersion = version;
    [[RainbowUserDefaults sharedInstance] setObject:version forKey:@"rosterVersion"];
}

- (void)xmppRosterDidEndPopulating:(XMPPRoster *)sender {
    [self xmppRosterDidEndPopulating:sender fromCache:NO];
    [self setMyPresenceTo:[Presence presenceAvailable]];
}

-(void) xmppRosterDidEndPopulating:(XMPPRoster *)sender fromCache:(BOOL) cache {
    NSLog(@"DID POPULATE ROSTER");
    NSMutableArray *jids = [NSMutableArray array];
    for (XMPPUserMemoryStorageObject *user in [_xmppRosterStorage unsortedUsers]) {
        
        XMPPUserMemoryStorageObject *theRealUser = user;
        // We don't want to create contact for tel_ jid so search for the real contact
        if([user.jid.bare containsString:@"tel_"]){
            NSString *jidWithoutTel = [user.jid.bare stringByReplacingOccurrencesOfString:@"tel_" withString:@""];
            theRealUser = [_xmppRosterStorage userForJID:[XMPPJID jidWithString:jidWithoutTel] options:XMPPJIDCompareUser];
        }
        if(theRealUser) {
            Contact *contact = [_contactsManager createOrUpdateRainbowContactFromXMPPUser:theRealUser];
            
            if([user.jid.bare containsString:@"tel_"]){
                // We add the tel_ jid in the contact
                [_contactsManager updateRainbowContact:contact withJidTel:user.jid.bare];
            }
            
            if(![jids containsObject:contact.jid] && contact)
                [jids addObject:contact.jid];
        }
    }
    
    NSLog(@"Get user network");
    [_contactsManager populateRosterVcardWithLimit:[jids count]];
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didAddUser:(XMPPUserMemoryStorageObject *)user {
    //    NSLog(@"Did add xmpp user %@", user);
    if(user.jid && ![user.jid.user hasPrefix:@"tel_"]){
        Contact *contact = [_contactsManager createOrUpdateRainbowContactFromXMPPUser:user];
        if(contact.jid)
            [_contactsManager addJidToFetch:contact.jid];
        [_contactsManager updateRainbowContactIsInRoster:contact];
    }
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didUpdateUser:(XMPPUserMemoryStorageObject *)user {
//    NSLog(@"Did update xmpp user %@ %@", user, user.subscription);
    [_contactsManager createOrUpdateRainbowContactFromXMPPUser:user];
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didRemoveUser:(XMPPUserMemoryStorageObject *)user {
//    NSLog(@"Did remove xmpp user %@", user);
    Contact *contact = [_contactsManager getContactWithJid:user.jid.bare];
    [_contactsManager deleteRainbowContact:contact];
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didAddResource:(XMPPResourceMemoryStorageObject *)resource withUser:(XMPPUserMemoryStorageObject *)user {
//    NSLog(@"Did add resource %@ for xmpp user %@", resource, user);
    [self searchMobileRessourceForUser:user];
    [self searchTelephonyRessourceForUser:user];
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didUpdateResource:(XMPPResourceMemoryStorageObject *)resource withUser:(XMPPUserMemoryStorageObject *)user {
//    NSLog(@"Did update resource %@ for xmpp user %@", resource, user);
    [self searchMobileRessourceForUser:user];
    [self searchTelephonyRessourceForUser:user];
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didRemoveResource:(XMPPResourceMemoryStorageObject *)resource withUser:(XMPPUserMemoryStorageObject *)user {
//    NSLog(@"Did remove resource %@ for xmpp user %@", resource, user);
    [self searchMobileRessourceForUser:user];
    [self searchTelephonyRessourceForUser:user];
}

-(void) searchMobileRessourceForUser:(XMPPUserMemoryStorageObject *) user {
    __block BOOL foundMobileDevice = NO;
    [user.allResources enumerateObjectsUsingBlock:^(XMPPResourceMemoryStorageObject *aRessource, NSUInteger idx, BOOL * stop) {
        if([aRessource.jid.resource containsString:@"mobile_"]){
            foundMobileDevice = YES;
            *stop = YES;
        }
    }];
    
    __block BOOL foundAnAvailableRessource = YES;
    if(foundMobileDevice){
        [user.allResources enumerateObjectsUsingBlock:^(XMPPResourceMemoryStorageObject *aRessource, NSUInteger idx, BOOL * stop) {
            if(aRessource.presence.intShow == 3 && ![aRessource.jid.resource containsString:@"mobile_"]){
                foundAnAvailableRessource = NO;
                *stop = YES;
            }
        }];
    }
    
    BOOL isConnectedThroughMobile = NO;
    if(foundMobileDevice && foundAnAvailableRessource)
        isConnectedThroughMobile = YES;
    
    Contact *contact = [_contactsManager getContactWithJid:user.jid.bare];
    [_contactsManager updateRainbowContact:contact withMobileRessource:isConnectedThroughMobile];
}

-(void) searchTelephonyRessourceForUser:(XMPPUserMemoryStorageObject *) user {
    __block BOOL foundTelephonyPresence = NO;
    [user.allResources enumerateObjectsUsingBlock:^(XMPPResourceMemoryStorageObject *aRessource, NSUInteger idx, BOOL * stop) {
        if([aRessource.jid.resource containsString:@"phone"]){
            foundTelephonyPresence = YES;
            *stop = YES;
        }
    }];
    
    NSString *jidWithoutTel = [user.jid.bare stringByReplacingOccurrencesOfString:@"tel_" withString:@""];

    Contact *contact = [_contactsManager getContactWithJid:jidWithoutTel];
    [_contactsManager updateRainbowContact:contact withTelephonyRessource:foundTelephonyPresence];
}

-(void) addContactWithJid:(NSString *) jid {
    [_xmppRoster addUser:[XMPPJID jidWithString:jid] withNickname:nil];
}

-(void) removeContactWithJid:(NSString *) jid {
    [_xmppRoster removeUser:[XMPPJID jidWithString:jid]];
    if(!_isConnected && ![jid hasPrefix:@"tel_"]){
        // Simulate that the user has been removed
        Contact *contact = [_contactsManager getContactWithJid:jid];
        [_contactsManager deleteRainbowContact:contact];
    }
}

#pragma mark - Core Data


#pragma mark - Send and receive message
-(Message *) sendMessage:(NSString *) message ofType:(MessageType) type to:(Peer *) peer withCompletionHandler:(XmppServiceSendMessageCompletionHandler) completionHandler {
    XMPPMessage *msg = [self createXMPPMessage:message ofType:type withOBData:nil desc:nil withId:nil to:peer withCompletionHandler:completionHandler];
    return [self sendXMPPMessage:msg];
}

-(Message *) sendXMPPMessage:(XMPPMessage *)msg {
    return [self sendXMPPMessage:msg fake:NO];
}

-(Message *) sendLocalyXMPPMessage:(XMPPMessage *)msg {
    return [self sendXMPPMessage:msg fake:YES];
}

- (NSDate *) getCorrectDateForMessageWithPeerJid:(NSString *)peerJid andDate :(NSDate *)date{
    NSArray *messagesArray = [self getArchivedMessageWithContactJid:peerJid maxSize:@1 offset:0];

    if (messagesArray.count) {
        if ([messagesArray objectAtIndex:0]) {
            Message *lastMessage = [messagesArray objectAtIndex:0];
            return ([lastMessage.timestamp isLaterThanDate:date])?[NSDate dateWithTimeInterval:1.0 sinceDate:lastMessage.timestamp]:date;
        }
    }
    
    return date;
}

-(Message *) sendXMPPMessage:(XMPPMessage *)msg fake:(BOOL) fake {
    // Save the message in a cache and remove it from the cache when the didSend fire
    // At startup resend all messages still in cache
    NSString * peerJid = [[msg attributeForName:@"to"] stringValue];
    // check the current data if less then last message, use last message date.
    NSDate *msgDate = [self getCorrectDateForMessageWithPeerJid:peerJid andDate:[NSDate date]];
    XMPPMessage *cachedMessage = [msg copy];
    // Add retransmission in cached message
    
    NSXMLElement *retransmission = [NSXMLElement elementWithName:@"retransmission" xmlns:@"jabber:iq:notification"];
    [retransmission addAttributeWithName:@"source" stringValue:@"client"];
    [cachedMessage addChild:retransmission];
    
    NSXMLElement *timestamp = [NSXMLElement elementWithName:@"timestamp" xmlns:@"urn:xmpp:receipts"];
    [timestamp addAttributeWithName:@"value" stringValue:[msgDate xmppDateTimeString]];
    [cachedMessage addChild:timestamp];
    if(!fake){
        [_savedActionscache setObjectAsync:cachedMessage forKey:msg.elementID completion:nil];
        [_xmppStream sendElement:cachedMessage];
    }
    
    
    Message *theMessage = [self messageFromXmppMessage:cachedMessage];
    theMessage.timestamp = msgDate;
    [theMessage setDeliveryState:MessageDeliveryStateSent atTimestamp:msgDate];
   
    [_conversationDelegate xmppService:self willSendMessage:theMessage];
    return theMessage;
}


-(void) removeXMPPMessagefromCache:(XMPPMessage *)msg {
    [_sendMessageRequestsCompletionHandler removeObjectForKey:msg.elementID];
    [_savedActionscache removeObjectForKey:msg.elementID];
}

-(void) removeMessageWithMessageIDfromCache:(NSString *)messageID {
    [_savedActionscache removeObjectForKeyAsync:messageID completion:nil];
}

- (XMPPMessage *) createXMPPMessage:(NSString *) message ofType:(MessageType) type withOBData:(NSURL *)url desc:(NSString *)urlDesc withId:(NSString *) messageID to:(Peer *) peer withCompletionHandler:(XmppServiceSendMessageCompletionHandler) completionHandler {
    NSString *jid = peer.jid;
    BOOL isMessageForABot = NO;
    if ([peer isKindOfClass:[Contact class]])
        isMessageForABot = ((Contact*)peer).isBot;
    
    if (!jid || [jid isEqualToString:@""]) {
        NSLog(@"Error: Cannot send message to empty JID.");
        if(completionHandler) {
            NSError *error = [NSError errorWithDomain:@"XMPPService" code:2 userInfo:@{NSLocalizedDescriptionKey:@"cannot send message to empty jid."}];
            completionHandler(nil, error);
        }
        return nil;
    }
    
    // Our message need an ID for XEP-0184 "Message Delivery Receipts"
    // Or the receiver will not be able to track and ACK the message.
     //if (!messageID) {
        messageID = [_xmppStream generateUUID];
     // }
    
    
    NSString *xmppType;
    switch (type) {
        case MessageTypeChat:{
            xmppType = @"chat";
            break;
        }
        case MessageTypeGroupChat:{
            xmppType = @"groupchat";
            break;
        }
        case MessageTypeGroupChatEvent:{
            xmppType = @"groupchatevent";
            break;
        }
        case MessageTypeWebRTC:{
            xmppType = @"webrtc";
            break;
        }
        case MessageTypeWebRTCStart:{
            xmppType = @"webrtc-start";
            break;
        }
        case MessageTypeWebRTCRinging:{
            xmppType = @"webrtc-ringing";
            break;
        }
        case MessageTypeFileTransfer:{
            xmppType = @"file";
            break;
        }
        case MessageTypeUnknown:
        default:{
            NSLog(@"Error : cannot send message of unknown type : %ld", (long)type);
            if(completionHandler) {
                NSError *error = [NSError errorWithDomain:@"XMPPService" code:3 userInfo:@{NSLocalizedDescriptionKey:@"Cannot send message of unknown type"}];
                completionHandler(nil, error);
            }
            return nil;
        }
    }
    if(completionHandler){
        @synchronized(_completionHandlerDictionaryMutex){
            [_sendMessageRequestsCompletionHandler setObject:[completionHandler copy] forKey:messageID];
        }
    }
    
    XMPPMessage* msg = [[XMPPMessage alloc] initWithType:xmppType to:[XMPPJID jidWithString:jid] elementID:messageID];
    if(isMessageForABot){
        NSString *languageCode = [[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode];
        [msg addAttributeWithName:@"xml:lang" stringValue:languageCode];
        [msg addBody:message withLanguage:languageCode];
    } else
        [msg addBody:message];
    
    // Force storage in MAM
    // espacially usefull for OutOfBand, without <body/> messages
    NSXMLElement *storeElement = [NSXMLElement elementWithName:@"store" xmlns:@"urn:xmpp:hints"];
    [msg addChild:storeElement];
    
    // XEP0066 XMPP Out of Band Data
    if(url){
        [msg addOutOfBandURL:url desc:urlDesc];
    }
    
    [msg addNamespaceWithPrefix:@"" stringValue:@"jabber:client"];

    return msg;
}



- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message {
    if(message.elementID){
        XmppServiceSendMessageCompletionHandler completionHandler = nil;
        @synchronized(_completionHandlerDictionaryMutex){
            completionHandler = [_sendMessageRequestsCompletionHandler objectForKey:message.elementID];
        }
        
        
        Message *theMessage = [self messageFromXmppMessage:message];
        Message *savedMessage = [_xmppMessageArchiveManagementStorage fetchMessagesForJID:[XMPPJID jidWithString:theMessage.peer.jid] wtihMessageId:theMessage.messageID xmmpStream:_xmppStream];
        
        if (savedMessage.timestamp != nil) {
            theMessage.timestamp = savedMessage.timestamp;
        }
        else{
            theMessage.timestamp = [self getCorrectDateForMessageWithPeerJid:theMessage.peer.jid andDate:[NSDate date]];
        }
        
        [theMessage setDeliveryState:MessageDeliveryStateSent atTimestamp:theMessage.timestamp];
        if(completionHandler){
            NSLog(@"Invoking completion handler for message %@", theMessage);
            completionHandler(theMessage, nil);
        }
        
        [_conversationDelegate xmppService:self didSendMessage:theMessage];
        
        @synchronized(_completionHandlerDictionaryMutex){
            [_sendMessageRequestsCompletionHandler removeObjectForKey:message.elementID];
        }
    }
}

- (void)xmppStream:(XMPPStream *)sender didFailToSendMessage:(XMPPMessage *)message error:(NSError *)error {
    NSLog(@"did fail to send message");
    if(message.elementID){
        XmppServiceSendMessageCompletionHandler completionHandler = nil;
        @synchronized(_completionHandlerDictionaryMutex){
            completionHandler = [_sendMessageRequestsCompletionHandler objectForKey:message.elementID];
        }
        
        if(completionHandler)
            completionHandler(nil, error);
        @synchronized(_completionHandlerDictionaryMutex){
            [_sendMessageRequestsCompletionHandler removeObjectForKey:message.elementID];
        }
    }
}

-(void) resetTimerBeforeAllowingToRefreshUI {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self resetTimerBeforeAllowingToRefreshUI];
        });
        return;
    }
    
    if (_timerToRefreshUI) {
        [_timerToRefreshUI invalidate];
        _timerToRefreshUI = nil;
    }
    
    _timerToRefreshUI = [NSTimer scheduledTimerWithTimeInterval:(0.3)
                         target:self
                         selector:@selector(timerToRefreshUIFired:)
                         userInfo:nil
                         repeats:NO];
}

-(void) timerToRefreshUIFired:(NSTimer *)theTimer {
    OTCLog(@"No more XMPP requests after resume - end of timer and post notification");
    [self setIsAbleToRefreshUIAndNotify:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kEndReceiveXMPPRequestsAfterResume object:nil];
}

-(void) setIsAbleToRefreshUIAndNotify:(BOOL) isAbleToRefreshUI {
    OTCLog(@"Change of isAbleToRefreshUI boolean to %s - notify UI", isAbleToRefreshUI ? "YES" : "NO");
    _isAbleToRefreshUI = isAbleToRefreshUI;
    [[NSNotificationCenter defaultCenter] postNotificationName:kIsAbleToRefreshUIChanged object:nil userInfo:@{@"isAbleToRefreshUI": @(isAbleToRefreshUI)}];
}

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
#if DEBUG
    NSLog(@"RECEIVE MESSAGE %@", message);
#endif
    
    if (!_isAbleToRefreshUI)
        [self resetTimerBeforeAllowingToRefreshUI];
    
    // Ignore messages webrtc-start and webrtc-ringing messages...
    if([message isWebRTCCall] && ![message isWebRTCCallWithSupportedBody] && ![message isWebRTCCallWithCallLogInfo]){
        return;
    }
    
    // Ignore message of type management they are treated by XMPPMessageManagement module
    if([message isManagementMessage] || [message isConferenceManagementMessage]){
        [_xmppRainbowCustomStanzaSupport treatManagementXMPPMessage:message];
        return;
    }
    
    // Ignore mam calllog message (they are treated by mam module directly)
    if([message isCallLogMessage] || [message isDeletedCallLogMessage] || [message isReadCallLogNotificationMessage]){
        return;
    }
    
    // Deal with updated callLog (live call log)
    if([message isUpdatedCallLogMessage] && (![message.from.bare containsString:@"janusgateway"] || ![message.from.bare containsString:@"mp_"])){
        CallLog *theCallLog = [self callLogFromUpdatedCallLogMessage:message];
        [_callLogsDelegate xmppService:self didAddCallLog:theCallLog];
        return;
    }
    
    // Deal with group message with event tag
    if([message isGroupChatMessageWithBodyWithEvent]){
        Message *theMessage = [self eventMessageFromXmppMessage:message];
        if(theMessage.groupChatEventType == MessageGroupChatEventConferenceAdd ||theMessage.groupChatEventType == MessageGroupChatEventConferenceRemove){
            // This message is for conference service
            [_conferenceDelegate xmppService:self didReceiveMessage:theMessage];
        }
        // Trigger didreceived
        [_conversationDelegate xmppService:self didReceiveMessage:theMessage];
        return;
    }
    
    if([XMPPPubSub isPubSubMessage:message]){
        NSXMLElement *event = [self channelEventFromXmppMessage:message];
        if(event){
            NSArray *delete = [event elementsForName:@"delete"];
            if([delete count] > 0){
                NSString *nodeId = [[delete[0] attributeForName:@"node"] stringValue];
                NSArray *nodeIdParts = [nodeId componentsSeparatedByString:@":"];
                if([nodeIdParts count]>0){
                    [_channelsDelegate xmppService:self didDeleteChannel:[nodeIdParts objectAtIndex: 0]];
                }
                return;
            } else {
                NSArray *items = [event elementsForName:@"items"];
                if([items count] > 0){
                    NSArray<NSDictionary *> *payloads = [self channelItemsFromXmppMessage:event];
                    [_channelsDelegate xmppService:self didReceiveItems:payloads];
                    return;
                }
            }
        } else {
            NSLog(@"No event in XMPP PubSub message");
        }
        return;
    }
    
    // Treat webRTC call logs (conversation)
    /**
     *  <message xmlns="jabber:client" from="fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud" to="c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud" type="webrtc" id="1488899586006591"><call_log xmlns="jabber:iq:notification:telephony:call_log" type="webrtc"><caller>fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud</caller><callee>c2bd56bf4ce54225a2bec296fb76e817@jerome-all-in-one-dev-1.opentouch.cloud</callee><state>answered</state><media>audio</media><duration>6839</duration><date>2017-03-07T15:13:06.006591Z</date></call_log></message>
     */
    if([message isWebRTCCallWithCallLogInfo]){
        Message *theMessage = [self callLogEventMessageFromXMPPMessage:message];
        // Trigger didreceived
        [_conversationDelegate xmppService:self didReceiveMessage:theMessage];
        return;
    }

    // Treat conference reminder message
    /**
     * <message xmlns="jabber:client" from="dd91ef00c2ca4a25b4c4704ca43e1beb@demo-all-in-one-dev-1.opentouch.cloud" to="9f46475b676c449eadb6268747f7da77@demo-all-in-one-dev-1.opentouch.cloud" type="chat" id="3910a6b5-edf4-4193-b74c-68b653ab41e6_9732">
         <x xmlns="jabber:x:audioconference" type="reminder" subject="Tests" start_date="2017-09-06T11:15:00.000Z" jid="room_a2ed26b89af84b2c8692b9e14298df59@muc.demo-all-in-one-dev-1.opentouch.cloud" thread="59afd6eb9707ef4a10ae6b00" confendpointid="59afd6f607d823d9a3fc35b3"/>
       </message>
     */
    if([message isConferenceReminder]){
        Message *theMessage = [self conferenceReminderMessageFromXmppMessage:message];
        // Trigger didreceived
        [_conversationDelegate xmppService:self didReceiveMessage:theMessage];
        return;
    }
    
    // chat message with changed
    /**
     * Notification received when the history of the room is available for the user (after joining the room with a big historic)
     *  <message xmlns="jabber:client" from="jerome-all-in-one-dev-1.opentouch.cloud" to="a62332478ded40e184076392dd0fbdb9@jerome-all-in-one-dev-1.opentouch.cloud" type="chat">
        <changed xmlns="jabber:iq:notification" id="all" with="room_6519dceb686047a490d0ca85d784dcac@muc.jerome-all-in-one-dev-1.opentouch.cloud"/>
        </message>
     */
    if( [message isChatMessageWithChangedNotification] ) {
        NSXMLElement *element = [message elementForName:@"changed"];
        NSString *roomJid = [[element attributeForName:@"with"] stringValue];
        Peer *peer = [self peerFromJidString: roomJid];
        
        // Trigger didreceived after setting the correct state (i.e. received)
        [_conversationDelegate xmppService:self didReceiveChangedInConversationWithPeer: peer];
        return;
    }
    
    //
    /**
     * <message xmlns="jabber:client" default:lang="fr" to="a62332478ded40e184076392dd0fbdb9@jerome-all-in-one-dev-1.opentouch.cloud/mobile_ios_F6BE3F34-447C-43C4-86B9-8B3A117FF972" from="c2673e1952ef40178b68cb4ae2c6c2ed@jerome-all-in-one-dev-1.opentouch.cloud/web_win_1.37.6_pxKsVeBB" type="chat" id="web_49ebfebd-997c-4e00-9e9f-5568cbbd2ffb1"><recording xmlns="jabber:iq:recordingP2P">start</recording><request xmlns="urn:xmpp:receipts"/><active xmlns="http://jabber.org/protocol/chatstates"/><body xmlns="jabber:client" lang="fr"/></message>
     */
    if ([message isChatMessageWithRecordingState]) {
        NSString *recordingState = [message elementForName:@"recording"].stringValue;
        [_rtcService xmppService:self didReceiveRemoteRecordingStatusChanged:recordingState forJID:message.from.bareJID.bare];
        
        Message *theMessage = [self messageFromXmppMessage:message];
        [_conversationDelegate xmppService:self didReceiveMessage:theMessage];
        return;
    }
    
    // Treat CallService message
    /**
     * <message xmlns="jabber:client" xmlns:ns2="jabber:client" from="tel_98d07126828d471682fb700945bde960@philou-all-in-one-dev-1.opentouch.cloud/phone" to="98d07126828d471682fb700945bde960@philou-all-in-one-dev-1.opentouch.cloud" type="normal"><callservice xmlns="urn:xmpp:pbxagent:callservice:1" xmlns:ns7="urn:xmpp:pbxagent:callservice:1"><nomadicStatus destination="00399887766" featureActivated="true" modeActivated="true"/></callservice></message>
     */
    if ([message isCallServiceWithNomadicStatus]) {
        Message *theMessage = [self messageFromXmppMessage:message];
        [_telephonyDelegate xmppService:self didReceiveNomadicStatusUpdate:theMessage.nomadicStatus];
        return;
    }
    
    /** CallForwardStatus
     * <message xmlns="jabber:client" default:lang="en" to="98d07126828d471682fb700945bde960@philou-all-in-one-dev-1.opentouch.cloud/mobile_ios_3B5AD4EE-CF79-439A-A03B-A22807DFD571" from="tel_98d07126828d471682fb700945bde960@philou-all-in-one-dev-1.opentouch.cloud/phone" id="40"><callservice xmlns="urn:xmpp:pbxagent:callservice:1" xmlns:ns7="urn:xmpp:pbxagent:callservice:1"><forwarded forwardTo="VOICEMAILBOX" forwardType="Activation"/></callservice></message>
     
     * <message xmlns="jabber:client" default:lang="en" to="98d07126828d471682fb700945bde960@philou-all-in-one-dev-1.opentouch.cloud/mobile_ios_3B5AD4EE-CF79-439A-A03B-A22807DFD571" from="tel_98d07126828d471682fb700945bde960@philou-all-in-one-dev-1.opentouch.cloud/phone" id="59"><callservice xmlns="urn:xmpp:pbxagent:callservice:1" xmlns:ns7="urn:xmpp:pbxagent:callservice:1"><forwarded forwardType="Deactivation"/></callservice></message>
     */

    if ([message isCallServiceMessage]) {
        CallForwardStatus *fwdStatus = [self callForwardStatusFromXMPPMessage:message];
        if (fwdStatus) {
            [_telephonyDelegate xmppService:self didReceiveCallForwardStatusUpdate:fwdStatus];
            return;
        }
    }
    
    /**
     <message xmlns='jabber:client' xml:lang='en' to='39341c3141d04ead8e0e24756e22be46@openrainbow.com/mobile_android_357537085482263' from='tel_39341c3141d04ead8e0e24756e22be46@openrainbow.com/phone' id='28456'><callservice xmlns='urn:xmpp:pbxagent:callservice:1' xmlns:ns7='urn:xmpp:pbxagent:callservice:1'><messaging><voiceMessageCounter>0</voiceMessageCounter></messaging></callservice></message>
     */
    if ( [self isCallServiceWithMessaging:message] ) {
        NSXMLElement *callServiceElem = [message elementForName:@"callservice" xmlns:@"urn:xmpp:pbxagent:callservice:1"];
        NSXMLElement *messagingElem = [callServiceElem elementForName:@"messaging"];
        NSXMLElement *voiceMessageCounterElem = [messagingElem elementForName:@"voiceMessageCounter"];
        NSInteger voiceMessageCounter = [voiceMessageCounterElem stringValueAsInt];
        
        NSLog(@"%@", [NSString stringWithFormat:@"CallService voiceMessageCounter: %ld", voiceMessageCounter ]);
        [_telephonyDelegate xmppService:self didReceiveVoicemailCountUpdate: voiceMessageCounter];
        return;
    }
    
    /** Call Event
     * <message xmlns='jabber:client' xml:lang='en' to='87a4b653ec2f47c6a648accff43b678b@openrainbow.net/mobile_android_355592070023822' from='tel_87a4b653ec2f47c6a648accff43b678b@openrainbow.net/phone' id='290'><callservice xmlns='urn:xmpp:pbxagent:callservice:1' xmlns:ns7='urn:xmpp:pbxagent:callservice:1'><initiated callId='10#9' deviceState='LCI_INITIATED' newCallId='20#9'/></callservice></message>
     * <message xmlns='jabber:client' xml:lang='en' to='87a4b653ec2f47c6a648accff43b678b@openrainbow.net/mobile_android_355592070023822' from='tel_87a4b653ec2f47c6a648accff43b678b@openrainbow.net/phone' id='292'><callservice xmlns='urn:xmpp:pbxagent:callservice:1' xmlns:ns7='urn:xmpp:pbxagent:callservice:1'><originated callId='20#9' deviceState='LCI_CONNECTED' endpointIm='522d516ed60e408d9310b99322305f79@openrainbow.net' endpointTel='113'/></callservice></message>
     * <message xmlns='jabber:client' xml:lang='en' to='87a4b653ec2f47c6a648accff43b678b@openrainbow.net/mobile_android_355592070023822' from='tel_87a4b653ec2f47c6a648accff43b678b@openrainbow.net/phone' id='293'><callservice xmlns='urn:xmpp:pbxagent:callservice:1' xmlns:ns7='urn:xmpp:pbxagent:callservice:1'><delivered callId='20#9' cause='NEWCALL' deviceState='LCI_CONNECTED' deviceType='SECONDARY' endpointIm='522d516ed60e408d9310b99322305f79@openrainbow.net' endpointTel='113' type='outgoing'/></callservice></message>
     * <message xmlns='jabber:client' xml:lang='en' to='87a4b653ec2f47c6a648accff43b678b@openrainbow.net/mobile_android_355592070023822' from='tel_87a4b653ec2f47c6a648accff43b678b@openrainbow.net/phone' id='296'><callservice xmlns='urn:xmpp:pbxagent:callservice:1' xmlns:ns7='urn:xmpp:pbxagent:callservice:1'><updateCall callId='20#9' endpointIm='522d516ed60e408d9310b99322305f79@openrainbow.net' endpointTel='113'><identity firstName='Cédric' lastName='Bruckner'/></updateCall></callservice></message>
     * <message xmlns='jabber:client' xml:lang='en' to='87a4b653ec2f47c6a648accff43b678b@openrainbow.net/mobile_android_355592070023822' from='tel_87a4b653ec2f47c6a648accff43b678b@openrainbow.net/phone' id='299'><callservice xmlns='urn:xmpp:pbxagent:callservice:1' xmlns:ns7='urn:xmpp:pbxagent:callservice:1'><established callId='20#9' endpointIm='522d516ed60e408d9310b99322305f79@openrainbow.net' endpointTel='113'/></callservice></message>
     */
    if([message isCallServiceMessage] && ![message wasDelayed]){
        CallEvent *event = [self callEventFromXMPPMessage:message];
        if(event){
            OTCLog(@"New CallEvent: %@", event);
            [_telephonyDelegate xmppService:self didReceiveCallEvent:event];
            return;
        }
    }
    
    if ([message isChatMessageWithMarkAllAsReadNotification]) {
        NSString *peerJid = [self markAllAsReadEventJid: message];
        BOOL isReadByMe = [self markAllAsReadIsReadByMe: message];
        [_conversationDelegate xmppService:self didReceiveMarkAllAsReadInConversationWithPeerJID:peerJid andReadByMe:isReadByMe];
    }
    
    // Deal with 'normal' chat message
    if (([message isChatMessageWithBody] || [message isGroupChatMessageWithBody]) || ([message wasDelayed] && ([message isChatMessageWithBody] || [message isGroupChatMessageWithBody])) || [message isWebRTCCallWithSupportedBody]) {
        Message *theMessage = [self messageFromXmppMessage:message];
        
        // XEP-184
        // Mark this message in Delivered state, as the server forwarded it to us.
        if([message hasReceiptRequest])
            [theMessage setDeliveryState:MessageDeliveryStateDelivered atTimestamp:theMessage.timestamp];
        
        // We receive our own message in muc cases so we must mark them as received *bullshit* server
        if(!theMessage.isOutgoing || [theMessage.via isKindOfClass:[Room class]]) {
            // XEP-184, send the received-ACK for normal message
            if([message hasReceiptRequest] && [message elementID] &&
               [_contactsManager getContactWithJid:message.from.bare] != _contactsManager.myContact) {
                
                NSDate * receivedDate ;
                if (theMessage.timestamp == nil) {
                    receivedDate = [NSDate date];
                }
                else{
                    receivedDate = theMessage.timestamp;
                }
                [theMessage setDeliveryState:MessageDeliveryStateReceived atTimestamp:[self getCorrectDateForMessageWithPeerJid:message.from.bare andDate:receivedDate]];
                [self sendReceivedByMeACKMessageToJID:message.from.bareJID messageID:[message elementID]];
                [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateMessage object:theMessage];
            }
        }
        
        // Trigger didreceived after setting the correct state (i.e. received)
        [_conversationDelegate xmppService:self didReceiveMessage:theMessage];
    }
    
    // Deal with carbon messages
    if([message isMessageCarbon]) {
        XMPPMessage *carbonMessage = [message messageCarbonForwardedMessage];
        
        // Carbon chat message
        
        Message *aMessage = [self messageFromXmppMessage:carbonMessage];
        
        // XEP-184
        // Mark this message in Delivered state, as the server forwarded it to us.
        [aMessage setDeliveryState:MessageDeliveryStateDelivered atTimestamp:aMessage.timestamp];
        
       
        if(aMessage.body.length > 0 || aMessage.attachment.url) {
             [_conversationDelegate xmppService:self didReceiveCarbonCopyMessage:aMessage];
        }
        
        // Carbon 'is typing' message
        if(([carbonMessage isChatMessage] || [carbonMessage isGroupChatMessage]) && [carbonMessage hasChatState] && ![message isMessageWithBody]) {
            Message *parsedMessage = [self messageFromXmppMessage:carbonMessage];
            if (!parsedMessage.isOutgoing) {
                parsedMessage.isComposing = [carbonMessage hasComposingChatState];
                [_conversationDelegate xmppService:self didReceiveComposingMessage:parsedMessage];
            }
        }
        
        // XEP-184, send the received-ACK for carbon message
        if([carbonMessage hasReceiptRequest] && [carbonMessage elementID] &&
           [_contactsManager getContactWithJid:carbonMessage.from.bare] != _contactsManager.myContact)
        {
            [self sendReceivedByMeACKMessageToJID:carbonMessage.from.bareJID messageID:[carbonMessage elementID]];
        }
        
        // The XEP-0184 Carbon received & read ACK messages
        if([carbonMessage hasReceiptResponse]) {
            Message *theMessage = [self messageFromXmppMessage:carbonMessage];
            NSString *messageID = [carbonMessage receiptResponseID];
            NSDate *timestamp = [carbonMessage timestampACKMessage];
            MessageDeliveryState state = [self messageDeliveryStateForXMPPMessage:carbonMessage];
            if(messageID.length > 0)
                [_conversationDelegate xmppService:self didReceiveDeliveryState:state at:timestamp forMessage:theMessage via:theMessage.via isOutgoing:theMessage.isOutgoing];
            else {
                NSLog(@"No message ID for this message %@", carbonMessage);
                NSAssert(messageID, @"MessageID should be set for didReceiveDeliveryState:");
            }
                
        }
    }
    
    // Deal with 'is typing' state in a received message
    if(([message isChatMessage] || [message isGroupChatMessage]) && [message hasChatState] && ![message isMessageWithBody]) {
        Message *parsedMessage = [self messageFromXmppMessage:message];
        if (!parsedMessage.isOutgoing) {
            parsedMessage.isComposing = [message hasComposingChatState];
            [_conversationDelegate xmppService:self didReceiveComposingMessage:parsedMessage];
        }
    }
    
    // The XEP-0184 'normal' (i.e. non-carbon) received & read ACK messages
    if([message hasReceiptResponse]) {
        Message *theMessage = [self messageFromXmppMessage:message];
        NSString *messageID = [message receiptResponseID];
        theMessage.messageID = messageID;
        NSDate *timestamp = [message timestampACKMessage];
        MessageDeliveryState state = [self messageDeliveryStateForXMPPMessage:message];
        
        NSString *entity = [[message elementForName:@"received"] attributeForName:@"entity"].stringValue;
        NSString *event = [[message elementForName:@"received"] attributeForName:@"event"].stringValue;
        if([event isEqualToString:@"received"] && [entity isEqualToString:@"server"]){
        // We could remove the message from our sendMessage cache
            NSLog(@"We receive the server received flag so we remove the message from our cache %@", messageID);
            [_savedActionscache removeObjectForKeyAsync:messageID completion:nil];
            [_internalOperationQueue addOperationWithBlock:^{
                [_outgoingStanzaCache removeObjectForKeyAsync:messageID completion:nil];
            }];
        }
        // There is no timestamp for received by server, so add one manually
        if (state == MessageDeliveryStateDelivered && !timestamp) {
            timestamp = [self getCorrectDateForMessageWithPeerJid:theMessage.peer.jid andDate:[NSDate date]];
        }
        
        
        
        [_conversationDelegate xmppService:self didReceiveDeliveryState:state at:timestamp forMessage:theMessage via:theMessage.via isOutgoing:theMessage.isOutgoing];
    
    }
}

-(MessageDeliveryState) messageDeliveryStateForXMPPMessage:(XMPPMessage *) message {
    if ([message isReceivedByServerACKMessage]) {
        return MessageDeliveryStateDelivered;
        
    } else if ([message isReceivedByClientACKMessage]) {
        return MessageDeliveryStateReceived;
        
    } else if ([message isReadByClientACKMessage]) {
        return MessageDeliveryStateRead;
    }
    return MessageDeliveryStateSent;
}


#pragma mark - send XEP-0184 Extension ACK for read message

// Move these functions in a separate 'XEP module', once the full extension will be implemented.

-(void) sendReceivedByMeACKMessageToJID:(XMPPJID *)toContactJID messageID:(NSString*)messageID {
    [self sendAckMessageTo:toContactJID.bare messageID:messageID eventType:@"received"];
}

-(void) sendReadByMeACKMessageTo:(NSString *)jid messageID:(NSString*)messageID {
    [self sendAckMessageTo:jid messageID:messageID eventType:@"read"];
}

-(void) sendAckMessageTo:(NSString *) jid messageID:(NSString *) messageID eventType:(NSString *) eventType {
    NSLog(@"Mark message with %@ messageID %@ as %@",jid, messageID, eventType);
    
    BOOL isRoom = [jid hasPrefix:@"room_"];
    BOOL isBot = [[XMPPJID jidWithString:jid] isServer];
    
    XMPPMessage *message = nil;
    if(isRoom) {
        // ACKs have to be of type 'groupchat' to be correctly sent in the room
        message = [[XMPPMessage alloc] initWithType:@"groupchat" to:[XMPPJID jidWithString:jid] elementID:[_xmppStream generateUUID] child:nil];
    } else {
        // ACKs have to be of type 'chat' to be correctly carboned by the server
        message = [[XMPPMessage alloc] initWithType:@"chat" to:[XMPPJID jidWithString:jid] elementID:[_xmppStream generateUUID] child:nil];
    }
    
    NSXMLElement *received = [NSXMLElement elementWithName:@"received" xmlns:@"urn:xmpp:receipts"];
    [received addAttributeWithName:@"id" stringValue:messageID];
    // ALE extension of XEP 184 :
    [received addAttributeWithName:@"event" stringValue:eventType];
    [received addAttributeWithName:@"entity" stringValue:@"client"];
    if(isRoom)
        [received addAttributeWithName:@"type" stringValue:@"muc"];
    else if (isBot)
        [received addAttributeWithName:@"type" stringValue:@"bot"];
    
    [message addChild:received];
   
    NSLog(@"SEND MESSAGE FOR MARK AS %@ : %@",eventType, message);
    [_xmppStream sendElement:message];
}


#pragma mark - XEP-085 Chat state

-(void) sendChatStateMessageTo:(Peer *) peer status:(ConversationStatus) status {
    XMPPMessage *message = nil;
    if ([peer isKindOfClass:[Contact class]]) {
        // ACKs have to be of type 'chat' to be correctly carboned by the server
        message = [[XMPPMessage alloc] initWithType:@"chat" to:[XMPPJID jidWithString:peer.jid] elementID:[XMPPStream generateUUID] child:nil];
    } else if ([peer isKindOfClass:[Room class]]) {
        // ACKs have to be of type 'groupchat' to be correctly dispatched by the room
        message = [[XMPPMessage alloc] initWithType:@"groupchat" to:[XMPPJID jidWithString:peer.jid] elementID:[XMPPStream generateUUID] child:nil];
    } else {
        return;
    }

    
    switch(status) {
        case ConversationStatusActive: {
            [message addActiveChatState];
            break;
        }
        case ConversationStatusInactive: {
            [message addInactiveChatState];
            break;
        }
        case ConversationStatusComposing: {
            [message addComposingChatState];
            break;
        }
    }
    
    [_xmppStream sendElement:message];
}


#pragma mark - Archive management

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndArchiveMessage:(XMPPMessage *)message {
    // Message has been archived, update contact in roster to keep trace of last message
    if (([message isChatMessageWithBody] || [message isGroupChatMessageWithBody]) || ([message wasDelayed] && ([message isChatMessageWithBody] || [message isGroupChatMessageWithBody])) || [message isWebRTCCallWithSupportedBody]) {
        Message *archivedMessage = [self messageFromXmppMessage:message];
        if(archivedMessage.isComposing)
            return;
        
        [_conversationDelegate xmppService:self didRetreiveNewLastMessageFromArchives:archivedMessage];
    }
}

-(void)loadArchivedMessagesWith:(Peer *) peer maxSize:(NSNumber *)maxSize beforeDate:(NSDate *)beforeDate lastMessageID:(NSString *)lastMessageID offset:(NSNumber *) offset withCompletionHandler:(XmppServiceLoadArchivedMessagesCompletionHandler) completionHandler {
    NSString *uniqueID = [_xmppStream generateUUID];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(peer.jid){
        [dic setObject:peer.jid forKey:@"jid"];
        if(completionHandler)
            [dic setObject:[completionHandler copy] forKey:peer.jid];
        else
            NSLog(@"No completion handler that is not possible");
        
        if(maxSize)
            [dic setObject:maxSize forKey:@"maxSize"];
        else
            NSLog(@"No max size how it is possile");
        
        if(offset)
            [dic setObject:offset forKey:@"offset"];
        else
            NSLog(@"No offset how it is possible");
    } else {
        NSLog(@"No peer JID can't fetch archive for it");
        if(completionHandler)
            completionHandler(nil, [NSError errorWithDomain:@"MAM" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Jid of user not existing"}], @"", @"", YES, 0);
    }
    
    if(beforeDate)
       [dic addEntriesFromDictionary:@{@"beforeDate":beforeDate}];
    else if (lastMessageID)
        [dic addEntriesFromDictionary:@{@"lastMessageID":lastMessageID}];
    NSLog(@"Fetch mam archive with paramaters %@", dic);
    @synchronized(_completionHandlerDictionaryMutex){
        [_loadArchiveRequestCompletionHandler setObject:dic forKey:uniqueID];
    }
    
    [_xmppMessageArchiveManagement fetchArchivedMessagesWithPeer:peer withUUID:uniqueID maxSize:maxSize beforeDate:nil lastMessageID:lastMessageID];
}

-(NSArray<Message *> *) getArchivedMessageWithContactJid:(NSString *) contactJid maxSize:(NSNumber *) maxSize offset:(NSNumber *) offset {
    NSArray<Message*> *archive = [_xmppMessageArchiveManagementStorage fetchMessagesForJID:[XMPPJID jidWithString:contactJid] xmmpStream:_xmppStream maxSize:maxSize offset:offset];
   
    return archive;
}

-(Message *) getLastArchivedMessageWithContactJid:(NSString *) contactJid {
    NSArray<Message*> *archive = [_xmppMessageArchiveManagementStorage fetchLastMessagesForJID:[XMPPJID jidWithString:contactJid] xmmpStream:_xmppStream];
    
    return [archive firstObject];
}

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didBeginFetchingArchiveForUUID:(NSString *)UUID {
    NSLog(@"Did begin fetch archive for UUID %@", UUID);
}

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndFetchingArchiveWithAnswer:(NSDictionary *)answer {
    NSLog(@"Did end fetch archive with answer %@", answer);
    
    NSString *UUID = answer[@"requestID"];
    NSString *firstElementReceived = answer[@"first"];
    NSString *lastElementReceived = answer[@"last"];
    NSNumber *count = [NSNumber numberWithInteger:((NSString*)(answer[@"count"])).integerValue];
    NSNumber *complete = (NSNumber *)answer[@"complete"];
    BOOL isComplete = [complete boolValue];
    
    NSDictionary *entry = nil;
    @synchronized(_completionHandlerDictionaryMutex){
        entry =  [_loadArchiveRequestCompletionHandler objectForKey:UUID];
    }
    NSString *jid = entry[@"jid"];
    NSNumber *maxSize = entry[@"maxSize"];
    NSNumber *offset = entry[@"offset"];
    NSDate *beforeDate = entry[@"beforeDate"];
    
    NSArray<Message *> *returnedArray = [self getArchivedMessageWithContactJid:jid maxSize:maxSize offset:offset];
    
    if(beforeDate){
        // This is the first page requested, so we wil probably found the a new lastMessage
        for (Message *theMessage in returnedArray) {
            if(theMessage.type != MessageTypeGroupChatEvent){
                [_conversationDelegate xmppService:self didRetreiveNewLastMessageFromArchives:theMessage];
            }
        }
    }
    
    // XEP-184 message acknowledgement extension.
    for (Message *message in returnedArray) {
        if (!message.isOutgoing || [message.via isKindOfClass:[Room class]]) {
            if (message.state != MessageDeliveryStateReceived && message.state != MessageDeliveryStateRead) {
                // mark as received immediately.
                [message setDeliveryState:MessageDeliveryStateReceived atTimestamp:[NSDate date]];
                [self sendReceivedByMeACKMessageToJID:[XMPPJID jidWithString:message.via.jid] messageID:message.messageID];
            }
        }
        
        [_conversationDelegate xmppService:self didReceiveArchivedMessage:message];
    }
    
    XmppServiceLoadArchivedMessagesCompletionHandler completionHandler = [entry objectForKey:jid];
    if(completionHandler)
        completionHandler(returnedArray, nil, firstElementReceived, lastElementReceived, isComplete, count);
    else {
        // Should never occur
        NSLog(@"ERROR no completion handler for %@ entry : %@", jid, entry);
    }
    @synchronized(_completionHandlerDictionaryMutex){
        [_loadArchiveRequestCompletionHandler removeObjectForKey:UUID];
    }
}

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndDeleteAllMessagesForUUID:(NSString *) uuid {
    Peer *peer = nil;
    @synchronized(_completionHandlerDictionaryMutex){
        NSDictionary *peerDic = [_deleteRequestCompletionHandler objectForKey:uuid];
         peer = [peerDic objectForKey:@"peer"];
    }
    [_conversationDelegate xmppService:self didRemoveAllMessagesForPeer:peer];
}

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didReceiveDeleteAllMessagesInConversationWithPeerJID:(NSString *)peerJID {
    [_conversationDelegate xmppService:self didReceiveDeleteAllMessagesInConversationWithPeerJID:peerJID];
}

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didReceiveDeleteMessageID:(NSString *)messageID inConversationWithPeerJID:(NSString *)peerJID {
    [_conversationDelegate xmppService:self didReceiveDeleteMessageID:messageID inConversationWithPeerJID:peerJID];
}

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *) sender didRemoveMessage:(XMPPMessage *) message {
    Message *deletedMessage = [self messageFromXmppMessage:message];
    [_conversationDelegate xmppService:self didReceiveDeleteMessageID:deletedMessage.messageID inConversationWithPeerJID:deletedMessage.peer.jid];
}

#pragma mark - MUC delegate

- (void)xmppMUC:(XMPPMUC *)sender roomJID:(XMPPJID *)senderJID didReceiveInvitation:(XMPPMessage *)message {
    // Since we are using Direct-invite, the roomJID (which correspond to 'from') will be the inviter contact and not the room.
    // The message contains : (rainbowID presence is temporary !)
    // <x xmlns="jabber:x:conference" jid="room_jid" thread="room_rainbowID"/>
    NSLog(@"didReceive Room Invitation from contact %@ : %@", senderJID, message);
    
    NSXMLElement *directInvite = [message elementForName:@"x" xmlns:@"jabber:x:conference"];
    NSString *roomJID = [directInvite attributeStringValueForName:@"jid"];
    NSString *roomRainbowID = [directInvite attributeStringValueForName:@"thread"];
    
    // This will create the room and fetch its information from server.
    // server response will say 'you are in "invited" state', then we'll trigger notification.
    [_roomsService createOrUpdateRoomFromJID:roomJID withRainbowID:roomRainbowID];
}

- (void)xmppMUC:(XMPPMUC *)sender roomJID:(XMPPJID *)roomJID didReceiveInvitationDecline:(XMPPMessage *)message {
    NSLog(@"didReceive Room Invitation DECLINE in room %@ : %@", roomJID, message);
}

#pragma mark - Voicemail

/*
 <iq to='tel_39341c3141d04ead8e0e24756e22be46@openrainbow.com/phone' id='6FKPy-1305' type='get’>
     <callservice xmlns='urn:xmpp:pbxagent:callservice:1'>
        <connections/>
     </callservice>
 </iq>
 */
-(void) sendCallServiceConnexionRequestToTarget:(NSString*) jidTel {
    NSXMLElement *callService = [NSXMLElement elementWithName:@"callservice" xmlns:@"urn:xmpp:pbxagent:callservice:1"];
    [callService addChild:[NSXMLElement elementWithName:@"connections"]];
    
    XMPPIQ *iq = [XMPPIQ iqWithType:@"get" to:[XMPPJID jidWithString:[jidTel stringByAppendingString:@"/phone"]] elementID:[XMPPStream generateUUID] child:callService];
    
    XMPPIDTracker *xmppIDTracker = [XMPPIDTracker new];
    [xmppIDTracker addElement:iq target:self selector:@selector(handleCallServiceConnexionRequest:withInfo:) timeout:60];
    
    [_xmppStream sendElement:iq];
}

-(void) handleCallServiceConnexionRequest:(XMPPIQ *)iq withInfo:(XMPPBasicTrackingInfo *)basicTrackingInfo {
    NSLog(@"handleCallServiceConnexionRequest");
}

#pragma mark - Room management

-(XMPPRoom *) getXMPPRoomForJID:(XMPPJID *) jid {
    @synchronized (_xmppRoomsMutex) {
        for (XMPPRoom *xmppRoom in _xmppRooms) {
            if ([xmppRoom.roomJID.bareJID isEqualToJID:jid.bareJID]) {
                return xmppRoom;
            }
        }
    }
    return nil;
}

-(void) joinRoom:(Room *) room {
    @synchronized (_xmppRoomsMutex) {
        XMPPRoom *xmppRoom = [self getXMPPRoomForJID:[XMPPJID jidWithString:room.jid]];
        
        if (!xmppRoom) {
            // Instanciate the XMPP Room module and activate it.
            xmppRoom = [[XMPPRoom alloc] initWithRoomStorage:[XMPPRoomMemoryStorage new] jid:[XMPPJID jidWithString:room.jid] dispatchQueue:xmppDelegateQueue];
            [xmppRoom addDelegate:self delegateQueue:xmppDelegateQueue];
            [xmppRoom activate:_xmppStream];
            [_xmppRooms addObject:xmppRoom];
        }
        
        // join with our *FULL JID* as nickname (convention used in Rainbow)
        NSXMLElement *history = [NSXMLElement elementWithName:@"history"];
        [history addAttributeWithName:@"maxchars" intValue:0];
        [xmppRoom joinRoomUsingNickname:_xmppStream.myJID.full history:history];
    }
}

-(void) leaveRoom:(Room *) room {
    XMPPRoom *xmppRoom = [self getXMPPRoomForJID:[XMPPJID jidWithString:room.jid]];
    [xmppRoom leaveRoom];
}

-(BOOL) isRoomJoined:(Room *) room {
    XMPPRoom *xmppRoom = [self getXMPPRoomForJID:[XMPPJID jidWithString:room.jid]];
    return xmppRoom.isJoined;
}

// The Room Delegate methods

- (void)xmppRoomDidCreate:(XMPPRoom *)sender {
    NSLog(@"Room %@ has been created", [sender roomJID]);
    Room *room = [_roomsService getRoomByJid:[sender roomJID].bare];
    [_roomsDelegate xmppService:self didCreateRoom:room];
}

- (void)xmppRoomDidJoin:(XMPPRoom *)sender {
    NSLog(@"Room %@ has been joined", [sender roomJID]);
    Room *room = [_roomsService getRoomByJid:[sender roomJID].bare];
    [_roomsDelegate xmppService:self didJoinRoom:room];
}

- (void)xmppRoomDidLeave:(XMPPRoom *)sender {
    NSLog(@"Room %@ has been left", [sender roomJID]);
    Room *room = [_roomsService getRoomByJid:[sender roomJID].bare];
    [_roomsDelegate xmppService:self didLeaveRoom:room];
}

- (void)xmppRoomDidDestroy:(XMPPRoom *)sender {
    NSLog(@"Room %@ has been destroyed", [sender roomJID]);
    Room *room = [_roomsService getRoomByJid:[sender roomJID].bare];
    [_roomsDelegate xmppService:self didDestroyRoom:room];
}

- (void)xmppRoom:(XMPPRoom *)sender didFailToDestroy:(XMPPIQ *)iqError {
    NSLog(@"Failed to destroy room %@ with error %@", [sender roomJID], iqError);
    Room *room = [_roomsService getRoomByJid:[sender roomJID].bare];
    [_roomsDelegate xmppService:self didFailedToDestroyRoom:room withError:[NSError errorWithDomain:@"XMPP" code:500 userInfo:@{NSLocalizedDescriptionKey: @"Unable to destroy room"}]];
}

- (void)xmppRoom:(XMPPRoom *)sender occupantDidJoin:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence {
    // occupantJID is the room_xxx@domain/participant_full_jid
    NSLog(@"Occupant %@ JOINED the Room %@", occupantJID, [sender roomJID]);
}

- (void)xmppRoom:(XMPPRoom *)sender occupantDidLeave:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence {
    // occupantJID is the room_xxx@domain/participant_full_jid
    NSLog(@"Occupant %@ LEFT the Room %@", occupantJID, [sender roomJID]);
}

- (void)xmppRoom:(XMPPRoom *)sender occupantDidUpdate:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence {
    // occupantJID is the room_xxx@domain/participant_full_jid
    NSLog(@"Occupant %@ UPDATED in the Room %@", occupantJID, [sender roomJID]);
}

/**
 * Invoked when a message is received.
 * The occupant parameter may be nil if the message came directly from the room, or from a non-occupant.
 **/
- (void)xmppRoom:(XMPPRoom *)sender didReceiveMessage:(XMPPMessage *)message fromOccupant:(XMPPJID *)occupantJID {
    NSLog(@"Message received from Room %@", [sender roomJID]);
}


#pragma mark - Message parsing

-(MessageType) messageTypeForXMPPMessage:(XMPPMessage *) message {
    if ([[message type] isEqualToString:@"chat"] && ![message isChatMessageWithRecordingState]) {
        return MessageTypeChat;
    } else if ([[message type] isEqualToString:@"groupchat"]) {
        if([message isEvent])
            return MessageTypeGroupChatEvent;
        else
            return MessageTypeGroupChat;
    } else if ([[message type] isEqualToString:@"file"]) {
        return MessageTypeFileTransfer;
    } else if ([[message type] isEqualToString:@"webrtc"]) {
        return MessageTypeWebRTC;
    } else if ([[message type] isEqualToString:@"webrtc-start"]) {
        return MessageTypeWebRTCStart;
    } else if ([[message type] isEqualToString:@"webrtc-ringing"]) {
        return MessageTypeWebRTCRinging;
    } else if ([message isConferenceReminder]){
        return MessageTypeGroupChatEvent;
    } else if ([message isChatMessageWithRecordingState]){
        NSString *recordingState = [message elementForName:@"recording"].stringValue;
        if([recordingState isEqualToString:@"start"])
            return MessageTypeCallRecordingStart;
        if([recordingState isEqualToString:@"stop"])
            return MessageTypeCallRecordingStop;
    }

    return MessageTypeUnknown;
}

/**
 *  Return the peer (i.e. real person I am chatting with) for a given JID.
 *  Multiple cases are possible :
 *  - toto@domain.com/rc1 -> Peer returned is the Contact "toto"
 *  - room_1234@domain.com/toto@domain.com/rc1 -> Peer returned is the Contact "toto"
 *  - room_1234@domain.com -> Peer returned is the Room 1234
 *  - bot.domain.com -> Peer is the special Contact "bot"
 *
 *  @param jid The Remote peer JID
 *
 *  @return The Peer
 */
-(Peer *) peerForJID:(XMPPJID *) jid {
    if([jid.user hasPrefix:@"room_"]) {
        NSString *contactJID = [jid resource];
        if (!contactJID || [contactJID length] == 0) {
            // return the Room itself as the Peer.
            Room *room = [_roomsService getRoomByJid:jid.bare];
            if(!room){
                NSLog(@"peerForJID Error: Unable to compute the peer, unknown room %@ -> Fetch room info!", jid.bare);
                room = [_roomsService getOrCreateRainbowRoomSynchronouslyWithJid:jid.bare];

            }
            return room;
        }
        // otherwise, it's a contact.
        NSString *jid = [XMPPJID jidWithString:contactJID].bare;
        Contact *contact = [_contactsManager getContactWithJid:jid];
        if (!contact) {
            contact = [_contactsManager createOrUpdateRainbowContactWithJid:jid];
        }
        return contact;
        
    } else {
        Contact *contact = [_contactsManager getContactWithJid:jid.bare];
        if (!contact) {
            contact = [_contactsManager createOrUpdateRainbowContactWithJid:jid.bare];
        }
        return contact;
    }
    return nil;
}

/**
 *  Return the via Peer (i.e. intermediate "person") for a given JID.
 *  Multiple cases are possible :
 *  - toto@domain.com -> Peer returned is the Contact "toto"
 *  - room_1234@domain.com/toto@domain.com -> Peer returned is the Room 1234
 *  - room_1234@domain.com -> Peer returned is the Room 1234
 *  - bot.domain.com -> Peer is the special Contact "bot"
 *
 *  @param jid The Remote via JID
 *
 *  @return The via peer
 */
-(Peer *) viaForJID:(XMPPJID *) jid {
    if([jid.user hasPrefix:@"room_"]) {
        Room *room = [_roomsService getRoomByJid:jid.bare];
        if(!room){
            NSLog(@"Error: Unable to compute the via peer, unknown room %@ -> Fetch room info!", jid.bare);
            room = [_roomsService getOrCreateRainbowRoomSynchronouslyWithJid:jid.bare];
        }
        return room;
        
    } else {
        Contact *contact = [_contactsManager getContactWithJid:jid.bare];
        if (!contact) {
            contact = [_contactsManager createOrUpdateRainbowContactWithJid:jid.bare];
        }
        return contact;
    }
    return nil;
}

-(Peer *) peerFromJidString:(NSString *) jidString {
    return [self peerForJID:[XMPPJID jidWithString:jidString]];
}

-(Message *) messageFromXmppMessage:(XMPPMessage *) aMessage {
    XMPPMessage *messageToThread = aMessage;
    
    NSXMLElement *result = [aMessage resultStanza];
    if(result)
        messageToThread = [aMessage resultForwardedMessage];
    
    if([aMessage isMessageCarbon])
        messageToThread = [aMessage messageCarbonForwardedMessage];
    
    
    Message *theMessage = nil;
    
    if([messageToThread isConferenceReminder]){
        theMessage = [self conferenceReminderMessageFromXmppMessage:messageToThread];
    } else if([messageToThread isGroupChatMessageWithBodyWithEvent]){
        theMessage = [self eventMessageFromXmppMessage:messageToThread];
    } else if([messageToThread isWebRTCCallWithCallLogInfo] || [messageToThread isUpdatedCallLogMessage]){
        theMessage = [self callLogEventMessageFromXMPPMessage:messageToThread];
    } else if([messageToThread isCallServiceWithNomadicStatus]){
        theMessage = [self nomadicStatusEventMessageFromXMPPMessage:messageToThread];
    } else {
        theMessage = [Message new];
        theMessage.type = [self messageTypeForXMPPMessage:messageToThread];
    }

    // Peer for webrtc call log is define using caller and callee information in callLogEventMessageFromXMPPMessage method
    if(!([messageToThread isWebRTCCallWithCallLogInfo] || [messageToThread isUpdatedCallLogMessage])){
        // Some message from room can have a nil jid (no to for exemple), so we will assume that the to it's me.
        if (messageToThread.to){
            theMessage.isOutgoing = !([messageToThread.to.bareJID isEqual:[XMPPJID jidWithString:_contactsManager.myContact.jid].bareJID]);
            if(theMessage.isOutgoing){
                theMessage.peer = [self peerForJID:messageToThread.to];
                theMessage.via = [self viaForJID:messageToThread.to];
            } else {
                theMessage.peer = [self peerForJID:messageToThread.from];
                theMessage.via = [self viaForJID:messageToThread.from];
            }
            
            // We have a special case here. When we write a message to a room,
            // The room re-send it to us but it's an outgoing message.
            if (theMessage.type == MessageTypeGroupChat && [theMessage.peer isEqual:_contactsManager.myContact]) {
                theMessage.isOutgoing = YES;
            }
        } else {
            // We cant' determine a peer and a via with the informations that we have here.
        }
    }
    
    if([messageToThread elementID]){
        theMessage.messageID = [messageToThread elementID];
    } else if ([messageToThread hasReceiptResponse]){
        theMessage.messageID = [messageToThread receiptResponseID];
    } else {
        theMessage.messageID = [NSString stringWithFormat:@"Error-%@", [_xmppStream generateUUID]];
    }
    
    theMessage.body = messageToThread.body;
    
    if([messageToThread isArchivedMessage]){
        theMessage.timestamp = [messageToThread archivedMessageDate];
    } else {
        theMessage.timestamp = [messageToThread delayedDeliveryDate];
    }
    
    if([messageToThread wasDelayed]){
        if([[[messageToThread delayElement] stringValue] isEqualToString:@"Resent"]){
            theMessage.isResentMessage = YES;
        }
        if([[[messageToThread delayElement] stringValue] isEqualToString:@"Offline Storage"]){
            theMessage.isOfflineMessage = YES;
        }
    }
    
    theMessage.hasBeenPresentedInPush = [messageToThread isRetransmissionMessage];
    theMessage.isComposing = [messageToThread hasComposingChatState];
    theMessage.isCarbonned = [aMessage isMessageCarbon];
    theMessage.isReceivedCarbon = [aMessage isReceivedMessageCarbon];
    theMessage.isSentCarbon = [aMessage isSentMessageCarbon];

    // XEP-0066 look at the XMPP message to check if the archived message has out of band data attached to it
    if([messageToThread hasOutOfBandData] && [messageToThread hasValidOutOfBandData]){
        NSString *rainbowID = [[messageToThread outOfBandURL] lastPathComponent];
        
        NSString *fileName = [messageToThread outOfBandFileName];
        NSNumber *fileSize = [NSNumber numberWithInteger:[messageToThread outOfBandFileSize]];
        NSString *fileMimeType = [messageToThread outOfBandMimeType];
        NSURL *fileUrl = [messageToThread outOfBandURL];
        NSString *ownerId = [messageToThread outOfBandOwnerId];
        BOOL isFileAvailable = [messageToThread outOfBandFileStatus];

        NSMutableDictionary *infos = [NSMutableDictionary new];
        if(fileName)
            [infos setObject:fileName forKey:@"fileName"];
        
        if(fileSize)
            [infos setObject:fileSize forKey:@"size"];
        
        if(fileMimeType)
            [infos setObject:fileMimeType forKey:@"mimeType"];
        
        if(fileUrl)
            [infos setObject:fileUrl forKey:@"url"];
        
        if (!isFileAvailable) {
            [infos setObject:[NSNumber numberWithBool:isFileAvailable] forKey:@"isDownloadAvailable"];
        }

        if(ownerId)
            [infos setObject:ownerId forKey:@"ownerId"];
        
        File *attachment = [_fileSharingService getOrCreateFileWithRainbowID:rainbowID withInfos:infos];
        
        if(!attachment.fileName)
            attachment.fileName = [NSString stringWithFormat:@"file-%@.%@", [Tools generateUniqueID], [File extensionForMimeType:attachment.mimeType]];
        
        theMessage.attachment = attachment;
    }
    
    if([theMessage.body isEqualToString:theMessage.attachment.fileName]){
        // The body is the same as the fileName that means the iOS app add that body when sending it to avoid problem with conversation order and push, we can remove it to avoid display it twice
        theMessage.body = nil;
    }
    
    if ([messageToThread hasReceiptResponse] && [messageToThread isArchivedMessageReceived]) {
        NSDate *timestamp = [messageToThread archivedMessageReceivedDate];
        [theMessage setDeliveryState:MessageDeliveryStateReceived atTimestamp:timestamp];
    }
    if ([messageToThread hasReceiptResponse] && [messageToThread isArchivedMessageRead]) {
        NSDate *timestamp = [messageToThread archivedMessageReadDate];
        [theMessage setDeliveryState:MessageDeliveryStateRead atTimestamp:timestamp];
    }
    
    
    // Search for identity element added in notification service
    NSXMLElement *identity = [messageToThread elementForName:@"identity"];
    if(identity && [theMessage.via isKindOfClass:[Contact class]]){
        theMessage.via.displayName = [identity attributeForName:@"firstLastName"].stringValue;
    }

    return theMessage;
}

-(Message *) eventMessageFromXmppMessage:(XMPPMessage *) aMessage {
    NSArray *events = [aMessage elementsForName:@"event"];
    if([events count] > 0){
        NSXMLElement *event = events[0];
        NSString *eventName = [[event attributeForName:@"name"] stringValue];
        NSString *jid = [[event attributeForName:@"jid"] stringValue];
        
        Message *theMessage = [Message new];
        theMessage.type = MessageTypeGroupChatEvent;
        theMessage.peer = [self peerForJID:aMessage.from];
        theMessage.via = [self viaForJID:aMessage.from];
        if(!theMessage.peer || !theMessage.via){
            return nil;
        }
        theMessage.isOutgoing = NO;
        
        if([aMessage isArchivedMessage]){
            theMessage.timestamp = [aMessage archivedMessageDate];
        } else {
            theMessage.timestamp = [aMessage delayedDeliveryDate];
        }
        
        if(!theMessage.timestamp)
            theMessage.timestamp = [NSDate date];
        
        theMessage.groupChatEventType = [Message messageGroupChatEventTypeFromString: eventName];
        Contact *contact =  [_contactsManager createOrUpdateRainbowContactWithJid:jid];
        theMessage.groupChatEventPeer = contact;
        if([aMessage elementID]){
            theMessage.messageID = [aMessage elementID];
        }
        theMessage.body = [NSString stringWithFormat:@"Group chat event '%@' for user %@", eventName, contact.fullName];
        return theMessage;
    } else {
        return nil;
    }
}

-(NSXMLElement *) channelEventFromXmppMessage:(XMPPMessage *) aMessage {
    NSArray *events = [aMessage elementsForName:@"event"];
    if([events count] > 0){
        return events[0];
    } else {
        return nil;
    }
}

-(NSArray<NSDictionary *> *) channelItemsFromXmppMessage:(NSXMLElement *)event {
    NSMutableArray<NSDictionary *> *items = [[NSMutableArray alloc] init];
    
    NSArray *messageItemsArray = [event elementsForName:@"items"];
    if([messageItemsArray count] > 0){
        NSXMLElement *firstMessageItems = messageItemsArray[0];
        NSString *channelNode = [[firstMessageItems attributeForName:@"node"] stringValue];
        
        NSArray *messageItems = [firstMessageItems elementsForName:@"item"];
        for(NSXMLElement *messageItem in messageItems){
            NSString *itemId = [[messageItem attributeForName:@"id"] stringValue];
            NSArray *entries = [messageItem elementsForName:@"entry"];
            if([entries count] > 0){
                NSXMLElement *firstEntry = entries[0];
                NSArray *payloads = [firstEntry elementsForName :@"payload"];
                NSString *channelId = [[firstEntry attributeForName:@"channelId"] stringValue];
                if([payloads count] > 0){
                    NSXMLElement *payload = payloads[0];
                    NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
                    item[@"node"] = channelNode;
                    item[@"channelId"] = channelId;
                    item[@"id"] = itemId;
                    NSXMLElement *title = [payload elementForName:@"title"];
                    if(title){
                        item[@"title"] = [title stringValue];
                    }
                    NSXMLElement *message = [payload elementForName:@"message"];
                    if(message){
                        item[@"message"] = [message stringValue];
                    }
                    NSXMLElement *url = [payload elementForName:@"url"];
                    if(url){
                        item[@"url"] = [url stringValue];
                    }
                    [items addObject:item];
                }
            }
        }
        
        NSArray *retractItems = [firstMessageItems elementsForName:@"retract"];
        for(NSXMLElement *retractItem in retractItems){
            NSString *itemId = [[retractItem attributeForName:@"id"] stringValue];
            NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
            item[@"node"] = channelNode;
            item[@"retractId"] = itemId;
            [items addObject:item];
        }
    }
    
    return items;
}
-(Message *) callLogEventMessageFromXMPPMessage:(XMPPMessage *) aMessage {
    NSXMLElement *callLog;
    if([aMessage isWebRTCCallWithCallLogInfo])
        callLog = [aMessage elementsForName:@"call_log"][0];
    if([aMessage isUpdatedCallLogMessage])
        callLog = [aMessage updatedCallLogStanza];

    NSString *caller = ((NSXMLElement*)[callLog elementsForName:@"caller"][0]).stringValue;
    XMPPJID *callerJid = [XMPPJID jidWithString:caller];
    XMPPJID *calleeJid = nil;
    if([callLog elementsForName:@"callee"].count > 0){
        NSString *callee = ((NSXMLElement*)[callLog elementsForName:@"callee"][0]).stringValue;
        calleeJid = [XMPPJID jidWithString:callee];
    }
    NSString *state = ((NSXMLElement*)[callLog elementsForName:@"state"][0]).stringValue;
    NSInteger duration = 0;
    CallLogState aState = [CallLog callLogStateFromEventTypeString:state];
    if(aState == CallLogStateAnswered)
        duration = ((NSXMLElement*)[callLog elementsForName:@"duration"][0]).stringValueAsInt;
    
    NSString *stampDate = [[callLog elementForName:@"date"] stringValue];
    NSDate *dateToUse = [NSDate dateWithXmppDateTimeString:stampDate];
    
    BOOL isOutgoing = ([callerJid isEqualToJID:_xmppStream.myJID options:XMPPJIDCompareBare]);
    
    Message *theMessage = [Message new];
    theMessage.type = MessageTypeWebRTC;
    theMessage.peer = isOutgoing?[self peerForJID:calleeJid]:[self peerForJID:callerJid];
    theMessage.via = isOutgoing?[self viaForJID:calleeJid]:[self viaForJID:callerJid];
    theMessage.isOutgoing = isOutgoing;
    
    theMessage.callLog = [CallLog new];
    theMessage.callLog.state = aState;
    theMessage.callLog.type = CallLogTypeWebRTC;
    theMessage.callLog.realDuration = [NSNumber numberWithInteger:duration];
    theMessage.callLog.date = dateToUse;
    theMessage.callLog.isOutgoing = isOutgoing;
    
    theMessage.timestamp = dateToUse;
    
    if([aMessage elementID]){
        theMessage.messageID = [aMessage elementID];
    }
    return theMessage;
}

-(Message *) nomadicStatusEventMessageFromXMPPMessage:(XMPPMessage *) aMessage {
    NSXMLElement *callServiceElement = [[aMessage elementsForName:@"callservice"] firstObject];

    Message *theMessage = [Message new];
    theMessage.type = MessageTypeCallServiceNomadicStatus;
    
    if(callServiceElement) {
        NSXMLElement *nomadicStatusElement = [[callServiceElement elementsForName:@"nomadicStatus"] firstObject];
        NSString *destination = [nomadicStatusElement attributeForName:@"destination"].stringValue;
        NSString *modeActivated = [nomadicStatusElement attributeForName:@"modeActivated"].stringValue;
        NSString *featureActivated = [nomadicStatusElement attributeForName:@"featureActivated"].stringValue;
        
        PhoneNumber *phone = [PhoneNumber new];
        phone.numberE164 = destination;
        
        // New status from message values
        NomadicStatus *status = [NomadicStatus new];
        status.destination = phone;
        status.activated = ([modeActivated isEqualToString:@"true"]) ? YES : NO;
        status.featureActivated = ([featureActivated isEqualToString:@"true"]) ? YES : NO;
        
        theMessage.nomadicStatus = status;
    }
    
    if([aMessage elementID]){
        theMessage.messageID = [aMessage elementID];
    }
    
    return theMessage;
}

-(NSString *) stringValueFromXMLElement:(NSXMLElement *)elem name:(NSString *)name {
    NSArray *subElem = [elem elementsForName: name];
    if(subElem){
        return ((NSXMLElement*)[subElem firstObject]).stringValue;
    } else {
        return nil;
    }
}

-(ConferenceParticipant *) conferenceParticipantFromXMLElement:(NSXMLElement *)elem {
    NSString *participantId = [self stringValueFromXMLElement:elem name: @"participant-id"];
    NSString *jidIM = [self stringValueFromXMLElement:elem name: @"jid-im"];
    NSString *phoneNumber = [self stringValueFromXMLElement:elem name: @"phone-number"];
    NSString *roleStr = [self stringValueFromXMLElement:elem name: @"role"];
    ParticipantRole role = [ConferenceParticipant participantRoleFromNSString:roleStr];
    NSString *stateStr = [self stringValueFromXMLElement:elem name: @"cnx-state"];
    ParticipantState state = [ConferenceParticipant participantStateFromNSString:stateStr];
    NSString *mutedStr = [self stringValueFromXMLElement:elem name: @"mute"];
    BOOL muted = [mutedStr isEqualToString:@"off"] ? NO : YES;
    NSString *holdStr = [self stringValueFromXMLElement:elem name: @"hold"];
    BOOL hold = [holdStr isEqualToString:@"off"] ? NO : YES;
    
    ConferenceParticipant *participant = [ConferenceParticipant new];
    participant.participantId = participantId;
    participant.jidIM = jidIM;
    participant.phoneNumber = phoneNumber;
    participant.role = role;
    participant.state = state;
    participant.muted = muted;
    participant.hold = hold;
    if(participant.phoneNumber && !participant.jidIM){
        participant.contact = [_contactsManager createOrUpdateRainbowContactWithPhoneNumber:participant.phoneNumber];
    } else {
        if(participant.jidIM)
            participant.contact = [_contactsManager createOrUpdateRainbowContactWithJid:participant.jidIM];
        else
            participant.contact = [_contactsManager createOrUpdateRainbowContactWithRainbowId:participantId];
    }
    return participant;
}


/**
 
 // Participant added
 <message xmlns='jabber:client' from='janusgateway.jerome-all-in-one-dev-1.opentouch.cloud' to='fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud/conf' type='chat'><conference-info xmlns='jabber:iq:conference'><conference-id>59b7e2cad556f5cdf0bec468</conference-id><media-type xmlns='urn:xmpp:janus:1'>webrtc</media-type><added-participants><participant><participant-id>59c529763077d86a6ccf0136</participant-id><jid-im>c2673e1952ef40178b68cb4ae2c6c2ed@jerome-all-in-one-dev-1.opentouch.cloud</jid-im><role>moderator</role><mute>off</mute><hold>off</hold><cnx-state>connected</cnx-state></participant></added-participants></conference-info></message>
 
 // Participant updated
 <message xmlns='jabber:client' from='janusgateway.jerome-all-in-one-dev-1.opentouch.cloud' to='fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud/conf' type='chat'><conference-info xmlns='jabber:iq:conference'><conference-id>59b7e2cad556f5cdf0bec468</conference-id><media-type xmlns='urn:xmpp:janus:1'>webrtc</media-type><updated-participants><participant><participant-id>59c52a043077d86a6ccf0137</participant-id><jid-im>c2673e1952ef40178b68cb4ae2c6c2ed@jerome-all-in-one-dev-1.opentouch.cloud</jid-im><role>moderator</role><mute>off</mute><cnx-state>connected</cnx-state></participant></updated-participants></conference-info></message>
 
 // Removed participant
 <message xmlns='jabber:client' from='janusgateway.jerome-all-in-one-dev-1.opentouch.cloud' to='fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud/conf' type='chat'><conference-info xmlns='jabber:iq:conference'><conference-id>59b7e2cad556f5cdf0bec468</conference-id><media-type xmlns='urn:xmpp:janus:1'>webrtc</media-type><removed-participants><participant-id>59c529763077d86a6ccf0136</participant-id></removed-participants></conference-info></message>
 
 // Talker
 <message xmlns='jabber:client' from='janusgateway.jerome-all-in-one-dev-1.opentouch.cloud' to='fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud/conf' type='chat'><conference-info xmlns='jabber:iq:conference'><conference-id>59b7e2cad556f5cdf0bec468</conference-id><media-type xmlns='urn:xmpp:janus:1'>webrtc</media-type><talkers from='janusgateway.jerome-all-in-one-dev-1.opentouch.cloud'><talker><participant-id>59c529763077d86a6ccf0136</participant-id></talker></talkers></conference-info></message>
 
 // Added publishers
 <message xmlns="jabber:client" from="janusgateway.jerome-all-in-one-dev-1.opentouch.cloud" to="c2673e1952ef40178b68cb4ae2c6c2ed@jerome-all-in-one-dev-1.opentouch.cloud/conf" type="chat"><conference-info xmlns="jabber:iq:conference"><conference-id>5a1ec1fdc6e700a32b26e624</conference-id><media-type xmlns="urn:xmpp:janus:1">webrtc</media-type><added-publishers><publisher><publisher-id>5a70485c22c67ed15086300b</publisher-id><media-type>sharing</media-type><jid-im>596e5cdb16e94756b2a2dc03fdb70c0b@jerome-all-in-one-dev-1.opentouch.cloud</jid-im><role>member</role></publisher></added-publishers></conference-info></message>
 
 // Removed publishers
 <message xmlns="jabber:client" from="janusgateway.jerome-all-in-one-dev-1.opentouch.cloud" to="c2673e1952ef40178b68cb4ae2c6c2ed@jerome-all-in-one-dev-1.opentouch.cloud/conf" type="chat"><conference-info xmlns="jabber:iq:conference"><conference-id>5a1ec1fdc6e700a32b26e624</conference-id><media-type xmlns="urn:xmpp:janus:1">webrtc</media-type><removed-publishers><publisher><participant-id>5a70485c22c67ed15086300b</participant-id><media-type>video</media-type></publisher></removed-publishers></conference-info></message>
 
 // Publisher at startup
 <message xmlns="jabber:client" to="0588d92fdf99415484fd7552815c6204@jerome-all-in-one-dev-1.opentouch.cloud/mobile_ios_FE2A6D3D-9F05-49B5-8200-3C1288053E1A" from="janusgateway.jerome-all-in-one-dev-1.opentouch.cloud" type="chat" id="janusgateway.jerome-all-in-one-dev-1.opentouch.cloud_1262"><conference-info xmlns="jabber:iq:conference"><conference-id>5a1ec0adc6e700a32b26e622</conference-id><media-type xmlns="urn:xmpp:janus:1">webrtc</media-type><conference-state><active>true</active><publisher-count>1</publisher-count></conference-state><publishers><publisher><publisher-id>5a9686743ab3c44aa4938c6d</publisher-id><media-type>video</media-type><jid-im>fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud</jid-im><role>moderator</role><cnx-state>connected</cnx-state></publisher></publishers></conference-info></message>
 
 */
-(ConferenceInfo *) conferenceInfoMessageFromXmppMessage:(XMPPMessage *) aMessage {
    NSArray *conferenceInfo = [aMessage elementsForName:@"conference-info"];
    if([conferenceInfo count] > 0){
        NSXMLElement *event = conferenceInfo[0];
        
        ConferenceInfo *confInfo = [[ConferenceInfo alloc] init];
        confInfo.eventType = ConferenceEventTypeUnknown;
        
        NSString *confId = [self stringValueFromXMLElement:event name:@"conference-id"];
        confInfo.confId = confId;
        
        // parse conference state
        NSArray *xmlStateArray = [event elementsForName:@"conference-state"];
        if([xmlStateArray count] > 0){
            confInfo.eventType = ConferenceEventTypeState;
            for(NSXMLElement *xmlState in ((NSXMLElement *)xmlStateArray[0]).children){
                
                if([[xmlState name] isEqualToString:@"active"]){
                    confInfo.isActive = [xmlState stringValueAsBool];
                } else if([[xmlState name] isEqualToString:@"talker-active"]){
                    confInfo.isTalkerActive = [xmlState stringValueAsBool];
                } else if([[xmlState name] isEqualToString:@"recording-started"]){
                    confInfo.isRecording = [xmlState stringValueAsBool];
                }
            }
        }
        
        NSArray *xmlParticipantsArray = [event elementsForName:@"participants"];
        NSArray *xmlAddedParticipantsArray = [event elementsForName:@"added-participants"];
        NSArray *xmlUpdatedParticipantsArray = [event elementsForName:@"updated-participants"];
        NSArray *xmlRemovedParticipantsArray = [event elementsForName:@"removed-participants"];
        NSArray *xmlTalkersArray = [event elementsForName:@"talkers"];
        NSArray *xmlAddedPublishersArray = [event elementsForName:@"added-publishers"];
        NSArray *xmlRemovedPublishersArray = [event elementsForName:@"removed-publishers"];
        NSArray *xmlPublishersArray = [event elementsForName:@"publishers"];
        
        if([xmlParticipantsArray count] > 0){
            // parse the participant array
            confInfo.eventType = ConferenceEventTypeState;
            NSArray *xmlParticipants = [xmlParticipantsArray[0] elementsForName:@"participant"];
            for(NSXMLElement *xmlParticipant in xmlParticipants){
                ConferenceParticipant *participant = [self conferenceParticipantFromXMLElement:xmlParticipant];
                [((NSMutableArray *)confInfo.addedParticipants) addObject:participant];
            }
        } else if ([xmlAddedParticipantsArray count] > 0){
            confInfo.eventType = ConferenceEventTypeParticipantsAdded;
            NSArray *xmlParticipants = [xmlAddedParticipantsArray[0] elementsForName:@"participant"];
            for(NSXMLElement *xmlParticipant in xmlParticipants){
                ConferenceParticipant *participant = [self conferenceParticipantFromXMLElement:xmlParticipant];
                [((NSMutableArray *)confInfo.addedParticipants) addObject:participant];
            }
        } else if([xmlUpdatedParticipantsArray count] > 0){
            // parse the updated participant array
            confInfo.eventType = ConferenceEventTypeParticipantsUpdate;
            NSArray *xmlParticipants = [xmlUpdatedParticipantsArray[0] elementsForName:@"participant"];
            for(NSXMLElement *xmlParticipant in xmlParticipants){
                ConferenceParticipant *participant = [self conferenceParticipantFromXMLElement:xmlParticipant];
                [((NSMutableArray *)confInfo.updatedParticipants) addObject:participant];
            }
            
        } else if([xmlRemovedParticipantsArray count] > 0){
            // parse the removed participant array
            confInfo.eventType = ConferenceEventTypeParticipantsRemoved;
            NSArray *xmlParticipants = [xmlRemovedParticipantsArray[0] elementsForName:@"participant-id"];
            for(NSXMLElement *xmlParticipant in xmlParticipants){
                NSString *participantId = [xmlParticipant stringValue];
                [((NSMutableArray *)confInfo.removedParticipants) addObject:participantId];
            }

        } else if([xmlTalkersArray count] > 0){
            // parse the talkers array
            confInfo.eventType = ConferenceEventTypeTalkers;
            NSArray *xmlParticipants = [xmlTalkersArray[0] elementsForName:@"talker"];
            for(NSXMLElement *xmlParticipant in xmlParticipants){
                NSString *participantId = [xmlParticipant stringValue];
                [((NSMutableArray *)confInfo.talkers) addObject:participantId];
            }
            NSArray *xmlParticipantsPGI = [xmlTalkersArray[0] elementsForName:@"participant-id"];
            for(NSXMLElement *xmlParticipant in xmlParticipantsPGI){
                NSString *participantId = [xmlParticipant stringValue];
                [((NSMutableArray *)confInfo.talkers) addObject:participantId];
            }
        } else if ([xmlAddedPublishersArray count] > 0){
            confInfo.eventType = ConferenceEventPublishersAdded;
            NSArray *xmlPublishers = [xmlAddedPublishersArray[0] elementsForName:@"publisher"];
            for (NSXMLElement *xmlPublisher in xmlPublishers) {
                ConferencePublisher *publisher = [ConferencePublisher publisherFromXMLElement:xmlPublisher];
                publisher.contact = [_contactsManager createOrUpdateRainbowContactWithJid:publisher.jidIM];
                [((NSMutableArray *)confInfo.addedPublishers) addObject:publisher];
            }
        } else if([xmlRemovedPublishersArray count] > 0){
            confInfo.eventType = ConferenceEventPublishersRemoved;
            NSArray *xmlPublishers = [xmlRemovedPublishersArray[0] elementsForName:@"publisher"];
            for(NSXMLElement *xmlPublisher in xmlPublishers){
                ConferencePublisher *publisher = [ConferencePublisher publisherFromXMLElement:xmlPublisher];
                if(publisher.jidIM)
                    publisher.contact = [_contactsManager createOrUpdateRainbowContactWithJid:publisher.jidIM];
                [((NSMutableArray *)confInfo.removedPublishers) addObject:publisher];
            }
        } else if ([xmlPublishersArray count] > 0){
            confInfo.eventType = ConferenceEventPublishersAdded;
            NSArray *xmlPublishers = [xmlPublishersArray[0] elementsForName:@"publisher"];
            for (NSXMLElement *xmlPublisher in xmlPublishers) {
                ConferencePublisher *publisher = [ConferencePublisher publisherFromXMLElement:xmlPublisher];
                publisher.contact = [_contactsManager createOrUpdateRainbowContactWithJid:publisher.jidIM];
                [((NSMutableArray *)confInfo.addedPublishers) addObject:publisher];
            }
        }
        return confInfo;
    } else {
        return nil;
    }
}


-(Message *)conferenceReminderMessageFromXmppMessage:(XMPPMessage *)aMessage {
    NSXMLElement *xElem = [aMessage elementForName:@"x" xmlns:kXMPPJabberXAudioConferenceNS];
    if(xElem){
        NSString *type = [xElem attributeForName:@"type"].stringValue;
        if([type isEqualToString:@"reminder"]){
            NSString *jid = [xElem attributeForName:@"jid"].stringValue;
            Room *room = [_roomsService getRoomByJid:jid];
            if(room){
                XMPPJID *xmppJid = [XMPPJID jidWithString:jid];
                NSDate *dateToUse = [NSDate date];
                
                Message *theMessage = [Message new];
                theMessage.type = MessageTypeGroupChatEvent;
                theMessage.groupChatEventType = MessageGroupChatEventConferenceReminder;
                theMessage.groupChatEventPeer = room;
                theMessage.peer = [self peerForJID:xmppJid];
                theMessage.via = [self viaForJID:xmppJid];
                theMessage.isOutgoing = NO;
                theMessage.timestamp = dateToUse;
                
                if([aMessage elementID]){
                    theMessage.messageID = [aMessage elementID];
                }
                
                Contact *contact = [_contactsManager createOrUpdateRainbowContactWithJid:theMessage.peer.jid];
                NSString *subject = [xElem attributeForName:@"subject"].stringValue;
                
                theMessage.body = [NSString stringWithFormat:@"Reminder for conference about '%@' organized by '%@'", subject , contact.fullName];
                return theMessage;
            }
        }
    }
    return nil;
}

-(Message *) createAndDispatchMessageFromDictionary:(NSDictionary *) dictionary {
    NSString *from = dictionary[@"from"];
    NSString *messageID = dictionary[@"messageID"];
    NSString *messageBody = dictionary[@"messageBody"];
    NSString *messageType = dictionary[@"messageType"];
    NSString *stamp = dictionary[@"stamp"];
    NSString *firstLastName = dictionary[@"first-last-name"];

    XMPPMessage *xmppMessage = [XMPPMessage messageWithType:messageType elementID:messageID];
    [xmppMessage addAttributeWithName:@"from" stringValue:from];
    [xmppMessage addAttributeWithName:@"to" stringValue:_contactsManager.myContact.jid];
    [xmppMessage addBody:messageBody];
    [xmppMessage addReceiptRequest];
    // Add a fake delay with Resent to avoid resending a local notification for this fake message
    NSXMLElement *delay = [NSXMLElement elementWithName:@"delay" xmlns:@"urn:xmpp:delay"];
    [delay setStringValue:@"Resent"];
    if(stamp.length > 0)
        [delay addAttributeWithName:@"stamp" stringValue:stamp];
    [xmppMessage addChild:delay];
    
    NSXMLElement *identity = [NSXMLElement elementWithName:@"identity"];
    [identity addAttributeWithName:@"firstLastName" stringValue:firstLastName];
    [xmppMessage addChild:identity];

//    NSXMLElement *retransmission = [NSXMLElement elementWithName:@"retransmission" xmlns:@"jabber:iq:notification"];
//    [retransmission addAttributeWithName:@"source" stringValue:@"push"];
//    [xmppMessage addChild:retransmission];
    
    Message *theMessage = [self messageFromXmppMessage:xmppMessage];
    
    //[_xmppStream simulateDidReceiveMessage:xmppMessage];
     [_xmppMessageArchiveManagement saveMessageInCache:xmppMessage xmppStream:_xmppStream];
    
    return theMessage;
}

-(void) deleteAllMessagesFromConversation:(Conversation *)conversation {
    
    [self deleteAllMessagesFromPeer:conversation.peer];
}

-(void) deleteAllMessagesFromPeer:(Peer *)peer {
   
    NSDictionary *dic = @{@"peer": peer};
    NSLog(@"Delete all message for %@", dic);
    NSString *uniqueID = [NSString stringWithFormat:@"%@%@",kRemoveCallLogsKeyPrefex, peer.jid];
    @synchronized(_completionHandlerDictionaryMutex){
        [_deleteRequestCompletionHandler setObject:dic forKey:uniqueID];
    }
    [_xmppMessageArchiveManagement deleteAllMessagesWithPeer:peer withUUID:uniqueID];
}


#pragma mark - Send JingleMessageInitiation Messages

-(void) sendJingleInitiationProposeTo:(NSString *) jid sessionID:(NSString *) sessionID withMedias:(NSArray<NSString *> *) medias {
    XMPPJID *target = [[XMPPJID jidWithString:jid] bareJID];
    [_xmppJingleMessageInitiation sendProposeMsg:sessionID medias:medias to:target];
}

-(void) sendJingleInitiationProposeTo:(NSString *) jid sessionID:(NSString *) sessionID withMedias:(NSArray<NSString *> *) medias andSubject: (NSString *) subject {
    XMPPJID *target = [[XMPPJID jidWithString:jid] bareJID];
    [_xmppJingleMessageInitiation sendProposeMsg:sessionID subject:subject medias:medias to:target];
}

-(void) sendJingleInitiationRetractTo:(NSString *) jid sessionID:(NSString *) sessionID {
    XMPPJID *target = [[XMPPJID jidWithString:jid] bareJID];
    [_xmppJingleMessageInitiation sendRetractMsg:sessionID to:target];
}

-(void) sendJingleInitiationAcceptsessionID:(NSString *) sessionID {
    [_xmppJingleMessageInitiation sendAcceptMsg:sessionID];
}

-(void) sendJingleInitiationProceedTo:(NSString *) jid sessionID:(NSString *) sessionID {
    XMPPJID *target = [XMPPJID jidWithString:jid];
    
    if (![target isFull]) {
        NSLog(@"Error: cannot send Jingle Initiation proceed message to non-full JID %@", target);
        return;
    }
    
    [_xmppJingleMessageInitiation sendProceedMsg:sessionID to:target];
}

-(void) sendJingleInitiationRejectsessionID:(NSString *) sessionID {
    [_xmppJingleMessageInitiation sendRejectMsg:sessionID to:[[_xmppStream myJID] bareJID]];
}

-(void) sendJingleInitiationRejectTo:(NSString *) jid sessionID:(NSString *) sessionID {
    XMPPJID *target = [XMPPJID jidWithString:jid];
    
    if (![target isFull]) {
        NSLog(@"Error: cannot send Jingle Initiation reject message to non-full JID %@", target);
        return;
    }
    
    [_xmppJingleMessageInitiation sendRejectMsg:sessionID to:target];
}


#pragma mark - Send Jingle IQs
-(void) sendJingleTerminatetoTarget:(NSString *)jid withReason:(NSString*)reason sessionID:(NSString *)sessionID peer:(Peer *) peer {
    XMPPJID *target = [XMPPJID jidWithString:jid];
    
    if (![target isFull] && ![target.bare containsString:@"janus"]) {
        NSLog(@"Error: cannot send Jingle Terminate message to non-full JID %@", target);
        return;
    }
    
    [_xmppJingle sendSessionTerminateReason:reason to:target sessionID:sessionID peer:peer];
}

-(void) sendJingleTransportInfoWithCandidate:(RTCIceCandidate *)candidate toTarget:(NSString *)jid sessionID:(NSString *)sessionID peer:(Peer *)peer {
    XMPPJID *target = [XMPPJID jidWithString:jid];

    NSLog(@"Send transport info");
    NSString *acandidate = [NSString stringWithFormat:@"a=%@", candidate.sdp];
    NSDictionary *data =@{@"candidate" : acandidate, @"id": candidate.sdpMid};
    [_xmppJingle sendTransportInfoCandidate:data to:target sessionID:sessionID peer:peer];
}

-(void) sendJingleSDP:(NSString *) sdp type:(XMPPJingleMessageType) type toTarget:(NSString *) jid sessionID:(NSString *) sessionID peer:(Peer *)peer {
    XMPPJID *target = [XMPPJID jidWithString:jid];
    
    switch (type) {
        case XMPPJingleMessageTypeSessionInitiate:
            [_xmppJingle sendSessionInitiateSDP:sdp to:target sessionID:sessionID peer:peer];
            break;
        case XMPPJingleMessageTypeSessionAccept:
            [_xmppJingle sendSessionAcceptSDP:sdp to:target sessionID:sessionID peer:peer];
            break;
        case XMPPJingleMessageTypeContentAdd:
            [_xmppJingle sendContentAddWithSDP:sdp to:target sessionID:sessionID peer:peer];
            break;
        case XMPPJingleMessageTypeContentModify:
            [_xmppJingle sendContentModifyWithSDP:sdp to:target sessionID:sessionID peer:peer];
            break;
        case XMPPJingleMessageTypeContentRemove:
            [_xmppJingle sendContentRemoveWithSDP:sdp to:target sessionID:sessionID peer:peer];
            break;
        case XMPPJingleMessageTypeContentAccept:
            [_xmppJingle sendContentAcceptWithSDP:sdp to:target sessionID:sessionID peer:peer];
            break;
        case XMPPJingleMessageTypeTransportAccept:
            [_xmppJingle sendTransportAcceptWithSDP:sdp to:target sessionID:sessionID peer:peer];
            break;
        case XMPPJingleMessageTypeTransportReplace:
            [_xmppJingle sendTransportReplaceWithSDP:sdp to:target sessionID:sessionID peer:peer];
            break;
        default:
            break;
    }
    
}

-(void) sendMediaPillarJingleSDP:(NSString *) sdp toTarget:(NSString *) jid sessionID:(NSString *) sessionID phoneNumber:(NSString *) number {
    XMPPJID *target = [XMPPJID jidWithString:jid];
    [_xmppJingle sendMediaPillarSessionInitiateSDP:sdp to:target sessionID:sessionID phoneNumber:number];
}

-(void) sendJingleHoldFromJid:(NSString*) jid toTarget:(NSString *) toJid sessionID:(NSString *) sessionID {
    XMPPIQ *iq = [XMPPIQ iqWithHoldJingleFrom:jid to:toJid elementID:[XMPPStream generateUUID] sessionID:sessionID];
    [_xmppStream sendElement:iq];
}

-(void) sendJingleUnholdFromJid:(NSString*) jid toTarget:(NSString *) toJid sessionID:(NSString *) sessionID {
    XMPPIQ *iq = [XMPPIQ iqWithUnholdJingleFrom:jid to:toJid elementID:[XMPPStream generateUUID] sessionID:sessionID];
    [_xmppStream sendElement:iq];
}


#pragma mark - XMPPJingleMessageInitiationDelegate

- (void)xmppJingleMessageInitiation:(XMPPJingleMessageInitiation *) xmppJMI didReceiveProposeMsg:(NSString *) sessionID
                             medias:(NSArray<NSString *> *) medias from:(XMPPJID *) from {

    BOOL isMediaPillarCall = NO;
    NSString* fromJid = from.bare;
    if (_contactsManager.myUser.mediaPillarStatus.jid)
        isMediaPillarCall = [fromJid containsString:_contactsManager.myUser.mediaPillarStatus.jid];
    NSLog (@"xmppJingleMessageInitiation didReceiveProposeMsg from '%@'%@ %@", sessionID, (isMediaPillarCall ? @" (media-pillar)" : @""), medias);
    
    Peer* peer;
    if (isMediaPillarCall)
    {
        peer = [Peer new];
        peer.jid = fromJid;
        [_telephonyDelegate xmppService:self didReceiveMpCall:sessionID withPeer:peer andResource:(NSString *) from.resource];
    }
    else
    {
        peer = [_contactsManager createOrUpdateRainbowContactWithJid:fromJid];
        if (!peer)
            peer = [_roomsService getRoomByRTCJid:from.bare];
        [_rtcService didReceiveProposeMsg:sessionID withMedias:medias from:peer resource:from.resource];
    }
}


- (void)xmppJingleMessageInitiation:(XMPPJingleMessageInitiation *) xmppJMI didReceiveRetractMsg:(NSString *) sessionID from:(XMPPJID *) from {
    NSLog(@"xmppJingleMessageInitiation didReceiveRetractMsg %@", sessionID);
    
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
        peer = [_roomsService getRoomByRTCJid:from.bare];
    [_rtcService didReceiveRetractMsg:sessionID from:peer resource:from.resource];
}

- (void)xmppJingleMessageInitiation:(XMPPJingleMessageInitiation *) xmppJMI didReceiveAcceptMsg:(NSString *) sessionID {
    NSLog(@"xmppJingleMessageInitiation didReceiveAcceptMsg %@", sessionID);
    [_rtcService didReceiveAcceptMsg:sessionID];
}

- (void)xmppJingleMessageInitiation:(XMPPJingleMessageInitiation *) xmppJMI didReceiveProceedMsg:(NSString *) sessionID from:(XMPPJID *) from {
    NSLog(@"xmppJingleMessageInitiation didReceiveProceedMsg %@", sessionID);
    
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
        peer = [_roomsService getRoomByRTCJid:from.bare];
    [_rtcService didReceiveProceedMsg:sessionID from:peer resource:from.resource];
}

- (void)xmppJingleMessageInitiation:(XMPPJingleMessageInitiation *) xmppJMI didReceiveRejectMsg:(NSString *) sessionID from:(XMPPJID *) from {
    NSLog(@"xmppJingleMessageInitiation didReceiveRejectMsg %@", sessionID);
    if([from.full isEqualToString:_xmppStream.myJID.full]){
        NSLog(@"This message is from our own ressource, don't treat it");
        return;
    }
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
        peer = [_roomsService getRoomByRTCJid:from.bare];
    [_rtcService didReceiveRejectMsg:sessionID from:peer resource:from.resource];
}


#pragma mark - XMPPJingleDelegate

-(PeerMediaType) peerMediaTypeFromMediaType:(NSString *) mediaType {
    if([mediaType isEqualToString:@"audio"]){
        return PeerMediaTypeAudio;
    }
    if([mediaType isEqualToString:@"video"]){
        return PeerMediaTypeVideo;
    }
    if([mediaType isEqualToString:@"sharing"]){
        return PeerMediaTypeSharing;
    }
    if([mediaType isEqualToString:@"sharing+video"]){
        return PeerMediaTypeVideoSharing;
    }
    return  PeerMediaTypeAudio;
}

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveSessionInitiate:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from mediaType:(NSString *) mediaType publisherID:(NSString *)publisherID {
    NSLog(@"xmppJingle didReceiveSessionInitiate %@", sessionID);
    Peer *peer = nil;
    if([from.bare containsString:@"janus"]){
        // We must create a unique peer
        peer = [_contactsManager createPeerWithJid:from.bare];
    } else  {
        peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
        if(!peer)
            peer = [_roomsService getRoomByRTCJid:from.bare];
    }
    peer.mediaType = [self peerMediaTypeFromMediaType:mediaType];
    if(publisherID)
        peer.publisherID = publisherID;
    [_rtcService didReceiveSessionInitiate:sessionID sdp:sdp from:peer resource:from.resource];
}

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveSessionAccept:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from  mediaType:(NSString *) mediaType publisherID:(NSString *)publisherID {
    NSLog(@"xmppJingle didReceiveSessionAccept %@", sessionID);
    
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
        peer = [_roomsService getRoomByRTCJid:from.bare];
    peer.mediaType = [self peerMediaTypeFromMediaType:mediaType];
    if(publisherID)
        peer.publisherID = publisherID;
    [_rtcService didReceiveSessionAccept:sessionID sdp:sdp from:peer resource:from.resource];
}

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveSessionTerminate:(NSString *) sessionID from:(XMPPJID *)from  mediaType:(NSString *) mediaType publisherID:(NSString *)publisherID {
    NSLog(@"xmppJingle didReceiveSessionTerminate %@", sessionID);
    
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
        peer = [_roomsService getRoomByRTCJid:from.bare];
    peer.mediaType = [self peerMediaTypeFromMediaType:mediaType];
    if(publisherID)
        peer.publisherID = publisherID;
    [_rtcService didReceiveSessionTerminate:sessionID from:peer];
}

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveTransportInfo:(NSString *) sessionID candidate:(NSDictionary *)candidate from:(XMPPJID *)from  mediaType:(NSString *) mediaType publisherID:(NSString *)publisherID {
    NSLog(@"xmppJingle didReceiveTransportInfo %@", sessionID);
    
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
        peer = [_roomsService getRoomByRTCJid:from.bare];
    peer.mediaType = [self peerMediaTypeFromMediaType:mediaType];
    if(publisherID)
        peer.publisherID = publisherID;
    [_rtcService didReceiveTransportInfo:sessionID candidate:candidate from:peer];
}

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveTransportReplace:(NSString *) sessionID sdp:(NSString *) sdp from:(XMPPJID *)from  mediaType:(NSString *) mediaType publisherID:(NSString *)publisherID {
    NSLog(@"xmppJingle didReceiveTransportReplace %@", sessionID);
    
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
        peer = [_roomsService getRoomByRTCJid:from.bare];
    peer.mediaType = [self peerMediaTypeFromMediaType:mediaType];
    if(publisherID)
        peer.publisherID = publisherID;
    [_rtcService didReceiveTransportReplace:sessionID sdp:sdp from:peer resource:from.resource];
}

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveTransportAccept:(NSString *) sessionID sdp:(NSString *) sdp from:(XMPPJID *)from  mediaType:(NSString *) mediaType publisherID:(NSString *)publisherID {
    NSLog(@"xmppJingle didReceiveTransportAccept %@", sessionID);
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
        peer = [_roomsService getRoomByRTCJid:from.bare];
    peer.mediaType = [self peerMediaTypeFromMediaType:mediaType];
    if(publisherID)
        peer.publisherID = publisherID;
    [_rtcService didReceiveTransportAccept:sessionID sdp:sdp from:peer];
}

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveContentAdd:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from  mediaType:(NSString *) mediaType publisherID:(NSString *)publisherID {
    NSLog(@"xmppJingle didReceiveContentAdd %@", sessionID);
    
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
        peer = [_roomsService getRoomByRTCJid:from.bare];
    peer.mediaType = [self peerMediaTypeFromMediaType:mediaType];
    if(publisherID)
        peer.publisherID = publisherID;
    [_rtcService didReceiveContentAdd:sessionID sdp:sdp from:peer resource:from.resource];
}

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveContentModify:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from  mediaType:(NSString *) mediaType publisherID:(NSString *)publisherID {
    NSLog(@"xmppJingle didReceiveContentModify %@", sessionID);
    
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
        peer = [_roomsService getRoomByRTCJid:from.bare];
    peer.mediaType = [self peerMediaTypeFromMediaType:mediaType];
    if(publisherID)
        peer.publisherID = publisherID;
    [_rtcService didReceiveContentModify:sessionID sdp:sdp from:peer resource:from.resource];
}

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveContentRemove:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from  mediaType:(NSString *) mediaType publisherID:(NSString *)publisherID {
    NSLog(@"xmppJingle didReceiveContentRemove %@", sessionID);
    
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
       peer = [_roomsService getRoomByRTCJid:from.bare];
    peer.mediaType = [self peerMediaTypeFromMediaType:mediaType];
    if(publisherID)
        peer.publisherID = publisherID;
    [_rtcService didReceiveContentRemove:sessionID sdp:sdp from:peer resource:from.resource];
}

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveContentAccept:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from  mediaType:(NSString *) mediaType publisherID:(NSString *)publisherID {
    NSLog(@"xmppJingle didReceiveContentAccept %@", sessionID);
    
    Peer *peer = [_contactsManager createOrUpdateRainbowContactWithJid:from.bare];
    if(!peer)
        peer = [_roomsService getRoomByRTCJid:from.bare];
    peer.mediaType = [self peerMediaTypeFromMediaType:mediaType];
    if(publisherID)
        peer.publisherID = publisherID;
    [_rtcService didReceiveContentAccept:sessionID sdp:sdp from:peer resource:from.resource];
}


#pragma mark - Last Activity
-(void) getLastActivityForContact:(Contact*)contact {
    [_xmppLastActivity sendLastActivityQueryToJID:[XMPPJID jidWithString:contact.jid] withTimeout:5];
}

- (void)xmppLastActivity:(XMPPLastActivity *)sender didReceiveResponse:(XMPPIQ *)response {
    NSXMLElement *queryElement = [response elementForName:@"query"];
    int seconds = [[queryElement attributeStringValueForName:@"seconds"] intValue];
    NSDate *date = [[NSDate date] dateBySubtractingSeconds:seconds];
    Contact *contact = [_contactsManager getContactWithJid:response.from.bare];
    if(!contact) {
        NSLog(@"Last-activity update error : Unable to find contact with JID %@", response.from.bare);
        return;
    }
    [_contactsManager updateLastActivityForContact:contact withSinceDate:date];
}

- (void)xmppLastActivity:(XMPPLastActivity *)sender didNotReceiveResponse:(NSString *)queryID dueToTimeout:(NSTimeInterval)timeout {
    // ignore error for now.
    NSLog(@"Error, unable to get the last-activity. QueryID %@", queryID);
}

#pragma mark - Send ping to server
-(void) sendPingToServer {
    if([_xmppStream isConnected]){
        NSLog(@"Send ping to server with 10 sec timeout");
        [_xmppPing sendPingToServerWithTimeout:10];
    }
}
- (void)xmppPing:(XMPPPing *)sender didReceivePong:(XMPPIQ *)pong withRTT:(NSTimeInterval)rtt {
    NSLog(@"did receive pong");
}

- (void)xmppPing:(XMPPPing *)sender didNotReceivePong:(NSString *)pingID dueToTimeout:(NSTimeInterval)timeout {
    NSLog(@"did not receive pong due to timeout");
    [_xmppStream forceCloseSocket];
}

#pragma mark - Stream management delegates
- (void)xmppStreamManagement:(XMPPStreamManagement *)sender wasEnabled:(NSXMLElement *)enabled {
    NSLog(@"Stream management is enabled");
}
- (void)xmppStreamManagement:(XMPPStreamManagement *)sender wasNotEnabled:(NSXMLElement *)failed {
    NSLog(@"Stream management is *NOT* enabled");
}
- (void)xmppStreamManagementDidRequestAck:(XMPPStreamManagement *)sender {
    NSLog(@"Stream management did request ack");
}
- (void)xmppStreamManagement:(XMPPStreamManagement *)sender didReceiveAckForStanzaIds:(NSArray *)stanzaIds {
    NSLog(@"Stream management did receive ack for stanza ids %@", stanzaIds);
    [_internalOperationQueue addOperationWithBlock:^{
        for (NSString *stanzaId in stanzaIds) {
            [_outgoingStanzaCache removeObjectForKey:stanzaId];
        }
        [self loadPendingOutgoingStanzaCache];
    }];
}

- (id)xmppStreamManagement:(XMPPStreamManagement *)sender stanzaIdForSentElement:(XMPPElement *)theElement {
    // Save the message in the cache to resend them in case of problem
    XMPPElement *element = [theElement copy];
    NSXMLElement *stamp = [NSXMLElement elementWithName:@"tmptimestamp" xmlns:@"urn:xmpp:receipts:tmp"];
    [stamp addAttributeWithName:@"value" stringValue:[[NSDate date] xmppDateTimeString]];
    [element addChild:stamp];
    if(element.elementID){
        if([[element name] isEqualToString:@"message"]){
            // For messages we must add the timestamp and the retransmission element
            XMPPMessage *message = [XMPPMessage messageFromElement:element];
            if([message body]){
                NSXMLElement *cachedElement = [element copy];
                // Add retransmission in cached message and timestamp
                NSXMLElement *retransmission = [NSXMLElement elementWithName:@"retransmission" xmlns:@"jabber:iq:notification"];
                [retransmission addAttributeWithName:@"stamp" stringValue:[[NSDate date] xmppDateTimeString]];
                [retransmission addAttributeWithName:@"source" stringValue:@"client"];
                [cachedElement addChild:retransmission];
                
                NSXMLElement *timestamp = [NSXMLElement elementWithName:@"timestamp" xmlns:@"urn:xmpp:receipts"];
                [timestamp addAttributeWithName:@"value" stringValue:[[NSDate date] xmppDateTimeString]];
                [cachedElement addChild:timestamp];
                [_internalOperationQueue addOperationWithBlock:^{
                    [_outgoingStanzaCache setObjectAsync:[cachedElement compactXMLString] forKey:element.elementID completion:nil];
                }];
            } else {
                [_internalOperationQueue addOperationWithBlock:^{
                    [_outgoingStanzaCache setObjectAsync:[element compactXMLString] forKey:element.elementID completion:nil];
                }];
            }
        } else {
            [_internalOperationQueue addOperationWithBlock:^{
                [_outgoingStanzaCache setObjectAsync:[element compactXMLString] forKey:element.elementID completion:nil];
            }];
        }
        return element.elementID;
    }
    return nil;
}

#pragma mark - CallLogs
-(void) loadAllCallLogsWithCompletionHandler:(XmppServiceLoadCallLogsCompletionHandler) completionHandler {
    NSString *uniqueID = [_xmppStream generateUUID];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:_xmppStream.myJID.bare forKey:@"jid"];
    if(completionHandler)
        [dic setObject:[completionHandler copy] forKey:_xmppStream.myJID.bare];
    else
        NSLog(@"No completion handler that is not possible");
    
    NSLog(@"Fetch mam calllogs with paramaters %@", dic);
    @synchronized(_completionHandlerDictionaryMutex){
        [_loadCallLogsRequestCompletionHandler setObject:dic forKey:uniqueID];
    }
    
    [_xmppMessageArchiveManagement fetchCallLogsWithUUID:uniqueID];
}

-(void)xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndFetchingCallLogsWithAnswer:(NSDictionary *) answer {
    NSLog(@"Did end fetching calllogs with answer %@", answer);

    NSString *UUID = answer[@"requestID"];
    NSString *firstElementReceived = answer[@"first"];
    NSString *lastElementReceived = answer[@"last"];
    NSNumber *count = [NSNumber numberWithInteger:((NSString*)(answer[@"count"])).integerValue];
    
    NSDictionary *entry = nil;
    @synchronized(_completionHandlerDictionaryMutex){
        entry =  [_loadCallLogsRequestCompletionHandler objectForKey:UUID];
    }
    NSString *jid = entry[@"jid"];
    
    NSArray *returnedArray = [self fetchCallLogsInCacheForJid:jid];
    
    XmppServiceLoadCallLogsCompletionHandler completionHandler = [entry objectForKey:jid];
    if(completionHandler)
        completionHandler(returnedArray, nil, firstElementReceived, lastElementReceived, count);
    @synchronized(_completionHandlerDictionaryMutex){
        [_loadCallLogsRequestCompletionHandler removeObjectForKey:UUID];
    }
}

-(NSArray<CallLog *> *) fetchCallLogsInCacheForJid:(NSString *) jid {
    NSArray<CallLog*> *archive = [_xmppMessageArchiveManagementStorage fetchCallLogsForJid:[XMPPJID jidWithString:jid] xmppStream:_xmppStream];
    return archive;
}

-(CallLog *) callLogFromUpdatedCallLogMessage:(XMPPMessage *) message {
    CallLog *theCallLog = nil;
    if([message isUpdatedCallLogMessage]){
        NSXMLElement *updatedCallLogElement = [message elementForName:@"updated_call_log" xmlns:XMLNS_XMPP_MAM_CALLLOGS];
        NSXMLElement *forwarded = [updatedCallLogElement forwardedStanza];
        NSDate *forwardedDelay = [forwarded delayedDeliveryDate];
        NSXMLElement *callLog = [forwarded elementForName:@"call_log" xmlns:XMLNS_XMPP_MAM_CALLLOGS];
        theCallLog = [self callLogFromCallLogStanza:callLog];
        theCallLog.date = forwardedDelay;
        theCallLog.isOutgoing = [theCallLog.caller isEqual:_contactsManager.myContact];
    }
    return theCallLog;
}

-(CallLog *) callLogFromCallLogStanza:(NSXMLElement *) callLogStanza {
    CallLog *theCallLog = [CallLog new];
    NSString *messageID = [callLogStanza elementForName:@"call_id"].stringValue;
    
    theCallLog.callLogId = messageID;
    theCallLog.media = [CallLog callLogMediaFromString:[callLogStanza elementForName:@"media"].stringValue];
    NSString *caller = [callLogStanza elementForName:@"caller"].stringValue;
    NSString *callee = [callLogStanza elementForName:@"callee"].stringValue;
    // We DO NOT handle calllogs where caller or callee can be null..
    if(!caller || !callee ||[callee containsString:@"janusgateway"] || [caller containsString:@"janusgateway"] || [caller containsString:@"mp_"])
        return nil;
    
    NSString *service = [callLogStanza attributeForName:@"service"].stringValue;
    
    NSMutableDictionary *callerIdentity = [NSMutableDictionary new];
    if(service)
        [callerIdentity setValue:service forKey:@"service"];
    
    // Read caller identity from call log
    NSXMLElement *callerInfo = [callLogStanza elementForName:@"caller_info"];
    if(callerInfo) {
        NSXMLElement *identity = [callerInfo elementForName:@"identity"];
        NSString *caller_firstName = [identity attributeForName:@"firstName"].stringValue;
        if(caller_firstName)
            [callerIdentity setValue:caller_firstName forKey:@"firstName"];
        NSString *caller_lastName = [identity attributeForName:@"lastName"].stringValue;
        if(caller_lastName)
            [callerIdentity setValue:caller_lastName forKey:@"lastName"];
        NSString *caller_displayName = [identity attributeForName:@"displayName"].stringValue;
        if(caller_displayName)
            [callerIdentity setValue:caller_displayName forKey:@"displayName"];
    }
    theCallLog.caller = [_contactsManager createOrUpdateRainbowContactFromCallLogJid:caller withDictionary:callerIdentity];
    
    NSMutableDictionary *calleeIdentity = [NSMutableDictionary new];
    if(service)
        [calleeIdentity setValue:service forKey:@"service"];
    
    // Read callee identity from call log
    NSXMLElement *calleeInfo = [callLogStanza elementForName:@"callee_info"];
    if(calleeInfo) {
        NSXMLElement *identity = [calleeInfo elementForName:@"identity"];
        NSString *callee_firstName = [identity attributeForName:@"firstName"].stringValue;
        if(callee_firstName)
            [calleeIdentity setValue:callee_firstName forKey:@"firstName"];
        NSString *callee_lastName = [identity attributeForName:@"lastName"].stringValue;
        if(callee_lastName)
            [calleeIdentity setValue:callee_lastName forKey:@"lastName"];
        NSString *callee_displayName = [identity attributeForName:@"displayName"].stringValue;
        if(callee_displayName)
            [calleeIdentity setValue:callee_displayName forKey:@"displayName"];
    }
    theCallLog.callee = [_contactsManager createOrUpdateRainbowContactFromCallLogJid:callee withDictionary:calleeIdentity];
    theCallLog.state = [CallLog callLogStateFromEventTypeString:[callLogStanza elementForName:@"state"].stringValue];
    theCallLog.type = [CallLog callLogTypeFromString:[callLogStanza attributeForName:@"type"].stringValue];
    theCallLog.service = [CallLog callLogServiceFromString:service];
    theCallLog.realDuration = [NSNumber numberWithInteger:[callLogStanza elementForName:@"duration"].stringValueAsInt];
    NSXMLElement *ackElement = [callLogStanza elementForName:@"ack"];
    BOOL isRead = [[ackElement attributeForName:@"read"].stringValue isEqualToString:@"true"];
    theCallLog.isRead = isRead;
    return theCallLog;
}

-(void) deleteCallLogsWithPeer:(Peer *) peer; {
    NSDictionary *dic = @{@"peer": peer};
    NSLog(@"Delete calllogs for %@", dic);
    
    // Is peer JID for rainbow user OR phone number for others
    NSString *requestID;
    
    if(peer.jid)
        requestID = peer.jid;
    else {
        Contact *contact = (Contact*)peer;
        PhoneNumber *phoneNumber = [[contact phoneNumbers] firstObject];
        requestID = phoneNumber.number;
    }
    
    NSString *uniqueID = [NSString stringWithFormat:@"%@%@", kRemoveCallLogsKeyPrefex, requestID];
    
    @synchronized(_completionHandlerDictionaryMutex) {
        [_deleteRequestCompletionHandler setObject:dic forKey:uniqueID];
    }
    
    [_xmppMessageArchiveManagement deleteCallLogsWithContactJID:requestID withUUID:uniqueID];
}

-(void) deleteAllCallLogs {
    NSLog(@"Delete all calllogs");
    NSDictionary *dic = @{@"deleteAll":@"deleteAllCallLogs"};
    NSString * generatedID = [Tools generateUniqueID];
    NSString *uniqueID = [NSString stringWithFormat:@"%@%@",kRemoveAllCallLogsKeyPrefex,generatedID];
    @synchronized(_completionHandlerDictionaryMutex){
        [_deleteRequestCompletionHandler setObject:dic forKey:uniqueID];
    }
    [_xmppMessageArchiveManagement deleteAllCallLogsWithUUID:uniqueID];
}

-(void) markAsReadCallLogWithId:(NSString *) callLogID {
    XMPPMessage *message = [XMPPMessage message];
    [message addAttributeWithName:@"id" stringValue:[_xmppStream generateUUID]];
    NSXMLElement *readElement = [NSXMLElement elementWithName:@"read" xmlns:XMLNS_XMPP_CALLLOGS_RECEIPTS];
    [readElement addAttributeWithName:@"call_id" stringValue:callLogID];
    [message addChild:readElement];
    [_xmppStream sendElement:message];
}

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didEndDeleteCallLogsForUUID:(NSString *) uuid {
    if(uuid){
        Peer *peer = nil;
        @synchronized(_completionHandlerDictionaryMutex){
            NSDictionary *peerDic = [_deleteRequestCompletionHandler objectForKey:uuid];
            peer = [peerDic objectForKey:@"peer"];
        }
        [_callLogsDelegate xmppService:self didRemoveCallLogsWithPeer:peer];
    }
}

-(void) didReceiveDeleteAllCallLogsInXmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender {
    [_callLogsDelegate didRemoveAllCallLogs:self];
}

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *)sender didReceiveDeleteCallLogForContactJID:(NSString *) jid {
    Peer *peer = [_contactsManager getContactWithJid:jid];
    [_callLogsDelegate xmppService:self didRemoveCallLogsWithPeer:peer];
}

-(void) xmppMessageArchiveManagement:(XMPPMessageArchiveManagement *) sender didUpdateCallLogReadState:(NSString *) callLogID {
    [_callLogsDelegate xmppService:self didUpdateCallLogReadState:callLogID];
}

#pragma mark - Call Service

-(BOOL) isCallServiceWithCallForwardStatus:(XMPPMessage *) message {
    if ([message isCallServiceMessage]) {
        NSXMLElement *xElem = [message elementForName:@"callservice" xmlns:@"urn:xmpp:pbxagent:callservice:1"];
        if(xElem && [xElem elementForName:@"forwarded"]) {
            return YES;
        }
    }
    return NO;
}

-(BOOL) isCallServiceWithMessaging:(XMPPMessage *) message {
    if ([message isCallServiceMessage]) {
        NSXMLElement *callServiceElem = [message elementForName:@"callservice" xmlns:@"urn:xmpp:pbxagent:callservice:1"];
        if(callServiceElem && [callServiceElem elementForName:@"messaging"]) {
            return YES;
        }
    }
    return NO;
}

-(CallForwardStatus *) callForwardStatusFromXMPPMessage:(XMPPMessage *) message {
    CallForwardStatus *status = nil;
    if([self isCallServiceWithCallForwardStatus:message]){
        NSXMLElement *callServiceElement = [message elementForName:@"callservice" xmlns:@"urn:xmpp:pbxagent:callservice:1"];
        NSXMLElement *forwardedElement = [callServiceElement elementForName:@"forwarded"];
        NSString *forwardType = [forwardedElement attributeForName:@"forwardType"].stringValue;
        NSString *forwardTo = [forwardedElement attributeForName:@"forwardTo"].stringValue;
        
        status = [CallForwardStatus new];
        
        if([forwardType isEqualToString:@"Activation"]) {
            // Forwarded to voicemail
            if([forwardTo isEqualToString:@"VOICEMAILBOX"])
                status.type = CallForwardTypeVoicemail;
            // Forwarded to phone number
            else {
                status.type = CallForwardTypePhoneNumber;
                status.destination = [PhoneNumber new];
                status.destination.numberE164 = forwardTo;
            }
        }
        
        else if ([forwardType isEqualToString:@"Deactivation"]){
            status.type = CallForwardTypeNotForwarded;
        }
        
    }
    return status;
}

-(NSString *) markAllAsReadEventJid:(XMPPMessage *) message {
    NSString * peerJid;
    NSXMLElement *element = [message elementForName:@"mark_as_read"];
    
    peerJid = [element attributeForName:@"with"].stringValue;
    return peerJid;
}

-(BOOL) markAllAsReadIsReadByMe:(XMPPMessage *) message {
    NSString * readByMe;
    NSXMLElement *element = [message elementForName:@"mark_as_read"];
    
    readByMe = [element attributeForName:@"id"].stringValue;
    return ![readByMe isEqualToString:@"all-sent"];
}

-(CallEvent *) callEventFromXMPPMessage:(XMPPMessage *) message {
    CallEvent *callEvent = [CallEvent new];

    NSXMLElement *callServiceElement = [message elementForName:@"callservice" xmlns:@"urn:xmpp:pbxagent:callservice:1"];
    
    NSXMLElement *initiatedElement = [callServiceElement elementForName:@"initiated"];
    NSXMLElement *deliveredElement = [callServiceElement elementForName:@"delivered"];
    NSXMLElement *originatedElement = [callServiceElement elementForName:@"originated"];
    NSXMLElement *retrieveCallElement = [callServiceElement elementForName:@"retrieveCall"];
    NSXMLElement *establishedElement = [callServiceElement elementForName:@"established"];
    NSXMLElement *holdCallElement = [callServiceElement elementForName:@"holdCall"];
    NSXMLElement *queuedElement = [callServiceElement elementForName:@"queued"];
    NSXMLElement *connectionClearedElement = [callServiceElement elementForName:@"connectionCleared"];
    NSXMLElement *failedElement = [callServiceElement elementForName:@"failed"];
    NSXMLElement *updateCallElement = [callServiceElement elementForName:@"updateCall"];
    NSXMLElement *conferencedElement = [callServiceElement elementForName:@"conferenced"];
    NSXMLElement *transferCallElement = [callServiceElement elementForName:@"transferCall"];
    NSXMLElement *callSubjectElement = [callServiceElement elementForName:@"callSubject"];
    
    if(initiatedElement) {
        // No state from this element
        [self parseCallEventAttributes:callEvent withXMLElement:initiatedElement];
    } else if (deliveredElement) {
        NSString *type = [deliveredElement attributeForName:@"type"].stringValue;
        
        if ([type isEqualToString:@"outgoing"]) {
            callEvent.state = CallStateRingingOutgoing;
        } else if ([type isEqualToString:@"incoming"]) {
            callEvent.state = CallStateRingingIncoming;
        }
        else {
            callEvent.state = CallStateIdle;
        }
        
        [self parseCallEventAttributes:callEvent withXMLElement:deliveredElement];
        
    } else if (originatedElement) {
        callEvent.state = CallStateRingingOutgoing;
        [self parseCallEventAttributes:callEvent withXMLElement:originatedElement];
        
    } else if (retrieveCallElement) {
        callEvent.state = CallStateActive;
        [self parseCallEventAttributes:callEvent withXMLElement:retrieveCallElement];
        
    } else if (establishedElement) {
        callEvent.state = CallStateActive;
        [self parseCallEventAttributes:callEvent withXMLElement:establishedElement];
              
    } else if (holdCallElement) {
        callEvent.state = CallStateHeld;
        [self parseCallEventAttributes:callEvent withXMLElement:holdCallElement];
        
    } else if (queuedElement) {
        callEvent.state = CallStateRingingIncoming;
        [self parseCallEventAttributes:callEvent withXMLElement:queuedElement];
        
    } else if (connectionClearedElement) {
        callEvent.state = CallStateReleasing;
        [self parseCallEventAttributes:callEvent withXMLElement:connectionClearedElement];
        
    } else if (failedElement) {
        callEvent.state = CallStateIdle;
        [self parseCallEventAttributes:callEvent withXMLElement:failedElement];
        
    } else if (updateCallElement) {
        callEvent.state = CallStateUnknown;
        [self parseCallEventAttributes:callEvent withXMLElement:updateCallElement];
        // Update call participant name
        callEvent.callParticipants.firstObject.firstname = [[updateCallElement elementForName:@"identity"] attributeStringValueForName:@"firstName"];
        callEvent.callParticipants.firstObject.lastname = [[updateCallElement elementForName:@"identity"] attributeStringValueForName:@"lastName"];
        
    } else if (conferencedElement) {
        callEvent.state = CallStateActive;
        
    } else if (transferCallElement) {
        callEvent.state = CallStateActive;
    
    } else if (callSubjectElement) {
        callEvent.state = CallStateRingingIncoming;
        [self parseCallEventAttributes:callEvent withXMLElement:callSubjectElement];
    }
    
    return callEvent;
}

-(void) parseCallEventAttributes:(CallEvent *) event withXMLElement:(NSXMLElement *) element {
    
    NSString *callId = [element attributeForName:@"callId"].stringValue;
    NSString *newCallId = [element attributeForName:@"newCallId"].stringValue;
    
    if (newCallId) {
        event.callReference = newCallId;
    } else {
        event.callReference = callId;
    }
    
    NSString *shortCallRef = [[event.callReference componentsSeparatedByString:@"#"] firstObject];
    event.shortCallReference = shortCallRef;
    
    NSString *endpointTel = [element attributeForName:@"endpointTel"].stringValue;
    if (endpointTel) {

        NSString *endpointIm = [element attributeForName:@"endpointIm"].stringValue;
        
        CallParticipant *callParticipant = [CallParticipant new];
        callParticipant.jid = endpointIm;
        callParticipant.number = endpointTel;
        
        [event.callParticipants addObject:callParticipant];
    }
    
    NSString *cause = [element attributeForName:@"cause"].stringValue;
    if (cause)
        event.callCause = cause;
    
    if ([cause isEqualToString:@"CALLPICKUPTANDEM"])
        event.callCause = @"CALLPICKUP";
    
    NSString *deviceType = [element attributeForName:@"deviceType"].stringValue;
    if (deviceType)
        event.deviceType = deviceType;
}

-(void) registerToMediaPillar:(NSString *)number displayName:(NSString *)name secret:(NSString *)secret address:(NSString *)address {
    [_xmppMediaPillar registerMediaPillar:number displayName:name secret:secret toURLAddress:address jid:_contactsManager.myUser.contact.jid jidTel:_contactsManager.myUser.contact.jid_tel];
}

-(void) unregisterToMediaPillar:(NSString *)number address:(NSString *)address {
    [_xmppMediaPillar unregisterMediaPillar:number toURLAddress:address];
}



@end
