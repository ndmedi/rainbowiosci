/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "Contact.h"
#import "Conversation.h"
#import "Message.h"
#import "Server.h"
#import "ContactsManagerService.h"
#import "RoomsService.h"
#import "Group.h"
#import "XMPPServiceProtocol.h"
#import "GroupsService.h"
#import "RTCService.h"
#import "CompaniesService.h"
#import "XMPPMessage.h"
#import "FileSharingService+Internal.h"
#import "ConferenceInfo.h"


#define CSI_ACTIVATED 1

typedef void (^XmppServiceSendMessageCompletionHandler)(Message *message, NSError *error);
typedef void (^XmppServiceLoadArchivedMessagesCompletionHandler)(NSArray<Message *> *archive, NSError *error, NSString *firstElementRetreived, NSString * lastElementRetreived, BOOL isCompleted, NSNumber *totalCount);

typedef void (^XmppServiceLoadCallLogsCompletionHandler)(NSArray<CallLog *> *archive, NSError *error, NSString *firstElementRetreived, NSString * lastElementRetreived, NSNumber *totalCount);

typedef NS_ENUM(NSInteger, XMPPJingleMessageType) {
    XMPPJingleMessageTypeSessionInitiate = 0,
    XMPPJingleMessageTypeSessionAccept,
    XMPPJingleMessageTypeContentAdd,
    XMPPJingleMessageTypeContentRemove,
    XMPPJingleMessageTypeContentModify,
    XMPPJingleMessageTypeContentAccept,
    XMPPJingleMessageTypeTransportReplace,
    XMPPJingleMessageTypeTransportAccept
};

@protocol XMPPJingleDelegate;
@class RTCIceCandidate;

@class XMPPService;

@interface XMPPService : NSObject

@property (nonatomic, assign) id<XMPPServiceLoginDelegate> loginDelegate;
@property (nonatomic, assign) id<XMPPServiceConversationDelegate> conversationDelegate;
@property (nonatomic, assign) id<XMPPServiceRoomsDelegate> roomsDelegate;
@property (nonatomic, assign) id<XMPPServiceConferenceDelegate> conferenceDelegate;
@property (nonatomic, assign) id<XMPPServiceCallLogsDelegate> callLogsDelegate;
@property (nonatomic, assign) id<XMPPServiceChannelsDelegate> channelsDelegate;
@property (nonatomic, assign) id<XMPPServiceContactsManagerDelegate> contactsDelegate;
@property (nonatomic, assign) id<XMPPServiceTelephonyDelegate> telephonyDelegate;
@property (nonatomic, readonly) BOOL isXmppConnected;
@property (nonatomic, weak) RoomsService *roomsService;
@property (nonatomic, weak) GroupsService *groupsService;
@property (nonatomic, weak) RTCService *rtcService;
@property (nonatomic, weak) CompaniesService *companiesService;
@property (nonatomic, weak) FileSharingService *fileSharingService;
@property (nonatomic, strong) NSString *applicationID;
@property (nonatomic, strong) NSData *pushToken;

@property (nonatomic, readonly) BOOL autoReconnectEnabled;


-(instancetype) initWithCertificateManager:(DownloadManager *) certificateTrustExtension contactManagerService:(ContactsManagerService *) contactsManager;

-(void) setResourceName:(NSString *) name;

-(void) reset;

-(void) sendPingToServer;

-(BOOL) connectToServer:(Server *) server withJID:(NSString *) jid andPassword:(NSString *) password withError:(NSError **) error;
-(void) disconnect;
-(void) forceClosingConnection;
-(void) forceNextConnectionToNotResumeTheStream;

-(Message *) messageFromXmppMessage:(XMPPMessage *) aMessage;

-(XMPPMessage *) createXMPPMessage:(NSString *) message ofType:(MessageType) type withOBData:(NSURL *)url desc:(NSString *)urlDesc withId:(NSString *) messageID to:(Peer *) peer withCompletionHandler:(XmppServiceSendMessageCompletionHandler) completionHandler;

-(Message *) sendMessage:(NSString *) message ofType:(MessageType) type to:(Peer *) peer withCompletionHandler:(XmppServiceSendMessageCompletionHandler) completionHandler;
-(Message *) sendXMPPMessage:(XMPPMessage *)msg;

-(Message *) sendLocalyXMPPMessage:(XMPPMessage *)msg;

-(void) removeXMPPMessagefromCache:(XMPPMessage *)msg;

-(void) removeMessageWithMessageIDfromCache:(NSString *)messageID;

-(Message *) getLastArchivedMessageWithContactJid:(NSString *) contactJid;

-(ConferenceInfo *) conferenceInfoMessageFromXmppMessage:(XMPPMessage *) aMessage;

-(void) sendReadByMeACKMessageTo:(NSString *)jid messageID:(NSString*)messageID;

-(void) sendChatStateMessageTo:(Peer *) peer status:(ConversationStatus) status;

-(void)loadArchivedMessagesWith:(Peer *) peer maxSize:(NSNumber *)maxSize beforeDate:(NSDate *)beforeDate lastMessageID:(NSString *)lastMessageID offset:(NSNumber *) offset withCompletionHandler:(XmppServiceLoadArchivedMessagesCompletionHandler) completionHandler;

-(NSArray<Message *> *) getArchivedMessageWithContactJid:(NSString *) contactJid maxSize:(NSNumber *) maxSize offset:(NSNumber *) offset;

-(void)loadAllCallLogsWithCompletionHandler:(XmppServiceLoadCallLogsCompletionHandler) completionHandler;

-(void) setMyPresenceTo:(Presence *) presence;

-(void) addContactWithJid:(NSString *) jid;

-(void) removeContactWithJid:(NSString *) jid;

-(void) acceptBuddyRequestFrom:(Contact *) contact;

-(void) rejectBuddyRequestFrom:(Contact *) contact;

-(void) sendJingleInitiationProposeTo:(NSString *) jid sessionID:(NSString *) sessionID withMedias:(NSArray<NSString *> *) medias;

-(void) sendJingleInitiationProposeTo:(NSString *) jid sessionID:(NSString *) sessionID withMedias:(NSArray<NSString *> *) medias andSubject: (NSString *) subject;

-(void) sendJingleInitiationRetractTo:(NSString *) jid sessionID:(NSString *) sessionID;

-(void) sendJingleInitiationAcceptsessionID:(NSString *) sessionID;

-(void) sendJingleInitiationProceedTo:(NSString *) jid sessionID:(NSString *) sessionID;

-(void) sendJingleInitiationRejectsessionID:(NSString *) sessionID;

-(void) sendJingleInitiationRejectTo:(NSString *) jid sessionID:(NSString *) sessionID;

-(void) sendJingleTerminatetoTarget:(NSString *)jid withReason:(NSString*)reason sessionID:(NSString *)sessionID peer:(Peer *) peer;

-(void) sendJingleTransportInfoWithCandidate:(RTCIceCandidate *)candidate toTarget:(NSString *)jid sessionID:(NSString *)sessionID peer:(Peer *) peer;

-(void) sendJingleSDP:(NSString *) sdp type:(XMPPJingleMessageType) type toTarget:(NSString *) jid sessionID:(NSString *) sessionID peer:(Peer *) peer;

-(void) sendMediaPillarJingleSDP:(NSString *) sdp toTarget:(NSString *) jid sessionID:(NSString *) sessionID phoneNumber:(NSString *) number;

-(void) sendJingleHoldFromJid:(NSString*) jid toTarget:(NSString *) toJid sessionID:(NSString *) sessionID;
-(void) sendJingleUnholdFromJid:(NSString*) jid toTarget:(NSString *) toJid sessionID:(NSString *) sessionID;

-(void) getLastActivityForContact:(Contact*)contact;

-(void) deleteAllMessagesFromConversation:(Conversation *)conversation;

// Voicemail
-(void) sendCallServiceConnexionRequestToTarget:(NSString*) jidTel;

// Rooms
-(void) joinRoom:(Room *) room;

-(void) leaveRoom:(Room *) room;

-(BOOL) isRoomJoined:(Room *) room;

// Return a message from the given dictionary (used when receive a push notification), it will also save the message in MAM, the return message is just given "for info"
-(Message *) createAndDispatchMessageFromDictionary:(NSDictionary *) dictionary;

// Retur a peer based on the given jid, used in connectionCompletionHandler deal with push 
-(Peer *) peerFromJidString:(NSString *) jidString;

-(void) deleteCallLogsWithPeer:(Peer *) peer;
-(void) deleteAllCallLogs;
-(void) markAsReadCallLogWithId:(NSString *) callLogID;

-(void) registerToMediaPillar:(NSString *)number displayName:(NSString *)name secret:(NSString *)secret address:(NSString *)address;
-(void) unregisterToMediaPillar:(NSString *)number address:(NSString *)address;
-(NSArray<CallLog *> *) fetchCallLogsInCacheForJid:(NSString *) jid;
-(CallLog *) callLogFromCallLogStanza:(NSXMLElement *) callLogStanza;

-(Peer *) peerForJID:(XMPPJID *) jid;
-(Peer *) viaForJID:(XMPPJID *) jid;

@end
