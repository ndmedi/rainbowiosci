/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "MessagesBrowser.h"
#import "CKItemsBrowser+protected.h"
#import "MessagesPageOperation.h"
#import "Message.h"
#import "Message+Browsing.h"
#import "MessageInternal.h"
#import "MessagesBrowser+Internal.h"
#import "ServicesManager+Internal.h"
#import "ConversationsManagerService.h"
#import "ConversationsManagerService+internal.h"
#import "MessagesCachedPageOperation.h"
#import "Conversation+Internal.h"

@interface MessagesBrowser ()
@property (nonatomic, strong) Conversation *conversation;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) NSString *firstElementRetreived;
-(void) performJob:(CKBrowsingOperation*)localJob withCompletionHandler:(CKItemsBrowsingCompletionHandler)completionHandler;
@end

@implementation MessagesBrowser

-(instancetype) initWithConversation:(Conversation *) conversation withXMPPService:(XMPPService *) xmppService withPageSize:(NSInteger) pageSize{
    self = [super initWithItemsCountPerPage:NSIntegerMax];
    if(self){
        _conversation = conversation;
        _xmppService = xmppService;
        _itemsCountPerPage = pageSize;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNewMessage:) name:kConversationsManagerDidReceiveNewMessage object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onACKMessageReceived:) name:kConversationsManagerDidAckMessageNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNewMessage:) name:kConversationsManagerDidReceiveCarbonCopyMessage object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAllMessagesDeleted:) name:kConversationsManagerDidRemoveAllMessagesForConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUpdateMessage:) name:kConversationsManagerDidUpdateMessage object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMessagesNeedResync:) name:kConversationsManagerDidChangeConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNewMessage:) name:kConversationsManagerWillSendNewMessage object:nil];
        
        

    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidRemoveAllMessagesForConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidReceiveNewMessage object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidAckMessageNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidReceiveCarbonCopyMessage object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidUpdateMessage object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidChangeConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerWillSendNewMessage object:nil];
    _conversation = nil;
    _xmppService = nil;
}

-(void) resyncBrowsingCacheWithCompletionHandler:(CKItemsBrowsingCompletionHandler)completionHandler {
    if(_conversation.isSynchronized) {
        NSLog(@"This conversation is synchronized %@", _conversation);
        if(completionHandler)
            completionHandler(nil, nil, nil, [NSError errorWithDomain:@"Browser" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Conversation is already synchronized"}]);
        return;
    }
    [super resyncBrowsingCacheWithCompletionHandler:completionHandler];
}

#pragma mark - CKItemsBrowser overriden selectors.

-(NSArray*) applyChangesToCurrentCacheItems:(NSArray*)cachedItems fromFreshItems:(NSArray*)freshItems {
    NSMutableArray* updatedItems = [NSMutableArray array];
    [cachedItems enumerateObjectsUsingBlock:^(Message* cachedItem, NSUInteger idx, BOOL *stop) {
        // ids should match at same index !
        Message* freshItem = [freshItems objectAtIndex:idx];
        NSAssert([cachedItem.referenceIdentifier isEqualToString:freshItem.referenceIdentifier], nil);
        if (![cachedItem.date isEqual:freshItem.date]) {
            // this message has been changed => update to its most recent state
            cachedItem.timestamp = freshItem.date;
            [updatedItems addObject:cachedItem];
        }
    }];
    return updatedItems;
}

#pragma mark - 

-(void) onNewMessage:(NSNotification*)notification {
    // make my own private copy !
    Message* msg = (Message*)notification.object;
    NSLog(@"did Recieve onNewMessage %@",msg);
    if ([msg.via isEqual:_conversation.peer]) {
        // the added message is related to the current browsed conversation.
        NSInteger idx = [self insertItemAtProperIndex:msg];
        if (idx != NSNotFound) {
            if ([_delegate respondsToSelector:@selector(itemsBrowser:didReceiveItemsAddedEvent:)]) {
                [_delegate itemsBrowser:self didReceiveItemsAddedEvent:[NSArray arrayWithObject:msg]];
            }
            [_delegate itemsBrowser:self didAddCacheItems:@[msg] atIndexes:[NSIndexSet indexSetWithIndex:idx]];
        }
        
       
    }
}

-(void) onUpdateMessage:(NSNotification *) notification {
    Message *msg = (Message *) notification.object;
    if ([msg.via isEqual:_conversation.peer]) {
        Message *cachedMessage = (Message*)[self getCacheItemWithReferenceIdentifier:msg.messageID];
        if(cachedMessage){
            cachedMessage.timestamp = msg.timestamp;
            cachedMessage.attachment = msg.attachment;
            [cachedMessage setDeliveryState:msg.state atTimestamp:msg.timestamp];
            [_delegate itemsBrowser:self didUpdateCacheItems:@[cachedMessage] atIndexes:[self browsingCacheIndexesForItems:@[cachedMessage]]];
        }
    }
}

-(void) onMessagesDelete:(NSNotification*)notification {
    // update the browsing cache;
    NSMutableArray* deletedIMs = notification.object;
    NSArray* foundAndRemovedIMs = [self removeCacheItems:deletedIMs];
    
    // informs that this is real delete and not just a just in the browsing window
    if ([self.delegate respondsToSelector:@selector(itemsBrowser:didReceiveItemsDeletedEvent:)]) {
        [self.delegate itemsBrowser:self didReceiveItemsDeletedEvent:foundAndRemovedIMs];
    }
}

-(void) onACKMessageReceived:(NSNotification*) notification {
    NSDictionary *userInfo = notification.object;
    //NSDictionary *userInfo = @{@"messageID": messageID, @"state": [NSNumber numberWithInt:state], @"datetime": datetime};
    
    Message *message = [self getCacheItemWithReferenceIdentifier:[userInfo objectForKey:@"messageID"]];
    
    if (message) {
        // set the new state (& timestamp is available) of the message.
        [message setDeliveryState:[[userInfo objectForKey:@"state"] intValue] atTimestamp:[userInfo objectForKey:@"datetime"]];
        if ([[userInfo objectForKey:@"state"]integerValue] < 2) {
            message.timestamp = [userInfo objectForKey:@"datetime"];
        }
        
        [_delegate itemsBrowser:self didUpdateCacheItems:@[message] atIndexes:[self browsingCacheIndexesForItems:@[message]]];
    }
}




-(CKBrowsingOperation*) nextPageOperation {
    MessagesPageOperation* operation = [[MessagesPageOperation alloc] initWithPeer:_conversation.peer withXMPPService:_xmppService withCompletionHandler:^(NSString *firstElementRetreived, NSString *lastElementRetreived, BOOL isCompleted, NSNumber *totalCount) {
        _firstElementRetreived = firstElementRetreived;
        @synchronized (_conversation) {
            _conversation.isSynchronized = YES;
        }   
    }];
    // This is the first page we must send a date instead of the lastMessageID
    if(self.browsingCache.count == 0){
        operation.beforeDate = [NSDate date];
        operation.lastMessageID = nil;
    } else {
        operation.beforeDate = nil;
        operation.lastMessageID = _firstElementRetreived;
    }
    operation.itemsCount = _itemsCountPerPage;
    return operation;
}

-(void) getArchivedMessages{
    NSLog(@"Get Archived messages");
    MessagesCachedPageOperation *operation = [[MessagesCachedPageOperation alloc] initWithPeer:_conversation.peer withXMPPService:_xmppService];
    operation.itemsCount = _itemsCountPerPage;
    
    operation.startIndex = [_browsingCache count];
    [self performJob:operation withCompletionHandler:^(NSArray *addedCacheItems, NSArray *removedCacheItems, NSArray *updatedCacheItems, NSError *error) {
        NSLog(@"Get archived messages ended, added %ld, updated %ld, removed %ld", addedCacheItems.count, updatedCacheItems.count, removedCacheItems.count );
        BOOL hasMore = ([_browsingCache count]==_itemsCountPerPage);
        _hasMorePages = hasMore;
        //completionHandler(YES);
    }];
}

-(void)onAllMessagesDeleted:(NSNotification *) notification {
    NSLog(@"Did receive delete all messages");
    [self reset];
}


-(void)onMessagesNeedResync:(NSNotification *) notification {
    NSLog(@"Need resync messages");
    
    @synchronized (_conversation) {
        _conversation.isSynchronized = NO;
    }
    
    [self resyncBrowsingCacheWithCompletionHandler: nil];
}

@end
