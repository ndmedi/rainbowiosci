/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "Contact.h"
#import "DownloadManager.h"
#import "ApiUrlManagerService.h"
/**
 *  Search completion handler
 *
 *
 *  @param searchedPattern the searched pattern
 *  @param response        json response received from the server
 *  @param error           network error returned by the server
 */
typedef void (^SearchCompletionHandler)(NSString *searchedPattern, NSArray *response, NSError *error);

@interface DirectoryService : NSObject

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService;

/**
 *  Send a search request to server with given pattern.
    Will invoke completionBlock with an array of `Contact`found, or an `NSError` if it encounter an error.
 *
 *  @param pattern         the searched pattern
 *  @param completionBlock the completion handler to invoked when the search is ended. Return an array of `Contact` found or an `NSError` in case of error
 */
-(void) searchRequestWithPattern:(NSString*) pattern withCompletionBlock:(SearchCompletionHandler) completionBlock;

/**
 *  Send a search request to server with given pattern.
    Will invoke completionBlock with an array of `Contact` found, or an `NSError` if it encounter an error.
 *
 *  @param pattern         the searched pattern
 *  @param completionBlock the completion handler to invoked when the search is ended. Return an array of `Contact` found or an `NSError` in case of error
 */
-(void) searchRequestOnPBXPhonebookWithPattern:(NSString*) pattern withCompletionBlock:(SearchCompletionHandler) completionBlock;

/**
 *  Send a search request to server (Active Directory) with given pattern.
 Will invoke completionBlock with an array of `Contact`found, or an `NSError` if it encounter an error.
 *
 *  @param pattern         the searched pattern
 *  @param completionBlock the completion handler to invoked when the search is ended. Return an array of `Contact` found or an `NSError` in case of error
 */
-(void) searchRequestOnActiveDirectoryWithPattern:(NSString *) pattern withCompletionBlock:(SearchCompletionHandler) completionBlock;

-(void) searchExternalContactDetailsWithObjectID:(NSString *) objectID withCompletionBlock:(SearchCompletionHandler) completionBlock;

/**
 *  Send a search request to the server with multiple login emails.
 *
 *  @param loginEmails     the emails to search for
 *  @param offset          the offset expected in response
 *  @param limit           the limit of users in response
 *  @param completionBlock completion handler called once search is done. Return the json response from the server or an `NSError` in case of error
 */
-(void) searchRainbowContactsWithLoginEmails:(NSArray<NSString *> *) loginEmails fromOffset:(NSUInteger) offset withLimit:(NSUInteger) limit withCompletionBlock:(SearchCompletionHandler) completionBlock;

/**
 *  Send a SYNCHRONOUS search request to the server with multiple login emails.
 *
 *  @param loginEmails     the emails to search for
 *  @param offset          the offset expected in response
 *  @param limit           the limit of users in response
 */
-(NSArray *) searchRainbowContactsWithLoginEmails:(NSArray<NSString *> *) loginEmails fromOffset:(NSUInteger) offset withLimit:(NSUInteger) limit;

/**
 *  Send a SYNCHRONOUS search request to the server for a single rainbow ID.
 *
 *  @param rainbowID       the rainbow ID to search for
 */
-(NSDictionary *) searchRainbowContactWithRainbowID:(NSString *) rainbowID;

/**
 *  Send a Asynchronous search request to the server for a single rainbow ID
 *
 *  @param rainbowID       the rainbowID to search for
 *  @param completionBlock completion handler called once search is done. Return the json response from the server or an `NSError` in case of error
 */
-(void) searchRainbowContactWithRainbowID:(NSString *)rainbowID withCompletionBlock:(SearchCompletionHandler)completionBlock;

/**
 *  Send a search request to the server with multiple jid.
 *
 *  @param jids            the jid to search for
 *  @param offset          the offset expected in response
 *  @param limit           the limit of users in response
 *  @param completionBlock completion handler called once search is done. Return an array of `Contact` found or an `NSError` in case of error
 */
-(void) searchRainbowContactsWithJids:(NSArray<NSString *> *) jids fromOffset:(NSUInteger) offset withLimit:(NSUInteger) limit withCompletionBlock:(SearchCompletionHandler) completionBlock;

/**
 *  Send a search request to the server with jid.
 *
 *  @param jid             the jid to search for
 *  @param completionBlock completion handler called once search is done. Return a `Contact` found or an `NSError` in case of error
 */
-(void) searchRainbowContactWithJid:(NSString *) jid withCompletionBlock:(SearchCompletionHandler) completionBlock;

@end
