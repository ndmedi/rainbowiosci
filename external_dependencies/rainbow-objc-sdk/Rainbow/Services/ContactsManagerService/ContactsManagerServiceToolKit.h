/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "Contact.h"
#import "XMPPvCardTemp.h"
#import "PresenceInternal.h"

@interface ContactsManagerServiceToolKit : NSObject

+(Contact *) createRainbowContactFromDictionary:(NSDictionary *) dictionary;
+(void) fillRainbowContact:(Contact *) contact fromDictionary:(NSDictionary *) dictionary;

#pragma mark - vcard to model object methods
+(PhoneNumber *) phoneNumberFromNumber:(NSString *) number type:(PhoneNumberType) type deviceType:(PhoneNumberDeviceType) deviceType;
+(PhoneNumber *) phoneNumberFromNumber:(NSString *) number numberE164:(NSString *) numberE164 type:(PhoneNumberType) type deviceType:(PhoneNumberDeviceType) deviceType isMonitored:(BOOL) isMonitored;

+(void) insertOrUpdatePhoneNumber:(PhoneNumber*) number forRainbowContact:(Contact*) contact;

+(EmailAddress *) emailAddressFromEmailAddress:(NSString*) emailAddress type:(EmailAddressType) type;
+(void) insertOrUpdateEmailAddress:(EmailAddress*) emailAddress forRainbowContact:(Contact*) contact;

+(Presence *) presenceFromXMPPPresence:(XMPPPresence *) presence;
@end
