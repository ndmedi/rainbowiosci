/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ContactsManagerService.h"
#import "XMPPService.h"
#import "LocalContacts.h"
#import "XMPPUserMemoryStorageObject.h"
#import "LoginManager.h"
#import "DownloadManager.h"
#import "MyUser.h"
#import "ApiUrlManagerService.h"
#import "DirectoryService.h"
#import "ConversationsManagerService+Internal.h"
#import "Invitation.h"

#define kContactsManagerServiceDidAddLocalContact @"didAddLocalContact"
#define kContactsManagerServiceDidUpdateLocalContact @"didUpdateLocalContact"
#define kContactsManagerServiceDidRemoveLocalContact @"didRemoveLocalContact"

typedef void (^ContactsManagerCreateContactWithRainbowID)(Contact * contact);

@interface ContactsManagerService ()

@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) MyUser *myUser;
@property (nonatomic, readwrite, strong) Contact *myContact;
// TODO : Remove this link it create a loop between the services
@property (nonatomic, strong) DirectoryService *directoryService;
@property (nonatomic) BOOL cacheLoaded;

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager myUser:(MyUser *) myUser apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService;


#pragma mark - my contact functions

-(Contact *) createMyRainbowContactForJid:(NSString*) jid withCompanyId:(NSString *) companyId withRainbowId:(NSString*) rainbowID companyName :(NSString *) companyName;
-(void) deleteMyContact;

// Invoked from myUser at startup
-(void) setMyPresence:(Presence *) presence;
// Invoked from XMPP Rainbow custom stanza module
-(void) didChangeUserSettings;
// Invoked from XMPP Rainbow custom stanza module
-(void) didReceiveCreateConfUserActivated:(BOOL) isReady;

-(void) resetMyContact;


#pragma mark - CRUD of rainbow part of contacts

-(Contact *) createOrUpdateRainbowContactWithRainbowId:(NSString *) rainbowID;
-(Contact *) createOrUpdateRainbowContactWithJid:(NSString *) jid;
-(Contact *) createOrUpdateRainbowContactFromCallLogJid:(NSString *) jid withDictionary:(NSDictionary*) infoDictionary;
-(Contact *) createOrUpdateRainbowContactFromXMPPUser:(XMPPUserMemoryStorageObject *) xmppUser;
-(Contact *) createOrUpdateRainbowContactFromPresence:(XMPPPresence *) presence;
-(Contact *) createOrUpdateRainbowContactFromJSON:(NSDictionary *) jsonDictionary;
-(Contact *) createRainbowContactFromLoginEmail:(NSDictionary *) jsonDictionary;
-(Contact *) createOrUpdateRainbowBotContactFromJSON:(NSDictionary *) jsonDictionary;
-(Contact *) createOrUpdateRainbowContactWithPhoneNumber:(NSString *) phoneNumber;

// This create method must be used only for rtcCall from janus server
-(Contact *) createPeerWithJid:(NSString *)jid;

-(void) createRainbowContactAsynchronouslyWithRainbowID:(NSString *) rainbowID withCompletionHandler:(ContactsManagerCreateContactWithRainbowID) completionHandler;

// Method use to check in local cache if we already have a contact matching the given rainbow ID, if not search for it on server and create it like usually.
-(void) getOrCreateRainbowContactAsynchronouslyWithRainbowID:(NSString *) rainbowID withCompletionHandler:(ContactsManagerCreateContactWithRainbowID) completionHandler;


-(void) updateRainbowContactNotInRoster:(Contact *) contact;
-(void) updateRainbowContactIsInRoster:(Contact *) contact;
-(void) updateRainbowContactIsInRoster:(Contact *) contact notify:(BOOL) notify;
-(void) updateRainbowContact:(Contact *) contact withGroups:(NSArray *) groups;
-(void) updateRainbowContact:(Contact *) contact withPresence:(XMPPPresence *) presence;
-(void) updateRainbowContact:(Contact *) contact withPresence:(XMPPPresence *) presence withLogs:(BOOL) withLogs;
-(void) updateRainbowContact:(Contact *) contact withCalendarPresence:(XMPPPresence *) presence;
-(void) updateRainbowContact:(Contact *) contact withJSONDictionary:(NSDictionary *) jsonDictionary;
-(void) updateRainbowContact:(Contact *) contact withMobileRessource:(BOOL) mobileRessource;
-(void) updateRainbowContact:(Contact *) contact withTelephonyRessource:(BOOL) telephonyRessource;
-(void) updateRainbowContact:(Contact *) contact withJidTel:(NSString *) jid_tel;
-(void) updateRainbowContact:(Contact *) contact withCompanyInvitation:(CompanyInvitation *) companyInvitation;
-(void) deleteRainbowContact:(Contact *) contact;

#pragma mark - CRUD of local part of contacts

//-(void) syncLocalContactsWithNSArrayOfContact:(NSArray *) contacts;


#pragma mark - Invitation management

-(Invitation *) createOrUpdateInvitation:(InvitationDirection) direction fromDictionary:(NSDictionary *) invitationDict;

-(void) createOrUpdateInvitation:(InvitationDirection) direction withID:(NSString *) invitationID;
-(void) removeInvitationWithID:(NSString *) invitationID;


#pragma mark - Last-activity management
/**
 *  Update the contact last-activity since date.
 *
 *  @param contact The contact concerned
 *  @param date The date since the user is inactive.
 */
-(void) updateLastActivityForContact:(Contact*)contact withSinceDate:(NSDate*)date;


#pragma mark - vcard in rest
-(void) populateAvatarForRainbowContact:(Contact *) contact forceReload:(BOOL) force;
-(void) populateVcardForContactsJids:(NSArray <NSString *> *) contactsJids;

#pragma mark - JID list for vCard to populate
-(void) addJidToFetch:(NSString *) jid;

// New api
-(void) populateRosterVcardWithLimit:(NSInteger) limit;

#pragma mark - to be moved

-(void) callPhoneNumber:(NSString *) phoneNumber directCall:(BOOL) directCall;

/**
 *
 *
 */
-(Contact *) searchRainbowContactWithEmailString:(NSString *)emailString;

/**
 *  Return the contact managed by Contacts Manager service matching the given jid
 *
 *  @param jid the JID of the contact to return
 *
 *  @return Return the mathching contact, or `nil` if no contact found
 *  @see Contact
 */
-(Contact *) getContactWithJid:(NSString *) jid;

/**
 *  Return the contact managed by Contacts Manager service matching the given rainbow ID
 *
 *  @param rainbowID the rainbow ID of the contact to return
 *
 *  @return Return the mathching contact, or `nil` if no contact found
 *  @see Contact
 */
-(Contact *) getContactWithRainbowID:(NSString *) rainbowID;

-(Contact *) getOrCreateRainbowContactSynchronouslyWithJid:(NSString *) jid;

@end
