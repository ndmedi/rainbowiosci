/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ContactsManagerServiceToolKit.h"
#import "PhoneNumberInternal.h"
#import "EmailAddressInternal.h"
#import "PostalAddressInternal.h"
#import "ContactInternal.h"
#import "Tools.h"
#import "NSDate+JSONString.h"
#import "NSObject+NotNull.h"

@implementation ContactsManagerServiceToolKit

+(PhoneNumber *) phoneNumberFromNumber:(NSString *) number type:(PhoneNumberType) type deviceType:(PhoneNumberDeviceType) deviceType {
    PhoneNumber *phoneNumber = [PhoneNumber new];
    phoneNumber.number = number;
    phoneNumber.numberE164 = nil;
    phoneNumber.type = type;
    phoneNumber.deviceType = deviceType;
    return phoneNumber;
}
+(PhoneNumber *) phoneNumberFromNumber:(NSString *) number numberE164:(NSString *) numberE164 type:(PhoneNumberType) type deviceType:(PhoneNumberDeviceType) deviceType isMonitored:(BOOL) isMonitored {
    if(number.length > 0 || numberE164.length > 0){
        PhoneNumber *phoneNumber = [PhoneNumber new];
        phoneNumber.number = number;
        phoneNumber.numberE164 = numberE164;
        phoneNumber.type = type;
        phoneNumber.deviceType = deviceType;
        phoneNumber.isMonitored = isMonitored;
        return phoneNumber;
    } else
        return nil;
}

+(void) insertOrUpdatePhoneNumber:(PhoneNumber*) number forRainbowContact:(Contact*) contact {
    // We have to insert/update or delete the number.
    
    if (!contact.rainbow)
        return;
    
    BOOL found = NO;
    NSMutableArray *discardedPhoneNumbers = [NSMutableArray array];
    
    for (PhoneNumber *num in contact.rainbow.phoneNumbers) {
        if (num.type == number.type && num.deviceType == number.deviceType) {
            // We found the same number <type,capa>
            if ([number.number length] == 0) {
                // if our replacing number is empty, it means delete.
                [discardedPhoneNumbers addObject:num];
            } else {
                num.number = number.number;
            }
            found = YES;
        }
    }
    
    // remove the elements required.
    [contact.rainbow removePhoneNumbersObjectFromArray:discardedPhoneNumbers];
    
    if (!found && [number.number length]) {
        [contact.rainbow addPhoneNumberObject:number];
    }
}

+(EmailAddress *) emailAddressFromEmailAddress:(NSString*) emailAddress type:(EmailAddressType) type {
    EmailAddress *email = [EmailAddress new];
    email.address = emailAddress;
    email.type = type;
    return email;
}

+(void) insertOrUpdateEmailAddress:(EmailAddress*) emailAddress forRainbowContact:(Contact*) contact {
    // We have to insert/update or delete the email address.
    
    if (!contact.rainbow)
        return;
    
    BOOL found = NO;
    NSMutableArray *discardedEmailAddresses = [NSMutableArray array];
    
    for (EmailAddress *address in contact.rainbow.emailAddresses) {
        if (address.type == emailAddress.type) {
            // We found the same email address <type>
            if ([emailAddress.address length] == 0) {
                // if our replacing email address is empty, it means delete.
                [discardedEmailAddresses addObject:address];
            } else {
                address.address = emailAddress.address;
            }
            found = YES;
        }
    }
    
    // remove the elements required.
    [contact.rainbow removeEmailAddressesObjectFromArray:discardedEmailAddresses];
    
    if (!found && [emailAddress.address length]) {
        [contact.rainbow addEmailAddressObject:emailAddress];
    }
}

+(Contact *) createRainbowContactFromDictionary:(NSDictionary *) dictionary {
    Contact* contact = [Contact new];
    contact.rainbow = [RainbowContact new];
    
    [ContactsManagerServiceToolKit fillRainbowContact:contact fromDictionary:dictionary];
    
    return contact;
}

+(void) fillRainbowContact:(Contact *) contact fromDictionary:(NSDictionary *) dictionary {
    // We can't do nothing without the rainbow part.
    if (!contact.rainbow)
        return;
    
    if (dictionary[@"id"])
        contact.rainbow.rainbowID = [dictionary[@"id"] notNull];
    if (dictionary[@"rainbowID"])
        contact.rainbow.rainbowID = [dictionary[@"rainbowID"] notNull];
    if(dictionary[@"companyId"])
        contact.rainbow.companyId = [dictionary[@"companyId"] notNull];
    if(dictionary[@"companyName"])
        contact.rainbow.companyName = [dictionary[@"companyName"] notNull];
    if (dictionary[@"firstName"])
        contact.rainbow.firstName = [dictionary[@"firstName"] notNull];
    if (dictionary[@"lastName"])
        contact.rainbow.lastName = [dictionary[@"lastName"] notNull];
    if(dictionary[@"nickName"])
        contact.rainbow.nickName = [dictionary[@"nickName"] notNull];
    if(dictionary[@"title"])
        contact.rainbow.title = [dictionary[@"title"] notNull];
    if(dictionary[@"jobTitle"])
        contact.rainbow.jobTitle = [dictionary[@"jobTitle"] notNull];
    if(dictionary[@"country"])
        contact.rainbow.countryCode = [dictionary[@"country"] notNull];
    if([dictionary[@"timezone"] notNull])
        contact.rainbow.timeZone = [NSTimeZone timeZoneWithName:dictionary[@"timezone"]];
    if(dictionary[@"language"])
        contact.rainbow.language = [dictionary[@"language"] notNull];
    if(dictionary[@"isTerminated"])
        contact.rainbow.isTerminated = [dictionary[@"isTerminated"] boolValue];
    
    if (dictionary[@"phoneNumbers"]){
        NSArray *phoneNumbers = dictionary[@"phoneNumbers"];
        for (NSDictionary *phoneNumberDic in phoneNumbers) {
            NSString *number = [phoneNumberDic[@"number"] notNull];
            NSString *numberE164 = [phoneNumberDic[@"numberE164"] notNull];
            NSString *type = [phoneNumberDic[@"type"] notNull];
            if([type isEqualToString:@"rainbow"]){
                // This phone number should be used by mediapillar but it actually broke the update profile so for now we filter it.
                continue;
            }
            if(!number)
                number = [phoneNumberDic[@"internalNumber"] notNull];
            
            NSString *pbxID = [[phoneNumberDic objectForKey:@"pbxId"] notNull];
            BOOL isMonitored = pbxID != nil;
            PhoneNumber *thePhoneNumber = [ContactsManagerServiceToolKit phoneNumberFromNumber:number numberE164:numberE164 type:[PhoneNumber typeFromPhoneNumberLabel:[phoneNumberDic[@"type"] lowercaseString]] deviceType:[PhoneNumber deviceTypeFromDeviceTypeLabel:[phoneNumberDic[@"deviceType"] lowercaseString]] isMonitored:isMonitored];
            
            if(thePhoneNumber){
                if(phoneNumberDic[@"country"]){
                    thePhoneNumber.countryCode = [phoneNumberDic[@"country"] notNull];
                } else if(dictionary[@"country"]){
                    thePhoneNumber.countryCode = [dictionary[@"country"] notNull];
                }
                [contact.rainbow addPhoneNumberObject:thePhoneNumber];
            }
        }
    }
    
    if (dictionary[@"jid_im"])
        contact.rainbow.jid = dictionary[@"jid_im"];
    if (dictionary[@"jid_tel"])
        contact.rainbow.jid_tel = dictionary[@"jid_tel"];
    
    if (dictionary[@"emails"]){
        NSArray *emails = dictionary[@"emails"];
        for (NSDictionary *email in emails) {
            EmailAddress *emailAddress = [ContactsManagerServiceToolKit emailAddressFromEmailAddress:email[@"email"] type:[EmailAddress typeFromEmailAddressLabel:email[@"type"] ]];
            emailAddress.isVisible = YES;
            [contact.rainbow addEmailAddressObject:emailAddress];
        }
    }
    
    if(dictionary[@"loginEmail"]){
        EmailAddress *emailAddress = [ContactsManagerServiceToolKit emailAddressFromEmailAddress:dictionary[@"loginEmail"] type:EmailAddressTypeWork];
        emailAddress.isVisible = NO;
        if(![contact.rainbow.emailAddresses containsObject:emailAddress])
            [contact.rainbow addEmailAddressObject:emailAddress];
    }
    
    if(dictionary[@"lastAvatarUpdateDate"] && [dictionary[@"lastAvatarUpdateDate"] isKindOfClass:[NSString class]]) {
        if(contact.rainbow.userPhoto){
            [contact.rainbow.userPhoto updateUserPhotoWith:[[UserPhoto alloc] initWithLastUpdateDate:[NSDate dateFromJSONString:dictionary[@"lastAvatarUpdateDate"]]]];
        } else {
            contact.rainbow.userPhoto = [[UserPhoto alloc] initWithLastUpdateDate:[NSDate dateFromJSONString:dictionary[@"lastAvatarUpdateDate"]]];
        }
    }
    
    if(dictionary[@"lastUpdateDate"] && [dictionary[@"lastUpdateDate"] isKindOfClass:[NSString class]]){
        contact.rainbow.lastUpdateDate = [NSDate dateFromJSONString:dictionary[@"lastUpdateDate"]];
    }
}

+(Presence *) presenceFromXMPPPresence:(XMPPPresence *) presence {
    NSLog(@"Presence intValue %d is %@ ", [presence intShow], presence);
    ContactPresence contactPresence = ContactPresenceUnavailable;
    if(presence){
        switch ([presence intShow]) {
            case -1: // Unavailable
                contactPresence = ContactPresenceUnavailable;
                break;
            case 0: // DND
                contactPresence = ContactPresenceDoNotDisturb;
                break;
            case 1: // Extanded away
                contactPresence = ContactPresenceInvisible;
                break;
            case 2: // away
                contactPresence = ContactPresenceAway;
                break;
            default:
            case 3: // Available
                contactPresence = ContactPresenceAvailable;
                break;
        }
    }
    
    Presence *thePresence = [Presence new];
    thePresence.presence = contactPresence;
    if([presence status].length > 0)
        thePresence.status = [presence status];
    
    return thePresence;
}

@end
