/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ContactsManagerService.h"
#import "ContactsManagerService+Internal.h"
#import "LocalContacts.h"
#import "LocalContactsToolkit.h"
#import "ContactInternal.h"
#import "PresenceInternal.h"
#import "CalendarPresenceInternal.h"
#import "MyUser+Internal.h"
#import "Tools.h"
#import "NSDictionary+JSONString.h"
#import "NSData+JSON.h"
#import "ContactsManagerServiceToolKit.h"
#import "NSXMLElement+XEP_0203.h"
#import "NSDate+Utilities.h"
#import "NSDate+JSONString.h"
#import "NSDate+Equallity.h"
#import "NSString+FileSize.h"
#import "NSDictionary+ChangedKeys.h"
#import "Invitation+Internal.h"
#import "LoginManager+Internal.h"
#import "XMPPDateTimeProfiles.h"
#import "PhoneNumberInternal.h"
#import "NSString+MD5.h"
#import "NSDate+JSONString.h"
#import "ExternalContact.h"
#import "PINCache.h"
#import "NSObject+NotNull.h"
#import "PINCache+OperationQueue.h"

NSString *const kFakeKeyPrefix = @"fake-";

NSString *const kContactKey = @"contact";
NSString *const kChangedAttributesKey = @"attributes";
NSString *const kContactsManagerServiceDidEndPopulatingMyNetwork = @"didEndPopulatingMyNetwork";
NSString *const kContactsManagerServiceDidAddContact = @"didAddContact";
NSString *const kContactsManagerServiceDidUpdateContact = @"didUpdateContact";
NSString *const kContactsManagerServiceDidRemoveContact = @"didRemoveContact";
NSString *const kContactsManagerServiceDidRemoveAllContacts = @"didRemoveAllContacts";
NSString *const kContactsManagerServiceDidInviteContact = @"didInviteContact";
NSString *const kContactsManagerServiceDidFailedToInviteContact = @"didFailedToInviteContact";
NSString *const kContactsManagerServiceDidUpdateMyContact = @"didUpdateMyContact";
NSString *const kContactsManagerServiceDidAcceptInvitation = @"didAcceptInvitation";
NSString *const kContactsManagerServiceDidChangeContactDisplayUserSettings = @"didChangeUserContactDisplaySettings";
NSString *const kContactsManagerServiceLocalAccessGrantedNotification = @"localAccessGranted";
NSString *const kContactsManagerServiceDidReceiveCreateConfUserActivated = @"didReceiveCreateConfUserActivated";
// to be moved in some telephony service.
NSString *const kContactsManagerServiceClickToCallMobile = @"clickToCallMobile";

NSString *const kContactsManagerServiceDidAddInvitation = @"didAddInvitation";
NSString *const kContactsManagerServiceDidUpdateInvitation = @"didUpdateInvitation";
NSString *const kContactsManagerServiceDidUpdateInvitationPendingNumber = @"didUpdateInvitationPendingNumber";
NSString *const kContactsManagerServiceDidRemoveInvitation = @"didRemoveInvitation";

typedef void (^ContactsManagerServiceLoadHiResAvatarInternalCompletionHandler)(NSData *receivedData, NSError *error, NSURLResponse *response);


@interface ContactsManagerService () <XMPPServiceContactsManagerDelegate>
@property (nonatomic, readwrite, strong) NSMutableArray <Contact *> *contacts;
@property (nonatomic, strong) NSObject *contactsMutex;
@property (nonatomic, strong) NSObject *invitationsMutex;
@property (nonatomic, readwrite, strong) NSMutableDictionary <NSString *, Invitation *> *invitations;
// LocalContacts is a Helper class to acces iPhone internal contacts
@property (nonatomic, strong) LocalContacts *localContacts;
@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property (nonatomic, readwrite) BOOL localDeviceAccessAlreadyRequired;
@property (nonatomic, strong) NSMutableArray <Invitation*> *invitationsLoadedFromCache;
@property (nonatomic, strong) PINCache *avatarCache;
@property (nonatomic, strong) PINCache *vcardCache;
@property (nonatomic, strong) PINCache *invitationCache;

@property (nonatomic, strong) NSMutableArray <NSString *> *jidsToFetch;
@property (nonatomic, strong) NSTimer *jidsToFetchTimer;

@property (nonatomic, strong) NSOperationQueue *internalQueue;
@end

@implementation ContactsManagerService

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager myUser:(MyUser *) myUser apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService {
    self = [super init];
    if(self){
        // Internal datamodel & dependencies
        _cacheLoaded = NO;
        _contacts = [NSMutableArray new];
        _contactsMutex = [NSObject new];
        _invitationsMutex = [NSObject new];
        _localContacts = [LocalContacts new];
        _localContacts.contactsManagerService = self;
        [_localContacts addObserver:self forKeyPath:kLocalContactsAccessGrantedKey options:NSKeyValueObservingOptionNew context:nil];
        [_localContacts addObserver:self forKeyPath:kLocalContactsDisplayFirstNameFirstKey options:NSKeyValueObservingOptionNew context:nil];
        [_localContacts addObserver:self forKeyPath:kLocalContactsSortByNameKey options:NSKeyValueObservingOptionNew context:nil];
        
        _invitations = [NSMutableDictionary new];
        _invitationsLoadedFromCache = [NSMutableArray array];
        _jidsToFetch = [NSMutableArray new];
        
        // External dependencies
        _downloadManager = downloadManager;
        _myUser = myUser;
        _apiUrlManagerService = apiUrlManagerService;

        // Login / Logout  and Connection notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willLogin:) name:kLoginManagerInternalWillLogin object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didDisconnect:) name:kLoginManagerDidDisconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        
        _avatarCache = [[PINCache alloc] initWithName:@"avatarCache"];
        _avatarCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        [_avatarCache trimToDateAsync:[NSDate dateWithDaysBeforeNow:30] completion:nil];

        _vcardCache = [[PINCache alloc] initWithName:@"vcardCache"];
        _vcardCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        [_vcardCache trimToDateAsync:[NSDate dateWithDaysBeforeNow:30] completion:nil];
        
        
        _invitationCache = [[PINCache alloc] initWithName:@"invitationCache"];
        _invitationCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        
        _internalQueue = [NSOperationQueue new];
        _internalQueue.name = @"ContactsManagerCacheQueue";
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalWillLogin object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidDisconnect object:nil];
    
    _xmppService = nil;
    [_localContacts removeObserver:self forKeyPath:kLocalContactsAccessGrantedKey context:nil];
    [_localContacts removeObserver:self forKeyPath:kLocalContactsDisplayFirstNameFirstKey];
    [_localContacts removeObserver:self forKeyPath:kLocalContactsSortByNameKey];
    _localContacts.contactsManagerService = nil;
    _localContacts = nil;
    _downloadManager = nil;
    _apiUrlManagerService = nil;
    _myUser = nil;
    _directoryService = nil;
    _xmppService.contactsDelegate = nil;
    _xmppService = nil;
    
    
    
    @synchronized (_invitations) {
        // unset the strong ref to prevent loop reference between
        // Invitation and Contact
        [_invitations enumerateKeysAndObjectsUsingBlock:^(NSString *key, Invitation *invitation, BOOL *stop) {
            invitation.peer = nil;
        }];
        [_invitations removeAllObjects];
        [_invitationsLoadedFromCache removeAllObjects];
    }
    _invitationsLoadedFromCache = nil;
    _invitations = nil;
    
    @synchronized(_contactsMutex) {
        [((NSMutableArray *)_contacts) removeAllObjects];
        _contacts = nil;
    }
    
    @synchronized(_jidsToFetch) {
        [_jidsToFetch removeAllObjects];
        _jidsToFetch = nil;
    }
    
    if(_jidsToFetchTimer) {
        [_jidsToFetchTimer invalidate];
        _jidsToFetchTimer = nil;
    }
    
    _contactsMutex = nil;
    _invitationsMutex = nil;
    _avatarCache = nil;
    _vcardCache = nil;
}

-(void) setXmppService:(XMPPService *)xmppService {
    _xmppService = xmppService;
    _xmppService.contactsDelegate = self;
}

#pragma mark - Login/Logout notifications

-(void) didLogin:(NSNotification *) notification {
    
    [self fetchInvitations];
    [self fetchRemoteContactDetail:_myContact];
}

-(void) didLogout:(NSNotification *) notification {
    @synchronized(_contactsMutex) {
        if(_myContact)
            [self deleteMyContact];
        
        [((NSMutableArray *)_contacts) removeAllObjects];
    }
     [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidRemoveAllContacts object:nil];
    @synchronized (_invitations) {
        // clean all invitations
        [_invitations removeAllObjects];
    }
    [_localContacts stop];
    
    [_avatarCache.operationQueue cancelAllOperations];
    [_vcardCache.operationQueue cancelAllOperations];
}

-(void) didChangeUser:(NSNotification *) notification {
    [_avatarCache removeAllObjects];
    [_vcardCache removeAllObjects];
    [_invitationCache removeAllObjects];
}

-(void) willLogin:(NSNotification *) notification {
    @synchronized (_invitationsMutex) {
        
        if (!_cacheLoaded) {
            [self loadInvitationsFromCache];
            _cacheLoaded = NO;
        }
    }

    [_localContacts start];
}

-(void) didEndPopulatingRoster {
    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidEndPopulatingMyNetwork object:nil];
}

#pragma mark - Local contacts access

-(void) requestAddressBookAccess {
    [_localContacts requestAddressBookAccess];
}

-(BOOL) localDeviceAccessGranted {
    return _localContacts.accessGranted;
}

-(BOOL) localDeviceAccessAlreadyRequired {
    return  [_localContacts getCurrentAddressBookAuthorization] != CNAuthorizationStatusNotDetermined;
}

-(BOOL) displayFirstNameFirst {
    if(self.localDeviceAccessGranted)
        return [_localContacts displayFirstNameFirst];
    else
        return YES;
}

-(BOOL) sortByFirstName {
    if(self.localDeviceAccessGranted)
        return [_localContacts sortByFirstName];
    else
        return NO;
}

-(NSArray *) myNetworkContacts {
    NSMutableArray *contacts = [NSMutableArray array];
    @synchronized (_contactsMutex) {
        [_contacts enumerateObjectsUsingBlock:^(Contact * aContact, NSUInteger idx, BOOL * stop) {
            if(aContact.isRainbowUser && !aContact.isTerminated && aContact.jid != nil && ![aContact isEqual:_myContact] && aContact.isVisible && !aContact.isBot)
                [contacts addObject:aContact];
        }];
    }
    return contacts;
}


#pragma mark - Search for local contacts

-(Contact *) searchLocalContactWithPhoneNumber: (PhoneNumber *) expectedNumber {
    @synchronized (_contactsMutex) {
        for (Contact *contact in _contacts) {
            if ([contact.locals count]) {
                for (LocalContact *localContact in contact.locals) {
                    for (PhoneNumber *phoneNumber in localContact.phoneNumbers) {
                        if ([expectedNumber isEqual:phoneNumber]) {
                            return contact;
                        }
                    }
                }
            }
        }
        return nil;
    }
}

-(Contact *) searchLocalContactWithEmailString:(NSString *)emailString {
    @synchronized (_contactsMutex) {
        EmailAddress *emailAddress = [ContactsManagerServiceToolKit emailAddressFromEmailAddress:emailString type:[EmailAddress typeFromEmailAddressLabel:@"work"]];
        
        return [self searchLocalContactWithEmailAddress:emailAddress];
    }
}

-(Contact *) searchLocalContactWithEmailAddress: (EmailAddress *)anEmailAddress {
    @synchronized (_contactsMutex) {
        for (Contact *contact in _contacts) {
            if ([contact.locals count]) {
                for (LocalContact *localContact in contact.locals) {
                    for (EmailAddress *email in localContact.emailAddresses) {
                        if ([anEmailAddress isEqual:email]) {
                            return contact;
                        }
                    }
                }
            }
        }
        return nil;
    }
}

-(Contact *) getContactWithAddressBookRecordID:(NSString *) addressBookRecordID {
    @synchronized (_contactsMutex) {
        for (Contact *contact in _contacts) {
            for (LocalContact *localContact in contact.locals) {
                if ([localContact.addressBookRecordID isEqualToString: addressBookRecordID]) {
                    return contact;
                }
            }
        }
        return nil;
    }
}


#pragma mark - Search for rainbow contacts

-(Contact *) searchRainbowContactWithEmailString:(NSString *)emailString {
    @synchronized (_contactsMutex) {
        EmailAddress *emailAddress = [ContactsManagerServiceToolKit emailAddressFromEmailAddress:emailString type:[EmailAddress typeFromEmailAddressLabel:@"work"]];
        
        return [self searchRainbowContactWithEmailAddress:emailAddress];
    }
}

-(Contact *) searchRainbowContactWithEmailAddress:(EmailAddress *)anEmailAddress {
    @synchronized (_contactsMutex) {
        for (Contact *contact in _contacts) {
            if (contact.rainbow) {
                for (EmailAddress *email in contact.rainbow.emailAddresses) {
                    if ([anEmailAddress isEqual:email]) {
                        return contact;
                    }
                }
            }
        }
        return nil;
    }
}

-(Contact *) searchRainbowContactWithPhoneNumber:(PhoneNumber *)phoneNumber {
    @synchronized (_contactsMutex) {
        for (Contact *contact in _contacts) {
            if (contact.rainbow) {
                for (PhoneNumber *phone in contact.rainbow.phoneNumbers) {
                    if ([phoneNumber isEqual:phone]) {
                        return contact;
                    }
                }
            }
        }
        return nil;
    }
}


#pragma mark - add/update/delete emitted notifications

-(void) didAddContact:(Contact *) contact {
    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidAddContact object:contact];
    [_delegate contactsService:self didAddContact:contact];
}

-(void) didUpdateContact:(Contact *) contact withChangedKeys:(NSArray *) changedKeys {
    // Here comes the magic : Don't trigger update if nothing has changed.
    if (!contact || !changedKeys || [changedKeys count] == 0) {
        return;
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        if([contact isEqual:_myContact]){
            [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateMyContact object:@{kContactKey: _myContact, kChangedAttributesKey: changedKeys}];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateContact object:@{kContactKey: contact, kChangedAttributesKey: changedKeys}];
            [_delegate contactsService:self didUpdateContact:contact withChangedKeys:changedKeys];
        }
    });
}

-(void) didRemoveContact:(Contact *) contact {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidRemoveContact object:contact];
        [_delegate contactsService:self didRemoveContact:contact];
    });
}

// Same for local contacts
//-(void) didAddLocalContact:(LocalContact *) contact {
//    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidAddLocalContact object:contact];
//}
//
//-(void) didUpdateLocalContact:(LocalContact *) contact {
//    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateLocalContact object:contact];
//}
//
//-(void) didRemoveLocalContact:(LocalContact *) contact {
//    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidRemoveLocalContact object:contact];
//}

#pragma mark - Search for remote rainbow contacts on server
/*
 * For all our "local-only" contacts, ask the server if they exist in Rainbow.
 */
-(void) searchForRainbowUserInLocalContacts {
    @synchronized (_contactsMutex) {
        
        // Prepare a set of all the "local-only" email addresses.
        NSMutableSet<NSString*> *emails = [NSMutableSet set];
        for (Contact *contact in _contacts) {
            // We search for local-only contacts
            if (!contact.rainbow) {
                for (EmailAddress *email in contact.emailAddresses) {
                    [emails addObject:email.address];
                }
            }
        }
        
        NSArray<NSString*> *emailsArray = [emails allObjects];
        if(emailsArray.count > 0){
            NSUInteger limit = 100;
            for (NSUInteger offset = 0; offset <= [emailsArray count]; offset += limit) {
                // We emit multiple time the same request but with different offset.
                [_directoryService searchRainbowContactsWithLoginEmails:emailsArray fromOffset:offset withLimit:limit withCompletionBlock:^(NSString *searchPattern, NSArray *response, NSError *error) {
                    if(!error && response.count > 0){
                        for (NSDictionary *userDict in response) {
                            
                            // Ignore we are about to make a match with myself.
                            if ([userDict[@"jid_im"] isEqualToString:_myUser.contact.jid])
                                continue;
                            
                            // Get the right contact.
                            [self createOrUpdateRainbowContactFromJSON:userDict];
                            
                            // Then try to merge it with a local contact.
//                            [self mergeOrUnmergeContact:contact];
                        }
                    }
                }];
            }
        }
    } // @synchronized
}


#pragma mark - Some getter

-(Contact *) getContactWithRainbowID:(NSString *) rainbowID {
    @synchronized(_contactsMutex) {
        for (Contact *contact in _contacts) {
            if([contact.rainbowID isEqualToString:rainbowID]){
                return contact;
            }
        }
        return nil;
    }
}

-(Contact *) getContactWithJid:(NSString *) jid {
    @synchronized(_contactsMutex) {
        NSString *jidWithoutDomain = [self getUserWithoutDomainFromBareJid:jid];
        
        for (Contact *contact in _contacts) {
            if ([contact.jid isEqualToString:jid] || [jidWithoutDomain isEqualToString:[self getUserWithoutDomainFromBareJid:contact.jid]]) {
                return contact;
            }
        }
        return nil;
    }
}

-(Contact *) getSupportBot {
    __block Contact *supportBot = nil;
    @synchronized (_contactsMutex) {
        [_contacts enumerateObjectsUsingBlock:^(Contact * aContact, NSUInteger idx, BOOL * stop) {
            if(aContact.isBot){
                supportBot = aContact;
                *stop = YES;
            }
                
        }];
    }
    return supportBot;
}

#pragma mark - Kvo on system contacts

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(Contact *) contact change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if([keyPath isEqualToString:kLocalContactsDisplayFirstNameFirstKey] || [keyPath isEqualToString:kLocalContactsSortByNameKey]){
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
    }
    if ([keyPath isEqualToString:kLocalContactsAccessGrantedKey]) {
        // TODO: do something
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceLocalAccessGrantedNotification object:nil];
        return;
    }
}


#pragma mark - Search

-(NSArray<Contact *> *) searchContactsWithPattern:(NSString *) pattern {
    NSString *trimmedPattern = [pattern stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSMutableArray *subPredicates = [NSMutableArray array];
    NSArray *terms = [trimmedPattern componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    for (NSString *term in terms) {
        if(term.length == 0)
            continue;
        
        
        NSString *theTerm = [term stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
        
        // verification failed because of that.
        NSString *goodTerm = [theTerm stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        // Avoid bad regex by escaping any special character, for example '+' becomes '//+' but 'A' does not change
        NSString *escapedPattern = [NSRegularExpression escapedPatternForString:goodTerm];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"((firstName BEGINSWITH[cd] %@) OR (lastName MATCHES[cd] %@)) AND isTerminated != YES", goodTerm, [NSString stringWithFormat:@"(?w:).*\\b%@.*", escapedPattern]];
        [subPredicates addObject:predicate];
    }

    NSMutableSet<Contact *> *result = [NSMutableSet set];
    @synchronized (_contactsMutex) {
        NSPredicate *filter = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
        [result addObjectsFromArray:[_contacts filteredArrayUsingPredicate:filter]];
    }
    
    return [result allObjects];
}

-(NSArray<Contact *> *) searchAdressBookWithPattern:(NSString *) pattern {
    NSMutableSet<Contact *> *result = [NSMutableSet set];
    
    NSError *error;
    CNContactStore *store = [[CNContactStore alloc]init];
    NSArray<CNContact*> *contacts = [store unifiedContactsMatchingPredicate:[CNContact predicateForContactsMatchingName:pattern] keysToFetch: @[CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPostalAddressesKey, CNContactJobTitleKey, CNContactThumbnailImageDataKey, CNContactIdentifierKey, CNContactOrganizationNameKey, CNContactUrlAddressesKey, CNContactImageDataAvailableKey] error:&error];
    
    if(error)
        return [result allObjects];
        
    for (CNContact *person in contacts) {
        __block Contact *contact = [ContactsManagerService createContactFromCNContact:person];
        
        // Search for invitation (by email OR phone number) for this CNContact
        [_invitations enumerateKeysAndObjectsUsingBlock:^(NSString *_Nonnull key, Invitation *_Nonnull invitation, BOOL *_Nonnull stopAll) {
            if(invitation.direction == InvitationDirectionSent) {
                // By email
                [contact.emailAddresses enumerateObjectsUsingBlock:^(EmailAddress *_Nonnull email, NSUInteger idx, BOOL *_Nonnull stopEmails) {
                    if([email.address isEqual:invitation.email]) {
                        contact.rainbow = [RainbowContact new];
                        contact.rainbow.sentInvitation = invitation;
                        *stopAll = YES;
                    }
                }];
                // By phone number
                [contact.phoneNumbers enumerateObjectsUsingBlock:^(PhoneNumber *_Nonnull phone, NSUInteger idx, BOOL *_Nonnull stopPhones) {
                    if([phone.number isEqual:invitation.phoneNumber]) {
                        contact.rainbow = [RainbowContact new];
                        contact.rainbow.sentInvitation = invitation;
                        *stopAll = YES;
                    }
                }];
            }
        }];
        
        [result addObject:contact];
    }
    
    return [result allObjects];
}

-(void) searchRemoteContactsWithPattern:(NSString *) pattern withCompletionHandler:(ContactsManagerServiceSearchRemoteContactsCompletionHandler) completionHandler {

    dispatch_group_t searchGroup = dispatch_group_create();
    dispatch_group_enter(searchGroup);
    NSMutableArray<Contact *> * foundRainbowContacts = [NSMutableArray new];
    NSMutableArray<Contact *> * foundADContacts = [NSMutableArray new];
    NSMutableArray<Contact *> * foundPBXContacts = [NSMutableArray new];
    NSLog(@"Search on rainbow");
    [_directoryService searchRequestWithPattern:pattern withCompletionBlock:^(NSString *searchedPattern, NSArray *result, NSError *error) {
        NSLog(@"Search on rainbow done");
        for (NSDictionary *userDict in result) {
            // Ignore myself
            if([userDict[@"jid_im"] isEqual:_myUser.contact.jid])
                continue;
            
            // If this user exists in our list of contact, use it.
            Contact *contact = [self getContactWithJid:userDict[@"jid_im"]];
            
            // Or create a temp-contact.
            if (!contact) {
                contact = [ContactsManagerServiceToolKit createRainbowContactFromDictionary:userDict];
            } else {
                [self updateRainbowContact:contact withJSONDictionary:userDict];
            }
            if (!contact.isTerminated) {
                [foundRainbowContacts addObject:contact];
            }
        }
        dispatch_group_leave(searchGroup);
    }];
    
    if(_myUser.isAllowedToSearchInActiveDirectory && _myUser.isADSearchAvailable){
        dispatch_group_enter(searchGroup);
        NSLog(@"Searching on Active Directory");
        [_directoryService searchRequestOnActiveDirectoryWithPattern:pattern withCompletionBlock:^(NSString *searchedPattern, NSArray *response, NSError *error) {
            NSLog(@"Search on Active Directory done");
            for (NSDictionary *userDict in response) {
                Contact *contact = [Contact new];
                ExternalContact *externalContact = [ExternalContact new];
                [ExternalContact fillExternalContact:externalContact withJsonDictionary:userDict];
                [contact addLocalContact:externalContact];
                
                [foundADContacts addObject:contact];
            }
            dispatch_group_leave(searchGroup);
        }];
    }
    
    if(_myUser.isAllowedToSearchInPBXPhonebook){
        dispatch_group_enter(searchGroup);
        NSLog(@"Searching on PBX phonebook");
        [_directoryService searchRequestOnPBXPhonebookWithPattern:pattern withCompletionBlock:^(NSString *searchedPattern, NSArray *response, NSError *error) {
            NSLog(@"Search on PBX phonebook done");
            for (NSDictionary *userDict in response) {
                Contact *contact = [Contact new];
                ExternalContact *externalContact = [ExternalContact new];
                externalContact.isPBXExternalContact = YES;
                [ExternalContact fillExternalContact:externalContact withJsonDictionary:userDict];
                [contact addLocalContact:externalContact];
                
                NSLog(@"Adding contact for PBX search %@", contact);
                [foundPBXContacts addObject:contact];
            }
            dispatch_group_leave(searchGroup);
        }];
    }
    
    dispatch_group_notify(searchGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableArray *foundContactsMerged = [NSMutableArray array];
        // Active Directory contact
        if(foundADContacts.count > 0){
            NSMutableArray<Contact *> *foundContacts = [NSMutableArray arrayWithArray:foundRainbowContacts];
            [foundContacts addObjectsFromArray:foundADContacts];
            NSArray *distinct = [foundContacts valueForKeyPath:@"@distinctUnionOfObjects.emailAddresses"];
            [distinct enumerateObjectsUsingBlock:^(NSArray<EmailAddress *> *anEmailAddresses, NSUInteger idx, BOOL *stop) {
                NSArray *filteredContacts = [foundContacts filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(Contact *aContact, NSDictionary *bindings) {
                    if(anEmailAddresses.count > 0)
                        return [aContact.emailAddresses containsObject:[anEmailAddresses firstObject]];
                    else
                        return YES;
                }]];
                if (filteredContacts.count > 0)
                    [foundContactsMerged addObject:filteredContacts.lastObject];
            }];
        } else {
            [foundContactsMerged addObjectsFromArray:foundRainbowContacts];
        }
        
        // PBX contact
        if(foundPBXContacts.count > 0){
            NSMutableArray<Contact *> *foundContacts = [NSMutableArray arrayWithArray:foundRainbowContacts];
            [foundContacts addObjectsFromArray:foundPBXContacts];
            NSArray *distinct = [foundContacts valueForKeyPath:@"@distinctUnionOfObjects.firstName"];
//            [distinct enumerateObjectsUsingBlock:^(NSArray<EmailAddress *> *anEmailAddresses, NSUInteger idx, BOOL *stop) {
//                NSArray *filteredContacts = [foundContacts filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(Contact *aContact, NSDictionary *bindings) {
//                    if(anEmailAddresses.count > 0)
//                        return [aContact.emailAddresses containsObject:[anEmailAddresses firstObject]];
//                    else
//                        return YES;
//                }]];
//                if (filteredContacts.count > 0)
//                    [foundContactsMerged addObject:filteredContacts.lastObject];
//            }];
            [foundContactsMerged addObjectsFromArray:foundPBXContacts];
        }
        
        NSLog(@"Invoke completionHandler %@", foundContactsMerged);
        if(completionHandler)
            completionHandler(pattern, foundContactsMerged);
    });
}

-(void) fetchRemoteContactDetail:(Contact *) theContact {
    if(theContact.isExternalContact){
        NSLog(@"Searching for contact details");
        [_directoryService searchExternalContactDetailsWithObjectID:theContact.externalObjectID withCompletionBlock:^(NSString *searchedPattern, NSArray *response, NSError *error) {
            NSLog(@"Searching for contact details, return data");
            if(!error){
                ExternalContact *externalContact = (ExternalContact*)[theContact.locals firstObject];
                NSDictionary *currentContactInfo = [theContact dictionaryRepresentation];
                
                [externalContact removeAllPhoneNumbers];
                [externalContact removeAllEmailAddresses];
                [externalContact.addresses removeAllObjects];
                
                [ExternalContact fillExternalContact:externalContact withJsonDictionary:[response firstObject]];
                NSArray<NSString *> *changedKeys = [currentContactInfo changedKeysIn:[theContact dictionaryRepresentation]];
                [self didUpdateContact:theContact withChangedKeys:changedKeys];
            }
        }];
    } else {
        if(!theContact.rainbow)
            return;
        
        [_directoryService searchRainbowContactWithRainbowID:theContact.rainbowID withCompletionBlock:^(NSString *searchedPattern, NSArray *response, NSError *error) {
            if(!error){
                [self updateRainbowContact:theContact withJSONDictionary:[response firstObject]];
            }
        }];
    }
}

-(void) fetchCalendarAutomaticReply:(Contact *) theContact {
    if(!theContact.rainbow)
        return;
    
    NSDate *lastUpdateDate = theContact.rainbow.calendarPresence.automaticReply.lastUpdateDate;
    
    if( lastUpdateDate != nil ) {
        NSDate *needUpdateDate = [NSDate date];
        NSDate *maxDate = [lastUpdateDate dateByAddingTimeInterval:(15*60)]; // 15 minutes between each request
        
        if( [maxDate compare:needUpdateDate] == NSOrderedDescending ) {
            NSLog(@"Get calendar automatic reply message not needed");
            return;
        }
    }

    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesCalendarAutomaticReply, theContact.jid];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesCalendarAutomaticReply];

    NSLog(@"Get calendar automatic reply message");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get calendar automatic reply message in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get calendar automatic reply message due to JSON parsing failure");
            return;
        }
        
        NSNumber *enabled = (NSNumber *)[jsonResponse objectForKey:@"enabled"];
        NSString *message_text = [jsonResponse objectForKey:@"message_text"];
        NSString *end = [jsonResponse objectForKey:@"end"];
        
        //
        theContact.calendarPresence.automaticReply.untilDate = [NSDate dateFromJSONString:(NSString *) end];
        theContact.calendarPresence.automaticReply.isEnabled = enabled.boolValue;
        theContact.calendarPresence.automaticReply.message = message_text;
        theContact.calendarPresence.automaticReply.lastUpdateDate = [NSDate date];
        
        [self didUpdateContact:theContact withChangedKeys:@[@"calendarPresence"]];
    }];
    
}

-(void) searchRemoteContactWithJid:(NSString *) contactJid withCompletionHandler:(ContactsManagerServiceSearchRemoteContactCompletionHandler) completionHandler {
    
    Contact *theContact = [self getContactWithJid:contactJid];
    
    if (theContact) {
        NSLog(@"Invoke completionHandler %@", theContact);
        if(completionHandler)
            completionHandler(theContact);
    } else {
        [_directoryService searchRainbowContactWithJid:contactJid withCompletionBlock:^(NSString *contactJid, NSArray *response, NSError *error) {
            if(!error){
                if (!response || response.count == 0) {
                    NSLog(@"searchRemoteContactWithJid return empty response : %@", response);
                    if(completionHandler)
                        completionHandler(nil);
                } else {
                    for (NSDictionary *contactDic in response) {
                        Contact *theNewContact = [self createOrUpdateRainbowContactWithJid:contactJid];
                        BOOL shouldSaveVcardInCache = [self shouldSaveVcardInCache:contactDic forContact:theNewContact];
                        BOOL shouldDownloadNewContactAvatar = [self shouldDownloadNewContactAvatar:contactDic forContact:theNewContact];
                        // We update the contact instead of created a new one to trigger the good updates(vcardPopulated etc...)
                        [self updateRainbowContact:theNewContact withJSONDictionary:contactDic];
                        
                        if(shouldSaveVcardInCache){
                            [self saveContactDictionaryInCache:contactDic forContact:theNewContact];
                        }
                        
                        if(shouldDownloadNewContactAvatar){
                            NSLog(@"Fetching new photo for %@", theNewContact);
                            [self populateAvatarForRainbowContact:theContact forceReload:YES];
                        }
                        
                        NSLog(@"Invoke completionHandler %@", theNewContact);
                        if(completionHandler)
                            completionHandler(theNewContact);
                    }
                }
            } else {
                NSLog(@"searchRemoteContactWithJid return with error %@", error);
                if(completionHandler)
                    completionHandler(nil);
            }
        }];
    }
}



#pragma mark - Invitation management
-(void) didReconnect:(NSNotification *) notification {
    // Do not fetch invitations on reconnect it not needed
    [self resyncFakeInvitations];
}

-(void) didDisconnect:(NSNotification *) notification {
   
}

-(void) loadInvitationsFromCache {
    
    NSMutableArray<NSString*> *existingKey = [NSMutableArray new];
    [_invitationCache.diskCache enumerateObjectsWithBlockAsync:^(NSString * _Nonnull key, NSURL * _Nullable fileURL, BOOL * _Nonnull stop) {
        [existingKey addObject:key];
    } completionBlock:^(id<PINCaching>  _Nonnull cache) {
        for (NSString* key in existingKey) {
            NSDictionary *object = [_invitationCache objectForKey:key];
            @synchronized (_invitations) {
                InvitationDirection direction = [Invitation invitationDirectionFromString:[object objectForKey:@"direction"]];
                
                Invitation *invitation = [self createOrUpdateInvitation:direction fromDictionary:object];
                invitation.invitationID = key;
                
                if(![_invitationsLoadedFromCache containsObject:invitation])
                    [_invitationsLoadedFromCache addObject:invitation];
                
                //                    NSLog(@"Invitation from cache %@ dic %@", invitation, object);
                if(!invitation.peer){
                    NSString *peerJid = [object objectForKey:@"peerJid"];
                    if(peerJid){
                        NSString *cachedJsonRepresentation = [_vcardCache objectForKey:peerJid];
                        NSMutableDictionary *cachedRepresentation = [NSMutableDictionary dictionaryWithDictionary:[[cachedJsonRepresentation dataUsingEncoding:NSUTF8StringEncoding] objectFromJSONData] ];
                        [cachedRepresentation setObject:key forKey:@"jid_im"];
                        Contact * contact = [self createOrUpdateRainbowContactFromJSON:cachedRepresentation];
                        invitation.peer = contact;
                        if(invitation.direction == InvitationDirectionSent){
                            contact.rainbow.sentInvitation = invitation;
                            [self didUpdateContact:contact withChangedKeys:@[@"sentInvitation"]];
                        } else {
                            contact.rainbow.requestedInvitation = invitation;
                            [self didUpdateContact:contact withChangedKeys:@[@"requestedInvitation"]];
                        }
                        [_invitationCache setObjectAsync:[invitation dictionaryRepresentation] forKey:invitation.invitationID completion:nil];
                    }
                }
                
            }
        }
    }];
}

-(void) fetchInvitations {
    // Make a REST get the pending invitations I received.
    NSURL *url_rcvd = [_apiUrlManagerService getURLForService:ApiServicesReceivedInvitations];
    NSURL *url_rcvd_pending = [NSURL URLWithString:[NSString stringWithFormat:@"%@?status=pending&format=medium", url_rcvd]];
    NSDictionary *headers_rcvd = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesReceivedInvitations];
    
    NSLog(@"Get pending received invitations list");
    [_downloadManager getRequestWithURL:url_rcvd_pending requestHeadersParameter:headers_rcvd completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get pending received invitations in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get pending received invitations in REST due to JSON parsing failure");
            return;
        }
        NSMutableArray *invitationsFromServer = [NSMutableArray new];
        NSDictionary *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get pending received invitations list, response ...");
        for (NSDictionary *invitationDict in data) {
            Invitation *invitation = [self createOrUpdateInvitation:InvitationDirectionReceived fromDictionary:invitationDict];
            [invitationsFromServer addObject:invitation];
        }
        
        [_invitationsLoadedFromCache enumerateObjectsUsingBlock:^(Invitation * cachedInvitation, NSUInteger idx, BOOL * stop) {
            if(![invitationsFromServer containsObject:cachedInvitation]){
                NSLog(@"The invitation has been removed %@", cachedInvitation);
                @synchronized (_invitations) {
                    if (cachedInvitation.direction != InvitationDirectionSent) {
                        [_invitationCache removeObjectForKeyAsync:cachedInvitation.invitationID completion:nil];
                        [_invitations removeObjectForKey:cachedInvitation.invitationID];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidRemoveInvitation object:cachedInvitation];
                    }
                }
            }
        }];
        [self resyncFakeInvitations];
        [_invitationsLoadedFromCache removeAllObjects];
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitationPendingNumber object:nil];
    }];
    
    // Make a REST get the invitations I sent which are still pending or has failed
    NSURL *url_sent = [_apiUrlManagerService getURLForService:ApiServicesSentInvitations];
    NSURL *url_sent_pending = [NSURL URLWithString:[NSString stringWithFormat:@"%@?status=pending&status=failed&format=medium", url_sent]];
    NSDictionary *headers_sent = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesSentInvitations];
    
    NSLog(@"Get pending sent invitations list");
    [_downloadManager getRequestWithURL:url_sent_pending requestHeadersParameter:headers_sent completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get pending sent invitations in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get pending sent invitations in REST due to JSON parsing failure");
            return;
        }
        
        NSDictionary *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get pending sent invitations list, response ...");
        for (NSDictionary *invitationDict in data) {
            [self createOrUpdateInvitation:InvitationDirectionSent fromDictionary:invitationDict];
        }
    }];
}

-(void) resyncFakeInvitations {
    NSLog(@"Resynchronizing fake invitations");
    @synchronized(_invitationsMutex){
        [_invitations enumerateKeysAndObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSString *key, Invitation *anInvitation, BOOL *stop) {
            if([key hasPrefix:kFakeKeyPrefix]){
                Contact *contactToInvite = (Contact *)anInvitation.peer;
                [self inviteContact:contactToInvite];
                [_invitations removeObjectForKey:key];
                [_invitationCache removeObjectForKeyAsync:key completion:nil];
            }
        }];
    }
}


-(void) didFailedToInvite:(Contact *) contact reason:(NSString *) reason {
    if (!contact)
        return;
    reason = reason ? reason : @"";
    NSDictionary *infos = @{@"contact": contact, @"reason": reason};
    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidFailedToInviteContact object:infos];
}

-(void) inviteContact:(Contact *) contact {
    
    if (contact.sentInvitation) {
        // If contact is already accepted/autoaccepted, ignore.
        if (contact.sentInvitation.status == InvitationStatusAccepted || contact.sentInvitation.status == InvitationStatusAutoAccepted) {
            NSLog(@"Cannot invite already (auto)accepted contact");
            [self didFailedToInvite:contact reason:NSLocalizedString(@"Cannot reinvite auto-accepted invitations", nil)];
            return;
        }
        
        // We can re-invite the contact
        [self resendInvitationToContact:contact];
        
    } else if (contact.rainbowID) {
        // if contact have a rainbowID, invite by rainbowID
        [self inviteContactByRainbowID:contact];
        
    } else {
        // Looking for contact email address to use
        if (contact.emailAddresses.count > 0) {
            EmailAddress *emailAddress = [contact getEmailAdressOfType:EmailAddressTypeWork];
            if(!emailAddress)
                emailAddress = [contact getEmailAdressOfType:EmailAddressTypeHome];
            if(!emailAddress)
                emailAddress = contact.emailAddresses[0];
            
            [self inviteContact:contact withEmailAddress:emailAddress withCompletionHandler:nil];
        } else {
            NSLog(@"No rainbowID and no usable email address to use. Cannot invite contact.");
            [self didFailedToInvite:contact reason:NSLocalizedString(@"No email address", nil)];
        }
    }
}

-(void) inviteCNContacts:(nonnull NSArray<CNContact*> *) contacts withCompletionHandler:(void(^)(BOOL success))completionHandler {
    __block NSMutableArray *emails = [NSMutableArray new];
    
    [contacts enumerateObjectsUsingBlock:^(CNContact *cncontact, NSUInteger idx, BOOL * stop) {
        if(cncontact.emailAddresses.count > 0) {
            NSString *selectedEmail = @"";
            
            // Loop through emails. First select Home if none. Then, try to find a Work and stop to the first one found.
            for( CNLabeledValue *email in cncontact.emailAddresses) {
                if([email.label isEqualToString: CNLabelHome] && [selectedEmail isEqualToString:@""]) {
                    selectedEmail = email.value;
                }
                if([email.label isEqualToString: CNLabelWork]) {
                    selectedEmail = email.value;
                    break; // One Work email found.
                }
            }
            
            if(![selectedEmail isEqualToString:@""])
                [emails addObject:selectedEmail];
        }
    }];

    if(emails.count == 0) {
        completionHandler(NO);
    } else {
        [self inviteByEmails:emails withCompletionHandler:completionHandler];
    }
}

-(void) inviteByEmails:(nonnull NSArray<NSString *> *) emails withCompletionHandler:(void(^)(BOOL success))completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesInviteBulk];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesInviteBulk];

    __block BOOL endOfList = NO;
    __block NSMutableArray *emailsBulk = [NSMutableArray new];
    __block NSMutableDictionary *bodyContent = [NSMutableDictionary new];
    [bodyContent setValue:[[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode] forKey:@"lang"];
    
    [emails enumerateObjectsUsingBlock:^(NSString *email, NSUInteger idx, BOOL * stop) {
        // Add this email to current set
        [emailsBulk addObject:email];
        
        endOfList = idx+1 == emails.count;
        
        // End of list or max number of emails in bulk reach.
        if(emailsBulk.count == 100 || endOfList ) {
            
            // Add this list of emails to the request body
            [bodyContent setValue:emailsBulk forKey:@"emails"];
            
            // Execute the request
            [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:YES] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
                
                // Notify end of operation
                if(endOfList) {
                    completionHandler(YES);
                }
            }];
            
            // clear current bulk to next time
            [emailsBulk removeAllObjects];
        }
    }];
}

-(void) inviteContactByRainbowID:(Contact *) contact {
    // NB:
    // This will also add the jid_tel to our roster if the user accept.
    // this is "Add to my network"
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesInvite];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesInvite];
    NSDictionary *bodyContent = @{@"invitedUserId": contact.rainbowID, @"lang": [[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode]};
    
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:YES] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"Error: Cannot invite by RainbowID in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            NSString *errorReason;
            if(error.code == 409)
                errorReason = NSLocalizedString(@"A user invitation has already been sent", nil);
            
            [self didFailedToInvite:contact reason:errorReason];

            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"Error: Cannot invite by RainbowID in REST due to JSON parsing failure");
            [self didFailedToInvite:contact reason:NSLocalizedString(@"Response parsing error", nil)];
            return;
        }
        
        if (jsonResponse[@"errorCode"]) {
            NSString *reason = [NSString stringWithFormat:@"Error %@", jsonResponse[@"errorCode"]];
            [self didFailedToInvite:contact reason:reason];
            
        } else if (jsonResponse[@"data"]) {
            NSDictionary *invitationDict = [jsonResponse objectForKey:@"data"];
            NSLog(@"Invitation %@ of invite by RainbowID in REST", invitationDict);
            
            // Update the invitation (at least the status will become declined)
            Invitation *invitation = [self createOrUpdateInvitation:InvitationDirectionSent fromDictionary:invitationDict];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidInviteContact object:(Contact *)invitation.peer];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitation object:invitation];
        }
        else if (error.code == NSURLErrorNotConnectedToInternet){
            // offline Mode ...
            NSMutableDictionary *invitationDict = [[NSMutableDictionary alloc]init];
            [invitationDict setObject:_myUser.contact.rainbowID forKey:@"invitingUserId"];
            EmailAddress * invitingEmailAddress = (EmailAddress *)[_myUser.contact.emailAddresses firstObject];
            [invitationDict setObject:invitingEmailAddress.address forKey:@"invitingUserEmail"];
            [invitationDict setObject:contact.rainbowID forKey:@"invitedUserId"];
            EmailAddress * invitedEmailAddress = (EmailAddress *)[contact.emailAddresses firstObject];
            [invitationDict setObject:invitedEmailAddress.address  forKey:@"invitedUserEmail"];
            [invitationDict setObject:@"pending" forKey:@"status"];
            [invitationDict setObject:[NSString stringWithFormat:@"%@%@",kFakeKeyPrefix,[Tools generateUniqueID]] forKey:@"id"];
         
            Invitation *invitation = [self createOrUpdateInvitation:InvitationDirectionSent fromDictionary:invitationDict];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidInviteContact object:(Contact *)invitation.peer];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitation object:invitation];
        }
    }];
}

-(void) inviteContact:(Contact *) contact withEmailAddress:(EmailAddress *) emailAddress  withCompletionHandler:(void(^)(Invitation *invitation))completionHandler {
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesInvite];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesInvite];
    NSDictionary *bodyContent = @{@"email":emailAddress.address, @"lang": [[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode]};
    
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:YES] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"Error: Cannot invite by email in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            NSString *errorReason;
            if(error.code == 409)
                errorReason = NSLocalizedString(@"A user invitation has already been sent", nil);
            
            if(contact)
                [self didFailedToInvite:contact reason:errorReason];
            else {
                [self didFailedToInvite:[self searchRainbowContactWithEmailAddress: emailAddress] reason:errorReason];

                if(completionHandler)
                    completionHandler(nil);
            }
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"Error: Cannot invite by email in REST due to JSON parsing failure");
            [self didFailedToInvite:contact reason:NSLocalizedString(@"Response parsing error", nil)];
            return;
        }
        NSMutableDictionary *invitationDict;
        if ( error.code == NSURLErrorNotConnectedToInternet) {
            invitationDict = [[NSMutableDictionary alloc]init];
            [invitationDict setObject:_myUser.contact.rainbowID forKey:@"invitingUserId"];
            EmailAddress * invitingEmailAddress = (EmailAddress *)[_myUser.contact.emailAddresses firstObject];
            [invitationDict setObject:invitingEmailAddress.address forKey:@"invitingUserEmail"];
            [invitationDict setObject:emailAddress.address  forKey:@"invitedUserEmail"];
            [invitationDict setObject:@"pending" forKey:@"status"];
            [invitationDict setObject:[NSString stringWithFormat:@"%@%@",kFakeKeyPrefix,[Tools generateUniqueID]] forKey:@"id"];
        }
        else{
            invitationDict = [jsonResponse objectForKey:@"data"];
        }
      
        NSLog(@"Invitation %@ of invite by email in REST", invitationDict);
        
        // Update the invitation (at least the status will become declined)
        Invitation *invitation = [self createOrUpdateInvitation:InvitationDirectionSent fromDictionary:invitationDict];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidInviteContact object:invitation.peer];
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitation object:invitation];
        
        if(completionHandler)
            completionHandler(invitation);
    }];
}

-(void) inviteContact:(Contact *) contact withPhoneNumber:(NSString *) phoneNumber withCompletionHandler:(void(^)(Invitation *invitation))completionHandler {
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesInvite];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesInvite];
    NSDictionary *bodyContent = @{@"invitedPhoneNumber":phoneNumber};
    
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:YES] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot invite by phoneNumber in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            NSString *errorReason;
            if(error.code == 409)
                errorReason = NSLocalizedString(@"A user invitation has already been sent", nil);
            
            [self didFailedToInvite:contact reason:errorReason];
            if(completionHandler)
                completionHandler(nil);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot invite by phoneNumber in REST due to JSON parsing failure");
            [self didFailedToInvite:contact reason:NSLocalizedString(@"Response parsing error", nil)];
            if(completionHandler)
                completionHandler(nil);
            return;
        }
        
        NSDictionary *invitationDict = [jsonResponse objectForKey:@"data"];
        NSLog(@"Invitation %@ of invite by phoneNumber in REST", invitationDict);
        
        // Update the invitation (at least the status will become declined)
        Invitation *invitation = [self createOrUpdateInvitation:InvitationDirectionSent fromDictionary:invitationDict];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitation object:invitation];
        
        if(completionHandler) {
            completionHandler(invitation);
        }
    }];
}

-(void) inviteByEmail:(NSString *_Nonnull) email withCompletionHandler:(void(^)(Invitation * _Nullable invitation))completionHandler {
    [self inviteContact:nil withEmailAddress:[ContactsManagerServiceToolKit emailAddressFromEmailAddress:email type:EmailAddressTypeWork] withCompletionHandler:completionHandler];
}

-(void) inviteByPhoneNumber:(NSString *_Nonnull) phoneNumber withCompletionHandler:(void(^)(Invitation * _Nullable invitation))completionHandler {
    [self inviteContact:nil withPhoneNumber:phoneNumber withCompletionHandler:completionHandler];
}

-(void) resendInvitationToContact:(Contact *) contact {
    NSURL *baseUrl = [_apiUrlManagerService getURLForService:ApiServicesInvite];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/re-send",baseUrl, contact.sentInvitation.invitationID]];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesInvite];
    
    [_downloadManager postRequestWithURL:url body:nil requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *responseData) {
        if(error && error.code != NSURLErrorNotConnectedToInternet){
            NSLog(@"Failed to re-send the invitation, error %@", error);
            NSString *reason;
            if (error.code == 409) {
                reason = NSLocalizedString(@"A user invitation has already been sent", nil);
            }
            
            [self didFailedToInvite:contact reason:reason];
            return;
        }
        
        NSDictionary *response = [receivedData objectFromJSONData];
        if(!response && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"Re-send invitation JSON parsing failed");
            [self didFailedToInvite:contact reason:NSLocalizedString(@"Response parsing error", nil)];
            return;
        }
        
        // Handle error cases
        if (response[@"errorCode"]) {
            NSLog(@"Re-send invitation error : %@", response);
            NSString *reason;
            if ([response[@"errorCode"] intValue] == 409) {
                reason = NSLocalizedString(@"A user invitation has already been sent", nil);
            }
            
            [self didFailedToInvite:contact reason:reason];
            
        } else if (response[@"data"] || error.code == NSURLErrorNotConnectedToInternet) {
            NSLog(@"Re-send invitation success : %@", response[@"data"]);
            [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidInviteContact object:contact];
        }
    }];
}

-(void) deleteInvitationWithID:(Invitation *) invitation {
    NSAssert(![NSThread isMainThread], @"deleteInvitationSentToContact mut *NOT* be invoked on main thread, it perform a synchronous delete request");
    if(!invitation){
        NSLog(@"This contact don't have sentInvitation");
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesInvitationDetails,invitation.invitationID];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesInvitationDetails];
    
    NSData *receivedData = nil;
    NSError *requestError = nil;
    NSURLResponse *response = nil;
    
    BOOL requestSucceed = [_downloadManager deleteSynchronousRequestWithURL:url requestHeadersParameter:headersParameters returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
    
    if(!requestSucceed || requestError) {
        if (requestError.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"Error: Cannot delete invitation in REST : %@ %@", requestError, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
       
    }
    NSDictionary *jsonResponse = [receivedData objectFromJSONData];
    if(!jsonResponse && requestError.code != NSURLErrorNotConnectedToInternet) {
        NSLog(@"Error: Cannot read REST answer due to JSON parsing failure");
        return;
    }
    
    NSLog(@"Did remove invitation");
    // If there is a data that means the delete works.
    
    if(jsonResponse[@"status"] && requestError.code != NSURLErrorNotConnectedToInternet){
        [self removeInvitationWithID:invitation.invitationID];
    }
    else if (requestError.code == NSURLErrorNotConnectedToInternet || requestError.code == 404) {
        // offline mode or not found
        [self removeInvitationWithID:invitation.invitationID];
    }
    else{
        // show error alert
        NSLog(@"Error Can't Cancel Invitation");
    }
   
}


-(void) createOrUpdateInvitation:(InvitationDirection) direction withID:(NSString *) invitationID {
    if (!invitationID) {
        return;
    }
    
    // Make a REST request to get the updated infos of the invitation
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesInvitationDetails, invitationID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesInvitationDetails];
    
    NSLog(@"Get invitation %@ details", invitationID);
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get invitation details in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get invitation details in REST due to JSON parsing failure");
            return;
        }
        
        NSDictionary *invitationDict = [jsonResponse objectForKey:@"data"];
        
        NSLog(@"Got invitations details, response ...");
        
        [self createOrUpdateInvitation:direction fromDictionary:invitationDict];
    }];
}

-(Invitation *) createOrUpdateInvitation:(InvitationDirection) direction fromDictionary:(NSDictionary *) invitationDict {
    
    if(!invitationDict){
        NSLog(@"No invitation dictionary");
        return nil;
    }
    NSString *invitationID = [invitationDict objectForKey:@"id"];
    
    @synchronized (_invitations) {
        
        Invitation *invitation = [_invitations objectForKey:invitationID];
        BOOL newInvitation = NO;
        if (!invitation) {
            invitation = [Invitation new];
            newInvitation = YES;
        }
        
        invitation.invitationID = invitationID;
        invitation.direction = direction;
        invitation.status = [Invitation invitationStatusFromString:[invitationDict objectForKey:@"status"]];
        invitation.invitingUserId = [invitationDict objectForKey:@"invitingUserId"];
        
        // Attention, invitingDate can be :
        // - a string like "2016-09-28T16:31:36.881Z" when info coming from API (most cases)
        id date = [invitationDict objectForKey:@"invitingDate"];
        
        if ([date isKindOfClass:[NSDate class]]) {
            invitation.date = (NSDate *)date;
        } else if ([date isKindOfClass:[NSString class]]) {
            invitation.date = [NSDate dateFromJSONString:(NSString *) date];
        }
        
        if (invitation.direction == InvitationDirectionSent) {
            invitation.email = [invitationDict objectForKey:@"invitedUserEmail"];
            invitation.phoneNumber = [invitationDict objectForKey:@"invitedPhoneNumber"];
            
            // For a sent invitation, the email is always filled, but the rainbowID might not be.
            // For example if we invite a complete unknown person to rainbow.
            // But if we invite somebody 'not visible to us', then the rainbowID is filled.
            NSString *invitedRainbowID = [invitationDict objectForKey:@"invitedUserId"];
            if (invitedRainbowID && [invitedRainbowID length]) {
                
                // If we have the rainbowID of the invited person, this invitation is not anymore pending.
                // remove the fake tmp_invited_XXX contact we might have created while it was pending.
                // Do this before creating the new contact, so the merge will work nicely.
                Contact *remove = [self getContactWithRainbowID:[NSString stringWithFormat:@"tmp_invited_%@", invitation.invitationID]];
                if (remove) {
                    [self deleteRainbowContact:remove];
                }
                
                Contact *contact = [self createOrUpdateRainbowContactWithRainbowId:invitedRainbowID];
                contact.rainbow.sentInvitation = invitation;
                invitation.peer = contact;
                
                [self didUpdateContact:contact withChangedKeys:@[@"sentInvitation"]];
                
                if( invitation.email ) {
                    
                    // Add the invited email address as email of the contact (if not already in)
                    // So it will eventually merge with a local contact.
                    // Because if we have no visibilty on the contact (invitation not yet accepted)
                    // we cannot have its emails
                    NSMutableArray *emailsToUpdate = [NSMutableArray new];
                    BOOL emailAlreadyInContact = NO;
                    for (EmailAddress *email in [contact emailAddresses]) {
                        if ([invitation.email isEqualToString:email.address]) {
                            emailAlreadyInContact = YES;
                            break;
                        }
                        [emailsToUpdate addObject:[email dictionaryRepresentation]];
                    }
                    if (!emailAlreadyInContact) {
                        // Add the invited email
                        [emailsToUpdate addObject:@{@"email": invitation.email}];
                        
                        NSDictionary *dict = @{@"emails": emailsToUpdate};
                        // This will trigger mergeOrUnmerge.
                        [self updateRainbowContact:contact withJSONDictionary:dict];
                    }
//                    else {
//                        // Still merge or unmerge in case of...
//                        [self mergeOrUnmergeContact:contact];
//                    }
                }
                
            } else {
                // Create a fake rainbow contact.
                // use the invitationID as fake rainbowID.
                Contact *contact = [self createOrUpdateRainbowContactWithJid:[NSString stringWithFormat:@"tmp_invited_%@", invitation.invitationID]];
                // We must keep the fake jid of the fake contact, because in case of re-invite (resend) the getContact will not be able to find the contact in the contact cache.
                //                    contact.rainbow.jid = nil;
                contact.rainbow.rainbowID = [NSString stringWithFormat:@"tmp_invited_%@", invitation.invitationID];
                contact.rainbow.sentInvitation = invitation;
                invitation.peer = contact;
                
                NSDictionary *dict;
                
                if( invitation.email ) {
                    dict = @{@"emails": @[@{@"email": invitation.email}]};
                    
                    // This will add the email, and trigger mergeOrUnmerge.
                    [self updateRainbowContact:contact withJSONDictionary:dict];
                }
                
                if( invitation.phoneNumber ) {
                    PhoneNumber *number = [[PhoneNumber alloc] init];
                    number.number = invitation.phoneNumber;
                    NSDictionary *phoneNumberDic = [number dictionaryRepresentation];
                    dict = @{@"phoneNumbers": @[phoneNumberDic]};
                    
                    [self updateRainbowContact:contact withJSONDictionary:dict];
                }
            }
        } else if (invitation.direction == InvitationDirectionReceived) {
            invitation.email = [invitationDict objectForKey:@"invitingUserEmail"];
            Contact *contact = [self createOrUpdateRainbowContactWithRainbowId:[invitationDict objectForKey:@"invitingUserId"]];
            contact.rainbow.requestedInvitation = invitation;
            invitation.peer = contact;
            [self didUpdateContact:contact withChangedKeys:@[@"requestedInvitation"]];
        }
        
        if (newInvitation) {
            [_invitations setObject:invitation forKey:invitationID];
            [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidAddInvitation object:invitation];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitation object:invitation];
        }
        
        if (invitation.direction == InvitationDirectionReceived) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitationPendingNumber object:nil];
        }
        [_invitationCache setObjectAsync:[invitation dictionaryRepresentation] forKey:invitation.invitationID completion:nil];
        
        return invitation;
    }
}

-(void) removeInvitationWithID:(NSString *) invitationID {
    if (!invitationID) {
        return;
    }
    NSLog(@"Remove invitation with id %@", invitationID);
    
    Invitation *invitation = nil;
    @synchronized (_invitations) {
        invitation = [_invitations objectForKey:invitationID];
        [_invitations removeObjectForKey:invitationID];
        [_invitationCache removeObjectForKeyAsync:invitationID completion:nil];
    }
    
    NSLog(@"Send did remove notification for object %@", invitation);
    if(invitation){
        invitation.status = InvitationStatusDeleted;
        // We remove the contact linked to the invitation, it's not really clean but we don't care about it anymore.
        NSLog(@"Deleting contact associated to invitation %@", invitation.peer);
        // We release the invitation we don't keep deleted invitations
        Contact *theContact = (Contact*)invitation.peer;
        theContact.rainbow.sentInvitation = nil;
        if([theContact.jid containsString:@"tmp_invited"])
            [self deleteRainbowContact:(Contact*)invitation.peer];
        
        [self didUpdateContact:theContact withChangedKeys:@[@"sentInvitation"]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidRemoveInvitation object:invitation];
    }
}

-(void) acceptInvitation:(Invitation *) invitation {
    
    if (invitation.direction != InvitationDirectionReceived) {
        NSLog(@"We can accept only received invitations : %@", invitation);
        return;
    }
    
    if (invitation.status != InvitationStatusPending) {
        NSLog(@"We can accept only pending invitations : %@", invitation);
        return;
    }
    
    // received invitation should always have a from contact.
    Contact *contact = (Contact *) invitation.peer;
    NSAssert(contact, @"Received invitation without contact ! %@", invitation);
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesInvitationAccept, invitation.invitationID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesInvitationAccept];
    
    [_downloadManager postRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"Error: Cannot accept invitation in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSMutableDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"Error: Cannot accept invitation in REST due to JSON parsing failure");
            return;
        }
        
        NSDictionary *invitationDict;
        if(error && error.code == NSURLErrorNotConnectedToInternet) { // network connection appear to be offline
            // create fake invitaion
            // save it in the cache 
            invitationDict = [NSMutableDictionary new];
            [invitationDict setValue:invitation.peer.rainbowID forKey:@"invitingUserId"];
            [invitationDict setValue:_myUser.contact.rainbowID forKey:@"invitedUserId"];
            [invitationDict setValue:invitation.email forKey:@"invitingUserEmail"];
            [invitationDict setValue:_myUser.contact.emailAddresses forKey:@"invitedUserEmail"];
            [invitationDict setValue:[NSString stringWithFormat:@"%@",invitation.invitationID] forKey:@"id"];
            [invitationDict setValue:@"accepted" forKey:@"status"];
        }
        else{
             invitationDict = [jsonResponse objectForKey:@"data"];
        }
       
        NSLog(@"Invitation %@ accepted in REST", invitationDict);
        
        // Update the invitation (at least the status will become accepted)
        Invitation *invitation = [self createOrUpdateInvitation:InvitationDirectionReceived fromDictionary:invitationDict];
        [self updateRainbowContactIsInRoster:(Contact*)invitation.peer];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitation object:invitation];
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidAcceptInvitation object:contact];
    }];

    
    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitationPendingNumber object:nil];
}

-(void) declineInvitation:(Invitation *) invitation {
    
    if (invitation.direction != InvitationDirectionReceived) {
        NSLog(@"We can decline only received invitations : %@", invitation);
        return;
    }
    
    if (invitation.status != InvitationStatusPending) {
        NSLog(@"We can decline only pending invitations : %@", invitation);
        return;
    }
    
    // received invitation should always have a from contact.
    Contact *contact = (Contact *) invitation.peer;
    NSAssert(contact, @"Received invitation without contact ! %@", invitation);
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesInvitationDecline, invitation.invitationID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesInvitationDecline];
    
    [_downloadManager postRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"Error: Cannot decline invitation in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"Error: Cannot decline invitation in REST due to JSON parsing failure");
            return;
        }
        
        NSDictionary *invitationDict;
        if(error && error.code == NSURLErrorNotConnectedToInternet) { // network connection appear to be offline
            // create fake invitaion
            // save it in the cache
            invitationDict = [NSMutableDictionary new];
            [invitationDict setValue:invitation.peer.rainbowID forKey:@"invitingUserId"];
            [invitationDict setValue:_myUser.contact.rainbowID forKey:@"invitedUserId"];
            [invitationDict setValue:invitation.email forKey:@"invitingUserEmail"];
            [invitationDict setValue:_myUser.contact.emailAddresses forKey:@"invitedUserEmail"];
            [invitationDict setValue:[NSString stringWithFormat:@"%@",invitation.invitationID] forKey:@"id"];
            [invitationDict setValue:@"declined" forKey:@"status"];
        }
        else{
            
            invitationDict = [jsonResponse objectForKey:@"data"];
        }
        NSLog(@"Invitation %@ declined in REST", invitationDict);
        
        // Update the invitation (at least the status will become declined)
        Invitation *invitation = [self createOrUpdateInvitation:InvitationDirectionReceived fromDictionary:invitationDict];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitation object:invitation];
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitationPendingNumber object:nil];
}


#pragma mark - old subscription management

-(NSInteger) totalNbOfPendingInvitations {
    __block NSInteger total = 0;
    @synchronized (_invitations) {
        [_invitations enumerateKeysAndObjectsUsingBlock:^(NSString * key, Invitation * invitation, BOOL * _Nonnull stop) {
            if(invitation.direction == InvitationDirectionReceived && invitation.status == InvitationStatusPending)
                total++;
        }];
    }
    
    return total;
}

-(void) removeContactFromMyNetwork:(Contact *) contact {
    if (!contact.rainbow) {
        NSLog(@"Cannot remove non-rainbow contacts from roster");
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,0), ^{
        if (contact.sentInvitation) {
            [self deleteInvitationWithID:contact.sentInvitation];
        }
        [self deleteUserFromMyNetworkWithId:contact.rainbowID];
    });
    
}

-(void)deleteUserFromMyNetworkWithId:(NSString *)userId {
    if (!userId) {
        NSLog(@"Cannot remove contacts with no id from roster");
        return;
    }
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRemoveUserFromMyNetwork, userId];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRemoveUserFromMyNetwork];
    NSLog(@"Remove user from my netowrk request");
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[ContactsManager] removeContactFromMyNetwork: error: %@", error);
        }
        else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if (!jsonResponse) {
                NSLog(@"[ContactsManager] removeContactFromMyNetwork: no response");
            }
        }
    }];
}

#pragma mark - My contact methods
-(Contact *) createMyRainbowContactForJid:(NSString*) jid withCompanyId:(NSString *) companyId withRainbowId:(NSString*) rainbowID companyName :(NSString *) companyName {
    if(!_myContact){
        _myContact = [self createOrUpdateRainbowContactWithJid:jid];
        _myContact.rainbow.isPresenceSubscribed = YES;
        _myContact.rainbow.companyId = companyId;
        _myContact.rainbow.rainbowID = rainbowID;
        _myContact.rainbow.companyName = companyName;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateMyContact object:@{kContactKey: _myContact}];
    }
    return _myContact;
}

-(void) resetMyContact {
    if(_myContact){
        _myContact.rainbow = nil;
        _myContact = nil;
    }
}

-(void) deleteMyContact {
    _myContact = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateMyContact object:nil];
}

-(void) changeMyPresence:(Presence *) presence {
    [_myUser updatePresenceSettings:[MyUser presenceSettingsFromPresence:presence]];
    [self setMyPresence:presence];
}

-(void) setMyPresence:(Presence *) presence {
    [_xmppService setMyPresenceTo:presence];
}

-(void) refreshMyVcard {
    [self fetchRemoteContactDetail:_myContact];
}

-(void) didChangeUserSettings {
    NSLog(@"Did change user settings notification");
    PresenceSettings currentPresenceSettings = _myUser.presenceSettings;
    // Fetch the new presence because we received an event from server that it have changed
    NSLog(@"Get the new presence");
    [_myUser fetchPresenceSettingsWithCompletionHandler:^(PresenceSettings newPresenceSettings) {
        if(currentPresenceSettings != newPresenceSettings){
            NSLog(@"Presence settings is not the current one so we must change our presence in xmpp");
            // There is a different presence settings on server side, we must change our presence
            [self setMyPresence:[MyUser presenceForPresenceSettings:newPresenceSettings]];
        }
    }];
}

-(void) didReceiveCreateConfUserActivated:(BOOL) isReady {
    _myUser.isReadyToCreateConference = isReady;
    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidReceiveCreateConfUserActivated object:nil];
}

#pragma mark - get vcard in cache
-(void) getVcardInCacheForContact:(Contact *) contact {
    __weak Contact *weakContact = contact;
    [_vcardCache.diskCache objectForKeyAsync:contact.jid completion:^(PINDiskCache * _Nonnull cache, NSString * _Nonnull key, NSString *cachedJsonRepresentation) {
        //        NSLog(@"We search a cached vcard for %@ : found it ? %@", key, NSStringFromBOOL(cachedJsonRepresentation.length>0));
        if(cachedJsonRepresentation.length > 0){
            NSDictionary *cachedRepresentation = [[cachedJsonRepresentation dataUsingEncoding:NSUTF8StringEncoding] objectFromJSONData];
            [self updateRainbowContact:weakContact withJSONDictionary:cachedRepresentation];
        }
    }];
//    [_vcardCache objectForKeyAsync:contact.jid completion:^(id<PINCaching> cache, NSString * key, NSString *cachedJsonRepresentation) {

//    }];
}

#pragma mark - get avatar in cache
-(void) getAvatarInCacheForContact:(Contact *) contact {
    NSString *jid = contact.jid;
    [_avatarCache.diskCache objectForKeyAsync:jid completion:^(PINDiskCache * _Nonnull cache, NSString * _Nonnull key, UserPhoto* object) {
        if(object){
            Contact *c = [self getContactWithJid:jid];
            if (c == nil)
                return;
            NSData *currentPhotoData = c.photoData;
            c.rainbow.userPhoto = object;
            // Handle cases where old or new values can be nil.
            if ((contact.photoData || currentPhotoData) && ![c.photoData isEqualToData:currentPhotoData]) {
                [self didUpdateContact:c withChangedKeys:@[@"photoData"]];
            }
        }
    }];
//    [_avatarCache objectForKeyAsync:contact.jid completion:^(id<PINCaching> cache, NSString * key, UserPhoto* object) {
//        //        NSLog(@"We search a cached avatar for %@ : found it ? %@", key, NSStringFromBOOL(object!=nil));
//        if(object){
//            NSData *currentPhotoData = contact.photoData;
//            contact.rainbow.userPhoto = object;
//            // Handle cases where old or new values can be nil.
//            if ((contact.photoData || currentPhotoData) && ![contact.photoData isEqualToData:currentPhotoData]) {
//                [self didUpdateContact:contact withChangedKeys:@[@"photoData"]];
//            }
//        }
//    }];
}

#pragma mark - vcard in rest
-(void) populateVcardForContactsJids:(NSArray <NSString *> *) contactsJids {
    [_directoryService searchRainbowContactsWithJids:contactsJids fromOffset:0 withLimit:[contactsJids count] withCompletionBlock:^(NSString *searchedPattern, NSArray *response, NSError *error) {
        if(!error){
            for (NSDictionary *contactDic in response) {
                // We must chek if we must download a new avatar
                NSString *jid = [contactDic valueForKey:@"jid_im"];
                
                NSString *rainbowId = [contactDic valueForKey:@"id"];
                if (!rainbowId || rainbowId.length == 0)
                    NSLog(@"Error no id for  %@", jid);
                Contact *theContact = [self createOrUpdateRainbowContactWithJid:jid];
                BOOL shouldSaveVcardInCache = [self shouldSaveVcardInCache:contactDic forContact:theContact];
                BOOL shouldDownloadNewContactAvatar = [self shouldDownloadNewContactAvatar:contactDic forContact:theContact];
                // We update the contact instead of created a new one to trigger the good updates(vcardPopulated etc...)
                [self updateRainbowContact:theContact withJSONDictionary:contactDic];
                
                if(shouldSaveVcardInCache){
                    [self saveContactDictionaryInCache:contactDic forContact:theContact];
                }
                
                if(shouldDownloadNewContactAvatar){
                    NSLog(@"Fetching new photo for %@", theContact);
                    [self populateAvatarForRainbowContact:theContact forceReload:YES];
                }
            }
        }
    }];
}

-(void) populateRosterVcardWithLimit:(NSInteger) limit {
    // GET users/network will return all vcard of my roster
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesUsers,[NSString stringWithFormat:@"networks?format=full&limit=%ld",limit]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesUsers];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get user network in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get user network in REST due to JSON parsing failure");
            return;
        }
        
        NSArray *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get user network , response ...");
        for (NSDictionary *contactDic in data) {
            // We must chek if we must download a new avatar
            Contact *theContact = [self getContactWithJid:[contactDic valueForKey:@"jid_im"]];
            BOOL shouldSaveVcardInCache = [self shouldSaveVcardInCache:contactDic forContact:theContact];
            BOOL shouldDownloadNewContactAvatar = [self shouldDownloadNewContactAvatar:contactDic forContact:theContact];
            
            [self updateRainbowContact:theContact withJSONDictionary:contactDic];
            if (theContact.isTerminated || theContact.jid == nil) {
                [self updateRainbowContactNotInRoster:theContact];
            }
            else {
                [self updateRainbowContactIsInRoster:theContact];
            }
            if(shouldSaveVcardInCache && theContact.jid != nil){
                [self saveContactDictionaryInCache:contactDic forContact:theContact];
            }
            
            if(shouldDownloadNewContactAvatar && theContact.jid != nil){
                NSLog(@"Fetching new photo for %@", theContact);
                [self populateAvatarForRainbowContact:theContact forceReload:YES];
            }
        }
        
        [self didEndPopulatingRoster];
    }];
}

-(BOOL) shouldSaveVcardInCache:(NSDictionary *) contactDic forContact:(Contact *) theContact{
    if(!theContact)
        return YES;
    
    BOOL shouldSaveVcardInCache = YES;
    NSDate *cachedLastUpdateDate = [theContact.rainbow.lastUpdateDate copy];
    NSDate *newLastUpdateDate = nil;
    if(contactDic[@"lastUpdateDate"] && [contactDic[@"lastUpdateDate"] isKindOfClass:[NSString class]]){
        newLastUpdateDate = [NSDate dateFromJSONString:contactDic[@"lastUpdateDate"]];
    }
    if(!newLastUpdateDate)
        shouldSaveVcardInCache = NO;
    
    if(cachedLastUpdateDate && newLastUpdateDate){
        if([cachedLastUpdateDate isEqualToDateAndTime:newLastUpdateDate]){
//            NSLog(@"Last update date are the same no need to save new vcard.");
            shouldSaveVcardInCache = NO;
        }
    }
    return shouldSaveVcardInCache;
}

-(BOOL) shouldDownloadNewContactAvatar:(NSDictionary *) contactDic forContact:(Contact *) theContact {
    if(!theContact)
        return NO;
    
    BOOL shouldDownloadNewAvatar = YES;
    
    NSDate *cachedLastPhotoUpdateDate = [theContact.rainbow.userPhoto.lastUpdateDate copy];
    NSDate *newLastPhotoUpdateDate = nil;
    if(contactDic[@"lastAvatarUpdateDate"] && [contactDic[@"lastAvatarUpdateDate"] isKindOfClass:[NSString class]]){
        newLastPhotoUpdateDate = [NSDate dateFromJSONString:contactDic[@"lastAvatarUpdateDate"]];
    }
    
    if(newLastPhotoUpdateDate && cachedLastPhotoUpdateDate){
        if([newLastPhotoUpdateDate isEqualToDateAndTime:cachedLastPhotoUpdateDate]){
//            NSLog(@"Photo date are the same no need to download the new one.");
            shouldDownloadNewAvatar = NO;
        }
    }
    if(!newLastPhotoUpdateDate){
//        NSLog(@"Contact %@ don't have avatar date, so don't try to download his avatar.", theContact);
        shouldDownloadNewAvatar = NO;
    }
    
    // There is a specific case where we determine that we don't need to download avatar but we don't have any photodata and that is not good.
    if(!theContact.rainbow.userPhoto.photoData && !shouldDownloadNewAvatar && newLastPhotoUpdateDate)
        shouldDownloadNewAvatar = YES;

    return shouldDownloadNewAvatar;
}

-(void) saveContactDictionaryInCache:(NSDictionary *) contactDic forContact:(Contact *) theContact {
    NSMutableDictionary *contactDicRepr = [NSMutableDictionary dictionaryWithDictionary:contactDic];
    if(theContact.rainbow.lastUpdateDate)
        [contactDicRepr setObject:[NSDate jsonStringFromDate:theContact.rainbow.lastUpdateDate] forKey:@"lastUpdateDate"];
    [_internalQueue addOperationWithBlock:^{
        [_vcardCache setObjectAsync:[contactDicRepr jsonStringWithPrettyPrint:NO] forKey:theContact.jid completion:nil];
    }];
}

#pragma mark - JID list for vCard to populate
-(void) addJidToFetch:(NSString *) jid {
    // ignore empty JIDs.
    if ([jid length] == 0)
        return;
    
    @synchronized(_jidsToFetch) {
        // Trigger fetch
        if(_jidsToFetchTimer) {
            [_jidsToFetchTimer invalidate];
            _jidsToFetchTimer = nil;
        }
        
        int minToFetch = 50;
        if(![_jidsToFetch containsObject:jid])
            [_jidsToFetch addObject:jid];
        
        // We have enough elements to send a request
        if(_jidsToFetch.count >= minToFetch) {
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange: NSMakeRange(0, minToFetch) ];
            [self populateVcardForContactsJids: [_jidsToFetch objectsAtIndexes:indexSet] ];
            [_jidsToFetch removeObjectsAtIndexes:indexSet];
        }
        
        //
        if(_jidsToFetch.count > 0) {
            _jidsToFetchTimer = [NSTimer timerWithTimeInterval:5 target:self selector:@selector(searchJidAndPopulateVCards) userInfo:nil repeats:NO];
            [[NSRunLoop mainRunLoop] addTimer:_jidsToFetchTimer forMode:NSDefaultRunLoopMode];
        }
    }
}

-(void) searchJidAndPopulateVCards {
    NSMutableArray *arrayOfPages = [NSMutableArray new];
    @synchronized(_jidsToFetch){
        if(_jidsToFetch.count > 0) {
            int pageSize = 50;

            NSRange range = NSMakeRange(0, pageSize);
            
            while (range.location < _jidsToFetch.count) {
                if (range.location + range.length >= _jidsToFetch.count)
                    range.length = _jidsToFetch.count - range.location;
                
                [arrayOfPages addObject:[_jidsToFetch objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:range]]];
                
                range.location += range.length;
            }
            
            [_jidsToFetch removeAllObjects];
        }
    }
    
    for (NSArray *array in arrayOfPages) {
        [self populateVcardForContactsJids:array];
    }
}

#pragma mark - CRUD on Rainbow part of Contact

/**
 *  This method creates or updates the rainbow part of a contact from a rainbow ID
 *
 *  @param rainbowID The rainbow ID user to use to create/update the contact
 *
 *  @return The new/updated contact
 */
-(Contact *) createOrUpdateRainbowContactWithRainbowId:(NSString *) rainbowID {
    NSAssert(![[NSThread currentThread] isEqual:[NSThread mainThread]], @"createOrUpdateRainbowContactWithRainbowId mut *NOT* be invoked on main thread, it perform a synchronous search");
    
    // ignore empty JIDs.
    if ([rainbowID length] == 0)
        return nil;
    
    @synchronized(_contactsMutex) {
        Contact *contact = [self getContactWithRainbowID:rainbowID];
        // No contact match this rainbowID ?
        if (!contact) {
            // Search this contact on server side .... This is synchronous
            NSDictionary *dict = [_directoryService searchRainbowContactWithRainbowID:rainbowID];
            contact = [self createOrUpdateRainbowContactFromJSON:dict];
            [self saveContactDictionaryInCache:[contact dictionaryRepresentation] forContact:contact];
        }
        
        return contact;
    }
}

-(void) createRainbowContactAsynchronouslyWithRainbowID:(NSString *) rainbowID withCompletionHandler:(ContactsManagerCreateContactWithRainbowID) completionHandler {
    [_directoryService searchRainbowContactWithRainbowID:rainbowID withCompletionBlock:^(NSString *searchedPattern, NSArray *response, NSError *error) {
        if(!error){
            Contact *contact = [self createOrUpdateRainbowContactFromJSON:[response firstObject]];
            if (completionHandler) {
                completionHandler(contact);
            }
        }
    }];
}

-(Contact *) getOrCreateRainbowContactSynchronouslyWithJid:(NSString *) jid {
    __block Contact *theContact = nil;
    
    int max_retry = 50;
    int retry = 0;
    
    @synchronized(_contactsMutex){
        theContact = [self getContactWithJid:jid];
        if([theContact.rainbowID isEqualToString:@"fake_rainbow_id"]){
            [_contacts removeObject:theContact];
            theContact = nil;
        }
    }
    
    if(!theContact){
        __block BOOL finished = NO;
        [self searchRemoteContactWithJid:jid withCompletionHandler:^(Contact *foundContact) {
            if(foundContact){
                theContact = foundContact;
            }
            finished = YES;            
        }];
        
        while (!finished && retry < max_retry) {
            retry ++;
            usleep(100000);//micro seconds (100000 = 0.1 second)
        }
    }

    return theContact;
}

-(void) getOrCreateRainbowContactAsynchronouslyWithRainbowID:(NSString *) rainbowID withCompletionHandler:(ContactsManagerCreateContactWithRainbowID) completionHandler {
    Contact *theContact = nil;
    @synchronized (_contactsMutex) {
        theContact = [self getContactWithRainbowID:rainbowID];
        if(theContact){
            if(completionHandler)
                completionHandler(theContact);
        } else {
            [self createRainbowContactAsynchronouslyWithRainbowID:rainbowID withCompletionHandler:completionHandler];
        }
    }
}

/**
 *  This method creates and returns a new contact with its rainbow part set.
 *
 *  @param jid The jid user to use to create the contact
 *
 *  @return The new contact
 */
-(Contact *) createOrUpdateRainbowContactWithJid:(NSString *) jid {
    // ignore empty JIDs.
    if ([jid length] == 0)
        return nil;
    
    if([jid containsString:@"room_"]){
        NSLog(@"Trying to create a contact with room jid !! That is not allowed");
        return nil;
    }
    
    // Prevent from creating contacts with jid_tel in jid_im.
    BOOL isJidTel = NO;
    if([jid containsString:@"tel_"]) {
        jid = [jid stringByReplacingOccurrencesOfString:@"tel_" withString:@""];
        isJidTel = YES;
    }
    BOOL newContact = NO;
    Contact *contact = nil;
    @synchronized(_contactsMutex) {
        contact = [self getContactWithJid:jid];
        if (!contact) {
            // Create the new contact
            contact = [Contact new];
            newContact = YES;
            [_contacts addObject:contact];
        }
        if (!contact.rainbow) {
            // Create the rainbow part of the contact
            contact.rainbow = [RainbowContact new];
        }
        
        // Then set its JID
        if (!isJidTel || !contact.rainbow.jid)
            contact.rainbow.jid = jid;
        
        if(newContact && [jid containsString:@"tmp_invited"])
            contact.rainbow.rainbowID = jid;
    }
    
    if (newContact) {
        [self getVcardInCacheForContact:contact];
        [self getAvatarInCacheForContact:contact];
        
        [self didAddContact:contact];
    }
    
    return contact;
}

-(Contact *) createOrUpdateRainbowContactFromCallLogJid:(NSString *) jid withDictionary:(NSDictionary*) infoDictionary {
    if([jid containsString:@"@"]) {
        Contact *rbContact = [self createOrUpdateRainbowContactWithJid:jid];
        if(!rbContact.vcardPopulated) {
            [self addJidToFetch:jid];
        }
        return rbContact;
    }
    
    // Jid is not a JID but a phone number !
    PhoneNumber *phoneFromJid = [PhoneNumber new];
    phoneFromJid.number = jid;
    phoneFromJid.type = PhoneNumberTypeOther; // is it ?
    phoneFromJid.deviceType = PhoneNumberDeviceTypeLandline;
    // Change phone type for conference service
    if(infoDictionary && [[infoDictionary valueForKey:@"service"] isEqualToString: @"conference"])
        phoneFromJid.type = PhoneNumberTypeConference;
    
    Contact *contact = nil;
    @synchronized(_contactsMutex) {
        // Search for a contact with the same phone number..
        contact = [self searchRainbowContactWithPhoneNumber:phoneFromJid];
        
        if (!contact) {
            contact = [Contact new];
            [_contacts addObject:contact];
        }
        if (!contact.rainbow) {
            RainbowContact *rainbow = [RainbowContact new];
            [rainbow addPhoneNumberObject:phoneFromJid];
            
            if(infoDictionary) {
                NSString *firstName = [infoDictionary valueForKey:@"firstName"];
                if(firstName)
                    rainbow.firstName = firstName;
                
                NSString *lastName = [infoDictionary valueForKey:@"lastName"];
                if(lastName)
                    rainbow.lastName = lastName;
                
                NSString *displayName = [infoDictionary valueForKey:@"displayName"];
                if(!(firstName && lastName) && displayName)
                    rainbow.firstName = displayName;
            }
            
            contact.rainbow = rainbow;
        }
    }

    return contact;
}

-(Contact *) createPeerWithJid:(NSString *)jid {
    BOOL newContact = NO;
    Contact *contact = nil;
    @synchronized(_contactsMutex) {
        // Create the new contact
        contact = [Contact new];
        newContact = YES;
        [_contacts addObject:contact];
        if (!contact.rainbow) {
            // Create the rainbow part of the contact
            contact.rainbow = [RainbowContact new];
        }
        contact.rainbow.jid = jid;
    }

    
    return contact;
}

// Method used to create contact from phone number retreive from conferences
-(Contact *) createOrUpdateRainbowContactWithPhoneNumber:(NSString *) phoneNumber {
    if([phoneNumber containsString:@"@"]){
        NSLog(@"Trying to create a contact from a phone number, but the given phonenumber contain a @ so we asume that is a jid");
        return nil;
    }
    
    // Jid is not a JID but a phone number !
    PhoneNumber *phoneFromJid = [PhoneNumber new];
    phoneFromJid.number = phoneNumber;
    phoneFromJid.type = PhoneNumberTypeOther;
    phoneFromJid.deviceType = PhoneNumberDeviceTypeLandline;
    Contact *contact = nil;
    NSDictionary *currentContactInfo;
    BOOL newContact = NO;
    @synchronized(_contactsMutex) {
        // Search for a contact with the same phone number..
        contact = [self searchRainbowContactWithPhoneNumber:phoneFromJid];
        if (!contact) {
            contact = [Contact new];
            [_contacts addObject:contact];
            newContact = YES;
        }
        currentContactInfo = [contact dictionaryRepresentation];
        if (!contact.rainbow) {
            RainbowContact *rainbow = [RainbowContact new];
            [rainbow addPhoneNumberObject:phoneFromJid];
            
            contact.rainbow = rainbow;
        }
    }

    
    if(newContact)
        [self didAddContact:contact];
    else
        [self didUpdateContact:contact withChangedKeys:[currentContactInfo changedKeysIn:[contact dictionaryRepresentation]]];
    
    return contact;
}

/**
 *  This method creates or updates the rainbow part of a contact from a XMPP roster user
 *
 *  @param xmppUser The XMPP roster user to use to create/update the contact
 *
 *  @return The new/updated contact
 */
-(Contact *) createOrUpdateRainbowContactFromXMPPUser:(XMPPUserMemoryStorageObject *) xmppUser {
    Contact *contact = nil;
    NSString *jid = xmppUser.jid.bare;
    NSDictionary *currentContactInfo;
    
    if([jid containsString:@"room_"]){
        NSLog(@"Trying to create a contact with room jid !! That is not allowed");
        return nil;
    }
    
    // Prevent from creating contacts with jid_tel in jid_im.
    BOOL isJidTel = NO;
    if([jid containsString:@"tel_"]) {
        jid = [xmppUser.jid.bare stringByReplacingOccurrencesOfString:@"tel_" withString:@""];
        isJidTel = YES;
    }
    BOOL newContact = NO;
    @synchronized(_contactsMutex) {
        contact = [self getContactWithJid:jid];
        if (!contact) {
            // Create the new contact
            contact = [Contact new];
            newContact = YES;
            [_contacts addObject:contact];
        }
        currentContactInfo = [contact dictionaryRepresentation];
        
        if (!contact.rainbow) {
            // Create the rainbow part of the contact
            contact.rainbow = [RainbowContact new];
        }

        // Then update it.
        if (!isJidTel || !contact.rainbow.jid)
            contact.rainbow.jid = jid;
        contact.rainbow.nickName = xmppUser.nickname;
        
    }
    
    
    // We don't set the isInRoster flag here because we are not sure that the user is still using rainbow, his account can be removed but still in our roster.
    
    // We receive our own contact in the roster, but we are not subscribed so we make an exception
    if([xmppUser.jid.bare isEqualToString:_myContact.jid])
        contact.rainbow.isPresenceSubscribed = YES;
    else {
        if(![xmppUser.jid.bare containsString:@"tel_"]){
            // The contact can be in our roster, but without presence subscription
            if (![[xmppUser subscription] isEqualToString:@"none"]) {
                contact.rainbow.isPresenceSubscribed = YES;
                // We only set a presence if we don't have one (case where we receive the presence before the roster)
                if(!contact.rainbow.presence){
                    contact.rainbow.presence = [Presence new];
                    contact.rainbow.calendarPresence = [CalendarPresence new];
                }
            } else {
                contact.rainbow.isPresenceSubscribed = NO;
                NSLog(@"createOrUpdateRainbowContactFromXMPPUser %@ isPresenceSubscribed = NO", contact.jid);
            }
        }
    }
    
    contact.rainbow.groups = xmppUser.groups.mutableCopy;
    
    if (newContact) {
        contact.rainbow.presence = [Presence new];
        contact.rainbow.calendarPresence = [CalendarPresence new];
        [self getVcardInCacheForContact:contact];
        [self getAvatarInCacheForContact:contact];
        
        [self didAddContact:contact];
    } else {
        [self didUpdateContact:contact withChangedKeys:[currentContactInfo changedKeysIn:[contact dictionaryRepresentation]]];
    }
    
    return contact;
}

/**
 *  This method creates or updates the rainbow part of a contact from a XMPP presence
 *
 *  @param presence The XMPP presence to use to create/update the contact
 *
 *  @return The new/updated contact
 */
-(Contact *) createOrUpdateRainbowContactFromPresence:(XMPPPresence *) presence {
    // Prevent from creating contacts with jid_tel in jid_im.
    NSString *jid = presence.from.bare;
    
    if([jid containsString:@"room_"]){
        NSLog(@"Trying to create a contact with room jid !! That is not allowed");
        return nil;
    }
    
    BOOL isJidTel = NO;
    if([jid containsString:@"tel_"]) {
        jid = [presence.from.bare stringByReplacingOccurrencesOfString:@"tel_" withString:@""];
        isJidTel = YES;
    }

    BOOL newContact = NO;
    Contact *contact = nil;
    @synchronized(_contactsMutex) {
        contact = [self getContactWithJid:jid];

        if (!contact) {
            // Create the new contact
            contact = [Contact new];
            newContact = YES;
            [_contacts addObject:contact];
        }
        if (!contact.rainbow) {
            // Create the rainbow part of the contact
            contact.rainbow = [RainbowContact new];
        }
        
        // Then update it.
        if (!isJidTel || !contact.rainbow.jid)
            contact.rainbow.jid = jid;
    }
    
    if (newContact) {
        [self getVcardInCacheForContact:contact];
        [self getAvatarInCacheForContact:contact];
        
        [self didAddContact:contact];
    }
    // Update is not possible, we have only creation here.
    
    // we don't update the presence of contact based on the received stanza, his presence will be calculated right after by the presence algorithm.
    
    return contact;
}

/**
 *  This method creates or updates the rainbow part of a contact from a json dictionary.
 *
 *  @param jsonDictionary The dictionary to use to create/update the contact
 *
 *  @return The new/updated contact
 */
-(Contact *) createOrUpdateRainbowContactFromJSON:(NSDictionary *) jsonDictionary {
    NSAssert(![jsonDictionary[@"jid_im"] containsString:@"tel_"], @"Should not create contact with jid_tel");
    BOOL newContact = NO;
    Contact *contact = nil;
    NSDictionary *currentContactInfo;
    @synchronized(_contactsMutex) {
        // Without a jid, we can't do anything.
        if (!jsonDictionary[@"jid_im"])
            return nil;

        contact = [self getContactWithJid:jsonDictionary[@"jid_im"]];
        if (!contact) {
            // Create the new contact
            contact = [Contact new];
            newContact = YES;
            
            [_contacts addObject:contact];
        }
        currentContactInfo = [contact dictionaryRepresentation];
        
        if (!contact.rainbow) {
            // Create the rainbow part of the contact
            contact.rainbow = [RainbowContact new];
        }
        
        // Then update it.
        [ContactsManagerServiceToolKit fillRainbowContact:contact fromDictionary:jsonDictionary];
    }
    
    if (newContact) {
        [self getVcardInCacheForContact:contact];
        [self getAvatarInCacheForContact:contact];
        [self didAddContact:contact];
    } else {
        [self didUpdateContact:contact withChangedKeys:[currentContactInfo changedKeysIn:[contact dictionaryRepresentation]]];
    }
    
    return contact;
}

/**
 *  This method creates the rainbow part of a contact from a json dictionary.
 *
 *  @param jsonDictionary The dictionary to use to create the contact
 *
 *  @return The new contact
 */
-(Contact *) createRainbowContactFromLoginEmail:(NSDictionary *) jsonDictionary {
    Contact *contact = [Contact new];
    if (!contact.rainbow) {
        // Create the rainbow part of the contact
        contact.rainbow = [RainbowContact new];
    }
    [ContactsManagerServiceToolKit fillRainbowContact:contact fromDictionary:jsonDictionary];
    return contact;
}


-(Contact *) createOrUpdateRainbowBotContactFromJSON:(NSDictionary *) jsonDictionary {
    Contact * bot = [self createOrUpdateRainbowContactFromJSON:jsonDictionary];
    NSDictionary *currentContactInfo = [bot dictionaryRepresentation];
    bot.rainbow.presence = [Presence presenceAvailable];
    bot.rainbow.isPresenceSubscribed = YES;
    bot.rainbow.isInRoster = YES;
    bot.rainbow.isBot = YES;
    // No vcard for bots, so try to retreive it.
    bot.rainbow.vcardPopulated = YES;
    // Because this a bot we must search it using is rainbowID instead of his jid
    // Bots have no vcard so don't request it
    [self populateAvatarForContact:bot];
    bot.rainbow.vcardPopulated = YES;
    [self didUpdateContact:bot withChangedKeys:[currentContactInfo changedKeysIn:[bot dictionaryRepresentation]]];
    NSLog(@"Did create bot %@", bot);
    return bot;
}


-(void) updateRainbowContactNotInRoster:(Contact *) contact {
    if (!contact.rainbow) {
        return;
    }
    
    NSLog(@"Update rainbow contact isNOTInRoster %@", contact.jid);
    
    BOOL currentIsInRoster = contact.isInRoster;
    BOOL currentIsPresenceSubscribed = contact.isPresenceSubscribed;
    
    contact.rainbow.isInRoster = NO;
    contact.rainbow.isPresenceSubscribed = NO;
    NSLog(@"updateRainbowContactNotInRoster %@ isPresenceSubscribed = NO", contact.jid);
    
    NSMutableArray *changedKeys = [NSMutableArray new];
    if (contact.isInRoster != currentIsInRoster) {
        [changedKeys addObject:@"isInRoster"];
    }
    if (contact.isPresenceSubscribed != currentIsPresenceSubscribed) {
        [changedKeys addObject:@"isPresenceSubscribed"];
    }
    [self didUpdateContact:contact withChangedKeys:changedKeys];
}

-(void) updateRainbowContactIsInRoster:(Contact *) contact {
    [self updateRainbowContactIsInRoster:contact notify:YES];
}

-(void) updateRainbowContactIsInRoster:(Contact *) contact notify:(BOOL) notify {
    if(!contact.rainbow || !contact.jid){
        return;
    }
    
    NSLog(@"Update rainbow contact isInRoster %@", contact.jid);
    
    BOOL currentIsInRoster = contact.isInRoster;
    BOOL currentIsPresenceSubscribed = contact.isPresenceSubscribed;
    
    contact.rainbow.isInRoster = YES;
    contact.rainbow.isPresenceSubscribed = YES;
    
    NSMutableArray *changedKeys = [NSMutableArray new];
    if (contact.isInRoster != currentIsInRoster) {
        [changedKeys addObject:@"isInRoster"];
    }
    if (contact.isPresenceSubscribed != currentIsPresenceSubscribed) {
        [changedKeys addObject:@"isPresenceSubscribed"];
    }
    
    if(notify)
        [self didUpdateContact:contact withChangedKeys:changedKeys];
    
    if(contact.requestedInvitation.status == InvitationStatusPending) {
        contact.requestedInvitation.status = InvitationStatusAccepted;
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateInvitation object:contact.requestedInvitation];
    }
}

-(void) updateRainbowContact:(Contact *) contact withJidTel:(NSString *) jid_tel {
    if (!contact.rainbow) {
        return;
    }
    NSString *currentJid_tel = contact.jid_tel;
    
    contact.rainbow.jid_tel = jid_tel;
    
    // Handle cases where old or new values can be nil.
    if ((contact.jid_tel || currentJid_tel) && ![contact.jid_tel isEqualToString:currentJid_tel]) {
        [self didUpdateContact:contact withChangedKeys:@[@"jid_tel"]];
    }
}

-(void) updateRainbowContact:(Contact *) contact withGroups:(NSArray *) groups {
    if (!contact.rainbow) {
        return;
    }
    // Handle cases where old or new values can be nil.
    BOOL triggerUpdate = ((contact.rainbow.groups || groups) && ![contact.rainbow.groups isEqualToArray:groups]);
    
    contact.rainbow.groups = groups.mutableCopy;
    
    if (triggerUpdate) {
        [self didUpdateContact:contact withChangedKeys:@[@"groups"]];
    }
}

-(void) updateRainbowContact:(Contact *) contact withPresence:(XMPPPresence *) presence {
    [self updateRainbowContact:contact withPresence:presence withLogs:YES];
}

-(void) updateRainbowContact:(Contact *) contact withPresence:(XMPPPresence *) presence withLogs:(BOOL)withLogs {
    Presence *currentPresence = contact.presence;
    
    ContactPresence contactPresence = ContactPresenceUnavailable;
    if(presence) {
        switch ([presence intShow]) {
            case -1: // Unavailable
                contactPresence = ContactPresenceUnavailable;
                break;
            case 0: // DND
                contactPresence = ContactPresenceDoNotDisturb;
                if(presence.status.length > 0)
                    contactPresence = ContactPresenceBusy;
                break;
            case 1: // Extanded away
                if([presence.status isEqualToString:@"away"]){
                    contactPresence = ContactPresenceAway;
                } else {
                    // Invisible means invisible for me, but unavailable for others.
                    if ([contact.jid isEqualToString:self.myContact.jid]) {
                        contactPresence = ContactPresenceInvisible;
                    } else {
                        contactPresence = ContactPresenceUnavailable;
                    }
                }
                break;
            case 2: // away
                contactPresence = ContactPresenceAway;
                break;
            default:
            case 3: // Available
                contactPresence = ContactPresenceAvailable;
                break;
            case 4: // Chat (Used for telephony presence)
                if([presence.status isEqualToString:@"EVT_SERVICE_INITIATED"] || [presence.status isEqualToString:@"EVT_ESTABLISHED"] || [presence.status isEqualToString:@"busy"]){
                    contactPresence = ContactPresenceBusy;
                }
                break;
        }
    }
    
    Presence *theContactPresence = [Presence new];
    theContactPresence.presence = contactPresence;
    if([presence status].length > 0 && ![presence.status containsString:@"EVT_"]){
        if(contactPresence != ContactPresenceAway && contactPresence != ContactPresenceAvailable){
            // We receive a telephony presence but it doesn't contain EVT_ tag that means this a PGI conf presence
            if([presence.fromStr containsString:@"tel_"])
                theContactPresence.status = @"phone";
            else
                theContactPresence.status = [presence status];
        }
    } else {
        if([presence.status isEqualToString:@"EVT_SERVICE_INITIATED"] || [presence.status isEqualToString:@"EVT_ESTABLISHED"] || [presence.status isEqualToString:@"busy"]){
            theContactPresence.status = @"phone";
        }
    }
    
    contact.rainbow.presence = theContactPresence;
    
    if (withLogs)
        NSLog(@"Set presence %@ for %@", theContactPresence, contact.jid);
    
    // Handle cases where old or new values can be nil.
    if ((contact.rainbow.presence || currentPresence) && ![contact.rainbow.presence isEqual:currentPresence]) {
        [self didUpdateContact:contact withChangedKeys:@[@"presence"]];
    }
    
    if(![presence.fromStr containsString:@"tel_"]){
        if(contact.presence.presence == ContactPresenceAway){
            //Extract date for delay tag in presence request
            if(presence.wasDelayed){
                [self updateLastActivityForContact:contact withSinceDate:presence.delayedDeliveryDate];
            } else {
                [self updateLastActivityForContact:contact withSinceDate:[NSDate date]];
            }
        } else if (contact.presence.presence == ContactPresenceUnavailable ||
                   contact.presence.presence == ContactPresenceInvisible
                   ) {
            //Extract date for delay tag in presence request
            if(presence.wasDelayed){
                [self updateLastActivityForContact:contact withSinceDate:presence.delayedDeliveryDate];
            } else {
                // If the new presence is {Offline, Invisible} update the last-activity date to now.
                [self updateLastActivityForContact:contact withSinceDate:[NSDate date]];
            }
        } else {
            // otherwise, remove the last-activity date.
            [self updateLastActivityForContact:contact withSinceDate:nil];
        }
    }
}

-(void) updateRainbowContact:(Contact *) contact withCalendarPresence:(XMPPPresence *) presence {
    CalendarPresence *currentPresence = contact.calendarPresence;
    
    ContactCalendarPresence calendarPresence = CalendarPresenceAvailable;
    NSDate *untilDate = nil;
    
    if(presence) {
        if( [presence.status isEqualToString: @"busy"] ) {
            calendarPresence = CalendarPresenceBusy;
        } else if( [presence.status isEqualToString: @"out_of_office"] ) {
            calendarPresence = CalendarPresenceOutOfOffice;
        }
        
        NSString * untilDateString = [presence elementForName:@"until"].stringValue;
        
        if(untilDateString) {
            untilDate = [XMPPDateTimeProfiles parseDateTime:untilDateString];
            if(!untilDate)
                untilDate = [XMPPDateTimeProfiles parseDate:untilDateString];
        }
    }
    
    // Create calendar presence object
    CalendarPresence *calPresence = [CalendarPresence new];
    calPresence.presence = calendarPresence;
    calPresence.status = presence.status;
    calPresence.until =  untilDate;
    // We must keep the automatic reply if it's enabled
    if(contact.rainbow.calendarPresence.automaticReply.isEnabled){
        calPresence.automaticReply.untilDate = contact.rainbow.calendarPresence.automaticReply.untilDate;
        calPresence.automaticReply.message = contact.rainbow.calendarPresence.automaticReply.message;
        calPresence.automaticReply.isEnabled = contact.rainbow.calendarPresence.automaticReply.isEnabled;
        calPresence.automaticReply.lastUpdateDate = contact.rainbow.calendarPresence.automaticReply.lastUpdateDate;
    }
    contact.rainbow.calendarPresence = calPresence;
    
    // Handle cases where old or new values can be nil.
    if ((contact.rainbow.calendarPresence || currentPresence) && ![contact.rainbow.calendarPresence isEqual:currentPresence]) {
        [self didUpdateContact:contact withChangedKeys:@[@"calendarPresence"]];
    }
}

/**
 *  This method is used to update the rainbow part of contact
 *  with its vCard from REST response. First we update the rainbow part of the
 *  contact passed in parameter. We trigger an update for this.
 *  Then, we try to unmerge/merge this contact with a local part.
 *  This might also trigger notifications.
 *
 *  @param contact The contact to update
 *  @param jsonDictionary      The json dictionary received in REST response
 */
-(void) updateRainbowContact:(Contact *) contact withJSONDictionary:(NSDictionary *) jsonDictionary {
    NSDictionary *currentContactInfo = [contact dictionaryRepresentation];
    @synchronized(_contactsMutex) {
        // nothing to do if we don't have the rainbow part of the contact.
        if (!contact.rainbow)
            return;
        
        if (jsonDictionary[@"phoneNumbers"]){
            // Clean the old phoneNumbers we have in our contact.
            [contact.rainbow removeAllPhoneNumbers];
        }
        
        if (jsonDictionary[@"emails"]){
            // Clean the old email addresses we have in our contact.
            [contact.rainbow removeAllEmailAddresses];
        }
        
        [ContactsManagerServiceToolKit fillRainbowContact:contact fromDictionary:jsonDictionary];
        
        if(!contact.rainbow.lastName && !contact.rainbow.firstName){
            NSArray *component = [contact.rainbow.nickName componentsSeparatedByString:@" "];
            if(component.count == 2){
                contact.rainbow.firstName = component[0];
                contact.rainbow.lastName = component[1];
            }
        }
        
        [self getAvatarInCacheForContact:contact];
        if(contact == _myContact){
            // extract the value of isInDefaultCompany and save it in myUser object if we have it
            if([jsonDictionary objectForKey:@"isInDefaultCompany"]){
                _myUser.isInDefaultCompany = ((NSNumber *)[jsonDictionary objectForKey:@"isInDefaultCompany"]).boolValue;
            }
            // Extract voice mail is defined.
            if(jsonDictionary[@"phoneNumbers"]) {
                NSArray *phoneNumbers = jsonDictionary[@"phoneNumbers"];
                
                for (NSDictionary *phoneNumberDic in phoneNumbers) {
                    NSString *pbxId = [phoneNumberDic[@"pbxId"] notNull];
                    NSString *voicemailNumber = [phoneNumberDic[@"voiceMailNumber"] notNull];
                    
                    if(pbxId != nil && voicemailNumber != nil) {
                        _myUser.voiceMailNumber = voicemailNumber;
                        break;
                    }
                    
                }
            }

            if (!contact.photoData) {                
                [self populateAvatarForRainbowContact:contact forceReload:YES ];
            }
        }

        if (contact.rainbow.lastName.length > 0 ||  contact.rainbow.firstName.length > 0)
            contact.rainbow.vcardPopulated = YES;
    } // - @synchronized
    
    // trigger the update notification
    NSArray<NSString *> *changedKeys = [currentContactInfo changedKeysIn:[contact dictionaryRepresentation]];
    [self didUpdateContact:contact withChangedKeys:changedKeys];
}

-(void) updateRainbowContact:(Contact *) contact withMobileRessource:(BOOL) mobileRessource {
    if (!contact.rainbow) {
        return;
    }
    BOOL currentIsConnectedWithMobile = contact.isConnectedWithMobile;
    
    contact.rainbow.isConnectedWithMobile = mobileRessource;
    
    if (contact.isConnectedWithMobile != currentIsConnectedWithMobile) {
        [self didUpdateContact:contact withChangedKeys:@[@"isConnectedWithMobile"]];
    }
}

-(void) updateRainbowContact:(Contact *) contact withTelephonyRessource:(BOOL) telephonyRessource {
    if (!contact.rainbow) {
        return;
    }
    // If a telephony ressource is found we update the contact with PBX access
    if (telephonyRessource) {
        contact.rainbow.hasPBXAccess = telephonyRessource;
        [self didUpdateContact:contact withChangedKeys:@[@"hasPBXAccess"]];
    }
}

-(void) populateAvatarForContact:(Contact *) contact {
    [self populateAvatarForRainbowContact:contact forceReload:NO];
}

/**
 *  This is a private method, available to load/force-reload a contact avatar.
 *
 *  @param contact The contact to update
 *  @param force   Whether to force or not the update.
 */
-(void) populateAvatarForRainbowContact:(Contact *) contact forceReload:(BOOL) force {
    
    [self populateAvatarForRainbowContact:contact forceReload:force atSize:256];
}

-(void) loadHiResAvatarForContact:(Contact*) contact withCompletionBlock:(ContactsManagerServiceLoadHiResAvatarCompletionHandler) completionHandler {
    [self loadContactPhotoData:contact atSize:1024 completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(completionHandler)
            completionHandler(receivedData, error);
    }];
}

/**
 *  This is a private method, available to load/force-reload a contact avatar.
 *
 *  @param contact The contact to update
 *  @param force   Whether to force or not the update.
 *  @param atSize The expected minimum photo size if available
 */
-(void) populateAvatarForRainbowContact:(Contact *) contact forceReload:(BOOL) force atSize: (int) size {
    
    // If we already have the photo, don't reload it.
    if (contact.photoData && !force)
        return;
    
    if (!contact.rainbowID || ![contact.rainbowID length]) {
        NSLog(@"No rainbowID for contact %@, cannot get avatar.", contact.jid);
        return;
    }
    if(!contact.rainbow.userPhoto.lastUpdateDate && !contact.isBot){
        NSLog(@"No user photo update date, the user don't have avatar, no need to try to download it");
        return;
    }
    
    NSLog(@"Fetching Avatar for %@", contact.jid);
    
    [self loadContactPhotoData:contact atSize:size completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(!error) {
            [self updateRainbowContact:contact withPhotoData:receivedData];
            UserPhoto *newPhoto = contact.rainbow.userPhoto;
            [_internalQueue addOperationWithBlock:^{
                NSLog(@"Saving %@ for contact %@", newPhoto, contact.jid);
                [_avatarCache setObjectAsync:newPhoto forKey:contact.jid completion:^(id<PINCaching> cache, NSString * key, id object) {
                    NSLog(@"Saved at key %@ object in cache %@", key, object);
                }];
            }];
            NSLog(@"Update %@ Avatar in cache %@", contact.jid, contact.rainbow.userPhoto);
            
            // Handle cases where old or new values can be nil.
            [self didUpdateContact:contact withChangedKeys:@[@"photoData"]];
            
        } else {
            NSLog(@"Could not get the avatar for contact %@: %ld", [contact description], (long)error.code);
            // 404 means no more photo for the user ?
            @synchronized (_contactsMutex) {
                [_internalQueue addOperationWithBlock:^{
                    [_avatarCache removeObjectForKeyAsync:contact.jid completion:nil];
                }];
                contact.rainbow.userPhoto = nil;
                [self didUpdateContact:contact withChangedKeys:@[@"photoData"]];
            }
        }
    }];
}

-(void) updateRainbowContact:(Contact *)contact withPhotoData:(NSData *) photoData {
    UserPhoto *newPhoto = [[UserPhoto alloc] initWithPhotoData:photoData lastUpdateDate:contact.rainbow.userPhoto.lastUpdateDate];
    
    @synchronized (_contactsMutex) {
        contact.rainbow.userPhoto = newPhoto;
    }
}

-(void) loadContactPhotoData:(Contact *) contact atSize:(int) size completionBlock:(ContactsManagerServiceLoadHiResAvatarInternalCompletionHandler) completionHandler {
    NSString *update = [[NSDate jsonStringFromDate:contact.rainbow.userPhoto.lastUpdateDate] MD5];
    
    NSMutableString *urlParam = [NSMutableString stringWithFormat:@"%u",size];
    if(update.length > 0)
       [urlParam appendFormat:@"&update=%@",update];
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesAvatar, contact.rainbowID, urlParam];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesAvatar];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(completionHandler)
            completionHandler(receivedData, error, response);
    }];
}

-(void) getLastActivityForContact:(Contact *)contact {
    [_xmppService getLastActivityForContact:contact];
}

-(void) updateLastActivityForContact:(Contact*)contact withSinceDate:(NSDate*)date {
    NSDate *currentLastActivityDate = contact.lastActivityDate;
    
    contact.rainbow.lastActivityDate = date;
    
    // Handle cases where old or new values can be nil.
    if ((contact.lastActivityDate || currentLastActivityDate) && ![contact.lastActivityDate isEqualToDateAndTime:currentLastActivityDate]) {
        [self didUpdateContact:contact withChangedKeys:@[@"lastActivityDate"]];
    }
}

-(void) updateRainbowContact:(Contact *) contact withCompanyInvitation:(CompanyInvitation *) companyInvitation {
    if(!contact.rainbow)
        return;
    
    contact.rainbow.companyInvitation = companyInvitation;
    [self didUpdateContact:contact withChangedKeys:@[@"companyInvitation"]];
}

/**
 *  This function is used internally to remove a RainbowContact part
 *  from a given contact. It also triggers the right notification
 *  depending on the contact state after the removal.
 *  -> an Update if the contact still exist (still one or more local parts)
 *  -> a Remove if there is no local parts attached.
 *
 *  We never remove a user from our array, because a user can still have a conversation or be in room ...
 *
 *  @param contact      The contact to update
 */
-(void) deleteRainbowContact:(Contact *) contact {
    NSDictionary *currentContactInfo = [contact dictionaryRepresentation];
    @synchronized(_contactsMutex) {
        // We do nothing unless we have a rainbow contact.
        if (contact.rainbow) {
            // If we also have a local contact, dont delete it
            // juste remove the rainbow part and emit and update notif.
            if ([contact.locals count]) {
                contact.rainbow = nil;
            } else {
                contact.rainbow.sentInvitation = nil;
                contact.rainbow.isInRoster = NO;
                contact.rainbow.isPresenceSubscribed = NO;
                contact.rainbow.presence = nil;
                NSLog(@"deleteRainbowContact %@ isPresenceSubscribed = NO", contact.jid);
            }
        }
    }
    
    [self didUpdateContact:contact withChangedKeys:[currentContactInfo changedKeysIn:[contact dictionaryRepresentation]]];
}


#pragma mark - CRUD of local part of contacts

///**
// *  This function is called by the LocalContacts helper, on all the iPhone AddressBook update.
// *  It take a list of ABRecords and re-sync this list with our list of contacts.
// *  First it remove from ou list, the LocalContact removed from iPhone. Then it 
// *  updates each LocalContact, merge them in case of LinkedContacts.
// *  Finally it tries to merge/unmerge with Rainbow part of contacts.
// *
// *  @param localContacts An array of CNContact
// */
//-(void) syncLocalContactsWithNSArrayOfContact:(NSArray *) localContacts {
//    @synchronized(_contactsMutex) {
//        // We have multiple things to do when re-syncing the local contacts with our list of contacts.
//
//        // First remove from our current contact list the local contacts
//        // that does not exist anymore, or that dont have (emails or phones) anymore.
//        
//        // Make a set with all the local contact IDs in the phone.
//        NSMutableSet *localContactsIDs = [NSMutableSet set];
//        for (CNContact *person in localContacts) {
//            [localContactsIDs addObject:person.identifier];
//        }
//
//        // We reverse enumerate, because we might remove elements in the array, while listing it.
//        [_contacts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(Contact *contact, NSUInteger idx, BOOL *stop) {
//
//            [contact.locals enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(LocalContact *localContact, NSUInteger idx, BOOL *stop) {
//                // If this local contact is not anymore in our new list of local contact, delete it.
//                if (![localContactsIDs containsObject:localContact.addressBookRecordID]) {
//                    [self deleteLocalContact:localContact onContact:contact];
//                //    [self didRemoveLocalContact:localContact];
//                }
//            }];
//            
//        }];
//
//        // Now we can create/update the local parts of our contacts
//        for (CNContact *person in localContacts) {
//
//            // This contact already exists ?
//            Contact *contact = [self getContactWithAddressBookRecordID:person.identifier];
//
//            if (contact) {
//
//                NSDictionary *currentContactInfo = [contact dictionaryRepresentation];
//                
//                // Find the right LocalContact to update
//                LocalContact *localContact = nil;
//                for (LocalContact *local in contact.locals) {
//                    if ([local.addressBookRecordID isEqualToString: person.identifier]) {
//                        localContact = local;
//                        break;
//                    }
//                }
//                NSAssert(localContact, @"The local contact should exist on this contact.");
//
//                // Now lets populate a temp-local contact
//                LocalContact *tempLocalContact = [LocalContact new];
//                [LocalContactsToolkit fillLocalContact:tempLocalContact withContact:(CNContact *)person];
//
//                // And now compare this up-to-date local contact, with our current local contact information.
//                // If we have something new, emit a LocalContactUpdate.
//                BOOL emitUpdate = NO;
//                if (![[tempLocalContact jsonRepresentation] isEqualToString:[localContact jsonRepresentation]]) {
//                    emitUpdate = YES;
//                }
//                // Then update our real local contact.
//                [LocalContactsToolkit fillLocalContact:localContact withContact:(CNContact *)person];
//                [self didUpdateContact:contact withChangedKeys:[currentContactInfo changedKeysIn:[contact dictionaryRepresentation]]];
//
//                if (emitUpdate) {
//                 //   [self didUpdateLocalContact:localContact];
//                }
//
//            } else {
//                // The contact does not exists, create it.
//                contact = [Contact new];
//                LocalContact *localContact = [LocalContact new];
//                [contact addLocalContact:localContact];
//                
//                [LocalContactsToolkit fillLocalContact:localContact withContact:(CNContact *)person];
//
//                [_contacts addObject:contact];
//                [self didAddContact:contact];
//            //    [self didAddLocalContact:localContact];
//            }
//
//            // Now try to merge/unmerge the full local-contact array with/from a rainbow contact
//            [self mergeOrUnmergeContact:contact];
//        }
//    }
//
//    // Now the local contacts are up-to-date,
//    // We can search if they exists in rainbow
//    [self searchForRainbowUserInLocalContacts];
//}

/**
 *  This function is used internally to remove a LocalContact part
 *  from a given contact. It also triggers the right notification
 *  depending on the contact state after the removal.
 *  -> an Update if the contact still exist (still have a rainbow part or others local parts)
 *  -> a Remove if it was the last local part and there were no rainbow part.
 *
 *  @param localContact The local part to remove
 *  @param contact      The contact to update
 */
-(void) deleteLocalContact:(LocalContact *) localContact onContact:(Contact *) contact {
    @synchronized(_contactsMutex) {
        NSDictionary *currentContactInfo = [contact dictionaryRepresentation];
        
        // Find and remove the right local contact
        [contact removeLocalContact:localContact];
        
        // If we still have a rainbow contact or others local contacts, dont delete it
        // just emit an update.
        if (contact.rainbow || [contact.locals count]) {
            [self didUpdateContact:contact withChangedKeys:[currentContactInfo changedKeysIn:[contact dictionaryRepresentation]]];
            
        } else {
            [((NSMutableArray*)_contacts) removeObject:contact];
            [self didRemoveContact:contact];
        }
    }
}

-(void) deleteAllLocalContact:(Contact *) contact {
    @synchronized(_contactsMutex) {
        NSDictionary *currentContactInfo = [contact dictionaryRepresentation];
        
        // Remove all the local contacts
        [contact removeAllLocalContacts];
        
        // If we still have a rainbow contact or others local contacts, dont delete it
        // just emit an update.
        if (contact.rainbow) {
            [self didUpdateContact:contact withChangedKeys:[currentContactInfo changedKeysIn:[contact dictionaryRepresentation]]];
            
        } else {
            [((NSMutableArray*)_contacts) removeObject:contact];
            [self didRemoveContact:contact];
        }
    }
}


#pragma mark - The merge/unmerge function

///**
// *  This is the heart of the merge/unmerge process between local and rainbow contacts.
// *  In this function, we first try to un-merge the contact if it was merged by
// *  checking if there is still a matching property between the local and rainbow parts.
// *  Then we try to merge it with another part if we can find a matching property
// *
// *  @param contact the contact to merge/unmerge
// */
//-(void) mergeOrUnmergeContact:(Contact*) contact {
//    @synchronized (_contactsMutex) {
//
//        // If we had both part, then check if the matching is still valid
//        // i.e. they still have (at least) one common email or phone number.
//        if ([contact.locals count] && contact.rainbow) {
//            BOOL stillMatch = NO;
//            for (EmailAddress *email in contact.rainbow.emailAddresses) {
//                for (LocalContact *localContact in contact.locals) {
//                    if ([localContact emailAddressesContainsObject:email]) {
//                        // at least one email still match
//                        stillMatch = YES;
//                        break;
//                    }
//                }
//                // Don't continue if still match.
//                if (stillMatch)
//                    break;
//            }
//
//            if (!stillMatch) {
//                for (PhoneNumber *number in contact.rainbow.phoneNumbers) {
//                    for (LocalContact *localContact in contact.locals) {
//                        if ([localContact phoneNumbersContainsObject:number]) {
//                            // at least one phone number still match
//                            stillMatch = YES;
//                            break;
//                        }
//                    }
//                    // Don't continue if still match.
//                    if (stillMatch)
//                        break;
//                }
//            }
//
//            // Here, if stillMatch is to NO
//            // Then the two part of the contact : rainbow and local
//            // have nothing anymore in common, we can split them.
//            if (!stillMatch) {
//                Contact *newLocalContact = [Contact new];
//                [newLocalContact transfertLocalsFrom:contact];
//                [self deleteAllLocalContact:contact];
//                [_contacts addObject:newLocalContact];
//                [self didAddContact:newLocalContact];
//            }
//        }
//
//        // Now try to find if we have a local contact matching with the rainbow part
//        // based on the email or phone numbers.
//        if (![contact.locals count]) {
//            BOOL merged = NO;
//            for (EmailAddress *email in contact.rainbow.emailAddresses) {
//                Contact *localContact = [self searchLocalContactWithEmailAddress:email];
//
//                // We have a matching local contact !
//                if (localContact) {
//                    NSDictionary *currentContactInfo = [contact dictionaryRepresentation];
//                    // transfert the locals object from the found contact to
//                    // our contact.
//                    [contact transfertLocalsFrom:localContact];
//                    [self didUpdateContact:contact withChangedKeys:[currentContactInfo changedKeysIn:[contact dictionaryRepresentation]]];
//                    [self deleteAllLocalContact:localContact];
//                    merged = YES;
//                    break;
//                }
//            }
//
//            // We can merge only 1 time, on email OR phone
//            if (!merged) {
//                for (PhoneNumber *number in contact.rainbow.phoneNumbers) {
//                    Contact *localContact = [self searchLocalContactWithPhoneNumber:number];
//
//                    // We have a matching local contact !
//                    if (localContact) {
//                        NSDictionary *currentContactInfo = [contact dictionaryRepresentation];
//                        // transfert the locals object from the found contact to
//                        // our contact.
//                        [contact transfertLocalsFrom:localContact];
//                        [self didUpdateContact:contact withChangedKeys:[currentContactInfo changedKeysIn:[contact dictionaryRepresentation]]];
//                        [self deleteAllLocalContact:localContact];
//                        break;
//                    }
//                }
//            }
//        }
//
//        // Now try to find if we have a rainbow contact matching with him
//        // based on the email or phone numbers.
//        if (!contact.rainbow) {
//            BOOL merged = NO;
//            for (LocalContact *localContact in contact.locals) {
//                for (EmailAddress *email in localContact.emailAddresses) {
//                    Contact *rainbowContact = [self searchRainbowContactWithEmailAddress:email];
//                    // We have a matching rainbow contact !
//                    if (rainbowContact && rainbowContact != _myUser.contact) {
//                        NSDictionary *currentContactInfo = [rainbowContact dictionaryRepresentation];
//                        // transfert the locals object from the found contact to
//                        // our contact.
//                        [rainbowContact transfertLocalsFrom:contact];
//                        [self didUpdateContact:rainbowContact withChangedKeys:[currentContactInfo changedKeysIn:[contact dictionaryRepresentation]]];
//                        [self deleteAllLocalContact:contact];
//                        merged = YES;
//                        break;
//                    }
//                }
//
//                // We can merge only 1 time, on email OR phone
//                if (!merged) {
//                    for (PhoneNumber *number in localContact.phoneNumbers) {
//                        Contact *rainbowContact = [self searchRainbowContactWithPhoneNumber:number];
//
//                        // We have a matching rainbow contact !
//                        if (rainbowContact && rainbowContact != _myUser.contact) {
//                            NSDictionary *currentContactInfo = [contact dictionaryRepresentation];
//                            // transfert the locals object from the found contact to
//                            // our contact.
//                            [rainbowContact transfertLocalsFrom:contact];
//                            [self didUpdateContact:rainbowContact withChangedKeys:[currentContactInfo changedKeysIn:[contact dictionaryRepresentation]]];
//                            [self deleteAllLocalContact:contact];
//                            merged = YES;
//                            break;
//                        }
//                    }
//                }
//
//                // Don't continue once merged !
//                if (merged)
//                    break;
//            }
//        }
//    } // - @synchronized
//}


#pragma mark - Update myContact vcard
/*
 * This function updates the Rainbow user, not the local one.
 */
-(void) updateUserWithFields:(NSDictionary*)fields withCompletionBlock:(void(^)(NSError* error)) completionBlock {

    BOOL forceGuestInitialize = NO;
    BOOL isInitializingContact = NO;
    if([fields objectForKey:@"isInitialized"])
        isInitializingContact = YES;
    
    if([fields objectForKey:@"forceGuestInitialize"])
        forceGuestInitialize = YES;
    
    NSString *myContactBeforeUpdate = [_myUser.contact jsonRepresentation];
    NSData *photoDataBeforeUpdate = [NSData dataWithData:_myUser.contact.rainbow.userPhoto.photoData];
    
    NSString *lastname = [fields objectForKey:@"lastname"];
    if (lastname)
        _myUser.contact.rainbow.lastName = lastname;
    
    NSString *firstname = [fields objectForKey:@"firstname"];
    if (firstname)
        _myUser.contact.rainbow.firstName = firstname;
    
    NSString *nickname = [fields objectForKey:@"nickname"];
    if (nickname)
        _myUser.contact.rainbow.nickName = nickname;
    
    NSString *jobRole = [fields objectForKey:@"jobrole"];
    if (jobRole)
        _myUser.contact.rainbow.jobTitle = jobRole;
    
    NSString *title = [fields objectForKey:@"title"];
    if (title)
        _myUser.contact.rainbow.title = title;
    
    NSString *personalEmail = [fields objectForKey:@"personal_email"];
    if (personalEmail) {
        EmailAddress *address = [ContactsManagerServiceToolKit emailAddressFromEmailAddress:personalEmail type:EmailAddressTypeHome];
        [ContactsManagerServiceToolKit insertOrUpdateEmailAddress:address forRainbowContact:_myUser.contact];
    }

    NSString *professionalLandphone = [fields objectForKey:@"professional_landphone"];
    if (professionalLandphone) {
        PhoneNumber *number = [ContactsManagerServiceToolKit phoneNumberFromNumber:professionalLandphone type:PhoneNumberTypeWork deviceType:PhoneNumberDeviceTypeLandline];
        [ContactsManagerServiceToolKit insertOrUpdatePhoneNumber:number forRainbowContact:_myUser.contact];
    }
    
    NSString *professionalMobilephone = [fields objectForKey:@"professional_mobilephone"];
    if (professionalMobilephone) {
        PhoneNumber *number = [ContactsManagerServiceToolKit phoneNumberFromNumber:professionalMobilephone type:PhoneNumberTypeWork deviceType:PhoneNumberDeviceTypeMobile];
        [ContactsManagerServiceToolKit insertOrUpdatePhoneNumber:number forRainbowContact:_myUser.contact];
    }
    
    NSString *personalLandphone = [fields objectForKey:@"personal_landphone"];
    if (personalLandphone) {
    PhoneNumber *number = [ContactsManagerServiceToolKit phoneNumberFromNumber:personalLandphone type:PhoneNumberTypeHome deviceType:PhoneNumberDeviceTypeLandline];
    [ContactsManagerServiceToolKit insertOrUpdatePhoneNumber:number forRainbowContact:_myUser.contact];
    }
    
    NSString *personalMobilephone = [fields objectForKey:@"personal_mobilephone"];
    if (personalMobilephone) {
    PhoneNumber *number = [ContactsManagerServiceToolKit phoneNumberFromNumber:personalMobilephone type:PhoneNumberTypeHome deviceType:PhoneNumberDeviceTypeMobile];
    [ContactsManagerServiceToolKit insertOrUpdatePhoneNumber:number forRainbowContact:_myUser.contact];
    }
    
    NSString *countryCode = [fields objectForKey:@"country"];
    if(countryCode)
        _myUser.contact.rainbow.countryCode = countryCode;
    
    NSString *timezone = [fields objectForKey:@"timezone"];
    if(timezone)
        _myUser.contact.rainbow.timeZone = [NSTimeZone timeZoneWithName:timezone];
    
    NSString *language = [fields objectForKey:@"language"];
    if(language)
        _myUser.contact.rainbow.language = language;
    BOOL needUpdateVcard = YES;
    
    NSString *myContactAfterUpdate = [_myUser.contact jsonRepresentation];
    if([myContactBeforeUpdate isEqualToString:myContactAfterUpdate] && !isInitializingContact && !forceGuestInitialize){
        needUpdateVcard = NO;
    }
    
    NSError *requestError = nil;
    
    if(needUpdateVcard){
        // Update profile through our custom REST API
        NSURL *urlUsers = [_apiUrlManagerService getURLForService:ApiServicesMyUsers];
        NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesMyUsers];
        NSLog(@"Update my vcard");
        
        NSData *receivedData = nil;
        NSURLResponse *response = nil;
        NSMutableDictionary *myContactDic = [NSMutableDictionary dictionaryWithDictionary:[_myUser.contact dictionaryRepresentation:NO]];
        
        if(isInitializingContact){
            if(_myUser.isGuest && !forceGuestInitialize){
                // IsInitialized must be false
                [myContactDic setObject:@NO forKey:@"isInitialized"];
                _myUser.isInitialized = NO;
            } else{
                [myContactDic setObject:@YES forKey:@"isInitialized"];
                _myUser.isInitialized = YES;
            }
        }
        
        // Force values for guest
        if([fields objectForKey:@"forceGuestInitialize"]){
            [myContactDic setObject:@NO forKey:@"guestMode"];
            [myContactDic setValue:@"public" forKey:@"visibility"];
        }
        
        BOOL requestSucceed = [_downloadManager putSynchronousRequestWithURL:urlUsers body:[myContactDic jsonStringWithPrettyPrint:NO] requestHeadersParameter:headersParameters returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
        if(requestSucceed && !requestError){
            NSDictionary *response = [receivedData objectFromJSONData];
            if(requestError || [response objectForKey:@"errorDetails"]) {
                NSLog(@"Could not update user raison : %@ %@", response[@"errorDetails"], requestError);
                if([response[@"errorDetails"] isKindOfClass:[NSArray class]])
                    requestError = [NSError errorWithDomain:@"Contact update" code:NSURLErrorBadServerResponse userInfo:@{NSLocalizedDescriptionKey:response[@"errorDetails"][0][@"msg"]}];
                else
                    requestError = [NSError errorWithDomain:@"Contact update" code:NSURLErrorBadServerResponse userInfo:@{NSLocalizedDescriptionKey:response[@"errorMsg"]}];
                
                // In case of error we must revert the already saved (in memory) contact modifications
                [self updateRainbowContact:_myContact withJSONDictionary:[[myContactBeforeUpdate dataUsingEncoding:NSUTF8StringEncoding] objectFromJSONData]];
            } else if([response objectForKey:@"data"]){
                // We must retreive the phone number returned by the server, because the server will canonize them if needed
                // So we update the myContact with all information returned by server
                [self updateRainbowContact:_myContact withJSONDictionary:response[@"data"]];
            }
        }
    }
    
    NSData *receivedDataAvatar = nil;
    NSError *requestErrorAvatar = nil;
    NSURLResponse *responseAvatar = nil;
    
    id photo = [fields objectForKey:@"photo"];
    if ([photo isKindOfClass:[UIImage class]]) {
        UIImage *image = (UIImage*)photo;
        
        // Resize picture to 512x512 max.
        if (image.size.height > 512 || image.size.width > 512) {
            NSLog(@"Resizing picture to 512x512, previous size was : %f x %f", image.size.width, image.size.height);
            CGSize preferedSize = CGSizeMake(512, 512);
            // Set scale factor to 1 to avoid to big images.
            UIGraphicsBeginImageContextWithOptions(preferedSize, NO, 1);
            [image drawInRect:CGRectMake(0, 0, preferedSize.width, preferedSize.height)];
            // Override with new resized image
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        _myUser.contact.rainbow.userPhoto = [[UserPhoto alloc] initWithPhotoData:UIImagePNGRepresentation(image)];
    }
    
    BOOL needUpdateAvatar = YES;
    if([photoDataBeforeUpdate isEqualToData:_myUser.contact.rainbow.userPhoto.photoData] && !isInitializingContact){
        // No need to update same contact
        needUpdateAvatar = NO;
    }
    
    if(needUpdateAvatar){
        // Update the Avatar through the REST API
        NSData *photoData = _myUser.contact.photoData;
        if(photoData.length > 0){
            NSURL *urlAvatar = [_apiUrlManagerService getURLForService:ApiServicesEndUserAvatar];
            NSMutableDictionary *headersParametersAvatar = (NSMutableDictionary*)[_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesEndUserAvatar];
            
            NSLog(@"Update avatar with url %@ photo data size %@", urlAvatar, [NSString formatFileSize:photoData.length]);
            
            // override the content-type
            [headersParametersAvatar setObject:@"image/png" forKey:@"Content-Type"];
            BOOL requestSucceedAvatar = [_downloadManager postSynchronousRequestWithURL:urlAvatar data:photoData requestHeadersParameter:headersParametersAvatar returningResponseData:&receivedDataAvatar returningNSURLResponse:&responseAvatar returningError:&requestErrorAvatar];
            if(requestSucceedAvatar && !requestErrorAvatar){
                NSDictionary *response = [receivedDataAvatar objectFromJSONData];
                NSLog(@"Avatar update API result  : %@", response);
                if(requestErrorAvatar || [response objectForKey:@"errorDetails"]) {
                    NSLog(@"Could not update my avatar: %ld", (long)requestErrorAvatar.code);
                    
                    if([response[@"errorDetails"] isKindOfClass:[NSArray class]])
                        requestErrorAvatar = [NSError errorWithDomain:@"Contact update" code:NSURLErrorBadServerResponse userInfo:@{NSLocalizedDescriptionKey:response[@"errorDetails"][0][@"msg"]}];
                    else
                        requestErrorAvatar = [NSError errorWithDomain:@"Contact update" code:NSURLErrorBadServerResponse userInfo:@{NSLocalizedDescriptionKey:response[@"errorMsg"]}];
                    
                } else {
                    [_internalQueue addOperationWithBlock:^{
                        [_avatarCache setObjectAsync:_myUser.contact.rainbow.userPhoto forKey:_myContact.rainbowID completion:nil];
                    }];
                }
            }
        } else {
            NSLog(@"No photo to upload");
        }
        
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidUpdateMyContact object:@{kContactKey: _myContact}];
    
    if(completionBlock) {
        if(requestError)
            completionBlock(requestError);
        else if(requestErrorAvatar)
            completionBlock(requestErrorAvatar);
        else
            completionBlock(nil);
    }
}


#pragma mark - to be moved

-(void) callPhoneNumber:(NSString *) phoneNumber directCall:(BOOL) directCall {
    // this is used to trigger a ClickToCall Notification.
    // This should be moved in some telephony service one day.
    if (!phoneNumber)
        return;
    [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceClickToCallMobile object:phoneNumber];
}

+(NSString *) currentCountryCode {
    NSLocale *currentLocale = [NSLocale currentLocale];
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    
    return [LocalContactsToolkit getISO3611Alpha3CodeFor:countryCode];
}

+(Contact *) createContactFromCNContact:(CNContact *) person {
    Contact *contact = [Contact new];
    LocalContact *localContact = [LocalContact new];
    [contact addLocalContact:localContact];
    
    [LocalContactsToolkit fillLocalContact:localContact withContact:(CNContact *)person];
    
    return contact;
}

-(NSString *) getUserWithoutDomainFromBareJid:(NSString *) bareJid {
    NSArray *items = [bareJid componentsSeparatedByString:@"@"];
    if (items.count > 0)
        return items[0];
    else
        return bareJid;
}

@end
