/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "MyUser.h"

typedef NS_ENUM(NSInteger, ApiServices) {
    ApiServicesAuthenticationLogin = 0,
    ApiServicesAuthenticationLogout,
    ApiServicesDirectory,
    ApiServicesUsers,
    ApiServicesMyUsers,
    ApiServicesMyUserSettings,
    ApiServicesUsersSelfRegister,
    ApiServicesSources,
    ApiServicesContacts,
    ApiServicesConversations,
    ApiServicesConversationDownload,
    ApiServicesEndUserNotificationsEmailsSelfRegister,
    ApiServicesAvatar,
    ApiServicesEndUserAvatar,
    ApiServicesEndUserNotificationEmailsResetPassword,
    ApiServicesEndUserResetPassword,
    ApiServicesEndUserChangePassword,
    ApiServicesCalendarAutomaticReply,
    ApiServicesRooms,
    ApiServicesRoomsDetail,
    ApiServicesRoomsUsers,
    ApiServicesRoomsChangeUserData,
    ApiServicesRoomsShareConference,
    ApiServicesRoomsUnshareConference,
    ApiServicesRoomsConferenceInvitations,
    ApiServicesRoomsSharedConference,
    ApiServicesRoomsConference,
    ApiServicesRoomsAvatar,
    ApiServicesRoomsChangeAvatar,
    ApiServicesRoomsCustomData,
    ApiServicesAbout,
	ApiServicesBots,
    ApiServicesGroups,
    ApiServicesInvite,
    ApiServicesInviteBulk,
    ApiServicesReceivedInvitations,
    ApiServicesSentInvitations,
    ApiServicesInvitationDetails,
    ApiServicesInvitationAccept,
    ApiServicesInvitationDecline,
    ApiServicesIceServers,
    ApiServicesCompanies,
    ApiServicesJoinCompaniesRequest,
    ApiServicesJoinCompaniesInvitations,
    ApiServicesJoinCompaniesInvitationAccept,
    ApiServicesJoinCompaniesInvitationDecline,
    ApiServicesFileStorage,
    ApiServicesFileServer,
    ApiServicesFileStorageBase,
    ApiServicesFileCapabilities,
    ApiServicesProfiles,
    ApiServicesConferences,
    ApiServicesConferencesSubscribe,
    ApiServicesConferencesSnapshot,
    ApiServicesConferencesStart,
    ApiServicesConferencesStop,
    ApiServicesConferencesJoin,
    ApiServicesConferencesUpdateState,
    ApiServicesConferencesUpdateParticipantState,
    ApiServicesConfProvisioningConferences,
    ApiServicesConfProvisioningConferencesDelete,
    ApiServicesConfProvisioningUsers,
    ApiServicesRoomsConferences,
    ApiServicesRoomsInstantConferences,
    ApiServicesRoomsScheduledConferences,
    ApiServicesSearch,
    ApiServicesSearchPBXPhonebook,
    ApiServicesSearchActiveDirectory,
    ApiServicesBanner,
    ApiServicesApplicationsAuthentificationLogin,
    ApiServicesChannelsCreateChannel,
    ApiServicesChannelsFindChannels,
    ApiServicesChannelsGetChannel,
    ApiServicesChannelsGetMyChannels,
    ApiServicesChannelsPublish,
    ApiServicesChannelsSubscribe,
    ApiServicesChannelsUpdateChannel,
    ApiServicesChannelsGetItemsFromChannel,
    ApiServicesChannelsGetFirstUsersFromChannel,
    ApiServicesChannelsGetNextUsersFromChannel,
    ApiServicesWebrtcmetrics,
    ApiServicesTelephony,
    ApiServicesMediaPillar,
    ApiServicesMediaPillarNumbering,
    ApiServicesMarkAllMessagesAsReadFroConversation,
    ApiServicesRemoveUserFromMyNetwork
};



@interface ApiUrlManagerService : NSObject

-(instancetype) initWithMyUser:(MyUser *) myUser;
-(NSURL *) getRootURL;
-(NSURL *) getURLForService:(ApiServices) service, ...;
-(NSURL *) getURLWithServer:(Server *) server forService:(ApiServices) service, ...;
-(NSDictionary *) getHTTPHeaderParametersForService:(ApiServices) service;
@end
