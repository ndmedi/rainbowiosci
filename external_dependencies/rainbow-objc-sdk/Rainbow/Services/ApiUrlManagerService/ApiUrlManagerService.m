/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ApiUrlManagerService.h"
#import "MyUser+Internal.h"
#import "Server+Internal.h"
#import "Tools+Internal.h"
#import <CommonCrypto/CommonDigest.h>

#define ApiCurrentVersion @"v1.0"

@interface ApiUrlManagerService ()
@property (nonatomic, strong) MyUser *myUser;
@end

@implementation ApiUrlManagerService

-(instancetype) initWithMyUser:(MyUser *) myUser {
    self = [super init];
    if(self){
        _myUser = myUser;
    }
    return self;
}

-(void) dealloc {
    _myUser = nil;
}

-(NSURL *) getRootURL {
    return [NSURL URLWithString:[self getBaseURLForServer:_myUser.server]];
}

-(NSURL *) getURLForService:(ApiServices) service, ... {
    va_list args;
    va_start(args, service);
    NSURL *url = [self getURLWithServer:_myUser.server forService:service arguments:args];
    va_end(args);
    return url;
}

-(NSURL *) getURLWithServer:(Server *) server forService:(ApiServices) service, ... {
    va_list args;
    va_start(args, service);
    NSURL *url = [self getURLWithServer:server forService:service arguments:args];
    va_end(args);
    return url;
}

-(NSURL *) getURLWithServer:(Server *) server forService:(ApiServices) service arguments:(va_list) args {
    NSString *urlAsString = @"";
    switch (service) {
        case ApiServicesAuthenticationLogin: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/authentication/%@/login",[self getBaseURLForServer:_myUser.server],ApiCurrentVersion];
            break;
        }
        case ApiServicesAuthenticationLogout:{
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/authentication/%@/logout",[self getBaseURLForServer:_myUser.server],ApiCurrentVersion];
            break;
        }
        case ApiServicesDirectory: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/search",[self getBaseURLForServer:_myUser.server], ApiCurrentVersion];
            break;
        }
        case ApiServicesSources: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/sources",[self getBaseURLForServer:_myUser.server], ApiCurrentVersion,_myUser.userID];
            break;
        }
        case ApiServicesUsers: {
            // User URL should be completed with 1 va_args : the userid
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%%@", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesMyUsers: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@",[self getBaseURLForServer:_myUser.server], ApiCurrentVersion,_myUser.userID];
            break;
        }
        case ApiServicesMyUserSettings:{
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/settings",[self getBaseURLForServer:_myUser.server], ApiCurrentVersion,_myUser.userID];
            break;
        }
        case ApiServicesContacts: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/sources/%@/contacts",[self getBaseURLForServer:_myUser.server], ApiCurrentVersion, _myUser.userID, _myUser.source.serverSourceId];
            break;
        }
        case ApiServicesConversations: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/conversations",[self getBaseURLForServer:_myUser.server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesConversationDownload: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/conversations/%%@/downloads",[self getBaseURLForServer:_myUser.server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesEndUserNotificationsEmailsSelfRegister:{
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/notifications/emails/self-register",[self getBaseURLForServer:_myUser.server], ApiCurrentVersion];
            break;
        }
        case ApiServicesUsersSelfRegister:{
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/self-register",[self getBaseURLForServer:_myUser.server], ApiCurrentVersion];
            break;
        }
        case ApiServicesAvatar: {
            urlAsString = [NSString stringWithFormat:@"%@/api/avatar/%%@?size=%%@", server.isAllInOne?[self getBaseURLForServer:server]:[self getCDNBaseURLForServer:server]];
            break;
        }
        case ApiServicesEndUserAvatar: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/avatar", [self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesEndUserNotificationEmailsResetPassword: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/notifications/emails/reset-password",[self getBaseURLForServer:_myUser.server], ApiCurrentVersion];
            break;
        }
        case ApiServicesEndUserResetPassword: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/reset-password", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesEndUserChangePassword: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/change-password", [self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesCalendarAutomaticReply: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/calendar/%@/automatic_reply?userid=%%@", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRooms: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRoomsDetail:{
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms/%%@", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRoomsUsers: {
            // URL should be completed with 1 va_args : the room id
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms/%%@/users", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRoomsChangeUserData: {
            // URL should be completed with 2 va_args : the room id and the user id
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms/%%@/users/%%@", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRoomsShareConference: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms/%%@/conferences", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRoomsUnshareConference: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms/%%@/conferences/%%@", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRoomsConferenceInvitations: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms/%%@/conferences/%%@/invitations", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // should be completed with 2 va_args : the room id and the conference id
        case ApiServicesRoomsSharedConference: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms/%%@/conferences/%%@", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRoomsConference: {
            // should be completed with 2 va_args : the user id and the room id
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms?format=full&userId=%%@&confId=%%@", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // should be completed with 2 va_args : the user id
        case ApiServicesRoomsConferences: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms?format=full&userId=%%@&hasConf=true", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRoomsInstantConferences: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms?format=full&userId=%%@&scheduled=false", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRoomsScheduledConferences: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms?format=full&userId=%%@&scheduled=true", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRoomsAvatar: {
            urlAsString = [NSString stringWithFormat:@"%@/api/room-avatar/%%@?size=%%@", server.isAllInOne?[self getBaseURLForServer:server]:[self getCDNBaseURLForServer:server]];
            break;
        }
        case ApiServicesRoomsChangeAvatar: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms/%%@/avatar", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesRoomsCustomData: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/rooms/%%@/custom-data", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesBots: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/bots", [self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesAbout:{
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/admin/%@/about",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesGroups:{
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%%@/groups",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesInvite: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/invitations",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesInviteBulk: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/invitations/bulk",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesReceivedInvitations: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/invitations/received",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesSentInvitations: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/invitations/sent",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesInvitationDetails: {
            // URL should be completed with 1 va_args : the invitation ID
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/invitations/%%@",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesInvitationAccept: {
            // URL should be completed with 1 va_args : the invitation ID
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/invitations/%%@/accept",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesInvitationDecline: {
            // URL should be completed with 1 va_args : the invitation ID
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/invitations/%%@/decline",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesIceServers: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/geolocation/%@/settings/iceservers?nbServers=%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesCompanies: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/companies",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesJoinCompaniesRequest: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/join-companies/requests",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesJoinCompaniesInvitations:{
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/join-companies/invitations?format=full",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesJoinCompaniesInvitationAccept: {
            // URL should be completed with 1 va_args : the invitation ID
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/join-companies/invitations/%%@/accept",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesJoinCompaniesInvitationDecline: {
            // URL should be completed with 1 va_args : the invitation ID
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/join-companies/invitations/%%@/decline",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesFileStorage: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/filestorage/%@/files",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesFileStorageBase :{
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/filestorage/%@/users",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesFileServer: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/fileserver/%@/files",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesFileCapabilities: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/fileserver/%@/capabilities",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesProfiles: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/profiles",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesConferences: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/conference/%@/conferences/%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the confId
        case ApiServicesConferencesSnapshot: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/conference/%@/conferences/%%@/snapshot",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the confId
        case ApiServicesConferencesStart: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/conference/%@/conferences/%%@/start",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the confId
        case ApiServicesConferencesStop: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/conference/%@/conferences/%%@/stop",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesConferencesJoin: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/conference/%@/conferences/%%@/join",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesConferencesUpdateState: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/conference/%@/conferences/%%@/update",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesConferencesUpdateParticipantState: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/conference/%@/conferences/%%@/participants/%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the query string
        case ApiServicesConfProvisioningConferences: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/confprovisioning/%@/conferences/%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the confId
        case ApiServicesConfProvisioningConferencesDelete: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/confprovisioning/%@/conferences/%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the userId
        case ApiServicesConfProvisioningUsers: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/confprovisioning/%@/users?userId=%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesSearch: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/search/%@/users",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesSearchPBXPhonebook: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/search/%@/phonebooks",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesBanner:{
            urlAsString = [NSString stringWithFormat:@"%@/api/banner/%%@?size=%%u",server.isAllInOne?[self getBaseURLForServer:server]:[self getCDNBaseURLForServer:server]];
            break;
        }
        case ApiServicesSearchActiveDirectory:{
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/office365/%@/users/%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesApplicationsAuthentificationLogin: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/applications/%@/authentication/login",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesConferencesSubscribe: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/conference/%@/conferences/%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesChannelsCreateChannel: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/channels/%@/channels",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesChannelsFindChannels: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/channels/%@/channels/search",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the channelId
        case ApiServicesChannelsGetChannel: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/channels/%@/channels/%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesChannelsGetMyChannels: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/channels/%@/channels",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the channelId
        case ApiServicesChannelsPublish: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/channels/%@/channels/%%@/publish",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the channelId
        case ApiServicesChannelsSubscribe: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/channels/%@/channels/%%@/subscribe",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the channelId
        case ApiServicesChannelsUpdateChannel: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/channels/%@/channels/%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the channelId
        case ApiServicesChannelsGetItemsFromChannel: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/channels/%@/channels/%%@/items",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 1 va_args : the channelId
        case ApiServicesChannelsGetFirstUsersFromChannel: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/channels/%@/channels/%%@/users?format=full",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        // User URL should be completed with 2 va_args : the channelId and the offset in the list
        case ApiServicesChannelsGetNextUsersFromChannel: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/channels/%@/channels/%%@/users?format=full&offset=%%d",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
       
        case ApiServicesWebrtcmetrics: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/metrics/%@/webrtcmetrics",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesTelephony:{
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/telephony/%@/%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesMediaPillar: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/mediapillar/%@/mediapillars/data",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesMediaPillarNumbering: {
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/mediapillarnumbering/%@/mediapillars",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
        case ApiServicesMarkAllMessagesAsReadFroConversation: {
            // URL should be completed with 1 va_args : the conversation ID
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/%@/conversations/%%@/markallread",[self getBaseURLForServer:server], ApiCurrentVersion, _myUser.userID];
            break;
        }
        case ApiServicesRemoveUserFromMyNetwork: {
            // URL should be completed with 1 va_args : the userId
            urlAsString = [NSString stringWithFormat:@"%@/api/rainbow/enduser/%@/users/networks/%%@",[self getBaseURLForServer:server], ApiCurrentVersion];
            break;
        }
    }
    
    NSString *urlStr = [[NSString alloc] initWithFormat:urlAsString arguments:args];
    
    return [NSURL URLWithString:urlStr];
}

-(NSDictionary *) getHTTPHeaderParametersForService:(ApiServices) service {
    NSMutableDictionary *httpHeadersParameters = [NSMutableDictionary dictionary];
    switch (service) {
        case ApiServicesAuthenticationLogin: {
            [httpHeadersParameters setValue:[self basicAuthenticationString] forKey:@"Authorization"];
            if(_myUser.appID && ![_myUser.appID isEqualToString:@""] && _myUser.secretKey && ![_myUser.secretKey isEqualToString:@""]){
                [httpHeadersParameters setValue:[self basicRainbowAppAuthenticationString] forKey:@"x-rainbow-app-auth"];
            }
            NSString *clientName = [Tools isUsingSDK] ? @"sdk_ios" : @"ios";
            [httpHeadersParameters setValue:clientName forKey:@"x-rainbow-client"];
            [httpHeadersParameters setValue:[Tools applicationVersion] forKey:@"x-rainbow-client-version"];
            [self setAppToken:httpHeadersParameters];
            break;
        }
        case ApiServicesAuthenticationLogout:
        case ApiServicesDirectory:
        case ApiServicesUsers:
        case ApiServicesMyUsers:
        case ApiServicesMyUserSettings:
        case ApiServicesSources:
        case ApiServicesContacts:
        case ApiServicesConversations:
        case ApiServicesConversationDownload:
        case ApiServicesEndUserAvatar:
        case ApiServicesRooms:
        case ApiServicesRoomsDetail:
        case ApiServicesRoomsUsers:
        case ApiServicesRoomsChangeUserData:
        case ApiServicesRoomsShareConference:
        case ApiServicesRoomsUnshareConference:
        case ApiServicesRoomsConferenceInvitations:
        case ApiServicesRoomsSharedConference:
        case ApiServicesRoomsAvatar:
        case ApiServicesRoomsChangeAvatar:
        case ApiServicesRoomsCustomData:
        case ApiServicesBots:
        case ApiServicesGroups:
        case ApiServicesInvite:
        case ApiServicesInviteBulk:
        case ApiServicesReceivedInvitations:
        case ApiServicesSentInvitations:
        case ApiServicesInvitationDetails:
        case ApiServicesInvitationAccept:
        case ApiServicesInvitationDecline:
        case ApiServicesCompanies :
        case ApiServicesJoinCompaniesRequest:
        case ApiServicesJoinCompaniesInvitations:
        case ApiServicesJoinCompaniesInvitationAccept:
        case ApiServicesJoinCompaniesInvitationDecline:
        case ApiServicesFileStorage:
        case ApiServicesFileStorageBase:
        case ApiServicesFileServer:
        case ApiServicesFileCapabilities:
        case ApiServicesIceServers:
        case ApiServicesProfiles:
        case ApiServicesConferences:
        case ApiServicesConferencesSubscribe:
        case ApiServicesConferencesSnapshot:
        case ApiServicesConferencesStop:
        case ApiServicesConferencesStart:
        case ApiServicesConferencesJoin:
        case ApiServicesConferencesUpdateState:
        case ApiServicesConferencesUpdateParticipantState:
        case ApiServicesConfProvisioningConferences:
        case ApiServicesConfProvisioningConferencesDelete:
        case ApiServicesConfProvisioningUsers:
        case ApiServicesRoomsConference:
        case ApiServicesRoomsConferences:
        case ApiServicesRoomsInstantConferences:
        case ApiServicesRoomsScheduledConferences:
        case ApiServicesSearch:
        case ApiServicesSearchPBXPhonebook:
        case ApiServicesEndUserChangePassword:
        case ApiServicesCalendarAutomaticReply:
        case ApiServicesBanner:
        case ApiServicesSearchActiveDirectory:
        case ApiServicesChannelsCreateChannel:
        case ApiServicesChannelsFindChannels:
        case ApiServicesChannelsGetChannel:
        case ApiServicesChannelsGetMyChannels:
        case ApiServicesChannelsPublish:
        case ApiServicesChannelsSubscribe:
        case ApiServicesChannelsUpdateChannel:
        case ApiServicesChannelsGetItemsFromChannel:
        case ApiServicesChannelsGetFirstUsersFromChannel:
        case ApiServicesChannelsGetNextUsersFromChannel:
        case ApiServicesWebrtcmetrics:
        case ApiServicesTelephony:
        case ApiServicesMediaPillar:
        case ApiServicesMediaPillarNumbering:
        case ApiServicesMarkAllMessagesAsReadFroConversation:
        case ApiServicesRemoveUserFromMyNetwork:
        {
            [httpHeadersParameters setValue:[NSString stringWithFormat:@"Bearer %@", _myUser.token] forKey:@"Authorization"];
            [self setAppToken:httpHeadersParameters];
            break;
        }
        case ApiServicesEndUserResetPassword:
        case ApiServicesEndUserNotificationEmailsResetPassword:
        case ApiServicesUsersSelfRegister:
        case ApiServicesEndUserNotificationsEmailsSelfRegister:
        case ApiServicesAbout:
        case ApiServicesAvatar:{
            // No specific header, just the defaults and app token if set
            if(service != ApiServicesAvatar)
                [self setAppToken:httpHeadersParameters];
            break;
        }
        case ApiServicesApplicationsAuthentificationLogin:{
            [httpHeadersParameters setValue:[self basicAppAuthenticationString] forKey:@"Authorization"];
            break;
        }
    }
    [httpHeadersParameters setValue:@"application/json; charset=UTF-8" forKey:@"Content-Type"];
    [httpHeadersParameters setValue:@"keep-alive" forKey:@"Connection"];
    
    if(service == ApiServicesFileServer)
        [httpHeadersParameters setValue: @"application/octet-stream" forKey:@"Content-Type"];
    return httpHeadersParameters;
}

-(NSString *) getBaseURLForServer:(Server *) server {
    return [NSString stringWithFormat:@"%@://%@",server.isSecure?@"https":@"http",server.serverHostname];
}

-(NSString *) getCDNBaseURLForServer:(Server *) server {
    return [NSString stringWithFormat:@"%@://cdn.%@",server.isSecure?@"https":@"http",server.serverHostname];
}

- (NSString*) basicAuthenticationString {
    NSString *basicAuth = [NSString stringWithFormat:@"%@:%@", _myUser.username, _myUser.password];
    NSData *basicAuthData =  [basicAuth dataUsingEncoding:NSASCIIStringEncoding];
    NSString * base64AuthValue = [basicAuthData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
    return [NSString stringWithFormat:@"Basic %@", base64AuthValue];
}

- (NSString*) basicAppAuthenticationString {
    NSString *basicAuth = [NSString stringWithFormat:@"%@:%@", _myUser.appID, _myUser.secretKey];
    NSData *basicAuthData =  [basicAuth dataUsingEncoding:NSASCIIStringEncoding];
    NSString * base64AuthValue = [basicAuthData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
    return [NSString stringWithFormat:@"Basic %@", base64AuthValue];
}

- (NSString *)toHexString:(NSData *)data {
    NSMutableString *str = [[NSMutableString alloc] initWithString:@""];
    UInt8 c;
    for(int i=0; i<data.length; i++){
        [data getBytes:&c range:NSMakeRange(i, 1)];
        [str appendString:[NSString stringWithFormat:@"%0.2x", c]];
    }
    return str;
}

- (NSString*) basicRainbowAppAuthenticationString {
    NSString *basicAuth = [NSString stringWithFormat:@"%@%@", _myUser.secretKey, _myUser.password];
    NSData *basicAuthData =  [basicAuth dataUsingEncoding:NSASCIIStringEncoding];
    NSMutableData *basicAuthDataSHA256 = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(basicAuthData.bytes, (CC_LONG)basicAuthData.length,  basicAuthDataSHA256.mutableBytes);
    NSString *basicAuthDataSHA256Str = [self toHexString:basicAuthDataSHA256];
    NSString *appId = [[_myUser.appID stringByAppendingString:@":"] stringByAppendingString:basicAuthDataSHA256Str];
    NSString* base64AuthValue =[[appId dataUsingEncoding:NSASCIIStringEncoding] base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
    return [NSString stringWithFormat:@"Basic %@", base64AuthValue];
}

-(void) setAppToken:(NSMutableDictionary *)httpHeadersParameters {
    if(_myUser.appToken && ![_myUser.appToken isEqualToString:@""]){
        NSString *bearer = [NSString stringWithFormat:@"Bearer %@", _myUser.appToken];
        [httpHeadersParameters setValue:bearer forKey:@"x-rainbow-app-token"];
    }
}

@end
