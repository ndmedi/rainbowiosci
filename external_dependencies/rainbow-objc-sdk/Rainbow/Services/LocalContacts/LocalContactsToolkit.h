/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import <Contacts/Contacts.h>
#import "Contact.h"
#import "LocalContact.h"

/**
 ABRecordRef <-> Contact bindings.
 */
@interface LocalContactsToolkit : NSObject

+(NSArray<EmailAddress*>*) getEmailAddressFromContact:(CNContact *)contact;
+(NSArray<PhoneNumber*>*) getPhoneNumbersFromContact:(CNContact *)contact;

+(void) fillLocalContact:(LocalContact*) contact withContact:(CNContact *) record;
+(NSString *) getISO3611Alpha3CodeFor:(NSString *) alpha2Code;
@end
