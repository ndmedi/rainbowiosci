/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "Contact.h"
#import <Contacts/Contacts.h>

#define kLocalContactsAccessGrantedKey @"accessGranted"
#define kLocalContactsModification @"localContactModified"
#define kLocalContactsDisplayFirstNameFirstKey @"displayFirstNameFirst"
#define kLocalContactsSortByNameKey @"sortByFirstName"

@class ContactsManagerService;

@interface LocalContacts : NSObject
@property (nonatomic, weak) ContactsManagerService *contactsManagerService;
@property (nonatomic, readonly) BOOL accessGranted;
@property (nonatomic, readonly) BOOL displayFirstNameFirst;
@property (nonatomic, readonly) BOOL sortByFirstName;

-(void) start;
-(void) stop;
-(void) requestAddressBookAccess;
-(CNAuthorizationStatus) getCurrentAddressBookAuthorization;

@end
