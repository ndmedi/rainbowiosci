/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ChannelsService.h"
#import "ChannelsService+Internal.h"
#import "Channel+Internal.h"
#import "ChannelPayload+Internal.h"
#import "ChannelUser+Internal.h"
#import "DownloadManager.h"
#import "LoginManager.h"
#import "LoginManager+Internal.h"
#import "NSDictionary+JSONString.h"
#import "NSData+JSON.h"
#import "Tools.h"
#import "XMPPServiceProtocol.h"

NSString *const kChannelsServiceDidReceiveItem = @"channelsServiceDidReceiveItem";
NSString *const kChannelsServiceDidRetractItem = @"channelsServiceDidRetractItem";
NSString *const kChannelsServiceDidDeleteChannel = @"channelsServiceDidDeleteChannel";

@interface ChannelDescription ()
@property (nonatomic, strong, readwrite) NSString *name;
@property (nonatomic, strong, readwrite) NSString *topic;
@property (nonatomic, strong, readwrite) NSString *id;
@property (nonatomic, readwrite) ChannelUserType userType;
@property (nonatomic, strong, readwrite) NSString *creatorId;
-(instancetype) initWithName:(NSString *)name topic:(NSString *)topic id:(NSString *)id userType:(ChannelUserType)userType;
-(instancetype) initWithName:(NSString *)name topic:(NSString *)topic id:(NSString *)id userType:(ChannelUserType)userType creatorId:(NSString *)creatorId;
@end

@implementation ChannelDescription
-(instancetype) initWithName:(NSString *)name topic:(NSString *)topic id:(NSString *)id userType:(ChannelUserType)userType{
    return [self initWithName:name topic:topic id:id userType:userType creatorId:nil];
}

-(instancetype) initWithName:(NSString *)name topic:(NSString *)topic id:(NSString *)id userType:(ChannelUserType)userType  creatorId:(NSString *)creatorId{
    self = [super init];
    if(self){
        _name = name;
        _topic = topic;
        _id = id;
        _userType = userType;
        _creatorId = creatorId;
    }
    return self;
}
@end

@interface ChannelsService () <XMPPServiceChannelsDelegate>
    @property (nonatomic, strong) XMPPService *xmppService;
    @property (nonatomic, strong) DownloadManager *downloadManager;
    @property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;

    @property (nonatomic, strong) NSMutableDictionary<NSString *, Channel *> *channels;
@end

@implementation ChannelsService

-(instancetype) initWithXMPPService:(XMPPService *) xmppService downloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService  {
    self = [super init];
    if(self){
        _xmppService = xmppService;
        _xmppService.channelsDelegate = self;
        _downloadManager = downloadManager;
        _apiUrlManagerService = apiUrlManagerService;
        _channels = [[NSMutableDictionary alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    
    _channels = nil;
    _xmppService.channelsDelegate = nil;
    _xmppService = nil;
    _downloadManager = nil;
    _apiUrlManagerService = nil;
}

-(void) didLogin:(NSNotification *) notification {
    [self getMyChannelsWithCompletionHandler:^(NSArray<ChannelDescription *> *channelDescriptions, NSError *error) {
        for(ChannelDescription *channelDescr in channelDescriptions){
            [self getChannelById:channelDescr.id completionHandler:^(Channel *channel, NSError *error) {
                if(!error){
                    NSAssert(channel, @"[ChannelsService] didLogin: channel should not be nil");
                    if(channel){
                        if(!self.channels[channel.id]){
                            ((NSMutableDictionary *)self.channels)[channel.id] = channel;
                        }
                        [self get:MAX_ITEMS_IN_CHANNEL itemsFromChannel:channel completionHandler:^(NSInteger availableItemsCount, NSArray<ChannelPayload *> *items, NSError *error) {
                            if(!error){
                                for(ChannelPayload *item in items){
                                    [self updateChannelWithItem:item];
                                }
                            }
                        }];
                    } else {
                        NSLog(@"[ChannelsService] didLogin: channel should not be nil");
                    }
                }
            }];
        }
    }];
}

-(void) didLogout:(NSNotification *) notification {
    self.channels = [[NSMutableDictionary alloc] init];
}

-(NSString *)channelIdFromNodeName:(NSString *)channel {
    NSArray *parts = [channel componentsSeparatedByString:@":"];
    if([parts count]>0){
        return [parts objectAtIndex: 0];
    } else {
        return nil;
    }
}

-(Channel *) createChannelFromJson:(NSDictionary *) jsonDictionary {
    Channel *channel = [[Channel alloc] init];
    channel.name = (NSString *)[jsonDictionary objectForKey:@"name"];
    channel.topic = (NSString *)[jsonDictionary objectForKey:@"topic"];
    channel.maxItems = [[jsonDictionary objectForKey:@"max_items"] intValue];
    channel.maxPayloadSize = [[jsonDictionary objectForKey:@"max_payload_size"] intValue];
    channel.visibility = [Channel channelVisibilityFromString:[jsonDictionary objectForKey:@"visibility"]];
    channel.id = (NSString *)[jsonDictionary objectForKey:@"id"];
    channel.creatorId = (NSString *)[jsonDictionary objectForKey:@"creatorId"];
    channel.companyId = (NSString *)[jsonDictionary objectForKey:@"companyId"];
    channel.creationDate = (NSDate *)[Tools dateFromString:[jsonDictionary objectForKey:@"creationDate"] withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
    channel.usersCount = [[jsonDictionary objectForKey:@"users_count"] intValue];
    
    return channel;
}

-(void) createChannelWithName:(NSString *)name topic:(NSString *)topic visibility:(ChannelVisibility)visibility maxItems:(int)maxItems maxPayloadSize:(int)maxPayloadSize completionHandler:(ChannelsServiceGetChannelCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsCreateChannel];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsCreateChannel];
    NSString *visibilityStr = [Channel stringFromChannelVisibility:visibility];
    NSMutableDictionary *bodyContent = [NSMutableDictionary dictionaryWithDictionary:@{@"name":name, @"visibility":visibilityStr}];
    if(topic){
        [bodyContent setObject:topic forKey:@"topic"];
    }
    if(maxItems>0){
        [bodyContent setObject:[NSNumber numberWithInt: maxItems] forKey:@"max_items"];
    }
    if(maxPayloadSize>0){
        [bodyContent setObject:[NSNumber numberWithInt: maxPayloadSize] forKey:@"max_payload_size"];
    }

    NSLog(@"Create %@ channel with name %@", visibilityStr, name);
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        Channel *channel = nil;
        if (error) {
            NSLog(@"Create channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        } else {
            NSDictionary *data = [receivedData objectFromJSONData];
            NSLog(@"Create channel succeeded, %@", data);
            if(data[@"data"]){
                channel = [self createChannelFromJson:data[@"data"]];
            }
        }
        if(completionHandler){
            completionHandler(channel, error);
        }
    }];
}

-(void) deleteChannelWithId:(NSString *)id completionHandler: (ChannelsServiceCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetChannel, id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetChannel];
    NSLog(@"Delete channel with id:%@",id);
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error){
           NSLog(@"Create channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

-(void)getMyChannelsWithCompletionHandler: (ChannelsServiceGetMyChannelsCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetMyChannels];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetMyChannels];
    NSLog(@"Get my channels");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error){
            if(completionHandler){
                completionHandler(nil, error);
            }
        } else {
            NSDictionary *data = [receivedData objectFromJSONData];
            NSMutableArray *channels = [[NSMutableArray alloc] init];
            for(NSDictionary *descr in data[@"data"]){
                ChannelDescription *channelDescription = [[ChannelDescription alloc] initWithName:descr[@"name"] topic:descr[@"topic"] id:descr[@"id"] userType:[Channel channelUserTypeFromString:descr[@"type"]]];
                [channels addObject: channelDescription];
            }
            if(completionHandler){
                completionHandler(channels, error);
            }
        }
    }];
}

-(void)getChannelById:(NSString *)id completionHandler: (ChannelsServiceGetChannelCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetChannel, id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetChannel];
    NSLog(@"Get channel by Id");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        Channel *channel = nil;
        if(error){
            NSLog(@"Get channel by id returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        } else {
            NSDictionary *data = [receivedData objectFromJSONData];
            if(data[@"data"]){
                channel = [self createChannelFromJson:data[@"data"]];
            }
        }
        if(completionHandler){
            completionHandler(channel, error);
        }
    }];
}

-(void)publishMessageToChannel:(Channel *)channel title:(NSString *)title message:(NSString *)message url:(NSString *)link completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsPublish, channel.id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsPublish];
    NSMutableDictionary *payload = [NSMutableDictionary dictionaryWithDictionary:@{@"title":title, @"message":message}];
    if(link){
        [payload setObject:link forKey:@"url"];
    }
    NSDictionary *bodyContent = @{@"payload":payload};
    NSLog(@"Publish to channel");
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"Publish to channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

-(void)subscribeToChannel:(Channel *)channel completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsSubscribe, channel.id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsSubscribe];
    NSDictionary *bodyContent = @{};
    NSLog(@"Subscribe to channel");
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"Subscribe to channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

-(void)unsubscribeToChannel:(Channel *)channel completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsSubscribe, channel.id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsSubscribe];
    NSDictionary *bodyContent = @{};
    NSLog(@"Unsubscribe to channel");
    [_downloadManager deleteRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"Unsubscribe to channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

-(void)get:(NSInteger)count itemsFromChannel:(Channel *)channel completionHandler:(ChannelsServiceGetItemsCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetItemsFromChannel, channel.id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetItemsFromChannel];
    if(count >= 0){
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?max=%ld", url, count]];
    }
    NSLog(@"Get items from channel");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSInteger availableItemsCount = 0;
        NSMutableArray<ChannelPayload *> *items = nil;
        if (error) {
            NSLog(@"Subscribe to channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        } else {
            items = [[NSMutableArray alloc] init];
            NSDictionary *data = [receivedData objectFromJSONData];
            if(data[@"data"]){
                availableItemsCount = [data[@"data"][@"status"][@"count"] integerValue];
                if(count>0){
                    for(NSDictionary *item in data[@"data"][@"items"]){
                        NSString *id = item[@"id"];
                        NSDictionary *payload = item[@"entry"][@"payload"][0];
                        NSString *title = payload[@"title"][0];
                        NSString *message = payload[@"message"][0];
                        NSString *url = payload[@"url"][0];
                        ChannelPayload *channelPayload = [[ChannelPayload alloc] init];
                        channelPayload.channelId = channel.id;
                        channelPayload.id = id;
                        channelPayload.title = title;
                        channelPayload.message = message;
                        channelPayload.url = url;
                        [items addObject:channelPayload];
                    }
                }
            }
        }
        if(completionHandler){
            completionHandler(availableItemsCount, items, error);
        }
    }];
}

-(ChannelUser *)channelUserFromDict:(NSDictionary *)userDict {
    ChannelUser *user = [[ChannelUser alloc] init];
    user.rainbowId = userDict[@"id"];
    user.type = [Channel channelUserTypeFromString:userDict[@"type"]];
    user.additionDate = [Tools dateFromString:userDict[@"additionDate"] withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
    user.loginEmail = userDict[@"loginEmail"];
    user.displayName = userDict[@"displayName"];
    user.companyId = userDict[@"companyId"];
    user.companyName = userDict[@"companyName"];
    
    return user;
}

-(void)getFirstUsersFromChannel:(Channel *)channel completionHandler:(ChannelsServiceGetUsersCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetFirstUsersFromChannel, channel.id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetFirstUsersFromChannel];
    NSLog(@"Get first users from a channel");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSMutableArray<ChannelUser *> *users = nil;
        if(!error){
            NSDictionary *data = [receivedData objectFromJSONData];
            if(data[@"data"]){
                users = [[NSMutableArray alloc] init];
                for(NSDictionary *userDict in data[@"data"]){
                    ChannelUser *user = [self channelUserFromDict:userDict];
                    if(user){
                        [users addObject:user];
                    }
                }
            }
        } else {
            NSLog(@"Get first users from channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(users, error);
        }
    }];
}

-(void)getNextUsersFromChannel:(Channel *)channel atIndex:(NSInteger)index completionHandler:(ChannelsServiceGetUsersCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetNextUsersFromChannel, channel.id, index];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetNextUsersFromChannel];
    NSLog(@"Get next users from a channel starting at index %ld", (long)index);
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSMutableArray<ChannelUser *> *users = nil;
        if(!error){
            NSDictionary *data = [receivedData objectFromJSONData];
            if(data[@"data"]){
                users = [[NSMutableArray alloc] init];
                for(NSDictionary *userDict in data[@"data"]){
                    ChannelUser *user = [self channelUserFromDict:userDict];
                    if(user){
                        [users addObject:user];
                    }
                }
            }
        } else {
            NSLog(@"Get next users from channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(users, error);
        }
    }];
}

-(void)findChannelByName:(NSString *)name limit:(NSInteger)limit completionHandler:(ChannelsServiceFindChannelsCompletionHandler) completionHandler {
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesChannelsFindChannels];
    NSURL *url;
    if(limit >= 0){
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?name=%@&limit=%ld", serviceUrl, name, limit]];
    } else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?name=%@", serviceUrl, name]];
    }
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsFindChannels];
    NSLog(@"Find channel by name using '%@'", name);
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error){
            if(completionHandler){
                completionHandler(nil, error);
            }
        } else {
            NSDictionary *data = [receivedData objectFromJSONData];
            NSMutableArray *channels = [[NSMutableArray alloc] init];
            for(NSDictionary *descr in data[@"data"]){
                ChannelDescription *channelDescription = [[ChannelDescription alloc] initWithName:descr[@"name"] topic:descr[@"topic"] id:descr[@"id"] userType:ChannelUserTypeNone creatorId:descr[@"creatorId"]];
                [channels addObject: channelDescription];
            }
            if(completionHandler){
                completionHandler(channels, error);
            }
        }
    }];
}

-(void)findChannelByTopic:(NSString *)topic limit:(NSInteger)limit completionHandler:(ChannelsServiceFindChannelsCompletionHandler) completionHandler {
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesChannelsFindChannels];
    NSURL *url;
    if(limit >= 0){
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?topic=%@&limit=%ld", serviceUrl, topic, limit]];
    } else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?topic=%@", serviceUrl, topic]];
    }
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsFindChannels];
    NSLog(@"Find channel by topic using '%@'", topic);
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error){
            if(completionHandler){
                completionHandler(nil, error);
            }
        } else {
            NSDictionary *data = [receivedData objectFromJSONData];
            NSMutableArray *channels = [[NSMutableArray alloc] init];
            for(NSDictionary *descr in data[@"data"]){
                ChannelDescription *channelDescription = [[ChannelDescription alloc] initWithName:descr[@"name"] topic:descr[@"topic"] id:descr[@"id"] userType:ChannelUserTypeNone creatorId:descr[@"creatorId"]];
                [channels addObject: channelDescription];
            }
            if(completionHandler){
                completionHandler(channels, error);
            }
        }
    }];
}

-(void)updateChannelWithId:(NSString *)id topic:(NSString *)topic visibility:(ChannelVisibility)visibility maxItems:(NSInteger)maxItems maxPayloadSize:(NSInteger)maxPayloadSize completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesChannelsUpdateChannel, id];
    NSMutableString *query = [NSMutableString stringWithString:@"%@?"];
    if(topic){
        [query appendString:[NSString stringWithFormat:@"topic=%@", topic]];
    }
    if(visibility>=0){
        if(topic){
            [query appendString:@"&"];
        }
        [query appendString:[NSString stringWithFormat:@"visibility=%@", [Channel stringFromChannelVisibility:visibility]]];
    }
    if(maxItems>=0){
        if(topic || visibility>=0){
            [query appendString:@"&"];
        }
        [query appendString:[NSString stringWithFormat:@"max_items=%ld", maxItems]];
    }
    if(maxPayloadSize>=0){
        if(topic || visibility>=0 || maxItems>=0){
            [query appendString:@"&"];
        }
        [query appendString:[NSString stringWithFormat:@"max_payload_size=%ld", maxPayloadSize]];
    }
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:query, serviceUrl]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsUpdateChannel];
    NSLog(@"Update channel");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

-(void)updateChannelWithId:(NSString *)id topic:(NSString *)topic completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    [self updateChannelWithId:id topic:topic visibility:-1 maxItems:-1 maxPayloadSize:-1 completionHandler:completionHandler];
}

-(void)updateChannelWithId:(NSString *)id visibility:(ChannelVisibility)visibility completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    [self updateChannelWithId:id topic:nil visibility:visibility maxItems:-1 maxPayloadSize:-1 completionHandler:completionHandler];
}

-(void)updateChannelWithId:(NSString *)id maxItems:(NSUInteger)maxItems completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    [self updateChannelWithId:id topic:nil visibility:-1 maxItems:maxItems maxPayloadSize:-1 completionHandler:completionHandler];
}

-(void)updateChannelWithId:(NSString *)id maxPayloadSize:(NSUInteger)maxPayloadSize completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    [self updateChannelWithId:id topic:nil visibility:-1 maxItems:-1 maxPayloadSize:maxPayloadSize completionHandler:completionHandler];
}

#pragma mark - XMPPServiceChannelsDelegate protocol

-(void) updateChannelWithItem:(ChannelPayload *)item {
    if(item.id){
        @synchronized(self.channels){
            NSString *channelId = item.channelId;
            Channel *channel = ((NSMutableDictionary *)self.channels)[channelId];
            if(item.message){
                if(!channel){
                    [self getChannelById:item.channelId completionHandler:^(Channel *newChannel, NSError *error) {
                        if(!error){
                            ((NSMutableDictionary *)self.channels)[channelId] = newChannel;
                            [newChannel addOrUpdateItem: item];
                            [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidReceiveItem object:item];
                        }
                    }];
                } else {
                    [channel addOrUpdateItem: item];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidReceiveItem object:item];
                }
            } else {
                [channel removeItem:item];
                [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidRetractItem object:item];
            }
        }
    }
}

-(Channel *)findChannelWithItemId:(NSString *)id {
    for(Channel *channel in [self.channels allValues]){
        for(ChannelPayload *item in channel.items){
            if([id isEqualToString: item.id]){
                return channel;
            }
        }
    }
    return nil;
}

-(void) xmppService:(XMPPService *) service didReceiveItems:(NSArray<NSDictionary *> *) items {
    for(NSDictionary *item in items){
        ChannelPayload *payload = [[ChannelPayload alloc] init];
        if(item[@"retractId"]){
            payload.id = item[@"retractId"];
            // The node name is in the form: channelId:channelName
            NSString *channelId = [self channelIdFromNodeName:item[@"node"]];
            payload.channelId = channelId;
        } else {
            payload.id = item[@"id"];
            payload.channelId = item[@"channelId"];
            payload.title = item[@"title"];
            payload.message = item[@"message"];
            payload.url = item[@"url"];
        }
        [self updateChannelWithItem:payload];
    }
}

-(void) xmppService:(XMPPService *) service didDeleteChannel:(NSString *) channelID {
    @synchronized(self.channels){
        if(channelID){
            ((NSMutableDictionary *)self.channels)[channelID] = nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidDeleteChannel object:channelID];
        } else {
            NSLog(@"Error didDeleteChannel without a channelID");
        }
    }
}

@end
