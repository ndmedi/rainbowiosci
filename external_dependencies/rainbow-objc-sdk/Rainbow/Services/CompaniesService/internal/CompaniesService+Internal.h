/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

#import "ApiUrlManagerService.h"
#import "DownloadManager.h"
#import "XMPPService.h"
#import "MyUser.h"
#import "ContactsManagerService.h"
#import "LoginManager.h"

@interface CompaniesService ()
@property (nonatomic) BOOL cacheLoaded;

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService xmppService:(XMPPService *) xmppService myUser:(MyUser *) myUser contactManager:(ContactsManagerService *) contactManager loginManager:(LoginManager *) loginManager;

-(void) createOrUpdateCompanyInvitationWithId:(NSString *) companyInvitationId direction:(NSString *) direction status:(NSString *) status;
-(void) updateCompanyInvitationJoinWithId:(NSString *) companyInvitationId direction:(NSString *) direction status:(NSString *) status;
-(void) removeCompanyInvitationWithID:(NSString *) companyInvitationId;
@end
