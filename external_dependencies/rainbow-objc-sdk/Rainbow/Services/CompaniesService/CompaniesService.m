/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CompaniesService.h"
#import "CompaniesService+Internal.h"
#import "NSData+JSON.h"
#import "Company+Internal.h"
#import "NSDictionary+JSONString.h"
#import "CompanyInvitation+Internal.h"
#import "NSObject+NotNull.h"
#import "NSDate+JSONString.h"
#import "ContactsManagerService+Internal.h"
#import "MyUser+Internal.h"
#import "NSDate+JSONString.h"
#import "NSString+MD5.h"
#import "NSString+URLEncode.h"
#import "LoginManager+Internal.h"
#import "PINCache.h"

NSString *const kCompaniesServiceDidAddCompanyInvitation = @"didAddCompanyInvitation";
NSString *const kCompaniesServiceDidUpdateCompanyInvitation = @"didUpdateCompanyInvitation";
NSString *const kCompaniesServiceDidRemoveCompanyInvitation = @"didRemoveCompanyInvitation";
NSString *const kCompaniesServiceDidUpdateCompanyInvitationPendingNumber = @"didUpdateCompanyInvitationPendingNumber";
NSString *const kCompaniesServiceDidUpdateCompany = @"didUpdateCompany";

typedef void (^CompaniesServiceFetchAvatarComplionHandler) (Company *conversation, NSError *error);

@interface CompaniesService ()
@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) MyUser *myUser;
@property (nonatomic, strong) ContactsManagerService *contactManager;
@property (nonatomic, strong) LoginManager *loginManager;
@property (nonatomic, strong) NSMutableArray <Company *> *companies;
@property (nonatomic, strong) NSMutableArray <CompanyInvitation *> *companiesInvitations;
@property (nonatomic, strong) PINCache *companyiesInvitationsCache;
@end

@implementation CompaniesService

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService xmppService:(XMPPService *) xmppService myUser:(MyUser *) myUser contactManager:(ContactsManagerService *) contactManager loginManager:(LoginManager *) loginManager {
    self = [super init];
    if(self){
        _downloadManager = downloadManager;
        _apiUrlManagerService = apiUrlManagerService;
        _xmppService = xmppService;
        _myUser = myUser;
        _contactManager = contactManager;
        _loginManager = loginManager;
        _companies = [NSMutableArray new];
        _companiesInvitations = [NSMutableArray array];
        
        _companyiesInvitationsCache = [[PINCache alloc] initWithName:@"companiesInvitations"];
        _companyiesInvitationsCache.memoryCache.removeAllObjectsOnEnteringBackground = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willLogin:) name:kLoginManagerInternalWillLogin object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeServer:) name:kLoginManagerDidChangeServer object:nil];
    }
    return self;
}

-(void) dealloc {
    _cacheLoaded = NO;
    _xmppService = nil;
    _apiUrlManagerService = nil;
    _downloadManager = nil;
    _myUser = nil;
    _contactManager = nil;
    _loginManager = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalWillLogin object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeServer object:nil];
    
    [_companies removeAllObjects];
    _companies = nil;
    
    [_companiesInvitations removeAllObjects];
    _companiesInvitations = nil;
    
    _companyiesInvitationsCache = nil;
}

#pragma mark - Login notifications

-(void) willLogin:(NSNotification *) notification {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        if (!_cacheLoaded) {
            [self loadCompaniesInvitationsFromCache];
            _cacheLoaded = NO;
        }
    });
}

-(void) didLogin:(NSNotification *) notification {
    [self fetchCompaniesInvitations];
    [self fetchCompaniesInvitationsRequests];
}

-(void) didLogout:(NSNotification *) notification {
    @synchronized (_companiesInvitations) {
        [_companiesInvitations removeAllObjects];
    }
    @synchronized (_companies) {
        [_companies removeAllObjects];
    }
}

-(void) didChangeUser:(NSNotification *) notification {
    [_companyiesInvitationsCache removeAllObjectsAsync:nil];
}

-(void) didChangeServer:(NSNotification *) notification {
    [_companyiesInvitationsCache removeAllObjectsAsync:nil];
}

-(void) loadCompaniesInvitationsFromCache {
    __weak __typeof__(self) weakSelf = self;
    
    NSMutableArray *exisintKeys = [NSMutableArray array];
    [_companyiesInvitationsCache.diskCache enumerateObjectsWithBlockAsync:^(NSString * key, NSURL * fileURL, BOOL * stop) {
        [exisintKeys addObject:key];
    } completionBlock:^(id<PINCaching> cache) {
        NSLog(@"Companies Invitations cache : Load cache completion block start");
        for (NSString *key in exisintKeys) {
            NSDictionary *object = [_companyiesInvitationsCache objectForKey:key];
            CompanyInvitation *companyInvitation = [weakSelf createOrUpdateCompanyInvitationFromDictionary:object];            
            __weak __typeof(companyInvitation) weakCompanyInvitation = companyInvitation;
            [_contactManager createRainbowContactAsynchronouslyWithRainbowID:object[@"invitingAdminId"] withCompletionHandler:^(Contact *contact) {
                weakCompanyInvitation.peer = contact;
                [_contactManager updateRainbowContact:contact withCompanyInvitation:weakCompanyInvitation];
                [_companyiesInvitationsCache setObjectAsync:[weakCompanyInvitation dictionaryRepresentation] forKey:weakCompanyInvitation.invitationID completion:nil];
                NSLog(@"Adding contact information %@ in company invitation %@", contact, weakCompanyInvitation);
                [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitation object:weakCompanyInvitation];
            }];
        }
        NSLog(@"Companies Invitations cache : Load cache completion block end");
    }];
}

-(void) fetchCompaniesInvitations {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesJoinCompaniesInvitations];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesJoinCompaniesInvitations];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get company invitations : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get company invitations due to JSON parsing failure");
            return;
        }
        
        NSDictionary *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get company invitation, response ... %@", data);
        NSMutableArray<CompanyInvitation*> *listOfInvitations = [NSMutableArray array];
        for (NSDictionary *companyInvitationDict in data) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:companyInvitationDict];
            [dic setObject:@"received" forKey:@"direction"];
            CompanyInvitation *companyInvitation = [self createOrUpdateCompanyInvitationFromDictionary:dic];
            [listOfInvitations addObject:companyInvitation];
            
            __weak __typeof(companyInvitation) weakCompanyInvitation = companyInvitation;
            [_contactManager createRainbowContactAsynchronouslyWithRainbowID:dic[@"invitingAdminId"] withCompletionHandler:^(Contact *contact) {
                weakCompanyInvitation.peer = contact;
                [_contactManager updateRainbowContact:contact withCompanyInvitation:weakCompanyInvitation];
                [_companyiesInvitationsCache setObjectAsync:[weakCompanyInvitation dictionaryRepresentation] forKey:weakCompanyInvitation.invitationID completion:nil];
                NSLog(@"Adding contact information %@ in company invitation %@", contact, weakCompanyInvitation);
                [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitation object:weakCompanyInvitation];
            }];
            NSLog(@"Company Invitation : %@", companyInvitation);
        }
        
        [listOfInvitations enumerateObjectsUsingBlock:^(CompanyInvitation * companyInvitation, NSUInteger idx, BOOL * stop) {
            [self fetchCompanyDetails:companyInvitation.company];
            [self fetchAvatarDataForCompany:companyInvitation.company];
            
        }];
    }];
}

-(void) fetchCompaniesInvitationsRequests {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesJoinCompaniesRequest];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesJoinCompaniesRequest];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get company invitations : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get company invitations due to JSON parsing failure");
            return;
        }
        
        NSDictionary *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get company invitation requests, response ...");
        NSMutableArray<CompanyInvitation*> *listOfInvitations = [NSMutableArray array];
        for (NSDictionary *companyInvitationDict in data) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:companyInvitationDict];
            [dic setObject:@"sent" forKey:@"direction"];
            CompanyInvitation *companyInvitation = [self createOrUpdateCompanyInvitationFromDictionary:dic];
            [listOfInvitations addObject:companyInvitation];
            
            NSLog(@"Company Invitation Requested : %@", companyInvitation);
        }
        
        [listOfInvitations enumerateObjectsUsingBlock:^(CompanyInvitation * companyInvitation, NSUInteger idx, BOOL * stop) {
            [self fetchCompanyDetails:companyInvitation.company];
            [self fetchAvatarDataForCompany:companyInvitation.company];
        }];
    }];
}

-(void) fetchCompanyInvitationDetailsForCompanyInvitation:(CompanyInvitation *) companyInvitation {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",[_apiUrlManagerService getURLForService:ApiServicesJoinCompaniesInvitations], companyInvitation.invitationID]];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesJoinCompaniesInvitations];
    
    __weak __typeof(self) weakSelf = self;
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get company invitation details : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get company invitation detail due to JSON parsing failure");
            return;
        }
        
        NSArray *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get company invitation details, response ...");
        for (NSDictionary *dic in data) {
            CompanyInvitation *theCompanyInvitation = [weakSelf createOrUpdateCompanyInvitationFromDictionary:dic];
            __weak __typeof(companyInvitation) weakCompanyInvitation = theCompanyInvitation;
            [_contactManager createRainbowContactAsynchronouslyWithRainbowID:dic[@"invitingAdminId"] withCompletionHandler:^(Contact *contact) {
                weakCompanyInvitation.peer = contact;
                [_contactManager updateRainbowContact:contact withCompanyInvitation:weakCompanyInvitation];
                NSLog(@"Adding contact information %@ in company invitation %@", contact, weakCompanyInvitation);
                [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitation object:weakCompanyInvitation];
            }];
            NSLog(@"Updating company invitation details %@", theCompanyInvitation);
        }
    }];
}

-(void) fetchCompanyDetails:(Company *) company {
    NSURL *baseUrl = [_apiUrlManagerService getURLForService:ApiServicesCompanies];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?format=full",baseUrl, company.companyId]];
    
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesCompanies];
    NSLog(@"Fetching company details %@", company);
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get company details : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get company details due to JSON parsing failure");
            return;
        }
        
        NSDictionary *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get company details, response ...");
        Company *theCompany = [self createOrUpdateCompanyFromDictionary:data];
        NSLog(@"Updating company details %@", theCompany);
    }];
}

-(void) populateLogoForCompany:(Company *) company {
    [self fetchAvatarDataForCompany:company];
}

-(void) populateBannerForCompany:(Company *) company {
    [self fetchBannerDataForCompany:company];
}

-(void) fetchAvatarDataForCompany:(Company *) company {
    if(!company.lastAvatarUpdateDate){
        NSLog(@"Company avatar last update date not found, the company have no avatar, don't download it");
        return;
    }
    
    NSString *update = [[NSDate jsonStringFromDate:company.lastAvatarUpdateDate] MD5];
    
    NSMutableString *urlParam = [NSMutableString stringWithFormat:@"%u",1024];
    if(update.length > 0)
        [urlParam appendFormat:@"&update=%@",update];
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesAvatar, company.companyId, urlParam];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesAvatar];
    
//    __weak __typeof__(company) weakCompany = company;
    NSLog(@"Fetching avatar for company %@", company);
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error){
            NSLog(@"Could not download avatar for company %@ error %@", company, error);
            return;
        }
        NSLog(@"Updating avatar for company %@", company);
        company.logo = receivedData;
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompany object:company];
    }];
}

-(void) fetchBannerDataForCompany:(Company *) company {
    if(!company.lastBannerUpdateDate){
        NSLog(@"Company banner last update date not found, the company have no banner, don't download it");
        return;
    }
    
    NSString *update = [[NSDate jsonStringFromDate:company.lastBannerUpdateDate] MD5];
    NSMutableString *urlParam = [NSMutableString stringWithFormat:@"%u",1024];
    if(update.length > 0)
        [urlParam appendFormat:@"&update=%@",update];
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesBanner, company.companyId, urlParam];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesBanner];
    
    __weak __typeof__(company) weakCompany = company;
    NSLog(@"Fetching banner for company %@", company);
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error){
            NSLog(@"Could not download avatar for company %@ error %@",company, error);
            return;
        }
        NSLog(@"Updating banner for company %@", weakCompany);
        weakCompany.banner = receivedData;
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompany object:weakCompany];
    }];
}

-(void) searchRainbowCompaniesWithPattern:(NSString *) pattern withCompletionBlock:(CompaniesServiceSearchCompanyCompletionHandler) completionBlock {
    
    NSURL* serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesCompanies];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesCompanies];
    NSString *trimmedPattern = [pattern stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?name=%@&format=medium",serviceUrl, [trimmedPattern urlEncode]]];
    
    NSLog(@"Searching for company on server with pattern %@", pattern);
    __weak __typeof__(self) weakSelf = self;
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSDictionary *returnedResponse = nil;
        NSMutableArray<Company*> *companies = [NSMutableArray array];
        if(!error) {
            returnedResponse = [receivedData objectFromJSONData];
            [returnedResponse[@"data"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
                Company *aCompany = [weakSelf createOrUpdateCompanyFromDictionary:obj];
                [companies addObject:aCompany];
            }];
            
            NSLog(@"Searching for company on server done");
        }
        if(completionBlock) {
            completionBlock(pattern, companies);
        }
    }];
}

-(void) requestJoinCompany:(Company *) company withCompletionHandler:(CompaniesServiceRequestJoinCompanyCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesJoinCompaniesRequest];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesJoinCompaniesRequest];
    NSDictionary *bodyContent = @{@"requestedCompanyId": company.companyId, @"lang": [[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode]};
    
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:YES] requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot join company %@ : %@ %@", company, error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if(completionHandler)
                completionHandler(company, nil, error);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot join company due to JSON parsing failure");
            if(completionHandler)
                completionHandler(company, nil, [NSError errorWithDomain:@"JSON" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Unable to parse server response"}]);
            return;
        }
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:jsonResponse[@"data"]];
        [dic setObject:@"sent" forKey:@"direction"];
        
        // Create a companyInvitation from server response
        CompanyInvitation *companyInvitation = [self createOrUpdateCompanyInvitationFromDictionary:dic];
        
        NSLog(@"Join request response %@ companyInvitation %@", dic, companyInvitation);
        if(completionHandler)
            completionHandler(company, companyInvitation, nil);
    }];
}

-(NSInteger) totalNbOfPendingCompanyInvitations {
    __block NSInteger total = 0;
    @synchronized (_companiesInvitations) {
        [_companiesInvitations enumerateObjectsUsingBlock:^(CompanyInvitation *companyInvitation, NSUInteger idx, BOOL *stop) {
            if(companyInvitation.direction == InvitationDirectionReceived && companyInvitation.status == InvitationStatusPending)
                total++;
        }];
    }
    
    return total;
}

#pragma mark - CRUD Company
-(Company *) createOrUpdateCompanyFromDictionary:(NSDictionary *) dictionary {
    NSString *companyID = [dictionary[@"id"] notNull];
    Company *theCompany = nil;
    @synchronized (_companies) {
        theCompany = [self getCompanyByID:companyID];
        if(!theCompany){
            theCompany = [Company new];
            [_companies addObject:theCompany];
        }
        
        theCompany.companyId = companyID;
        if([dictionary[@"country"] notNull])
            theCompany.country = [dictionary[@"country"] notNull];
        if([dictionary[@"name"] notNull])
            theCompany.name = [dictionary[@"name"] notNull];
        if([dictionary[@"adminEmail"] notNull])
            theCompany.adminEmail = [dictionary[@"adminEmail"] notNull];
        if([dictionary[@"supportEmail"] notNull])
            theCompany.supportEmail = [dictionary[@"supportEmail"] notNull];
        if([dictionary[@"description"] notNull])
            theCompany.companyDescription = [dictionary[@"description"] notNull];
        
        if(dictionary[@"lastAvatarUpdateDate"] && [dictionary[@"lastAvatarUpdateDate"] isKindOfClass:[NSString class]]) {
            theCompany.lastAvatarUpdateDate = [NSDate dateFromJSONString:dictionary[@"lastAvatarUpdateDate"]];
        }
        
        if(dictionary[@"lastBannerUpdateDate"] && [dictionary[@"lastBannerUpdateDate"] isKindOfClass:[NSString class]]) {
            theCompany.lastBannerUpdateDate = [NSDate dateFromJSONString:dictionary[@"lastBannerUpdateDate"]];
        }
        if([dictionary[@"size"] notNull])
            theCompany.companySize = [dictionary[@"size"] notNull];
        if(theCompany.companySize){
            theCompany.companySize = [theCompany.companySize stringByReplacingOccurrencesOfString:@" employees" withString:@""];
        }
        if([dictionary[@"slogan"] notNull])
            theCompany.slogan = [dictionary[@"slogan"] notNull];
        if([dictionary[@"website"] notNull])
            theCompany.websiteURL = [dictionary[@"website"] notNull];
        
        if([dictionary[@"avatarShape"] notNull]){
            NSString *avatarShape = [dictionary[@"avatarShape"] notNull];
            if([avatarShape isEqualToString:@"circle"]){
                theCompany.logoIsCircle = YES;
            }
        }
        
        if([dictionary[@"economicActivityClassification"] notNull]){
            theCompany.economicActivityClassification = [dictionary[@"economicActivityClassification"] notNull];
        }
        
        if([dictionary[@"companyContactId"] notNull]){
            theCompany.companyContact = [_contactManager createOrUpdateRainbowContactWithRainbowId:dictionary[@"companyContactId"]];
        }
    }
    return theCompany;
}

-(Company *) getCompanyByID:(NSString *) companyID {
    __block Company *foundCompany = nil;
    @synchronized (_companies) {
        [_companies enumerateObjectsUsingBlock:^(Company * company, NSUInteger idx, BOOL * stop) {
            if([company.companyId isEqualToString:companyID]){
                foundCompany = company;
                *stop = YES;
            }
        }];
    }
    
    return foundCompany;
}

-(void) acceptCompanyInvitation:(CompanyInvitation *) companyInvitation {
    if (companyInvitation.direction != InvitationDirectionReceived) {
        NSLog(@"We can accept only received invitations : %@", companyInvitation);
        return;
    }
    
    if (companyInvitation.status != InvitationStatusPending) {
        NSLog(@"We can accept only pending invitations : %@", companyInvitation);
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesJoinCompaniesInvitationAccept, companyInvitation.invitationID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesJoinCompaniesInvitationAccept];
    
    [_downloadManager postRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot accept company invitation in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot accept company invitation in REST due to JSON parsing failure");
            return;
        }
        
        NSDictionary *invitationDict = [jsonResponse objectForKey:@"data"];
        NSLog(@"Company Invitation %@ accepted in REST", invitationDict);
        
        // Update the invitation (at least the status will become accepted)
        CompanyInvitation *companyInvitation = [self createOrUpdateCompanyInvitationFromDictionary:invitationDict];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitation object:companyInvitation];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitationPendingNumber object:nil];
    }];
}

-(void) declineCompanyInvitation:(CompanyInvitation *) companyInvitation {
    if (companyInvitation.direction != InvitationDirectionReceived) {
        NSLog(@"We can decline only received invitations : %@", companyInvitation);
        return;
    }
    
    if (companyInvitation.status != InvitationStatusPending) {
        NSLog(@"We can decline only pending invitations : %@", companyInvitation);
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesJoinCompaniesInvitationDecline, companyInvitation.invitationID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesJoinCompaniesInvitationDecline];
    
    [_downloadManager postRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot decline company invitation in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot decline company invitation in REST due to JSON parsing failure");
            return;
        }
        
        NSDictionary *invitationDict = [jsonResponse objectForKey:@"data"];
        NSLog(@"Company Invitation %@ declined in REST", invitationDict);
        
        // Update the invitation (at least the status will become accepted)
        CompanyInvitation *companyInvitation = [self createOrUpdateCompanyInvitationFromDictionary:invitationDict];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitation object:companyInvitation];
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitationPendingNumber object:nil];
    }];
}

-(void) cancelRequestedJoinCompany:(Company *) company withCompletionHandler:(CompaniesServiceRequestJoinCompanyCompletionHandler) completionHandler {
    if(!company.alreadyRequestToJoin){
        if(completionHandler)
            completionHandler(company, nil, [NSError errorWithDomain:@"RequestCompanyInvitation" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Could not cancel request to a non requested company"}]);
        return;
    }
    
    CompanyInvitation *theCompanyInvitation = [self getPendingRequestCompanyInvitationForCompany:company];
    if(!theCompanyInvitation){
        if(completionHandler)
            completionHandler(company, nil, [NSError errorWithDomain:@"RequestCompanyInvitation" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Could not found company invitation matching given company"}]);
        return;
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/cancel", [_apiUrlManagerService getURLForService:ApiServicesJoinCompaniesRequest], theCompanyInvitation.invitationID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesJoinCompaniesInvitationDecline];
    
    [_downloadManager postRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot cancel company invitation in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if(completionHandler)
                completionHandler(company, nil, error);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot cancel company invitation in REST due to JSON parsing failure");
            if(completionHandler)
                completionHandler(company, nil, [NSError errorWithDomain:@"RequestCompanyInvitation" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Cannot cancel company invitation in REST due to JSON parsing failure"}]);
            return;
        }
        
        NSDictionary *invitationDict = [jsonResponse objectForKey:@"data"];
        NSLog(@"Company Invitation %@ canceled in REST", invitationDict);
        
        // Update the invitation (at least the status will become accepted)
        CompanyInvitation *companyInvitation = [self createOrUpdateCompanyInvitationFromDictionary:invitationDict];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitation object:companyInvitation];
        company.alreadyRequestToJoin = (theCompanyInvitation.direction == InvitationDirectionSent && theCompanyInvitation.status == InvitationStatusPending);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitationPendingNumber object:nil];
        
        if(completionHandler)
            completionHandler(company, companyInvitation, nil);
    }];
}

-(void) resendRequestJoinCompany:(Company *) company withCompletionHandler:(CompaniesServiceRequestJoinCompanyCompletionHandler) completionHandler {
    if(!company.alreadyRequestToJoin){
        if(completionHandler)
            completionHandler(company, nil, [NSError errorWithDomain:@"RequestCompanyInvitation" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Could not resend request to a non requested company"}]);
        return;
    }
    
    CompanyInvitation *theCompanyInvitation = [self getPendingRequestCompanyInvitationForCompany:company];
    if(!theCompanyInvitation){
        if(completionHandler)
            completionHandler(company, nil, [NSError errorWithDomain:@"RequestCompanyInvitation" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Could not found company invitation matching given company"}]);
        return;
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/re-send", [_apiUrlManagerService getURLForService:ApiServicesJoinCompaniesRequest], theCompanyInvitation.invitationID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesJoinCompaniesInvitationDecline];
    
    [_downloadManager postRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot resend company invitation in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if(completionHandler)
                completionHandler(company, nil, error);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot resend company invitation in REST due to JSON parsing failure");
            if(completionHandler)
                completionHandler(company, nil, [NSError errorWithDomain:@"RequestCompanyInvitation" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Cannot cancel company invitation in REST due to JSON parsing failure"}]);
            return;
        }
        
        NSDictionary *invitationDict = [jsonResponse objectForKey:@"data"];
        NSLog(@"Company Invitation %@ resent in REST", invitationDict);
        
        // Update the invitation (at least the status will become accepted)
        CompanyInvitation *companyInvitation = [self createOrUpdateCompanyInvitationFromDictionary:invitationDict];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitation object:companyInvitation];
        company.alreadyRequestToJoin = (theCompanyInvitation.direction == InvitationDirectionSent && theCompanyInvitation.status == InvitationStatusPending);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitationPendingNumber object:nil];
        
        if(completionHandler)
            completionHandler(company, companyInvitation, nil);
    }];
}

#pragma mark - CRUD CompanyInvitations
-(CompanyInvitation *) getCompanyInvitationWithID:(NSString *) companyInvitationID {
    __block CompanyInvitation *foundCompanyInvitation = nil;
    @synchronized (_companiesInvitations) {
        [_companiesInvitations enumerateObjectsUsingBlock:^(CompanyInvitation * companyInvitation, NSUInteger idx, BOOL * stop) {
            if([companyInvitation.invitationID isEqualToString:companyInvitationID]){
                foundCompanyInvitation = companyInvitation;
                *stop = YES;
            }
        }];
    }
    return foundCompanyInvitation;
}

-(CompanyInvitation *) getPendingRequestCompanyInvitationForCompany:(Company *) company {
    __block CompanyInvitation *foundCompanyInvitation = nil;
    @synchronized (_companiesInvitations) {
        [_companiesInvitations enumerateObjectsUsingBlock:^(CompanyInvitation * companyInvitation, NSUInteger idx, BOOL * stop) {
            if([companyInvitation.company isEqual:company] && companyInvitation.direction == InvitationDirectionSent && companyInvitation.status == InvitationStatusPending){
                foundCompanyInvitation = companyInvitation;
                *stop = YES;
            }
        }];
    }
    return foundCompanyInvitation;
}

-(CompanyInvitation *) createOrUpdateCompanyInvitationFromDictionary:(NSDictionary *) dictionary {
    NSString *companyInvitationID = [dictionary[@"id"] notNull];
    CompanyInvitation *theCompanyInvitation = nil;
    BOOL newCompanyInvitation = NO;
    @synchronized (_companiesInvitations) {
        theCompanyInvitation = [self getCompanyInvitationWithID:companyInvitationID];
        if(!theCompanyInvitation){
            theCompanyInvitation = [CompanyInvitation new];
            theCompanyInvitation.invitationID = dictionary[@"id"];
            [_companiesInvitations addObject:theCompanyInvitation];
            newCompanyInvitation = YES;
        }
        
        theCompanyInvitation.peer = [_contactManager getContactWithRainbowID:dictionary[@"requestingUserId"]];
        if([dictionary objectForKey:@"status"])
            theCompanyInvitation.status = [Invitation invitationStatusFromString:dictionary[@"status"]];
        
        // Replace the id by the companyID to create the company with the good id.
        NSMutableDictionary *dic2 = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        [dic2 removeObjectForKey:@"id"];
        
        if([dictionary objectForKey:@"companyId"])
            [dic2 setObject:[dictionary objectForKey:@"companyId"] forKey:@"id"];
        else if ([dictionary objectForKey:@"requestedCompanyId"])
            [dic2 setObject:[dictionary objectForKey:@"requestedCompanyId"] forKey:@"id"];
        theCompanyInvitation.company = [self createOrUpdateCompanyFromDictionary:dic2];
        
        id lastNotificationDate = dictionary[@"lastNotificationDate"];
        if ([lastNotificationDate isKindOfClass:[NSDate class]]) {
            theCompanyInvitation.lastNotificationDate = (NSDate *)lastNotificationDate;
        } else if ([lastNotificationDate isKindOfClass:[NSString class]]) {
            theCompanyInvitation.lastNotificationDate = [NSDate dateFromJSONString:(NSString *) lastNotificationDate];
        }
        // when we get the company invitation details there is no direction :/
        if([dictionary objectForKey:@"direction"]){
            theCompanyInvitation.direction = [Invitation invitationDirectionFromString:dictionary[@"direction"]];
            theCompanyInvitation.company.alreadyRequestToJoin = (theCompanyInvitation.direction == InvitationDirectionSent &&
                                                                 theCompanyInvitation.status == InvitationStatusPending);
        }
        
        [_companyiesInvitationsCache setObjectAsync:[theCompanyInvitation dictionaryRepresentation] forKey:theCompanyInvitation.invitationID completion:nil];
    }
    if(newCompanyInvitation)
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidAddCompanyInvitation object:theCompanyInvitation];
    else
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitation object:theCompanyInvitation];
    
    if(theCompanyInvitation.direction == InvitationDirectionReceived && theCompanyInvitation.status == InvitationStatusPending)
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitationPendingNumber object:nil];
    
    return theCompanyInvitation;
}

-(void) createOrUpdateCompanyInvitationWithId:(NSString *) companyInvitationId direction:(NSString *) direction status:(NSString *) status {
    CompanyInvitation *theCompanyInvitation = nil;
    BOOL newCompanyInvitation = NO;
    @synchronized (_companiesInvitations) {
        theCompanyInvitation = [self getCompanyInvitationWithID:companyInvitationId];
        
        if(!theCompanyInvitation){
            theCompanyInvitation = [CompanyInvitation new];
            theCompanyInvitation.invitationID = companyInvitationId;
            [_companiesInvitations addObject:theCompanyInvitation];
            newCompanyInvitation = YES;
        }

        theCompanyInvitation.status = [Invitation invitationStatusFromString:status];
        theCompanyInvitation.direction = [Invitation invitationDirectionFromString:direction];
    }
    
    if(newCompanyInvitation){
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidAddCompanyInvitation object:theCompanyInvitation];
        if(theCompanyInvitation.status != InvitationStatusCanceled)
            [self fetchCompanyInvitationDetailsForCompanyInvitation:theCompanyInvitation];
    } else
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitation object:theCompanyInvitation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitationPendingNumber object:nil];
}

-(void) updateCompanyInvitationJoinWithId:(NSString *) companyInvitationId direction:(NSString *) direction status:(NSString *) status {
    CompanyInvitation *theCompanyInvitation = nil;
    @synchronized (_companiesInvitations) {
        theCompanyInvitation = [self getCompanyInvitationWithID:companyInvitationId];
        
        if(!theCompanyInvitation){
            NSLog(@"Company Invitation Join Update : there is no pending invitation for company with id=%@", companyInvitationId);
            return;
        }
        
        theCompanyInvitation.status = [Invitation invitationStatusFromString:status];
        theCompanyInvitation.direction = [Invitation invitationDirectionFromString:direction];
        theCompanyInvitation.company.alreadyRequestToJoin = (theCompanyInvitation.direction == InvitationDirectionSent && theCompanyInvitation.status == InvitationStatusPending);
        
        if (theCompanyInvitation.status == InvitationStatusAccepted) {
            _myUser.isInDefaultCompany = NO;
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidUpdateCompanyInvitation object:theCompanyInvitation];
}

-(void) removeCompanyInvitationWithID:(NSString *) companyInvitationId {
    CompanyInvitation *theCompanyInvitation = nil;
    @synchronized (_companiesInvitations) {
        theCompanyInvitation = [self getCompanyInvitationWithID:companyInvitationId];
        
        if(!theCompanyInvitation){
            NSLog(@"Company Invitation Join Update : there is no pending invitation for company with id=%@", companyInvitationId);
            return;
        }
        theCompanyInvitation.status = [Invitation invitationStatusFromString:@"deleted"];
        theCompanyInvitation.company.alreadyRequestToJoin = NO;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCompaniesServiceDidRemoveCompanyInvitation object:theCompanyInvitation];
        
        [_companiesInvitations removeObject:theCompanyInvitation];
    }
}

@end
