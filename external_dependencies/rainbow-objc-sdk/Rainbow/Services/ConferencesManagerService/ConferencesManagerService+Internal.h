/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ApiUrlManagerService.h"
#import "XMPPService.h"

typedef void (^ConferenceManagerFetchConfEndpointDetailsCompletionHandler) (NSError *error);

@interface ConferencesManagerService ()
-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService roomsService:(RoomsService *) roomsService xmppService:(XMPPService *) xmppService myUser:(MyUser *)myUser rtcService:(RTCService *) rtcService;

-(Conference *) createOrUpdateConferenceFromJson:(NSDictionary *) jsonDictionary;
-(Conference *) getConferenceByRainbowID:(NSString *) conferenceRainbowID;
-(void) fetchConfEndpointDetails:(ConfEndpoint *) confEndpoint completionBlock:(ConferenceManagerFetchConfEndpointDetailsCompletionHandler) completionHandler;

/**
 *  Create a WebRTC conference room for the given room with a given conference endpoint
 *  @param  room                the room in which we want to create a conference
 *  @param  confEndpoint        the conference endpoint to use
 *  @param  completionHandler   use in return of the create action
 */
-(void) createConferenceForRoom:(Room *) room withConfEndpoint:(ConfEndpoint *) confEndpoint completionHandler:(ConferenceManagerCreateConferenceCompletionHandler) completionHandler;

@end

