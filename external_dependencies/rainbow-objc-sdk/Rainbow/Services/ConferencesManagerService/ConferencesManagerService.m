
/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConferencesManagerService.h"
#import "ConferencesManagerService+Internal.h"
#import "ConfEndpoint+Internal.h"
#import "ConferenceParticipant.h"
#import "Conference+Internal.h"
#import "NSDictionary+JSONString.h"
#import "NSData+JSON.h"
#import "XMPPMessage+RainbowCustomMessageTypes.h"
#import "Peer+Internal.h"
#import "NSObject+NotNull.h"
#import "NSDate+JSONString.h"
#import "NSDictionary+ChangedKeys.h"
#import "NSDate+Utilities.h"
#import "MyUser+Internal.h"
#import "PhoneNumberInternal.h"
#import "LoginManager+Internal.h"
#import "ConferencePublisher+Internal.h"
#import "RTCService+Internal.h"
#import "Room+Internal.h"

NSString *const kConferencesManagerDidAddConference = @"didAddConference";
NSString *const kConferencesManagerDidUpdateConference = @"didUpdateConference";
NSString *const kConferencesManagerDidRemoveConference = @"didRemoveConference";

/* key for did update conference notification */
NSString *const kConferenceKey = @"conference";
NSString *const kConferenceChangedAttributesKey = @"conferenceChangedAttributes";

NSString *const ConferenceManagerErrorDomainAttach = @"attachError";
NSString *const ConferenceManagerErrorDomainStart = @"startError";
NSString *const ConferenceManagerErrorDomainJoin = @"joinError";
NSString *const ConferenceManagerErrorDomainSnapshot = @"snapshotError";

typedef void (^ConferenceManagerFetchEndpointsCompletionHandler) (NSArray<ConfEndpoint *> *confEndpoints, NSError *error);
typedef void (^ConferenceManagerChangeConfereneStateCompletionHandler) (NSError *error);

@interface ConferencesManagerService () <XMPPServiceConferenceDelegate>

@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property (nonatomic, strong) ContactsManagerService *contactsManagerService;
@property (nonatomic, strong) RoomsService *roomsService;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) MyUser *myUser;
@property (nonatomic, strong) RTCService *rtcService;
@property (nonatomic, strong) NSObject *conferencesMutex;
@property (nonatomic, strong) NSMutableArray<ConfEndpoint *> * conferenceEndpoints;
@property (nonatomic, strong) NSMutableArray<Conference *> *conferences;

@end

@implementation ConferencesManagerService

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService roomsService:(RoomsService *) roomsService xmppService:(XMPPService *) xmppService myUser:(MyUser *)myUser rtcService:(RTCService *) rtcService {
    self = [super init];
    if(self){
        _downloadManager = downloadManager;
        _contactsManagerService = contactsManagerService;
        _roomsService = roomsService;
        _xmppService = xmppService;
        _xmppService.conferenceDelegate = self;
        _apiUrlManagerService = apiUrlManagerService;
        _myUser = myUser;
        _rtcService = rtcService;
    
        
        _conferencesMutex = [NSObject new];
        
        _conferenceEndpoints = [NSMutableArray new];
        _conferences = [NSMutableArray new];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];

    _xmppService.conferenceDelegate = nil;

    @synchronized (_conferencesMutex) {
        [_conferences removeAllObjects];
        _conferences = nil;
        [_conferenceEndpoints removeAllObjects];
        _conferenceEndpoints = nil;
    }
    _conferencesMutex = nil;
    _downloadManager = nil;
    _contactsManagerService = nil;
    _roomsService = nil;
    _xmppService = nil;
    _apiUrlManagerService = nil;
    _myUser = nil;
}

-(void) didLogin:(NSNotification *) notification {
    @synchronized (_conferencesMutex) {
        [self fetchConfEndpointsWithUserId:_myUser.contact.rainbowID completionBlock:^(NSArray<ConfEndpoint *> *confEndpoints, NSError *error) {
            if(!error){
                @synchronized (_conferencesMutex) {
                    // endPoints used in conference could already have been added by RoomsServices in loadRoomsFromCacheSynchronously
                    // add only the new endPoints
                    for(ConfEndpoint *newEndPoint in confEndpoints){
                        BOOL found = NO;
                        for(ConfEndpoint *oldEndPoint in _conferenceEndpoints){
                            if([newEndPoint.confEndpointId isEqualToString:oldEndPoint.confEndpointId]){
                                found = YES;
                                break;
                            }
                        }
                        if(!found){
                            [_conferenceEndpoints addObject:newEndPoint];
                        }
                    }
                }
                // In case we arrived there after a reconnect we may have conferences
                for(Conference *conference in [self.conferences copy]){
                    if(conference.confId){
                        [self fetchConferenceSnapshot:conference completionBlock:^(NSError *error) {
                        }];
                    } else {
                        [(NSMutableArray<Conference *> *)self.conferences removeObject: conference];
                    }
                }
            }
        }];
    }
}

-(void) didReconnect:(NSNotification *) notification {
    [self didLogin:notification];
}

-(void) didLogout:(NSNotification *) notification {
    @synchronized (_conferencesMutex) {
        [_conferenceEndpoints removeAllObjects];
    }
}

-(BOOL) hasJoinedConference {
    for(Conference *conference in self.conferences){
        if([self hasContact:_myUser.contact joinedConference: conference]){
            return YES;
        }
    }
    return NO;
}

-(void) fetchConfEndpointsWithUserId:(NSString *)userId completionBlock: (ConferenceManagerFetchEndpointsCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConfProvisioningConferences, [NSString stringWithFormat:@"?userId=%@&format=medium", userId]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConfProvisioningConferences];
    NSLog(@"[ConferencesManager] Get conference endpoint list");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *urlResponse) {
        if (error) {
            NSLog(@"[ConferencesManager] Get conference endpoints request returned an error: %@ receivedData %@", error, [receivedData objectFromJSONData]);
            completionHandler(nil, error);
            return;
        }
        NSDictionary *response = [receivedData objectFromJSONData];
        if(!response) {
            NSString *jsonData = [[NSString alloc] initWithBytes:[receivedData bytes] length:[receivedData length] encoding:NSUTF8StringEncoding];
            NSLog(@"[ConferencesManager] Get conference endpoints JSON parsing failed, received data was :\n%@", jsonData);
            return;
        }
        
        NSDictionary *data = [response objectForKey:@"data"];
        NSLog(@"[ConferencesManager] Get conference endpoints return data ...");
        NSMutableArray<ConfEndpoint *> *confEndpoints = [NSMutableArray new];
        for (NSDictionary *serverConfEndpoint in data) {
            ConfEndpoint *confEndpoint = [self createOrUpdateConfEndpointFromJson:serverConfEndpoint];
            if([confEndpoint.userId isEqualToString:_myUser.contact.rainbowID]){
                [confEndpoints addObject:confEndpoint];
            }
        }
        if(completionHandler)
            completionHandler(confEndpoints, nil);
    }];
}

#pragma mark - create conference
-(void) createConferenceForRoom:(Room *) room startDate:(NSDate *) startDate endDate:(NSDate*) endDate completionHandler:(ConferenceManagerCreateConferenceCompletionHandler) completionHandler {
    if(!room){
        NSLog(@"[ConferencesManager] Room is required to create a conference");
        NSAssert(room, @"room is required to create a conference");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Room is required to create a conference"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    BOOL isInstant = NO;
    if(!endDate && !startDate){
        isInstant = YES;
    }
    
    if(!isInstant && [endDate isEarlierThanDate:startDate]){
        NSLog(@"[ConferencesManager] End date %@ must be greater than start date %@", endDate, startDate);
        NSAssert([endDate isEarlierThanDate:startDate], @"End date %@ must be greater than start date %@", endDate, startDate);
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"End date %@ must be greater than start date %@", endDate, startDate]}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    if(!isInstant && !startDate){
        NSLog(@"[ConferencesManager] No start date");
        NSAssert(startDate, @"No start date");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"No start date"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    if(!isInstant && !endDate){
        NSLog(@"[ConferencesManager] No end date");
        NSAssert(endDate, @"No end date");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"No end date"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    if(!_myUser.pgiConfUserID){
        NSLog(@"[ConferencesManager] No PGI Conf user id");
        NSAssert(_myUser.pgiConfUserID, @"No pgi conf user id");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"No PGI confuserId"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    if(!isInstant){
        NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConfProvisioningConferences,@""];
        NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConfProvisioningConferences];
        NSDictionary *body = @{@"confUserId": _myUser.pgiConfUserID, @"name":room.displayName, @"startDate": [NSDate jsonStringFromDate:startDate], @"endDate":[NSDate jsonStringFromDate:endDate], @"timeZone":[[NSCalendar currentCalendar] timeZone].name};
        
        NSLog(@"[ConferencesManager] Create a conference for room %@", room);
        [_downloadManager postRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
            if(error){
                NSLog(@"[ConferencesManager] Create a conference for room %@ failed %@ receivedData %@", room, error, [receivedData objectFromJSONData]);
                if(completionHandler)
                    completionHandler(error);
                return;
            }
            
            NSMutableDictionary *json = [NSMutableDictionary dictionaryWithDictionary:[receivedData objectFromJSONData]];
            if(json){
                NSLog(@"[ConferencesManager] Create a conference %@", json);
                Conference *aConference = [self createOrUpdateConferenceFromJson:json[@"data"]];
                ConfEndpoint *aConfEndpoint = aConference.endpoint;
                NSLog(@"[ConferencesManager] Created conference endpoint %@", aConfEndpoint);
                [_roomsService attachConferenceEndpoint:aConfEndpoint inRoom:room completionBlock:^(NSError *error, Conference *conference) {
                    if(completionHandler){
                        completionHandler(error);
                    }
                }];
            }
        }];
    } else {
        // we are creating a instant meeting
        ConfEndpoint *pstnInstantConfEndpoint = [self pstnInstantConferenceEndPoint];
        if(pstnInstantConfEndpoint){
            [_roomsService attachConferenceEndpoint:pstnInstantConfEndpoint inRoom:room completionBlock:^(NSError *error, Conference *conference) {
                if(completionHandler){
                    completionHandler(error);
                }
            }];
        } else {
            NSLog(@"[ConferencesManager] No ConfEndPoint initialised for instant conference creation");
            NSAssert(pstnInstantConfEndpoint, @"No pgi conf user id");
            NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey: @"Could not create conference"}];
            if(completionHandler){
                completionHandler(error);
            }
        }
    }
}

-(void) createConferenceForRoom:(Room *) room withConfEndpoint:(ConfEndpoint *) confEndpoint completionHandler:(ConferenceManagerCreateConferenceCompletionHandler) completionHandler {
    if(!room){
        NSLog(@"[ConferencesManager] Room is required to create a conference");
        NSAssert(room, @"room is required to create a conference");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Room is required to create a conference"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    if(!confEndpoint){
        NSLog(@"[ConferencesManager] ConfEndpoint is required to create a conference");
        NSAssert(confEndpoint, @"ConfEndpoint is required to create a conference");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"ConfEndpoint is required to create a conference"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    if(confEndpoint.mediaType != ConferenceEndPointMediaTypeWebRTC){
        NSLog(@"[ConferencesManager] Only WebRTC conferences could be created this way");
        NSAssert(confEndpoint.mediaType == ConferenceEndPointMediaTypeWebRTC, @"Only WebRTC conferences could be created this way");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Only WebRTC conferences could be created this way"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    confEndpoint.confEndpointId, @"id",
                                    [ConfEndpoint stringFromConferenceEndPointMediaType: confEndpoint.mediaType], @"mediaType",
                                    nil];
    Conference *conference = [self createOrUpdateConferenceFromJson:jsonDictionary];
    conference.endpoint = confEndpoint;
    conference.status = ConferenceStatusAttached;
    if(completionHandler)
        completionHandler(nil);
}

-(void) updateConferenceForRoom:(Room *) room startDate:(NSDate *) startDate endDate:(NSDate *) endDate completionHandler:(ConferenceManagerUpdateConferenceCompletionHandler) completionHandler {
    if(!room){
        NSLog(@"[ConferencesManager] Room is required to update a conference");
        NSAssert(room, @"Room is required to update a conference");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Room is required to update a conference"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    if(!room.conference){
        NSLog(@"[ConferencesManager] Conference is required to update a conference");
        NSAssert(room.conference, @"Conference is required to update a conference");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Conference is required to update a conference"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    if(!endDate && !startDate){
        NSLog(@"[ConferencesManager] startDate and endDate are required to update a conference");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"startDate and endDate are required to update a conference"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    if([endDate isEarlierThanDate:startDate]){
        NSLog(@"[ConferencesManager] End date %@ must be greater than start date %@", endDate, startDate);
        NSAssert([endDate isEarlierThanDate:startDate], @"End date %@ must be greater than start date %@", endDate, startDate);
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"End date %@ must be greater than start date %@", endDate, startDate]}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    if(!startDate){
        NSLog(@"[ConferencesManager] No start date");
        NSAssert(startDate, @"No start date");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"No start date"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    if(!endDate){
        NSLog(@"[ConferencesManager] No end date");
        NSAssert(endDate, @"No end date");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"No end date"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConfProvisioningConferences,[NSString stringWithFormat: @"/%@",room.conference.confId]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConfProvisioningConferences];
    NSDictionary *body = @{@"name":room.displayName, @"startDate": [NSDate jsonStringFromDate:startDate], @"endDate":[NSDate jsonStringFromDate:endDate], @"timeZone":[[NSCalendar currentCalendar] timeZone].name};
    
    NSLog(@"[ConferencesManager] Update a conference for room %@", room);
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error){
            NSLog(@"[ConferencesManager] Update a conference for room %@ failed %@ receivedData %@", room, error, [receivedData objectFromJSONData]);
            if(completionHandler)
                completionHandler(error);
            return;
        }
        
        NSDictionary *json = [receivedData objectFromJSONData];
        if(json){
            NSLog(@"[ConferencesManager] Update a conference %@", json);
            ConfEndpoint *aConfEndpoint = [self createOrUpdateConfEndpointFromJson:json[@"data"]];
            // Also create a conference
            Conference *aConference = [self createOrUpdateConferenceFromJson:json[@"data"]];
            aConference.endpoint = aConfEndpoint;
            NSLog(@"[ConferencesManager] Updated confEndPoint %@", aConfEndpoint);
            if(completionHandler){
                completionHandler(error);
            }
        }
    }];
}

-(void) deleteConference:(Conference *) conference completionBlock:(ConferenceManagerDeleteConferenceCompletionHandler) completionHandler {
    if(!conference.confId){
        NSLog(@"[ConferencesManager] No conference id found %@", conference);
        NSAssert(conference.confId, @"No conference id found");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"No conference id found"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    if(conference.type == ConferenceTypeInstant){
        NSLog(@"[ConferencesManager] Could not delete an instant conference %@", conference);
        NSAssert(conference.type == ConferenceTypeInstant, @"Could not delete an instant conference");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Could not delete an instant conference"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConfProvisioningConferencesDelete, conference.confId];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConfProvisioningConferencesDelete];
    NSLog(@"[ConferencesManager] Delete a conference");
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"[ConferencesManager] Delete a conference returned an error: %@ receivedData %@", error, [receivedData objectFromJSONData]);
        } else {
            NSLog(@"[ConferencesManager] Delete a conference succeeded");
            BOOL removed = NO;
            @synchronized (_conferencesMutex) {
                if([_conferences containsObject:conference]){
                    [_conferences removeObject:conference];
                    removed = YES;
                }
            }
            if(removed){
                [[NSNotificationCenter defaultCenter] postNotificationName:kConferencesManagerDidRemoveConference object:conference];
            }
        }
        if(completionHandler)
            completionHandler(error);
    }];
}

-(void) joinConference:(Conference *)conference phoneNumber:(NSString *) phoneNumber role:(ParticipantRole)role completionBlock:(ConferenceManagerJoinConferenceCompletionHandler)completionHandler {
    if(!conference.confId){
        NSLog(@"[ConferencesManager] No conference id found %@", conference);
        NSAssert(conference.confId, @"No conference id found");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"No conference id found"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    if(conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio && !phoneNumber){
        NSLog(@"[ConferencesManager] We are trying to join a PGI conference without phone number, that mean the user will do a dial in, so we don't have to join it.");
        if(completionHandler)
            completionHandler(nil);
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConferencesJoin, conference.confId];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConferencesJoin];
    NSString *roleStr = [ConferenceParticipant stringFromParticipantRole:role];
    NSMutableDictionary *bodyContent = [NSMutableDictionary dictionaryWithDictionary:@{@"participant":@{@"role":roleStr , @"type":@"unmuted"}, @"mediaType":[ConfEndpoint stringFromConferenceEndPointMediaType:conference.endpoint.mediaType]}];
    if(phoneNumber)
        [bodyContent setObject:phoneNumber forKey:@"participantPhoneNumber"];
    
    NSLog(@"[ConferencesManager] Join a audio conference");
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers shouldSaveRequest:NO completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"[ConferencesManager] Join a audio conference returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
            if(completionHandler)
                completionHandler(error);
        } else {
            NSDictionary *data = [receivedData objectFromJSONData];
            NSLog(@"[ConferencesManager] Join a audio conference succeeded, %@", data);
            NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
            // PGI conf return only a status
            if(data[@"status"]) {
                conference.isActive = YES;
            }
            // webrtc conf return data
            if(data[@"data"]){
                NSString *jingleJid = data[@"data"][@"jingleJid"];
                conference.jingleJid = jingleJid;
                conference.isActive = YES;
            }
            [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
            if(completionHandler)
                completionHandler(error);
        }
    }];
}

-(void) startAndJoinConference:(Conference *)aConference inRoom:(Room *) room phoneNumber:(NSString *) phoneNumber role:(ParticipantRole)role completionBlock:(ConferenceManagerStartAndJoinConferenceCompletionHandler)completionHandler {
    if(self.hasJoinedConference){
        NSLog(@"[ConferencesManager] A conference is already active, can't start another one");
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"A conference is already active, can't start another one"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    ConfEndpoint *confEndpointToAttach = aConference.endpoint;
    Conference *conference = aConference;
    // Attach the webrtc bridge if needed
    if(room.isMyRoom){
        if(!confEndpointToAttach.confEndpointId){
            if (confEndpointToAttach.mediaType == ConferenceEndPointMediaTypePSTNAudio) {
                confEndpointToAttach = [self pstnInstantConferenceEndPoint];
            } else {
                // This room have no conference we must attach check which bridge we want to attach
                if(_myUser.isAllowedToUseWebRTCTelephonyConference && room.participants.count <= _myUser.maxNumberOfWebRTCParticipantsPerRoom){
                    confEndpointToAttach = [self webRTCConferenceEndPoint];
                }
            }
        }
        
        if(!conference){
            conference = [self createOrUpdateConferenceFromJson:[confEndpointToAttach dictionaryRepresentation]];
            conference.endpoint = confEndpointToAttach;
            conference.owner = room.creator;
            room.conference = conference;
        } else {
            if(!conference.endpoint.confEndpointId)
                conference.endpoint = confEndpointToAttach;
        }
        
        if(!confEndpointToAttach.attachedRoomID || ![confEndpointToAttach.attachedRoomID isEqualToString:room.rainbowID]){
            // Go to the good state
            if(conference.status == ConferenceStatusAttached){
                if(conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                    // The conference is already attached, that could be done from another device or because of a wrong end of the conference
                    // do a snapshot to determine the real state
                    [self fetchConferenceSnapshot:conference completionBlock:^(NSError *error) {
                        if(error){
                            if(error.code == 409000){
                                if(completionHandler)
                                    completionHandler(error);
                                return;
                            } else {
                                // not a specific error so we can start ??
                                [self changeConferenceState:ConferenceStatusAttached conference:conference room:room phoneNumber:phoneNumber role:role completionHandler:completionHandler];
                            }
                        } else {
                            [self changeConferenceState:ConferenceStatusStarted conference:conference room:room phoneNumber:phoneNumber role:role completionHandler:completionHandler];
                        }
                    }];
                } else {
                    conference.endpoint.attachedRoomID = room.rainbowID;
                    [self changeConferenceState:ConferenceStatusAttached conference:conference room:room  phoneNumber:phoneNumber role:role completionHandler:completionHandler];
                }
            } else if(conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC){
                [self changeConferenceState:ConferenceStatusAttached conference:conference room:room phoneNumber:phoneNumber role:role completionHandler:completionHandler];
            }
        } else {
            NSLog(@"[ConferencesManager] conference already attached");
            if(conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                [self fetchConferenceSnapshot:conference completionBlock:^(NSError *error) {
                    if(error){
                        if(error.code == 409000){
                            if(completionHandler)
                                completionHandler(error);
                            return;
                        } else {
                            // not a specific error so we can start ??
                            [self changeConferenceState:ConferenceStatusAttached conference:conference room:room phoneNumber:phoneNumber role:role completionHandler:completionHandler];
                        }
                    } else {
                        [self changeConferenceState:ConferenceStatusStarted conference:conference room:room phoneNumber:phoneNumber role:role completionHandler:completionHandler];
                    }
                }];
            } else if(conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC){
                [self fetchConferenceSnapshot:conference completionBlock:^(NSError *error) {
                    if(error){
                        conference.isActive = NO;
                        [self changeConferenceState:ConferenceStatusUnknown conference:conference room:room phoneNumber:phoneNumber role:role completionHandler:completionHandler];
                        if(completionHandler)
                            completionHandler(error);
                        return;
                    } else {
                        if(conference.isActive){
                            [self joinConference:conference phoneNumber:phoneNumber role:role completionBlock:completionHandler];
                        } else {
                            [_roomsService detachConference:conference fromRoom:room completionBlock:nil];
                        }
                        if(completionHandler)
                            completionHandler(nil);
                    }
                }];
            }
        }
    } else {
        NSLog(@"[ConferencesManager] not our conference so we can only join it");
        if(conference.endpoint.confEndpointId && _myUser.isAllowedToParticipateInWebRTCMobile){
            conference.status = ConferenceStatusStarted;
            [self changeConferenceState:ConferenceStatusJoined conference:conference room:room  phoneNumber:phoneNumber role:role completionHandler:completionHandler];
        }
        else {
            NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Error while trying to join the conference"}];
            if(completionHandler)
                completionHandler(error);
        }
    }
}

-(void) changeConferenceState:(ConferenceStatus) newStatus conference:(Conference *) conference room:(Room *) room phoneNumber:(NSString *) phoneNumber role:(ParticipantRole)role completionHandler:(ConferenceManagerChangeConfereneStateCompletionHandler) completionHandler {
    switch (conference.status) {
        case ConferenceStatusUnknown:{
            // We don't known our start so we suppose that we can attach the room
            if(newStatus == ConferenceStatusAttached){
                [_roomsService attachConferenceEndpoint:conference.endpoint inRoom:room completionBlock:^(NSError *error, Conference *conference) {
                    // TODO: treat return
                    if(error){
                        NSLog(@"[ConferencesManager] Attaching conference Failed : %@", error);
                        NSError *theError = [NSError errorWithDomain:ConferenceManagerErrorDomainAttach code:500 userInfo:@{NSLocalizedDescriptionKey:@"Could not attach the conference"}];
                        if(completionHandler)
                            completionHandler(theError);
                    } else {
                        NSLog(@"[ConferencesManager] Attached conference done");
                        conference.status = ConferenceStatusAttached;
                        // Ok continue to the next action
                        [self changeConferenceState:ConferenceStatusStarted conference:conference room:room phoneNumber:phoneNumber role:role completionHandler:completionHandler];
                    }
                }];
            }
            if(newStatus == ConferenceStatusStarted){
                conference.status = ConferenceStatusAttached;
                [self changeConferenceState:newStatus conference:conference room:room phoneNumber:phoneNumber role:role completionHandler:completionHandler];
            }
            break;
        }
        case ConferenceStatusAttached:{
            // The conference is attach so we can start it if it's our conference or join it if we are participant
            // if the conference is already attached (wrong stop of the application) we must start it to so we add an exception
            if(newStatus == ConferenceStatusAttached || newStatus == ConferenceStatusStarted || newStatus == ConferenceStatusJoined){
                if(conference.isMyConference){
                    [self startConference:conference completionHandler:^(NSError *error) {
                        if(error){
                            NSLog(@"[ConferencesManager] Start conference Failed : %@", error);
                            NSError *theError = [NSError errorWithDomain:ConferenceManagerErrorDomainStart code:500 userInfo:@{NSLocalizedDescriptionKey:@"Could not start the conference"}];
                            if(completionHandler)
                                completionHandler(theError);
                        } else {
                            NSLog(@"[ConferencesManager] Start conference done");
                            conference.status = ConferenceStatusStarted;
                            // Ok continue to the next action
                            [self changeConferenceState:ConferenceStatusJoined conference:conference room:room phoneNumber:phoneNumber role:role completionHandler:completionHandler];
                        }
                    }];
                } else {
                    NSLog(@"[ConferencesManager] trying a conference that is not our, don't do it, join it instead if it's possible");
                    // This is not our conference so check if we can join it
                    if(conference.canJoin){
                        conference.status = ConferenceStatusStarted;
                        [self changeConferenceState:ConferenceStatusJoined conference:conference room:room phoneNumber:phoneNumber role:role completionHandler:completionHandler];
                    } else {
                        NSError *theError = [NSError errorWithDomain:ConferenceManagerErrorDomainJoin code:500 userInfo:@{NSLocalizedDescriptionKey:@"Could not join the conference"}];
                        if(completionHandler)
                            completionHandler(theError);
                    }
                }
            }
            break;
        }
        case ConferenceStatusStarted:{
            // The conference is already started we can join it
            [self joinConference:conference phoneNumber:phoneNumber role:role completionBlock:^(NSError *error) {
                if(error){
                    NSLog(@"[ConferencesManager] Join conference Failed : %@", error);
                    NSError *theError = [NSError errorWithDomain:ConferenceManagerErrorDomainJoin code:500 userInfo:@{NSLocalizedDescriptionKey:@"Could not join the conference"}];
                    if(completionHandler)
                        completionHandler(theError);
                } else {
                    NSLog(@"[ConferencesManager] Join conference done");
                    conference.status = ConferenceStatusJoined;
                    // Ok continue to the next action
                    [self changeConferenceState:conference.status conference:conference room:room phoneNumber:phoneNumber role:role completionHandler:completionHandler];
                }
            }];
            break;
        }
        case ConferenceStatusJoined:{
            if(conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC)
                [_rtcService beginNewOutgoingCallWithPeer:room withFeatures:RTCCallFeatureAudio];
            if(completionHandler)
                completionHandler(nil);
            break;
        }
        default:{
            // Nothing else to do
        }
            break;
    }
}

-(void) fetchConferenceSnapshot:(Conference *) conference completionBlock:(ConferenceManagerFetchConferenceSnapshotCompletionHandler) completionHandler {
    if(!conference.confId){
        NSLog(@"[ConferencesManager] No conference id found %@", conference);
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"No conference id found"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesConferencesSnapshot, conference.confId];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?mediaType=%@",serviceUrl, [ConfEndpoint stringFromConferenceEndPointMediaType:conference.endpoint.mediaType]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConferencesSnapshot];
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *urlResponse) {
        NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
        // WebRTC conferences always return 200 OK, only PGI could return a error
        if (error) {
            NSLog(@"[ConferencesManager] fetchConferenceDetails returned an error: %@, received data %@", error, [receivedData objectFromJSONData]);
            if(conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                NSDictionary *response = [receivedData objectFromJSONData];
                if(response){
                    NSInteger errorDetailsCode = [response[@"errorDetailsCode"] integerValue];
                    if(errorDetailsCode == 403300){
                        conference.canJoin = YES;
                    } else {
                        conference.canJoin = NO;
                    }
                    if(errorDetailsCode == 409000){
                        NSError *theError = [NSError errorWithDomain:ConferenceManagerErrorDomainSnapshot code:errorDetailsCode userInfo:@{NSLocalizedDescriptionKey:@"You are already connected to this conference"}];
                        if(completionHandler)
                            completionHandler(theError);
                        return;
                    }
                }
                [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
            }
            if(completionHandler)
                completionHandler(error);
            
            return;
        }
        NSDictionary *response = [receivedData objectFromJSONData];
        if(!response) {
            NSLog(@"[ConferencesManager] fetchConferenceDetails JSON parsing failed");
            NSError *parsingError = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorDownloadDecodingFailedToComplete userInfo:nil];
            if(completionHandler)
                completionHandler(parsingError);
            return;
        }
        
        NSDictionary *data = [response objectForKey:@"data"];
        if(data){
            conference.isActive = [[[data objectForKey:@"active"] notNull] boolValue];
            conference.canJoin = conference.isActive;
        }
        [self updateConference:conference withSnapshotJson:response];
        
        NSLog(@"[ConferencesManager] %@", [conference description]);
        [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
        if(completionHandler)
            completionHandler(error);
    }];
}

-(void) fetchConfEndpointDetails:(ConfEndpoint *) confEndpoint completionBlock:(ConferenceManagerFetchConfEndpointDetailsCompletionHandler) completionHandler {
    if(!confEndpoint.confEndpointId){
        NSLog(@"[ConferencesManager] No confEndpoint id found %@", confEndpoint);
        NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"No confEndpoint id found"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesConfProvisioningConferences, confEndpoint.confEndpointId];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?mediaType=%@",serviceUrl, [ConfEndpoint stringFromConferenceEndPointMediaType:confEndpoint.mediaType]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConferencesSnapshot];
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *urlResponse) {
        if (error) {
            NSLog(@"[ConferencesManager] fetchConferenceEndpointDetails returned an error: %@, received data %@", error, [receivedData objectFromJSONData]);
            if(completionHandler)
                completionHandler(error);
            
            return;
        }
        NSDictionary *response = [receivedData objectFromJSONData];
        if(!response) {
            NSLog(@"[ConferencesManager] fetchConferenceEndpointDetails JSON parsing failed");
            NSError *parsingError = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorDownloadDecodingFailedToComplete userInfo:nil];
            if(completionHandler)
                completionHandler(parsingError);
            return;
        }
        
        NSDictionary *data = [response objectForKey:@"data"];
        if(data){
            ConfEndpoint *aConfEndpoint = [self createOrUpdateConfEndpointFromJson:data];
            NSLog(@"[ConferencesManager] ConfEndPoint %@", aConfEndpoint);
        }
        
        if(completionHandler)
            completionHandler(error);
    }];
}

-(void) muteParticipant:(ConferenceParticipant *) participant inConference:(Conference *) conference {
    if(participant.muted){
        NSLog(@"[ConferencesManager] Participant %@ is already muted", participant);
        return;
    }
    [self muteUnmute:participant inConference:conference];
}

-(void) unmuteParticipant:(ConferenceParticipant *) participant inConference:(Conference *) conference {
    if(!participant.muted){
        NSLog(@"[ConferencesManager] Participant %@ is not muted", participant);
        return;
    }
    [self muteUnmute:participant inConference:conference];
}

-(void) muteUnmute:(ConferenceParticipant *) participant inConference:(Conference *) conference {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConferencesUpdateParticipantState, conference.confId, participant.participantId];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConferencesUpdateParticipantState];
    NSString *muteStr = !participant.muted ? @"mute" : @"unmute";
    NSDictionary *bodyContent = @{@"option":muteStr, @"mediaType": [ConfEndpoint stringFromConferenceEndPointMediaType:conference.endpoint.mediaType]};
    NSLog(@"[ConferencesManager] %@ of participant in conference", muteStr);
    [_downloadManager putRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:YES] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
          if (error) {
              NSLog(@"[ConferencesManager] UpdateMuteState returned a error: %@ receivedData %@", error, [receivedData objectFromJSONData]);
                }
        else {
         NSDictionary *dic = [receivedData objectFromJSONData];
         NSLog(@"[ConferencesManager] UpdateMuteState returned %@", dic);
        }
    }];
}

-(void) muteAllParticipantsInConference:(Conference *) conference {
    [self muteUnmuteAllParticipantsInConference:conference];
}

-(void) unmuteAllParticipantsInConference:(Conference *) conference {
    [self muteUnmuteAllParticipantsInConference:conference];
}

-(void) muteUnmuteAllParticipantsInConference:(Conference *) conference {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConferencesUpdateState, conference.confId];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConferencesUpdateState];
    
    NSString *muteStr = conference.allParticipantsMuted ? @"unmute" : @"mute";
    NSDictionary *bodyContent = @{@"option":muteStr, @"mediaType": [ConfEndpoint stringFromConferenceEndPointMediaType:conference.endpoint.mediaType]};
    NSLog(@"[ConferencesManager] UpdateMuteState of participant in conference");
    
    [_downloadManager putRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:YES] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"[ConferencesManager] muteUnmuteAllParticipantsInConference returned a error: %@ received data %@", error, [receivedData objectFromJSONData]);
        } else {
            NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
            
            NSDictionary *dic = [receivedData objectFromJSONData];
            NSLog(@"[ConferencesManager] muteUnmuteAllParticipantsInConference returned %@", dic);
            if(dic)
                conference.allParticipantsMuted = ! conference.allParticipantsMuted;
            [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
        }
    }];
}

-(void) disconnectParticipant:(ConferenceParticipant *) participant inConference:(Conference *) conference completionBlock:(ConferenceManagerDisconnectParticipantCompletionHandler) completionHandler {
    if (!participant) {
        NSLog(@"[ConferencesManager] Error Disconnect participant with null participant");
        if(completionHandler){
            NSError *error = [NSError errorWithDomain:@"ConferencesManager" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Error Disconnect participant with null participant"}];
            completionHandler(error);
        }
        return;
    }
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesConferencesUpdateParticipantState, conference.confId, participant.participantId];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?mediaType=%@",serviceUrl,[ConfEndpoint stringFromConferenceEndPointMediaType:conference.endpoint.mediaType]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConferencesUpdateParticipantState];
     NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
    NSLog(@"[ConferencesManager] Disconnect participant in conference");
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
       
            NSDictionary *dic = [receivedData objectFromJSONData];
            NSLog(@"[ConferencesManager] Disconnect participant returned %@", dic);
            // We consider the participant as removed if we don't get any error ...
            [conference.participants enumerateObjectsUsingBlock:^(ConferenceParticipant * aParticipant, NSUInteger idx, BOOL * stop) {
                if([aParticipant.participantId isEqualToString:participant.participantId]){
                    aParticipant.state = ParticipantStateDisconnected;
                    if([aParticipant.contact isEqual:_myUser.contact]) {
                        conference.myConferenceParticipant = aParticipant;
                        // TODO: what to do for WebRTC conferences ? Is a WebRTC conference stopped when the owner disconnect ?
                        if(conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                            conference.isActive = NO;
                        }
                    }
                    
                    *stop = YES;
                }
            }];
            if(!conference.isMyConference && [participant.contact isEqual:_myUser.contact]){
                [((NSMutableArray*)(conference.publishers)) removeAllObjects];
            }
            [((NSMutableArray*)conference.participants) removeObject:participant];
        
        [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
        
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

-(void) disconnectParticipant:(ConferenceParticipant *) participant inConference:(Conference *) conference {
    [self disconnectParticipant:participant inConference:conference completionBlock:nil];
}

-(void) inviteParticipants:(NSArray <Participant *> *) participants toJoinConference:(Conference *) conference inRoom:(Room *) room completionBlock:(ConferenceManagerInviteParticipantToJoinConferenceCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsConferenceInvitations, room.rainbowID, conference.confId];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsConferenceInvitations];

    NSArray *usersIds = [participants valueForKeyPath:@"@distinctUnionOfObjects.contact.rainbowID"];
    
    NSMutableDictionary *bodyContent = [NSMutableDictionary dictionaryWithDictionary:@{@"users":usersIds, @"emails":@[], @"instantMessage":@"", @"lang": [[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode]}];

    NSLog(@"[ConferencesManager] Inviting participants to conference");
    
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[ConferencesManager] Inviting participants to conference, error: sending invitations to conference : %@", error);
            if(completionHandler)
                completionHandler(error);
            return;
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            NSLog(@"[ConferencesManager] Inviting participants to conference, json response %@", jsonResponse);
            if(completionHandler)
                completionHandler(nil);
        }
    }];
}

-(void) cancelInvitationSentToParticipants:(NSArray <Participant *> *) participants toConference:(Conference *) conference inRoom:(Room *) room completionBlock:(ConferenceManagerCancelInvitationToJoinConferenceCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsConferenceInvitations, room.rainbowID, conference.confId];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsConferenceInvitations];
    NSArray *usersIds = [participants valueForKeyPath:@"@distinctUnionOfObjects.contact.rainbowID"];
    NSMutableDictionary *bodyContent = [NSMutableDictionary dictionaryWithDictionary:  @{@"users":usersIds, @"emails":@[], @"lang": [[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode]}];
    NSLog(@"[ConferencesManager] Cancel invitations sent to conference");
    [_downloadManager deleteRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[ConferencesManager] Cancel invitations sent to conference : error: sending conference cancellation notifications : %@", error);
            if(completionHandler)
                completionHandler(error);
            return;
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            NSLog(@"[ConferencesManager] Cancel invitations sent to conference : json response %@", jsonResponse);
            if(completionHandler)
                completionHandler(nil);
        }
    }];
}

-(void) didUpdateConference:(Conference *) conference withChangedKeys:(NSArray *) changedKeys {
    if(!conference || !changedKeys || [changedKeys count] == 0)
        return;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kConferencesManagerDidUpdateConference object:@{kConferenceKey: conference, kConferenceChangedAttributesKey: changedKeys}];
}

-(bool) hasContact:(Contact *)contact joinedConference: (Conference *)conference {
    for(ConferenceParticipant *participant in conference.participants){
        if(participant.contact == contact){
            return participant.state != ParticipantStateDisconnected ? YES : NO;
        }
    }
    return NO;
}

-(bool) hasMyUserJoinedConference: (Conference *)conference {
    return [self hasContact:[ServicesManager sharedInstance].myUser.contact joinedConference:conference];
}

#pragma mark - Conference delegate from XMPPService
/*
 Will deal with message like :
 <message xmlns="jabber:client" from="room_229f0a1285e9403b947658124fe74b5e@muc.jerome-all-in-one-dev-1.opentouch.cloud" to="fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud/mobile_ios_F0C550F7-F524-45D7-9036-CBDF86001643" type="groupchat" id="3329c818-7077-4965-8e50-7b9d38c2c653_2078"><subject xmlns="jabber:client">room event</subject><body xmlns="jabber:client">Jerome Company is sharing an end point in the bubble</body><event xmlns="jabber:client" name="conferenceAdd" jid="c2673e1952ef40178b68cb4ae2c6c2ed@jerome-all-in-one-dev-1.opentouch.cloud"/></message>
 */
-(void) xmppService:(XMPPService *) service didReceiveMessage:(Message *)message {
    NSLog(@"[ConferencesManager] Conference Manager did receive message %@", message);
    Room *room = [_roomsService getRoomByJid:message.peer.jid];
    Conference *theConference = room.conference;
    if(theConference){
        NSDictionary *currentConferenceInfo = [theConference dictionaryRepresentation];
        if(message.groupChatEventType == MessageGroupChatEventConferenceAdd){
            theConference.endedConference = NO;
            theConference.canJoin = YES;
            theConference.myConferenceParticipant.state = ParticipantStateDisconnected;
            theConference.endpoint.attachedRoomID = room.rainbowID;
            [_roomsService updateRoomInCache:room];
            [_roomsService fetchRoomDetails:room];
        }
        if(message.groupChatEventType == MessageGroupChatEventConferenceRemove){
            theConference.canJoin = NO;
            theConference.isActive = NO;
            theConference.myConferenceParticipant.state = ParticipantStateDisconnected;
            [theConference resetParticipants];
            theConference.endpoint.attachedRoomID = nil;
            [_roomsService updateRoomInCache:room];
        }
        [self didUpdateConference:theConference withChangedKeys:[currentConferenceInfo changedKeysIn:[theConference dictionaryRepresentation]]];
    } else {
        NSLog(@"[ConferencesManager] No conference found for this room %@, don't treat this message", room);
    }
}

-(void) xmppService:(XMPPService *) service didReceiveConferenceInfo:(ConferenceInfo *) conferenceInfo {
    NSString *confId = conferenceInfo.confId;
    Conference *conference = [self getConferenceByRainbowID:confId];
    
    switch (conferenceInfo.eventType) {
            
        case ConferenceEventTypeState: {
            if(conference){
                NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
                conference.isActive = conferenceInfo.isActive;
                conference.isRecording = conferenceInfo.isRecording;
                conference.isTalkerActive = conferenceInfo.isTalkerActive;
                
                // Add participants
                for (ConferenceParticipant *aParticipant in conferenceInfo.addedParticipants) {
                    if(![conference.participants containsObject:aParticipant]){
                        [((NSMutableArray *)conference.participants) addObject:aParticipant];
                        if(aParticipant.contact == _myUser.contact){
                            conference.myConferenceParticipant = aParticipant;
                        }
                    }
                }
                
                [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
            } else {
                NSLog(@"[ConferencesManager] Unknown conference ask to server");
            }
            break;
        }
            
        case ConferenceEventTypeParticipantsAdded:{
            NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
            for (ConferenceParticipant *aParticipant in conferenceInfo.addedParticipants) {
                if(![conference.participants containsObject:aParticipant])
                    [((NSMutableArray *)conference.participants) addObject:aParticipant];
            }
            [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
            break;
        }
            
        case ConferenceEventTypeParticipantsUpdate:{
            NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
            for (ConferenceParticipant *aParticipant in conferenceInfo.updatedParticipants) {
                NSInteger index = [conference.participants indexOfObject:aParticipant];
                if(index != NSNotFound){
                    ConferenceParticipant *theParticipant = [conference.participants objectAtIndex:index];
                    [theParticipant updateParticipantWithNewParticipant:aParticipant];
                }
            }
            [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
            break;
        }
        
        case ConferenceEventTypeParticipantsRemoved:{
            NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
            for (NSString *participantID in conferenceInfo.removedParticipants) {
                NSMutableArray<ConferenceParticipant*> *removedParticipants = [NSMutableArray new];
                [conference.participants enumerateObjectsUsingBlock:^(ConferenceParticipant * aParticipant, NSUInteger idx, BOOL * stop) {
                    if([aParticipant.participantId isEqualToString:participantID]){
                        aParticipant.state = ParticipantStateDisconnected;
                        [removedParticipants addObject:aParticipant];
                        if([aParticipant.contact isEqual:_myUser.contact]) {
                            conference.myConferenceParticipant = aParticipant;
                            // TODO: what to do for WebRTC conferences ? Is a WebRTC conference stopped when the owner disconnect ?
                            if(conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                                conference.isActive = NO;
                            }
                        }
                        *stop = YES;
                    }
                }];
                
                [((NSMutableArray*)conference.participants) removeObjectsInArray:removedParticipants];
            }
            [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
            break;
        }
            
        case ConferenceEventTypeTalkers:{
            // TODO : check if active talkers are good now
            NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
            if(conferenceInfo.talkers.count > 0){
                for (ConferenceParticipant *participant in conference.participants) {
                    if([conferenceInfo.talkers containsObject:participant.participantId])
                        participant.isTalking = YES;
                    else
                        participant.isTalking = NO;
                }
            } else {
                for (ConferenceParticipant *participant in conference.participants) {
                    participant.isTalking = NO;
                }
            }
            [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
            break;
        }
            
        case ConferenceEventPublishersAdded:{
            NSLog(@"[ConferencesManager] PUBLISHER ADDED %@", conferenceInfo);
            NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
            for (ConferencePublisher *aPublisher in conferenceInfo.addedPublishers) {
                // Only video publisher can be subscribed so filter them here
                if(aPublisher.mediaType == PublisherMediaTypeVideo){
                    if(![conference.publishers containsObject:aPublisher])
                        [((NSMutableArray *)conference.publishers) addObject:aPublisher];
                }
            }
            [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
            break;
        }
            
        case ConferenceEventPublishersRemoved:{
            NSLog(@"[ConferencesManager] PUBLISHER REMOVED %@", conferenceInfo);
            NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
            
            for (ConferencePublisher *aPublisher in conferenceInfo.removedPublishers) {
                NSMutableArray<ConferencePublisher*> *removedPublisher = [NSMutableArray new];
                [conference.publishers enumerateObjectsUsingBlock:^(ConferencePublisher * thePublisher, NSUInteger idx, BOOL * stop) {
                    if([aPublisher.publisherID isEqualToString:thePublisher.publisherID]){
                        [removedPublisher addObject:thePublisher];
                        *stop = YES;
                    }
                }];
                 [((NSMutableArray*)conference.publishers) removeObjectsInArray:removedPublisher];
            }
            [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - ConfEndpoints
-(ConfEndpoint *) getConfEndpointByRainbowID:(NSString *) confEndpointRainbowID {
    __block ConfEndpoint *confEndpoint = nil;
    @synchronized (_conferencesMutex) {
        [_conferenceEndpoints enumerateObjectsUsingBlock:^(ConfEndpoint * aConfEndpoint, NSUInteger idx, BOOL * stop) {
            if([aConfEndpoint.confEndpointId isEqualToString:confEndpointRainbowID]){
                confEndpoint = aConfEndpoint;
                *stop = YES;
            }
        }];
    }
    return confEndpoint;
}

-(ConfEndpoint *) createOrUpdateConfEndpointFromJson:(NSDictionary *) jsonDictionary {
    ConfEndpoint *confEndpoint = nil;
    NSString *confEndPointID = [jsonDictionary[@"id"] notNull];
    if(!confEndPointID){
        // Check if the conf endpoint id is not called confEndpointId in the dictionary
        confEndPointID = [jsonDictionary[@"confEndpointId"] notNull];
        if(!confEndPointID){
            NSLog(@"[ConferencesManager] No confendpoint id");
        }
    }
    @synchronized (_conferencesMutex) {
        BOOL newConfEndpoint = NO;
        confEndpoint = [self getConfEndpointByRainbowID:confEndPointID];
        if(!confEndpoint){
            confEndpoint = [ConfEndpoint new];
            newConfEndpoint = YES;
            [_conferenceEndpoints addObject:confEndpoint];
        }
        
        confEndpoint.confEndpointId = confEndPointID;
        if([jsonDictionary[@"userId"] notNull])
            confEndpoint.userId = [jsonDictionary[@"userId"] notNull];
        if([jsonDictionary[@"confUserId"] notNull])
            confEndpoint.confUserId = [jsonDictionary[@"confUserId"] notNull];
        if([jsonDictionary[@"providerConfId"] notNull])
            confEndpoint.providerConfId = [jsonDictionary[@"providerConfId"] notNull];
        if([jsonDictionary[@"providerUserId"] notNull])
            confEndpoint.providerUserId = [jsonDictionary[@"providerUserId"] notNull];
        if([jsonDictionary[@"providerType"] notNull])
            confEndpoint.providerType = [jsonDictionary[@"providerType"] notNull];
        if([jsonDictionary[@"mediaType"] notNull])
            confEndpoint.mediaType = [ConfEndpoint conferenceEndPointMediaTypeFromString:[jsonDictionary[@"mediaType"] notNull]];
        if([jsonDictionary[@"companyId"] notNull])
            confEndpoint.companyId = [jsonDictionary[@"companyId"] notNull];
        if([jsonDictionary[@"scheduled"] notNull])
            confEndpoint.isScheduled = [[jsonDictionary[@"scheduled"] notNull] boolValue];
        if([jsonDictionary[@"confDialOutDisabled"] notNull])
            confEndpoint.isDialOutDisabled = [[jsonDictionary[@"confDialOutDisabled"] notNull] boolValue];

        if([jsonDictionary[@"phoneNumbers"] notNull]){
            NSArray *phoneNumbers = [jsonDictionary[@"phoneNumbers"] notNull];
            [((NSMutableArray*)confEndpoint.phoneNumbers) removeAllObjects];
            
            for (NSDictionary *phoneNumber in phoneNumbers) {
                PhoneNumber *aPhoneNumber = [PhoneNumber new];
                aPhoneNumber.numberE164 = [phoneNumber[@"number"] notNull];
                aPhoneNumber.countryName = [phoneNumber[@"location"] notNull];
                aPhoneNumber.countryCode = [phoneNumber[@"locationcode"] notNull];
                aPhoneNumber.type = PhoneNumberTypeConference;
                BOOL needLanguageSelection = [[phoneNumber[@"needLanguageSelection"] notNull] boolValue];
                aPhoneNumber.needLanguageSelection = needLanguageSelection;
                [((NSMutableArray*)confEndpoint.phoneNumbers) addObject:aPhoneNumber];
            }
        }
        if([jsonDictionary[@"passCodes"] notNull]){
            for (NSDictionary *passCodesDic in [jsonDictionary[@"passCodes"] notNull]) {
                if([[passCodesDic[@"name"] notNull] isEqualToString:@"ModeratorPassCode"]){
                    confEndpoint.moderatorCode = [passCodesDic[@"value"] notNull];
                }
                if([[passCodesDic[@"name"] notNull] isEqualToString:@"ParticipantPassCode"]){
                    confEndpoint.participantCode = [passCodesDic[@"value"] notNull];
                }
                if([[passCodesDic[@"name"] notNull] isEqualToString:@"ListenOnlyPassCode"]){
                    confEndpoint.listenOnlyCode = [passCodesDic[@"value"] notNull];
                }
            }
        }
    }
    return confEndpoint;
}

#pragma mark - Conferences
-(Conference *) getConferenceByRainbowID:(NSString *) conferenceRainbowID {
    __block Conference *foundConference = nil;
    @synchronized (_conferencesMutex) {
        [_conferences enumerateObjectsUsingBlock:^(Conference * aConference, NSUInteger idx, BOOL * stop) {
            if([aConference.confId isEqualToString:conferenceRainbowID]){
                foundConference = aConference;
                *stop = YES;
            }
        }];
    }
    return foundConference;
}

-(Conference *) createOrUpdateConferenceFromJson:(NSDictionary *) jsonDictionary {
    NSString *conferenceID = [[jsonDictionary objectForKey:@"id"] notNull];
    if(!conferenceID){
        // Check if the conf endpoint id is not called confEndpointId in the dictionary, this could be the case when the jsonDictionnary is
        // from fetchRoomDetails, then the confId is read from the endpoint
        conferenceID = [jsonDictionary[@"confEndpointId"] notNull];
        if(!conferenceID){
            NSLog(@"[ConferencesManager] No conference ID found, we assume that is a ended conference");
        }
    }
    
    Conference *conference = nil;
    BOOL newConference = NO;
    @synchronized (_conferencesMutex) {
        conference = [self getConferenceByRainbowID:conferenceID];
        if(!conference){
            conference = [Conference new];
            newConference = YES;
            
            [_conferences addObject:conference];
            
            conference.confId = conferenceID;
            if(!conference.confId)
                conference.endedConference = YES;
            
            if([jsonDictionary[@"scheduledStartDate"] notNull] && jsonDictionary[[@"scheduledEndDate" notNull]]){
                conference.type = ConferenceTypeScheduled;
            } else {
                conference.type = ConferenceTypeInstant;
            }
        }
    }
    
    NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
    NSArray *confEndPoints = [jsonDictionary[@"confEndpoints"] notNull];
    if(confEndPoints && confEndPoints.count > 0){
        NSDictionary *confEndPointDict = [confEndPoints firstObject];
        ConfEndpoint *confEndPoint = [self createOrUpdateConfEndpointFromJson:confEndPointDict];
        conference.endpoint = confEndPoint;
    }
    // If there is no endpoint in the conference, try to create one with the provided data
    if(!conference.endpoint){
        ConfEndpoint *confEndPoint = [self createOrUpdateConfEndpointFromJson:jsonDictionary];
        conference.endpoint = confEndPoint;
    }
    if(conference.endpoint && conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio && conference.endpoint.confEndpointId){
        // if there is a confEndPointId that means the confendpoint is attached to this conference.
        conference.status = ConferenceStatusAttached;
    }
    
    if(conference.type == ConferenceTypeScheduled){
        conference.start = [NSDate dateFromJSONString:[jsonDictionary[@"scheduledStartDate"] notNull]];
        conference.end = [NSDate dateFromJSONString:[jsonDictionary[@"scheduledEndDate"] notNull]];
    } else if(conference.type == ConferenceTypeInstant){
        conference.start = [NSDate dateFromJSONString:[jsonDictionary[@"scheduledStartDate"] notNull]];
    }
    
    if(newConference){
        [[NSNotificationCenter defaultCenter] postNotificationName:kConferencesManagerDidAddConference object:conference];
    } else {
        [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
    }
    
    return conference;
}

-(void) updateConference:(Conference *) conference withSnapshotJson:(NSDictionary *)jsonDictionary {
    
    NSDictionary *data = [jsonDictionary objectForKey:@"data"];
    for (NSDictionary *dic in data[@"participants"]) {
        NSString *participantId = [[dic objectForKey:@"participantId"] notNull];
        NSString *jidIM = [[dic objectForKey:@"jid_im"] notNull];
        NSString *phoneNumber = [[dic objectForKey:@"phoneNumber"] notNull];
        NSString *roleString = [[dic objectForKey:@"participantRole"] notNull];
        ParticipantRole role = [ConferenceParticipant participantRoleFromNSString:roleString];
        NSString *stateString = [[dic objectForKey:@"participantState"] notNull];
        ParticipantState state = [ConferenceParticipant participantStateFromNSString:stateString];
        BOOL muted = [[[dic objectForKey:@"mute"] notNull] boolValue];
        BOOL hold =  [[[dic objectForKey:@"held"] notNull] boolValue];
        
        ConferenceParticipant *participant = [ConferenceParticipant new];
        participant.participantId = participantId;
        participant.jidIM = jidIM;
        participant.phoneNumber = phoneNumber;
        participant.role = role;
        participant.state = state;
        participant.muted = muted;
        participant.hold = hold;
        participant.contact = [_contactsManagerService createOrUpdateRainbowContactWithJid:participant.jidIM];
        if(participant.contact == _myUser.contact){
            conference.myConferenceParticipant = participant;
        }
        if([conference.participants containsObject:participant]){
            NSInteger index = [conference.participants indexOfObject:participant];
            if(index != NSNotFound){
                ConferenceParticipant *theParticipant = [conference.participants objectAtIndex:index];
                [theParticipant updateParticipantWithNewParticipant:participant];
            }
        } else {
            [((NSMutableArray *)conference.participants) addObject:participant];
        }
        
    }
}

-(ConfEndpoint *) webRTCConferenceEndPoint {
    __block ConfEndpoint *conferenceEndPoint = nil;
    @synchronized (_conferencesMutex) {
        [_conferenceEndpoints enumerateObjectsUsingBlock:^(ConfEndpoint *confEndPoint, NSUInteger idx, BOOL * stop) {
            if(confEndPoint.mediaType == ConferenceEndPointMediaTypeWebRTC && confEndPoint.confEndpointId && [confEndPoint.userId isEqualToString:_myUser.contact.rainbowID]){
                conferenceEndPoint = confEndPoint;
                *stop = YES;
            }
        }];
    }
    return conferenceEndPoint;
}

-(ConfEndpoint *) pstnInstantConferenceEndPoint {
    __block ConfEndpoint *conferenceEndPoint = nil;
    @synchronized (_conferencesMutex) {
        [_conferenceEndpoints enumerateObjectsUsingBlock:^(ConfEndpoint *confEndPoint, NSUInteger idx, BOOL * stop) {
            if(confEndPoint.mediaType == ConferenceEndPointMediaTypePSTNAudio && confEndPoint.isScheduled == NO && confEndPoint.confEndpointId && [confEndPoint.userId isEqualToString:_myUser.contact.rainbowID]){
                conferenceEndPoint = confEndPoint;
                *stop = YES;
            }
        }];
    }
    return conferenceEndPoint;
}

-(BOOL) hasWebRTCInstantConference {
    return [self webRTCConferenceEndPoint] != nil;
}

-(BOOL) hasPstnInstantConference {
    return [self pstnInstantConferenceEndPoint] != nil;
}

-(void) startConference:(Conference *) conference completionHandler:(ConferenceManagerStartConferenceCompletionHandler) completionHandler {
    NSAssert(conference.confId, @"Conference id is mandatory");
    if(!conference.confId){
        NSError *error = [NSError errorWithDomain:ConferenceManagerErrorDomainStart code:400 userInfo:@{NSLocalizedDescriptionKey:@"Conference is mandatory to start a conference"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    NSAssert(conference.isMyConference, @"Trying to start a conference that we are not the owner");
    if(!conference.isMyConference){
        NSError *error = [NSError errorWithDomain:ConferenceManagerErrorDomainStart code:400 userInfo:@{NSLocalizedDescriptionKey:@"Trying to start a conference that we are not the owner"}];
        if(completionHandler)
            completionHandler(error);
        return;
    }
    
    // We must not start a scheduled conference (PSTN) if the current time is less than 5 min before the conference start time
    if(conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
        if(conference.endpoint.isScheduled){
            NSDate *start5minEarlier = [conference.start dateBySubtractingMinutes:5];
            if([[NSDate date] isEarlierThanDate:start5minEarlier]){
                NSLog(@"[ConferencesManager] Don't start the conference it has been done automatically by the server");
                NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
                conference.isActive = YES;
                [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
                if(completionHandler)
                    completionHandler(nil);
                return;
            }
        }
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConferencesStart, conference.confId];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConferences];
    NSLog(@"[ConferencesManager] start an audio conference");
    NSMutableDictionary *body = [NSMutableDictionary dictionaryWithDictionary: @{@"mediaType": [ConfEndpoint stringFromConferenceEndPointMediaType:conference.endpoint.mediaType]}];
    if(conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC){
        if(conference.endpoint.attachedRoomID){
            body[@"roomId"] = conference.endpoint.attachedRoomID;
        } else {
            NSLog(@"[ConferencesManager] The WebRTC conference has no attached room, don't start it");
            return;
        }
    }
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
        if (error) {
            NSDictionary *response = [receivedData objectFromJSONData];
            NSLog(@"[ConferencesManager] Start conference returned an error: %@, receivedData %@", error, response);
            if(response){
                NSInteger errorDetailsCode = [response[@"errorDetailsCode"] integerValue];
                if(errorDetailsCode == 500022 || errorDetailsCode == 500025){
                    conference.isActive = YES;
                    // Consider this error as ok
                    if(completionHandler)
                        completionHandler(nil);
                    return;
                }
            }
            [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
        } else {
            NSLog(@"[ConferencesManager] Start conference succeeded");
            conference.isActive = YES;
            [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
        }
        if(completionHandler)
            completionHandler(error);
    }];
}

-(void) terminateConference:(Conference *) conference completionHandler:(ConferenceManagerTerminateConferenceCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesConferencesStop, conference.confId];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConferencesStop];
    NSMutableDictionary *body = [NSMutableDictionary dictionaryWithDictionary: @{@"mediaType": [ConfEndpoint stringFromConferenceEndPointMediaType:conference.endpoint.mediaType]}];
    if(conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC){
        if(conference.endpoint.attachedRoomID){
            body[@"roomId"] = conference.endpoint.attachedRoomID;
        } else {
            NSLog(@"[ConferencesManager] The WebRTC conference has no attached room, don't stop it");
            return;
        }
    }
    NSLog(@"[ConferencesManager] Terminate a audio conference %@", url);
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"[ConferencesManager] Terminate a audio conference returned an error: %@ receivedData %@", error, [receivedData objectFromJSONData]);
        } else {
            NSLog(@"[ConferencesManager] Terminate a audio conference succeeded");
        }
        NSDictionary *currentConferenceInfo = [conference dictionaryRepresentation];
        conference.isActive = NO;
        conference.myConferenceParticipant = nil;
        conference.status = ConferenceStatusUnknown;
        [((NSMutableArray*)conference.publishers) removeAllObjects];
        [self didUpdateConference:conference withChangedKeys:[currentConferenceInfo changedKeysIn:[conference dictionaryRepresentation]]];
        
        if(completionHandler)
            completionHandler(error);
    }];
}

-(NSArray<ConferenceParticipant*> *) createConferenceParticipantsFromRoom: (Room*) room {
    if(room.participants.count == 0)
        return nil;
    
    NSMutableArray<ConferenceParticipant*> *participants = [NSMutableArray new];
    [room.participants enumerateObjectsUsingBlock:^(Participant * _Nonnull roomParticipant, NSUInteger idx, BOOL * _Nonnull stop) {
        
        ConferenceParticipant *participant = [ConferenceParticipant new];
        participant.participantId = nil;
        participant.jidIM = roomParticipant.contact.jid;
        participant.phoneNumber = nil;
        participant.role = roomParticipant.privilege == ParticipantPrivilegeModerator ? ParticipantRoleModerator : ParticipantRoleMember;
        participant.state = ParticipantStateDisconnected;
        participant.muted = NO;
        participant.hold = NO;
        participant.contact = roomParticipant.contact;
        
        [participants addObject:participant];
    }];
    
    return participants;
}

#pragma mark - publishers
-(void) subscribeToVideoSharedByConferencePublisher:(ConferencePublisher *) conferencePublisher inConference:(Conference *) conference {
    // Check if the subscriber is already subscribed
    ConferencePublisher *currentlySubscribedPublisher = nil;
    for (ConferencePublisher *publisher in conference.publishers) {
        if(publisher.subscribed){
            currentlySubscribedPublisher = publisher;
        }
    }
    if(currentlySubscribedPublisher)
        [self unsubscribeToVideoSharedBy:currentlySubscribedPublisher inConference:conference];
    
    NSURL *baseUrl = [_apiUrlManagerService getURLForService:ApiServicesConferencesSubscribe, conference.confId];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/participants/%@/subscribe",baseUrl.absoluteString, conferencePublisher.publisherID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesConferencesStop];
    NSLog(@"[ConferencesManager] Subscribe to video shared by %@", url);
    [_downloadManager postRequestWithURL:url body:[@{@"mediaType":[ConfEndpoint stringFromConferenceEndPointMediaType:conference.endpoint.mediaType]} jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"[ConferencesManager] Subscribe to video shared returned an error: %@ receivedData %@", error, [receivedData objectFromJSONData]);
        } else {
            NSLog(@"[ConferencesManager] Subscribe to video shared succeeded");
            conferencePublisher.subscribed = YES;
        }
    }];
}

-(void) unsubscribeToVideoSharedBy:(ConferencePublisher *) conferencePublisher inConference:(Conference *) conference {
    NSLog(@"[ConferencesManager] Unsubscribe to video shared by %@", conferencePublisher);
    [_rtcService hangupCallForPublisherRainbowID:conferencePublisher.publisherID];
    conferencePublisher.subscribed = NO;
}
@end
