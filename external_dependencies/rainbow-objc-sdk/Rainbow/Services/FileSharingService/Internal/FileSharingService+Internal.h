/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "FileSharingService.h"
#import "DownloadManager.h"
#import "ApiUrlManagerService.h"
#import "ContactsManagerService+Internal.h"
#import "RoomsService+Internal.h"
#import "ApiUrlManagerService.h"

typedef void (^FileSharingAddViewerComplionHandler) (File *file, NSError *error);

@interface FileSharingService ()
@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) ApiUrlManagerService *apiURLManager;
@property (nonatomic, strong) ContactsManagerService *contactsManager;
@property (nonatomic, strong) RoomsService *roomsService;
@property (nonatomic, strong) MyUser *myUser;
@property (nonatomic, strong, readwrite) NSMutableArray <File *> *files;
@property (readwrite) NSInteger currentSize;
@property (readwrite) NSInteger maxQuotaSize;

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiURLManager:(ApiUrlManagerService *) apiURLManager contactsManagerService:(ContactsManagerService *) contactsManagerService roomService:(RoomsService *) roomsService myUser:(MyUser *) myUser;

-(void) fetchDataForFile:(File *) file sizeLimit:(NSInteger) sizeLimit completionHandler:(FileSharingComplionHandler) completionHandler;

-(void) fetchFileInformation:(File *) file completionHandler:(FileSharingComplionHandler) completionHandler;

-(File *) createFileWithFileName:(NSString *) fileName size:(NSInteger) size mimeType:(NSString *) mimeType data:(NSData *) data url:(NSURL *) url;
-(File *) getOrCreateFileWithRainbowID:(NSString *) rainbowID withInfos:(NSDictionary *) infos;

-(void) downloadDataForFile:(File *) file withCompletionHandler:(FileSharingComplionHandler) completionHandler;

-(void) addViewer:(Peer *) viewer forFile:(File *) file withCompletionHandler:(FileSharingAddViewerComplionHandler) completionHandler;

-(void) uploadFile:(File *) file forConversation:(Conversation *) conversation completionHandler:(FileSharingComplionHandler) completionHandler;

- (void) updateFile:(File *) file withJsonDic:(NSDictionary *) jsonDic;

-(void) deleteFile:(File *) file withCompletionHandler :(FileSharingComplionHandler) completionHandler;
@end
