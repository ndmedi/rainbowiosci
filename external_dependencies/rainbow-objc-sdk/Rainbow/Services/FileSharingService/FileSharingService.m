/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "FileSharingService.h"
#import "FileSharingService+Internal.h"
#import "LoginManager+Internal.h"
#import "NSData+JSON.h"
#import "File+Internal.h"
#import "NSObject+NotNull.h"
#import "NSDate+JSONString.h"
#import "PINCache.h"
#import "NSDate+Utilities.h"
#import "NSData+MimeType.h"
#import "NSDictionary+JSONString.h"
#import "Tools.h"
#import "NSString+FileSize.h"

@interface FileSharingService ()
@property (nonatomic, strong) PINCache *fileSharingCache;
@property (nonatomic, strong) PINCache *thumbmailFileSharingCache;
@end

NSString *const kFileSharingServiceDidUpdateUploadedBytesSent = @"UploadedBytesSent";
NSString *const kFileSharingServiceDidUpdateDownloadedBytes = @"DownloadedBytes";
NSString *const kFileSharingServiceDidUpdateFile = @"didUpdateFile";
NSString *const kFileSharingServiceDidRemoveFile = @"didRemoveFile";
NSInteger const kOneMegaByte = 1024*1024;
NSInteger const kOneKiloByte = 1024;

typedef void (^FileSharingServiceDownloadFileCompletionHandler)(NSData *receivedData, NSError *error);
typedef void (^FileSharingServiceDownloadFileProgressionHandler) (File* file, double totalBytesSent, double totalBytesExpectedToSend);

@implementation FileSharingService
-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiURLManager:(ApiUrlManagerService *) apiURLManager contactsManagerService:(ContactsManagerService *) contactsManagerService roomService:(RoomsService *) roomsService myUser:(MyUser *)myUser {
    self = [super init];
    if(self){
        _downloadManager = downloadManager;
        _apiURLManager = apiURLManager;
        _contactsManager = contactsManagerService;
        _roomsService = roomsService;
        _myUser = myUser;
        _files = [NSMutableArray array];
        
        _fileSharingCache = [[PINCache alloc] initWithName:@"fileSharingCache"];
        _fileSharingCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        [_fileSharingCache trimToDateAsync:[NSDate dateWithDaysBeforeNow:30] completion:nil];
        

        _thumbmailFileSharingCache = [[PINCache alloc] initWithName:@"thumbmailFileSharingCache"];
        _thumbmailFileSharingCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        [_thumbmailFileSharingCache trimToDate:[NSDate dateWithDaysBeforeNow:30]];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    _downloadManager = nil;
    _apiURLManager = nil;
    _contactsManager = nil;
    _myUser = nil;
    [_files removeAllObjects];
    _files = nil;
    _fileSharingCache = nil;
    _thumbmailFileSharingCache = nil;
}

-(void) didLogin:(NSNotification *) notification {
    [self fetchFileSharingConsumption];
}

-(void) didLogout:(NSNotification *) notification {
    [_files removeAllObjects];
}

-(void) didChangeUser:(NSNotification *) notification {
    [_fileSharingCache removeAllObjects];
    [_thumbmailFileSharingCache removeAllObjects];
}

-(void) didReconnect:(NSNotification *) notification {
    [self fetchFileSharingConsumption];
}

#pragma mark - fetch list of files
-(void) fetchListOfFilesFromOffset:(NSInteger)offset withLimit:(NSInteger)limit withTypeMIME:(FilterFileType)typeMIME withCompletionHandler:(FileSharingRefreshSharedFileListComplionHandler) completionHandler {
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?format=full&offset=%li&sortField=registrationDate&limit=%li&sortOrder=-1%@",[_apiURLManager getURLForService:ApiServicesFileStorage],(long)offset,(long)limit,[self getURLFromFileType:typeMIME]]];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileStorage];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get list of files : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if(completionHandler)
                completionHandler(nil,0,0, [NSError errorWithDomain:@"FileSharing" code:500 userInfo:@{NSLocalizedDescriptionKey:[error description]}]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get list of files due to JSON parsing failure");
            if(completionHandler)
                completionHandler(nil,0,0, [NSError errorWithDomain:@"FileSharing" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Error: Cannot get list of files due to JSON parsing failure"}]);
            return;
        }
        [_files removeAllObjects];
        NSArray *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get list of files, response ...");
        for (NSDictionary *fileDic in data) {
            [self createOrUpdateFileWithJsonDic:fileDic];
        }
        // Download the thumbnail for file where we need one but we don't have it
        for (File *aFile in _files) {
            if(aFile.canDownloadThumbnail && !aFile.thumbnailData && aFile.hasThumbnailOnServer){
                [self downloadThumbnailDataForFile:aFile withCompletionHandler:^(File *file, NSError *error) {
                    if (file.thumbnailData) {
                        NSDictionary *jsonDic = @{@"thumbnailData":file.thumbnailData};
                        [self updateFile:aFile withJsonDic:jsonDic];
                    }
                }];
            }
        }
        NSUInteger offset = [((NSString *)([jsonResponse[@"offset"] notNull])) integerValue];
        NSUInteger total = [((NSString *)([jsonResponse[@"total"] notNull])) integerValue];

        if(completionHandler)
            completionHandler(_files,offset,total,nil);
    }];
}

-(void) refreshSharedFileListFromOffset :(NSInteger)offset withLimit:(NSInteger)limit withTypeMIME:(FilterFileType)typeMIME withCompletionHandler:(FileSharingRefreshSharedFileListComplionHandler) completionHandler{
    // Fetch consumption only the first time we load the file list
    if(offset == 0)
        [self fetchFileSharingConsumption];
    [self fetchListOfFilesFromOffset:offset withLimit:limit withTypeMIME:typeMIME withCompletionHandler:completionHandler];
}

-(void) fetchFileSharingConsumption {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/consumption", [_apiURLManager getURLForService:ApiServicesFileStorageBase]]];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileStorageBase];
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get file sharing consumption : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get file sharing consumption due to JSON parsing failure");
            return;
        }
        
        NSDictionary *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get file sharing consumption, response ...");
        if([data[@"feature"] isEqualToString:@"FILE_SHARING_QUOTA_GB"]){
            NSInteger currentValue = [((NSString *)([data[@"currentValue"] notNull])) integerValue];
            NSInteger maxValue = [((NSString *)([data[@"maxValue"] notNull])) integerValue];
            NSString *unit = [data[@"unit"] notNull];
            if([unit isEqualToString:@"octet"]){
                _maxQuotaSize = [[NSString formatFileSizeWithoutUnit:maxValue] fileSizeFromFormat];
                _currentSize = currentValue;
                NSLog(@"FILE SHARING QUOTA %ld  current size %ld", _maxQuotaSize, _currentSize);
            }
        }
    }];
}

-(File *) getFileByRainbowID:(NSString *) rainbowID {
    __block File *theFile = nil;
    @synchronized (_files) {
        [_files enumerateObjectsUsingBlock:^(File * obj, NSUInteger idx, BOOL * stop) {
            if([obj.rainbowID isEqualToString:rainbowID]){
                theFile = obj;
                *stop = YES;
            }
        }];
    }
    
    return theFile;
}

/**
 {
    fileName = "Absences OTPC R&D.xls";
    id = 58d943ca8dd8b4bddac15597;
 
    ownerId = 5739dead1217912fe6bdcce5;
 
    size = 35840;
    typeMIME = "application/vnd.ms-excel";
    uploadedDate = "2017-03-27T16:54:35.368Z";
    viewers =         (
        {
            downloadedDate = "2017-03-27T16:54:46.725Z";
            type = user;
            viewerId = 58adb02c692a1aaf01ef475e;
        }
    );
 }
 */

- (void) updateFile:(File *) file withJsonDic:(NSDictionary *) jsonDic {
    @synchronized (_files) {
        File * aFile = [self getFileByRainbowID:file.rainbowID];
        if (jsonDic[@"isDownloadAvailable"]) {
            BOOL isDownloadAvailable = [jsonDic[@"isDownloadAvailable"] boolValue];
            aFile.isDownloadAvailable = isDownloadAvailable;
            aFile.thumbnailData = nil;
            aFile.data = nil;
        }
        
        if (jsonDic[@"thumbnailData"]){
            aFile.thumbnailData = (NSData *)jsonDic[@"thumbnailData"];
        }
        // update current file in chat view
        [[NSNotificationCenter defaultCenter] postNotificationName:kFileSharingServiceDidUpdateFile object:aFile];
      
    }
    
}

-(File *) createOrUpdateFileWithJsonDic:(NSDictionary *) jsonDic {
    @synchronized (_files) {
        NSString *rainbowID = jsonDic[@"id"];
        BOOL newFile = NO;
        File *theFile = [self getFileByRainbowID:rainbowID];
        if(!theFile){
            theFile = [File new];
            theFile.rainbowID = rainbowID;
            newFile = YES;
        }
        
        theFile.fileName = [jsonDic objectForKey:@"fileName"];
        theFile.size = ((NSString*)[jsonDic objectForKey:@"size"]).integerValue;
        theFile.mimeType = [[jsonDic objectForKey:@"typeMIME"] notNull];
        if(!theFile.mimeType)
            theFile.mimeType = @"application/octet-stream";
        
        id dateToSort = [jsonDic objectForKey:@"dateToSort"];
        if ([dateToSort isKindOfClass:[NSDate class]]) {
            theFile.dateToSort = (NSDate *)dateToSort;
        } else if ([dateToSort isKindOfClass:[NSString class]]) {
            theFile.dateToSort = [NSDate dateFromJSONString:(NSString *) dateToSort];
        }
        
        id updateDate = [jsonDic objectForKey:@"uploadedDate"];
        if ([updateDate isKindOfClass:[NSDate class]]) {
            theFile.uploadDate = (NSDate *)updateDate;
        } else if ([updateDate isKindOfClass:[NSString class]]) {
            theFile.uploadDate = [NSDate dateFromJSONString:(NSString *) updateDate];
        }
        
        id registrationDate = [jsonDic objectForKey:@"registrationDate"];
        if ([registrationDate isKindOfClass:[NSDate class]]) {
            theFile.registrationDate = (NSDate *)registrationDate;
        } else if ([registrationDate isKindOfClass:[NSString class]]) {
            theFile.registrationDate = [NSDate dateFromJSONString:(NSString *) registrationDate];
        }
        
        NSArray *viewers = [jsonDic objectForKey:@"viewers"];
    
         [theFile removeAllPeerFromViewers];
        
        for (NSDictionary *viewerDic in viewers) {
            NSString *viewerType = viewerDic[@"type"];
            if([viewerType isEqualToString:@"user"]){
                [_contactsManager getOrCreateRainbowContactAsynchronouslyWithRainbowID:viewerDic[@"viewerId"] withCompletionHandler:^(Contact *contact) {
                    if(contact)
                        [theFile addPeerInViewers:contact];
                }];
            }
            if([viewerType isEqualToString:@"room"]){
                [_roomsService getRoomAsynchronouslyWithRainbowID:viewerDic[@"viewerId"] withCompletionHandler:^(Room *room) {
                    if(room){
                         [theFile addPeerInViewers:room];
                    }
                }];
            
            }
        }
        Contact *owner = [_contactsManager createOrUpdateRainbowContactWithRainbowId:jsonDic[@"ownerId"]];
        if(owner)
            theFile.owner = owner;
        
        if(!theFile.url){
            NSURL *OBDataUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [_apiURLManager getURLForService:ApiServicesFileServer], rainbowID]];
            theFile.url = OBDataUrl;
        }
        
        NSDictionary *thumbnailInfos = [jsonDic objectForKey:@"thumbnail"];
        if(thumbnailInfos){
            if ([thumbnailInfos objectForKey:@"availableThumbnail"]) {
                BOOL availableThumbnail = [((NSNumber*)([thumbnailInfos objectForKey:@"availableThumbnail"])) boolValue];
                
                theFile.canDownloadThumbnail = availableThumbnail;
            }
           
        }

        // Always load the thumbnail from cache if we have it
        [self loadDataFromCacheForThumbnailFile:theFile];
        
        
        if(newFile){
            [_files addObject:theFile];
        }
        return theFile;
    }
}

-(BOOL) loadDataFromCacheForFile:(File *) file {
    NSData *cachedData = [_fileSharingCache objectForKey:[file.url absoluteString]];
    if(cachedData){
        if(cachedData.length == file.size){
            file.data = cachedData;
            NSLog(@"Found cached data for file %@", file);
        } else {
            NSLog(@"Data found in cache don't have the excepted size");
        }
    }
    return file.data!=nil;
}

-(BOOL) loadDataFromCacheForThumbnailFile:(File *) file {
    NSData *cachedData = [_thumbmailFileSharingCache objectForKey:file.fileName];
    if(cachedData){
        if(cachedData.length == file.size){
            file.thumbnailData = cachedData;
            NSLog(@"Found cached data for file %@", file);
        } else {
            NSLog(@"Data found in cache don't have the excepted size");
        }
    }
    return file.thumbnailData!=nil;
}


-(void) fetchDataForFile:(File *) file sizeLimit:(NSInteger) sizeLimit completionHandler:(FileSharingComplionHandler) completionHandler {
    if(!file){
        return;
    }
   
    if(file && !file.data){
         //Check in the cache first
        BOOL foundInCache = [self loadDataFromCacheForThumbnailFile:file];
        if(!foundInCache){
            [self populateDataForFile:file sizeLimit:sizeLimit completionHandler:^(File *file, NSError *error) {
                if(completionHandler)
                    completionHandler(file, error);
            }];
        }
       
    
    } else {
        [self populateDataForFile:file sizeLimit:sizeLimit completionHandler:completionHandler];
    }
}

-(void) populateDataForFile:(File *) file sizeLimit:(NSInteger) sizeLimit completionHandler:(FileSharingComplionHandler) completionHandler {
    // Populate the data for this message
        NSLog(@"Fetching data from server for file %@", file);
    if(file.hasThumbnailOnServer){
        [self downloadThumbnailDataForFile:file withCompletionHandler:^(File *file, NSError *error) {
            if(completionHandler)
                completionHandler(file, error);
        }];
     }
}

-(void) downloadThumbnailForFile:(File*)file withCompletionHandler:(FileSharingServiceDownloadFileCompletionHandler) completionHandler progressBlock:(FileSharingServiceDownloadFileProgressionHandler) progressBlock {
    
    NSString *urlString = [NSString stringWithFormat:@"%@/?noCache=true&thumbnail=true",file.url];
    [_downloadManager getRequestWithURL:[NSURL URLWithString:urlString] requestHeadersParameter:[_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileServer] completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *urlResponse) {
      
        if(completionHandler)
            completionHandler(receivedData, error);
    }];
}

-(void) downloadFile:(File*)file withMaxChunkSize :(NSUInteger)maxChunkSizeFromAPI withCompletionHandler:(FileSharingServiceDownloadFileCompletionHandler) completionHandler progressBlock:(FileSharingServiceDownloadFileProgressionHandler) progressBlock {
    float progressValue = 0.01; // initial value
    if (file.tag != nil) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kFileSharingServiceDidUpdateDownloadedBytes object:@{@"value":[NSNumber numberWithFloat:progressValue],@"tag":file.tag,@"fileURL":file.url}];
    }
    NSInteger maxChunkSize = kOneMegaByte;
    if (file.size > maxChunkSize) {//file.size > maxChunkSize
        if (file.size > 100 * maxChunkSize) {
            maxChunkSize = (file.size/100) + kOneKiloByte; // make sure the cunk size is .01 from file size
            // check if maxchunksize > from max from API
            if (maxChunkSize > maxChunkSizeFromAPI) {
                maxChunkSize = maxChunkSizeFromAPI;// if chunk size is more than maxChunkSize that allow from API
            }
        }
        NSUInteger  numberOfChunks = ceilf((float)file.size/maxChunkSize);
        NSMutableDictionary * dataDictionary = [NSMutableDictionary dictionary];
        __block int chunksCounter = 0;
        
        for (int i = 0 ; i < numberOfChunks; i++) {
            NSMutableDictionary *headers =[NSMutableDictionary dictionaryWithDictionary:[_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileServer]];
            if (i == numberOfChunks -1) {
                [headers setObject:[NSString stringWithFormat:@"bytes=%lu-%lu",i*maxChunkSize,file.size-1] forKey:@"range"];
            }
            else{
                [headers setObject:[NSString stringWithFormat:@"bytes=%lu-%lu",i*maxChunkSize,(i+1)*maxChunkSize-1] forKey:@"range"];
            }

            NSString *urlString = [NSString stringWithFormat:@"%@",file.url];///?noCache=true&thumbnail=true
            [_downloadManager getRequestWithURL:[NSURL URLWithString:urlString] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *urlResponse) {
                // TODO: handle returned errors from server, do a retry
                
                if(completionHandler){
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) urlResponse;
                    NSString * contentRange = [[httpResponse allHeaderFields] objectForKey:@"Content-Range"];
                    
                    NSLog(@"downloaded chunk with range: %@", contentRange);
                    
                    chunksCounter++;
                    // notify
                    float progressValue = ((float)chunksCounter/numberOfChunks);
                    if (file.tag != nil) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:kFileSharingServiceDidUpdateDownloadedBytes object:@{@"value":[NSNumber numberWithFloat:progressValue],@"tag":file.tag,@"fileURL":file.url}];
                    }
                    
                    //
                    //TODO: check if error is nil
                    if (error == nil) {
                        [dataDictionary setObject:receivedData forKey:[self parseStartRangeFromRangeString:contentRange]];
                    }
                    else{
                        completionHandler(nil, error);
                    }
                    
                    // TODO : handle errors
                    if (chunksCounter == numberOfChunks) {
                        if (dataDictionary.count == numberOfChunks) {
                            NSMutableData *allDataRecived = [NSMutableData data];
                            NSArray * sortedKeys = [self sortDictionaryKeys:dataDictionary];
                            NSArray * objects = [dataDictionary objectsForKeys: sortedKeys notFoundMarker: [NSNull null]];
                          
                            for (int i =0; i<objects.count; i++) {
                                [allDataRecived appendData:objects[i]];
                            }
                            NSLog(@"Success downloading the File ..");
                            completionHandler(allDataRecived, error);
                        }
                        else{
                            NSLog(@"Error in downloading the File ..");
                            completionHandler(nil, error);
                            
                        }
                    }
                }
            }];
        }
    }
    else{
        // Download file not in chunks , the file size is less than maxChunkSize
        
        NSString *urlString = [NSString stringWithFormat:@"%@",file.url];
        [_downloadManager getRequestWithURL:[NSURL URLWithString:urlString] requestHeadersParameter:[_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileServer] completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *urlResponse) {
           
            if(completionHandler)
                completionHandler(receivedData, error);
        }];
        
    }
    
}

-(File *) getOrCreateFileWithRainbowID:(NSString *) rainbowID withInfos:(NSDictionary *) infos {
    File *theFile = nil;
    @synchronized (_files) {
        theFile = [self getFileByRainbowID:rainbowID];
        if(!theFile){
            theFile = [self createFileWithFileName:infos[@"fileName"] size:[infos[@"size"] integerValue] mimeType:infos[@"mimeType"] data:infos[@"data"] url:nil];
            theFile.rainbowID = rainbowID;
            theFile.url = infos[@"url"];
            
            [_files addObject:theFile];
        }
        
        else if (!theFile.url || !theFile.type || !theFile.fileName){
            theFile.url = infos[@"url"];
            theFile.fileName = infos[@"fileName"];
            theFile.mimeType = infos[@"mimeType"];
            NSLog(@"update the file %@",theFile.fileName);
        }
        
        if ([_myUser.contact.rainbowID isEqualToString:infos[@"ownerId"]] && !theFile.owner) {
            theFile.owner = _myUser.contact;
            
        }
        
        if (infos[@"isDownloadAvailable"]) {
            BOOL isDownloadAvailable = [infos[@"isDownloadAvailable"] boolValue];
            if (!isDownloadAvailable) {
                theFile.isDownloadAvailable = NO;
                theFile.canDownloadFile = NO;
            }
        }
        else{ // get file info from server only if isDownloadAvailable to check if the file is not deleted
            [self fetchFileInformation:theFile completionHandler:^(File *file, NSError *error) {
                if (error.code == 404 || error.code == 403) { // in this case the file is deleted from the server
                    theFile.isDownloadAvailable = NO;
                    theFile.canDownloadFile = NO;
                    [[NSNotificationCenter defaultCenter] postNotificationName:kFileSharingServiceDidRemoveFile object:theFile];
                }
            }];
            
        }
    }
        return theFile;
    }
-(File *) createFileWithFileName:(NSString *) fileName size:(NSInteger) size mimeType:(NSString *) mimeType data:(NSData *) data url:(NSURL *) url{
    File *theFile = [File new];
    theFile.fileName = fileName;
    theFile.size = size;
    theFile.mimeType = mimeType;
    theFile.data = data;
    theFile.url = url;
    return theFile;
}

-(BOOL) uploadDataForFile:(File *)file withMaxChunkSize:(NSUInteger)maxChunkSize error:(NSError **)returnedError {
    NSMutableDictionary *headers = (NSMutableDictionary *)[_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileServer];
    NSUInteger  numberOfChunks = 0;
    NSUInteger offset = 0;
    NSUInteger remainChunk = file.size;
    BOOL uploadHasFailed = NO;
    
//    NSLog(@"start uploade attachment:url=%@ mimeType=%@",[file.url absoluteString], mimeType);
    
    if (file.size > maxChunkSize) {
        numberOfChunks = ceilf((float)file.size/maxChunkSize);
        for (int i = 0; i < numberOfChunks; i++) {
            NSData *responseData = nil;
            NSURLResponse *returnedResponse = nil;
            NSError *putError = nil;
            NSData *chunkData;
            
            NSURL * fileURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/parts/%i",file.url,i]];
            
            if (i == numberOfChunks-1){
                // use remain chunk in the last chunk uploaded ..
                chunkData = [file.data subdataWithRange:(NSMakeRange(offset, remainChunk))];
            }
            else{
                chunkData = [file.data subdataWithRange:(NSMakeRange(offset, maxChunkSize))];
                
            }
            NSLog(@"Uploading file chunk%i", i);
            BOOL success = [_downloadManager putSynchronousRequestWithURL:fileURL  data:chunkData requestHeadersParameter:headers returningResponseData:&responseData returningNSURLResponse:&returnedResponse returningError:&putError progressionHandler:^(double totalBytesSent, double totalBytesExpectedToSend) {
                
                float progressValue = (float)((totalBytesSent/totalBytesExpectedToSend)/numberOfChunks) + ((float)i/numberOfChunks);
                [[NSNotificationCenter defaultCenter] postNotificationName:kFileSharingServiceDidUpdateUploadedBytesSent object:@{@"value":[NSNumber numberWithFloat:progressValue],@"tag":file.tag,@"fileURL":file.url}];
        
            }];
            
            if(success && !putError){
                offset += maxChunkSize;
                remainChunk -= maxChunkSize;
                NSLog(@"success upload chunk%i", i);
            }
            else {
                // if failed in one chunk stop the upload
                // TODO : handle re-send the failed chunk
                NSLog(@"failed upload chunk%i", i);
                *returnedError = putError;
                uploadHasFailed = YES;
                break;
            }
        }
    }
    if (!uploadHasFailed) {
        NSData *responseData = nil;
        NSURLResponse *returnedResponse = nil;
        NSError *putError = nil;
        NSURL * fileURL;
        BOOL success;
        if (file.size < maxChunkSize) {
            // if file size is less than maxChunkSizeUpload send it not in chunks
            NSLog(@"start uploading the file");
            fileURL = file.url;
            success = [_downloadManager putSynchronousRequestWithURL:fileURL  data:file.data requestHeadersParameter:headers returningResponseData:&responseData returningNSURLResponse:&returnedResponse returningError:&putError progressionHandler:^(double totalBytesSent, double totalBytesExpectedToSend) {
                float progressValue = (totalBytesSent/totalBytesExpectedToSend);
                [[NSNotificationCenter defaultCenter] postNotificationName:kFileSharingServiceDidUpdateUploadedBytesSent object:@{@"value":[NSNumber numberWithFloat:progressValue],@"tag":file.tag,@"fileURL":file.url}];
            }];
        }
        else{
            // if file size is greater than maxChunkSizeUpload we sent it chunks and now send the end
            NSLog(@"send end PUT request for chunks");
            fileURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/parts/end",file.url]];
            success = [_downloadManager putSynchronousRequestWithURL:fileURL  data:nil requestHeadersParameter:headers returningResponseData:&responseData returningNSURLResponse:&returnedResponse returningError:&putError progressionHandler:^(double totalBytesSent, double totalBytesExpectedToSend) {
              
            }];
        }
        
        if(success && !putError){
            NSDictionary *putResponse = [responseData objectFromJSONData];
            NSDictionary *putData = [putResponse objectForKey:@"data"];
            if(putData){
                // TODO: save thumbnail in cache and show it instead of image
                [_fileSharingCache setObjectAsync:file.data forKey:[file.url absoluteString] completion:nil];
                _currentSize += file.size;
                
            }
            else {
                uploadHasFailed = YES;
                NSInteger errorCode = [[putResponse objectForKey:@"errorCode"] integerValue];
                if(errorCode){
                    NSLog(@"Send message with attachment PUT errorCode: %ld %@ : %@", (long)errorCode, [putResponse objectForKey:@"errorMsg"], [putResponse objectForKey:@"errorDetails"]);
                    *returnedError = [NSError errorWithDomain:@"FileSharing" code:errorCode userInfo:@{NSLocalizedDescriptionKey:@"File cannot be transferred because you have not enough storage space."}];
                } else {
                    NSLog(@"Send message with attachment PUT undefined error");
                }
            }
            
        }
        else {
            NSLog(@"Send message with attachment failed in putSynchronousRequestWithURL with error: %@",putError);
            *returnedError = putError;
           uploadHasFailed = YES;
        }
    }
   
    return !uploadHasFailed;
}

-(void) addViewer:(Peer *) viewer forFile:(File *) file withCompletionHandler:(FileSharingAddViewerComplionHandler) completionHandler {
    // If this file have a rainbowID that means we already upload it, so we have to check if the conversation peer is in viewer list, and resend the xmpp message. And that all (we don't re-upload the message to server)
    
    if(![file.viewers containsObject:viewer]){
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/viewers",[_apiURLManager getURLForService:ApiServicesFileStorage], file.rainbowID]];
        NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileStorage];
        
        NSDictionary *body = @{@"viewerId":viewer.rainbowID, @"type":[viewer isKindOfClass:[Contact class]]?@"user":@"room"};
        
        [_downloadManager postRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
            if(!error){
                NSDictionary *response = [receivedData objectFromJSONData];
                if(response) {
                    if([response objectForKey:@"data"]){
                        // We have a data that means the server is ok with our request.
                        // So we can add the new viewer in the file object
                        [file addPeerInViewers:viewer];
                    }
                }
            }
            
            if(completionHandler){
                completionHandler(file, error);
            }
        }];
    } else {
        if(completionHandler)
            completionHandler(file, nil);
    }
    
}

-(void) uploadFile:(File *) file forConversation:(Conversation *) conversation completionHandler:(FileSharingComplionHandler) completionHandler {
    NSURL *serviceUrl = [_apiURLManager getURLForService:ApiServicesFileStorage];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileStorage];
    
    NSData *data = file.data;
    NSString *extension = [data extension];
    
    if( [extension isEqualToString:@""] ) {
        extension = [file.fileName pathExtension];
    }
    if (extension == nil)
        extension = @"";
    
    NSMutableDictionary *bodyContent = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                       @"fileName":file.fileName,
                                                                                       @"extension":extension,
                                                                                       @"size":[NSString stringWithFormat:@"%ld", (unsigned long)[data length]]}];
    if(conversation){
        
        NSDictionary *viewersContent = @{@"viewerId":conversation.peer.rainbowID, @"type":conversation.type == ConversationTypeRoom?@"room":@"user"};
        [bodyContent setObject:@[viewersContent] forKey:@"viewers"];
    }
    
    NSString *body = [bodyContent jsonStringWithPrettyPrint:NO];
    [_downloadManager postRequestWithURL:serviceUrl body:body requestHeadersParameter:headers completionBlock: ^(NSData *receivedData, NSError *error, NSURLResponse *urlResponse) {
        if(!error){
            NSDictionary *response = [receivedData objectFromJSONData];
            if(response) {
                if([response objectForKey:@"errorCode"]){
                    NSLog(@"Send message with attachment failed in postRequestWithURL with HTML error '%@'", [response objectForKey:@"errorCode"]);
                    // TODO: Use the errorCode from server
                    NSError *anError = [NSError errorWithDomain:@"FileSharing" code:500 userInfo:@{NSLocalizedDescriptionKey: response}];
                    if(completionHandler)
                        completionHandler(file, anError);
                    return;
                }
                NSDictionary *responseData = [response objectForKey:@"data"];
                if(responseData){
                    // get the maxChunkSizeUpload
                    [self getMaxChunkSizeUploadandDownloadWithCompletionHandler:^(NSUInteger maxChunkSizeDownload,NSUInteger maxChunkSizeUpload, NSError *error) {
                        NSString *fileID = [responseData objectForKey:@"id"];
                        if(fileID){
                            file.rainbowID = fileID;
                            // Save this file in our local array
                            @synchronized (_files) {
                                if(![_files containsObject:file])
                                    [_files addObject:file];
                            }
                            // Now update the message with all the informations from server
                            [self createOrUpdateFileWithJsonDic:responseData];
                            NSError *returnedError = nil;
                            NSURL *serviceUrl = [_apiURLManager getURLForService:ApiServicesFileServer];
                            NSURL *OBDataUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", serviceUrl, fileID]];
                            file.uploadDate = [NSDate date];
                            file.url = OBDataUrl;
                            BOOL sucess = [self uploadDataForFile:file withMaxChunkSize:maxChunkSizeUpload error:&returnedError];
                            NSLog(@"Send message with attachment uploaded: status %@ url=%@ mimeType=%@",NSStringFromBOOL(sucess), [file.url absoluteString], file.mimeType);
                            if(completionHandler)
                                completionHandler(file, returnedError);
                        }
                    }];
                }
            }
        } else {
            NSLog(@"Send message with attachment failed in postRequestWithURL with error: %@", [error description]);
            NSError *customError = error;
            NSDictionary *response = [receivedData objectFromJSONData];
            NSNumber *detailErrorCode = [response objectForKey:@"errorDetailsCode"];
            if([[detailErrorCode stringValue] isEqualToString:@"403620"]){
                customError = [NSError errorWithDomain:@"FileSharing" code:error.code userInfo:@{NSLocalizedDescriptionKey:@"File cannot be transferred because you have not enough storage space."}];
            }
            if(completionHandler)
                completionHandler(file, customError);
        }
    }];
}

- (void) getMaxChunkSizeUploadandDownloadWithCompletionHandler:(FileSharingMaxDataSizeComplionHandler) completionHandler  {
   NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileCapabilities];
    NSURL *serviceUrl = [_apiURLManager getURLForService:ApiServicesFileCapabilities];
    [_downloadManager getRequestWithURL:serviceUrl requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get file max file chunk : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if (completionHandler) {
                completionHandler(0,0,error);
            }
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get file  max file chunk due to JSON parsing failure");
            if (completionHandler) {
                completionHandler(0,0,error);
            }
        }
        
       NSDictionary *data = [jsonResponse objectForKey:@"data"];
       NSLog(@"Get max chunk size , response ...");
        NSUInteger maxSizeDownload = [((NSString *)([data[@"maxChunkSizeDownload"] notNull])) integerValue];
        NSUInteger maxSizeUpload = [((NSString *)([data[@"maxChunkSizeUpload"] notNull])) integerValue];
        
        if (completionHandler) {
            completionHandler(maxSizeDownload,maxSizeUpload,error);
        }
    }];
}

-(void) transferFile:(File *)file  withCompletionHandler:(FileSharingComplionHandler) completionHandler {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/copy",[_apiURLManager getURLForService:ApiServicesFileStorage], file.rainbowID]];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileStorage];
    
    [_downloadManager postRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(!error){
            NSDictionary *response = [receivedData objectFromJSONData];
            if(response) {
                if([response objectForKey:@"data"]){
                    File * aFile = [self createOrUpdateFileWithJsonDic:[response objectForKey:@"data"]];
                    if(completionHandler){
                        completionHandler(aFile, nil);
                    }
                }
            }
        }
        else{
            if(completionHandler){
                completionHandler(nil, error);
            }
        }
    }];

}

#pragma mark - general methods
- (NSNumber *)parseStartRangeFromRangeString:(NSString *)rangeString {
    rangeString = [rangeString stringByReplacingOccurrencesOfString:@"bytes " withString:@""];
    NSArray *ranges = [rangeString componentsSeparatedByString:@"-"];
    if (ranges.count) {
        return @([ranges[0] longLongValue]);
    }
    return @(0);
}

- (NSArray *) sortDictionaryKeys :(NSDictionary *) dic {
    NSArray * allKeys = [dic allKeys];
    NSMutableArray * newKeysArray = [NSMutableArray arrayWithArray:allKeys];
    NSSortDescriptor *ascending = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    [newKeysArray sortUsingDescriptors:[NSArray arrayWithObject:ascending]];
    return [NSArray arrayWithArray:newKeysArray];
}

- (NSString *)getURLFromFileType:(FilterFileType) filetype {
    NSString * fileMime ;
    switch (filetype) {
        case FilterFilesAll:
            fileMime = @"";
            break;
        case FilterTypeImage:
            fileMime = @"&typeMIME=image";
            break;
        case FilterTypeAudioORVideo:
            fileMime = @"&typeMIME=audio&typeMIME=video";
            break;
        case FilterTypePDF:
            fileMime = @"&typeMIME=application/pdf";
            break;
        case FilterTypeMicrosoftOffice:
            fileMime =@"&extension=docx&extension=doc&extension=pptx&extension=ppt&extension=xls&extension=xlsx";
            break;
        default:
            fileMime = @"";
            break;
    }
    return fileMime;
}

#pragma mark - Public methods
-(File *) createTemporaryFileWithFileName:(NSString *) fileName andData:(NSData *) data andURL:(NSURL *)url {
    File *theFile = [File new];
    theFile.fileName = [Tools encodeToPercentEscapeString:fileName];
    theFile.data = data;
    theFile.mimeType = [data mimeTypeByGuessing];
    theFile.url = url;
    if (theFile.url) {
        theFile.thumbnailData = [NSData dataWithContentsOfURL:theFile.url];
        [_thumbmailFileSharingCache setObject:theFile.thumbnailData forKey:theFile.fileName];
    }
    theFile.owner = _myUser.contact;
    theFile.tag = [Tools generateUniqueID];
    return theFile;
}

-(void) deleteFile:(File *) file {
    [self deleteFile:file withCompletionHandler:nil];
}

-(void) deleteFile:(File *) file withCompletionHandler :(FileSharingComplionHandler) completionHandler{
    if(!file.rainbowID){
        NSLog(@"Could not remove file without rainbowID");
        return;
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",[_apiURLManager getURLForService:ApiServicesFileStorage], file.rainbowID]];
    NSDictionary *header = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileStorage];
    
    NSData *receivedData = nil;
    NSError *requestError = nil;
    NSURLResponse *response = nil;
    BOOL requestSucceed = [_downloadManager deleteSynchronousRequestWithURL:url requestHeadersParameter:header returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError shouldSaveRequest:NO];
    
    if(!requestSucceed || requestError) {
        NSLog(@"Error: Cannot delete file in REST : %@ %@", requestError, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        completionHandler(nil,requestError);
        return;
    }
    NSDictionary *jsonResponse = [receivedData objectFromJSONData];
    if(!jsonResponse) {
        NSLog(@"Error: Cannot read REST answer due to JSON parsing failure");
        completionHandler(nil,requestError);
        return;
    }
    
    NSLog(@"Did remove file");
    // If there is a data that means the delete works.
    if(jsonResponse[@"status"]){
        NSLog(@"Delete file answer %@", jsonResponse);
        @synchronized (_files) {
            [_files removeObject:file];
            [_fileSharingCache removeObjectForKey:file.rainbowID];
            _currentSize -= file.size;
            completionHandler(file,requestError);

        }
    }
    else{
         completionHandler(nil,requestError);
    }
}

-(void) downloadThumbnailDataForFile:(File *) file withCompletionHandler:(FileSharingComplionHandler) completionHandler {
    // this code to remove value stored in *_thumbmailFileSharingCache* for key file.url
    if ([_thumbmailFileSharingCache objectForKey:[file.url absoluteString]]) {
        [_thumbmailFileSharingCache removeObjectForKey:[file.url absoluteString]];
    }
    BOOL foundCachedData = [self loadDataFromCacheForThumbnailFile:file];
    if(foundCachedData){
        if(completionHandler)
            completionHandler(file, nil);
    } else {
        
        [self downloadThumbnailForFile:file  withCompletionHandler:^(NSData *receivedData, NSError *error) {
            if(!error){
                file.thumbnailData = receivedData;
                NSLog(@"Data fetch for file %@", file);
                [_thumbmailFileSharingCache setObjectAsync:receivedData forKey:file.fileName completion:nil];
            } else {
                NSLog(@"Could not download attachment, error %@, response from server %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            }
            
            if(completionHandler)
                completionHandler(file, error);
        } progressBlock:^(File *file, double totalBytesSent, double totalBytesExpectedToSend) {
            
        }];

    }
}

-(void) downloadDataForFile:(File *) file withCompletionHandler:(FileSharingComplionHandler) completionHandler {
    BOOL foundCachedData = [self loadDataFromCacheForFile:file];
    if(foundCachedData){
        if(completionHandler)
            completionHandler(file, nil);
    } else {
        [self getMaxChunkSizeUploadandDownloadWithCompletionHandler:^(NSUInteger maxChunkSizeDownload,NSUInteger maxChunkSizeUpload, NSError *error) {
            if (!error) {
                [self downloadFile:file withMaxChunkSize:maxChunkSizeDownload withCompletionHandler:^(NSData *receivedData, NSError *error) {

                    if(!error){
                        file.data = receivedData;
                        NSLog(@"Data fetch for file %@", file);
                        file.canDownloadFile = YES;
                        [_fileSharingCache setObjectAsync:receivedData forKey:[file.url absoluteString] completion:nil];
                       
                    } else {
                        NSLog(@"Could not download attachment, error %@, response from server %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
                        file.canDownloadFile = NO;
                    }

                    if(completionHandler)
                        completionHandler(file, error);
                } progressBlock:^(File *file, double totalBytesSent, double totalBytesExpectedToSend) {

                }];
            }
            else{
                file.canDownloadFile = NO;
                if(completionHandler)
                    completionHandler(file, error);
            }

        }];

    }

}

-(void) loadSharedFilesWithPeer:(Peer *) peer fromOffset :(NSUInteger)offset completionHandler:(FileSharingDataLoadSharedFilesWithPeerComplionHandler)completionHandler {
    if(!peer.rainbowID){
        NSLog(@"Cannot load shared files of contact without rainbowID");
        return;
    }
    NSURL *url = nil;
    if([peer isKindOfClass:[Room class]]) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/viewers/%@",[_apiURLManager getURLForService:ApiServicesFileStorage], peer.rainbowID]];
    } else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/viewers/%@?ownerId=%@",[_apiURLManager getURLForService:ApiServicesFileStorage], _myUser.contact.rainbowID, peer.rainbowID]];
    }
    
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileStorage];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get list of shared files : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if(completionHandler)
                completionHandler(nil, error);
            return;
        }
    
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get list of shared files due to JSON parsing failure");
            if(completionHandler)
                completionHandler(nil, error);
            return;
        }

        NSArray *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get list of shared files, response ...");
        NSMutableArray *listOfFilesRetreived = [NSMutableArray array];
        for (NSDictionary *fileDic in data) {
            File *theFile = [self createOrUpdateFileWithJsonDic:fileDic];
            [listOfFilesRetreived addObject:theFile];
        }

        if (completionHandler)
            completionHandler(listOfFilesRetreived, nil);
    }];
}

-(void) removeViewer:(Peer *) viewer fromFile:(File *) file completionHandler:(FileSharingComplionHandler) completionHandler {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/viewers/%@",[_apiURLManager getURLForService:ApiServicesFileStorage], file.rainbowID, viewer.rainbowID]];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileStorage];

    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(!error){
            NSDictionary *response = [receivedData objectFromJSONData];
            if(response) {
                if([response objectForKey:@"data"]){
                    // We have a data that means the server is ok with our request.
                    // So we can add the new viewer in the file object
                    [file removePeerFromViewers:viewer];
                }
            }
        }
        
        if(completionHandler){
            completionHandler(file, error);
        }
    }];
}


-(void) fetchFileInformation:(File *) file completionHandler:(FileSharingComplionHandler) completionHandler {
    if (!file.rainbowID) {
        NSLog(@"Error: unable to get file details from server, file has no rainbowID..");
        return;
    }
    
    NSURL *url_base = [_apiURLManager getURLForService:ApiServicesFileStorage];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", url_base, file.rainbowID]];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesFileStorage];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get file %@ details in REST : %@ %@", file.rainbowID, error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            completionHandler(nil, error);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
           if(!jsonResponse) {
            NSLog(@"Error: Cannot get file %@ details in REST due to JSON parsing failure", file.rainbowID);
            return;
        }
        
        NSDictionary *fileDict = [jsonResponse objectForKey:@"data"];
        NSLog(@"Fetch file details");
        File * aFile = [self createOrUpdateFileWithJsonDic:fileDict];
        if(completionHandler){
            [[NSNotificationCenter defaultCenter] postNotificationName:kFileSharingServiceDidUpdateFile object:aFile];
            completionHandler(aFile, error);
        }

    }];
    
}

@end
