/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "TelephonyService+Internal.h"
#import "ServicesManager.h"
#import "ServicesManager+Internal.h"
#import "RTCService+Internal.h"
#import "DownloadManager.h"
#import "PhoneNumberInternal.h"
#import "NomadicStatus+Internal.h"
#import "MediaPillarStatus+Internal.h"
#import "MyUser+Internal.h"
#import "NSData+JSON.h"
#import "NSDictionary+JSONString.h"
#import "CallForwardStatus+Internal.h"
#import "Call+Internal.h"
#import "Peer+Internal.h"
#import "ContactsManagerService+Internal.h"
#import "PhoneState.h"
#import "PhoneStateIdle.h"
#import "PhoneStateOneActiveCall.h"
#import "PhoneStateGsmConnected.h"
#import "PhoneStateEvtRingingIncoming.h"
#import "PhoneStateInitMakeCall.h"
#import "NSString+URLEncode.h"
#import "XMPPServiceProtocol.h"
#import "CallEvent.h"
#import "NSObject+NotNull.h"
#import "CallEvent+Internal.h"
#import "CallParticipant+Internal.h"

NSString *const kTelephonyServiceDidAddCallNotification = @"TelephonyServiceDidAddCallNotification";
NSString *const kTelephonyServiceDidUpdateCallNotification = @"TelephonyServiceDidUpdateCallNotification";
NSString *const kTelephonyServiceDidRemoveCallNotification = @"TelephonyServiceDidRemoveCallNotification";
NSString *const kTelephonyServiceDidFailedToCallNotification = @"TelephonyServiceDidFailedToCallNotification";


@interface TelephonyService () <XMPPServiceTelephonyDelegate, CXCallObserverDelegate>

@property (nonatomic, strong) MyUser *myUser;
@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) ContactsManagerService *contactsManagerService;
@property (nonatomic, strong) RTCService *rtcService;

#if TARGET_OS_IPHONE
@property (nonatomic, strong) CXCallObserver *callObserver;
#endif
@property (nonatomic, strong) PhoneState* currentPhoneState;
@property (nonatomic, strong) NSObject *currentPhoneStateMutex;

@property (nonatomic, strong) NSMutableArray *calls;
@property (nonatomic, strong) NSObject *callsMutex;
@property (nonatomic, strong) NSString* currentOutgoingCallRef;
@property (nonatomic, strong) NSString* rtcSessionId;
@property (nonatomic, strong) NSString* rtcRessource;
@property (nonatomic, strong) Peer* rtcPeer;

@property (nonatomic, copy, readwrite) TelephonyServiceMakeCallCompletionHandler makeCallCompletionHandler;
@property (nonatomic, strong) NSTimer *makeCallTimer;

@property (nonatomic) BOOL needToAddCall;

@property (nonatomic) BOOL isCallKitAvailable;

@end


@implementation TelephonyService

-(instancetype) initWithXMPPService:(XMPPService *) xmppService myUser:(MyUser*) myUser downloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService rtcService:(RTCService * _Nonnull) rtcService {
    self = [super init];
    if (self) {
        _xmppService = xmppService;
        _myUser = myUser;
        _xmppService.telephonyDelegate = self;
        _downloadManager = downloadManager;
        _apiUrlManagerService = apiUrlManagerService;
        _contactsManagerService = contactsManagerService;
        _rtcService = rtcService;
        _needToAddCall = NO;
        _isCallKitAvailable = _rtcService.isCallKitAvailable;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didMyUserFeatureDidUpdate:) name:kMyUserFeatureDidUpdate object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveIncomingMpCall:) name:kRTCServiceDidReceiveIncomingMpCall object:nil];
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveHangupCall:) name:kRTCServiceDidReceiveHangupCall object:nil];
        
        _currentPhoneStateMutex = [NSObject new];
        
        @synchronized(_currentPhoneStateMutex) {
            _currentPhoneState = [PhoneStateIdle new];
            _currentPhoneState.telService = self;
            _currentPhoneState.rtcService = _rtcService;
        }
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        
        _callObserver = [CXCallObserver new];
        [_callObserver setDelegate:self queue:dispatch_get_main_queue()];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
        
        _calls = [NSMutableArray array];
        _callsMutex = [NSObject new];
    }
    return self;
}


-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserFeatureDidUpdate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidReceiveIncomingMpCall object:nil];

    _downloadManager = nil;
    _apiUrlManagerService = nil;
    _myUser = nil;
    _xmppService.telephonyDelegate = nil;
    _xmppService = nil;
    _contactsManagerService = nil;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    _callObserver = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
    _calls = nil;
    _callsMutex = nil;
    
    _currentPhoneState.telService = nil;
    _currentPhoneState = nil;
    _currentPhoneStateMutex = nil;
}


/**
 *  On login event, we fetch nomadic status if required.
 */
-(void) didMyUserFeatureDidUpdate:(NSNotification *) notification {
    [self fetchNomadicStatus];
    [self fetchForwardStatus];
    [self fetchMediaPillarStatus];
}


#pragma mark - mediapillar routing

-(void) fetchMediaPillarStatus {
    if (![_myUser isAllowedToUseTelephonyWebRTCtoPSTN]) {
        OTCLog (@"TelephonyWebRTCGateway (MediaPillar) not allowed.");
        return;
    }
    
    [_rtcService fetchMediaPillarDataWithCompletionHandler:^(NSString *mediaPillarJid, NSString *rainbowPhoneNumber, NSString *prefix, NSError *error) {
        MediaPillarStatus *newStatus = _myUser.mediaPillarStatus;
        newStatus.jid = mediaPillarJid;
        newStatus.rainbowPhoneNumber = rainbowPhoneNumber;
        newStatus.prefix = prefix;
        newStatus.featureActivated = [_myUser isAllowedToUseTelephonyWebRTCtoPSTN];
        
        if (!error) {
            OTCLog (@"fetchMediaPillarStatus - Register to webRTCGateway");
            [_rtcService webRTCGatewayRegisterRequest:mediaPillarJid number:rainbowPhoneNumber displayName:_myUser.contact.fullName secret:rainbowPhoneNumber];
        }
    }];
}


-(NSString *) getMediaPillarNumberWithPrefix {
    NSString *mediaPillarNumberWithPrefix = @"";
    
    if (_myUser.mediaPillarStatus)
        mediaPillarNumberWithPrefix = [_myUser.mediaPillarStatus.prefix stringByAppendingString:_myUser.mediaPillarStatus.rainbowPhoneNumber];
    
    return mediaPillarNumberWithPrefix;
}

#pragma mark - nomadic routing

-(void) fetchNomadicStatus {
    if (![_myUser isAllowedToUseTelephonyNomadicMode]) {
        OTCLog  (@"TelephonyNomadicMode not allowed.");
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"nomadic"];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    OTCLog (@"Get telephony nomadic status");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot get my user telephony nomadic status in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot get my user telephony nomadic status in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Updating nomadic status");
        NSDictionary *status = [jsonResponse objectForKey:@"data"];
        NSString *destinationString = [status objectForKey:@"destination"];
        NomadicStatus *newStatus = _myUser.nomadicStatus;
        
        if (destinationString && [destinationString length] > 0) {
            if ([destinationString isEqualToString:[self getMediaPillarNumberWithPrefix]]) {
                // Nomadic is configured on mediapillar
                // Update the mediapillar status to activated
                _myUser.mediaPillarStatus.activated = YES;
            } else {
                _myUser.mediaPillarStatus.activated = NO;
            }
            
            PhoneNumber *phone = [PhoneNumber new];
            phone.numberE164 = destinationString;
            newStatus.destination = phone;
        }
        
        newStatus.activated = ([[status objectForKey:@"modeActivated"] isEqualToString:@"true"]) ? YES : NO;
        newStatus.featureActivated = ([[status objectForKey:@"featureActivated"] isEqualToString:@"true"]) ? YES : NO;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserNomadicStatusDidUpdate object:newStatus];
    }];
}


-(void) nomadicLoginWithPhoneString:(NSString * _Nonnull)phoneNumber withVoIP:(BOOL)voip withCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    [self changeNomadicModeActived:YES withPhoneNumber:phoneNumber withVoIP:voip withCompletionBlock:completionBlock];
}


-(void) nomadicLoginWithPhoneNumber:(PhoneNumber * _Nonnull)phoneNumber withVoIP:(BOOL)voip withCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    [self changeNomadicModeActived:YES withPhoneNumber:phoneNumber.number withVoIP:voip withCompletionBlock:completionBlock];
}


-(void) nomadicLogoutWithCompletionBlock:(void (^ _Nullable)(NSError * _Nonnull error))completionBlock {
    [self changeNomadicModeActived:NO withPhoneNumber:nil withVoIP:NO withCompletionBlock:completionBlock];
}


-(void) changeNomadicModeActived:(BOOL)activate withPhoneNumber:(NSString*) phoneNumber withVoIP:(BOOL)voip withCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    NSURL *url;
    NSDictionary *body = nil;
    NSString *thePhoneNumber = phoneNumber;
    if (activate) {
        if (voip) {
            thePhoneNumber = [self getMediaPillarNumberWithPrefix];
        }
        if ([thePhoneNumber length] > 0) {
            url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"nomadic/login"];
            body = @{@"destinationExtNumber": thePhoneNumber};
            OTCLog (@"Update nomadic status to login");
        }
        else {
            if (completionBlock)
                completionBlock([NSError errorWithDomain:@"TelephonyService" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Nomadic mode login require a phone number"}]);
            OTCLog (@"Login to telephony nomadic require a phone number");
            return;
        }
    }
    else {
        url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"nomadic/logout"];
        OTCLog (@"Update nomadic status to logout");
    }
    
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot change telephony nomadic status in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if (completionBlock)
                completionBlock(error);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot change telephony nomadic status in REST due to JSON parsing failure");
            if (completionBlock)
                completionBlock(error);
            return;
        }
        
        //NSDictionary *loginStatus = [jsonResponse objectForKey:@"nomadicLogin"];
        NSString *requestStatus = [jsonResponse objectForKey:@"status"];
        NomadicStatus *newStatus = _myUser.nomadicStatus;
        newStatus.featureActivated = YES;
        BOOL updateDone = NO;
        
        //if ([[loginStatus objectForKey:@"login"] isEqualToString:@"false"]) {
        if (activate && [requestStatus isEqualToString:@"Nomadic Login successfully sent"]) {
            PhoneNumber *phone = [PhoneNumber new];
            phone.number = phoneNumber;
            newStatus.destination = phone;
            newStatus.activated = YES;
            updateDone = YES;
            _myUser.mediaPillarStatus.activated = voip;
            
        } else if (!activate && [requestStatus isEqualToString:@"Nomadic Logout successfully sent"]) {
            newStatus.activated = NO;
            updateDone = YES;
            _myUser.mediaPillarStatus.activated = NO;
        }
        
        if (!updateDone) {
            if (completionBlock)
                completionBlock([NSError errorWithDomain:@"TelephonyService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Nomadic mode login/logout failed"}]);
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserNomadicStatusDidUpdate object:newStatus];
            if (completionBlock)
                completionBlock(nil);
        }
    }];
}


-(void) xmppService:(XMPPService *) service didReceiveNomadicStatusUpdate:(NomadicStatus *) status {
    OTCLog (@"Did receive Nomadic status update %@", status);
    if (![status isEqual:_myUser.nomadicStatus]) {
        _myUser.nomadicStatus = status;
        _myUser.mediaPillarStatus.activated = [status.destination.number isEqual:[self getMediaPillarNumberWithPrefix]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserNomadicStatusDidUpdate object:status];
    }
}


#pragma mark - forward routing

-(void) fetchForwardStatus {
    if (![_myUser isAllowedToUseTelephonyCallForward])
        return;
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"forward"];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    OTCLog (@"Get telephony forward status");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot get my user telephony call forward status in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
    }];
    
}


-(void) xmppService:(XMPPService *) service didReceiveCallForwardStatusUpdate:(CallForwardStatus *) status {
    OTCLog (@"Did receive message for call forward %@", status);
    if (![status isEqual:_myUser.callForwardStatus]) {
        _myUser.callForwardStatus = status;
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserCallForwardDidUpdate object:status];
    }
}


-(void) forwardToExternalPhoneNumber:(PhoneNumber * _Nonnull)phoneNumber withCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    [self forwardTo:@{@"calleeExtNumber": phoneNumber.number}  withCompletionBlock:completionBlock];
}


-(void) forwardToExternalPhoneString:(NSString * _Nonnull)phoneNumber withCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    [self forwardTo:@{@"calleeExtNumber": phoneNumber}  withCompletionBlock:completionBlock];
}


-(void) forwardToVoicemailWithCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    if (_myUser.voiceMailNumber == nil) {
        completionBlock( [NSError errorWithDomain:@"TelephonyService" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Unable to update call forward to voicemail, phone number not defined in user profile."}]);
        return;
    }
    
    [self forwardTo:@{@"calleeIntNumber": _myUser.voiceMailNumber } withCompletionBlock:completionBlock];
}


-(void) cancelForwardWithCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    [self forwardTo:@{@"calleeIntNumber": @"CANCELFORWARD"} withCompletionBlock:completionBlock];
}


-(void) forwardTo:(NSDictionary * _Nonnull)calleeDic withCompletionBlock:(void (^ _Nullable)(NSError *_Nullable error))completionBlock {
    NSURL *url;
    NSDictionary *body = nil;
    
    if (calleeDic) {
        url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"forward"];
        body = calleeDic;
        OTCLog (@"Update telephony forward");
    } else {
        if (completionBlock)
            completionBlock([NSError errorWithDomain:@"TelephonyService" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Forward to device require a phone number"}]);
        OTCLog (@"Forward to device require a phone number");
        return;
    }
    
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot change telephony forward in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if (completionBlock)
                completionBlock(error);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot change telephony forward in REST due to JSON parsing failure");
            if (completionBlock)
                completionBlock(error);
            return;
        }
        
        if (completionBlock)
            completionBlock(nil);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserCallForwardDidUpdate object:nil];
    }];
}

#pragma mark - Voicemail count

-(void) xmppService:(XMPPService *) service didReceiveVoicemailCountUpdate:(NSInteger) count {
    OTCLog (@"Did receive Voicemail count update %ld", count);
    if (count != _myUser.voiceMailCount) {
        _myUser.voiceMailCount = count;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserVoicemailCountDidUpdate object:nil];
    }
}

#pragma mark - Calls management

-(void) addCall:(Call* _Nullable) newCall {
    // We don't add the object into the calls array that is done automatically when the call object is created by createOrUpdateCall methods
    Call * theCall = newCall;
    OTCLog (@"Add : %@", theCall);
    if (_makeCallCompletionHandler) {
        _makeCallCompletionHandler(nil);
        _makeCallCompletionHandler = nil;
    }
    
    if (!theCall) {
        @synchronized(_callsMutex) {
            theCall = [_calls firstObject];
        }
    }
    if (newCall.readyToDisplay)
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidAddCallNotification object:theCall];
}


-(void) updateCall:(Call*) updatedCall {
    OTCLog (@"Update : %@", updatedCall);
    if (_makeCallCompletionHandler) {
        _makeCallCompletionHandler(nil);
        _makeCallCompletionHandler = nil;
    }
    if (updatedCall.readyToDisplay)
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:updatedCall];
}


-(void) removeCall:(Call*) oldCall {
    OTCLog (@"Remove : %@", oldCall);
    __block Call *theCall = oldCall;
    if (!theCall) {
        OTCLog (@"No call to remove assume that is the GSM call, the first call");   // Remove all calls ?
        @synchronized(_callsMutex) {
            theCall = [_calls firstObject];
        }
    }
    
    @synchronized(_callsMutex) {
        [_calls removeObject:theCall];
    }
    
    if (_makeCallCompletionHandler)
    {
        NSError* error = nil;
        if ( (theCall.status == CallStatusCanceled || theCall.status == CallStatusHangup) && theCall.callCause && ![theCall.callCause isEqualToString:@"NORMALCLEARING"] )
        {
            error = [NSError errorWithDomain:@"Telephony" code:500 userInfo:@{NSLocalizedFailureReasonErrorKey:theCall.callCause}];
        }
        _makeCallCompletionHandler(error);
        _makeCallCompletionHandler = nil;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:theCall];
}


-(void) getTelephonicState {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"snapshotdevice?deviceType=SECONDARY"];
    NSMutableDictionary *headers = [NSMutableDictionary dictionaryWithDictionary:[_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony]];
    [headers removeObjectForKey:@"Content-Type"];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot get telephonic state in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot get telephonic state in REST due to JSON parsing failure");
            return;
        }
        NSDictionary *data = jsonResponse[@"data"];
        if (data) {
            OTCLog (@"Get telephonic state done ...");
            NSArray *connections = [[data objectForKey:@"connections"] notNull];
            for (NSDictionary *aConnection in connections) {
                OTCLog (@"Get telephonic state has connection : %@", aConnection);
                NSString *callID = [[aConnection objectForKey:@"callId"] notNull];
                NSString *endpointIM = [[aConnection objectForKey:@"endpointIm"] notNull];
                NSString *endpointLci = [[aConnection objectForKey:@"endpointLci"] notNull];
                NSString *endpointTel = [[aConnection objectForKey:@"endpointTel"] notNull];
                NSDictionary *identity = [[aConnection objectForKey:@"identity"] notNull];
                NSString *firstName = [[identity objectForKey:@"firstName"] notNull];
                NSString *lastName = [[identity objectForKey:@"lastName"] notNull];
                NSString *lci = [[aConnection objectForKey:@"lci"] notNull];
                
                CallEvent *aCallEvent = [CallEvent new];
                aCallEvent.callReference = callID;
                NSString *shortCallRef = [[callID componentsSeparatedByString:@"#"] firstObject];
                aCallEvent.shortCallReference = shortCallRef;
                
                if ([[aConnection objectForKey:@"participants"] notNull]) {
                    // TODO: Parse participants, for conferences ??
                } else {
                    CallParticipant *callParticipant = [CallParticipant new];
                    callParticipant.jid = endpointIM;
                    callParticipant.number = endpointTel;
                    callParticipant.firstname = firstName;
                    callParticipant.lastname = lastName;
                    
                    [aCallEvent.callParticipants addObject:callParticipant];
                }
                
                if ([lci isEqualToString:@"LCI_CONNECTED"] && ([endpointLci isEqualToString:@"LCI_CONNECTED"] || [endpointLci isEqualToString:@"LCI_UNKNOWN"])) {
                    aCallEvent.state = CallStateActive;
                } else if ([lci isEqualToString:@"LCI_HELD"] && [endpointLci isEqualToString:@"LCI_CONNECTED"]) {
                    aCallEvent.state = CallStateHeld;
                } else if ([lci isEqualToString:@"LCI_CONNECTED"] && [endpointLci isEqualToString:@"LCI_HELD"]) {
                    aCallEvent.state = CallStateHeld;
                } else if ([lci isEqualToString:@"LCI_CONNECTED"] && [endpointLci isEqualToString:@"LCI_ALERTING"]) {
                    aCallEvent.state = CallStateRingingOutgoing;
                } else if ([lci isEqualToString:@"LCI_QUEUED"] && [endpointLci isEqualToString:@"LCI_CONNECTED"]) {
                    aCallEvent.state = CallStateRingingIncoming;
                } else if ([lci isEqualToString:@"LCI_HELD"]) {
                    aCallEvent.state = CallStateRingingIncoming;
                } else if ([lci isEqualToString:@"LCI_ALERTING"]) {
                    aCallEvent.state = CallStateRingingIncoming;
                } else
                    aCallEvent.state = CallStateUnknown;
                
                [self xmppService:nil didReceiveCallEvent:aCallEvent];
            }
        } else {
            OTCLog (@"Get telephonic state done, no data retrieved");
        }
        
    }];
}


-(void) addRtcCall
{
    _rtcPeer.rtcJid = _rtcPeer.jid;
    Call* firstCall =  [_calls firstObject];
    firstCall.isMediaPillarCall = YES;
    NSString* dispName = firstCall.peer.displayName;
    if (dispName.length)
        _rtcPeer.displayName = dispName;
    else
        OTCLog (@"addRtcCall no displayName ? From call '%@'", firstCall);
//    NSString* mpMedia = firstCall.isIncoming ? @"mediaPillar" : @"mediaPillarMakeCall";
    NSString* mpMedia = _currentOutgoingCallRef ? @"mediaPillarMakeCall" : @"mediaPillar";
    OTCLog (@"addRtcCall with sessionId '%@', displayname '%@' and ressource '%@'", _rtcSessionId, _rtcPeer.displayName, _rtcRessource);
    [_rtcService didReceiveProposeMsg:_rtcSessionId withMedias:@[@"audio", mpMedia] from:_rtcPeer resource:_rtcRessource];
}


- (RTCCall*) rtcCallFromCxCallOne:(CXCall*) cxCall
{
    if (!cxCall)
    {
        OTCLog(@"rtcCallFromCxCallOne 'nil' : usefirst rtcCall");
        return _rtcService.calls.firstObject;
    }
    
    NSArray* rtcCalls = [_rtcService.calls copy];
    for (RTCCall* rtCall in rtcCalls)
    {
        if ([rtCall.callID isEqual:cxCall.UUID])
            return rtCall;
    }
    
    OTCLog (@"rtcCallFromCxCallOne from cxCall.UUID '%@' not found", cxCall.UUID);
    return nil;
}


#pragma mark - 3PCC calls management methods

-(void) makeCallTo:(PhoneNumber *) phoneNumber fallBackHandler:(TelephonyServiceMakeCallFallBack) fallbackHandler completionHandler:(TelephonyServiceMakeCallCompletionHandler) completionHandler {
    
    if (!_myUser.isAllowedToUseTelephony) {
        if (fallbackHandler) {
            NSError *error = [NSError errorWithDomain:@"TelephonyService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"User don't have the right to use this feature"}];
            fallbackHandler(error, phoneNumber);
            return;
        }
    }
    
    if (!_myUser.isNomadicModeActivated) {
        if (fallbackHandler) {
            NSError *error = [NSError errorWithDomain:@"TelephonyService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"User does not have the nomadic mode activated"}];
            fallbackHandler(error, phoneNumber);
            return;
        }
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony, @"calls"];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    
    NSDictionary *body = @{@"calleeExtNumber":phoneNumber.number, @"secondaryDeviceMakeCall":@"true"};
    OTCLog (@"makeCall body %@",body);
    @synchronized(_currentPhoneStateMutex) {
        [_currentPhoneState startMakeCall];
    }
    
    if (completionHandler) {
        _makeCallCompletionHandler = nil;
        _makeCallCompletionHandler = [completionHandler copy];
    }
    
    [self stopTimer];

    __weak __typeof__(self) weakSelf = self;
    _makeCallTimer = [NSTimer scheduledTimerWithTimeInterval:30 repeats:NO block:^(NSTimer * timer) {
        OTCLog (@"MAKE CALL TIMER TIMEOUT AFTER 30s");
        @synchronized(_currentPhoneStateMutex) {
            [weakSelf cancelOutgoingCall];
            [_currentPhoneState timerOff];
        }
        if (fallbackHandler) {
            NSError *error = [NSError errorWithDomain:@"TelephonyService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Make call action failed in allowed delay (30s) fallback"}];
            fallbackHandler(error, phoneNumber);
        }
    }];
    OTCLog  (@"Make call timer started");

    [_downloadManager postRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot make call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }

        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot make call in REST due to JSON parsing failure");
            return;
        }

        OTCLog (@"MAKE CALL DONE %@", jsonResponse);
        NSDictionary *jsonData = jsonResponse[@"data"];
        if (jsonData) {
            NSString* callRef = jsonData[@"callId"];
            if (callRef.length)
                weakSelf.currentOutgoingCallRef = callRef;
        }
    }];
}


-(void) cancelOutgoingCall
{
    if (_currentOutgoingCallRef == nil)
    {
        OTCLog  (@"cannot cancel outgoing call, no currentOutgoingCallRef");
    }

    Call* outgoingCall = [self getCallByCallReference:_currentOutgoingCallRef];
    if (!outgoingCall)
    {
        outgoingCall = [Call new];
        outgoingCall.callRef = _currentOutgoingCallRef;
    }
    [self releaseCall:outgoingCall];
    [self stopTimer];
}


-(void) releaseCall:(Call *) call {
    // For test only
    call.status = CallStatusHangup;
    [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:call];

    if ([call.callRef isEqualToString:_currentOutgoingCallRef])
        _currentOutgoingCallRef = nil;

    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony, [NSString stringWithFormat:@"calls/%@", [call.callRef urlEncode]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot release call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot release call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Release call done %@", jsonResponse);
    }];
    if (call.isMediaPillarCall && _calls.count == 1)
    {
        OTCLog (@"Release unique call, also release RTC one ? (as Android)");
        RTCCall* rtCall = [self rtcCallFromCxCallOne:nil];
        [_rtcService cancelOutgoingCall:rtCall];
    }
}


-(void) answerCall:(Call *) call {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony, [NSString stringWithFormat:@"calls/%@/answer", [call.callRef urlEncode]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    [_downloadManager putRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot answer call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot answer call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Answer call done %@", jsonResponse);
    }];
}


-(void) deflectCall:(Call *) call toPhoneNumber:(PhoneNumber *) phoneNumber {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,[NSString stringWithFormat:@"calls/%@/deflect", [call.callRef urlEncode]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    
    NSDictionary *body = @{@"calleeExtNumber":phoneNumber.number};
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot deflect call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot deflect call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Deflect call done %@", jsonResponse);
    }];
}


-(void) holdCall:(Call *) call {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,[NSString stringWithFormat:@"calls/%@/hold", [call.callRef urlEncode]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    
    [_downloadManager putRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot hold call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot hold call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Hold call done %@", jsonResponse);
    }];
}


-(void) retrieveCall:(Call *) call {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,[NSString stringWithFormat:@"calls/%@/retrieve", [call.callRef urlEncode]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    
    [_downloadManager putRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot retreive call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot retreive call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Retreive call done %@", jsonResponse);
    }];
}


-(void) transferCall:(Call *) call toHeldCall:(Call *) heldCall {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,[NSString stringWithFormat:@"calls/%@/transfer/%@", [call.callRef urlEncode], [heldCall.callRef urlEncode]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    
    [_downloadManager putRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot transfer call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot transfer call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Transfer call done %@", jsonResponse);
    }];
}


#pragma mark - State machine state

/**
 Invoked on __main__ thread (euh... Sure ?)
-(void) onGsmCallStateChanged:(CTCall*) ctCall currentPhoneState:(PhoneState *) currentPhoneState{
    OTCLog  (@"[TELEPHONYSVC] CT Center callback for call : %@", ctCall);
    if ([ctCall.callState isEqualToString:CTCallStateIncoming]) {
        [_currentPhoneState gsmRinging];
    }
    else if ([ctCall.callState isEqualToString:CTCallStateDialing]) {
    }
    else if ([ctCall.callState isEqualToString:CTCallStateConnected]) {
        [currentPhoneState gsmConnected];
    [self stopTimer];
    }
    else if ([ctCall.callState isEqualToString:CTCallStateDisconnected]) {
        [_currentPhoneState gsmIdle];
    }
}

-(void) applicationDidBecomeActive:(NSNotification *) notification {
    if (_ctCenter.currentCalls.count > 0) {
        OTCLog (@"we have GSM calls check if there is calls in the system");
        [self getTelephonicState];
//        [self getTelephonicStateThrowXMPP];
    }
}
 */


-(void) setState:(Class) newPhoneState {
    @synchronized(_currentPhoneStateMutex) {
        _currentPhoneState.telService = nil;
        _currentPhoneState.rtcService = nil;
        _currentPhoneState = nil;
        _currentPhoneState = [newPhoneState new];
        _currentPhoneState.telService = self;
        _currentPhoneState.rtcService = _rtcService;
        [_currentPhoneState doEnter];
    }
}


//-(void) startTimerOf:(NSTimeInterval) duration withBlock:(TelephonyServiceMakeCallCompletionHandler _Nullable) fireBlock
-(void) startTimerOf:(NSTimeInterval) duration
{
    [self stopTimer];

    _makeCallTimer = [NSTimer scheduledTimerWithTimeInterval:duration repeats:NO block:^(NSTimer * timer) {
        OTCLog  (@"call timer fired");
        @synchronized(_currentPhoneStateMutex) {
            [_currentPhoneState timerOff];
        }
    }];
    OTCLog  (@"call timer started");
}


-(void) stopTimer
{
    if (_makeCallTimer)
    {
        OTCLog  (@"call timer invalidated");
        [_makeCallTimer invalidate];
        _makeCallTimer = nil;
    }
}

#pragma mark - RTCService events
-(void) didReceiveIncomingMpCall:(NSNotification *)notification {
    OTCLog(@"We receive a mediapillar incoming call so put _needToAddCall = YES");
    if (!_isCallKitAvailable) {
        RTCCall *rtcCall = (RTCCall *)notification.object;
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidAddCallNotification object:rtcCall];
        // _needToAddCall = YES;
    }else {
        _needToAddCall = YES;
    }
}


-(void) didReceiveHangupCall: (NSNotification *)notification {
    OTCLog(@"didReceiveHangupCall");
    if (!_isCallKitAvailable) {
        RTCCall *rtcCall = (RTCCall *)notification.object;
        @synchronized(_currentPhoneStateMutex) {
            OTCLog (@"didReceiveHangupCall -> GSMIdle %@", _currentPhoneState);
            [_currentPhoneState gsmIdle:nil];
        }
    }
}

#pragma mark - Xmpp telephony events

-(void) xmppService:(XMPPService *)service didReceiveCallEvent:(CallEvent *)callEvent {
    if ([callEvent.deviceType isEqualToString:@"MAIN"])   // Warning : no deviceType on makeCall
    {
        OTCLog(@"state machine ignores MAIN device %@", callEvent);
        if ([callEvent.callReference isEqualToString:_currentOutgoingCallRef])
            _currentOutgoingCallRef = nil;
        return;
    }

    OTCLog(@"state machine %@ Did receive %@", _currentPhoneState, callEvent);
    Call* concernedCall = [self createOrUpdateCallWithCallEvent:callEvent];
    if (!concernedCall)
    {
        OTCLog(@"didReceiveCallEvent ignored, no concernedCall");
        return;
    }

    switch (callEvent.state) {
        case CallStateActive: {
            // Don't reset the timer if we already have the information (from callkit)
            if (!concernedCall.connectionDate)
                concernedCall.connectionDate = [NSDate date];
            
            @synchronized(_currentPhoneStateMutex) {
                // The call is already active from the callkit screen
                if ([_currentPhoneState isKindOfClass:[PhoneStateOneActiveCall class]]) {
                    OTCLog(@"The current state is PhoneStateOneActiveCall --> this is probably from CallKit, just add the call");
                    [self addCall:concernedCall];
                } else {
                    [_currentPhoneState activeEvent:concernedCall];
                }
            }
            break;
        }
        case CallStateIdle: {
            @synchronized(_currentPhoneStateMutex) {
                [_currentPhoneState releaseEvent:concernedCall];
            }
            break;
        }
            
        case CallStateRingingIncoming:
            [_currentPhoneState incomingRingEvent:concernedCall];
            break;
            
        case CallStateUnknown:
            OTCLog (@"state 'Unknown', participant displayName should have been updated : '%@'", concernedCall.peer.displayName);
            if (concernedCall.status == CallStatusEstablished)
                [self updateCall:concernedCall];
            break;
            
        case CallStateOffHook:
            
            break;
        case CallStateReleasing: {
            @synchronized(_currentPhoneStateMutex) {
                [_currentPhoneState releaseEvent:concernedCall];
            }
            break;
        }
        case CallStateDialing:
            
            break;
        case CallStateHeld:
            
            break;
        case CallStateRingingOutgoing: {
            @synchronized(_currentPhoneStateMutex) {
                [_currentPhoneState outgoingRingEvent:concernedCall callCause:callEvent.callCause];
            }
            break;
        }
        default:
            break;
    }
}


-(void) xmppService:(XMPPService *) service didReceiveMpCall:(NSString*) sessionID withPeer:(Peer*) mpPeer andResource:(NSString*) resource
{
    self.rtcSessionId = sessionID;
    self.rtcPeer = mpPeer;
    self.rtcRessource = resource;
    @synchronized(_currentPhoneStateMutex) {
        OTCLog (@"mpIncomingCall -> GSMRinging %@", _currentPhoneState);
        if (![_currentPhoneState isKindOfClass:[PhoneStateInitMakeCall class]]) // RTODO: PLZ REMOVE THIS LATER (from 1.46 app)
            [self setState:[PhoneStateEvtRingingIncoming class]];
        
        [_currentPhoneState gsmRinging:nil];
    }
}



#pragma mark - CXCallObserver Delegate

- (void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call {
    OTCLog (@"callObserver CALL %@", call);
    // Check ended before connected as an ended call is generally connected before !
    if (call.hasEnded) {
        @synchronized(_currentPhoneStateMutex) {
            OTCLog (@"callObserver -> GSMIdle %@", _currentPhoneState);
            [_currentPhoneState gsmIdle:call];
        }
        return;
    }
    if (call.hasConnected) {
        @synchronized(_currentPhoneStateMutex) {
            OTCLog (@"callObserver -> GSMConnected %@", _currentPhoneState);
            [_currentPhoneState gsmConnected:call];
        }
        [self stopTimer];
        return;
    }
    if (call.onHold) {
        // TODO: Implement onHold state for call
        return;
    }
    if (call.outgoing) {
        // TODO: implement GSM outgoing call
        return;
    }
    // if we don't exit earlier that means we have an incoming call
    @synchronized(_currentPhoneStateMutex) {
        OTCLog (@"callObserver -> GSMRinging %@", _currentPhoneState);
        [_currentPhoneState gsmRinging:call];
    }
}

-(void) applicationDidBecomeActive:(NSNotification *) notification {
    if (_callObserver.calls.count > 0) {
        OTCLog (@"we have GSM calls check if there is calls in the system");
        if (_needToAddCall) {
            OTCLog(@"We need to add the call - probably from CallKit screen");
            [self getTelephonicState];
            _needToAddCall = NO;
        }
    }
}


#pragma mark - CRUD calls

-(Call *) getCallByCallReference:(NSString *) callReference {
    Call *call = nil;
    @synchronized(_callsMutex) {
        for (Call *aCall in _calls) {
            if ([aCall.callRef isEqualToString:callReference]) {
                call = aCall;
                break;
            }
        }
    }
    
    return call;
}


-(Call*) createOrUpdateCallWithCallEvent:(CallEvent*) callEvent
{
    Call *aCall = nil;
    BOOL newCall = NO;
    @synchronized(_callsMutex) {
        aCall = [self getCallByCallReference:callEvent.callReference];
        if (!aCall && callEvent.state == CallStateUnknown)
        {
            OTCLog(@"update for another call !");
            return nil;
        }
        
        if (!aCall)
        {
            aCall = [Call new];
            aCall.callRef = callEvent.callReference;
            aCall.shortCallRef = callEvent.shortCallReference;
            [_calls addObject:aCall];
            newCall = YES;
        }
    }
    
    if (callEvent.callParticipants.count > 0)
    {
        // we create only one peer for now
        // Search for the rainbow contact based on the jid
        CallParticipant *aParticipant = [callEvent.callParticipants firstObject];
        if ([aParticipant.lastname containsString:@"REX "] || [aParticipant.lastname containsString:@"REX "])// ignorer lastName REX 304, number 314, voir Cédric
        {
            OTCLog (@"Don't update participant with 'REX ' !");
            return nil;
        }
        
        Contact *aContact = [_contactsManagerService getContactWithJid:aParticipant.jid];
        if (!aContact)
        {
            // We don't known this contact so create one
            if (aParticipant.jid)
               aContact = [_contactsManagerService createOrUpdateRainbowContactWithJid:aParticipant.jid];
            if (!aContact)
                aContact = [_contactsManagerService createOrUpdateRainbowContactWithPhoneNumber:aParticipant.number];
            
            if (aParticipant.firstname || aParticipant.lastname) {
                NSMutableDictionary *dic = [NSMutableDictionary new];
                if (aParticipant.firstname)
                    [dic setObject:aParticipant.firstname forKey:@"firstName"];
                if (aParticipant.lastname)
                    [dic setObject:aParticipant.lastname forKey:@"lastName"];
                [_contactsManagerService updateRainbowContact:aContact withJSONDictionary:dic];
            }
        }
        if (!aContact)
            OTCLog (@"We cannot find/create a contact from the callEvent informations %@", callEvent);
        aCall.peer = aContact;
    }
    if (callEvent.callCause)
        aCall.callCause = [NSString stringWithString:callEvent.callCause];
    
    switch (callEvent.state)
    {
        case CallStateUnknown: {
            
            break;
        }
        case CallStateOffHook: {
            
            break;
        }
        case CallStateIdle: {
            aCall.status = CallStatusCanceled;
            break;
        }
        case CallStateReleasing: {
            aCall.status = CallStatusHangup;
            break;
        }
        case CallStateDialing: {
            aCall.status = CallStatusConnecting;
            break;
        }
        case CallStateHeld: {
            break;
        }
        case CallStateRingingIncoming: {
            aCall.status = CallStatusRinging;
            break;
        }
        case CallStateRingingOutgoing: {
            aCall.status = CallStatusRinging;
            break;
        }
        case CallStateActive: {
            aCall.status = CallStatusEstablished;
            break;
        }
    }
    
    return aCall;
}

+(PhoneNumber *) getPhoneNumberFromString:(NSString *) phoneString {
    PhoneNumber * phone = [PhoneNumber new];
    phone.number = phoneString;
    return phone;
}

@end
