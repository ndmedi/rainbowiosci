/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

#import "TelephonyService.h"
#import "DownloadManager.h"
#import "ApiUrlManagerService.h"
#import "XMPPService.h"
#import "MyUser.h"
#import "RTCService.h"
#if TARGET_OS_IPHONE
#import <CallKit/CallKit.h>
#endif


@interface TelephonyService()

-(instancetype _Nonnull) initWithXMPPService:(XMPPService * _Nonnull) xmppService myUser:(MyUser * _Nonnull) myUser downloadManager:(DownloadManager * _Nonnull) downloadManager apiUrlManagerService:(ApiUrlManagerService * _Nonnull) apiUrlManagerService contactsManagerService:(ContactsManagerService * _Nonnull) contactsManagerService rtcService:(RTCService * _Nonnull) rtcService;

-(void) addCall:(Call* _Nullable) newCall;
-(void) addRtcCall;
-(RTCCall*) rtcCallFromCxCallOne:(CXCall*) cxCall;

-(void) updateCall:(Call* _Nullable) upCall;
-(void) removeCall:(Call* _Nullable) oldCall;
-(void) setState:(Class _Nonnull) newPhoneState;

/**
 *  Start a timer of duration seconds
 */
-(void) startTimerOf:(NSTimeInterval) duration;

/**
 *  Stop internal timer
 */
-(void) stopTimer;

@end
