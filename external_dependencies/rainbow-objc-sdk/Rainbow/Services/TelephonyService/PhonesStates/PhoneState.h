//
//  PhoneState.h
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#ifndef PhoneState_h
#define PhoneState_h

#import <Foundation/Foundation.h>
#import "TelephonyService+Internal.h"
#import <CallKit/CallKit.h>
#import "Call+Internal.h"


typedef enum : NSInteger {
    Init,
    InitMakeCall,
    InitSecondMakeCall,
    MakeCallRinging,
    OutgoingRinging,
    RingingIncomingEVS,
    DoubleRingingIncomingEVS,
    RingingIncoming,
    ingingIncomingGSM,
    SimpleConvers,
    SimpleDirectConvers,
    SecondIncomingRing,
    SecondOutgoingRing,
    DoubleConvers,
    DoubleIncomingRing,
    WaitIdle,
    WaitEVSConversActive,
    WaitEVSInitActive,
    WaitGSMConversActive,
    PrivateCall,
    OutgoingIncoming,
    MakePrivateCall,
    WaitEVSSecondIncomingActive,
    WaitGSMSecondIncomingActive,
    Callback,
    RingingOutgoingEVS,
    WaitGSMOutgoingRinging,
    WaitEVSOutgoingRinging,
    TransferToDeskphone
} PhoneStates;


@interface PhoneState : NSObject

@property (readwrite, assign) TelephonyService* telService;
@property (readwrite, assign) RTCService* rtcService;

@property (nonatomic, strong) NSString* currentState;


/**
 * Executed when entering in the state.
 */
- (void) doEnter;


/**
 * End of timer.
 */
- (void) timerOff;

/**
 * Start a make call interaction.
 */
- (void) startMakeCall;

/**
 * Abort the makecall in progress. This event is received when an error message is receive from
 * the framework when MakeCall is invoked.
 */
- (void) abortMakeCall;

/**
 * The phone is starting to ring.
 */
- (void) gsmRinging:(CXCall *) call;

/**
 * The phone is off hook.
 */
- (void) gsmConnected:(CXCall *) call;

/**
 * The phone is being idle.
 */
- (void) gsmIdle:(CXCall *) call;

/**
 * The active event is received.
 *
 * @param iSession the session id.
 */
- (void) activeEvent:(Call*) aCall;

/**
 * The held event is received.
 *
 * @param iSession the session id.
 */
- (void) heldEventEvent:(Call*) aCall;

/**
 * The release event is received.
 *
 * @param aCall : released call.
 */
- (void) releaseEvent:(Call*) aCall;

/**
 * The outgoing ring event is received.
 *
 * @param aCall : the outgoing call.
 */
- (void) outgoingRingEvent:(Call *)aCall callCause:(NSString *) callCause;

/**
 * The incoming ring event is received.
 *
 * @param aCall : the incoming call.
 */
- (void) incomingRingEvent:(Call*) aCall;


#ifdef LATER


/**
 * Stop the call in case of error
 */
- (void) stopMakeCall;

/**
 * Take the GSM call while 2 incoming calls
 */
- (void) takeCallInDoubleIncomingCall;

/**
 * private call detected
 */
- (void) privateCallDetected;

/**
 * start the 30s timer waitong for pabx callback if makecall WS succeeded
 */
- (void) startMakeCallPABXCallbackTimer;

- (void) transferToDeskphone;

#endif

@end

#endif /* PhoneState_h */
