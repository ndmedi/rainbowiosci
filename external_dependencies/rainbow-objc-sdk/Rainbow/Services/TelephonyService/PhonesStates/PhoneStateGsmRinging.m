//
//  PhoneStateGsmRinging.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateGsmRinging.h"
#import "AllPhoneStates.h"


@implementation PhoneStateGsmRinging

- (void) doEnter
{
    self.currentState = @"PhoneStateGsmRinging";
}

/**
 * The phone is starting to ring.
 */
- (void) gsmRinging:(CXCall *) call
{
    OTCLog (@"event GsmRinging : new GSM call ?");
}

/**
 * The phone is off hook.
 */
- (void) gsmConnected:(CXCall *) call
{
    OTCLog (@"gsmConnected : change to GsmConnected");
    [self.telService setState:[PhoneStateGsmConnected class]];
}

/**
 * The phone is being idle.
 */
- (void) gsmIdle:(CXCall *) call
{
    OTCLog (@"gsmIdle : change to Idle");
    [self.telService setState:[PhoneStateIdle class]];
}


- (void) incomingRingEvent:(Call*) aCall
{
    OTCLog (@"incomingRingEvent : change to EvtRingingIncoming");
    aCall.readyToDisplay = NO;
    [self.telService setState:[PhoneStateRingingIncoming class]];
}

@end

