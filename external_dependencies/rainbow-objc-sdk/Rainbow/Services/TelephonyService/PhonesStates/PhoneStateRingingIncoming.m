//
//  PhoneStateRingingIncoming.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateRingingIncoming.h"
#import "AllPhoneStates.h"


@implementation PhoneStateRingingIncoming

- (void) doEnter
{
    self.currentState = @"PhoneStateRingingIncoming";
}

/**
 * End of timer.
 */
- (void) timerOff
{
    
}


/**
 * The phone is off hook.
 */
- (void) gsmConnected:(CXCall *) call
{
    OTCLog (@"gsmConnected : change to WaitActiveEvt");
    [self.telService setState:[PhoneStateWaitActiveEvt class]];
}


- (void) activeEvent:(Call*) aCall
{
    OTCLog (@"activeEvent : change to ActiveCall");
    aCall.readyToDisplay = YES;
    [self.telService addCall:aCall];
    [self.telService setState:[PhoneStateOneActiveCall class]];
}


- (void) releaseEvent:(Call*) aCall
{
    if ([aCall.callCause isEqualToString:@"CALLPICKUP"])
    {
        OTCLog (@"releaseEvent with cause 'pickup' : no change on release ");
        [self.telService removeCall:aCall];
    }
    else
    {
        OTCLog (@"releaseEvent : no change on release, wait GSM one ");
    }
}

@end
