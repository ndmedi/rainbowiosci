//
//  PhoneStateInitMakeCall.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateInitMakeCall.h"
#import "AllPhoneStates.h"
#import "RTCService+Internal.h"


@implementation PhoneStateInitMakeCall

- (void) doEnter
{
    self.currentState = @"PhoneStateInitMakeCall";
}

/**
 * End of timer.
 */
- (void) timerOff
{
    [self gsmIdle:nil];
}

/**
 * Start a make call interaction.
 */
- (void) startMakeCall
{
    OTCLog (@"PhoneState event : startMakeCall -> new business call ?");
}

/**
 * Abort the makecall in progress. This event is received when an error message is receive from
 * the framework when MakeCall is invoked.
 */
- (void) abortMakeCall
{
    
}

/**
 * The phone is starting to ring.
 */
- (void) gsmRinging:(CXCall *) cxCall
{
    RTCCall* rtCall = [self.telService rtcCallFromCxCallOne:cxCall];
    if (cxCall)
    {
        OTCLog (@"PhoneState no change : InitMakeCall -> GsmRinging ignored...");
        //    [self.telService setState:[PhoneStateMakeCallGsmRinging class]];
    }
    else   // rtcWeb call
    {
        OTCLog (@"PhoneState no change : InitMakeCall -> GsmRinging for rtcCall, should auto-answer...");
        [self.telService addRtcCall];
        [NSThread sleepForTimeInterval:0.2f];
        RTCCall* rtCall = [self.telService rtcCallFromCxCallOne:cxCall];
        if (rtCall)
            [self.rtcService acceptIncomingCall:rtCall withFeatures:RTCCallFeatureAudio];
    }
}

/**
 * The phone is off hook.
 */
- (void) gsmConnected:(CXCall *) call
{
    OTCLog (@"gsmConnected : change to OutgoingRinging");
    [self.telService setState:[PhoneStateOutgoingRinging class]];
}

- (void) gsmIdle:(CXCall*) call
{
    OTCLog (@"gsmIdle : change to Idle");
    [self.telService removeCall:nil];   // all calls ???
    [self.telService setState:[PhoneStateIdle class]];
}


-(void) releaseEvent:(Call*) aCall
{
    OTCLog (@"releaseEvent : change to Idle");
    [self.telService removeCall:aCall];
    [self.telService setState:[PhoneStateIdle class]];
}


-(void) outgoingRingEvent:(Call*) aCall callCause:(NSString*) callCause
{
    OTCLog (@"outgoingRingEvent : change to OutgoingRinging");
    aCall.readyToDisplay = YES;
    [self.telService stopTimer];
    [self.telService addCall:aCall];
    [self.telService setState:[PhoneStateOutgoingRinging class]];
}

@end

