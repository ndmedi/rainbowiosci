//
//  PhoneStateOutgoingRinging.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateInitMakeCall.h"
#import "AllPhoneStates.h"
#import "Call+Internal.h"


@implementation PhoneStateOutgoingRinging
- (void) doEnter
{
    self.currentState = @"PhoneStateOutgoingRinging";
}

/**
 * End of timer.
 */
- (void) timerOff
{
    
}

/**
 * Start a make call interaction.
 */
- (void) startMakeCall
{
    OTCLog (@"PhoneState event : startMakeCall -> new business call ?");
}

/**
 * Abort the makecall in progress. This event is received when an error message is received from
 * the framework when MakeCall is invoked.
 */
- (void) abortMakeCall
{
    
}


- (void) activeEvent:(Call*) aCall
{
    OTCLog (@"activeEvent : change to ActiveCall");
    aCall.readyToDisplay = YES;
    [self.telService updateCall:aCall];
    [self.telService setState:[PhoneStateOneActiveCall class]];
}


- (void) releaseEvent:(Call*) aCall
{
    OTCLog (@"releaseEvent : change to Idle");
    [self.telService removeCall:aCall];
    [self.telService setState:[PhoneStateIdle class]];
}


- (void) gsmIdle:(CXCall *) call
{
    OTCLog (@"gsmIdle : change to Idle");
    [self.telService removeCall:nil];   // all calls ???
    [self.telService setState:[PhoneStateIdle class]];
}

@end

