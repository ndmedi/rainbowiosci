//
//  PhoneStateGsmConnected.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateGsmConnected.h"
#import "AllPhoneStates.h"


@implementation PhoneStateGsmConnected

- (void) doEnter
{
    self.currentState = @"PhoneStateGsmConnected";
}

/**
 * The phone is starting to ring.
 */
- (void) gsmRinging:(CXCall *) call
{
    OTCLog (@"event gsmRinging -> new GSM call ?");
}

/**
 * The phone is being idle.
 */
- (void) gsmIdle:(CXCall *) call
{
    OTCLog (@"gsmIdle : change to Idle");
    [self.telService setState:[PhoneStateIdle class]];
}


- (void) activeEvent:(Call*) aCall
{
    OTCLog (@"activeEvent : change to ActiveCall");
    aCall.readyToDisplay = YES;
    [self.telService addCall:aCall];
    [self.telService setState:[PhoneStateOneActiveCall class]];
}

@end

