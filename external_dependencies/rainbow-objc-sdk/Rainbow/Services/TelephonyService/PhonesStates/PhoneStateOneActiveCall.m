//
//  PhoneStateOneActiveCall.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateInitMakeCall.h"
#import "AllPhoneStates.h"


@implementation PhoneStateOneActiveCall

- (void) doEnter
{
    self.currentState = @"PhoneStateOneActiveCall";
}

- (void) timerOff
{
    
}


- (void) startMakeCall
{
    OTCLog (@"event startMakeCall : new business call ?");
}


- (void) releaseEvent:(Call*) aCall
{
    OTCLog (@"releaseEvent : change to Idle");
    [self.telService removeCall:aCall];
    [self.telService setState:[PhoneStateIdle class]];
}


- (void) gsmIdle
{
    OTCLog (@"gsmIdle : change to Idle");
    [self.telService removeCall:nil];   // all calls ???
    [self.telService setState:[PhoneStateIdle class]];
}

@end
