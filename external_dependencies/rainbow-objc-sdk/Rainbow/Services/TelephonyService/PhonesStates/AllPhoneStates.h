//
//  AllPhoneStates.h
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#define USE_CLASS 0

#if USE_CLASS

Error :
/Users/jlf/developments/Rainbow/otcl-ios-app/external_dependencies/rainbow-objc-sdk/Rainbow/Services/TelephonyService/PhonesStates/PhoneStateGsmRinging.m:30:32: Receiver 'PhoneStateGsmConnected' for class message is a forward declaration
@class PhoneStateIdle;
@class PhoneStateGsmRinging;
@class PhoneStateGsmConnected;

#else

#import "PhoneState.h"
#import "PhoneStateIdle.h"
#import "PhoneStateGsmRinging.h"
#import "PhoneStateGsmConnected.h"
#import "PhoneStateInitMakeCall.h"
#import "PhoneStateOutgoingRinging.h"
#import "PhoneStateOneActiveCall.h"
#import "PhoneStateEvtRingingIncoming.h"
#import "PhoneStateRingingIncoming.h"
#import "PhoneStateWaitActiveEvt.h"

#endif
/*
*/

