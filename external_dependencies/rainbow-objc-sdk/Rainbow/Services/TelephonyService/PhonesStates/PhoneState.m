//
//  PhoneState.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneState.h"
#import "PhoneStateIdle.h"


@implementation PhoneState
//-(instancetype) initWithTelService:(TelephonyService*) teSce

-(instancetype) init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}


- (void) doEnter
{
    
}


/**
 * End of timer.
 */
- (void) timerOff
{
    
}

/**
 * Start a make call interaction.
 */
- (void) startMakeCall
{
    
}

/**
 * Abort the makecall in progress. This event is received when an error message is receive from
 * the framework when MakeCall is invoked.
 */
- (void) abortMakeCall
{
    
}

/**
 * The phone is starting to ring.
 */
- (void) gsmRinging:(CXCall *) call
{
    OTCLog (@"Unexpected event received: gsmRinging in state: %@",self.currentState);
}

/**
 * The phone is off hook.
 */
- (void) gsmConnected:(CXCall *) call
{
    OTCLog (@"Unexpected event received: gsmConnected in state: %@",self.currentState);
}

/**
 * The phone is being idle.
 */
- (void) gsmIdle:(CXCall *) call
{
    OTCLog (@"(generic) gsmIdle : change to Idle current state: %@",self.currentState);
    [self.telService removeCall:nil];   // all calls ???
    [self.telService setState:[PhoneStateIdle class]];
}


/**
 * The active event is received.
 *
 * @param iSession the session id.
 */
- (void) activeEvent:(Call*) aCall
{
    OTCLog (@"Unexpected event received: activeEvent in state: %@",self.currentState);
}


/**
 * The held event is received.
 *
 * @param iSession the session id.
 */
- (void) heldEventEvent:(Call*) aCall {
    OTCLog (@"Unexpected event received: heldEventEvent in state: %@",self.currentState);
}

/**
 * The release event is received.
 *
 * @param iSession the session id.
 - (void) releaseEvent(ISession iSession, CallCause callCause); voir si callCause needed ?
 */
-(void) releaseEvent:(Call*) aCall
{
    OTCLog (@"releaseEvent : generic (provisoire ???) -> Idle in state %@",self.currentState );
    [self.telService removeCall:aCall];
    [self.telService setState:[PhoneStateIdle class]];
}


- (void) outgoingRingEvent:(Call *)aCall callCause:(NSString *) callCause {
    OTCLog (@"Unexpected event received: outgoingRingEvent in state: %@",self.currentState);
}


/**
 * The incoming ring event is received.
 *
 * @param aCall : the incoming call.
 */
- (void) incomingRingEvent:(Call*) aCall

{
    OTCLog (@"Unexpected event received: incomingRingEvent in state: %@",self.currentState);
}


//#define LATER 0
#ifdef LATER

/**
 * The outgoing ring event is received.
 *
 * @param iSession the session id.
 */
- (void) outgoingRingEvent(ISession iSession, CallCause callCause);

/**
 * The active event is received.
 *
 * @param iSession the session id.
 */
- (void) activeEvent(ISession iSession, CallCause callCause);

/**
 * The held event is received.
 *
 * @param iSession the session id.
 */
- (void) heldEvent(ISession iSession);

/**
 * Stop the call in case of error
 */
- (void) stopMakeCall;

/**
 * Take the GSM call while 2 incoming calls
 */
- (void) takeCallInDoubleIncomingCall;

/**
 * private call detected
 */
- (void) privateCallDetected;

/**
 * start the 30s timer waiting for pabx callback if makecall WS succeeded
 */
- (void) startMakeCallPABXCallbackTimer;

- (void) transferToDeskphone;

#endif

@end

