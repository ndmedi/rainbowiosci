//
//  PhoneStateEvtRingingIncoming.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateEvtRingingIncoming.h"
#import "AllPhoneStates.h"


@implementation PhoneStateEvtRingingIncoming

- (void) doEnter
{
    
    self.currentState = @"PhoneStateEvtRingingIncoming";
    [self.telService startTimerOf:30];
}


- (void) timerOff
{
    OTCLog (@"Waiting pbx call timeout after 30s");
    [self.telService removeCall:nil];
    [self.telService setState:[PhoneStateIdle class]];
}


- (void) gsmRinging:(CXCall *) cxCall
{
    if (!cxCall)
    {
        OTCLog (@"gsmRinging : show rtcCall in CallKit");
        [self.telService addRtcCall];
    }
    OTCLog (@"gsmRinging : change to RingingIncoming");
    [self.telService setState:[PhoneStateRingingIncoming class]];
}


- (void) gsmConnected:(CXCall *) cxCall
{
    //    OTCLog (@"gsmConnected : change to GsmConnected");
//    [self.telService setState:[PhoneStateGsmConnected class]];
}


- (void) releaseEvent:(Call*) aCall
{
    OTCLog (@"releaseEvent : change to Idle");
    [self.telService removeCall:aCall];
    [self.telService setState:[PhoneStateIdle class]];
}

@end

