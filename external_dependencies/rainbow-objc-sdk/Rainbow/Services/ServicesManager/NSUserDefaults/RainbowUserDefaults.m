/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowUserDefaults.h"
#import "PINCache.h"
#import "PINDiskCache+forceRefresh.h"

#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
#import <UIKit/UIKit.h>
#endif

static RainbowUserDefaults *singleton = nil;
static NSString *suiteName = nil;

@interface RainbowUserDefaults ()
@property (nonatomic,strong) PINCache *userDefaultsCache;
@end

@implementation RainbowUserDefaults

+(void) setSuiteName:(NSString *) newSuiteName {
    suiteName = newSuiteName;
}

+(NSString *) suiteName {
    return suiteName;
}

+(RainbowUserDefaults*) sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[RainbowUserDefaults alloc] init];
    });
    return singleton;
}

-(instancetype) init {
    self = [super init];
    if(self){
        _userDefaultsCache = [[PINCache alloc] initWithName:@"userDefaults"];
        _userDefaultsCache.memoryCache.removeAllObjectsOnMemoryWarning = NO;
        _userDefaultsCache.memoryCache.removeAllObjectsOnEnteringBackground = NO;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
    }
    return self;
}

-(void) dealloc {
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
}

-(void) applicationDidBecomeActive:(NSNotification *) notification {
    [_userDefaultsCache.diskCache forceRefresh];
}

-(void) setObject:(id)value forKey:(NSString *)defaultName {
    [_userDefaultsCache setObject:value forKey:defaultName];
}

- (nullable id)objectForKey:(NSString *)defaultName {
    return [_userDefaultsCache objectForKey:defaultName];
}

-(void)removeObjectForKey:(NSString *)defaultName {
    [_userDefaultsCache removeObjectForKeyAsync:defaultName completion:nil];
}

-(NSString *)stringForKey:(NSString *)defaultName {
    return [NSString stringWithFormat:@"%@",[self objectForKey:defaultName]];
}

- (void)setBool:(BOOL)value forKey:(NSString *)defaultName {
    [_userDefaultsCache setObject:[NSNumber numberWithBool:value] forKey:defaultName];
}

- (void)setInteger:(NSInteger)value forKey:(NSString *)defaultName {
    [_userDefaultsCache setObject:[NSNumber numberWithInteger:value] forKey:defaultName];
}
- (void)setFloat:(float)value forKey:(NSString *)defaultName {
    [_userDefaultsCache setObject:[NSNumber numberWithFloat:value] forKey:defaultName];
}
- (void)setDouble:(double)value forKey:(NSString *)defaultName {
    [_userDefaultsCache setObject:[NSNumber numberWithDouble:value] forKey:defaultName];
}

- (BOOL)boolForKey:(NSString *)defaultName {
    id object = [_userDefaultsCache objectForKey:defaultName];
    if([object isKindOfClass:[NSNumber class]]){
        return ((NSNumber*)object).boolValue;
    }
    if([object isKindOfClass:[NSString class]]){
        NSString *theObject = (NSString *) object;
        if([theObject isEqualToString:@"YES"])
            return YES;
        else
            return NO;
    }
    return NO;
}

- (nullable NSArray *)arrayForKey:(NSString *)defaultName {
    id object = [_userDefaultsCache objectForKey:defaultName];
    if([object isKindOfClass:[NSArray class]]){
        return ((NSArray*)object);
    }
    return nil;
}

- (void)registerDefaults:(NSDictionary<NSString *, id> *)registrationDictionary {
    [registrationDictionary enumerateKeysAndObjectsUsingBlock:^(NSString * key, id obj, BOOL * stop) {
        [_userDefaultsCache setObject:obj forKey:key];
    }];
}

-(NSInteger) count {
    NSMutableArray *keys = [NSMutableArray array];
    [_userDefaultsCache.diskCache enumerateObjectsWithBlock:^(NSString * _Nonnull key, NSURL * _Nullable fileURL, BOOL * _Nonnull stop) {
        [keys addObject:key];
    }];
    return keys.count;
}

-(void) migrateNSUserDefaultsToSharedDefaults {
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *standardDic = [standardDefaults dictionaryRepresentation];
    if([self count] == 0){
        for (NSString *key in standardDic) {
            NSObject *obj = [standardDefaults objectForKey:key];
            [self setObject:obj forKey:key];
        }
    }
}

-(NSString *) description {
    return [_userDefaultsCache description];
}

@end
