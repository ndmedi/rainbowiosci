/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ServicesManager.h"
#import "ServicesManager+Internal.h"
#import "DownloadManager.h"
#import "ContactsManagerService.h"
#import "ContactsManagerService+Internal.h"
#import "RoomsService+Internal.h"
#import "XMPPService.h"
#import "LoginManager+Internal.h"
#import "ApiUrlManagerService.h"
#import "DirectoryService.h"
#import "ConversationsManagerService+Internal.h"
#import "RTCService+Internal.h"
#import "Reachability.h"
#import "MyUser+Internal.h"
#import "SourcesManagerService.h"
#import "BotsService.h"
#import "Server+Internal.h"
#import "GroupsService+Internal.h"
#import "defines.h"
#import "NotificationsManager.h"
#import "NotificationsManager+Internal.h"
#import "CompaniesService+Internal.h"
#import "FileSharingService+Internal.h"
#import "RainbowUserDefaults.h"
#import "ConferencesManagerService+Internal.h"
#import "CallLogsService+Internal.h"
#import "ChannelsService+Internal.h"
#import "TelephonyService+Internal.h"

#define kSessionKeepAliveTimeOut 600

#if DEBUG
#define kEnteringInBackgroundTimer 50
#else
#define kEnteringInBackgroundTimer 60
#endif


#include "sys/types.h"
#include "sys/socket.h"
#include "ifaddrs.h"
#include "arpa/inet.h"

NSString *const kServicesManagerNewNetworkConnectionDetected = @"servicesManagerNewNetworkConnectionDetected";
NSString *const kServicesManagerNetworkConnectionDetected = @"servicesManagerNetworkConnectionDetected";
static NSString *customResourceName = nil;

static ServicesManager *singleton = nil;

@interface ServicesManager ()
@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) NSMutableArray *listChallenge;
@property (nonatomic, strong) LoginManager *loginManager;
@property (nonatomic, strong) DirectoryService *directoryService;
@property (nonatomic, strong) ContactsManagerService *contactsManagerService;
@property (nonatomic, strong) RTCService *rtcService;
@property (nonatomic, strong) RoomsService *roomsService;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property (nonatomic, strong) NSTimer* foregroundTimer;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
@property (nonatomic) UIBackgroundTaskIdentifier ongoingCheckTokenExpirationID;
@property (nonatomic) UIBackgroundTaskIdentifier enterInBackgroundLrtID;
@property (nonatomic, strong) NSTimer *enterInBackgroundTimer;
#endif
@property (nonatomic, strong) Reachability *reachability;
@property (nonatomic, strong) SourcesManagerService *sourcesManager;
@property (nonatomic) NetworkStatus lastNetworkStatus;
@property (nonatomic) NSString * lastIPAddress;
@property (nonatomic, strong) BotsService *botService;
@property (nonatomic, strong) GroupsService *groupsService;
@property (nonatomic, strong, readwrite) NotificationsManager *notificationsManager;
@property (nonatomic, strong) NSOperationQueue *internalQueue;
@property (nonatomic, strong) NSTimer *connectionTimer;
@property (nonatomic, strong, readwrite) CompaniesService *companiesService;
@property (nonatomic, strong, readwrite) FileSharingService *fileSharingService;
@property (nonatomic, strong, readwrite) CallLogsService *callLogsService;
@property (nonatomic, strong, readwrite) ChannelsService *channelsService;
@property (nonatomic, strong, readwrite) TelephonyService *telephonyService;
@property (nonatomic, strong) NSTimer *tryToReconnectNotificationTimer;
@property (nonatomic) BOOL isNetworkDisconnected;

@end

@implementation ServicesManager
@synthesize downloadManager = _downloadManager;

+(ServicesManager*) sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[ServicesManager alloc] init];
    });
    return singleton;
}

-(instancetype) init {
    self = [super init];
    if(self){
        _internalQueue = [NSOperationQueue new];
        _internalQueue.name = @"Services Manager internal queue";
        _internalQueue.maxConcurrentOperationCount = 1;
        
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        _ongoingCheckTokenExpirationID = UIBackgroundTaskInvalid;
#else
        [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self selector:@selector(onWillSleep:) name:NSWorkspaceWillSleepNotification object:nil];
        [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self selector:@selector(onDidWakeUp:) name:NSWorkspaceDidWakeNotification object:nil];
#endif
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerInternalDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailedToAuthenticate:) name:kLoginManagerDidFailedToAuthenticate object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRTCCall:) name:kTelephonyServiceDidRemoveCallNotification object:nil];
        
        _myUser = [[MyUser alloc] init];
        _apiUrlManagerService = [[ApiUrlManagerService alloc] initWithMyUser:_myUser];
        _downloadManager = [DownloadManager new];
        
        _myUser.downloadManager = _downloadManager;
        _myUser.apiURLManager = _apiUrlManagerService;
        
        _directoryService = [[DirectoryService alloc] initWithDownloadManager:_downloadManager apiUrlManagerService:_apiUrlManagerService];
        
        _contactsManagerService = [[ContactsManagerService alloc] initWithDownloadManager:_downloadManager myUser:_myUser apiUrlManagerService:_apiUrlManagerService];
        
        _myUser.contactsManagerService = _contactsManagerService;
        if(_myUser.haveMinimalInformationsToStartWithOutToken)
            [_contactsManagerService createMyRainbowContactForJid:_myUser.jid_im withCompanyId:_myUser.companyID withRainbowId:_myUser.userID companyName:_myUser.companyName];
        
        _xmppService = [[XMPPService alloc] initWithCertificateManager:_downloadManager contactManagerService:_contactsManagerService];
        
        _roomsService = [[RoomsService alloc] initWithDownloadManager:_downloadManager apiUrlManagerService:_apiUrlManagerService contactsManagerService:_contactsManagerService xmppService:_xmppService];
    
        _fileSharingService = [[FileSharingService alloc] initWithDownloadManager:_downloadManager apiURLManager:_apiUrlManagerService contactsManagerService:_contactsManagerService roomService:_roomsService myUser:_myUser];
        
        _xmppService.fileSharingService = _fileSharingService;
        _xmppService.roomsService = _roomsService;
        _myUser.xmppService = _xmppService;
        
//        _sourcesManager = [[SourcesManagerService alloc] initWithDownloadManager:_downloadManager myUser:_myUser apiUrlManagerService:_apiUrlManagerService];
        
        _conversationsManagerService = [[ConversationsManagerService alloc] initWithDownloadManager:_downloadManager apiUrlManagerService:_apiUrlManagerService contactsManagerService:_contactsManagerService roomsService:_roomsService xmppService:_xmppService fileSharingService:_fileSharingService];

        _loginManager = [[LoginManager alloc] initWithDownloadManager:_downloadManager withXmppService:_xmppService myUser:_myUser apiUrlManagerService:_apiUrlManagerService contactsManagerService:_contactsManagerService];
        
        _contactsManagerService.xmppService = _xmppService;
        _contactsManagerService.directoryService = _directoryService;
        
        _botService = [[BotsService alloc] initWithContactsManagerService:_contactsManagerService downloadManager:_downloadManager apiUrlManagerService:_apiUrlManagerService];
        
        _groupsService = [[GroupsService alloc] initWithDownloadManager:_downloadManager apiUrlManagerService:_apiUrlManagerService contactsManagerService:_contactsManagerService xmppService:_xmppService];
        
        _notificationsManager = [[NotificationsManager alloc] initWithConversationManager:_conversationsManagerService xmppService:_xmppService myUser:_myUser];
        _notificationsManager.servicesManager = self;
        
        _companiesService = [[CompaniesService alloc] initWithDownloadManager:_downloadManager apiUrlManagerService:_apiUrlManagerService xmppService:_xmppService myUser:_myUser contactManager:_contactsManagerService loginManager:_loginManager];
        
        _xmppService.companiesService = _companiesService;
        
        _rtcService = [[RTCService alloc] initWithDownloadManager:_downloadManager apiUrlManagerService:_apiUrlManagerService xmppService:_xmppService contactManagerService:_contactsManagerService myUser:_myUser apiManager:_apiUrlManagerService];
        _rtcService.servicesManager = self;
        
        _conferencesManagerService = [[ConferencesManagerService alloc] initWithDownloadManager:_downloadManager apiUrlManagerService:_apiUrlManagerService contactsManagerService:_contactsManagerService roomsService:_roomsService xmppService:_xmppService myUser:_myUser rtcService:_rtcService];
        _roomsService.conferenceManagerService = _conferencesManagerService;
        
        _callLogsService = [[CallLogsService alloc] initWithXMPPService:_xmppService myUser:_myUser];
        _channelsService = [[ChannelsService alloc] initWithXMPPService:_xmppService downloadManager:_downloadManager apiUrlManagerService:_apiUrlManagerService];
        _telephonyService = [[TelephonyService alloc] initWithXMPPService:_xmppService myUser:_myUser downloadManager:_downloadManager apiUrlManagerService:_apiUrlManagerService contactsManagerService:_contactsManagerService rtcService:_rtcService];
        
        _lastNetworkStatus = NotReachable;
        
        if(_myUser.server.serverHostname.length>0)
            _reachability = [Reachability reachabilityWithHostName:_myUser.server.serverHostname];
        else
            _reachability = [Reachability reachabilityWithHostName:@"apple.com"];
        [_reachability startNotifier];
        _lastIPAddress = [self getIPAddress];
        
        _loginManager.reachability = _reachability;
        _lastNetworkStatus = _reachability.currentReachabilityStatus;
        
        NSLog(@"lastNetworkStatus saved is %@", [Reachability stringForNetworkStatus:_lastNetworkStatus]);
        NSLog(@"lastIPAddress saved is %@", _lastIPAddress);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        
        [self registerDefaultSettings];
    }
    return self;
}

-(void) dealloc {
    [_internalQueue cancelAllOperations];
    _internalQueue = nil;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
#else
//    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self selector:@selector(onWillSleep:) name:NSWorkspaceWillSleepNotification object:nil];
//    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self selector:@selector(onDidWakeUp:) name:NSWorkspaceDidWakeNotification object:nil];
#endif
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kLoginManagerDidFailedToAuthenticate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidRemoveCallNotification object:nil];
    
    // release the services in reverse order of allocation.
    [_reachability stopNotifier];
    _reachability = nil;
    _contactsManagerService.directoryService = nil;
    _contactsManagerService.xmppService = nil;
    _loginManager = nil;
    _conversationsManagerService = nil;
    _sourcesManager = nil;
    _myUser.xmppService = nil;
    _xmppService.roomsService = nil;
    _xmppService = nil;
    _roomsService = nil;
    _myUser.contactsManagerService = nil;
    _contactsManagerService = nil;
    _directoryService = nil;
    _downloadManager = nil;
    _apiUrlManagerService = nil;
    _myUser = nil;
    _groupsService = nil;
    _notificationsManager.servicesManager = nil;
    _notificationsManager = nil;
    _rtcService.servicesManager = nil;
    _rtcService = nil;
    _companiesService = nil;
    _fileSharingService = nil;
    _contactsManagerService = nil;
    _channelsService = nil;
}

-(void) migrationDetectionFrom:(NSNumber *) previousAppVersion toVersion:(NSNumber *) currentAppVersion {
    if([previousAppVersion compare:@1.141371] == NSOrderedDescending){
        NSLog(@"Migration needed");
        // in version 1.41 we must force the full login to get the list of all rooms & ice servers
        _myUser.token = nil;
        [_xmppService forceNextConnectionToNotResumeTheStream];
    }
}

-(void) setActionToPerformHandler:(ServicesManagerDidConnectExtension)handler {
    if(!_loginManager.isConnected) {
        _actionToPerformHandler = handler;
    } else {
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:handler];
        [_internalQueue addOperation:op];
    }
}

#pragma mark - Keep the session up and running

-(void) configureForegroundTimer {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self configureForegroundTimer];
        });
        return;
    }
    
    BOOL isAppActive = YES;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    isAppActive = ([UIApplication sharedApplication].applicationState==UIApplicationStateActive);
#endif
    if (isAppActive) {
        [self cleanForegroundTimer];
        _foregroundTimer = [NSTimer timerWithTimeInterval:kSessionKeepAliveTimeOut target:self selector:@selector(checkTokenExpirationAndRenewIt) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_foregroundTimer forMode:NSDefaultRunLoopMode];
        NSLog(@"Foreground session timer SCHEDULED (%ld s).", (long)kSessionKeepAliveTimeOut);
    }
}

-(void) cleanForegroundTimer {
    if (_foregroundTimer) {
        [_foregroundTimer invalidate];
        _foregroundTimer = nil;
        NSLog(@"Foreground session timer CLEANED.");
    }
}

-(void) cleanConnectionTimer {
    if(_connectionTimer){
        [_connectionTimer invalidate];
        _connectionTimer = nil;
    }
}

-(void) cleanTryToReconnectTimer {
    NSLog(@"Clean timer");
    if(_tryToReconnectNotificationTimer){
        [_tryToReconnectNotificationTimer invalidate];
        _tryToReconnectNotificationTimer = nil;
    }
}

-(void) cleanEnterInBackgroundTimer {
    if(_enterInBackgroundTimer){
        [_enterInBackgroundTimer invalidate];
        _enterInBackgroundTimer = nil;
    }
}

-(void) internalCheckTokenExpirationDateAndRenewIt {
    BOOL needRenew = YES;
    // We have to check the token expiration date
    NSDate *currentDate = [NSDate date];
    if ([currentDate compare:_myUser.tokenExpireDate] == NSOrderedDescending) {
        NSLog(@"currentDate is later than token date, we need to renew the token");
    } else if ([currentDate compare:_myUser.tokenExpireDate] == NSOrderedAscending) {
        NSLog(@"currentDate is earlier than token date, no need to renew token");
        NSTimeInterval secs = [_myUser.tokenExpireDate timeIntervalSinceDate:currentDate];
        if(secs < kSessionKeepAliveTimeOut){
            NSLog(@"Delta between the dates is less than 10 min, so renew the token");
        } else
            needRenew = NO;
    } else {
        NSLog(@"dates are the same, we need to renew the token");
    }
    if(needRenew)
        [self renewToken];
    else {
        // Check if the xmpp connection is established
        if(_loginManager.loginDidSucceed){
            if(!_xmppService.isXmppConnected && [_myUser haveMinimalInformationsToStart])
                [_loginManager connectToXMPPWithUserName:_myUser.jid_im password:_myUser.jid_password];
            else
                [_loginManager connect];
        }
    }
}

-(void) renewToken {
    if(_loginManager.loginDidSucceed){
        if(!_loginManager.isConnected)
            [_loginManager connect];
        else
            [self endOngoingCheckTokenExpirationLongRunningTask];
    } else {
        if(_loginManager.autoLogin && _myUser.username.length > 0 && _myUser.password.length > 0)
            [_loginManager connect];
        else {
            NSLog(@"Login did Succeed is KO or autoLogin is OFF, we don't retry connecting");
            [self endOngoingCheckTokenExpirationLongRunningTask];
        }
    }
}

-(void) didLogin:(NSNotification *) notification {
    NSLog(@"ServiceManager did login");
     __weak __typeof__(self) weakSelf = self;
    NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"Performing operation, didConnectExtension");
        [_notificationsManager didConnectExtension];
        if(_actionToPerformHandler)
            _actionToPerformHandler();
        _actionToPerformHandler = nil;
        
        if(_rtcServiceActionToPerformHandler)
            _rtcServiceActionToPerformHandler();
        _rtcServiceActionToPerformHandler = nil;
        [weakSelf endOngoingCheckTokenExpirationLongRunningTask];
        [weakSelf configureForegroundTimer];
        [weakSelf cleanTryToReconnectTimer];
    }];
    [_internalQueue addOperation:op];
}

-(void) didLogout:(NSNotification *) notification {
    NSLog(@"ServiceManager did logout");
    [_internalQueue cancelAllOperations];
    [self endOngoingCheckTokenExpirationLongRunningTask];
    [self cleanForegroundTimer];
}

-(void) didLostConnection:(NSNotification *) notification {
    NSLog(@"ServiceManager did lostConnection");
    [_internalQueue cancelAllOperations];
    [self endOngoingCheckTokenExpirationLongRunningTask];
    [self cleanForegroundTimer];
}

-(void) didReconnect:(NSNotification *) notification {
    NSLog(@"ServiceManager did Reconnect");
    __weak __typeof__(self) weakSelf = self;
    NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"Performing operation, didConnectExtension");
        [_notificationsManager didConnectExtension];
        if(_actionToPerformHandler)
            _actionToPerformHandler();
        _actionToPerformHandler = nil;
        
        if(_rtcServiceActionToPerformHandler)
            _rtcServiceActionToPerformHandler();
        _rtcServiceActionToPerformHandler = nil;
        [weakSelf endOngoingCheckTokenExpirationLongRunningTask];
        [weakSelf configureForegroundTimer];
        [weakSelf cleanTryToReconnectTimer];
    }];
    NSLog(@"Adding operation in internal queue %@", op);
    [_internalQueue addOperation:op];
}

-(void) didFailedToAuthenticate:(NSNotification *) notification {
    NSLog(@"ServiceManager did failed to authenticate");
    [_internalQueue cancelAllOperations];
    [self endOngoingCheckTokenExpirationLongRunningTask];
    [self cleanForegroundTimer];
}

#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
-(void) applicationDidEnterBackground:(NSNotification*)notif {
    NSLog(@"application did enter background ! ");
    if(!_rtcService.hasActiveCalls){
        // Reset check and recover timer for foreground use.
        [self cleanForegroundTimer];
        [self cleanConnectionTimer];
        [self cleanTryToReconnectTimer];
    } else {
        NSLog(@"We have active calls so don't close the network sockets");
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self applicationWillResignActive:application];
        });
        return;
    }
    NSLog(@"[SESSMNGR] application Will resign active");
    if(_rtcService.calls.count > 0){
        NSLog(@"[SESSMNGR] Don't start long running task \"enter in background\" there is calls in progress");
        return;
    }
    // Start a long running task to keep the app active 1 more minute
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    UIApplication* app = [UIApplication sharedApplication];
    if (_enterInBackgroundLrtID == UIBackgroundTaskInvalid) {
        _enterInBackgroundLrtID = [app beginBackgroundTaskWithExpirationHandler:^{
            NSLog(@"**** [ServicesManager] [LRT] Enter in background long running task expired ! (terminating id %lu ) ***", (unsigned long)_enterInBackgroundLrtID);
            [self endEnterInBackgroundLongRunningTask];
        }];
        
        if (_enterInBackgroundLrtID != UIBackgroundTaskInvalid) {
            NSLog(@"************ [ServicesManager] [LRT] starting enter in background long running task (id:%lu)************", (unsigned long)_enterInBackgroundLrtID);
            _enterInBackgroundTimer = [NSTimer scheduledTimerWithTimeInterval:kEnteringInBackgroundTimer target:self selector:@selector(endEnterInBackgroundLongRunningTask:) userInfo:nil repeats:NO];
        } else {
            NSLog(@"[ServicesManager] [LRT] Enter in background long running can't be started !!!");
        }
    } else {
        NSLog(@"[ServicesManager] [LRT] enter in background long running task already running -> ignoring !  (id : %ld)", (unsigned long)_enterInBackgroundLrtID);
    }
#endif
}

-(void) endEnterInBackgroundLongRunningTask:(NSTimer *) timer {
    NSLog(@"Enter in background timer fire, closing socket");
    [_xmppService forceClosingConnection];
    [NSThread sleepForTimeInterval:5];
    [self endEnterInBackgroundLongRunningTask];
}

-(void) endEnterInBackgroundLongRunningTask {
    if(_enterInBackgroundLrtID != UIBackgroundTaskInvalid){
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        NSLog(@"************ [ServicesManager] [LRT] End of long running task \"enter in background\" %lu",(unsigned long)_enterInBackgroundLrtID);
        [[UIApplication sharedApplication] endBackgroundTask:_enterInBackgroundLrtID];
#endif
        _enterInBackgroundLrtID = UIBackgroundTaskInvalid;
    }
}

-(void) applicationDidBecomeActive:(NSNotification*)notif {
    NSLog(@"application did become active");
    
    [self checkTokenExpirationAndRenewIt];
    if(!_tryToReconnectNotificationTimer){
        NSLog(@"Add timer");
        _tryToReconnectNotificationTimer = [NSTimer timerWithTimeInterval:5 target:self selector:@selector(connectionTooLong:) userInfo:nil repeats:NO];
        [[NSRunLoop mainRunLoop] addTimer:_tryToReconnectNotificationTimer forMode:NSRunLoopCommonModes];
    }
    
    [self endEnterInBackgroundLongRunningTask];
    [self cleanEnterInBackgroundTimer];
}
#else
#pragma mark - Application lifecycle MacOS X
-(void) onWillSleep:(NSNotification*)notification {
    NSLog(@"App will sleep");
    [self cleanForegroundTimer];
}

-(void) onDidWakeUp:(NSNotification*)notification {
    NSLog(@"App did wake up");
}
#endif

// Long running task
-(void) checkTokenExpirationAndRenewIt {
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    UIApplication* app = [UIApplication sharedApplication];
    if (_ongoingCheckTokenExpirationID == UIBackgroundTaskInvalid) {
        _ongoingCheckTokenExpirationID = [app beginBackgroundTaskWithExpirationHandler:^{
            NSLog(@"**** [ServicesManager] check token expiration long running task expired ! (terminating id %lu ) ***", (unsigned long)_ongoingCheckTokenExpirationID);
            [app endBackgroundTask:_ongoingCheckTokenExpirationID];
            _ongoingCheckTokenExpirationID = UIBackgroundTaskInvalid;
        }];
        
        if (_ongoingCheckTokenExpirationID != UIBackgroundTaskInvalid) {
            NSLog(@"************ [ServicesManager] starting check token expiration long running task in background (id:%lu)************", (unsigned long)_ongoingCheckTokenExpirationID);
            
            [self cleanConnectionTimer];
            _connectionTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(connectionTooLong:) userInfo:nil repeats:NO];
            
            __weak __typeof__(self) weakSelf = self;
            NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^{
                [_xmppService sendPingToServer];
                [weakSelf internalCheckTokenExpirationDateAndRenewIt];
            }];
            [_internalQueue addOperation:op];
        } else {
            NSLog(@"!!!!!! CANNOT ACQUIRE BackgroundTaskID !!!!! :'(");
        }
    } else {
        NSLog(@"[ServicesManager] ongoing check token expiration long running background check already going -> ignoring !  (id : %ld)", (unsigned long)_ongoingCheckTokenExpirationID);
    }
#endif
}

-(void) endOngoingCheckTokenExpirationLongRunningTask {
    if(_ongoingCheckTokenExpirationID != UIBackgroundTaskInvalid){
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        NSLog(@"************ [ServicesManager] End of long running task check token expiration %lu",(unsigned long)_ongoingCheckTokenExpirationID);
        [[UIApplication sharedApplication] endBackgroundTask:_ongoingCheckTokenExpirationID];
#endif
        _ongoingCheckTokenExpirationID = UIBackgroundTaskInvalid;
    }
    [self cleanConnectionTimer];
}

-(void) connectionTooLong:(NSTimer *) timer {
    if(!_loginManager.isConnected && (_myUser.username && _myUser.password)){
        NSLog(@"Connection too long");
        if (_lastNetworkStatus == NotReachable ) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerTryToReconnect object:nil];
        }
    
    }
}

-(void) reachabilityChanged:(NSNotification *) notification {
    Reachability* curReach = [notification object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    NSLog(@"reachability changed");
    
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground){
        NSLog(@"Reachability changed but application is in background so we don't do anythings");
        if([curReach currentReachabilityStatus] == NotReachable) {
            _isNetworkDisconnected = YES;
        }        
        return;
    }
  
    if([curReach currentReachabilityStatus] == NotReachable) {
        // no internet connection
        _isNetworkDisconnected = YES;
        _lastIPAddress = nil;
        if([curReach currentReachabilityStatus] != _lastNetworkStatus) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kLoginManagerDidLostConnection object:nil];
        }
       
    }
    else if(![curReach connectionRequired]){
        _isNetworkDisconnected = NO;
        // If we don't have the lastIPAddress that mean we never connect
        if(!_lastIPAddress || [curReach currentReachabilityStatus] != _lastNetworkStatus){
            NSLog(@"Reachability has changed, checking connections");
           
           if (![[self getIPAddress] isEqualToString:_lastIPAddress]) {
               if (!_loginManager.isConnected) {
                   NSLog(@"[loginManager] is not Connected ");
                   [self checkTokenExpirationAndRenewIt];
               }
               else {
                   // Network is not the same try to connect on the new network
                   NSLog(@"[loginManager] is Connected ");
                   [_xmppService forceClosingConnection];
               }
               
               _lastIPAddress = [self getIPAddress];
               NSLog(@"lastIPAddress saved is %@", _lastIPAddress);
               [[NSNotificationCenter defaultCenter] postNotificationName:kServicesManagerNewNetworkConnectionDetected object:curReach];
           }
           
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kServicesManagerNetworkConnectionDetected object:[Reachability stringForNetworkStatus:curReach.currentReachabilityStatus]];
    }
    _lastNetworkStatus = curReach.currentReachabilityStatus;
    
    NSLog(@"lastNetworkStatus saved is %@", [Reachability stringForNetworkStatus:_lastNetworkStatus]);
}

-(void) registerDefaultSettings {
    [_myUser registerDefaultsSettings];
}

-(void) didRemoveRTCCall:(NSNotification *) notification {
    // Check in this notification if there is still an active connection while the application is in background, if it's the case we have to close (forceClose) it to allow reception of push again.
    
    [_myUser didRemoveCall:notification];
    [NSThread sleepForTimeInterval:1];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(!_rtcService.hasActiveCalls && [UIApplication sharedApplication].applicationState != UIApplicationStateActive){
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
            [self applicationWillResignActive:[UIApplication sharedApplication]];
#endif
        }
    });
}

-(void)setAppID:(NSString *)appID secretKey:(NSString *)secretKey {
    _myUser.appToken = nil;
    _myUser.appID = appID;
    _myUser.secretKey = secretKey;
    _xmppService.applicationID = appID;
}

- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                // Check if interface is pdp_ip0 which is the 3G/4G connection on the iPhone
                else if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"pdp_ip0"]) {
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    // Check if we got an error while fetching the IP address
    if([address isEqualToString:@"error"])
        address = nil;
    return address;
    
}

+(void) setCustomResourceName:(NSString *) customResourceName {
    customResourceName = customResourceName;
}
@end
