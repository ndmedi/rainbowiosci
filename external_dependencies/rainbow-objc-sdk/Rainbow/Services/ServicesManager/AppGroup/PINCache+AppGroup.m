/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "PINCache.h"
#import "Tools.h"

@implementation PINCache (AppGroup)

- (instancetype)initWithName:(NSString *)name {
    NSString *cacheDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSString *rainbowAppGroupPath = [Tools rainbowAppGroupPath];
    NSString *cacheGroupDirectory = cacheDirectory;
    // Can be nil if there is no suiteName define so we use the default cache directory in that case
    if(rainbowAppGroupPath){
        cacheGroupDirectory = [rainbowAppGroupPath stringByAppendingString:@"/Library/Caches"];
        NSArray *cacheGroupContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cacheGroupDirectory error:nil];
        NSArray *defaultCacheContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cacheDirectory error:nil];
        if(cacheGroupContents.count == 0 && defaultCacheContents.count > 0){
            // Need to move content of default cache directory into the group directory
            for (NSString *content in defaultCacheContents) {
                if([content containsString:@"com.pinterest"]){
                    NSError *error = nil;
                    NSString *source = [NSString stringWithFormat:@"%@/%@",cacheDirectory, content];
                    NSString *dest = [NSString stringWithFormat:@"%@/%@",cacheGroupDirectory, content];
                    [[NSFileManager defaultManager] moveItemAtPath:source toPath:dest error:&error];
                    NSLog(@"Error while moving cache content %@", error);
                }
            }
        }
        
        if ([name isEqualToString:@"userDefaults"]) {//return documents path for userdefaults
            NSString *userDefaultsDirectory = [NSString stringWithFormat:@"/%@.%@", PINDiskCachePrefix, name];
            NSString *userDefaultsCachePath = [cacheGroupDirectory stringByAppendingString:userDefaultsDirectory];
            NSString *documentsGroupDirectory = [rainbowAppGroupPath stringByAppendingString:@"/Documents"];
            NSString *userDefaultsDocumentsPath = [documentsGroupDirectory stringByAppendingString:userDefaultsDirectory];
            if ([[NSFileManager defaultManager] fileExistsAtPath:userDefaultsCachePath]) {//move userdefaults into documents
                NSError *error = nil;
                [[NSFileManager defaultManager] moveItemAtPath:userDefaultsCachePath toPath:userDefaultsDocumentsPath error:&error];
                NSLog(@"Error while moving userDefaults from cache to Documents directory %@", error);
            }
            return [self initWithName:name rootPath:documentsGroupDirectory];
        }
    }
    
    return [self initWithName:name rootPath:cacheGroupDirectory];
}
@end
