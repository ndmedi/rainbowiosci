/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ServicesManager.h"
#import "XMPPService.h"
#import "MyUser.h"

FOUNDATION_EXPORT NSString *const  kServicesManagerNewNetworkConnectionDetected;

@interface ServicesManager ()
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong, readwrite) MyUser *myUser;

-(void) checkTokenExpirationAndRenewIt;
// Method used to start a long runing task of 1 min that will allow the app to "finish" his work
- (void)applicationWillResignActive:(UIApplication *)application;
@property (nonatomic, copy) ServicesManagerDidConnectExtension rtcServiceActionToPerformHandler;
@end
