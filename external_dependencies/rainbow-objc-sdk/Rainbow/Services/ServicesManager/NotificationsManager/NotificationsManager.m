/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NotificationsManager.h"
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
#import <UIKit/UIKit.h>
#import "UIUserNotificationSettings+Extension.h"
#endif
#import "ServicesManager.h"
#import "NotificationsManager+Internal.h"
#import "defines.h"

#import <PushKit/PushKit.h>
#import <CallKit/CallKit.h>
#import "RTCService+Internal.h"
#import <UserNotifications/UserNotifications.h>

NSString *const kNotificationsManagerHandleNotification = @"handleNotification";

typedef void (^NotificationsManagerActionToPerformAfterConnectionHandler)(void);

@interface NotificationsManager () <PKPushRegistryDelegate>
@property (nonatomic, strong) ConversationsManagerService* conversationManager;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, copy) NotificationsManagerActionToPerformAfterConnectionHandler actionToPerformHandler;
@property (nonatomic, strong) NSCondition *operationLock;
@property (nonatomic, strong) PKPushRegistry *voipRegistry;
@property (nonatomic, strong) MyUser *myUser;
@end

@implementation NotificationsManager
-(instancetype) initWithConversationManager:(ConversationsManagerService *) conversationManager xmppService:(XMPPService *) xmppService myUser:(MyUser *) myUser {
    self = [super init];
    if(self){
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
#endif
        
        _conversationManager = conversationManager;
        _xmppService = xmppService;
        _operationLock = [NSCondition new];
        
        _voipRegistry = [[PKPushRegistry alloc] initWithQueue:nil];
        _voipRegistry.delegate = self;
        _voipRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
        
        _myUser = myUser;
    }
    return self;
}

-(void) dealloc {
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidFinishLaunchingNotification object:nil];
#endif
    _conversationManager = nil;
    _xmppService = nil;
    _actionToPerformHandler = nil;
    _operationLock = nil;
    _myUser = nil;
}

#pragma mark - Application lifecycle iOS
-(void) applicationDidFinishLaunching:(NSNotification*) notification {
    // We don't have launch options when answering to a notification when application is launched
    // The handleNotification delegate is triggered instead
    
    NSLog(@"Application did finish launching");
    NSDictionary *launchOptions = notification.userInfo;
    if(launchOptions && [launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"] != nil){
        NSLog(@"UIApplicationLaunchOptionsRemoteNotificationKey found, that means we have been started from a notification, we will have things to do after connection.");
        
        // Extract all info from notification
        NSDictionary *userInfo = [launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
        
        //NSDictionary *customUserInfo = [self extractUsableInfoFromNotificationUserInfo:userInfo];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self handleNotificationWithIdentifier:nil withUserInfo:userInfo withResponseInformation:nil];
        });
    }
}

-(void) didConnectExtension {
    if(_actionToPerformHandler)
        _actionToPerformHandler();
    
    _actionToPerformHandler = nil;
}

-(void) handleNotificationWithIdentifier:(NSString *)identifier withUserInfo:(NSDictionary *)userInfo withResponseInformation:(NSDictionary *) responseInfo {
    
    NSDictionary *customUserInfo = [self extractUsableInfoFromNotificationUserInfo:userInfo];
    
    NSString *from = [customUserInfo valueForKey:@"from"];
    if(!from){
        // Try to extract info like it was a local notification
        customUserInfo = [self extractUsableInfoFromLocalNotificationUserInfo:userInfo];
    }
    
    [_conversationManager forceRefreshCache];

    NSString *category = [customUserInfo valueForKey:@"category"];
    __weak __typeof__(_conversationManager) weakConversationManager = _conversationManager;
    __weak __typeof__(_xmppService) weakXmppService = _xmppService;
    __weak __typeof__(_servicesManager.rtcService) weakRTCService = _servicesManager.rtcService;
    // Notification has been tapped
    if([category isEqualToString:@"im_category"] && !identifier){
        // we have to open a conversation after connection
        
        _actionToPerformHandler = ^{
            NSLog(@"Invoke connection completion handler");
            [weakXmppService createAndDispatchMessageFromDictionary:customUserInfo];
            // We don't need to do a startConversation here because the notification is also handled in view controller
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationsManagerHandleNotification object:customUserInfo];
        };
    }
    
    // The user answer from the notification
    if([identifier isEqualToString:@"inline-reply"]) {
        // We have to send the message after connection is established
        NSString *message = [responseInfo valueForKey:UIUserNotificationActionResponseTypedTextKey];
        
        __weak __typeof__(_operationLock) weakLock = _operationLock;
        __weak __typeof__(_servicesManager) weakServiceManager = _servicesManager;
        
        __block BOOL hasCompletedAction = NO;
        _actionToPerformHandler = ^{
            NSString *theFrom = [customUserInfo valueForKey:@"from"];
            NSLog(@"Invoke connection completion handler");
            NSString *messageType = [customUserInfo valueForKey:@"messageType"];
            if([messageType isEqualToString:@"groupchat"])
                theFrom = [customUserInfo valueForKey:@"roomJid"];
            Peer *thePeer = [weakXmppService peerFromJidString:theFrom];
            [weakXmppService createAndDispatchMessageFromDictionary:customUserInfo];
            [weakConversationManager startConversationWithPeer:thePeer withCompletionHandler:^(Conversation *conversation, NSError *error) {
                [weakConversationManager markAsReadByMeAllMessageForConversation:conversation];
                [weakConversationManager sendMessage:message fileAttachment:nil to:conversation completionHandler:^(Message *message, NSError *error) {
                    if(!error){
                        NSLog(@"The following message has been send %@", message);
                    } else
                        NSLog(@"We got an error while sending the message");
                    
                    [weakServiceManager applicationWillResignActive:nil];
                    
                    [weakLock lock];
                    hasCompletedAction = YES;
                    [weakLock signal];
                    [weakLock unlock];
                } attachmentUploadProgressHandler:nil];
            }];
            
            [weakLock lock];
            while (!hasCompletedAction) {
                [weakLock wait];
            }
            [weakLock unlock];
        };
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        if(_servicesManager.loginManager.isConnected){
            if(_actionToPerformHandler){
                _actionToPerformHandler();
                _actionToPerformHandler = nil;
            }
        } else {
            NSLog(@"Check token expiration");
            [_servicesManager checkTokenExpirationAndRenewIt];
        }
    }
    
    // TODO : Implement one day all other kind of notification possible
    /*
    NSString *type = [userInfo valueForKey:@"type"];
    if([identifier isEqualToString:@"decline_invitation"]) {
        if ([type isEqualToString:@"user"]) {
            Contact *contact = [[ServicesManager sharedInstance].contactsManagerService getContactWithJid:from];
            [[ServicesManager sharedInstance].contactsManagerService refuseSubscriptionFromContact:contact];
            
        } else if ([type isEqualToString:@"room"]) {
            Room *room = [[ServicesManager sharedInstance].roomsService getRoomByJid:from];
            [[ServicesManager sharedInstance].roomsService declineInvitation:room];
        }
    }
    if([identifier isEqualToString:@"accept_invitation"]){
        if ([type isEqualToString:@"user"]) {
            Contact *contact = [[ServicesManager sharedInstance].contactsManagerService getContactWithJid:from];
            [[ServicesManager sharedInstance].contactsManagerService acceptSubscriptionFromContact:contact];
            
        } else if ([type isEqualToString:@"room"]) {
            Room *room = [[ServicesManager sharedInstance].roomsService getRoomByJid:from];
            [[ServicesManager sharedInstance].roomsService acceptInvitation:room];
        }
    }
    if([identifier isEqualToString:@"accept_c2c"]) {
        // If we arrive here, the user already accepted to call,
        // don't show the prompt and start the call directly with tel://
        NSLog(@"Accepting clickToCall for %@", [userInfo valueForKey:@"phoneNumber"]);
        NSString *phoneNumber = [userInfo valueForKey:@"phoneNumber"];
        NSString *telWithoutSpaces = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *numberURL = [NSString stringWithFormat:@"tel://%@", telWithoutSpaces];
        // Cannot be called on mainthread in this function.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:numberURL]];
        });
    }
    if([identifier isEqualToString:@"decline_c2c"]) {
        NSLog(@"Declining clickToCall for %@", [userInfo valueForKey:@"phoneNumber"]);
        // nothing to do..
    }
    */
}

-(NSDictionary *) extractUsableInfoFromNotificationUserInfo:(NSDictionary *) notificationUserInfo {
    NSString *from = notificationUserInfo[@"last-message-sender"];
    NSString *messageType = notificationUserInfo[@"last-message-type"];
    NSString *roomJid = notificationUserInfo[@"room-jid"];
    NSString *stamp = notificationUserInfo[@"stamp"];
    NSString *firstLastName = notificationUserInfo[@"first-last-name"];
    if([messageType isEqualToString:@"groupchat"]){
        from = [NSString stringWithFormat:@"%@/%@",notificationUserInfo[@"room-jid"], notificationUserInfo[@"last-message-sender"]];
    }
    
    // Prevent dict creation from crash if server send bad stuff.. (or if we handle it bad)
    if (!notificationUserInfo[@"aps"][@"category"] ||
        !from ||
        !notificationUserInfo[@"last-message-body"] ||
        !notificationUserInfo[@"message-id"]) {
        return nil;
    }
    
    NSMutableDictionary *customUserInfo = [NSMutableDictionary dictionaryWithDictionary: @{@"category": notificationUserInfo[@"aps"][@"category"], @"from": from, @"messageBody":notificationUserInfo[@"last-message-body"], @"messageID":notificationUserInfo[@"message-id"]}];
    if(messageType.length >0)
        [customUserInfo setObject:messageType forKey:@"messageType"];
    if(roomJid.length > 0)
        [customUserInfo setObject:roomJid forKey:@"roomJid"];
    if(stamp.length > 0)
        [customUserInfo setObject:stamp forKey:@"stamp"];
    
    if(firstLastName.length > 0)
        [customUserInfo setObject:firstLastName forKey:@"first-last-name"];
    
    return customUserInfo;
}

-(NSDictionary *) extractUsableInfoFromLocalNotificationUserInfo:(NSDictionary *) userInfo {
    NSString *from = userInfo[@"from"];
    NSString *type = userInfo[@"type"];
    NSString *category = userInfo[@"category"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"from":from,@"messageType":type,@"category":category}];
    
    if([type isEqualToString:@"groupchat"]){
        [dic setObject:from forKey:@"roomJid"];
    }
    
    return dic;
}

#pragma mark - Notifications
-(void) didReceiveNotificationWithUserInfo:(NSDictionary *) userInfo {
    NSLog(@"[NotificationsManager] didReceiveNotificationWithUserInfos %@", userInfo);
    [_conversationManager forceRefreshCache];
    NSDictionary *customUserInfo = [self extractUsableInfoFromNotificationUserInfo:userInfo];
    Message *pushedMessage = [_xmppService createAndDispatchMessageFromDictionary:customUserInfo];
    NSLog(@"[NotificationsManager] create messages from push informations %@", pushedMessage);
    // We don't need to do a startConversation here because the notification is also handled in view controller
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationsManagerHandleNotification object:customUserInfo];
}

-(void) saveNotificationWithUserInfo:(NSDictionary *) userInfo {
    __block UIBackgroundTaskIdentifier backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask: backgroundTask];
        backgroundTask = UIBackgroundTaskInvalid;
    }];
    
    NSLog(@"[NotificationsManager] saveNotificationWithUserInfo");
    //[_conversationManager forceRefreshCache];
    NSDictionary *customUserInfo = [self extractUsableInfoFromNotificationUserInfo:userInfo];
    Message *message = [_xmppService createAndDispatchMessageFromDictionary:customUserInfo];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [_conversationManager forceRefreshCache];
        [_conversationManager simulateDidReceiveMessage:message];
        if (backgroundTask != UIBackgroundTaskInvalid) {
            [[UIApplication sharedApplication] endBackgroundTask: backgroundTask];
            backgroundTask = UIBackgroundTaskInvalid;
        }
    });    
}

#pragma mark - PushKit delegates
- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(PKPushType)type {
    if([registry isEqual:_voipRegistry]){
        NSLog(@"[PUSHKIT] didUpdatePushCredentials %@ for Type %@", credentials.token, type);
        [[NSNotificationCenter defaultCenter] postNotificationName:kPushKitDidUpdatePushCredentialsNotification object:credentials.token];
        
    }
}

- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(PKPushType)type {
    if([registry isEqual:_voipRegistry]) {
        NSLog(@"[PUSHKIT] didReceiveIncomingPushWithPayload %@", payload.dictionaryPayload);
        
        // Handle 4 msg types :
        /*  {
                "aps":{},
                "type": "propose" | "accept" | "reject" | "retract",
                "sessionID": "1234567890",
                "from": "bareJID@domain.com",
                "resource": "mobile_ios_7124302D-27FE-4CCD-AF8C-B5707C93920E",
                "medias": ["audio", "video"],
            }
         */
        
        // Payload shoud contains :
        // - Jingle session ID
        // - JID of caller
        // - Resource of the caller
        // - medias
        
        // {"aps":{}, "type":"propose", "sessionID":"4236756061", "medias":["audio"], "from": "c7cdc03d4cd64550a97b641d7681fede@jerome-all-in-one-dev-1.opentouch.cloud", "resource": "mobile_ios_7124302D-27FE-4CCD-AF8C-B5707C93920E"}
        
        if(!_myUser.isAllowedToUseWebRTCMobile){
            NSLog(@"User is not allowed to use webrtc on mobile");
            return;
        }
        
        NSString *type = [payload.dictionaryPayload objectForKey:@"type"];
        if (!type || [type length] == 0) {
            NSLog(@"Error: no 'type' value in PushKit payload");
            return;
        }
        
        NSString *sessionID = [payload.dictionaryPayload objectForKey:@"sessionID"];
        if (!sessionID || [sessionID length] == 0) {
            NSLog(@"Error: no 'sessionID' value in PushKit payload");
            return;
        }
        
        NSString *from = [payload.dictionaryPayload objectForKey:@"from"];
        if (!from || [from length] == 0) {
            NSLog(@"Error: no 'from' value in PushKit payload");
            return;
        }
        
        NSString *resource = [payload.dictionaryPayload objectForKey:@"resource"];
        if (!resource || [resource length] == 0) {
            NSLog(@"Error: no 'resource' value in PushKit payload");
            return;
        }
        
        NSArray *medias = [payload.dictionaryPayload objectForKey:@"medias"];
        NSMutableArray<NSString *> * mediasList = [NSMutableArray array];
        for (NSString *media in medias) {
            [mediasList addObjectsFromArray:[media componentsSeparatedByString:@","]];
        }

        if ([type isEqualToString:@"propose"] && (!mediasList || [mediasList count] == 0)) {
            NSLog(@"Error: no 'medias' values in PushKit payload, when type is 'propose'");
            return;
        }
        
        Contact *contact = [_servicesManager.contactsManagerService getContactWithJid:from];
        
        if (!contact && [from containsString:@"mp_"]) {
            contact = [_servicesManager.contactsManagerService createPeerWithJid:from];
        }
        
        if (!contact) {
            NSLog(@"Error: unknown contact with JID %@", from);
            return;
        }
        
        if ([type isEqualToString:@"propose"]) {
            NSLog(@"Propose action");
            [_servicesManager.rtcService didReceiveProposeMsg:sessionID withMedias:mediasList from:contact resource:resource];
            
        } else if ([type isEqualToString:@"accept"]) {
            NSLog(@"Accept action");
            [_servicesManager.rtcService didReceiveAcceptMsg:sessionID];
            
        } else if ([type isEqualToString:@"reject"]) {
            NSLog(@"Reject action");
            [_servicesManager.rtcService didReceiveRejectMsg:sessionID from:contact resource:resource];
            
        } else if ([type isEqualToString:@"retract"]) {
            NSLog(@"Retract action");
            [_servicesManager.rtcService didReceiveRetractMsg:sessionID from:contact resource:resource];
            
        } else {
            NSLog(@"Unknown PushKit payload type.");
        }
    }
}


#pragma mark - push notifications
-(void) registerForUserNotificationsSettingsWithCompletionHandler:(void (^)(BOOL, NSError * _Nullable))completionHandler {
    // Inline answer to message
    UNTextInputNotificationAction *replyAction = [UNTextInputNotificationAction actionWithIdentifier:@"inline-reply" title:NSLocalizedString(@"Reply", nil) options:UNNotificationActionOptionAuthenticationRequired textInputButtonTitle:NSLocalizedString(@"Send", nil) textInputPlaceholder:@""];
    UNNotificationCategory *replyUserNotificationCategory = [UNNotificationCategory categoryWithIdentifier:@"im_category" actions:@[replyAction] intentIdentifiers:@[@"inline-reply"] options:UNNotificationCategoryOptionNone];
    
    // Accept / Decline invitations
    UNNotificationAction *declineInvitationAction = [UNNotificationAction actionWithIdentifier:@"decline_invitation" title:NSLocalizedString(@"Decline", nil) options:UNNotificationActionOptionAuthenticationRequired];
    UNNotificationAction *acceptInvitationAction = [UNNotificationAction actionWithIdentifier:@"accept_invitation" title:NSLocalizedString(@"Accept", nil) options:UNNotificationActionOptionAuthenticationRequired];
    UNNotificationCategory *invitationUserNotificationCategory = [UNNotificationCategory categoryWithIdentifier:@"invitation_category" actions:@[acceptInvitationAction, declineInvitationAction] intentIdentifiers:@[@"decline_invitation", @"accept_invitation"] options:UNNotificationCategoryOptionNone];
    
    // Accept / Decline click to call
    UNNotificationAction *declineC2CAction = [UNNotificationAction actionWithIdentifier:@"decline_c2c" title:NSLocalizedString(@"Decline", nil) options:UNNotificationActionOptionNone];
    // ! use a forground notification here !
    UNNotificationAction *acceptC2CAction = [UNNotificationAction actionWithIdentifier:@"accept_c2c" title:NSLocalizedString(@"Accept", nil) options:UNNotificationActionOptionNone];
    UNNotificationCategory *C2CNotificationCategory = [UNNotificationCategory categoryWithIdentifier:@"c2c_category" actions:@[acceptC2CAction, declineC2CAction] intentIdentifiers:@[@"decline_c2c", @"accept_c2c"] options:UNNotificationCategoryOptionNone];
    
     UNNotificationAction *acceptCallAction = [UNNotificationAction actionWithIdentifier:@"accept_call" title:NSLocalizedString(@"Accept", nil) options:UNNotificationActionOptionNone];
     UNNotificationAction *declineCallAction = [UNNotificationAction actionWithIdentifier:@"decline_call" title:NSLocalizedString(@"Decline", nil) options:UNNotificationActionOptionNone];
    
     UNNotificationCategory *CallNotificationCategory = [UNNotificationCategory categoryWithIdentifier:@"call_category" actions:@[acceptCallAction, declineCallAction] intentIdentifiers:@[@"decline_call", @"accept_call"] options:UNNotificationCategoryOptionNone];
    
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center setNotificationCategories:[NSSet setWithObjects:replyUserNotificationCategory, invitationUserNotificationCategory, C2CNotificationCategory, CallNotificationCategory, nil]];
    
    if(completionHandler == nil)
        completionHandler = ^(BOOL granted, NSError * _Nullable error) { };
    
    UNAuthorizationOptions authOptions =
    UNAuthorizationOptionAlert
    | UNAuthorizationOptionSound
    | UNAuthorizationOptionBadge;
    [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:completionHandler];
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

-(void) didRegisterForRemoteNotificationsWithDeviceToken:(NSData *) deviceToken {
    _xmppService.pushToken = deviceToken;
}

@end
