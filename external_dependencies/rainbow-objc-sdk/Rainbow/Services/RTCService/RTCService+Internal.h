/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import <CallKit/CallKit.h>
#import "RTCService.h"
#import "ServicesManager+Internal.h"

@class DownloadManager;
@class ApiUrlManagerService;
@class XMPPService;
@class ContactsManagerService;
@class MyUser;
@class ApiUrlManagerService;

typedef void (^MediaPillarDataCompletionHandler) (NSString *mediaPillarJid, NSString *rainbowPhoneNumber, NSString *prefix, NSError *error);

@interface RTCService () <XMPPServiceRTCDelegate>
@property (nonatomic, weak) ServicesManager *servicesManager;
@property (nonatomic, strong) ApiUrlManagerService *apiURLManager;

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService xmppService:(XMPPService *) xmppService contactManagerService:(ContactsManagerService *) contactsManager myUser:(MyUser *) myUser apiManager:(ApiUrlManagerService *) apiManager;

-(void) hangupCallForPublisherRainbowID:(NSString *) rainbowID;

// These maps the JingleMessageInitiation delegates
-(void) didReceiveProposeMsg:(NSString *) sessionID withMedias:(NSArray<NSString *> *) medias from:(Peer *) peer resource:(NSString *) resource;
-(void) didReceiveRetractMsg:(NSString *) sessionID from:(Peer *) peer resource:(NSString *) resource;
-(void) didReceiveAcceptMsg:(NSString *) sessionID;
-(void) didReceiveProceedMsg:(NSString *) sessionID from:(Peer *) peer resource:(NSString *) resource;
-(void) didReceiveRejectMsg:(NSString *) sessionID from:(Peer *) peer resource:(NSString *) resource;

// These maps the Jingle delegates
-(void) didReceiveSessionInitiate:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource;
-(void) didReceiveSessionAccept:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource;
-(void) didReceiveSessionTerminate:(NSString *) sessionID from:(Peer *) peer;
-(void) didReceiveTransportInfo:(NSString *) sessionID candidate:(NSDictionary *)candidate from:(Peer *) peer;
-(void) didReceiveTransportAccept:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer;
-(void) didReceiveTransportReplace:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource;
-(void) didReceiveContentAdd:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource;
-(void) didReceiveContentModify:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource;
-(void) didReceiveContentRemove:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource;
-(void) didReceiveContentAccept:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource;

//
-(void) didReceiveHoldJingleMessage:(NSString *) sessionID;
-(void) didReceiveUnholdJingleMessage:(NSString *) sessionID;

+(NSString *) stringForRTCIceGatheringState:(RTCIceGatheringState) iceGatheringState;
+(NSString *) stringForRTCSignalingState:(RTCSignalingState) signalingState;
+(NSString *) stringForRTCIceConnectionState:(RTCIceConnectionState) iceConnectionState;

-(void) webRTCGatewayRegisterRequest:(NSString *_Nonnull)gatewayJid number:(NSString *_Nonnull)number displayName:(NSString *_Nullable)name secret:(NSString *_Nullable)secret;
-(void) webRTCGatewayUnRegisterRequest:(NSString *_Nonnull)number;
-(void) fetchMediaPillarDataWithCompletionHandler:(MediaPillarDataCompletionHandler) completionHandler;
-(void) acceptMediaPillarIncomingCall:(RTCCall *)rtcCall withFeatures:(RTCCallFeatureFlags)features;
-(RTCCall *) getCallByCallId:(NSUUID *)callId;

@end
