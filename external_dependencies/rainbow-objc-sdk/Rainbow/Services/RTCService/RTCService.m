/*
 * Rainbow
 *
 * Copyright (c) 2016-2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <AVFoundation/AVFoundation.h>
#import "RTCService+Internal.h"
#import <WebRTC/WebRTC.h>

#import "XMPPService.h"
#import "XMPPFramework.h"
#import "ContactsManagerService.h"
#import "NSData+JSON.h"
#import "LoginManager+Internal.h"
#import "Tools.h"

#import "RTCCall+Internal.h"
#import <objc/runtime.h>

#import <CallKit/CallKit.h>
#import "UIDevice+VersionCheck.h"
#import "RTCSessionDescription+PreferredCodec.h"
#import <Intents/Intents.h>
#import "Reachability.h"
#import "PINCache.h"
#import "NSDate+Utilities.h"
#import "UserSettings.h"
#import "Peer+Internal.h"
#import "NSDictionary+JSONString.h"
#import "Call+Internal.h"
#import "XMPPIQ+XEP_0167.h"

typedef void (^RTCServicePrepareOfferCompletionHandler)(RTCSessionDescription *offer);
typedef void (^RTCServicePrepareAnswerCompletionHandler)(RTCSessionDescription *answer);
typedef void (^WebrtcMetricsComplionHandler) (NSError *error);
typedef void (^WebrtcMetricsCreateComplionHandler) (NSString * webRTCMetricsID, NSError *error);

NSString *const kRTCServiceDidAddLocalVideoTrackNotification = @"RTCServiceDidAddLocalVideoTrack";
NSString *const kRTCServiceDidAddCaptureSessionNotification = @"kRTCServiceDidAddCaptureSessionNotification";
NSString *const kRTCServiceDidAddRemoteVideoTrackNotification = @"RTCServiceDidAddRemoteVideoTrack";
NSString *const kRTCServiceDidRemoveLocalVideoTrackNotification = @"RTCServiceDidRemoveLocalVideoTrack";
NSString *const kRTCServiceDidRemoveRemoteVideoTrackNotification = @"RTCServiceDidRemoveRemoteVideoTrack";

NSString *const kRTCServiceDidAllowMicrophoneNotification = @"RTCServiceDidAllowMicrophone";
NSString *const kRTCServiceDidRefuseMicrophoneNotification = @"RTCServiceDidRefuseMicrophone";

NSString *const kRTCServiceDidChangeRecordingCallStateNotification = @"RTCServiceDidChangeRecordingCall";
NSString *const kRTCServiceDidReceiveIncomingMpCall = @"RTCServiceDidReceiveIncomingMpCall";
NSString *const kRTCServiceDidReceiveHangupCall = @"kRTCServiceDidReceiveHangupCall";

// REMOVE/change ?
NSString *const kRTCServiceCallStatsNotification = @"RTCServiceCallStats";

// In seconds
#define kHangupIfNoAnswerAfter 45


@interface RTCService () <RTCPeerConnectionDelegate, CXProviderDelegate, RTCAudioSessionDelegate>

// Other SDK services we'll use
@property(nonatomic, strong) DownloadManager *downloadManager;
@property(nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property(nonatomic, strong) XMPPService *xmppService;
@property(nonatomic, strong) ContactsManagerService *contactsManager;
@property(nonatomic, strong) MyUser *myUser;

// RTC stack
@property(nonatomic, strong) RTCPeerConnectionFactory *factory;
@property(nonatomic, strong) NSMutableArray<RTCCall *> *calls;
@property(nonatomic, strong) NSMutableArray<NSDictionary *> *iceServers;
@property(nonatomic, strong) NSTimer *statsTimer;

// CallKit
@property(nonatomic, strong) CXProvider *provider;
@property(nonatomic, strong) CXCallController *callController;

@property (nonatomic) BOOL isSpeakerEnabled;
@property(nonatomic, strong) AVAudioPlayer *outgoingToneAudioPlayer;
@property(nonatomic, strong) AVAudioPlayer *hangupAudioPlayer;
@property(nonatomic, strong) AVAudioPlayer *holdToneAudioPlayer;

@property (nonatomic, strong) NSOperationQueue *notificationsOperationQueue;
@property (nonatomic, strong) NSString *appSoundIncomingCall;
@property (nonatomic, strong) NSString *appIconName;
@property (nonatomic) BOOL useTurnsServers;

@property (nonatomic, strong) PINCache *iceServersCache;
@property (nonatomic, strong) NSObject *iceServersMutex;
@property (nonatomic, strong) NSMutableArray *availableIceServers;

// MediaPillar
@property (nonatomic, strong) NSString *mediaPillarAddress;

@property (nonatomic, strong) RTCCall *rtcCall;
@property (nonatomic, strong) CXAnswerCallAction *answerCallAction;
@property (nonatomic) CallStatus cxCallState;  // doesn't know currently held state !
@property (nonatomic, strong) RTCCall* callOnHold;

@end

@implementation RTCService

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService xmppService:xmppService contactManagerService:(ContactsManagerService *) contactsManager myUser:(MyUser *) myUser apiManager:(ApiUrlManagerService *) apiManager{
    self = [super init];
    if (self) {
        _downloadManager = downloadManager;
        _apiUrlManagerService = apiUrlManagerService;
        _xmppService = xmppService;
        _xmppService.rtcService = self;
        _contactsManager = contactsManager;
        _myUser = myUser;
        _apiURLManager = apiManager;
        _isCallKitAvailable = [self isCallKitAvailable];
        
        _rtcCall = nil;
        _answerCallAction = nil;
        _callOnHold = nil;
        
        // Initialize RTC Stack
        
#if DEBUG
        RTCSetMinDebugLogLevel(RTCLoggingSeverityWarning);
#else
        RTCSetMinDebugLogLevel(RTCLoggingSeverityWarning);
#endif
        RTCInitializeSSL();
        
        [RTCAudioSession sharedInstance].useManualAudio = NO;
        [[RTCAudioSession sharedInstance] addDelegate:self];
        
        // Our RTC factory
        _factory = [[RTCPeerConnectionFactory alloc] init];
        
        _calls = [NSMutableArray new];
        
        _iceServers = [NSMutableArray array];
        _iceServersMutex = [NSObject new];
        
        _availableIceServers = [[NSMutableArray alloc] init];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kServicesManagerNewNetworkConnectionDetected object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeAdvancedSettings:) name:@"didChangeAdvancedSettings" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willLogin:) name:kLoginManagerInternalWillLogin object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeServer:) name:kLoginManagerDidChangeServer object:nil];
       
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
        
        // Configure CallKit
        // for iOS 10+
        if (_isCallKitAvailable) {
            
            /* An app should use the CXCallController for the following:
             * - Report when the user has started an outgoing call to the System.
             * - Report when the user answers an incoming call to the System.
             * - Report when the user ends a call to the System.
             */
            
            _callController = [CXCallController new];
        }
        _isSpeakerEnabled = NO;
        
        _notificationsOperationQueue = [NSOperationQueue new];
        _notificationsOperationQueue.maxConcurrentOperationCount = 1;
        _notificationsOperationQueue.name = @"Push Notification operation queue";
        
        _useTurnsServers = YES;
        _iceServersCache = [[PINCache alloc] initWithName:@"iceServers"];
        [_iceServersCache trimToDate:[NSDate dateWithDaysBeforeNow:1]];
    }
    return self;
}

-(void) startCallKitWithIncomingSoundName:(NSString *) incomingSoundName iconTemplate:(NSString *)iconTemplate appName:(NSString *) appName {
    _appSoundIncomingCall = incomingSoundName;
    _appIconName = iconTemplate;
    if (_isCallKitAvailable) {
        
        CXProviderConfiguration *providerConfiguration = [[CXProviderConfiguration alloc] initWithLocalizedName:appName];
        if (_appIconName)
            providerConfiguration.iconTemplateImageData = UIImagePNGRepresentation([UIImage imageNamed:_appIconName]);
        else
            providerConfiguration.iconTemplateImageData = UIImagePNGRepresentation([UIImage imageNamed:@"logo_rainbow_mask"]);
        providerConfiguration.ringtoneSound = _appSoundIncomingCall;
        providerConfiguration.supportsVideo = YES;
        providerConfiguration.supportedHandleTypes = [NSSet setWithObjects:[NSNumber numberWithInt:CXHandleTypeGeneric],[NSNumber numberWithInt:CXHandleTypeEmailAddress], nil];
        
        
        /* An app should use the CXProvider for the following:
         * - Report an incoming call to the System.
         * - Report an that outgoing call has connected to the System.
         * - Report the remote user ending the call to the System.
         */
        _provider = [[CXProvider alloc] initWithConfiguration:providerConfiguration];
        [_provider setDelegate:self queue:nil];
    }
}

-(void) dealloc {
    [_notificationsOperationQueue cancelAllOperations];
    _notificationsOperationQueue = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kServicesManagerNewNetworkConnectionDetected object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didChangeAdvancedSettings" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerWillLogin object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeServer object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
    
    @synchronized(_iceServersMutex) {
        [_iceServers removeAllObjects];
        _iceServers = nil;
    }
    
    
    [_availableIceServers removeAllObjects];
    
    _statsTimer = nil;
    _iceServersCache = nil;
    // hangup any active calls (enumerate in reverse, because we remove them from the array
    // while enumerating it
    [_calls enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(RTCCall *rtcCall, NSUInteger idx, BOOL *stop) {
        [self hangupCall:rtcCall];
    }];
    _calls = nil;
    _contactsManager = nil;
    _xmppService.rtcService = nil;
    _xmppService = nil;
    _factory = nil;
    _downloadManager = nil;
    _apiUrlManagerService = nil;
    _myUser = nil;
    _apiURLManager = nil;
    _outgoingToneAudioPlayer = nil;
    _hangupAudioPlayer = nil;
    _holdToneAudioPlayer = nil;
    RTCCleanupSSL();
    
    // Unconfigure CallKit for iOS 10+
    if (_isCallKitAvailable) {
        [_provider setDelegate:nil queue:nil];
        _provider = nil;
        _callController = nil;
    }
}

-(void) didChangeUser:(NSNotification *) notification {
    OTCLog (@"didChangeUser");
    [_iceServersCache removeAllObjectsAsync:nil];
}

-(void) didChangeServer:(NSNotification *) notification {
    OTCLog (@"didChangeServer");
    [_iceServersCache removeAllObjectsAsync:nil];
}

-(void) didLogin:(NSNotification *) notification {
    [self fetchIceServers];
    
    if (_rtcCall && _answerCallAction) {
        OTCLog(@"didLogin --> performAcceptCall");
        [self performAcceptCall:_rtcCall withAction:_answerCallAction];
        _rtcCall = nil;
        _answerCallAction = nil;
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if (_availableIceServers.count == 0) {
        OTCLog (@"No ice server found in cache, request them to the server");
        [self fetchIceServers];
    }
    
    if (_rtcCall && _answerCallAction) {
        OTCLog(@"didReconnect --> performAcceptCall");
        [self performAcceptCall:_rtcCall withAction:_answerCallAction];
        _rtcCall = nil;
        _answerCallAction = nil;
    }
}

-(void) willLogin:(NSNotification *) notification {
    @synchronized(_iceServersMutex) {
        [_iceServers removeAllObjects];
    }
    NSMutableArray *tempList = [NSMutableArray new];
    [_iceServersCache.diskCache enumerateObjectsWithBlockAsync:^(NSString * key, NSURL * fileURL, BOOL * stop) {
        [tempList addObject:key];
    } completionBlock:^(id<PINCaching> cache) {
        NSDictionary *iceServersCredentials = [UserSettings sharedInstance].iceServersCredentials;
        for (NSString *key in tempList) {
            NSDictionary *object = [_iceServersCache objectForKey:key];
            NSDictionary *creds = [iceServersCredentials objectForKey:key];
            NSMutableDictionary *iceServer = [NSMutableDictionary dictionaryWithDictionary:object];
            OTCLog (@"Load ice-servers : Adding ice server %@",iceServer);
            if (creds) {
                [iceServer setObject:[creds objectForKey:@"credential"] forKey:@"credential"];
                [iceServer setObject:[creds objectForKey:@"username"] forKey:@"username"];
            }
            @synchronized(_iceServersMutex) {
                [_iceServers addObject:iceServer];
            }
            [self buildPeerConnectionIceServerList];
        }
    }];
}

- (void) buildPeerConnectionIceServerList {
    
    NSDictionary *iceServersCredentials = [UserSettings sharedInstance].iceServersCredentials;
    
    for (NSDictionary *server in _iceServers) {
        NSString *iceServerId = [server objectForKey:@"id"];
        NSString *url = [server objectForKey:@"urls"];
        NSString *username = [server objectForKey:@"username"];
        NSString *credential = [server objectForKey:@"credential"];
        
        if (![url length]) {
            continue;
        }
        if (username.length == 0 || credential.length == 0) {
            NSDictionary *creds = [iceServersCredentials objectForKey:iceServerId];
            if (creds) {
                credential = [creds objectForKey:@"credential"];
                username = [creds objectForKey:@"username"];
            } else {
                OTCLog (@"No credentials for this server ! That is not recommended avoid using that server %@", server);
                continue;
            }
        }
        
        BOOL canAddIceServer = NO;
        if (_useTurnsServers && [url containsString:@"turns"])
            canAddIceServer = YES;
        
        if (!_useTurnsServers && ![url containsString:@"turns"])
            canAddIceServer = YES;
        
        if (canAddIceServer) {
            RTCIceServer *iceServer = [[RTCIceServer alloc] initWithURLStrings:@[url] username:username credential:credential];
            
            if (!iceServer) {
                OTCLog (@"Cannot add IceServer %@ to RTC stack", url);
                continue;
            }
            [_availableIceServers addObject:iceServer];
            OTCLog (@"IceServers available: %@",iceServer);
        }
    }
}

-(void) fetchIceServers {
    // Make a REST request to get all the ICE Servers (STUN/TURN) we can use to make RTC calls
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesIceServers, @2];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesIceServers];
    
    OTCLog (@"Get ice-servers list");
    [_iceServersCache removeAllObjects];
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot get ice-servers list in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot get ice-servers list in REST due to JSON parsing failure");
            return;
        }
        
        NSArray *iceServers = [jsonResponse objectForKey:@"data"];
        NSMutableDictionary *iceServersCredentials = [NSMutableDictionary dictionary];
        for (NSDictionary *iceServer in iceServers) {
            // Save the ice server without the credentials
            NSMutableDictionary *theIceServer = [NSMutableDictionary dictionaryWithDictionary:iceServer];
            [theIceServer removeObjectForKey:@"credential"];
            [theIceServer removeObjectForKey:@"username"];
            NSString *iceServerID = [iceServer objectForKey:@"id"];
            [_iceServersCache setObjectAsync:theIceServer forKey:iceServerID completion:nil];
            
            // Keep credential for this ice server (will be saved in keychain)
            NSString *credential = [iceServer objectForKey:@"credential"];
            NSString *username = [iceServer objectForKey:@"username"];
            if (credential.length > 0 && username.length > 0)
                [iceServersCredentials setObject:@{@"credential":credential, @"username":username} forKey:iceServerID];
            else
                OTCLog (@"No credentials for ice server %@", iceServer);
            
            @synchronized(_iceServersMutex) {
                OTCLog (@"Get ice-servers : Add ice server %@", theIceServer);
                [_iceServers addObject:iceServer];
            }
        }
        // Save the credentials in the keychain
        [UserSettings sharedInstance].iceServersCredentials = iceServersCredentials;
        
        [self buildPeerConnectionIceServerList];
        
        OTCLog (@"Get ice-servers list, response ...");
    }];
}

-(RTCCall *) getCallByCallId:(NSUUID *)callId {
    @synchronized(_calls) {
        for (RTCCall *rtcCall in _calls) {
            if ([rtcCall.callID isEqual:callId]) {
                return rtcCall;
            }
        }
    }
    return nil;
}

-(RTCCall *) getCallByJingleSessionId:(NSString *)jingleSessionid {
    @synchronized(_calls) {
        for (RTCCall *rtcCall in _calls) {
            if ([rtcCall.jingleSessionID isEqualToString:jingleSessionid]) {
                return rtcCall;
            }
        }
    }
    return nil;
}

-(RTCCall *) getCallByPeerConnection:(RTCPeerConnection *)peerConnection {
    @synchronized(_calls) {
        for (RTCCall *rtcCall in _calls) {
            if ([rtcCall.peerConnection isEqual:peerConnection]) {
                return rtcCall;
            }
        }
    }
    return nil;
}

-(RTCCall *) getCallByPeerJID:(NSString *)jid {
    @synchronized(_calls) {
        for (RTCCall *rtcCall in _calls) {
            if ([rtcCall.peer.jid isEqual:jid]) {
                return rtcCall;
            }
        }
    }
    return nil;
}


-(BOOL) hasActiveCalls {
    @synchronized(_calls) {
        return _calls.count > 0;
    }
}

-(RTCCall *) getCallByPeerContainingString:(NSString *) type {
    @synchronized(_calls) {
        for (RTCCall *rtcCall in _calls) {
            if ([rtcCall.peer.rtcJid containsString:type])
                return rtcCall;
        }
    }
    return nil;
}

-(RTCCall *) getCallByPeerRainbowID:(NSString *) rainbowID {
    @synchronized(_calls) {
        for (RTCCall *rtcCall in _calls) {
            if([rtcCall.peer.publisherID containsString:rainbowID] && rtcCall.peer.mediaType == PeerMediaTypeVideo)
                return rtcCall;
        }
    }
    return nil;
}

-(RTCCall *) getFakeCallByWithFeature:(RTCCallFeatureFlags) feature {
    @synchronized(_calls) {
        for (RTCCall *rtcCall in _calls) {
            if(rtcCall.features & feature && rtcCall.fakeCall)
                return rtcCall;
        }
    }
    return nil;
}


-(NSDictionary *) getMediaContraintsForCall:(RTCCall *) rtcCall iceRestart:(BOOL) iceRestart  {
    NSMutableDictionary *mandatoryConstraints = [[NSMutableDictionary alloc] init];
    /*
     * Allowed constraints for answer are :
     * - OfferToReceiveAudio
     * - OfferToReceiveVideo
     * - VoiceActivityDetection
     * - googUseRtpMUX
     * - IceRestart
     */
    if ([rtcCall isAudioEnabled]) {
        [mandatoryConstraints setObject:kRTCMediaConstraintsValueTrue forKey:@"OfferToReceiveAudio"];
        [mandatoryConstraints setObject:kRTCMediaConstraintsValueTrue forKey:@"VoiceActivityDetection"];
    }
    if ([rtcCall isVideoEnabled]) {
        [mandatoryConstraints setObject:kRTCMediaConstraintsValueTrue forKey:@"OfferToReceiveVideo"];
    }
    
    if (iceRestart)
        [mandatoryConstraints setObject:kRTCMediaConstraintsValueTrue forKey:@"IceRestart"];
    
    return mandatoryConstraints;
}

#pragma mark - XMPP Service delegate
-(void) xmppService:(XMPPService *)service didReceiveRemoteRecordingStatusChanged:(NSString *)status forJID:(NSString *)jid {
    if ([jid length] == 0) {
        OTCLog (@"Error: We need a jid to find the rtc call");
        return;
    }
    
    RTCCall *rtcCall = [self getCallByPeerJID:jid];
    
    if (rtcCall) {
        if ([status isEqualToString:@"start"]) {
            rtcCall.isRecording = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidChangeRecordingCallStateNotification object:rtcCall];
        } else if ([status isEqualToString:@"stop"]) {
            rtcCall.isRecording = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidChangeRecordingCallStateNotification object:rtcCall];
        }
    }
}

#pragma mark - Public functions

-(void) setAppSoundIncomingCall:(NSString *)appSoundIncomingCall {
    _appSoundIncomingCall = appSoundIncomingCall;
    
    if (_isCallKitAvailable) {
        CXProviderConfiguration *config = [_provider.configuration copy];
        config.ringtoneSound = _appSoundIncomingCall;
        
    
    }
}

-(void) setAppIconName:(NSString *)appIconName {
    _appIconName = appIconName;
}

-(RTCCall *) beginNewOutgoingCallWithPeer:(Peer *)peer withFeatures:(RTCCallFeatureFlags)features {
    return [self beginNewOutgoingCallWithPeer:peer phoneNumber:nil withFeatures:features andSubject:nil];
}

-(RTCCall *) beginNewOutgoingCallWithPeer:(Peer *)peer withFeatures:(RTCCallFeatureFlags)features andSubject:(NSString *)subject {
    return [self beginNewOutgoingCallWithPeer:peer phoneNumber:nil withFeatures:features andSubject:subject];
}

-(RTCCall *) beginNewOutgoingCallWithPeer:(Peer * _Nonnull)peer phoneNumber:(NSString * _Nullable) phoneNumber withFeatures:(RTCCallFeatureFlags)features {
    return [self beginNewOutgoingCallWithPeer:peer phoneNumber:nil withFeatures:features andSubject:nil];
}

-(RTCCall *) beginNewOutgoingCallWithPeer:(Peer * _Nonnull)peer phoneNumber:(NSString * _Nullable) phoneNumber withFeatures:(RTCCallFeatureFlags)features andSubject:(NSString *)subject {
    if (!peer) {
        OTCLog (@"Error: We need a peer to call it");
        return nil;
    }
    if ([peer.rtcJid length] == 0 && !(features & RTCCallFeatureMediaPillar)) {
        OTCLog (@"Error: Peer needs a JID to be called");
        return nil;
    }
    if (features == 0) {
        OTCLog (@"Error: We need at least 1 feature to do a call");
        return nil;
    }
    if (subject && ![peer isKindOfClass:[Contact class]]) {
        OTCLog (@"Error: subject of the call is only handled in P2P call");
        return nil;
    }
    
    // Build a new RTCCall object
    RTCCall *rtcCall = [RTCCall new];
    rtcCall.callID = [NSUUID UUID];
    rtcCall.jingleSessionID = [NSString stringWithFormat:@"%u", arc4random()];
    rtcCall.peer = peer;
    rtcCall.isIncoming = NO;
    rtcCall.status = CallStatusRinging;
    rtcCall.features = features;
    
    [self createWebrtcStatsforCall:rtcCall withCompletionHandler:^(NSString * webRTCMetricsID, NSError *error) {
        if (!error && webRTCMetricsID != nil) {
            rtcCall.webRTCMetricsID = webRTCMetricsID;
            OTCLog (@"create WebRTC metrics");
        }
    }];
    
    [self addCall:rtcCall];
    
    NSMutableArray *medias = [NSMutableArray new];
    if (rtcCall.isAudioEnabled)
        [medias addObject:@"audio"];
    if (rtcCall.isVideoEnabled)
        [medias addObject:@"video"];
    
    if ([peer isKindOfClass:[Contact class]]) {
        // Send a JingleMessageInitiation to ask for call
        // <propose> to remote bareJID
        if(subject){
            [_xmppService sendJingleInitiationProposeTo:rtcCall.peer.rtcJid sessionID:rtcCall.jingleSessionID withMedias:medias andSubject:subject];
        } else {
            [_xmppService sendJingleInitiationProposeTo:rtcCall.peer.rtcJid sessionID:rtcCall.jingleSessionID withMedias:medias];
        }
    } else {
        if (features & RTCCallFeatureMediaPillar) {
            OTCLog (@"[MEDIAPILLAR] RTCCallFeatureMediaPillar feature");
            // We are calling with mediapillar we must invoke directly the session-initiate there is no negotiation phase
            [self didReceiveMediaPillarProceedMsg:rtcCall.jingleSessionID from:phoneNumber resource:@"mediapillar"];
        } else {
            // We are calling a room we must invoke directly the session-initiate there is no negotiation phase
            [self didReceiveProceedMsg:rtcCall.jingleSessionID from:rtcCall.peer resource:nil];
        }
    }
    
    void(^actionCompletion)(NSError*) = ^(NSError *error) {
        if (error) {
            OTCLog (@"Error: beginNewOutgoingCallWithContact failed. %@", error);
            [self cancelOutgoingCall:rtcCall];
            return;
        }
        
        if ([peer isKindOfClass:[Contact class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setupAudioSession];
                [self preparePlayingOutgoingRingTone];
                [_outgoingToneAudioPlayer play];
                OTCLog (@"Play outgoing ringtone %@", _outgoingToneAudioPlayer);
            });
        }
        if (!rtcCall.fakeCall && !rtcCall.isMediaPillarCall)
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidAddCallNotification object:rtcCall];
        
        if (!rtcCall.fakeCall && [rtcCall.peer isKindOfClass:[Contact class]]) {
            rtcCall.outgoingCallTimer = [NSTimer timerWithTimeInterval:kHangupIfNoAnswerAfter target:self selector:@selector(noAnswerInTime:) userInfo:rtcCall repeats:NO];
            [[NSRunLoop mainRunLoop] addTimer:rtcCall.outgoingCallTimer forMode:NSRunLoopCommonModes];
        }
        
        if (rtcCall.isVideoEnabled)
            [self forceAudioOnSpeaker];
        
    };
    
    // For iOS 10+, notify CallKit we'll start an outgoing call
    if (_isCallKitAvailable) {
        
        CXHandle *remoteHandle = [[CXHandle alloc] initWithType:CXHandleTypeGeneric
                                                          value:(rtcCall.isMediaPillarCall) ? _mediaPillarAddress : rtcCall.peer.rtcJid];
        CXStartCallAction *startCallAction = [[CXStartCallAction alloc] initWithCallUUID:rtcCall.callID handle:remoteHandle];
        startCallAction.contactIdentifier = (rtcCall.isMediaPillarCall) ? phoneNumber : rtcCall.peer.displayName;
        startCallAction.video = rtcCall.isVideoEnabled;
        
        CXTransaction *transaction = [CXTransaction new];
        [transaction addAction:startCallAction];
        
        [_callController requestTransaction:transaction completion:actionCompletion];
        
    } else {
        actionCompletion(nil);
    }
    
    // Process continue in one of these functions :
    // - xmppService:didReceiveProceedMsg:from:resource: (if the remote peer accept the call)
    // - xmppService:didReceiveRejectMsg:from:resource: (if the remove peer decline the call)
    // - cancelOutgoingCall: (if we cancel manually the call, before it is accepted/declined)
    // - noAnswerInTime: (if the remote peer gives no answer at all)
    
    return rtcCall;
    
}

-(void) cancelOutgoingCall:(RTCCall *)rtcCall {
    if (!rtcCall) {
        OTCLog (@"Error: We need a RTCCall to cancel it");
        return;
    }
    if (rtcCall.isIncoming) {
        OTCLog (@"Error: cannot cancel an incoming call %@. Maybe you want to decline it ?", rtcCall);
        return;
    }
    if (rtcCall.status != CallStatusRinging) {
        OTCLog (@"Error: cannot cancel a non-ringing call %@. Maybe you want to hangup it ?", rtcCall);
        return;
    }
    
    [self stopPlayingOutgoingRingTone];
    
    [rtcCall.outgoingCallTimer invalidate];
    rtcCall.outgoingCallTimer = nil;
    
    // Send JingleMessageInitiation to cancel
    // <retract> to remote bareJID
    [_xmppService sendJingleInitiationRetractTo:(rtcCall.isMediaPillarCall) ? _mediaPillarAddress : rtcCall.peer.jid
                                      sessionID:rtcCall.jingleSessionID];
    
    rtcCall.status = CallStatusCanceled;
    
    [self updateWebrtcMetricsForCall:rtcCall  withCompletionHandler:^(NSError *error) {
        if (!error) {
            OTCLog (@"update WebRTC metrics");
        }
    }];
    
    // For iOS 10+, notify CallKit we canceled the call
    if (_isCallKitAvailable) {
        CXEndCallAction *endCallAction = [[CXEndCallAction alloc] initWithCallUUID:rtcCall.callID];
        CXTransaction *transaction = [CXTransaction new];
        [transaction addAction:endCallAction];
        
        // The call is removed from _calls and notif triggered in the performEndCallAction: delegate
        [_callController requestTransaction:transaction completion:^(NSError *error) {
            if (error) {
                OTCLog (@"Error: cancelOutgoingCall failed. %@", error);
                return;
            }
        }];
    } else {
        [self stopPlayingIncomingRingingTone];
        [_calls removeObject:rtcCall];
        if (!rtcCall.fakeCall)
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:rtcCall];
    }
}


-(void) acceptIncomingCall:(RTCCall *)rtcCall withFeatures:(RTCCallFeatureFlags)features {
    if (!rtcCall) {
        OTCLog (@"Error: We need a RTCCall to accept it");
        return;
    }
    if (!rtcCall.isIncoming) {
        OTCLog (@"Error: cannot accept an outgoing call %@", rtcCall);
        return;
    }
    if (rtcCall.status != CallStatusRinging) {
        OTCLog (@"Error: cannot accept a non-ringing call %@", rtcCall);
        return;
    }
    
    rtcCall.features |= features;
    
    // Send JingleMessageInitiation to accept
    // <accept> to myself (kinda carbon to stop ringing on my other devices)
    // <proceed> to remote (so he'll know we accepted and HE will start the real jingle session)
    [_xmppService sendJingleInitiationAcceptsessionID:rtcCall.jingleSessionID];
    [_xmppService sendJingleInitiationProceedTo:rtcCall.fullJID sessionID:rtcCall.jingleSessionID];
    
#if TARGET_IPHONE_SIMULATOR
    CXAnswerCallAction *answerCallAction = [[CXAnswerCallAction alloc] initWithCallUUID:rtcCall.callID];
    CXTransaction *transaction = [CXTransaction new];
    [transaction addAction:answerCallAction];
    
    [_callController requestTransaction:transaction completion:^(NSError *error) {
        if (error) {
            OTCLog (@"Error: acceptIncomingCall failed. %@", error);
            return;
        }
    }];
#endif
    rtcCall.status = CallStatusConnecting;
    
    if (rtcCall.isMediaPillarCall) {
        OTCLog (@"acceptIncomingCall from mediaPillar");
    }
    else {
        if (!rtcCall.connectionDate)
            rtcCall.connectionDate = [NSDate date];
        if (!rtcCall.fakeCall)
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:rtcCall];
    }
    
    [self createWebrtcStatsforCall:rtcCall  withCompletionHandler:^(NSString * webRTCMetricsID, NSError *error) {
        if (!error && webRTCMetricsID != nil) {
            rtcCall.webRTCMetricsID = webRTCMetricsID;
            OTCLog (@"create WebRTC metrics");
        }
    }];
}


-(void) declineIncomingCall:(RTCCall *)rtcCall {
    if (!rtcCall) {
        OTCLog (@"Error: We need a RTCCall to decline it");
        return;
    }
    if (!rtcCall.isIncoming) {
        OTCLog (@"Error: cannot decline an outgoing call %@", rtcCall);
        return;
    }
    if (rtcCall.status != CallStatusRinging) {
        OTCLog (@"Error: cannot decline a non-ringing call %@", rtcCall);
        return;
    }
    
    // Send JingleMessageInitiation to decline
    // <reject> to myself (kinda carbon to stop ringing on my other devices)
    // <reject> to remote (so he'll know we declined)
    [_xmppService sendJingleInitiationRejectsessionID:rtcCall.jingleSessionID];
    [_xmppService sendJingleInitiationRejectTo:rtcCall.fullJID sessionID:rtcCall.jingleSessionID];
    
    rtcCall.status = CallStatusDeclined;
    
    // For iOS 10+, notify CallKit we declined the call
    if (_isCallKitAvailable) {
        CXEndCallAction *endCallAction = [[CXEndCallAction alloc] initWithCallUUID:rtcCall.callID];
        CXTransaction *transaction = [CXTransaction new];
        [transaction addAction:endCallAction];
        
        // The call is removed from _calls and notif triggered in the performEndCallAction: delegate
        [_callController requestTransaction:transaction completion:^(NSError *error) {
            if (error) {
                OTCLog (@"Error: declineIncomingCall failed. %@", error);
                return;
            }
        }];
    } else {
         [self stopPlayingIncomingRingingTone];
        if (rtcCall.status == CallStatusEstablished) {
            
            // We need to reconnect (if phone is locked and we stay in CallKit UI)
            [_servicesManager checkTokenExpirationAndRenewIt];
           
        }
        [self hangupCall:rtcCall];
       
        [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidReceiveHangupCall object:rtcCall];

        [_calls removeObject:rtcCall];
        if (!rtcCall.fakeCall)
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:rtcCall];
        
    }
}

-(void) hangupCallForPublisherRainbowID:(NSString *) rainbowID {
    OTCLog (@"Hangup RTCCall for publisher rainbowID %@", rainbowID);
    RTCCall *publisherCall = [self getCallByPeerRainbowID:rainbowID];
    
    [self peerConnection:publisherCall.peerConnection didRemoveStream:publisherCall.stream];
    [publisherCall.peerConnection close];
    [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRemoveRemoteVideoTrackNotification object:nil];
    
    RTCCall *realCall = [self getCallByCallId:publisherCall.realCallID];
    if (realCall) {
        [realCall.linkedCalls removeObject:publisherCall];
    }
    
    [_calls removeObject:publisherCall];
}

-(void) hangupCall:(RTCCall *)rtcCall {
    OTCLog (@"Hangup RTCCall %@", rtcCall);
    // We have to hangup all linkedCall
    if (rtcCall.linkedCalls.count > 0) {
        [rtcCall.linkedCalls enumerateObjectsUsingBlock:^(RTCCall * obj, NSUInteger idx, BOOL * stop) {
            [self hangupCall:obj];
        }];
    }
    
    [self stopPlayingOutgoingRingTone];
    [self stopPlayingHoldTone];
    
    if (!rtcCall) {
        OTCLog (@"Error: We need a RTCCall to hangup it");
        return;
    }
    
    if (rtcCall.status == CallStatusRinging) {
        if (rtcCall.isIncoming) {
            OTCLog (@"Warning: you should decline a ringing-incoming call, not directly hangup it. I'll do it for you ;-)");
            [self declineIncomingCall:rtcCall];
            return;
            
        } else {
            OTCLog (@"Warning: you should cancel a ringing-outgoing call, not directly hangup it. I'll do it for you ;-)");
            [self cancelOutgoingCall:rtcCall];
            return;
        }
    }

    [_xmppService sendJingleTerminatetoTarget:(rtcCall.isMediaPillarCall) ? _mediaPillarAddress : rtcCall.fullJID
                                   withReason:@"success"
                                    sessionID:rtcCall.jingleSessionID
                                         peer:rtcCall.peer];
    
    RTCMediaStream *stream = rtcCall.stream;
    if (stream) {
        [self unForceAudioOnSpeaker];
        [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRemoveRemoteVideoTrackNotification object:nil];
        [self peerConnection:rtcCall.peerConnection didRemoveStream:stream];
    }
    
    __weak __typeof__(self) weakSelf = self;
    [self getAllStatsWithCompletionHandler:^(NSMutableArray *stats) {
        [self addWebrtcMetricsforCall:rtcCall andStatsArray:stats withCompletionHandler:^(NSError *error) {
            if (!error) {
                OTCLog (@"add WebRTC metrics");
            }
            
            [weakSelf.statsTimer invalidate];
            [rtcCall.peerConnection close];
            
            rtcCall.status = CallStatusHangup;
            
            if (!rtcCall.fakeCall) {
                
                // find a way to check if this is end of feature or cancel the call
                [weakSelf updateWebrtcMetricsForCall:rtcCall  withCompletionHandler:^(NSError *error) {
                    if (!error) {
                        OTCLog (@"update WebRTC metrics");
                    }
                }];
                
                if (_isCallKitAvailable)
                    [weakSelf requestTransaction:rtcCall];
                else {
                    [self stopPlayingIncomingRingingTone];
                    [_calls removeObject:rtcCall];
                }
             
                [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:rtcCall];
                
            } else {
                // find a way to check if this is end of feature or cancel the call
                [weakSelf updateWebrtcMetricsForCall:rtcCall  withCompletionHandler:^(NSError *error) {
                    if (!error) {
                        OTCLog (@"update Webrtc Metrics!");
                    }
                    
                    [weakSelf removeCall:rtcCall];
                    if (!rtcCall.fakeCall)
                        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:rtcCall];
                }];
                
            }
            
        }];
    }];
}

- (void) requestTransaction :(RTCCall *)rtcCall {
    CXEndCallAction *endCallAction = [[CXEndCallAction alloc] initWithCallUUID:rtcCall.callID];
    CXTransaction *transaction = [CXTransaction new];
    [transaction addAction:endCallAction];
    // The call is removed from _calls and notif triggered in the performEndCallAction: delegate
    [self.callController requestTransaction:transaction completion:^(NSError *error) {
        if (error) {
            OTCLog (@"Error: hangupCall failed : %@", error);
            return;
        }
    }];
}

-(BOOL) isCallMuted:(RTCCall *) rtcCall {
    NSArray<RTCMediaStream *> *localStreams = rtcCall.peerConnection.localStreams;
    BOOL isMuted = NO;
    for (RTCMediaStream *localStream in localStreams) {
        for (RTCAudioTrack *audioTrack in localStream.audioTracks) {
            if (!audioTrack.isEnabled) {
                isMuted = YES;
                break;
            }
        }
    }
    return isMuted;
}

-(void) muteLocalAudioForCall:(RTCCall *)rtcCall {
    // In case of mediapillar call rtcCall is a Call object.
    if([rtcCall isKindOfClass:[Call class]]){
        rtcCall = [_calls firstObject];
    }
    if (!rtcCall) {
        OTCLog (@"Error: We need a RTCCall to local mute it");
        return;
    }
    
    NSArray<RTCMediaStream *> *localStreams = rtcCall.peerConnection.localStreams;
    
    for (RTCMediaStream *localStream in localStreams) {
        for (RTCAudioTrack *audioTrack in localStream.audioTracks) {
            audioTrack.isEnabled = NO;
        }
    }
}

-(void) unMuteLocalAudioForCall:(RTCCall *)rtcCall {
    // In case of mediapillar call rtcCall is a Call object
    if([rtcCall isKindOfClass:[Call class]]){
        rtcCall = [_calls firstObject];
    }
    if (!rtcCall) {
        OTCLog (@"Error: We need a RTCCall to local un-mute it");
        return;
    }
    
    NSArray<RTCMediaStream *> *localStreams = rtcCall.peerConnection.localStreams;
    
    for (RTCMediaStream *localStream in localStreams) {
        for (RTCAudioTrack *audioTrack in localStream.audioTracks) {
            audioTrack.isEnabled = YES;
        }
    }
}

-(BOOL) isSpeakerEnabled {
    return _isSpeakerEnabled;
}

- (BOOL)areHeadphonesPluggedIn {
    NSArray *availableOutputs = [[AVAudioSession sharedInstance] currentRoute].outputs;
    for (AVAudioSessionPortDescription *portDescription in availableOutputs) {
        if ([portDescription.portType isEqualToString:AVAudioSessionPortHeadphones] || [portDescription.portType isEqualToString:AVAudioSessionPortBluetoothHFP]) {
            return YES;
        }
    }
    return NO;
}

-(void) forceAudioOnSpeaker {
    NSError *error = nil;
    [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
    _isSpeakerEnabled = YES;
    
    if (error) {
        OTCLog (@"Error: cannot force audio on speaker, error %@", error);
    }
}



-(void) unForceAudioOnSpeaker {
    NSError *error = nil;
    [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
    _isSpeakerEnabled = NO;
    
    if (error) {
        OTCLog (@"Error: cannot un-force audio on speaker, error %@", error);
    }
}

#pragma mark - video
-(RTCMediaStream *) remoteVideoStreamForCall:(RTCCall *) call {
    if (call.linkedCalls.count > 0) {
        __block RTCMediaStream *sharingStream = nil;
        // There is a linked call so look in this call for the media to return
        [call.linkedCalls enumerateObjectsUsingBlock:^(RTCCall * linkedCall, NSUInteger idx, BOOL * stop) {
            if (linkedCall.features & RTCCallFeatureRemoteVideo) {
                sharingStream = linkedCall.stream;
                *stop = YES;
            }
        }];
        return sharingStream;
    }
    
    return call.stream;
}

-(RTCMediaStream *) remoteSharingStreamForCall:(RTCCall *) call {
    if (call.linkedCalls.count > 0) {
        __block RTCMediaStream *sharingStream = nil;
        // There is a linked call so look in this call for the media to return
        [call.linkedCalls enumerateObjectsUsingBlock:^(RTCCall * linkedCall, NSUInteger idx, BOOL * stop) {
            if (linkedCall.features & RTCCallFeatureRemoteSharing) {
                sharingStream = linkedCall.stream;
                *stop = YES;
            }
        }];
        return sharingStream;
    }
    
    return call.stream;
}

-(RTCMediaStream *) localVideoStreamForCall:(RTCCall *) call {
    __block RTCMediaStream * localStream = nil;
    
    if (call.linkedCalls.count > 0) {
        // There is a linked call so look in this call for the media to return
        [call.linkedCalls enumerateObjectsUsingBlock:^(RTCCall * linkedCall, NSUInteger idx, BOOL * stop) {
            if (linkedCall.features & RTCCallFeatureLocalVideo) {
                localStream = [linkedCall.peerConnection.localStreams objectAtIndex:0];
                *stop = YES;
            }
        }];
    } else {
        [call.peerConnection.localStreams enumerateObjectsUsingBlock:^(RTCMediaStream * stream, NSUInteger idx, BOOL * stop) {
            if (stream.videoTracks.count > 0) {
                localStream = stream;
                *stop = YES;
            }
        }];
    }
    return localStream;
}

-(void) addVideoMediaToCall:(RTCCall *) call {
    if (call.features & RTCCallFeatureLocalVideo) {
        OTCLog (@"This call is already a Local video call");
        return;
    }
    
    if (call.status != CallStatusEstablished) {
        OTCLog (@"Call must be established before allowing adding video");
        return;
    }
    
    if (call.peerConnection.signalingState != RTCSignalingStateStable) {
        OTCLog (@"Signaling state is not stable doing nothing.");
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        if ([call.peer isKindOfClass:[Room class]]) {
            // Build a new RTCCall object
            RTCCall *rtcCall = [RTCCall new];
            rtcCall.callID = [NSUUID UUID];
            rtcCall.jingleSessionID = [NSString stringWithFormat:@"%u", arc4random()];
            rtcCall.peer = call.peer;
            rtcCall.isIncoming = NO;
            rtcCall.status = CallStatusRinging;
            rtcCall.features = RTCCallFeatureLocalVideo;
            rtcCall.fakeCall = YES;
            [self addCall:rtcCall];
            [self didReceiveProceedMsg:rtcCall.jingleSessionID from:rtcCall.peer resource:nil];
            
            call.features |= rtcCall.features;
            OTCLog (@"Add video to call requested %@", rtcCall);
            [call.linkedCalls addObject:rtcCall];
        } else {
            OTCLog (@"Add video to call requested %@", call);
            call.features |= RTCCallFeatureLocalVideo;
            
            RTCMediaStream *localStream = [call.peerConnection.localStreams firstObject];
            OTCLog (@"Remove local stream %@", localStream);
            [call.peerConnection removeStream:localStream];
            
            RTCMediaStream *newLocalStream = [self createLocalMediaStreamForCall:call];
            OTCLog (@"Create new local stream %@", newLocalStream);
            [call.peerConnection addStream:newLocalStream];
            
            OTCLog (@"Prepare RTC Offer");
            [self prepareRTCOfferForCall:call iceRestart:NO completionHandler:^(RTCSessionDescription *offer) {
                OTCLog (@"Sending SDP Offer content add : %@", offer);
                [_xmppService sendJingleSDP:offer.sdp type:XMPPJingleMessageTypeContentAdd toTarget:call.fullJID sessionID:call.jingleSessionID peer:call.peer];
            }];
        }
        
        if (![self areHeadphonesPluggedIn])
            [self forceAudioOnSpeaker];
        
        if (!call.fakeCall)
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:call];
    });
}

-(void) removeVideoMediaFromCall:(RTCCall *) call {
    if (call.status != CallStatusEstablished) {
        OTCLog (@"Call must be established before allowing adding video");
        return;
    }
    
 
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        if (call.peerConnection.signalingState != RTCSignalingStateStable) {
            OTCLog (@"Signaling state is not stable doing nothing.");
            return;
        }
        
        
        if ([call.peer isKindOfClass:[Room class]]) {
            // session-terminate
            RTCCall *fakeCall = [self getFakeCallByWithFeature:RTCCallFeatureLocalVideo];
            fakeCall.webRTCMetricsID = call.webRTCMetricsID;
            RTCMediaStream *localStream = [fakeCall.peerConnection.localStreams firstObject];
            RTCVideoTrack *localVideoTrack = localStream.videoTracks[0];
            [self hangupCall:fakeCall];
            call.features &= ~fakeCall.features;
            [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRemoveLocalVideoTrackNotification object:localVideoTrack];
            if (!call.fakeCall)
                [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:call];
            [call.linkedCalls removeObject:fakeCall];
        } else {
            if ([call isLocalVideoEnabled]) {
                OTCLog (@"Remove video from call requested %@", call);
                call.features &= ~RTCCallFeatureLocalVideo;
                call.isModifingMedia = YES;
                
                RTCMediaStream *localStream = [call.peerConnection.localStreams firstObject];
                RTCVideoTrack *localVideoTrack = localStream.videoTracks[0];
                [call.peerConnection removeStream:localStream];
                [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRemoveLocalVideoTrackNotification object:localVideoTrack];
                
                RTCMediaStream *newLocalStream = [self createLocalMediaStreamForCall:call];
                [call.peerConnection addStream:newLocalStream];
                
                [self prepareRTCOfferForCall:call iceRestart:NO completionHandler:^(RTCSessionDescription *offer) {
                    OTCLog (@"Sending SDP Offer Remove video : %@", offer);
                    [_xmppService sendJingleSDP:offer.sdp type:XMPPJingleMessageTypeContentRemove toTarget:call.fullJID sessionID:call.jingleSessionID peer:call.peer];
                }];
                if (!call.fakeCall)
                    [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:call];
                
            } else {
                OTCLog (@"This call doesn't have a Local video call");
                return;
            }
        }
    });
}

#pragma mark - RingTone
-(void) preparePlayingOutgoingRingTone {
    // play the outgoing rings sound
    NSURL *musicFile = [self getNSURLOfAudioFile:_appSoundOutgoingCall];
    
    if (musicFile) {
        _outgoingToneAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile error:nil];
        _outgoingToneAudioPlayer.numberOfLoops = -1;
        [_outgoingToneAudioPlayer prepareToPlay];

        OTCLog (@"Info: preparePlayingOutgoingRingTone");
        // _audioPlayer start can only be done in didActivateAudioSession
    }
}

-(void) stopPlayingOutgoingRingTone {
    OTCLog (@"Info: stopPlayingOutgoingRingTone");
    if (_outgoingToneAudioPlayer.isPlaying) {
        OTCLog (@"Info: audioPlayer is playing stop it");
        [_outgoingToneAudioPlayer stop];
    } else {
        OTCLog (@"Info: audioPlayer is not playing");
    }
    _outgoingToneAudioPlayer = nil;
}


-(void) startPlayingHangupTone {
    // play the hangup sound
    NSURL *musicFile = [self getNSURLOfAudioFile:_appSoundHangup];
    
    if (musicFile) {
        _hangupAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile error:nil];
        _hangupAudioPlayer.numberOfLoops = 3;
        if ([_hangupAudioPlayer prepareToPlay]) {
            OTCLog (@"Info: startPlayingHangupTone");
            [_hangupAudioPlayer play];
        }
    }
}

-(void) startPlayingIncomingRingingTone {
    NSURL *musicFile = [self getNSURLOfAudioFile:_appSoundIncomingCall];
    
    if (musicFile) {
        _outgoingToneAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile error:nil];
        _outgoingToneAudioPlayer.numberOfLoops = -1;
        [_outgoingToneAudioPlayer prepareToPlay];
        [_outgoingToneAudioPlayer play];
        
        OTCLog (@"Info: preparePlayingOutgoingRingTone");
        // _audioPlayer start can only be done in didActivateAudioSession
    }
}

-(void) stopPlayingIncomingRingingTone {
    [self stopPlayingOutgoingRingTone];
}

#pragma mark - Hold Tone

-(void) startPlayingHoldTone {
    // play the hold tone sound
    NSURL *musicFile = [self getNSURLOfAudioFile:_appSoundHoldTone];
    
    if (musicFile) {
        _holdToneAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile error:nil];
        _holdToneAudioPlayer.numberOfLoops = -1; // infinite loop
        if ([_holdToneAudioPlayer prepareToPlay]) {
            OTCLog (@"Info: startPlayingHoldTone");
            [_holdToneAudioPlayer play];
        }
    }
}

-(void) stopPlayingHoldTone {
    OTCLog (@"Info: stopPlayingHoldTone");
    if (_holdToneAudioPlayer.isPlaying) {
        OTCLog (@"Info: hold tone audioPlayer is playing. Stop it");
        [_holdToneAudioPlayer stop];
    } else {
        OTCLog (@"Info: hold tone audioPlayer is not playing");
    }
    _holdToneAudioPlayer = nil;
}

#pragma mark - SDK Private functions

-(void) hangupCallNoSignaling:(RTCCall *)rtcCall {
    if (!rtcCall) {
        OTCLog (@"Error: We need a RTCCall to hangup it");
        return;
    }
    
    if (rtcCall.status == CallStatusRinging) {
        if (rtcCall.isIncoming) {
            OTCLog (@"Warning: you should decline a ringing-incoming call, not directly hangup it. I'll do it for you ;-)");
            [self declineIncomingCall:rtcCall];
            return;
            
        } else {
            OTCLog (@"Warning: you should cancel a ringing-outgoing call, not directly hangup it. I'll do it for you ;-)");
            [self cancelOutgoingCall:rtcCall];
            return;
        }
    }
    
    rtcCall.status = CallStatusHangup;
    
    // For iOS 10+, notify CallKit we hangup the call
    if (_isCallKitAvailable) {
        // The call is removed from _calls and notif triggered in the performEndCallAction: delegate
        [_provider reportCallWithUUID:rtcCall.callID endedAtDate:[NSDate date] reason:CXCallEndedReasonRemoteEnded];
    } else {
        [self stopPlayingOutgoingRingTone];
    }
    if (!rtcCall.fakeCall && (!rtcCall.isMediaPillarCall || !_isCallKitAvailable))
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:rtcCall];
    
    __weak __typeof__(self) weakSelf = self;
    [self getAllStatsWithCompletionHandler:^(NSMutableArray *stats) {
        [self addWebrtcMetricsforCall:rtcCall andStatsArray:stats withCompletionHandler:^(NSError *error) {
            if (!error) {
                OTCLog (@"add WebRTC metrics");
            }
            
            if (!rtcCall.fakeCall) {
                
                [weakSelf updateWebrtcMetricsForCall:rtcCall withCompletionHandler:^(NSError *error) {
                    if (!error) {
                        OTCLog (@"update WebRTC metrics");
                    }
                    [weakSelf removeCall:rtcCall];
                    if (!rtcCall.fakeCall) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:rtcCall];
                    }
                    [weakSelf.statsTimer invalidate];
                    [rtcCall.peerConnection close];
                    
                }];
                
            }
            
        }];
    }];
}


- (void) addCall :(RTCCall *)rtcCall {
    @synchronized(_calls) {
        [_calls addObject: rtcCall];
    }
}

- (void) removeCall :(RTCCall *)rtcCall {
    @synchronized(_calls) {
        [_calls removeObject:rtcCall];
    }
}


-(void) noAnswerInTime:(NSTimer *) timer {
    // Do stuff..
    RTCCall *rtcCall = timer.userInfo;
    
    [self stopPlayingOutgoingRingTone];
    
    OTCLog (@"Error: No answer in time, aborting call %@", rtcCall);
    
    if (rtcCall.isIncoming) {
        [self declineIncomingCall:rtcCall];
        
    } else {
        [self cancelOutgoingCall:rtcCall];
    }
}

// Jingle Initiation

-(void) didReceiveProposeMsg:(NSString *) sessionID withMedias:(NSArray<NSString *> *) medias from:(Peer *) peer resource:(NSString *) resource {
    if (!self.microphoneAccessGranted) {
        OTCLog (@"Microphone is not enabled, don't present this call %@", sessionID);
        return;
    }
    // Ignore second RTC call
    if (_calls.count >= 1) {
        OTCLog (@"Ignoring second call from peer : %@ with sessionID : %@", peer, sessionID);
        return;
    }
    // Make sure we don't already have a call with this sessionID
    RTCCall *aRtcCall = [self getCallByJingleSessionId:sessionID];
    if (aRtcCall) {
        OTCLog (@"Error: we cannot handle <propose> on an existing call %@", aRtcCall);
        return;
    }

    [_notificationsOperationQueue addOperationWithBlock:^{
        BOOL isMediaPillarCall = NO;
        BOOL isMediaPillarMakeCall = NO;
        if (_myUser.mediaPillarStatus.jid)
            isMediaPillarCall = [peer.jid containsString:_myUser.mediaPillarStatus.jid];
        
        // Compute features from medias
        RTCCallFeatureFlags features = 0;
        if ([medias containsObject:@"audio"]) {
            features |= RTCCallFeatureAudio;
        }
        
        if ([medias containsObject:@"video"]) {
            features |= RTCCallFeatureRemoteVideo;
        } else {
            OTCLog (@"User is not allowed to use video call, so answer with audio only");
        }
        
        if (isMediaPillarCall || [resource isEqualToString:@"mediapillar"] || [medias containsObject:@"mediaPillarMakeCall"]) {
            OTCLog (@"MediaPillar call feature");
            features |= RTCCallFeatureMediaPillar;
            isMediaPillarCall = YES;
        }
        if ([medias containsObject:@"mediaPillarMakeCall"]) {
            OTCLog (@"mediaPillarMakeCall call feature");
            isMediaPillarMakeCall = YES;
        }

        // Our internal callID
        NSUUID *callID = [NSUUID UUID];
        
        // Instanciate and register our new RTCCall
        RTCCall *rtcCall = [RTCCall new];
        rtcCall.callID = callID;
        rtcCall.jingleSessionID = sessionID;
        rtcCall.peer = peer;
        rtcCall.resource = resource;
        rtcCall.isIncoming = YES;
        rtcCall.status = CallStatusRinging;
        rtcCall.features = features;
        // Disable Video capabilities for MediaPillar calls
        if (isMediaPillarCall)
            rtcCall.capabilities &= ~CallCapabilitiesVideo;
        [self addCall:rtcCall];

        void(^reportCallCompletion)(NSError*) = ^(NSError *error) {
            if (error) {
                // On iOS 10+ (CallKit) :
                // An incoming call may be disallowed by the system if, for example,
                // the caller handle is blocked, or the user has Do Not Disturb enabled.
                OTCLog (@"Error: didReceiveProposeMsg failed. %@", error);
                return;
            }
            
            if (!rtcCall.fakeCall) {
                if (isMediaPillarCall) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidReceiveIncomingMpCall object:rtcCall];
                    if (!isMediaPillarMakeCall) {
                        [self setupAudioSession];
                        [self startPlayingIncomingRingingTone];
                    }
                }
                else {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidAddCallNotification object:rtcCall];
                    if (!_isCallKitAvailable) {
                        [self setupAudioSession];
                        [self startPlayingIncomingRingingTone];
                    }
                }
            }
        };
        
        [self setupAudioSession];
        
        if (isMediaPillarMakeCall) {
           if (_isCallKitAvailable) {
                OTCLog (@"didReceiveProposeMsg : MediaPillarMakeCall put in CallKit as an outgoing call");
                CXHandle *remoteHandle = [[CXHandle alloc] initWithType:CXHandleTypeGeneric
                                                                  value:_mediaPillarAddress];
                CXStartCallAction *startCallAction = [[CXStartCallAction alloc] initWithCallUUID:rtcCall.callID handle:remoteHandle];
                startCallAction.contactIdentifier = rtcCall.peer.displayName; //(rtcCall.isMediaPillarCall) ? phoneNumber : rtcCall.peer.displayName;
                startCallAction.video = rtcCall.isVideoEnabled;
                
                CXTransaction *transaction = [CXTransaction new];
                [transaction addAction:startCallAction];
                [_callController requestTransaction:transaction completion:^(NSError *error) {
                    if (error) {
                        OTCLog (@"Error: cancelOutgoingCall failed. %@", error);
                        return;
                    }
                    [self setupAudioSession];
                }];
                return;
            } else {
                //TODO BW
         
            }
        }

       if (_isCallKitAvailable) {
            // On iOS 10+, use CallKit to handle the call.
            // CallKit will display the ringing UI for us.
            CXCallUpdate *callUpdate = [CXCallUpdate new];
            callUpdate.remoteHandle = [[CXHandle alloc] initWithType:CXHandleTypeGeneric value:peer.rtcJid];
            callUpdate.localizedCallerName = peer.displayName;
            callUpdate.supportsHolding = YES;
            callUpdate.supportsGrouping = NO;
            callUpdate.supportsUngrouping = NO;
            callUpdate.supportsDTMF = NO;
            callUpdate.hasVideo = rtcCall.isVideoEnabled;
           
            if (isMediaPillarCall && [UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
                [_xmppService.telephonyDelegate xmppService:_xmppService didReceiveMpCall:sessionID withPeer:peer andResource:resource];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    RTCCall *theRtcCall = [self getCallByCallId:callID];
                    if (theRtcCall) {
                        OTCLog(@"We delay the reportNewIncomingCallWithUUID from 6 seconds because we need to let the app to load.");
                        [_provider reportNewIncomingCallWithUUID:callID update:callUpdate completion:reportCallCompletion];
                    } else {
                        OTCLog(@"No RTCCall, it could be hangup on another device.");
                    }
                });
            } else {
                [_provider reportNewIncomingCallWithUUID:callID update:callUpdate completion:reportCallCompletion];
            }
        } else {
            // On iOS 8-9, CallKit is not available, so App should :
            // if we are in background -> show a UILocalNotification
            // if we are in foreground -> show a custom ringing UI
           //rtcCall.readyToDisplay = YES;
            // [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidAddCallNotification object:rtcCall];
            reportCallCompletion(nil);
        }
    }];
}

-(void) didReceiveRetractMsg:(NSString *) sessionID from:(Peer *) peer resource:(NSString *) resource {
    [_notificationsOperationQueue addOperationWithBlock:^{
        RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
        if (!rtcCall) {
            OTCLog (@"Error: we cannot handle <retract> unknown sessionID %@", sessionID);
            return;
        }
        // add this flag beacause some times the recipent accept the call at the same time the sender cancel it,
        // so we need to remove the call UI, at this case the status is RTCCallStatusConnecting
        
        if (rtcCall.status != CallStatusRinging  && rtcCall.status != CallStatusConnecting) {
            OTCLog (@"Error: cannot handle <retract> on non-ringing call %@", rtcCall);
            return;
        }
        
        // The RTCCall is not yet established, no need to close PeerConnection
        
        // For iOS 10+, notify CallKit the remote hangup the call
        if (_isCallKitAvailable) {
            [_provider reportCallWithUUID:rtcCall.callID endedAtDate:[NSDate date] reason:CXCallEndedReasonRemoteEnded];
        } else {
            [self stopPlayingOutgoingRingTone];
        }
        
        rtcCall.status = CallStatusCanceled;
        if (!rtcCall.fakeCall && (!rtcCall.isMediaPillarCall  || !_isCallKitAvailable))
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:rtcCall];
        
        [_calls removeObject:rtcCall];
        if (!rtcCall.fakeCall)
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:rtcCall];
    }];
}

-(void) didReceiveAcceptMsg:(NSString *) sessionID {
    [_notificationsOperationQueue addOperationWithBlock:^{
        RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
        if (!rtcCall) {
            OTCLog (@"Error: we cannot handle <accept> unknown sessionID %@", sessionID);
            return;
        }
        if (rtcCall.status == CallStatusConnecting) {
            OTCLog (@"ignore own <accept> on connecting call %@", rtcCall);
            return;
        }
        if (rtcCall.status != CallStatusRinging) {
            OTCLog (@"Error: cannot handle <accept> on non-ringing call %@", rtcCall);
            return;
        }
        
        [rtcCall.outgoingCallTimer invalidate];
        rtcCall.outgoingCallTimer = nil;
        
        // The RTCCall is not yet established, no need to close PeerConnection
        
        // For iOS 10+, notify CallKit to remove the call answered on another device
       if (_isCallKitAvailable) {
            [_provider reportCallWithUUID:rtcCall.callID endedAtDate:[NSDate date] reason:CXCallEndedReasonAnsweredElsewhere];
       } else {
           [self stopPlayingOutgoingRingTone];
       }
        
        rtcCall.status = CallStatusHangup;
        if (!rtcCall.fakeCall)
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:rtcCall];
        
        // This call has been accepted on another device, we can remove it from our local list
        [_calls removeObject:rtcCall];
        if (!rtcCall.fakeCall)
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:rtcCall];
    }];
}

-(void) didReceiveRejectMsg:(NSString *) sessionID from:(Peer *) peer resource:(NSString *) resource {
    [_notificationsOperationQueue addOperationWithBlock:^{
        RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
        if (!rtcCall) {
            OTCLog (@"Error: we cannot handle <reject> unknown sessionID %@", sessionID);
            return;
        }
        
        [rtcCall.outgoingCallTimer invalidate];
        rtcCall.outgoingCallTimer = nil;
        
        [self stopPlayingOutgoingRingTone];
        
        // The RTCCall is not yet established, no need to close PeerConnection
        
        // For iOS 10+, notify CallKit the remote hangup the call
        if (_isCallKitAvailable) {
            [_provider reportCallWithUUID:rtcCall.callID endedAtDate:[NSDate date] reason:CXCallEndedReasonDeclinedElsewhere];
        } else {
            [self stopPlayingOutgoingRingTone];
        }
        
        rtcCall.status = CallStatusHangup;
        if (!rtcCall.fakeCall)
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:rtcCall];
        
        [_calls removeObject:rtcCall];
        if (!rtcCall.fakeCall && (!rtcCall.isMediaPillarCall || ! _isCallKitAvailable)) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:rtcCall];
        
            [self startPlayingHangupTone];
        }
    }];
}

-(void) didReceiveProceedMsg:(NSString *) sessionID from:(Peer *) peer resource:(NSString *) resource {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: we cannot handle <proceed> unknown sessionID %@", sessionID);
        return;
    }
    if (rtcCall.status != CallStatusRinging) {
        OTCLog (@"Error: cannot handle <proceed> on non-ringing call %@", rtcCall);
        return;
    }
    
    // Very important !
    // This remote contact resource has accepted the call,
    // use it to "Gettin' Jingle With It" :p
    rtcCall.resource = resource;
    
    rtcCall.status = CallStatusConnecting;
    [rtcCall.outgoingCallTimer invalidate];
    rtcCall.outgoingCallTimer = nil;
    
    if (!rtcCall.connectionDate)
        rtcCall.connectionDate = [NSDate date];
    
    if (!rtcCall.fakeCall)
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:rtcCall];
    
    // The remote accepted our outgoing call.
    // WE have to start the jingle session-initiate
    [self prepareRTCOfferForCall:rtcCall iceRestart:YES completionHandler:^(RTCSessionDescription *offer) {
        OTCLog (@"Sending SDP Offer : %@", offer);
        [_xmppService sendJingleSDP:offer.sdp type:XMPPJingleMessageTypeSessionInitiate toTarget:rtcCall.fullJID sessionID:rtcCall.jingleSessionID peer:rtcCall.peer];
    }];
}

-(void) didReceiveMediaPillarProceedMsg:(NSString *) sessionID from:(NSString *) number resource:(NSString *) resource {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: we cannot handle <proceed> unknown sessionID %@", sessionID);
        return;
    }
    if (rtcCall.status != CallStatusRinging) {
        OTCLog (@"Error: cannot handle <proceed> on non-ringing call %@", rtcCall);
        return;
    }
    
    rtcCall.resource = resource;
    
    rtcCall.status = CallStatusConnecting;
    [rtcCall.outgoingCallTimer invalidate];
    rtcCall.outgoingCallTimer = nil;
    
    if (!rtcCall.connectionDate)
        rtcCall.connectionDate = [NSDate date];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:rtcCall];
    
    [self prepareRTCOfferForCall:rtcCall iceRestart:YES completionHandler:^(RTCSessionDescription *offer) {
        OTCLog (@"[MEDIAPILLAR] Sending Mediapillar SDP Offer : %@", offer);
        [_xmppService sendMediaPillarJingleSDP:offer.sdp toTarget:_mediaPillarAddress sessionID:rtcCall.jingleSessionID phoneNumber:number];
    }];
}

// Jingle

-(void) didReceiveSessionInitiate:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        // Try to search for a call with the given peer
        if ([peer.jid containsString:@"janus"]) {
            // check if we have a call with a janus already established
            RTCCall *janusCall = [self getCallByPeerContainingString:@"janus"];
            if (janusCall) {
                // We already have a call so we can consider this new session-initiate has valid ....
                NSUUID *callID = [NSUUID UUID];
                rtcCall = [RTCCall new];
                rtcCall.callID = callID;
                rtcCall.jingleSessionID = sessionID;
                rtcCall.peer = peer;
                rtcCall.resource = resource;
                rtcCall.isIncoming = YES;
                rtcCall.status = CallStatusRinging;
                [self addCall:rtcCall];

                rtcCall.fakeCall = YES;
                rtcCall.realCallID = janusCall.callID;
                [janusCall.linkedCalls addObject:rtcCall];
            }
        }
        if (!rtcCall) {
            OTCLog (@"Error: cannot handle session-initiate. Unknown call for sessionID '%@'", sessionID);
            return;
        }
    }
    
    OTCLog (@"didReceiveSessionInitiate for sessionID '%@'", sessionID);
    
    // With our new way-of-doing : we are auto-accepting jingle session-initiate.
    // The calling process (ringing-answer-decline) has already been done by the JingleMessageInitiation
    
    [self prepareRTCAnswerForCall:rtcCall withSDP:sdp iceRestart:NO completionHandler:^(RTCSessionDescription *answer) {
        OTCLog (@"Send SDP answer session accept %@",answer);
        [_xmppService sendJingleSDP:answer.sdp type:XMPPJingleMessageTypeSessionAccept toTarget:rtcCall.fullJID sessionID:rtcCall.jingleSessionID peer:rtcCall.peer];
    }];
}

-(void) didReceiveSessionAccept:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: cannot handle session-accept. Unknown call for sessionID %@", sessionID);
        return;
    }
    OTCLog (@"didReceiveSessionAccept for sessionID %@", sessionID);
    
    [rtcCall.outgoingCallTimer invalidate];
    rtcCall.outgoingCallTimer = nil;
    
    [self stopPlayingOutgoingRingTone];
    
    __weak RTCCall *weakRTCCall = rtcCall;
    RTCSessionDescription *session = [[RTCSessionDescription alloc] initWithType:RTCSdpTypeAnswer sdp:sdp];
    OTCLog (@"Set remote description for SDP %@", sdp);
    [rtcCall.peerConnection setRemoteDescription:session completionHandler:^(NSError *error) {
        if (error) {
            OTCLog (@"Failed to set session description for outgoing call %@. Error: %@", weakRTCCall, error);
            return;
        }
        
        // add the ICE candidate we may have missed.
        for (RTCIceCandidate *candidate in weakRTCCall.candidates) {
            [weakRTCCall.peerConnection addIceCandidate:candidate];
        }
        [weakRTCCall.candidates removeAllObjects];
        OTCLog (@"Set remote description done");
    }];
}

-(void) didReceiveSessionTerminate:(NSString *) sessionID from:(Peer *) peer {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: cannot handle session-terminate. Unknown call for sessionID %@", sessionID);
        return;
    }
    
    OTCLog (@"Session-terminate received for call %@", rtcCall);
    // We have to simulate the stream removed for the on the real call ?
    if (rtcCall.fakeCall) {
        RTCCall *realCall = [self getCallByCallId:rtcCall.realCallID];
        RTCMediaStream *stream = rtcCall.stream;
        if (stream)
            [self peerConnection:rtcCall.peerConnection didRemoveStream:stream];
        
        [realCall.linkedCalls removeObject:rtcCall];
        rtcCall.status = CallStatusHangup;
        //when the call ended from the puplisher ...
       
        __weak __typeof__(self) weakSelf = self;
        [self getAllStatsWithCompletionHandler:^(NSMutableArray *stats) {
            [self addWebrtcMetricsforCall:rtcCall andStatsArray:stats withCompletionHandler:^(NSError *error) {
                if (!error) {
                    OTCLog (@"add WebRTC metrics");
                }
                [weakSelf updateWebrtcMetricsForCall:rtcCall withCompletionHandler:^(NSError *error) {
                    if (!error) {
                        OTCLog (@"update WebRTC metrics");
                    }
                    [weakSelf removeCall:rtcCall];
                   
                }];
            }];
        }];
        
        
 
    } else {
        [self hangupCallNoSignaling:rtcCall];
        [self setupAudioSession];
        if (!rtcCall.isMediaPillarCall) // done by OXE
            [self startPlayingHangupTone];
    }
}


- (BOOL) isCallKitAvailable {
    NSLocale *userLocale = [NSLocale currentLocale];
    
   if ([[UIDevice currentDevice] systemMajorVersion] < 10)
       return NO;
    
    if ([userLocale.countryCode containsString: @"CN"] || [userLocale.countryCode containsString: @"CHN"]) {
        NSLog(@"currentLocale is China so we cannot use CallKit.");
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - Jingle RTP Sessions
-(void) didReceiveHoldJingleMessage:(NSString *) sessionID {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: no matching RTCCall found in didReceiveHoldJingleMessage for sessionID %@", sessionID);
        return;
    }
    
    [self startPlayingHoldTone];
}

-(void) didReceiveUnholdJingleMessage:(NSString *) sessionID {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: no matching RTCCall found in didReceiveUnholdJingleMessage for sessionID %@", sessionID);
        return;
    }
    
    [self stopPlayingHoldTone];
}

#pragma mark - Transport
-(void) didReceiveTransportInfo:(NSString *) sessionID candidate:(NSDictionary *)candidate from:(Peer *) peer {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: no matching RTCCall found in didReceiveTransportInfo for sessionID %@", sessionID);
        return;
    }
    
    OTCLog (@"didReceiveTransportInfo for sessionID %@ adding candidate %@", sessionID, candidate);
    
    NSString *medium = [candidate objectForKey:@"id"];
    // remove the starting 'a='
    NSString *sdp = [[candidate objectForKey:@"candidate"] substringFromIndex:2];
    
    RTCIceCandidate *iceCandidate = [[RTCIceCandidate alloc] initWithSdp:sdp sdpMLineIndex:0 sdpMid:medium];
    
    // peerConnection might not exists yet
    if (rtcCall.peerConnection && rtcCall.peerConnection.remoteDescription) {
        [rtcCall.peerConnection addIceCandidate:iceCandidate];
    } else {
        [rtcCall.candidates addObject:iceCandidate];
    }
    
    OTCLog (@"Candidate added %@", candidate);
}

-(void) didReceiveTransportAccept:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: no matching RTCCall found in didReceiveTransportAccept for sessionID %@", sessionID);
        return;
    }
    
    OTCLog (@"didReceiveTransportAccept for sessionID %@", sessionID);
    
    if (rtcCall.peerConnection.signalingState != RTCSignalingStateStable) {
        RTCSessionDescription *session = [[RTCSessionDescription alloc] initWithType:RTCSdpTypeAnswer sdp:sdp];
        OTCLog (@"Set remote description");
        __weak __typeof(rtcCall) weakRTCCall = rtcCall;
        [rtcCall.peerConnection setRemoteDescription:session completionHandler:^(NSError * _Nullable error) {
            OTCLog (@"Set remote description done");

            // When we receive the transport-accept we can set the localDescription with the offer
            [weakRTCCall.peerConnection setLocalDescription:weakRTCCall.offer completionHandler:^(NSError * _Nullable error) {
                OTCLog (@"Set local description done");
            }];
        }];
    }
}

-(void) didReceiveTransportReplace:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: cannot handle transport-replace. Unknown call for sessionID %@", sessionID);
        return;
    }
    
    OTCLog (@"didReceiveTransportReplace for sessionID %@", sessionID);
    
    __weak RTCCall *weakRTCCall = rtcCall;
    __weak typeof(self) weakSelf = self;
    
    // We have to update the ressource of the call on transport-replace, because the web client change his ressource name when it lost the network connection.
    // So we must answer him on is new ressource
    rtcCall.resource = resource;
    
    // Create a answer and send it
    [weakSelf prepareRTCAnswerForCall:weakRTCCall withSDP:sdp iceRestart:NO completionHandler:^(RTCSessionDescription *answer) {
        OTCLog (@"Send SDP answer transport accept %@", answer);
        [_xmppService sendJingleSDP:answer.sdp type:XMPPJingleMessageTypeTransportAccept toTarget:weakRTCCall.fullJID sessionID:weakRTCCall.jingleSessionID peer:weakRTCCall.peer];
    }];
}

#pragma mark - Content
-(void) didReceiveContentAdd:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: cannot handle content-add. Unknown call for sessionID %@", sessionID);
        return;
    }
    OTCLog (@"DidReceiveContent Add %@", sessionID);
    if (!rtcCall.fakeCall)
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:rtcCall];
    
    __weak RTCCall *weakRTCCall = rtcCall;
    // Create a answer and send it
    [self prepareRTCAnswerForCall:rtcCall withSDP:sdp iceRestart:NO completionHandler:^(RTCSessionDescription *answer) {
        OTCLog (@"Send SDP answer content accept %@",answer);
        [_xmppService sendJingleSDP:answer.sdp type:XMPPJingleMessageTypeContentAccept toTarget:weakRTCCall.fullJID sessionID:weakRTCCall.jingleSessionID peer:rtcCall.peer];
    }];
}

-(void) didReceiveContentModify:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: cannot handle transport-replace. Unknown call for sessionID %@", sessionID);
        return;
    }
    
    __weak RTCCall *weakRTCCall = rtcCall;
    
    OTCLog (@"Did receive content modify %@", sessionID);
    
    if (!rtcCall.fakeCall)
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:rtcCall];
    
    // Create a answer and send it
    [self prepareRTCAnswerForCall:rtcCall withSDP:sdp iceRestart:NO completionHandler:^(RTCSessionDescription *answer) {
        OTCLog (@"Send SDP answer content accept %@", answer);
        [_xmppService sendJingleSDP:answer.sdp type:XMPPJingleMessageTypeContentAccept toTarget:weakRTCCall.fullJID sessionID:weakRTCCall.jingleSessionID peer:rtcCall.peer];
    }];
}

-(void) didReceiveContentRemove:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: cannot handle content-add. Unknown call for sessionID %@", sessionID);
        return;
    }
    OTCLog (@"Did receive content delete %@", sessionID);

    __weak RTCCall *weakRTCCall = rtcCall;
    // Create a answer and send it
    [self prepareRTCAnswerForCall:rtcCall withSDP:sdp iceRestart:NO completionHandler:^(RTCSessionDescription *answer) {
        OTCLog (@"Send SDP answer content accept %@", answer);
        [_xmppService sendJingleSDP:answer.sdp type:XMPPJingleMessageTypeContentAccept toTarget:weakRTCCall.fullJID sessionID:weakRTCCall.jingleSessionID peer:weakRTCCall.peer];
    }];
}

-(void) didReceiveContentAccept:(NSString *) sessionID sdp:(NSString *) sdp from:(Peer *) peer resource:(NSString *) resource {
    RTCCall *rtcCall = [self getCallByJingleSessionId:sessionID];
    if (!rtcCall) {
        OTCLog (@"Error: cannot handle content-add. Unknown call for sessionID %@", sessionID);
        return;
    }
    
    OTCLog (@"Did receive content Accept %@", sessionID);
    rtcCall.isModifingMedia = NO;
    
    __weak RTCCall *weakRTCCall = rtcCall;
    
    OTCLog (@"Set Local description");
    [rtcCall.peerConnection setLocalDescription:rtcCall.offer completionHandler:^(NSError * _Nullable error) {
        if (error) {
            OTCLog (@"Failed to set local session description on answering call. Error: %@", error);
            return;
        }
    }];
    
    RTCSessionDescription *session = [[RTCSessionDescription alloc] initWithType:RTCSdpTypeAnswer sdp:sdp];
    OTCLog (@"Set remote description");
    [rtcCall.peerConnection setRemoteDescription:session completionHandler:^(NSError *error) {
        if (error) {
            OTCLog (@"Failed to set session description for outgoing call %@. Error: %@", weakRTCCall, error);
            return;
        }

        
        // add the ICE candidate we may have missed.
        for (RTCIceCandidate *candidate in weakRTCCall.candidates) {
            [weakRTCCall.peerConnection addIceCandidate:candidate];
        }
        [weakRTCCall.candidates removeAllObjects];
        
        OTCLog (@"Set remote description done");
    }];
}

#pragma mark - Internal WebRTC functions

-(void) prepareRTCOfferForCall:(RTCCall *)rtcCall iceRestart:(BOOL) iceRestart completionHandler:(RTCServicePrepareOfferCompletionHandler)completionHandler {
    NSAssert(completionHandler, @"You should have a completion handler, to send the SDP through sig-channel or handle errors");
    
    // Start the peer connection
    [self startPeerConnectionForCall:rtcCall];
    
    // Compute our constraints depending on call type
    NSDictionary *mandatoryConstraints = [self getMediaContraintsForCall:rtcCall iceRestart:iceRestart];
    
    RTCMediaConstraints* mediaConstraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    
    __weak RTCCall *weakRTCCall = rtcCall;
    
    // Create the offer with our constraints
    OTCLog (@"Request new offer with constraint %@", mediaConstraints);
    [rtcCall.peerConnection offerForConstraints:mediaConstraints completionHandler:^(RTCSessionDescription *offerSdp, NSError *error) {
        if (error) {
            OTCLog (@"Failed to create offer session description for call %@. Error: %@", weakRTCCall, error);
            return;
        }
        
        // We don't set the local description in case of ice Restart because it must be done when we got the transport-accept answer
        // We don't set the local description in case we have changed our media(add/remove video) to allow crossing add media to work correctly
        if (!weakRTCCall.iceFailed && !weakRTCCall.isModifingMedia) {
            // Set the newly created SDP as our local-description
            [weakRTCCall.peerConnection setLocalDescription:offerSdp completionHandler:^(NSError *error) {
                if (error) {
                    OTCLog (@"Failed to set local session description on offering call. Error: %@", error);
                    return;
                }
            }];
        } else {
            OTCLog (@"Not setting the local offer there is a iceFailed or a mediaModification");
        }
        
        // Make sure to put H264 codec on top of the list
        // so it will be interpreted as "preferred-codec"
        // If we don't call this method for all call (audio call too) the app crash because there is no sdp ....
        RTCSessionDescription *sdpPreferringH264 = [offerSdp descriptionWithPreferredVideoCodec:@"H264"];
        // We keep the created offer just in case we need it later
        weakRTCCall.offer = sdpPreferringH264;
        OTCLog (@"Request new offer done");
        
        if (completionHandler) {
            completionHandler(sdpPreferringH264);
        }
    }];
}


-(void) prepareRTCAnswerForCall:(RTCCall *)rtcCall withSDP:(NSString *) sdp iceRestart:(BOOL) iceRestart completionHandler:(RTCServicePrepareAnswerCompletionHandler)completionHandler {
    
    //Should display the call view before creating the local media stream
    if ( (rtcCall.status == CallStatusEstablished || rtcCall.status == CallStatusRinging || rtcCall.status == CallStatusConnecting) && !(rtcCall.isMediaPillarCall) && rtcCall.isVideoEnabled)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showCallView" object:rtcCall];
    
    // Start the peer connection
    [self startPeerConnectionForCall:rtcCall];
    
    __weak RTCCall *weakRTCCall = rtcCall;
    
    // Set the remote SDP we have received as an *offer* (i.e. offer from the remote peer)
    RTCSessionDescription *session = [[RTCSessionDescription alloc] initWithType:RTCSdpTypeOffer sdp:sdp];
    
    // Force received SDP to use H264 in priority
    RTCSessionDescription *receivedSDPWithHD64 = [session descriptionWithPreferredVideoCodec:@"H264"];
    
    OTCLog (@"Set remote description");
    [rtcCall.peerConnection setRemoteDescription:receivedSDPWithHD64 completionHandler:^(NSError *error) {
        if (error) {
            OTCLog (@"Failed to set session description for incoming call %@. Error: %@", weakRTCCall, error);
            return;
        }
        
        NSDictionary *mandatoryConstraints = [self getMediaContraintsForCall:weakRTCCall iceRestart:iceRestart];
        
        RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
        OTCLog (@"Request answer");
        [weakRTCCall.peerConnection answerForConstraints:constraints completionHandler:^(RTCSessionDescription *answerSdp, NSError *error) {
            if (error) {
                OTCLog (@"Failed to create session description answer for call %@. Error: %@", weakRTCCall, error);
                return;
            }
            
            RTCSessionDescription *sdpPreferringH264 = [answerSdp descriptionWithPreferredVideoCodec:@"H264"];
            
            // We keep the created offer just in case we need it later
            weakRTCCall.answer = sdpPreferringH264;
            
            NSAssert(completionHandler, @"You should have a completion handler, to send the SDP through sig-channel");
            if (completionHandler) {
                completionHandler(sdpPreferringH264);
            }
            
            OTCLog (@"Set Local description");
            // Set the newly created SDP as our local-description
            [weakRTCCall.peerConnection setLocalDescription:sdpPreferringH264 completionHandler:^(NSError *error) {
                if (error) {
                    OTCLog (@"Failed to set local session description on answering call. Error: %@", error);
                    return;
                }
            }];
            
            // add the ICE candidate we may have missed.
            for (RTCIceCandidate *candidate in weakRTCCall.candidates) {
                [weakRTCCall.peerConnection addIceCandidate:candidate];
            }
            [weakRTCCall.candidates removeAllObjects];
        }];
    }];
}

-(void) startPeerConnectionForCall:(RTCCall *)rtcCall {
    if (rtcCall.peerConnection) {
        OTCLog (@"We already have peerConnection so don't create a new one");
        return;
    }
    OTCLog (@"Create peer connection");
    /*
     * Allowed constraints for peer connection are :
     * - kEnableIPv6 / googIPv6
     * - kEnableDscp / googDscp
     * - kCpuOveruseDetection / googCpuOveruseDetection
     * - kEnableRtpDataChannels / RtpDataChannels
     * - kEnableVideoSuspendBelowMinBitrate / googSuspendBelowMinBitrate
     * - kScreencastMinBitrate / googScreencastMinBitrate (int)
     * - kCombinedAudioVideoBwe / googCombinedAudioVideoBwe (bool)
     * - kEnableDtlsSrtp / DtlsSrtpKeyAgreement (bool)
     */
    NSMutableDictionary *mandatoryConstraints = [NSMutableDictionary new];
    NSDictionary *optionalConstraints = @{@"DtlsSrtpKeyAgreement": kRTCMediaConstraintsValueTrue,@"googDscp":kRTCMediaConstraintsValueTrue, @"RtpDataChannels":kRTCMediaConstraintsValueTrue };
    
    // The Media PeerConnection constraints
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:optionalConstraints];
    
    if ([_availableIceServers count] == 0) {
        OTCLog (@"Warning: no IceServers available, communication might fail: %@", _iceServers);
    }
    
    // RTCConfiguration with IceServers
    RTCConfiguration *config = [[RTCConfiguration alloc] init];
    config.iceServers = _availableIceServers;
    config.continualGatheringPolicy = RTCContinualGatheringPolicyGatherContinually;
    RTCPeerConnection *peerConnection = [_factory peerConnectionWithConfiguration:config constraints:constraints delegate:self];
    rtcCall.peerConnection = peerConnection;
    if (!rtcCall.peerConnection) {
        OTCLog (@"Peer connection is not here ! That is not good ! %@", config);
    }
    // Create AV media stream and add it to the peer connection.
    // Our delegate must be ready to receive didReceiveLocalVideoTrack event.
    RTCMediaStream *localStream = [self createLocalMediaStreamForCall:rtcCall];
    [rtcCall.peerConnection addStream:localStream];
    
    // Start the stats fetching timer in the main queue
    
    //disable stat fetching in current version
    //_statsTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(getStats:) userInfo:nil repeats:YES];
    //[[NSRunLoop mainRunLoop] addTimer:_statsTimer forMode:NSDefaultRunLoopMode];
    OTCLog (@"PeerConnection : %@", peerConnection);
}

-(void) createVideoTrackForLocalStream:(RTCMediaStream *) localStream {
    // Create our VideoSource and VideoTrack
    OTCLog (@"Create video track");
    /*
     * Allowed constraints for local video source are :
     * - minWidth
     * - maxWidth
     * - minHeight
     * - maxHeight
     * - minFrameRate
     * - maxFrameRate
     * - minAspectRatio
     * - maxAspectRatio
     * - googNoiseReduction
     */
    
    RTCVideoSource *source = [_factory videoSource];
    RTCCameraVideoCapturer *capturer = [[RTCCameraVideoCapturer alloc] initWithDelegate:source];
    
    RTCVideoTrack* localVideoTrack = [_factory videoTrackWithSource:source trackId:[NSString stringWithFormat:@"iOS-video-track-%u", arc4random()]];
    [localStream addVideoTrack:localVideoTrack];
    
    // We must notify the view controller with the capturer
    [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidAddCaptureSessionNotification object:capturer];
    
    // We must notify that a video track has been added
    [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidAddLocalVideoTrackNotification object:localVideoTrack];
    OTCLog (@"Video track created");
}

-(void) createAudioTrackForLocalStream:(RTCMediaStream *) localStream {
    // Create our AudioSource and AudioTrack
    OTCLog (@"Create audio track");
    /*
     * Allowed constraints for local audio source are : (true/false value)
     * - googEchoCancellation
     * - googEchoCancellation2 (extended)
     * - googDAEchoCancellation
     * - googAutoGainControl
     * - googAutoGainControl2 (experimental)
     * - googNoiseSuppression
     * - googNoiseSuppression2 (experimental)
     * - intelligibilityEnhancer
     * - levelControl
     * - levelControlInitialPeakLevelDBFS (float value, not bool !)
     * - googHighpassFilter
     * - googTypingNoiseDetection
     * - googAudioMirroring
     */
    NSMutableDictionary *mandatoryConstraints = [[NSMutableDictionary alloc] init];
    [mandatoryConstraints setObject:kRTCMediaConstraintsValueTrue forKey:@"googEchoCancellation"];
    [mandatoryConstraints setObject:kRTCMediaConstraintsValueTrue forKey:@"googEchoCancellation2"];
    [mandatoryConstraints setObject:kRTCMediaConstraintsValueTrue forKey:@"googAutoGainControl"];
    [mandatoryConstraints setObject:kRTCMediaConstraintsValueTrue forKey:@"googAutoGainControl2"];
    [mandatoryConstraints setObject:kRTCMediaConstraintsValueTrue forKey:@"googNoiseSuppression"];
    [mandatoryConstraints setObject:kRTCMediaConstraintsValueTrue forKey:@"googNoiseSuppression2"];
    [mandatoryConstraints setObject:kRTCMediaConstraintsValueTrue forKey:@"googHighpassFilter"];
    
    RTCMediaConstraints* mediaConstraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    
    RTCAudioSource *source = [_factory audioSourceWithConstraints:mediaConstraints];
    RTCAudioTrack *audioTrack = [_factory audioTrackWithSource:source trackId:[NSString stringWithFormat:@"iOS-audio-track-%u", arc4random()]];
    [localStream addAudioTrack:audioTrack];
    OTCLog (@"Audio track created");
}

- (RTCMediaStream *)createLocalMediaStreamForCall:(RTCCall *) call {
    RTCMediaStream* localStream = [_factory mediaStreamWithStreamId:[NSString stringWithFormat:@"iOS-stream-%u", arc4random()]];
    
    if (call.features & RTCCallFeatureLocalVideo) {
        [self createVideoTrackForLocalStream:localStream];
    }
    
    if (call.features & RTCCallFeatureAudio) {
        [self createAudioTrackForLocalStream:localStream];
    }
    
    return localStream;
}


#pragma mark - RTCPeerConnectionDelegate
// Callbacks for this delegate occur on non-main thread and need to be
// dispatched back to main queue as needed.

/** Called when the SignalingState changed. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeSignalingState:(RTCSignalingState)stateChanged {
    OTCLog (@"Signaling state changed: %@", [RTCService stringForRTCSignalingState:stateChanged]);
}

/** Called when media is received on a new stream from remote peer. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didAddStream:(RTCMediaStream *)stream {
    
    RTCCall *rtcCall = [self getCallByPeerConnection:peerConnection];
    if (!rtcCall) {
        OTCLog (@"Error: no matching RTCCall found in peerConnection:addedStream:");
        return;
    }
    
    [self addStream:stream inRTCCall:rtcCall];
    
    if (rtcCall.fakeCall) {
        RTCCall *realCall = [self getCallByCallId:rtcCall.realCallID];
        if (realCall) {
            // Add fake call feature to the real call
            realCall.features |= rtcCall.features;
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:realCall];
            
            if (realCall.isRemoteVideoEnabled) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidAddRemoteVideoTrackNotification object:nil];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRemoveRemoteVideoTrackNotification object:nil];
            }
            
            if (realCall.isSharingEnabled) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidAddRemoteVideoTrackNotification object:nil];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRemoveRemoteVideoTrackNotification object:nil];
            }
        }
    }
}

-(void) addStream:(RTCMediaStream *) stream inRTCCall:(RTCCall *) rtcCall {
    OTCLog (@"Current stream %@ didAddStream %@", rtcCall.stream, stream);
    
    if (rtcCall.peer.mediaType == PeerMediaTypeVideoSharing) {
        OTCLog (@"We are in video+sharing");
        if (rtcCall.stream) {
            if (rtcCall.stream.audioTracks.count == 0 && rtcCall.stream.videoTracks.count == 1 && stream.audioTracks.count == 1 && stream.videoTracks.count == 0) {
                OTCLog (@"Audio added to Sharing");
                [stream addVideoTrack:rtcCall.stream.videoTracks[0]];
                rtcCall.features |= RTCCallFeatureAudio;
                rtcCall.features |= RTCCallFeatureRemoteSharing;
            }
            
            if (rtcCall.stream.audioTracks.count == 1 && rtcCall.stream.videoTracks.count == 0 && stream.audioTracks.count == 0 && stream.videoTracks.count == 1) {
                OTCLog (@"Sharing added to Audio");
                [stream addAudioTrack:rtcCall.stream.audioTracks[0]];
                rtcCall.features |= RTCCallFeatureAudio;
                rtcCall.features |= RTCCallFeatureRemoteSharing;
            }
            
            if (rtcCall.stream.audioTracks.count == 0 && rtcCall.stream.videoTracks.count == 1 && stream.audioTracks.count == 1 && stream.videoTracks.count == 1) {
                OTCLog (@"Add video to sharing");
                RTCVideoTrack *previousVideoTrack = rtcCall.stream.videoTracks[0];
                
                RTCVideoTrack *currentVideoTrack = stream.videoTracks[0];
                [stream removeVideoTrack:currentVideoTrack];
                [stream addVideoTrack:previousVideoTrack];
                [stream addVideoTrack:currentVideoTrack];
                OTCLog (@"stream after addVideo %@", stream.videoTracks);
                
                rtcCall.features |= RTCCallFeatureAudio;
                rtcCall.features |= RTCCallFeatureRemoteSharing;
                rtcCall.features |= RTCCallFeatureRemoteVideo;
            }
            
            if (rtcCall.stream.audioTracks.count == 1 && rtcCall.stream.videoTracks.count == 1 && stream.audioTracks.count == 0 && stream.videoTracks.count == 1) {
                OTCLog (@"Sharing added to Audio+Video");
                [stream addAudioTrack:rtcCall.stream.audioTracks[0]];
                [stream addVideoTrack:rtcCall.stream.videoTracks[0]];
                rtcCall.features |= RTCCallFeatureAudio;
                rtcCall.features |= RTCCallFeatureRemoteSharing;
                rtcCall.features |= RTCCallFeatureRemoteVideo;
            }
            
            if (rtcCall.stream.audioTracks.count == 1 && rtcCall.stream.videoTracks.count == 1 && stream.audioTracks.count == 1 && stream.videoTracks.count == 1) {
                OTCLog (@"Video added to Audio+Sharing");
                [stream addVideoTrack:rtcCall.stream.videoTracks[0]];
                rtcCall.features |= RTCCallFeatureAudio;
                rtcCall.features |= RTCCallFeatureRemoteSharing;
                rtcCall.features |= RTCCallFeatureRemoteVideo;
            }
            
            if (rtcCall.stream.audioTracks.count == 1 && rtcCall.stream.videoTracks.count == 2 && stream.audioTracks.count == 1 && stream.videoTracks.count == 0) {
                OTCLog (@"Video removed from Audio+Video+Sharing");
                [stream addVideoTrack:rtcCall.stream.videoTracks[1]];
                rtcCall.features |= RTCCallFeatureAudio;
                rtcCall.features |= RTCCallFeatureRemoteSharing;
                rtcCall.features &= ~RTCCallFeatureRemoteVideo;
            }
            
            if (rtcCall.stream.audioTracks.count == 1 && rtcCall.stream.videoTracks.count == 2 && stream.audioTracks.count == 1 && stream.videoTracks.count == 1) {
                OTCLog (@"Sharing removed from Audio+Video+Sharing");
                rtcCall.features |= RTCCallFeatureAudio;
                rtcCall.features &= ~RTCCallFeatureRemoteSharing;
                rtcCall.features |= RTCCallFeatureRemoteVideo;
            }
        }
    } else {
        if (stream.audioTracks.count == 0 && stream.videoTracks.count == 1) {
            if (rtcCall.peer.mediaType == PeerMediaTypeSharing)
                rtcCall.features |= RTCCallFeatureRemoteSharing;
            if (rtcCall.peer.mediaType == PeerMediaTypeVideo)
                rtcCall.features |= RTCCallFeatureRemoteVideo;
        }
        if (stream.audioTracks.count == 1 && stream.videoTracks.count == 0) {
            if (rtcCall.features & RTCCallFeatureRemoteSharing) {
                OTCLog (@"We were in sharing, we remove it");
                rtcCall.features &= ~RTCCallFeatureRemoteSharing;
            }
        }
        if (stream.audioTracks.count == 1 && stream.videoTracks.count == 1) {
            rtcCall.features |= RTCCallFeatureRemoteVideo;
            if (rtcCall.features & RTCCallFeatureRemoteSharing) {
                OTCLog (@"We were in sharing, we remove it");
                rtcCall.features &= ~RTCCallFeatureRemoteSharing;
            }
        }
        if (rtcCall.stream.audioTracks.count == 1 && rtcCall.stream.videoTracks.count == 2 &&  stream.audioTracks.count == 1 && stream.videoTracks.count == 0) {
            OTCLog (@"We were in sharing + Video, we remove ????");
            if (rtcCall.peer.mediaType == PeerMediaTypeSharing) {
                rtcCall.features &= ~RTCCallFeatureRemoteVideo;
                [stream addVideoTrack:rtcCall.stream.videoTracks[0]];
            }
            if (rtcCall.peer.mediaType == PeerMediaTypeVideo) {
                rtcCall.features &= ~RTCCallFeatureRemoteSharing;
                [stream addVideoTrack:rtcCall.stream.videoTracks[1]];
            }
        }
        if (rtcCall.stream.audioTracks.count == 1 && rtcCall.stream.videoTracks.count == 1 &&  stream.audioTracks.count == 1 && stream.videoTracks.count == 0) {
            OTCLog (@"Video removed");
            rtcCall.features &= ~RTCCallFeatureRemoteVideo;
            if (rtcCall.features & RTCCallFeatureRemoteSharing) {
                OTCLog (@"We were in sharing, we remove it");
                rtcCall.features &= ~RTCCallFeatureRemoteSharing;
            }
        }
    }
    if (stream.audioTracks.count == 0 && stream.videoTracks.count == 0) {
        OTCLog (@"There is nothing in the stream don't save it");
    } else
        rtcCall.stream = stream;
    
    if (!rtcCall.fakeCall)
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:rtcCall];
    
    if (rtcCall.isRemoteVideoEnabled) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidAddRemoteVideoTrackNotification object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRemoveRemoteVideoTrackNotification object:nil];
    }
    
    if (rtcCall.isSharingEnabled) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidAddRemoteVideoTrackNotification object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRemoveRemoteVideoTrackNotification object:nil];
    }
}

/** Called when a remote peer closes a stream. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didRemoveStream:(RTCMediaStream *)stream {
    RTCCall *rtcCall = [self getCallByPeerConnection:peerConnection];
    if (!rtcCall) {
        OTCLog (@"Error: no matching RTCCall found in peerConnection:removedStream:");
        return;
    }
    RTCCallFeatureFlags flag = rtcCall.features;
    [self removeStream:stream forRTCCall:rtcCall];
    
    if (rtcCall.fakeCall) {
        RTCCall *realCall = [self getCallByCallId:rtcCall.realCallID];
        if (realCall) {
            // Remove fake call feature from the real call
            realCall.features &= ~flag;
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:realCall];
            
            if (realCall.isRemoteVideoEnabled) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidAddRemoteVideoTrackNotification object:nil];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRemoveRemoteVideoTrackNotification object:nil];
            }
            
            if (realCall.isSharingEnabled) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidAddRemoteVideoTrackNotification object:nil];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRemoveRemoteVideoTrackNotification object:nil];
            }
        }
    }
}

-(void) removeStream:(RTCMediaStream *) stream forRTCCall:(RTCCall *) rtcCall {
    OTCLog (@"REMOVE STREAM %@ current stream %@", stream, rtcCall.stream);
    if ([stream isEqual:rtcCall.stream]) {
        rtcCall.stream = nil;
        
        rtcCall.features &= ~RTCCallFeatureAudio;
        rtcCall.features &= ~RTCCallFeatureRemoteVideo;
        rtcCall.features &= ~RTCCallFeatureRemoteSharing;
        rtcCall.features &= ~RTCCallFeatureLocalVideo;
        
        if (!rtcCall.fakeCall && !rtcCall.isMediaPillarCall)
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:rtcCall];
        
        if (stream.videoTracks.count > 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRemoveRemoteVideoTrackNotification object:nil];
        }
    }
}

/** Called when negotiation is needed, for example ICE has restarted. */
- (void)peerConnectionShouldNegotiate:(RTCPeerConnection *)peerConnection {
    OTCLog (@"Renegotiation needed");
    // I guess we always go here because we have separated session and transport messages
    // and the initial SDP offer does not contain any ICE candidates.
    // We can ignore this renegotiation.
}

/** Called any time the IceConnectionState changes. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeIceConnectionState:(RTCIceConnectionState)newState {
    OTCLog (@"Ice Connection state changed: %@", [RTCService stringForRTCIceConnectionState:newState]);
    RTCCall *call = [self getCallByPeerConnection:peerConnection];
    if (call.peerConnection != peerConnection)
        return;
    
    if (call.status == CallStatusHangup) {
        OTCLog (@"Don't treat the new ice state we are already in hangup state");
        return;
    }
    switch (newState) {
        case RTCIceConnectionStateConnected :{
            OTCLog (@"Call is connected");
            call.iceFailed = NO;
            call.status = CallStatusEstablished;
            call.capabilities = CallCapabilitiesAll;
            // Remove video capabilities with mediapillar feature
            if (call.isMediaPillarCall)
                call.capabilities &= ~CallCapabilitiesVideo;
            [self stopPlayingOutgoingRingTone];
            [self stopPlayingIncomingRingingTone];
            if (_isCallKitAvailable) {
                if (call.isIncoming) {
                    [((CXAnswerCallAction*)call.actionToFullFill) fulfillWithDateConnected:[NSDate date]];
                    call.actionToFullFill = nil;
                }
                [_provider reportOutgoingCallWithUUID:call.callID connectedAtDate:[NSDate date]];
            } else {
                //ToDo connect audio
                AVAudioSession* audioSession = [AVAudioSession sharedInstance];
                [audioSession setActive:YES error:nil];
                [self activateAudioSession:audioSession];
            }
            
            if (!call.fakeCall && (!call.isMediaPillarCall || !_isCallKitAvailable) )
                [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:call];
            break;
        }
        case RTCIceConnectionStateCompleted :
            break;
            
        case RTCIceConnectionStateNew :
        case RTCIceConnectionStateChecking : {
            OTCLog (@"Call is connecting");
            call.status = CallStatusConnecting;
            if (!call.fakeCall)
                [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:call];
            break;
        }
        case RTCIceConnectionStateClosed : {
            OTCLog (@"Call is disconnected");
            call.status = CallStatusHangup;
            
            if (!_isCallKitAvailable) {
                AVAudioSession* audioSession = [AVAudioSession sharedInstance];
                [audioSession setActive:NO error:nil];
                [self deactivateAudioSession:audioSession];
            }
            
            if (!call.fakeCall)
                [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:call];
            break;
        }
        case RTCIceConnectionStateDisconnected :{
            // Passé en état connection en cours
            call.status = CallStatusConnecting;
            if (!call.fakeCall)
                [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:call];
            break;
        }
        case RTCIceConnectionStateFailed : {
            // Passé en état connection en cours
            call.status = CallStatusConnecting;
            if (!call.fakeCall)
                [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:call];
#pragma mark - ICE FAILED
            OTCLog (@"ICE FAILED");
            if (!_xmppService.isXmppConnected) {
                call.iceFailed = YES;
                __weak RTCCall *weakRTCCall = call;
                [self prepareRTCOfferForCall:call iceRestart:YES completionHandler:^(RTCSessionDescription *offer) {
                    OTCLog (@"Sending SDP Offer : %@", offer);
                    [_xmppService sendJingleSDP:offer.sdp type:XMPPJingleMessageTypeTransportReplace toTarget:weakRTCCall.fullJID sessionID:weakRTCCall.jingleSessionID peer:weakRTCCall.peer];
                }];
            }
            break;
        }
        
        default:
            break;
    }
}

/** Called any time the IceGatheringState changes. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeIceGatheringState:(RTCIceGatheringState)newState {
    OTCLog (@"Ice Gathering state changed: %@", [RTCService stringForRTCIceGatheringState:newState]);
}

/** New ice candidate has been found. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didGenerateIceCandidate:(RTCIceCandidate *)candidate {
    RTCCall *rtcCall = [self getCallByPeerConnection:peerConnection];
    if (!rtcCall) {
        OTCLog (@"Error: no matching RTCCall found in peerConnection:didGenerateIceCandidate:");
        return;
    }
    
    if (rtcCall.peerConnection != peerConnection)
        return;
    
    // example of candidate :
    // video:1:candidate:3427893008 2 tcp 1518280446 10.28.202.54 56002 typ host tcptype passive generation 0 ufrag DfVl1fKZomHbK8El
    OTCLog (@"Got an Ice Candidate : %@", candidate);
    if (![candidate.sdp containsString:@"169.254."]) //see RFC5735 for 169.254.x.x block
        [_xmppService sendJingleTransportInfoWithCandidate:candidate
                                                  toTarget:(rtcCall.isMediaPillarCall) ? _mediaPillarAddress : rtcCall.fullJID
                                                 sessionID:rtcCall.jingleSessionID
                                                      peer:rtcCall.peer];
    else
        OTCLog (@"Do not use local link address as Ice Candidate"); //it's only when iPhone is connected to MAC USB port ...
}

/** Called when a group of local Ice candidates have been removed. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didRemoveIceCandidates:(NSArray<RTCIceCandidate *> *)candidates{
    OTCLog (@"Ice Candidates removed : %@", candidates);
    // in XEP-0176 Jingle ICE-UDP
    // There is no way to "remove" a candidate
    // so we don't handle this.
}

/** New data channel has been opened. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didOpenDataChannel:(RTCDataChannel *)dataChannel {
    OTCLog (@"didOpenDataChannel");
}


#pragma mark - Stats

-(void) getStats:(NSTimer *)timer {
    // Get stats for all the available calls and for each call, for all the available tracks.
    // In this function which is triggered every 1 sec by our timer,
    // we simply call RTCGetAndResetMetrics()
    // which will then trigger our delegate peerConnection:didGetStats:
    
    // Legacy stats ? -> New way of getting stats not found
    // RTCGetAndResetMetrics() does not seems to fit my needs.
    
    RTCStatsOutputLevel level = RTCStatsOutputLevelStandard;
    @synchronized(_calls) {

        for (RTCCall *rtcCall in _calls) {
            // Stats on my local streams.
            for (RTCMediaStream *stream in rtcCall.peerConnection.localStreams) {
                for (RTCAudioTrack *audioTrack in stream.audioTracks) {
                    [rtcCall.peerConnection statsForTrack:(RTCMediaStreamTrack *)audioTrack statsOutputLevel:level completionHandler:^(NSArray<RTCLegacyStatsReport *> *stats) {
                        NSLog(@"didGetStats:stats audio localStream");
                        [self didGetStats:stats];
                    }];
                }
                for (RTCVideoTrack *videoTrack in stream.videoTracks) {
                    [rtcCall.peerConnection statsForTrack:(RTCMediaStreamTrack *)videoTrack statsOutputLevel:level completionHandler:^(NSArray<RTCLegacyStatsReport *> *stats) {
                        NSLog(@"didGetStats:stats video localStream");
                        [self didGetStats:stats];
                    }];
                }
            }
            
            // Stats for remote streams
    //        for (RTCMediaStream *stream in rtcCall.remoteStreams) {
    //            for (RTCAudioTrack *audioTrack in stream.audioTracks) {
    //                [rtcCall.peerConnection statsForTrack:(RTCMediaStreamTrack *)audioTrack statsOutputLevel:level completionHandler:^(NSArray<RTCLegacyStatsReport *> *stats) {
    //                    NSLog(@"didGetStats:stats audio remoteStream");
    //                    [self didGetStats:stats];
    //                }];
    //            }
    //            for (RTCVideoTrack *videoTrack in stream.videoTracks) {
    //                [rtcCall.peerConnection statsForTrack:(RTCMediaStreamTrack *)videoTrack statsOutputLevel:level completionHandler:^(NSArray<RTCLegacyStatsReport *> *stats) {
    //                    NSLog(@"didGetStats:stats video remoteStream");
    //                    [self didGetStats:stats];
    //                }];
    //            }
    //        }
        }
    }
}

-(void) didGetStats:(NSArray<RTCLegacyStatsReport *> *)stats {
    
    // Convert RTCStuff to a simple dict.
    NSMutableDictionary *out_stats = [NSMutableDictionary new];
    NSMutableDictionary *out_values = [NSMutableDictionary new];
    [out_stats setObject:out_values forKey:@"values"];
    
    for (RTCLegacyStatsReport *report in stats) {
        
        // For the googTrack type, extract on single value :
        // The googTrackId and put it in our stat dict
        if ([report.type isEqualToString:@"googTrack"]) {
            [report.values enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
                if ([key isEqualToString:@"googTrackId"]) {
                    if (value) {
                        [out_stats setObject:value forKey:@"trackId"];
                    }
                    *stop = YES;
                }
            }];
        }
        
        // For the ssrc type, extract anything an put it in our values
        if ([report.type isEqualToString:@"ssrc"]) {
            // timestamp is CFTimeInterval (double)
            [out_stats setObject:@(report.timestamp) forKey:@"timestamp"];
            [out_stats setObject:report.reportId forKey:@"reportId"];
            [report.values enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
                if (key && value) {
                    [out_values setObject:value forKey:key];
                }
            }];
        }
        
        
    }
    OTCLog (@"WebRTC stats : %@",out_stats);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceCallStatsNotification object:out_stats];
    
    /*for (RTCStatsReport *report in stats) {
        // id like googLibjingleSession_6030545827919980834 | googTrack_RTCMSv0 | ssrc_1645852067_send
        NSString *reportId = report.reportId;
        // type like googLibjingleSession | googTrack | ssrc
        NSString *type = report.type;
        // timestamp like 1484746093043.142090
        CFTimeInterval timestamp = report.timestamp;
        // Array of RTCPair*
        NSArray<RTCPair *> *values = report.values;
        
        for (RTCPair *pair in values) {
            // key might be many things.
            // when type = googLibjingleSession -> googInitiator (value = true|false)
            // when type = googTrack -> googTrackId (value = string ex: RTCMSv0)
            
            
            // when type = ssrc -> many many things, depending on audio or video track.
            
            // ----- audio example (my local audio track) :
            // audioInputLevel: 37
            // bytesSent: 53819
            // packetsLost: 0
            // packetsSent: 630
            // ssrc: 531057783
            // transportId: Channel-audio-1
            // googCodecName: opus
            // googEchoCancellationQualityMin: -1
            // googEchoCancellationEchoDelayMedian: -1
            // googEchoCancellationEchoDelayStdDev: -1
            // googEchoCancellationReturnLoss: -100
            // googEchoCancellationReturnLossEnhancement: -100
            // googJitterReceived: 4
            // googRtt: 5
            // googTrackId: RTCMSa0
            // googTypingNoiseState: false
            
            // ----- audio example (chrome web remote audio track) :
            // audioOutputLevel: 2230
            // bytesReceived: 57021
            // packetsLost: 0
            // packetsReceived: 606
            // ssrc: 243631308
            // transportId: Channel-audio-1
            // googAccelerateRate: 0
            // googCaptureStartNtpTimeMs: 3693734880938
            // googCodecName: opus
            // googCurrentDelayMs: 185
            // googDecodingCNG: 0
            // googDecodingCTN: 1269
            // googDecodingCTSG: 0
            // googDecodingNormal: 1182
            // googDecodingPLC: 32
            // googDecodingPLCCNG: 55
            // googExpandRate: 0
            // googJitterBufferMs: 142
            // googJitterReceived: 4
            // googPreemptiveExpandRate: 0
            // googPreferredJitterBufferMs: 180
            // googSecondaryDecodedRate: 0
            // googSpeechExpandRate: 0
            // googTrackId: 322a4e49-ed8f-4e02-8154-636a4b5faf7c
            
            // ----- video example (my local video track) :
            // bytesSent: 2647995
            // codecImplementationName: VideoToolbox
            // packetsLost: 3
            // packetsSent: 2451
            // ssrc: 1645852067
            // transportId: Channel-audio-1
            // googAdaptationChanges: 0
            // googAvgEncodeMs: 0
            // googBandwidthLimitedResolution: false
            // googCodecName: H264
            // googCpuLimitedResolution: false
            // googEncodeUsagePercent: 23
            // googFirsReceived: 0
            // googFrameHeightInput: 640
            // googFrameHeightSent: 640
            // googFrameRateInput: 30
            // googFrameRateSent: 20
            // googFrameWidthInput: 480
            // googFrameWidthSent: 480
            // googNacksReceived: 4
            // googPlisReceived: 0
            // googRtt: 8
            // googTrackId: RTCMSv0
            // googViewLimitedResolution: false
            
            // ----- video example (chrome web remote video track) :
            // bytesReceived: 1772231
            // codecImplementationName: VideoToolbox
            // packetsLost: 1
            // packetsReceived: 1672
            // ssrc: 1032891265
            // transportId: Channel-audio-1
            // googCaptureStartNtpTimeMs: 0
            // googCodecName: H264
            // googCurrentDelayMs: 75
            // googDecodeMs: 8
            // googFirsSent: 0
            // googFrameHeightReceived: 480
            // googFrameRateDecoded: 30
            // googFrameRateOutput: 31
            // googFrameRateReceived: 28
            // googFrameWidthReceived: 640
            // googJitterBufferMs: 43
            // googMaxDecodeMs: 22
            // googMinPlayoutDelayMs: 0
            // googNacksSent: 1
            // googPlisSent: 0
            // googRenderDelayMs: 10
            // googTargetDelayMs: 75
            // googTrackId: 12a75210-0098-40b8-be48-fe31a0f4ccf4
        }
    }*/
}

-(void) getAllStatsWithCompletionHandler:(nullable void (^)(NSMutableArray *stats))completionHandler {
    // Get all stats available
    // It triggers our delegate peerConnection:didGetAllStats:
    
    RTCStatsOutputLevel level = RTCStatsOutputLevelStandard;
    @synchronized(_calls){
        for (RTCCall *rtcCall in _calls) {
            // Stats of the call.
            
            [rtcCall.peerConnection statsForTrack:nil statsOutputLevel:level completionHandler:^(NSArray<RTCLegacyStatsReport *> *stats) {
                NSLog(@"didGetAllStats:stats");
                [self didGetAllStats:stats completionHandler:^(NSMutableArray *statsArray) {
                    completionHandler (statsArray);
                }];
            }];
        }
    
    }
    
}

-(void) didGetAllStats:(NSArray<RTCLegacyStatsReport *> *)stats completionHandler:(nullable void (^)(NSMutableArray *statsArray))completionHandler{
    // Convert RTCStuff to a simple dict.
    NSMutableDictionary *out_stats = [NSMutableDictionary new];
    NSMutableDictionary *out_googCandidatePair = [NSMutableDictionary new];
    NSMutableDictionary *out_localcandidate = [NSMutableDictionary new];
    NSMutableDictionary *out_remotecandidate = [NSMutableDictionary new];
    NSMutableDictionary *out_ssrcAudio = [NSMutableDictionary new];
    NSMutableDictionary *out_ssrcVideo = [NSMutableDictionary new];
    
    NSMutableArray * out_body_stats_array = [NSMutableArray new];
    
    bool isVideoAvailable = NO;
    NSString *localCandidateId = @"";
    NSString *remoteCandidateId = @"";

    
    for (RTCLegacyStatsReport *report in stats) {
        
        // timestamp is CFTimeInterval (double)
        [out_stats setObject:@(report.timestamp) forKey:@"timestamp"];
        
        NSMutableDictionary *out_body_stats = [NSMutableDictionary new];
        [out_body_stats setObject:@(report.timestamp) forKey:@"timestamp"];
        [out_body_stats setObject:report.type forKey:@"type"];
        [out_body_stats setObject:report.reportId forKey:@"reportId"];
       
        
        OTCLog (@"WebRTC stats : %@ : %@", report.type, report.reportId);

        //available statistic type : googLibjingleSession googComponent googCandidatePair localcandidate remotecandidate ssrc googTrack googCertificate
        
        if ([report.type isEqualToString:@"googTrack"]&&[report.reportId.description containsString:@"video"])
            isVideoAvailable = YES;
        
    
        
        // For the googCandidatePair type, extract anything an put it in our values
        if ([report.type isEqualToString:@"googCandidatePair"] && (![out_stats objectForKey:@"googCandidatePair"])) {
            [out_stats setObject:out_googCandidatePair forKey:@"googCandidatePair"];
            [report.values enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
                if (key && value) {
                    [out_googCandidatePair setObject:value forKey:key];
                    [out_body_stats setObject:value forKey:key];
                }
            }];
            localCandidateId = [out_googCandidatePair objectForKey:@"localCandidateId"];
            remoteCandidateId = [out_googCandidatePair objectForKey:@"remoteCandidateId"];
            //OTCLog (@"WebRTC stats out_googCandidatePair : %@",out_googCandidatePair);
        }

        // For the localcandidate type, extract anything an put it in our values
        if ([report.type isEqualToString:@"localcandidate"] && [report.reportId isEqualToString:localCandidateId]) {

            [out_stats setObject:out_localcandidate forKey:@"localcandidate"];
            [report.values enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
                if (key && value) {
                    [out_localcandidate setObject:value forKey:key];
                    [out_body_stats setObject:value forKey:key];
                }
            }];
            //OTCLog (@"WebRTC stats out_localcandidate : %@",out_localcandidate);
        }
        // For the remotecandidate type, extract anything an put it in our values
        if ([report.type isEqualToString:@"remotecandidate"] && [report.reportId isEqualToString:remoteCandidateId]) {
            [out_stats setObject:out_remotecandidate forKey:@"remotecandidate"];
            [report.values enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
                if (key && value) {
                    [out_remotecandidate setObject:value forKey:key];
                    [out_body_stats setObject:value forKey:key];
                }
            }];
            //OTCLog (@"WebRTC stats out_remotecandidate : %@",out_remotecandidate);

        }
        // For the ssrc type, extract anything an put it in our values
        if ([report.type isEqualToString:@"ssrc"]&&[report.reportId.description containsString:@"send"]) {
            if (![out_stats objectForKey:@"ssrcAudio"]) {
                [out_stats setObject:out_ssrcAudio forKey:@"ssrcAudio"];
                [report.values enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
                    if (key && value) {
                        [out_ssrcAudio setObject:value forKey:key];
                        [out_body_stats setObject:value forKey:key];
                    }
                }];
                if (![[out_ssrcAudio objectForKey:@"mediaType"] isEqualToString:@"audio"])  {
                    [out_stats removeObjectForKey:@"ssrcAudio"];
                    
                }
            } else if (isVideoAvailable) {
                [out_stats setObject:out_ssrcVideo forKey:@"ssrcVideo"];
                [report.values enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
                    if (key && value) {
                        [out_ssrcVideo setObject:value forKey:key];
                        [out_body_stats setObject:value forKey:key];
                    }
                }];
                if (![[out_ssrcVideo objectForKey:@"mediaType"] isEqualToString:@"video"])  {
                    [out_stats removeObjectForKey:@"ssrcVideo"];
                   
                }
            }
        }
        
        [out_body_stats_array addObject:out_body_stats];
    }
    
    
    
    OTCLog (@"WebRTC stats : %@",out_stats);
    [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceCallStatsNotification object:out_stats];

    OTCLog (@"WebRTC stats for API: %@",out_body_stats_array);
  
    if (out_body_stats_array != nil) {
        completionHandler (out_body_stats_array);
    }
    
    /*
    2017-07-04 14:17:46.512106+0200 Rainbow[12353:1343671] WebRTC stats : {
        googCandidatePair =     {
            bytesReceived = 14690231;
            bytesSent = 816405;
            consentRequestsSent = 3;
            googActiveConnection = true;
            googChannelId = "Channel-audio-1";
            googLocalAddress = "149.202.202.213:59710";
            googLocalCandidateType = relay;
            googReadable = true;
            googRemoteAddress = "149.202.202.213:62012";
            googRemoteCandidateType = relay;
            googRtt = 103;
            googTransportType = udp;
            googWritable = true;
            localCandidateId = "Cand-1GoWOBAx";
            packetsDiscardedOnSend = 0;
            packetsSent = 6073;
            remoteCandidateId = "Cand-HCT23RJ1";
            requestsReceived = 35;
            requestsSent = 35;
            responsesReceived = 35;
            responsesSent = 35;
        };
        localcandidate =     {
            candidateType = relayed;
            ipAddress = "149.202.202.213";
            networkType = wwan;
            portNumber = 59710;
            priority = 24848383;
            transport = udp;
        };
        remotecandidate =     {
            candidateType = relayed;
            ipAddress = "149.202.202.213";
            portNumber = 62012;
            priority = 8332031;
            transport = udp;
        };
        ssrcAudio =     {
            audioInputLevel = 0;
            bytesSent = 371376;
            googCodecName = opus;
            googEchoCancellationReturnLoss = "-100";
            googEchoCancellationReturnLossEnhancement = "-100";
            googResidualEchoLikelihood = "0.253305";
            googResidualEchoLikelihoodRecentMax = "0.803259";
            googTrackId = "iOS-audio-track-2145979025";
            googTypingNoiseState = false;
            mediaType = audio;
            packetsSent = 3956;
            ssrc = 3067356216;
            transportId = "Channel-audio-1";
        };
        ssrcVideo =     {
            bytesSent = 351975;
            codecImplementationName = unknown;
            framesEncoded = 1579;
            googAdaptationChanges = 0;
            googAvgEncodeMs = 5;
            googBandwidthLimitedResolution = true;
            googCodecName = H264;
            googCpuLimitedResolution = false;
            googEncodeUsagePercent = 19;
            googFirsReceived = 0;
            googFrameHeightInput = 480;
            googFrameHeightSent = 240;
            googFrameRateInput = 31;
            googFrameRateSent = 30;
            googFrameWidthInput = 640;
            googFrameWidthSent = 320;
            googNacksReceived = 0;
            googPlisReceived = 0;
            googRtt = 99;
            googTrackId = "iOS-video-track-3897637948";
            mediaType = video;
            packetsLost = 0;
            packetsSent = 1601;
            qpSum = 77144;
            ssrc = 867103867;
            transportId = "Channel-audio-1";
        };
        timestamp = "1499170666492.94";
    }    */
}

-(void) getLocalAudioVideoStats {
    // Get stats for all the available calls and for each call, for all the available media tracks.

    
    RTCStatsOutputLevel level = RTCStatsOutputLevelStandard;
    @synchronized(_calls) {
        for (RTCCall *rtcCall in _calls) {
            // Stats on my local streams.
            for (RTCMediaStream *stream in rtcCall.peerConnection.localStreams) {
                for (RTCAudioTrack *audioTrack in stream.audioTracks) {
                    [rtcCall.peerConnection statsForTrack:(RTCMediaStreamTrack *)audioTrack statsOutputLevel:level completionHandler:^(NSArray<RTCLegacyStatsReport *> *stats) {
                        NSLog(@"didGetStats:stats audio localStream");
                        [self didGetAudioStats:stats];
                    }];
                }
                for (RTCVideoTrack *videoTrack in stream.videoTracks) {
                    [rtcCall.peerConnection statsForTrack:(RTCMediaStreamTrack *)videoTrack statsOutputLevel:level completionHandler:^(NSArray<RTCLegacyStatsReport *> *stats) {
                        NSLog(@"didGetStats:stats video localStream");
                        [self didGetVideoStats:stats];
                    }];
                }
            }
        }
        
    }
    
}

-(void) didGetAudioStats:(NSArray<RTCLegacyStatsReport *> *)stats {
    
    // Convert RTCStuff to a simple dict.
    NSMutableDictionary *out_stats = [NSMutableDictionary new];
    NSMutableDictionary *out_ssrc = [NSMutableDictionary new];
    
    for (RTCLegacyStatsReport *report in stats) {
        
        // timestamp is CFTimeInterval (double)
        [out_stats setObject:@(report.timestamp) forKey:@"timestamp"];
        
        OTCLog (@"WebRTC stats type : %@ reportId : %@", report.type, report.reportId);
        
        // For the ssrc type, extract anything an put it in our values
        if ([report.type isEqualToString:@"ssrc"]) {
            [out_stats setObject:out_ssrc forKey:@"ssrcAudio"];
            [report.values enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
                if (key && value) {
                    [out_ssrc setObject:value forKey:key];
                }
            }];
        }
    }
    
    OTCLog (@"WebRTC ssrcAudio stats : %@",out_stats);
    [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceCallStatsNotification object:out_stats];
}

-(void) didGetVideoStats:(NSArray<RTCLegacyStatsReport *> *)stats {
    
    // Convert RTCStuff to a simple dict.
    NSMutableDictionary *out_stats = [NSMutableDictionary new];
    NSMutableDictionary *out_ssrc = [NSMutableDictionary new];
    
    for (RTCLegacyStatsReport *report in stats) {
        
        // timestamp is CFTimeInterval (double)
        [out_stats setObject:@(report.timestamp) forKey:@"timestamp"];
        
        OTCLog (@"WebRTC stats type : %@ reportId : %@", report.type, report.reportId);
        
        // For the ssrc type, extract anything an put it in our values
        if ([report.type isEqualToString:@"ssrc"]) {
            [out_stats setObject:out_ssrc forKey:@"ssrcVideo"];
            [report.values enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
                if (key && value) {
                    [out_ssrc setObject:value forKey:key];
                }
            }];
        }
    }
    
    OTCLog (@"WebRTC ssrcVideo stats : %@",out_stats);
    [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceCallStatsNotification object:out_stats];
}


#pragma mark - CallKit CXProviderDelegate

/// Called when the provider has been reset. Delegates must respond to this callback by cleaning up all internal call state (disconnecting communication channels, releasing network resources, etc.). This callback can be treated as a request to end all calls without the need to respond to any actions
- (void)providerDidReset:(CXProvider *)provider {
    OTCLog (@"CXProviderDelegate providerDidReset:");
}

/// Called when the provider has been fully created and is ready to send actions and receive updates
- (void)providerDidBegin:(CXProvider *)provider {
    OTCLog (@"CXProviderDelegate providerDidBegin:");
}

/// Called whenever a new transaction should be executed. Return whether or not the transaction was handled:
///
/// - NO: the transaction was not handled indicating that the perform*CallAction methods should be called sequentially for each action in the transaction
/// - YES: the transaction was handled and the perform*CallAction methods should not be called sequentially
///
/// If the method is not implemented, NO is assumed.
- (BOOL)provider:(CXProvider *)provider executeTransaction:(CXTransaction *)transaction {
    OTCLog (@"CXProviderDelegate executeTransaction:");
    return NO;
}


- (void)provider:(CXProvider *)provider performStartCallAction:(CXStartCallAction *)action {
    // Called when we have an outgoing call
    OTCLog (@"CXProviderDelegate performStartCallAction:");
    
    // Configure the audio session, but do not start call audio here, since it must be done once
    // the audio session has been activated by the system after having its priority elevated.
    // see provider:didActivateAudioSession: delegate
    
    RTCCall *rtcCall = [self getCallByCallId:action.callUUID];
    if (!rtcCall) {
        [action fail];
        return;
    }
    
    CXCallUpdate *callUpdate = [CXCallUpdate new];
    callUpdate.remoteHandle = action.handle;
    callUpdate.localizedCallerName = rtcCall.peer.displayName;
    callUpdate.hasVideo = action.video;
    
    [_provider reportOutgoingCallWithUUID:rtcCall.callID startedConnectingAtDate:[NSDate date]];
    [_provider reportCallWithUUID:rtcCall.callID updated:callUpdate];
    
    [action fulfillWithDateStarted:[NSDate date]];
    
    if ([rtcCall.peer isKindOfClass:[Contact class]] || (rtcCall.isMediaPillarCall)) {
        //Play ringback tone
        [self setupAudioSession];
        [self preparePlayingOutgoingRingTone];
    }

}

- (void)provider:(CXProvider *)provider performAnswerCallAction:(CXAnswerCallAction *)action {
    // Called when the user accept to answer an incoming call through CallKit
    OTCLog (@"CXProviderDelegate performAnswerCallAction:");
    
    // Configure the audio session, but do not start call audio here, since it must be done once
    // the audio session has been activated by the system after having its priority elevated.
    // see provider:didActivateAudioSession: delegate
//    [self setupAudioSession];
    
    RTCCall *rtcCall = [self getCallByCallId:action.callUUID];
    if (!rtcCall) {
        [action fail];
        return;
    }
    
    if (!_xmppService.isXmppConnected) {
        OTCLog(@"Not connected when we want to perform the answer call action - wait the didLogin or didReconnect");
        _rtcCall = rtcCall;
        _answerCallAction = action;
    } else {
        OTCLog(@"Connected so just perform accept call");
        [self performAcceptCall:rtcCall withAction:action];
        _rtcCall = nil;
        _answerCallAction = nil;
    }
    
    [_servicesManager checkTokenExpirationAndRenewIt];

}

- (void)performAcceptCall:(RTCCall *) rtcCall withAction:(CXAnswerCallAction *)action {
    // We need to reconnect (if phone is locked and we stay in CallKit UI)
    [_servicesManager setActionToPerformHandler:^{
        if (rtcCall.isVideoEnabled)
            rtcCall.features |= RTCCallFeatureLocalVideo;
        [self acceptIncomingCall:rtcCall withFeatures:rtcCall.features];
        rtcCall.actionToFullFill = action;
    }];
}


- (void) provider:(CXProvider *)provider performEndCallAction:(CXEndCallAction *)action {
    // Called in multiple cases :
    // - incoming call declined
    // - call hangup
    OTCLog (@"CXProviderDelegate performEndCallAction:");
    _callOnHold = nil;
    
    RTCCall *rtcCall = [self getCallByCallId:action.callUUID];
    if (!rtcCall) {
        [action fail];
        return;
    }
    
    if (rtcCall.isIncoming && rtcCall.status == CallStatusRinging) {
        
        // We need to reconnect (if phone is locked and we stay in CallKit UI)
        [_servicesManager checkTokenExpirationAndRenewIt];
        [self declineIncomingCall:rtcCall];
    }
    
    if (!rtcCall.isIncoming && rtcCall.status == CallStatusRinging) {
        
        // We need to reconnect (if phone is locked and we stay in CallKit UI)
        [_servicesManager checkTokenExpirationAndRenewIt];
        [self cancelOutgoingCall:rtcCall];
    }
    
    if (rtcCall.status == CallStatusEstablished) {
        
        // We need to reconnect (if phone is locked and we stay in CallKit UI)
        [_servicesManager checkTokenExpirationAndRenewIt];
        [self hangupCall:rtcCall];
    }
    
    rtcCall.actionToFullFill = nil;
    [_calls removeObject:rtcCall];
    if (!rtcCall.fakeCall)
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:rtcCall];
    
    [action fulfillWithDateEnded:[NSDate date]];
}


- (void) provider:(CXProvider *)provider performSetHeldCallAction:(CXSetHeldCallAction *)action {
    if (action.isOnHold) {
        OTCLog (@"CXProviderDelegate performSetHeldCallAction: holdCall");

        RTCCall *rtcCall = [self getCallByCallId:action.callUUID];
        if (!rtcCall) {
            [action fail];
            return;
        } else {
            [_xmppService sendJingleHoldFromJid:_myUser.contact.jid toTarget:[NSString stringWithFormat:@"%@/%@", rtcCall.peer.jid, rtcCall.resource] sessionID:rtcCall.jingleSessionID];
        }
        _callOnHold = rtcCall;
    }
    else {
        OTCLog (@"CXProviderDelegate performSetHeldCallAction: retrieveCall");
        
        RTCCall *rtcCall = [self getCallByCallId:action.callUUID];
        if(rtcCall) {
            [_xmppService sendJingleUnholdFromJid:_myUser.contact.jid toTarget:[NSString stringWithFormat:@"%@/%@", rtcCall.peer.jid, rtcCall.resource] sessionID:rtcCall.jingleSessionID];
        }
        _callOnHold = nil;
    }
    [action fulfill];
}


- (void) provider:(CXProvider *)provider performSetMutedCallAction:(CXSetMutedCallAction *)action {
    
    RTCCall *rtcCall = [self getCallByCallId:action.callUUID];
    if (!rtcCall) {
        [action fail];
        return;
    }
    
    if (action.isMuted) {
        [self muteLocalAudioForCall:rtcCall];
            OTCLog (@"CXProviderDelegate performSetMutedCallAction: muted");
        
    } else {
        [self unMuteLocalAudioForCall:rtcCall];
            OTCLog (@"CXProviderDelegate performSetMutedCallAction: unmuted");
    }
    [action fulfill];
}

- (void)provider:(CXProvider *)provider timedOutPerformingAction:(CXAction *)action {
    // Called when an action was not performed in time and has been inherently failed.
    // Depending on the action, this timeout may also force the call to end.
    // An action that has already timed out should not be fulfilled or failed by the provider delegate
    
    // Note : This is NOT called for example when we have an incoming call
    // and we don't answer in a given time.
    OTCLog (@"CXProviderDelegate timedOutPerformingAction:");
}

- (void) provider:(CXProvider *)provider didActivateAudioSession:(AVAudioSession *)audioSession {
    // Called when the provider's audio session activation state changes.
    if (_callOnHold)
        OTCLog (@"CXProviderDelegate didActivateAudioSession: call is on hold");
    else
        OTCLog (@"CXProviderDelegate didActivateAudioSession:");
    
    [self activateAudioSession:audioSession];
}


- (void) provider:(CXProvider *)provider didDeactivateAudioSession:(AVAudioSession *)audioSession {
#if DEBUG
    OTCLog (@"CXProviderDelegate didDeactivateAudioSession '%p' : _callOnHold=%ld, audio=%ld, appState=%ld", audioSession, _callOnHold, [RTCAudioSession sharedInstance].isActive, [UIApplication sharedApplication].applicationState);
#endif
    [self deactivateAudioSession:audioSession];
}


-(void) applicationDidBecomeActive:(NSNotification *) notification
{
#if DEBUG
    OTCLog (@"applicationDidBecomeActive: _callOnHold=%ld, audio=%ld, appState=%ld", _callOnHold, [RTCAudioSession sharedInstance].isActive, [UIApplication sharedApplication].applicationState);
#endif
    if (_callOnHold && ![RTCAudioSession sharedInstance].isActive)
    {
        // https://github.com/twilio/voice-quickstart-objc#notifications--callbacks-during-interruption
        // Resume call audio programmatically after interruption
        CXSetHeldCallAction *setHeldCallAction = [[CXSetHeldCallAction alloc] initWithCallUUID:_callOnHold.callID onHold:NO];
        CXTransaction* retrieveTransaction = [[CXTransaction alloc] initWithAction:setHeldCallAction];
        [_callController requestTransaction:retrieveTransaction completion:^(NSError *error) {
            if (error)
            {
                OTCLog (@"applicationDidBecomeActive: failed to submit setHeldCallAction transaction request");
            }
            else
            {
                OTCLog (@"applicationDidBecomeActive: setHeldCallAction transaction successfully done");
                _callOnHold = nil;
            }
        }];
    }
}


#pragma mark - from SpeakerBox

- (void) setupAudioSession {
    RTCAudioSessionConfiguration *configuration = [[RTCAudioSessionConfiguration alloc] init];
    configuration.category = AVAudioSessionCategoryPlayAndRecord;
    configuration.categoryOptions = AVAudioSessionCategoryOptionAllowBluetooth;
    configuration.mode = AVAudioSessionModeVoiceChat;
    configuration.ioBufferDuration = .005;
    configuration.sampleRate = 44100;
    
    RTCAudioSession *session = [RTCAudioSession sharedInstance];
    [session lockForConfiguration];
    session.useManualAudio = YES;
    BOOL hasSucceeded = NO;
    NSError *error = nil;
    hasSucceeded = [session setConfiguration:configuration error:&error];
    if (!hasSucceeded) {
        OTCLog (@"Error setting configuration: %@", error.localizedDescription);
    }
    [session unlockForConfiguration];
}


-(void) activateAudioSession:(AVAudioSession *)audioSession {
    if (_calls.count>0) {
        [[RTCAudioSession sharedInstance] lockForConfiguration];
        [RTCAudioSession sharedInstance].isAudioEnabled = YES;
        NSError *error = nil;
        [[RTCAudioSession sharedInstance] setActive:YES error:&error];
        OTCLog (@"activateAudioSession SetActive YES %@", error ? error : @"OK");
        [[RTCAudioSession sharedInstance] audioSessionDidActivate:audioSession];
        [[RTCAudioSession sharedInstance] unlockForConfiguration];
        
        if (_outgoingToneAudioPlayer)
            [_outgoingToneAudioPlayer play];
        
        RTCCall *rtcCall = [_calls objectAtIndex:0];
        if (rtcCall.isVideoEnabled)
            [self forceAudioOnSpeaker];
        
        // Not really needed but will force the UI to be refresh after forcing the speaker
        if (!rtcCall.fakeCall && !rtcCall.isMediaPillarCall)
            [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:rtcCall];
    }
}


-(void) deactivateAudioSession:(AVAudioSession *)audioSession {
    [[RTCAudioSession sharedInstance] lockForConfiguration];
    [RTCAudioSession sharedInstance].isAudioEnabled = NO;
    NSError *error = nil;
    [[RTCAudioSession sharedInstance] setActive:NO error:&error];
    OTCLog (@"deactivateAudioSession SetActive NO %@", error ? error : @"OK");
    [[RTCAudioSession sharedInstance] audioSessionDidDeactivate:audioSession];
    [[RTCAudioSession sharedInstance] unlockForConfiguration];
    _isSpeakerEnabled = NO;
}


-(void) requestMicrophoneAccess {
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            // Microphone enabled code
            [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidAllowMicrophoneNotification object:nil];
        }
        else {
            // Microphone disabled code
            [[NSNotificationCenter defaultCenter] postNotificationName:kRTCServiceDidRefuseMicrophoneNotification object:nil];
        }
    }];
}

-(BOOL) microphoneAccessAlreadyDetermined {
    AVAudioSessionRecordPermission sessionRecordPermission = [[AVAudioSession sharedInstance] recordPermission];
    return sessionRecordPermission != AVAudioSessionRecordPermissionUndetermined;
}

-(BOOL) microphoneAccessGranted {
    AVAudioSessionRecordPermission sessionRecordPermission = [[AVAudioSession sharedInstance] recordPermission];
    return sessionRecordPermission == AVAudioSessionRecordPermissionGranted;
}

-(void) handleContinueUserActivity:(NSUserActivity *)userActivity error:(NSError **)errorPointer waitingForResponse:(void (^_Nullable)(BOOL receivedResponse))waitingForResponseBlock restorationHandler:(void (^_Nullable)(NSArray * _Nullable))restorationHandler {
    if ([userActivity.interaction.intent isKindOfClass:[INStartAudioCallIntent class]] ||[userActivity.interaction.intent isKindOfClass:[INStartVideoCallIntent class]]) {
        
        INInteraction *interaction = userActivity.interaction;
        INPerson *contact;
        BOOL isVideo = [userActivity.interaction.intent isKindOfClass:[INStartVideoCallIntent class]];
        
        if (isVideo) {
            INStartVideoCallIntent *startCallIntent = (INStartVideoCallIntent *)interaction.intent;
            contact = startCallIntent.contacts[0];
        } else {
            INStartAudioCallIntent *startCallIntent = (INStartAudioCallIntent *)interaction.intent;
            contact = startCallIntent.contacts[0];
        }
        
        INPersonHandle *personHandle = contact.personHandle;
        NSString *emailString = personHandle.value;
        
        // Try to get the contact by jid
        Contact *rainbowContact = [_contactsManager getContactWithJid: emailString];
        
        // If no rainbow contact match, try a search by email
        if (!rainbowContact)
            rainbowContact = [_contactsManager searchRainbowContactWithEmailString: emailString];

        // No local contact found. Do a search on server
        if (!rainbowContact) {
            if (waitingForResponseBlock)
                waitingForResponseBlock(YES);
            
            NSArray *users = [_contactsManager.directoryService searchRainbowContactsWithLoginEmails:@[emailString] fromOffset:0 withLimit:10];
            if (users != nil && users.count > 0)
                rainbowContact = [_contactsManager createOrUpdateRainbowContactFromJSON: users[0]];
            
            if (waitingForResponseBlock)
                waitingForResponseBlock(NO);
        }
        
        // No rainbow contact found
        if (!rainbowContact) {
            if (errorPointer)
                *errorPointer = [NSError errorWithDomain:@"UserActivity" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"User not found"}];
        }
        
        if (rainbowContact && rainbowContact.canChatWith) {
            if (isVideo)
                [self beginNewOutgoingCallWithPeer:rainbowContact withFeatures:(RTCCallFeatureAudio|RTCCallFeatureLocalVideo)];
            else
                [self beginNewOutgoingCallWithPeer:rainbowContact withFeatures:RTCCallFeatureAudio];
            
            [_servicesManager checkTokenExpirationAndRenewIt];
        } else {
            if (errorPointer) {
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{NSLocalizedDescriptionKey:@"User not found"}];
                if (rainbowContact)
                    [dic setObject:rainbowContact forKey:@"object"];
                *errorPointer = [NSError errorWithDomain:@"UserActivity" code:-1 userInfo:dic];
            }
        }
    }
}

#pragma mark - Network reachability
-(void)reachabilityChanged:(NSNotification *) notification {
    Reachability* curReach = [notification object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    if (_calls.count > 0) {
        OTCLog (@"reachability changed");
        if ([curReach currentReachabilityStatus] != NotReachable && ![curReach connectionRequired]) {
            RTCCall *call = [_calls firstObject];
            if (call.status == CallStatusConnecting || call.status == CallStatusEstablished) {
                __weak RTCCall *weakRTCCall = call;
                OTCLog (@"network changed, don't wait for IceFailed, we resend our offer");
                [self prepareRTCOfferForCall:call iceRestart:YES completionHandler:^(RTCSessionDescription *offer) {
                    OTCLog (@"Sending SDP Offer : %@", offer);
                    [_xmppService sendJingleSDP:offer.sdp type:XMPPJingleMessageTypeTransportReplace toTarget:weakRTCCall.fullJID sessionID:weakRTCCall.jingleSessionID peer:call.peer];
                }];
            }
        }
    }
}

#pragma mark - Facilitors
+(NSString *) stringForRTCIceConnectionState:(RTCIceConnectionState) iceConnectionState {
    switch (iceConnectionState) {
        case RTCIceConnectionStateNew:
            return @"New";
            break;
        case RTCIceConnectionStateChecking:
            return @"Checking";
            break;
        case RTCIceConnectionStateConnected:
            return @"Connected";
            break;
        case RTCIceConnectionStateCompleted:
            return @"Completed";
            break;
        case RTCIceConnectionStateFailed:
            return @"Failed";
            break;
        case RTCIceConnectionStateDisconnected:
            return @"Disconnected";
            break;
        case RTCIceConnectionStateClosed:
            return @"Closed";
            break;
        case RTCIceConnectionStateCount:
            return @"Count";
            break;
    }
}

+(NSString *) stringForRTCIceGatheringState:(RTCIceGatheringState) iceGatheringState {
    switch (iceGatheringState) {
        case RTCIceGatheringStateNew:
            return @"New";
            break;
        case RTCIceGatheringStateGathering:
            return @"Gathering";
            break;
        case RTCIceGatheringStateComplete:
            return @"Complete";
            break;
    }
}
+(NSString *) stringForRTCSignalingState:(RTCSignalingState) signalingState {
    switch (signalingState) {
        case RTCSignalingStateStable:
            return @"Stable";
            break;
        case RTCSignalingStateHaveLocalOffer:
            return @"Have local offer";
            break;
        case RTCSignalingStateHaveLocalPrAnswer:
            return @"Have local PR Answer";
            break;
        case RTCSignalingStateHaveRemoteOffer:
            return @"Have remote offer";
            break;
        case RTCSignalingStateHaveRemotePrAnswer:
            return @"Have remote PR answer";
            break;
        case RTCSignalingStateClosed:
            return @"Closed";
            break;
    }
}

#pragma mark RTAudioSession delegates
- (void)audioSessionDidBeginInterruption:(RTCAudioSession *)session {
    OTCLog (@"RTCAudioSession : audioSessionDidBeginInterruption");
}

- (void)audioSessionDidEndInterruption:(RTCAudioSession *)session
                   shouldResumeSession:(BOOL)shouldResumeSession {
    OTCLog (@"RTCAudioSession : audioSessionDidEndInterruption");
}

- (void)audioSessionDidChangeRoute:(RTCAudioSession *)session
                            reason:(AVAudioSessionRouteChangeReason)reason
                     previousRoute:(AVAudioSessionRouteDescription *)previousRoute {
    OTCLog (@"RTCAudioSession : audioSessionDidChangeRoute");
}

- (void)audioSessionMediaServerTerminated:(RTCAudioSession *)session {
    OTCLog (@"RTCAudioSession : audioSessionMediaServerTerminated");
}

- (void)audioSessionMediaServerReset:(RTCAudioSession *)session {
    OTCLog (@"RTCAudioSession : audioSessionMediaServerReset");
}

- (void)audioSessionShouldConfigure:(RTCAudioSession *)session {
    OTCLog (@"RTCAudioSession : audioSessionShouldConfigure");
}

- (void)audioSessionShouldUnconfigure:(RTCAudioSession *)session {
    OTCLog (@"RTCAudioSession : audioSessionShouldUnconfigure");
}

- (void)audioSession:(RTCAudioSession *)audioSession
didChangeOutputVolume:(float)outputVolume {
    OTCLog (@"RTCAudioSession : didChangeOutputVolume");
}

-(NSURL *) getNSURLOfAudioFile: (NSString *) fileName {
    NSURL* musicFile = nil;
    
    if ( fileName ) {
        NSString *theFileName = [[fileName lastPathComponent] stringByDeletingPathExtension];
        NSString *theFileExtension = [fileName pathExtension];
        OTCLog ( @"getNSURLOfAudioFile: file: %@, ext: %@", theFileName, theFileExtension);
        
        musicFile = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource: theFileName ofType: theFileExtension]];
    }
    
    return musicFile;
}

-(void) didChangeAdvancedSettings:(NSNotification *) notification {
    NSNumber* object = [[NSUserDefaults standardUserDefaults] objectForKey:@"advancedSettingsDisableTurns"];
    if (object) {
        _useTurnsServers = [object boolValue];
    }

}

#pragma mark - webRTC Stats
-(void) createWebrtcStatsforCall:(RTCCall *)rtcCall withCompletionHandler:(WebrtcMetricsCreateComplionHandler) completionHandler {
    NSString *peerId = rtcCall.peer.rainbowID;
    if ([rtcCall.peer isKindOfClass:[Room class]]) {
        Room * myRoom = (Room *)rtcCall.peer;
        if (myRoom.conference != nil) {
             peerId = myRoom.conference.confId;
        }
    }
    NSURL *url = [_apiURLManager getURLForService:ApiServicesWebrtcmetrics];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesWebrtcmetrics];
    NSDictionary *body = @{@"callId":[NSString stringWithFormat:@"%@",rtcCall.callID], @"peerId":(peerId != nil)?peerId:@""};
    [_downloadManager postRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (!error) {
            NSDictionary *response = [receivedData objectFromJSONData];
            if (response) {
                if ([response objectForKey:@"data"]) {
                    NSString *metricsId = [[response objectForKey:@"data"] objectForKey:@"id"];
                    if (completionHandler) {
                        completionHandler(metricsId, nil);
                    }
                }
            }
        }
        else{
            OTCLog (@"Error when create WebRTC metrics: %@",error);
            
            if (completionHandler) {
                completionHandler(nil, error);
            }
        }

    }];
}

-(void) addWebrtcMetricsforCall:(RTCCall *)rtcCall andStatsArray:(NSMutableArray *)statsArray withCompletionHandler:(WebrtcMetricsComplionHandler) completionHandler {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/metrics",[_apiURLManager getURLForService:ApiServicesWebrtcmetrics],rtcCall.webRTCMetricsID]];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesWebrtcmetrics];
    
    // TODO: connection ID must be the peerConnectionID, but we don't have access to it for now
    // I use _myUser.contact.rainbowID to make this unique in conference call.
    NSString *peerId = _myUser.contact.rainbowID;
    NSDictionary *body = @{@"metrics":@[@{@"connectionId":(peerId != nil)?peerId:@"",@"stats":statsArray}]};
    [_downloadManager postRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
       
        if (error) {
            OTCLog (@"Error when add WebRTC metrics: %@",error);
        }

        if (completionHandler) {
            completionHandler(error);
        }
    }];

}

-(void) updateWebrtcMetricsForCall:(RTCCall *)rtcCall withCompletionHandler:(WebrtcMetricsComplionHandler) completionHandler {
    NSString *peerId = rtcCall.peer.rainbowID;
    if ([rtcCall.peer isKindOfClass:[Room class]]) {
        Room * myRoom = (Room *)rtcCall.peer;
        if (myRoom.conference != nil) {
            peerId = myRoom.conference.confId;
        }
        
    }
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",[_apiURLManager getURLForService:ApiServicesWebrtcmetrics],rtcCall.webRTCMetricsID]];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesWebrtcmetrics];
    NSDictionary *body = @{@"callState":@"ended",@"peerId":(peerId != nil)?peerId:@""};
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        
        if (error) {
            OTCLog (@"Error when update WebRTC metrics: %@",error);
        }
        
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

#pragma mark - MediaPillar

-(void) fetchMediaPillarDataWithCompletionHandler:(MediaPillarDataCompletionHandler) completionHandler {
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesMediaPillar];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesMediaPillar];
    OTCLog (@"Get MediaPillar data");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot get Mediapillar data in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if (completionHandler) {
                completionHandler(nil, nil, nil, error);
            }
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot get Mediapillar jid in REST due to JSON parsing failure or the non-presence of a Mediapillar");
            NSError *error = [NSError errorWithDomain:@"RTCService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"WebRTC Gateway not found"}];
            completionHandler(nil, nil, nil, error);
            return;
        }
        
        NSDictionary *data = [jsonResponse objectForKey:@"data"];
        if (data) {
            NSString *jid_im = [data objectForKey:@"jid_im"];
            NSString *rainbowPhoneNumber = [data objectForKey:@"rainbowPhoneNumber"];
            NSString *prefix = [data objectForKey:@"prefix"];
            
            if (completionHandler) {
                completionHandler(jid_im, rainbowPhoneNumber, prefix, nil);
            }
        } else {
            if (completionHandler) {
                NSError *error = [NSError errorWithDomain:@"RTCService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"No WebRTC Gateway information (empty data response)"}];
                completionHandler(nil, nil, nil, error);
            }
        }
    }];
    
}

-(void) webRTCGatewayRegisterRequest:(NSString *)gatewayJid number:(NSString *)number displayName:(NSString *)name secret:(NSString *)secret {
    OTCLog (@"[MEDIAPILLAR] mediaPillarRegisterRequest");
    if (gatewayJid) {
        _mediaPillarAddress = [gatewayJid stringByAppendingString:@"/mediapillar"];
        [_xmppService registerToMediaPillar:number displayName:name secret:secret address:_mediaPillarAddress];
    }
}

-(void) webRTCGatewayUnRegisterRequest:(NSString *)number {
    OTCLog (@"[MEDIAPILLAR] mediaPillarUnRegisterRequest");
    [_xmppService unregisterToMediaPillar:number address:_mediaPillarAddress];
}

-(RTCCall *) beginNewOutgoingCallWithWebRTCGatewayToNumber:(NSString *) number {
    if(_mediaPillarAddress){
        return [self beginNewOutgoingCallWithPeer:[Peer new] phoneNumber:number withFeatures:RTCCallFeatureAudio|RTCCallFeatureMediaPillar];
    }else {
        [self fetchMediaPillarDataWithCompletionHandler:^(NSString *mediaPillarJid, NSString *rainbowPhoneNumber, NSString *prefix, NSError *error) {
            _mediaPillarAddress = mediaPillarJid;
            [self webRTCGatewayRegisterRequest:mediaPillarJid number:number displayName:_contactsManager.myUser.contact.fullName secret:nil];
        }];
        return [self beginNewOutgoingCallWithPeer:[Peer new] phoneNumber:number withFeatures:RTCCallFeatureAudio|RTCCallFeatureMediaPillar];
    }
}

@end
