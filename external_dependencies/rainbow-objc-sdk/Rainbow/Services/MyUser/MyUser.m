/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "MyUser.h"
#import "MyUser+Internal.h"
#import "Source+Internal.h"
#import "ServicesManager.h"
#import "ContactsManagerService+Internal.h"
#import "NSData+JSON.h"
#import "LoginManager.h"
#import "NSDictionary+JSONString.h"
#import "PresenceInternal.h"
#import "RainbowUserDefaults.h"
#import "NSDate+Utilities.h"
#import "NSDate+Equallity.h"
#import "NSDate+Distance.h"
#import "LoginManager+Internal.h"
#import "NomadicStatus+Internal.h"
#import "PhoneNumberInternal.h"

#define kMyUserFeatureWebRTCMobileKey @"webRtcMobileFeature"
#define kMyUserFeatureWebRTCMobileVideoKey @"webRtcMobileVideoFeature"
#define kMyUserFeatureWebRTCParticipantAllowedKey @"webRtcParticipantAllowedFeature"
#define kMyUserFeatureMaxParticipantPerBulleKey @"maxParticipantPerBulleFeature"
#define kMyUserFeatureConferenceAllowedKey @"conferenceAllowedFeature"
#define kMyUserFeatureConferenceParticipantAllowedKey @"conferenceParticipantAllowedFeature"
#define kMyUserFeatureSearchActiveDirectoryAllowedKey @"searchActiveDirectoryAllowedFeature"
#define kMyUserFeatureMaxWebRTCParticipantsPerBulleKey @"maxWebRTCParticipantsPerBulleFeature"
#define kMyUserFeatureWebRTCConferenceAllowedKey @"webRTCConferenceAllowedFeature"
#define kMyUserFeatureTelephonyNomadicAllowedKey @"telephonyNomadicAllowedFeature"
#define kMyUserFeatureTelephonyWebRTCtoPSTNAllowedKey @"telephonyWebRTCtoPSTNAllowedFeature"
#define kMyUserFeatureTelephonyWebRTCGatewayAllowedKey @"telephonyWebRTCGatewayAllowedFeature"
#define kMyUserFeatureTelephonySecondCallAllowedKey @"telephonySecondCallAllowedFeature"
#define kMyUserFeatureTelephonyTransferCallAllowedKey @"telephonyTransferCallAllowedFeature"
#define kMyUserFeatureTelephonyConferenceCallAllowedKey @"telephonyConferenceCallAllowedFeature"
#define kMyUserFeatureTelephonyVoiceMailAllowedKey @"telephonyVoiceMailAllowedFeature"
#define kMyUserFeatureTelephonyCallForwardAllowedKey @"telephonyCallForwardAllowedFeature"
#define kMyUserIsADSearchAvailableKey @"isADSearchAvailable"
#define kMyUserFeatureSearchPBXPhonebookAllowedKey @"searchPBXPhonebookAllowedFeature"

NSString *const kMyUserFeatureDidUpdate = @"userFeatureDidUpdate";
NSString *const kMyUserProfilesDidUpdate = @"MyUserProfilesDidUpdate";
NSString *const kMyUserNomadicStatusDidUpdate = @"userNomadicStatusDidUpdate";
NSString *const kMyUserCallForwardDidUpdate = @"userCallForwardDidUpdate";
NSString *const kMyUserVoicemailCountDidUpdate = @"userVoicemailCountDidUpdate";


@implementation MyUser

@synthesize profilesName = _profilesName;

-(instancetype) init {
    self = [super init];
    if(self){
        _settings = [UserSettings sharedInstance];
        _appToken = nil;
        _appID = nil;
        _secretKey = nil;
        /*
        NSUserDefaults *defaults = [RainbowUserDefaults sharedInstance];
        NSData *myEncodedObject = [defaults objectForKey:@"source"];
        Source *savedSource = nil;
        if(myEncodedObject)
            savedSource = (Source*)[NSKeyedUnarchiver unarchiveObjectWithData:myEncodedObject];
        
        if(savedSource && [savedSource.sourceVersion isEqualToString:kSourceCurrentVersion]){
            _source = savedSource;
        } else
            _source = [Source new];
         
        
        _contactSearchMatchingMutex = [NSObject new];
    
        NSData *contactSearchMatchingEncoded = [defaults objectForKey:@"contactSearchMatching"];
        if(contactSearchMatchingEncoded)
            _contactSearchMatching = [NSMutableDictionary dictionaryWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithData:contactSearchMatchingEncoded]];
        else
            _contactSearchMatching = [NSMutableDictionary dictionary];
        */
        _roles = [NSMutableArray array];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailedToAuthenticate:) name:kLoginManagerDidFailedToAuthenticate object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddCall:) name:kTelephonyServiceDidAddCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateCall:) name:kTelephonyServiceDidUpdateCallNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        
        _profilesName = [NSMutableArray array];
        
        _nomadicStatus = [NomadicStatus new];
        _mediaPillarStatus = [MediaPillarStatus new];
    }
    return self;
}

-(void) dealloc {
    _settings = nil;
    _source = nil;
    _contactSearchMatchingMutex = nil;
    _contactSearchMatching = nil;
    _presenceSettings = PresenceSettingsUnknown;

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidAddCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidUpdateCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidFailedToAuthenticate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    
    [_profilesName removeAllObjects];
    _profilesName = nil;
    _nomadicStatus = nil;
    _mediaPillarStatus = nil;
    _nomadicStatus = nil;
}

-(NSString *) username {
    return _settings.username;
}

-(NSString *) password {
    return _settings.password;
}

-(NSString *) token {
    return _settings.token;
}

-(void) setUsername:(NSString *)username {
    _settings.username = username;
}

-(void) setPassword:(NSString *)password {
    _settings.password = password;
}

-(void) setServerSourceID:(NSString *) serverSourceID {
    _source.serverSourceId = serverSourceID;
}

-(void) setToken:(NSString *) newToken {
    _settings.token = newToken;
}

-(NSDate *) tokenExpireDate {
    NSArray *splitedToken = [_settings.token componentsSeparatedByString:@"."];
    if(splitedToken.count == 3){
        NSString *base64String = [splitedToken objectAtIndex: 1];
        NSInteger requiredLength = (int)(4 * ceil((float)[base64String length] / 4.0));
        NSInteger nbrPaddings = requiredLength - [base64String length];
        
        if (nbrPaddings > 0) {
            NSString *padding = [[NSString string] stringByPaddingToLength:nbrPaddings withString:@"=" startingAtIndex:0];
            base64String = [base64String stringByAppendingString:padding];
        }
        
        base64String = [base64String stringByReplacingOccurrencesOfString:@"-" withString:@"+"];
        base64String = [base64String stringByReplacingOccurrencesOfString:@"_" withString:@"/"];
        
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
        
        NSDictionary *jsonDictionary = [[decodedString dataUsingEncoding:NSUTF8StringEncoding] objectFromJSONData];
        if(jsonDictionary){
            NSString *expireDate = jsonDictionary[@"exp"];
            _tokenExpireDate = [NSDate dateWithTimeIntervalSince1970:[expireDate doubleValue]];
        }
    }
    return _tokenExpireDate;
}

-(BOOL) isTokenValid {
    if(!self.token || self.token.length == 0){
        NSLog(@"No token renew it");
        return YES;
    }
    BOOL needRenew = NO;
    // We have to check the token expiration date
    NSDate *currentDate = [NSDate date];
    if([currentDate isLaterThanDate:self.tokenExpireDate]){
        NSLog(@"currentDate is later than token date, we need to renew the token");
        needRenew = YES;
    } else if ([currentDate isEarlierThanDate:_tokenExpireDate]) {
        NSLog(@"currentDate is earlier than token date, no need to renew token");
        NSTimeInterval secs = [self.tokenExpireDate distanceInSecondsToDate:currentDate];
        if(secs < 600){
            NSLog(@"Delta between the dates is less than 10 min, so renew the token");
            needRenew = YES;
        } else
            needRenew = NO;
    } else {
        NSLog(@"dates are the same, we need to renew the token");
        needRenew = YES;
    }
    return needRenew;
}

-(NSString *) jid_im {
    return _settings.jidIm;
}

-(void) setJid_im:(NSString *)jid_im {
    _settings.jidIm = jid_im;
}

-(NSString *) jid_password {
    return _settings.jidPwd;
}

-(void) setJid_password:(NSString *)jid_password {
    _settings.jidPwd = jid_password;
}

-(NSString *) userID {
    return _settings.userID;
}

-(void) setUserID:(NSString *)userID {
    _settings.userID = userID;
}

-(NSString *) jid_tel {
    return _settings.jidTel;
}

-(void) setJid_tel:(NSString *)jid_tel {
    _settings.jidTel = jid_tel;
}

-(NSString *) companyID {
    return _settings.companyID;
}

-(void) setCompanyID:(NSString *)companyID {
    _settings.companyID = companyID;
}

-(NSString *) companyName {
    return _settings.companyName;
}

-(void) setCompanyName:(NSString *)companyName {
    _settings.companyName = companyName;
}

-(BOOL) isInitialized {
    return _settings.isInitialized;
}

-(void) setIsInitialized:(BOOL)isInitialized {
    _settings.isInitialized = isInitialized;
}

-(BOOL) isGuest {
    return _settings.isGuest;
}

-(void) setIsGuest:(BOOL)guestMode{
    _settings.isGuest = guestMode;
}


-(void) setIsADSearchAvailable:(BOOL)isADSearchAvailable {
    [[RainbowUserDefaults sharedInstance] setBool:isADSearchAvailable forKey:kMyUserIsADSearchAvailableKey];
}

-(BOOL) isADSearchAvailable {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserIsADSearchAvailableKey];
}

-(NSArray *)profilesName {
    if(_profilesName.count == 0){
        NSArray *savedArray = [[RainbowUserDefaults sharedInstance] arrayForKey:@"profiles"];
        [_profilesName addObjectsFromArray:savedArray];
    }
    
    return _profilesName;
}

-(void) setProfilesName:(NSMutableArray *)profilesName {
    [_profilesName removeAllObjects];
    [_profilesName addObjectsFromArray:profilesName];
    [[RainbowUserDefaults sharedInstance] setObject:_profilesName forKey:@"profiles"];
    [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserProfilesDidUpdate object:nil userInfo:nil];
}

-(BOOL) isReadyToCreateConference {
    return _settings.isReadyToCreateConference;
}

-(void) setIsReadyToCreateConference:(BOOL)isReadyToCreateConference {
    _settings.isReadyToCreateConference = isReadyToCreateConference;
    [self fetchProfilesFeatures];
}

-(BOOL) haveMinimalInformationsToStart {
    BOOL minimalInfo = [self haveMinimalInformationsToStartWithOutToken];
    if(!self.isTokenValid && minimalInfo){
        NSLog(@"[%@] Token is valid and we have the necessary informations", NSStringFromSelector(_cmd));
        minimalInfo = YES;
    } else {
        NSLog(@"[%@] Token is no longer valid or we don't have the necessary informations", NSStringFromSelector(_cmd));
        minimalInfo = NO;
    }
    return minimalInfo;
}

-(BOOL) haveMinimalInformationsToStartWithOutToken {
    if(self.jid_im){
        if(self.jid_password){
            if(self.userID){
                if([_settings.userSettingsVersion isEqualToString:kUserSettingsCurrentVersion]){
                    NSLog(@"[%@] Everythings is OK, use the saved infos", NSStringFromSelector(_cmd));
                    return YES;
                } else
                    NSLog(@"[%@] Don't have the good version", NSStringFromSelector(_cmd));
            } else
                NSLog(@"[%@] Don't have the userID", NSStringFromSelector(_cmd));
        } else
            NSLog(@"[%@] No jid_pwd", NSStringFromSelector(_cmd));
    } else
        NSLog(@"[%@] No jid_im", NSStringFromSelector(_cmd));
    return NO;
}

#pragma mark - sources

-(void) writeSourceOnDisk {
    [[RainbowUserDefaults sharedInstance] setObject:[NSKeyedArchiver archivedDataWithRootObject:_source] forKey:@"source"];
}

// ContactServerInfo will contain two key contactServerId, and contactJid
-(void) insertContactServerInfo:(NSDictionary <NSString *, NSString *> *) contactServerInfo andAddressBookRecordId:(NSString *) addressBookRecordId {
    @synchronized(_contactSearchMatchingMutex) {
        if(![_contactSearchMatching valueForKey:addressBookRecordId])
            [_contactSearchMatching setObject:contactServerInfo forKey:addressBookRecordId];
        else
            NSLog(@"Contact server ID %@ already exist", addressBookRecordId);
    }
}

-(void) removeContactWithAddressBookRecordId:(NSString*) addressBookRecordId {
    @synchronized(_contactSearchMatchingMutex) {
        if([_contactSearchMatching valueForKey:addressBookRecordId])
            [_contactSearchMatching removeObjectForKey:addressBookRecordId];
        else
            NSLog(@"Contact server ID %@ don't exist", addressBookRecordId);
    }
}

-(NSDictionary *) getContactServerInfoForAddressBookRecordId:(NSString *) addressBookRecordId {
    NSDictionary *contactServerInfo = nil;
    @synchronized(_contactSearchMatchingMutex) {
        contactServerInfo = (NSDictionary*)[_contactSearchMatching objectForKey:addressBookRecordId];
    }
    return contactServerInfo;
}

-(void) writeContactSearchMatchingToDisk {
    [[RainbowUserDefaults sharedInstance] setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSDictionary*)_contactSearchMatching] forKey:@"contactSearchMatching"];
}

-(Contact*) contact {
    return _contactsManagerService.myContact;
}

-(void) reset {
    [_settings reset];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:@"source"];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:@"contactSearchMatching"];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kMyUserFeatureWebRTCMobileKey];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kMyUserFeatureConferenceAllowedKey];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kMyUserFeatureConferenceParticipantAllowedKey];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kMyUserFeatureWebRTCParticipantAllowedKey];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kMyUserFeatureWebRTCMobileVideoKey];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kMyUserFeatureWebRTCParticipantAllowedKey];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kMyUserFeatureMaxParticipantPerBulleKey];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kMyUserFeatureSearchActiveDirectoryAllowedKey];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kMyUserFeatureMaxWebRTCParticipantsPerBulleKey];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kMyUserFeatureTelephonyNomadicAllowedKey];
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kMyUserFeatureSearchPBXPhonebookAllowedKey];
}

#pragma mark - Login manager notification

-(void) didFailedToAuthenticate:(NSNotification *) notifcation {
}

-(void) didLogin:(NSNotification *)notification {
    [self fetchPresenceSettingsWithCompletionHandler:^(PresenceSettings newPresenceSettings) {
        // We must set the presence of our ressource to the good one so invoke the didChangeUserSettings method
        [_contactsManagerService setMyPresence:[MyUser presenceForPresenceSettings:_presenceSettings]];
    }];
    [self fetchProfilesFeatures];
    [self fetchConferenceUser];
    [self fetchProfiles];
    
    // Login has been successfull so all the informations saved in userData are valid, we can update the userSettingsVersion
    _settings.userSettingsVersion = kUserSettingsCurrentVersion;
    
    // Request PBX for voicemails
    [self registerForVoicemail];
}

-(void) didReconnect:(NSNotification *) notification {
    [self fetchPresenceSettingsWithCompletionHandler:^(PresenceSettings newPresenceSettings) {
        // We must set the presence of our ressource to the good one so invoke the didChangeUserSettings method
        [_contactsManagerService setMyPresence:[MyUser presenceForPresenceSettings:_presenceSettings]];
    }];
    [self fetchProfilesFeatures];
    [self fetchConferenceUser];
    
    // Request PBX for voicemails
    [self registerForVoicemail];
}

-(void) registerForVoicemail {
    [_xmppService sendCallServiceConnexionRequestToTarget:self.contact.jid_tel];
}

-(void) didLogout:(NSNotification *)notification {
    [_roles removeAllObjects];
    [_profilesName removeAllObjects];
    [self setToken:nil];
    [self setJid_im:nil];
    [self setJid_password:nil];
}

-(void) didChangeUser:(NSNotification *) notification {
    [self didLogout:nil];
}

#pragma mark - Settings api
-(void) fetchConferenceUser {
    NSURL *url = [_apiURLManager getURLForService:ApiServicesConfProvisioningUsers, self.contact.rainbowID];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesConfProvisioningUsers];
    NSLog(@"Get conference user");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *urlResponse) {
        if (error) {
            NSLog(@"Get conference user request returned an error: %@", error);
            return;
        }
        NSDictionary *response = [receivedData objectFromJSONData];
        if(!response) {
            NSString *jsonData = [[NSString alloc] initWithBytes:[receivedData bytes] length:[receivedData length] encoding:NSUTF8StringEncoding];
            NSLog(@"Get conference user JSON parsing failed, received data was :\n%@", jsonData);
            return;
        }
        
        NSArray *data = [response objectForKey:@"data"];
        if(data && data.count>0){
            NSDictionary *jsonDic = [data objectAtIndex:0];
            if(jsonDic){
                _pgiConfUserID = [jsonDic objectForKey:@"id"];
                NSLog(@"PGI USER CONF ID %@", _pgiConfUserID);
            }
        }
    }];
}

-(void) fetchPresenceSettingsWithCompletionHandler:(FetchPresenceSettingsCompletionHandler) completionHandler {
    NSURL *url = [_apiURLManager getURLForService:ApiServicesMyUserSettings];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesMyUserSettings];
    NSLog(@"Get my user presence setting");
    __weak __typeof__ (self) weakSelf = self;
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get my user presence setting in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get my user settings in REST due to JSON parsing failure");
            return;
        }
        
        NSDictionary *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get my user presence setting response data ...");
        if(data[@"presence"]){
            _presenceSettings = [[weakSelf class] presenceSettingsFromString:data[@"presence"]];
            if(completionHandler)
                completionHandler(_presenceSettings);
        }
    }];
}

-(void) updatePresenceSettings:(PresenceSettings) newPresenceSettings {
    NSURL *url = [_apiURLManager getURLForService:ApiServicesMyUserSettings];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesMyUserSettings];
    NSString *bodyValue = [[self class] stringForPresenceSettings:newPresenceSettings];
    
    _presenceSettings = newPresenceSettings;
    NSLog(@"Update presence settings to %@", [MyUser stringForPresenceSettings:newPresenceSettings]);
    __weak __typeof__ (self) weakSelf = self;
    [_downloadManager putRequestWithURL:url body:[@{@"presence":bodyValue} jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot update presence setting in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        if(!error){
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(jsonResponse){
                NSLog(@"Update presence settings done ...");
                if(jsonResponse[@"presence"])
                    _presenceSettings = [[weakSelf class] presenceSettingsFromString:jsonResponse[@"presence"]];
            }
        }
    }];
}

#pragma mark - PresenceSettings facilitors
+(NSString *) stringForPresenceSettings:(PresenceSettings) presenceSettings {
    switch (presenceSettings) {
        case PresenceSettingsUnknown: {
            return @"unknonw";
            break;
        }
        case PresenceSettingsOnline: {
            return @"online";
            break;
        }
        case PresenceSettingsAway: {
            return @"away";
            break;
        }
        case PresenceSettingsInvisible: {
            return @"invisible";
            break;
        }
        case PresenceSettingsDND: {
            return @"dnd";
            break;
        }
    }
}

+(PresenceSettings) presenceSettingsFromString:(NSString *) presenceString {
    if ([presenceString isEqualToString:@"online"]) {
        return PresenceSettingsOnline;
    } else if ([presenceString isEqualToString:@"away"]) {
        return PresenceSettingsAway;
    } else if ([presenceString isEqualToString:@"invisible"]) {
        return PresenceSettingsInvisible;
    } else if ([presenceString isEqualToString:@"dnd"]) {
        return PresenceSettingsDND;
    } else {
        return PresenceSettingsUnknown;
    }
}

+(PresenceSettings) presenceSettingsFromPresence:(Presence *) presence {
    switch (presence.presence) {
        case ContactPresenceBusy:
        case ContactPresenceUnavailable: {
            // Not applicable
            break;
        }
        case ContactPresenceAvailable: {
            return PresenceSettingsOnline;
            break;
        }
        case ContactPresenceDoNotDisturb: {
            return PresenceSettingsDND;
            break;
        }
        case ContactPresenceAway: {
            return PresenceSettingsAway;
            break;
        }
        case ContactPresenceInvisible: {
            return PresenceSettingsInvisible;
            break;
        }
    }
    return PresenceSettingsUnknown;
}

+(Presence *) presenceForPresenceSettings:(PresenceSettings) presenceSettings {
    switch (presenceSettings) {
        case PresenceSettingsUnknown: {
            // Not applicable
            break;
        }
        case PresenceSettingsOnline: {
            return [Presence presenceAvailable];
            break;
        }
        case PresenceSettingsAway: {
            return [Presence presenceAway];
            break;
        }
        case PresenceSettingsInvisible: {
            return [Presence presenceExtendedAway];
            break;
        }
        case PresenceSettingsDND: {
            return [Presence presenceDoNotDisturb];
            break;
        }
    }
    return nil;
}

#pragma mark - Profiles

-(BOOL) isUser {
    return [_roles containsObject:@"user"] && ![_roles containsObject:@"admin"] && ![_roles containsObject:@"superadmin"];
}

-(BOOL) isAllowedToUseWebRTCMobile {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureWebRTCMobileKey];
}

-(BOOL) isAllowedToUseTelephonyConference {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureConferenceAllowedKey];
}

-(BOOL) isAllowedToParticipateInTelephonyConference {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureConferenceParticipantAllowedKey];
}

-(BOOL) isAllowedToUseWebRTCTelephonyConference {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureWebRTCConferenceAllowedKey];
}

-(BOOL) isAllowedToParticipateInWebRTCMobile {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureWebRTCParticipantAllowedKey];
}

-(BOOL) isAllowedToUseWebRTCMobileVideo {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureWebRTCMobileVideoKey];
}

-(BOOL) isAllowedToSearchInActiveDirectory {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureSearchActiveDirectoryAllowedKey];
}

-(BOOL) isAllowedToUseTelephonyNomadicMode {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureTelephonyNomadicAllowedKey] && self.isAllowedToUseTelephony;
}

-(BOOL) isAllowedToSearchInPBXPhonebook {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureSearchPBXPhonebookAllowedKey];
}

-(BOOL) isAllowedToUseTelephonyWebRTCtoPSTN {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureTelephonyWebRTCtoPSTNAllowedKey] && self.isAllowedToUseTelephony;
}

-(BOOL) isAllowedToUseTelephonyWebRTCGateway {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureTelephonyWebRTCGatewayAllowedKey] && self.isAllowedToUseTelephony;
}
    
-(BOOL) isAllowedToUseTelephonyCallForward {
    return [[RainbowUserDefaults sharedInstance] boolForKey:kMyUserFeatureTelephonyCallForwardAllowedKey] && self.isAllowedToUseTelephony;
}

-(BOOL) isAllowedToUseTelephony {
    return self.contact.hasPBXAccess;
}

-(NSInteger) maxNumberOfParticipantPerRoom {
    NSNumber *maxNum = [[RainbowUserDefaults sharedInstance] objectForKey:kMyUserFeatureMaxParticipantPerBulleKey];
    if(maxNum)
        return maxNum.integerValue;
    else
        return 20;
}

-(NSInteger) maxNumberOfWebRTCParticipantsPerRoom {
    NSNumber *maxNum = [[RainbowUserDefaults sharedInstance] objectForKey:kMyUserFeatureMaxWebRTCParticipantsPerBulleKey];
    if(maxNum)
        return maxNum.integerValue;
    else
        return 20;
}

-(void) fetchProfilesFeatures {
    NSURL *serviceUrl = [_apiURLManager getURLForService:ApiServicesProfiles];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/features", serviceUrl]];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesProfiles];
    NSLog(@"Get my user profiles features");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get my user profiles features in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get my user profiles features in REST due to JSON parsing failure");
            return;
        }
        
        NSArray *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get my user profiles features response data ...");
        
        // Set all values to some defaults values and update them if we have them
        [[RainbowUserDefaults sharedInstance] setBool:YES forKey:kMyUserFeatureWebRTCMobileKey];
        [[RainbowUserDefaults sharedInstance] setBool:YES forKey:kMyUserFeatureWebRTCMobileVideoKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureWebRTCParticipantAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureConferenceAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureWebRTCMobileVideoKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureSearchActiveDirectoryAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:YES forKey:kMyUserFeatureConferenceParticipantAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureWebRTCConferenceAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureTelephonyNomadicAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureSearchPBXPhonebookAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureTelephonyCallForwardAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureTelephonyWebRTCtoPSTNAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureTelephonyWebRTCGatewayAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureTelephonySecondCallAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureTelephonyTransferCallAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureTelephonyConferenceCallAllowedKey];
        [[RainbowUserDefaults sharedInstance] setBool:NO forKey:kMyUserFeatureTelephonyVoiceMailAllowedKey];
        
        if(data.count > 0){
            for (NSDictionary *featuresDic in data) {
                NSString *featureUniqueRef = [featuresDic objectForKey:@"featureUniqueRef"];
                if([featureUniqueRef isEqualToString:@"WEBRTC_FOR_MOBILE"]){
                    BOOL isWebRTCMobileFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isWebRTCMobileFeature forKey:kMyUserFeatureWebRTCMobileKey];
                }
                if([featureUniqueRef isEqualToString:@"WEBRTC_FOR_MOBILE_VIDEO"]){
                    BOOL isWebRTCMobileVideoFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isWebRTCMobileVideoFeature forKey:kMyUserFeatureWebRTCMobileVideoKey];
                }
                if([featureUniqueRef isEqualToString:@"WEBRTC_PARTICIPANT_ALLOWED"]){
                    BOOL isWebRTCParticipantAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isWebRTCParticipantAllowedFeature forKey:kMyUserFeatureWebRTCParticipantAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"BUBBLE_PARTICIPANT_COUNT"]){
                    NSInteger maxNbInBulle = ((NSString*)featuresDic[@"limitMax"]).integerValue;
                    [[RainbowUserDefaults sharedInstance] setObject:[NSNumber numberWithInteger:maxNbInBulle] forKey:kMyUserFeatureMaxParticipantPerBulleKey];
                }
                if([featureUniqueRef isEqualToString:@"CONFERENCE_ALLOWED"]){
                    BOOL isConferenceAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isConferenceAllowedFeature forKey:kMyUserFeatureConferenceAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"CONFERENCE_PARTICIPANT_ALLOWED"]){
                    BOOL isConferenceParticipantAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isConferenceParticipantAllowedFeature forKey:kMyUserFeatureConferenceParticipantAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"MSO365_DIRECTORY_SEARCH"]){
                    BOOL isSearchActiveDirectoryAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isSearchActiveDirectoryAllowedFeature forKey:kMyUserFeatureSearchActiveDirectoryAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"WEBRTC_CONFERENCE_PARTICIPANT_COUNT"]){
                    NSInteger maxNbWebRTCParticipantInBulle = ((NSString*)featuresDic[@"limitMax"]).integerValue;
                    [[RainbowUserDefaults sharedInstance] setObject:[NSNumber numberWithInteger:maxNbWebRTCParticipantInBulle] forKey:kMyUserFeatureMaxWebRTCParticipantsPerBulleKey];
                }
                if([featureUniqueRef isEqualToString:@"WEBRTC_CONFERENCE_ALLOWED"]){
                    BOOL iswebRTCConferenceAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:iswebRTCConferenceAllowedFeature forKey:kMyUserFeatureWebRTCConferenceAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"TELEPHONY_NOMADIC"]){
                    BOOL isTelephonyNomadicAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isTelephonyNomadicAllowedFeature forKey:kMyUserFeatureTelephonyNomadicAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"TELEPHONY_PHONE_BOOK"]){
                    BOOL isSearchPBXPhonebookAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isSearchPBXPhonebookAllowedFeature forKey:kMyUserFeatureSearchPBXPhonebookAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"TELEPHONY_CALL_FORWARD"]){
                    BOOL isTelephonyCallForwardAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isTelephonyCallForwardAllowedFeature forKey:kMyUserFeatureTelephonyCallForwardAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"TELEPHONY_WEBRTC_PSTN_CALLING"]){
                    BOOL isTelephonyWebRTCtoPSTNAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isTelephonyWebRTCtoPSTNAllowedFeature forKey:kMyUserFeatureTelephonyWebRTCtoPSTNAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"TELEPHONY_WEBRTC_GATEWAY"]){
                    BOOL isTelephonyWebRTCGatewayAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isTelephonyWebRTCGatewayAllowedFeature forKey:kMyUserFeatureTelephonyWebRTCGatewayAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"TELEPHONY_SECOND_CALL"]){
                    BOOL isTelephonySecondCallAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isTelephonySecondCallAllowedFeature forKey:kMyUserFeatureTelephonySecondCallAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"TELEPHONY_TRANSFER_CALL"]){
                    BOOL isTelephonyTransferCallAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isTelephonyTransferCallAllowedFeature forKey:kMyUserFeatureTelephonyTransferCallAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"TELEPHONY_CONFERENCE_CALL"]){
                    BOOL isTelephonyConferenceCallAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isTelephonyConferenceCallAllowedFeature forKey:kMyUserFeatureTelephonyConferenceCallAllowedKey];
                }
                if([featureUniqueRef isEqualToString:@"TELEPHONY_VOICE_MAIL"]){
                    BOOL isTelephonyVoiceMailAllowedFeature = ((NSString *)featuresDic[@"isEnabled"]).boolValue;
                    [[RainbowUserDefaults sharedInstance] setBool:isTelephonyVoiceMailAllowedFeature forKey:kMyUserFeatureTelephonyVoiceMailAllowedKey];
                }
            }
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserFeatureDidUpdate object:nil];
    }];
}

-(void) fetchProfiles {
    NSURL *url = [_apiURLManager getURLForService:ApiServicesProfiles];
    NSDictionary *headers = [_apiURLManager getHTTPHeaderParametersForService:ApiServicesProfiles];
    NSLog(@"Get my user profiles");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get my user profiles in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get my user profiles in REST due to JSON parsing failure");
            return;
        }
        
        NSArray *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"Get my user profiles response data ...");
        NSMutableArray *currentProfiles = [NSMutableArray array];
        for (NSDictionary *profile in data) {
            NSString *profileName = profile[@"offerName"];
            [currentProfiles addObject:profileName];
        }
        
        [self setProfilesName:currentProfiles];
    }];
}

-(void) setAvatar:(UIImage *)image withCompletionHandler:(MyUserAvatarUploadCompletionHandler) completionHandler {
    if(!image){
        if(completionHandler){
            NSError *error = [NSError errorWithDomain:@"MyUser" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Image must not be nil"}];
            completionHandler(error);
        }
        return;
    }
    [_contactsManagerService updateUserWithFields:@{@"photo" : image} withCompletionBlock:^(NSError *error) {
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

#pragma mark - Telephony MediaPillar



#pragma mark - Telephony Nomadic status

-(BOOL) isNomadicModeActivated {
    if (!_nomadicStatus || !_nomadicStatus.featureActivated || !_nomadicStatus.activated || !_nomadicStatus.destination)
        return NO;
    
    // Compare the nomadic number and the mediapillar number
    if ([[self getMediapillarNumber] isEqualToString:_nomadicStatus.destination.numberE164])
        return YES;
    
    // Compare the nomadic number with the available mobile numbers (work and/or personal)
    PhoneNumber *workMobileNumber = [self.contact getPhoneNumberOfType:PhoneNumberTypeWork withDeviceType:PhoneNumberDeviceTypeMobile];
    PhoneNumber *personalMobileNumber = [self.contact getPhoneNumberOfType:PhoneNumberTypeHome withDeviceType:PhoneNumberDeviceTypeMobile];

    NSMutableArray <PhoneNumber *> *validNumbers = [NSMutableArray array];
    if(workMobileNumber)
        [validNumbers addObject:workMobileNumber];
    if(personalMobileNumber)
        [validNumbers addObject:personalMobileNumber];

    if ([validNumbers containsObject:_nomadicStatus.destination])
        return YES;
    
    // The nomadic seems to be activated but its number does not correspond to mediapillar and to any mobile numbers, so return NO
    return NO;
}

-(NSString *) getMediapillarNumber {
    if (_mediaPillarStatus && _mediaPillarStatus.prefix && _mediaPillarStatus.rainbowPhoneNumber)
        return [NSString stringWithFormat:@"%@%@", _mediaPillarStatus.prefix, _mediaPillarStatus.rainbowPhoneNumber];
    else
        return @"";
}

#pragma mark - RTC call monitoring
/**
 * for outgoing call we mut change our presence to busy audio (for audio call)
 * for incoming call we must *NOT* change our presence since the call is not connected we will do it in didUpdate
 * when removing call we must return to our previous presence
 */
-(void) didAddCall:(NSNotification *)notification {
    Call *call = (Call *)notification.object;
    if(!call.isIncoming){
        ContactPresence myPresence = self.contact.presence.presence;
        if(myPresence != ContactPresenceInvisible && myPresence != ContactPresenceUnavailable && myPresence != ContactPresenceDoNotDisturb){
            // Set the presence to dnd with audio or video status
            Presence *dnd = [Presence presenceDoNotDisturb];
            if([call isKindOfClass:[RTCCall class]]){
                dnd.status = ((RTCCall*)call).isVideoEnabled?@"video":@"audio";
            } else
                dnd.status = @"phone";
            [_contactsManagerService setMyPresence:dnd];
        }
    }
}

-(void) didUpdateCall:(NSNotification *)notification {
    Call *call = (Call *)notification.object;
    if(call.status == CallStatusEstablished){
        ContactPresence myPresence = self.contact.presence.presence;
        if(myPresence != ContactPresenceInvisible && myPresence != ContactPresenceUnavailable && myPresence != ContactPresenceDoNotDisturb){
            // Set the presence to dnd with audio or video status
            Presence *dnd = [Presence presenceDoNotDisturb];
            if([call isKindOfClass:[RTCCall class]]){
                dnd.status = ((RTCCall*)call).isVideoEnabled?@"video":@"audio";
            } else
                dnd.status = @"phone";
            [_contactsManagerService setMyPresence:dnd];
        }
    }
}

-(void) didRemoveCall:(NSNotification *)notification {
    // Reapply previous presence based on presenceSettings except if we was in manual away in that case we return into online presence
    Presence *previousPresence = [MyUser presenceForPresenceSettings:_presenceSettings];
    [_contactsManagerService setMyPresence:previousPresence];
}

-(void) registerDefaultsSettings {
    [[RainbowUserDefaults sharedInstance] registerDefaults:@{kMyUserFeatureMaxParticipantPerBulleKey:@20}];
}

@end
