/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RoomsService.h"
#import "DownloadManager.h"
#import "ApiUrlManagerService.h"
#import "ContactsManagerService.h"
#import "XMPPService.h"
#import "ConferencesManagerService.h"
#import "Room.h"
#define kRoomsServiceDidJoinRoom @"didJoinRoom"
typedef void (^RoomsManagerCreateRoomWithRainbowID)(Room * room);

@interface RoomsService ()

@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property (nonatomic, strong) ContactsManagerService *contactsManagerService;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) NSObject *roomsMutex;
@property (nonatomic, readwrite, strong) NSMutableArray<Room *> *rooms;
@property (nonatomic, strong) NSMutableArray <Room*> *roomsLoadedFromCache;
@property (nonatomic, strong) NSObject *cacheMutex;
@property (nonatomic) BOOL cacheLoaded;
/**
 * Boolean indicating wether rooms have been fully loaded from server
 */
@property (nonatomic) BOOL roomFullyLoaded;
@property (nonatomic) BOOL cacheLoadOngoing;

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService xmppService:(XMPPService*) xmppService;

@property (nonatomic, weak) ConferencesManagerService *conferenceManagerService;

-(void) getRoomAsynchronouslyWithRainbowID:(NSString *) rainbowID withCompletionHandler:(RoomsManagerCreateRoomWithRainbowID) completionHandler;

-(Room *) getRoomByRainbowID:(NSString *) rainbowID;
-(Room *) getRoomByRTCJid:(NSString *) rtcJid;
-(Room *) createOrUpdateRoomFromJSON:(NSDictionary *) dict;

-(Room *) createOrUpdateRoomFromJID:(NSString *) jid withRainbowID:(NSString *) rainbowID;
-(void) removeRoom:(Room *) room;

-(void) addParticipant:(Participant *) participant inRoom:(Room *) room;
-(void) updateParticipant:(NSString *) participantJid withStatus:(ParticipantStatus) status inRoom:(Room *) room;
-(void) internalUpdateParticipant:(NSString *) participantJid withPrivilege:(ParticipantPrivilege)privilege inRoom:(Room *)room;

/* Used by ConferenceManagerService when the conference tied to the room need to be updated in the cache */
-(void) updateRoomInCache:(Room *)room;

@end
