/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RoomsService.h"
#import "RoomsService+Internal.h"
#import "NSDictionary+JSONString.h"
#import "Room+Internal.h"
#import "ContactsManagerService+Internal.h"
#import "Tools.h"
#import "NSDate+JSONString.h"
#import "NSData+JSON.h"
#import "PINCache.h"
#import "NSDate+Utilities.h"
#import "LoginManager+Internal.h"
#import "NSDictionary+ChangedKeys.h"
#import "NSString+MD5.h"
#import "NSDate+Equallity.h"
#import "NSObject+NotNull.h"
#import "ConferencesManagerService+Internal.h"
#import "ConfEndpoint+Internal.h"
#import "LoginManager+Internal.h"
#import "PINCache+OperationQueue.h"
#import <Rainbow/RainbowUserDefaults.h>

NSString *const kRoomKey = @"room";
NSString *const kRoomChangedAttributesKey = @"roomChangedAttributes";
NSString *const kRoomsServiceDidAddRoom = @"didAddRoom";
NSString *const kRoomsServiceDidUpdateRoom = @"didUpdateRoom";
NSString *const kRoomsServiceDidRemoveRoom = @"didRemoveRoom";
NSString *const kRoomsServiceDidRemoveAllRooms = @"didRemoveAllRooms";
NSString *const kRoomsServiceDidReceiveRoomInvitation = @"didReceiveRoomInvitation";
NSString *const kRoomsServiceDidRoomInvitationStatusChanged = @"didRoomInvitationStatusChanged";
NSString *const kRoomsServiceDidFailRemoveRoom = @"didFailRemoveRoom";
NSString *const kRoomFullyLoaded = @"roomFullyLoaded";

typedef void (^RoomsServiceLoadAvatarInternalCompletionHandler)(NSData *receivedData, NSError *error, NSURLResponse *response);


@interface RoomsService () <XMPPServiceRoomsDelegate>
@property (nonatomic, strong) PINCache *roomCache;
@property (nonatomic, strong) PINCache *roomAvatarCache;
@end

@implementation RoomsService

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService xmppService:(XMPPService*) xmppService {
    self = [super init];
    if (self) {
        _cacheLoaded = NO;
        _downloadManager = downloadManager;
        _apiUrlManagerService = apiUrlManagerService;
        _contactsManagerService = contactsManagerService;
        _xmppService = xmppService;
        _xmppService.roomsDelegate = self;
        _rooms = [NSMutableArray array];
        _roomsMutex = [NSObject new];
        _cacheMutex = [NSObject new];
        _roomsLoadedFromCache = [NSMutableArray array];
        _roomFullyLoaded = [[RainbowUserDefaults sharedInstance] boolForKey:kRoomFullyLoaded];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willLogin:) name:kLoginManagerInternalWillLogin object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        
        _roomCache = [[PINCache alloc] initWithName:@"roomCache"];
        _roomCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        
        _roomAvatarCache = [[PINCache alloc] initWithName:@"roomAvatarCache"];
        _roomAvatarCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        [_roomAvatarCache trimToDateAsync:[NSDate dateWithDaysBeforeNow:30] completion:nil];
    
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
           [self loadRoomsFromCacheSynchronously];
        });
        
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalWillLogin object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    
    [_rooms removeAllObjects];
    _rooms = nil;
    [_roomsLoadedFromCache removeAllObjects];
    _roomsLoadedFromCache = nil;
    _roomsMutex = nil;
    _downloadManager = nil;
    _apiUrlManagerService = nil;
    _contactsManagerService = nil;
    _xmppService.roomsDelegate = nil;
    _xmppService = nil;
    _roomCache = nil;
    _roomAvatarCache = nil;
    _cacheMutex = nil;
    
}

-(void) willLogin:(NSNotification *) notification {
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
//        if (!_cacheLoaded) {
//            [self loadRoomsFromCache];
//            _cacheLoaded = NO;
//        }
//    });
    
//    [self loadRoomsFromCacheSynchronously];
}

-(void) didChangeUser:(NSNotification *) notification {
    [_roomCache removeAllObjects];
    [_roomAvatarCache removeAllObjects];
}

/**
 *  On login event, we fetch our rooms an the room we are in from the server.
 */
-(void) didLogin:(NSNotification *) notification {
    [self fetchRooms];
}

-(void) didLogout:(NSNotification *) notification {
    @synchronized (_roomsMutex) {
        [_rooms removeAllObjects];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidRemoveAllRooms object:nil];
    [_roomCache.operationQueue cancelAllOperations];
}

-(void) didReconnect:(NSNotification *) notification {    
    if (_roomFullyLoaded) {
        // Do not fetch rooms on reconnect it's not needed
        // TODO: Resynchronize the fake rooms from xmpp messages
        // We have to join rooms
        @synchronized(_roomsMutex){
            for (Room *aRoom in _rooms) {
                // Join/leave room from an xmpp point of view
                // Do this out of the @syncronized
                if(aRoom.myStatusInRoom == ParticipantStatusAccepted) {
                    [_xmppService joinRoom:aRoom];
                }
                if(aRoom.myStatusInRoom == ParticipantStatusUnsubscribed) {
                    [_xmppService leaveRoom:aRoom];
                }
            }
        }
    } else {
        //Rooms were not retrieved from server, need to resynchronize cache
        NSLog(@"Room not Fully loaded => reload");
        [self fetchRooms];
    }
}

-(void) loadRoomsFromCacheSynchronously {
    // Load rooms from cache
    __weak __typeof__(self) weakSelf = self;
    
    if (_cacheLoadOngoing) {
        NSLog(@"[RoomsManager] Room cache : cache load ongoing");
        return;
    }
    _cacheLoadOngoing = true;
    [_roomsLoadedFromCache removeAllObjects];
    NSMutableArray *existingKeys = [NSMutableArray array];
    
    [_roomCache.diskCache enumerateObjectsWithBlock:^(NSString * _Nonnull key, NSURL * _Nullable fileURL, BOOL * _Nonnull stop) {
        [existingKeys addObject:key];
    }];
    
    NSLog(@"[RoomsManager] Room cache : Load cache Completion block start");
    for (NSString *key in existingKeys) {
        NSDictionary *roomDict = [_roomCache objectForKey:key];
        Room *theRoom = [weakSelf createOrUpdateRoomFromJSON:roomDict];
        theRoom.rainbowID = key;
        if(theRoom.conference){
            if(theRoom.conference.endpoint) {
                if(theRoom.conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC){
                    // Don't restore active state of WebRTC conferences
                    theRoom.conference.isActive = NO;
                    theRoom.conference.status = ConferenceStatusUnknown;
                    theRoom.conference.endpoint.attachedRoomID = nil;
                    theRoom.conference.myConferenceParticipant = nil;
                    [theRoom.conference resetParticipants];
                } else {
                    // We can ask for conference details
                    [_conferenceManagerService fetchConfEndpointDetails:theRoom.conference.endpoint completionBlock:^(NSError *error) {
                        NSLog(@"[RoomsManager] Fetch conference details done %@", error);
                        if(!error){
                            if(!theRoom.isMyRoom){
                                [_conferenceManagerService fetchConferenceSnapshot:theRoom.conference completionBlock:^(NSError *error) {
                                    NSLog(@"[RoomsManager] Fetch conference snapshot %@", error);
                                    if(theRoom.conference.status == ConferenceStatusAttached)
                                        theRoom.conference.endpoint.attachedRoomID = theRoom.rainbowID;
                                }];
                            }
                        }
                    }];
                }
            }
        }
        @synchronized(_cacheMutex){
            if(![_roomsLoadedFromCache containsObject:theRoom])
                [_roomsLoadedFromCache addObject:theRoom];
        }
    }
    NSLog(@"[RoomsManager] Room cache : Load cache Completion block end");
    // Search for missing vcards
    [_roomsLoadedFromCache enumerateObjectsUsingBlock:^(Room * aRoom, NSUInteger idx, BOOL * stop) {
        for (Participant *aParticipant in aRoom.participants) {
            if(!aParticipant.contact.vcardPopulated && aParticipant.contact.jid.length > 0){
                [_contactsManagerService addJidToFetch:aParticipant.contact.jid];
            }
        }
    }];
    _cacheLoadOngoing = false;
}

-(void) fetchRooms {
    [self fetchRoomsWithOffset:0];
}

-(void) fetchRoomsWithOffset:(NSInteger) offset {
    
    _roomFullyLoaded = NO;
   [[RainbowUserDefaults sharedInstance] setBool:_roomFullyLoaded forKey:kRoomFullyLoaded];
    // Make a REST get all my rooms and the rooms we are in.
    NSInteger pageSize = 100;
    NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesRooms];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?userId=%@&format=full&offset=%li&limit=%li", url_base, _contactsManagerService.myContact.rainbowID, offset, pageSize]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRooms];
    
    NSLog(@"[RoomsManager] Get rooms list");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[RoomsManager] Error: Cannot get all rooms in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"[RoomsManager] Error: Cannot get all rooms in REST due to JSON parsing failure");
            return;
        }
        
        NSUInteger offset = [((NSString *)([jsonResponse[@"offset"] notNull])) integerValue];
        NSUInteger total = [((NSString *)([jsonResponse[@"total"] notNull])) integerValue];
        
        NSDictionary *data = [jsonResponse objectForKey:@"data"];
        NSLog(@"[RoomsManager] Get rooms response data .... total %li offset %li",total,offset);
        
        // List of rooms returned by server.
        NSMutableArray *listOfRooms = [NSMutableArray array];
        BOOL shoudUpdate;
        
        for (NSDictionary *roomDict in data) {
            
            shoudUpdate = [self shouldLoadUpdateAvatar:roomDict forRoom: [self getRoomByJid:[roomDict objectForKey:@"jid"]] ];
            
            Room *theRoom = [self createOrUpdateRoomFromJSON:roomDict];
            [listOfRooms addObject:theRoom];
            
            if( shoudUpdate ) {
                [self populateAvatarForRoom:theRoom forceReload:YES atSize:512];
            }
            
        }
        
        @synchronized (_cacheMutex) {
            NSMutableArray *fullRoomsList = [NSMutableArray arrayWithArray:_rooms];
            [fullRoomsList addObjectsFromArray:listOfRooms];
            // Check if there is some rooms that must be removed (because loaded from cache)
            [_roomsLoadedFromCache enumerateObjectsUsingBlock:^(Room * aRoom, NSUInteger idx, BOOL * stop) {
                if(![fullRoomsList containsObject:aRoom]){
                    [self removeRoom:aRoom];
                }
            }];
        }
        
        for (Room *aRoom in listOfRooms) {
            // Join/leave room from an xmpp point of view
            // Do this out of the @syncronized
            if(aRoom.myStatusInRoom == ParticipantStatusAccepted) {
                [_xmppService joinRoom:aRoom];
            }
            if(aRoom.myStatusInRoom == ParticipantStatusUnsubscribed) {
                [_xmppService leaveRoom:aRoom];
            }
            // Request all missing vcard
            // Android search by rainbowID
            for (Participant *aParticipant in aRoom.participants) {
                if(!aParticipant.contact.vcardPopulated && aParticipant.contact.jid.length > 0){
                    [_contactsManagerService addJidToFetch:aParticipant.contact.jid];
                }
            }
        }
        
        if(total > offset+pageSize){
            [self fetchRoomsWithOffset:offset+pageSize];
        }
        _roomFullyLoaded = YES;
        [[RainbowUserDefaults sharedInstance] setBool:_roomFullyLoaded forKey:kRoomFullyLoaded];
        
    }];
}

-(Room*) getRoomByJid:(NSString *) jid {
    @synchronized (_roomsMutex) {
        for (Room *room in _rooms) {
            if ([room.jid isEqualToString:jid]) {
                return room;
            }
        }
        return nil;
    }
}

-(Room*) getOrCreateRainbowRoomSynchronouslyWithJid:(NSString *) room_jid {
    __block Room *theRoom;
    int max_retry = 50;
    int retry = 0;
    
    @synchronized (_rooms) {
        
        theRoom = [self getRoomByJid:room_jid];
        if(theRoom && [theRoom.rainbowID isEqualToString: @"fake_rainbow_id"]){
            [_rooms removeObject:theRoom];
            theRoom = nil;
        }
        theRoom = [self getRoomByJid:room_jid];
    }
    
    if(!theRoom) {
        __block BOOL finished = NO;
        [self fetchRoomWithJid:room_jid withCompletionHandler:^(Room *room, NSError *error) {
            if (room) {
                theRoom = room;
            }
            finished = YES;
        }];
        
        while(!finished && retry < max_retry) {
            retry ++;
            usleep(100000);//micro seconds (100000 = 0.1 second)
        }
    }
    return theRoom;
}

-(void) getRoomAsynchronouslyWithRainbowID:(NSString *) rainbowID withCompletionHandler:(RoomsManagerCreateRoomWithRainbowID) completionHandler {
   Room * theRoom  = nil;
    @synchronized (_rooms) {
        theRoom = [self getRoomByRainbowID:rainbowID];
        if(theRoom){
            if(completionHandler)
                completionHandler(theRoom);
        } else {
            completionHandler(nil);
        }
    }
}
-(Room *) getRoomByRainbowID:(NSString *) rainbowID {
    @synchronized (_roomsMutex) {
        for (Room *room in _rooms) {
            if([room.rainbowID isEqualToString:rainbowID])
                return room;
        }
    }
    return nil;
}

-(Room *) getRoomByRTCJid:(NSString *) rtcJid {
    @synchronized (_roomsMutex) {
        for (Room *room in _rooms) {
            if([room.rtcJid isEqualToString:rtcJid])
                return room;
        }
    }
    return nil;
}

#pragma mark - Public methods
-(NSUInteger) numberOfPendingRoomInvitations {
    NSUInteger total = 0;
    @synchronized (_roomsMutex) {        
        for (Room *room in _rooms) {
            if (room.myStatusInRoom == ParticipantStatusInvited && room.isPGIConferenceRoom == NO)
                total++;
        }
    }
    return total;
}

-(NSUInteger) numberOfPendingConferenceRoomInvitations {
    NSUInteger total = 0;
    @synchronized (_roomsMutex) {
        for (Room *room in _rooms) {
            if (room.myStatusInRoom == ParticipantStatusInvited && room.isPGIConferenceRoom == YES){
                if([room.conference.end isLaterThanDate:[NSDate date]] || room.conference.type == ConferenceTypeInstant)
                    total++;
            }
        }
    }
    return total;
}

#pragma mark - CRUD Rooms
-(Room *) createOrUpdateRoomFromJSON:(NSDictionary *) dict {
    Room *room = nil;
    BOOL newRoom = NO;
   
    @synchronized (_roomsMutex) {
        room = [self getRoomByRainbowID:[dict objectForKey:@"id"]];
        if (!room) {
            room = [Room new];
            newRoom = YES;
            [_rooms addObject:room];
        }
    }
    
    NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
    room.rainbowID = [dict objectForKey:@"id"];
    room.jid = [dict objectForKey:@"jid"];
    if([dict objectForKey:@"name"])
        room.displayName = [dict objectForKey:@"name"];
    if([dict objectForKey:@"displayName"])
        room.displayName = [dict objectForKey:@"displayName"];
    if([dict objectForKey:@"topic"])
        room.topic = [dict objectForKey:@"topic"];
    
    // We have creationDate and creator only from room api so we set it only when we have them...
    if([[dict objectForKey:@"creationDate"] length] > 0 && [[dict objectForKey:@"creationDate"] isKindOfClass:[NSString class]])
        room.creationDate = [NSDate dateFromJSONString:dict[@"creationDate"]];
    else {
        if(!room.creationDate) {
            NSLog(@"[RoomsManager] We don't have the room creation date so we set it to now");
        }
    }
    
    // Only use the provided conference data if the room hasn't already a attached conference
    if([dict objectForKey:@"conference"] && !room.conference.confId){
        id conferenceObject = [dict objectForKey:@"conference"];
        if([conferenceObject isKindOfClass:[NSDictionary class]]){
            NSDictionary *conferenceDic = (NSDictionary *) conferenceObject;
            NSString *confEndPointID = nil;
            NSString *creator = nil;
            NSArray *confEndPoints = [[dict objectForKey:@"confEndpoints"] notNull];
            if(confEndPoints.count>0){
                for (NSDictionary *aConfEndPoint in confEndPoints) {
                    confEndPointID = [[aConfEndPoint objectForKey:@"confEndpointId"] notNull];
                    creator = [[aConfEndPoint objectForKey:@"userId"] notNull];
                }
            }
            
            NSMutableDictionary *jsonDic = [NSMutableDictionary new];
            if(confEndPointID)
                [jsonDic setObject:confEndPointID forKey:@"id"];
            if(creator)
                [jsonDic setObject:creator forKey:@"creator"];
            
            if(confEndPoints)
                [jsonDic setObject:confEndPoints forKey:@"confEndpoints"];
            
            
            if([[conferenceObject objectForKey:@"mediaType"] notNull]){
                NSString *mediaType = [[conferenceObject objectForKey:@"mediaType"] notNull];
                [jsonDic setObject:mediaType forKey:@"mediaType"];
            }
            
            if([[conferenceDic objectForKey:@"scheduled"] boolValue]){
                NSString *startDate = [conferenceDic[@"scheduledStartDate"] notNull];
                NSString *endDate = [conferenceDic[@"scheduledEndDate"] notNull];
                
                if(startDate)
                    [jsonDic setObject:startDate forKey:@"scheduledStartDate"];
                
                if(endDate)
                    [jsonDic setObject:endDate forKey:@"scheduledEndDate"];
            } else {
                NSString *startDate = [dict[@"creationDate"] notNull];
                if(startDate)
                    [jsonDic setObject:startDate forKey:@"scheduledStartDate"];
            }

            Conference *aConference = [_conferenceManagerService createOrUpdateConferenceFromJson:jsonDic];
            room.conference = aConference;
            if(room.conference.status == ConferenceStatusAttached)
                room.conference.endpoint.attachedRoomID = room.rainbowID;
        }
    }
    
    // Avatar last update date
    if( [dict objectForKey:@"lastAvatarUpdateDate"] && [[dict objectForKey:@"lastAvatarUpdateDate"] isKindOfClass:[NSString class]] ) {
        if([[dict objectForKey:@"lastAvatarUpdateDate"] length] > 0) {
            room.lastAvatarUpdateDate = [NSDate dateFromJSONString:dict[@"lastAvatarUpdateDate"]];
        }
    }
    
    // custom data
    if( [[dict objectForKey:@"customData"] notNull]) {
        room.customData = nil;
        room.customData = [NSDictionary dictionaryWithDictionary:[dict objectForKey:@"customData"]];
    }
    
    NSArray *users = dict[@"users"];
    NSString *creatorDictValue = [dict valueForKey:@"creator"];
    
    if(!room.creator){
        if([creatorDictValue containsString:@"@"])
            room.creator = [_contactsManagerService createOrUpdateRainbowContactWithJid:creatorDictValue];
        else
            room.creator = [_contactsManagerService createOrUpdateRainbowContactWithRainbowId:creatorDictValue];
    }
    
    if(room.creator){
        // Attach the owner contact to the conference
        room.conference.owner = room.creator;
        room.conference.organizerId = room.creator.rainbowID;
    }
    
    if ([room.creator isEqual:_contactsManagerService.myContact]){
        room.conference.isMyConference = YES;
        room.isMyRoom = YES;
        room.isAdmin = YES;
    }
    
    // Guest emails list
    if( [[dict objectForKey:@"guestEmails"] notNull]) {
        room.guestEmails = nil;
        room.guestEmails = [dict objectForKey:@"guestEmails"];
    }
    
    // Create participant from guest emails list
    // These "poor or light" participants have only a "loginEmail" and "ParticipantStatusInvitedGuest" status
    for (NSString *guestEmail in room.guestEmails) {
        [self createParticipantFromGuestEmail:guestEmail inRoom:room];
    }
    
    for (NSDictionary *userDict in users) {
        Participant *participant = [self createOrUpdateParticipantFromJSONDict:userDict inRoom:room];
        
        // am I in 'invited' state in this room ?
        if ([participant.contact isEqual:_contactsManagerService.myContact]) {
            room.myStatusInRoom = participant.status;
        }
        
        // Search the participant with the moderator privilege
        // Single CREATOR - Multiple MODERATOR
        if(participant.privilege == ParticipantPrivilegeModerator) {
            // Change the privilege of the real owner to PrivilegeOwner
            if([participant.contact isEqual:room.creator])
                participant.privilege = ParticipantPrivilegeOwner;
            
            // Participant can be a moderator
            if ([participant.contact isEqual:_contactsManagerService.myContact]) {
                room.isAdmin = YES;
            }
        }
    }
    
    // Don't extract creator from json dict because we only have his rainbow ID.
    
    if ([dict[@"visibility"] isEqualToString:@"private"]) {
        room.visibility = RoomVisibilityPrivate;
    } else if ([dict[@"visibility"] isEqualToString:@"public"]) {
        room.visibility = RoomVisibilityPublic;
    }
    if (newRoom) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidAddRoom object:room];
    } else {
        [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
        [[NSNotificationCenter defaultCenter] postNotificationName:kConversationsManagerDidUpdateConversation object:nil];
    }
    
    if(![[dict objectForKey:@"type"] notNull]){
        // We found a type in the dictionary that means we comes from the conversation content, so we don't want to save this room in the cache because it doesn't contain all the information to save it properly.
        [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:^(id<PINCaching>  _Nonnull cache, NSString * _Nonnull key, NSDictionary *object) {
//            NSLog(@"[RoomsManager] cache saved for key %@ object count %ld",key, object.count);
        }];
    }
    if(newRoom)
        [self getAvatarInCacheForRoom:room];
    
    // I have 'invited' status in this room ? then trigger notification.
    if(room.myStatusInRoom == ParticipantStatusInvited) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidReceiveRoomInvitation object:room];
        });
    }
   
    return room;
}

/**
 *  The rainbowID parameter is temporary. Once we'll be able to search by JID, we can remove this parameter.
 *
 *  @param jid       The Room JID
 *  @param rainbowID The room rainbowID
 *
 *  @return The Newly created Room.
 */
-(Room *) createOrUpdateRoomFromJID:(NSString *) jid withRainbowID:(NSString *) rainbowID {
    @synchronized (_roomsMutex) {
        Room *room = [self getRoomByJid:jid];
        BOOL newRoom = NO;
        if (!room) {
            room = [Room new];
            newRoom = YES;
        }
        
        room.jid = jid;
        room.rainbowID = rainbowID;
        
        if (newRoom) {
            [_rooms addObject:room];
            [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidAddRoom object:room];
        }
        
        // Get the other information from server.
        [self fetchRoomInformation:room];
        
        return room;
    }
}

-(void) addParticipant:(Participant *) new_participant inRoom:(Room *) room {
    if (!new_participant) {
        return;
    }
    for (Participant *participant in room.participants) {
        if ([participant.contact isEqual:new_participant.contact]) {
            return;
        }
    }
    [room addParticipant:new_participant];
}

-(Participant *) getParticipantWithJid:(NSString *) participantJid inRoom:(Room *) room {
    __block Participant *theParticipant = nil;
    [room.participants enumerateObjectsUsingBlock:^(Participant * participant, NSUInteger idx, BOOL * stop) {
        if([participant.contact.jid isEqualToString:participantJid]){
            theParticipant = participant;
            *stop = YES;
        }
    }];
    
    return theParticipant;
}

-(Participant *) getParticipantWithRainbowID:(NSString *) rainbowID inRoom:(Room *) room {
    __block Participant *theParticipant = nil;
    [room.participants enumerateObjectsUsingBlock:^(Participant * participant, NSUInteger idx, BOOL * stop) {
        if([participant.contact.rainbowID isEqualToString:rainbowID]){
            theParticipant = participant;
            *stop = YES;
        }
    }];
    
    return theParticipant;
}

-(Participant *) getParticipantWithLoginEmail:(NSString *) loginEmail inRoom:(Room *) room {
    __block Participant *theParticipant = nil;
    [room.participants enumerateObjectsUsingBlock:^(Participant * participant, NSUInteger idx, BOOL * stop) {
        if([participant.contact.displayName isEqualToString:loginEmail]){
            theParticipant = participant;
            *stop = YES;
        }
    }];
    
    return theParticipant;
}

-(Participant *) createOrUpdateParticipantFromJSONDict:(NSDictionary *) jsonDict inRoom:(Room *) room {
    NSString *rainbowID = jsonDict[@"userId"];
    Participant *theParticipant = [self getParticipantWithRainbowID:rainbowID inRoom:room];
    
    BOOL newParticipant = NO;
    if(!theParticipant){
        theParticipant = [Participant new];
        // Search for rainbow contact ID with the given jid_im
        // Force the rainbowID in filed id like we except in other methods
        NSMutableDictionary *jsonDicModified = [NSMutableDictionary dictionaryWithDictionary:jsonDict];
        [jsonDicModified setObject:rainbowID forKey:@"id"];
        Contact *contact = [_contactsManagerService createOrUpdateRainbowContactFromJSON:jsonDicModified];
        theParticipant.contact = contact;
        newParticipant = YES;
    }
    
    theParticipant.privilege = [Participant stringToPrivilege:jsonDict[@"privilege"]];
    theParticipant.status = [Participant stringToStatus:jsonDict[@"status"]];
    if([[jsonDict objectForKey:@"additionDate"] length] > 0 && [[jsonDict objectForKey:@"additionDate"] isKindOfClass:[NSString class]])
        theParticipant.addedDate = [NSDate dateFromJSONString:jsonDict[@"additionDate"]];
    
    if(newParticipant)
        [self addParticipant:theParticipant inRoom:room];
    
    return theParticipant;
}

-(Participant *) createOrUpdateParticipantFromJid:(NSString *) jid inRoom:(Room *) room {
    @synchronized (_roomsMutex) {
        Participant *theParticipant = [self getParticipantWithJid:jid inRoom:room];
        BOOL newParticipant = NO;
        if(!theParticipant){
            theParticipant = [Participant new];
            // Search for rainbow contact ID with the given jid_im
            Contact *contact = [_contactsManagerService createOrUpdateRainbowContactWithJid:jid];
            theParticipant.contact = contact;
            newParticipant = YES;
        }
        
        theParticipant.privilege = ParticipantPrivilegeUser;
        theParticipant.status = ParticipantStatusInvited;
        theParticipant.addedDate = [NSDate date];
        if(newParticipant)
            [self addParticipant:theParticipant inRoom:room];
        
        return theParticipant;
    }
}

-(void) createParticipantFromGuestEmail:(NSString *) guestEmail inRoom:(Room *) room {
    @synchronized (_roomsMutex) {
        Participant *theParticipant = [self getParticipantWithLoginEmail:guestEmail inRoom:room];
        BOOL newParticipant = NO;
        if(!theParticipant){
            theParticipant = [Participant new];
            // Create a participant with a login email for display purposes only.
            NSDictionary *dict = @{@"loginEmail":guestEmail};
            Contact *contact = [_contactsManagerService createRainbowContactFromLoginEmail:dict];
            theParticipant.contact = contact;
            newParticipant = YES;
        }
        
        theParticipant.status = ParticipantStatusInvitedGuest;
        theParticipant.privilege = ParticipantPrivilegeGuest;
        theParticipant.addedDate = [NSDate date];
        if(newParticipant)
            [self addParticipant:theParticipant inRoom:room];
    }
}

-(void) updateParticipant:(NSString *) participantJid withStatus:(ParticipantStatus) status inRoom:(Room *) room {
    @synchronized(room){
        NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
        
        Participant *theParticipant = [self getParticipantWithJid:participantJid inRoom:room];
        if(!theParticipant)
            theParticipant = [self createOrUpdateParticipantFromJid:participantJid inRoom:room];
        
        ParticipantStatus currentStatus = theParticipant.status;
        // The server may not send the participant status in room, if it is unknown don't update it
        if(status != ParticipantStatusUnknown){
            theParticipant.status = status;
            NSLog(@"[RoomsManager] Participant %@ is now in state %@", theParticipant, [Participant stringForStatus:status]);
            if([theParticipant.contact isEqual:_contactsManagerService.myContact]){
                room.myStatusInRoom = theParticipant.status;
            }
        }
        
        if(currentStatus == ParticipantStatusInvited && status == ParticipantStatusDeleted){
            // That means we cancel the invitation so we remove the participant from the room participant list
            NSLog(@"[RoomsManager] Participant was in invited state and he is now in deleted state (we consider that as a cancel invitation, so remove the participant");
            [room removeParticipant:theParticipant];
        }
        [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
        [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
    }
}

-(void) internalUpdateParticipant:(NSString *)participantJid withPrivilege:(ParticipantPrivilege)privilege inRoom:(Room *)room {
    NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
    Participant *theParticipant = [self getParticipantWithJid:participantJid inRoom:room];
    
    if(!theParticipant)
        theParticipant = [self createOrUpdateParticipantFromJid:participantJid inRoom:room];
    
    theParticipant.privilege = privilege;
    
    if(privilege == ParticipantPrivilegeOwner) {
        // Update old owner
        Participant *oldOwnerParticipant = [self getParticipantWithRainbowID:room.creator.rainbowID inRoom:room];
        oldOwnerParticipant.privilege = ParticipantPrivilegeModerator;
        // Update new owner
        room.creator = theParticipant.contact;
        room.isMyRoom = [_contactsManagerService.myContact.jid isEqualToString:room.creator.jid];
        
        NSLog(@"[RoomsManager] Owner as changed from %@ to %@", oldOwnerParticipant, theParticipant);
    }
    [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
    NSLog(@"[RoomsManager] Participant %@ have now privilege %ld", theParticipant, (long)privilege);
    
    [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
}

-(void) removeRoom:(Room *) room {
    BOOL removed = NO;
    @synchronized (_roomsMutex) {
        if([_rooms containsObject:room]) {
            [_rooms removeObject:room];
            removed = YES;
        }
    }
    
    if(removed) {
        [_roomCache removeObjectForKeyAsync:room.rainbowID completion:nil];
        [_roomAvatarCache removeObjectForKeyAsync:room.rainbowID completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidRemoveRoom object:room];
    }
}

-(void) removeRoomAvatar:(Room *) room {
    BOOL removed = NO;
    NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
    
    @synchronized (_roomsMutex) {
        if([_rooms containsObject:room]) {
            room.photoData = nil;
            room.lastAvatarUpdateDate = nil;
            removed = YES;
        }
    }
    
    if(removed) {
        [_roomAvatarCache removeObjectForKeyAsync:room.rainbowID completion:nil];
        [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
        [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
    }
}

-(void) didUpdateRoom:(Room *) room withChangedKeys:(NSArray *) changedKeys {
    if(!room || !changedKeys || [changedKeys count] == 0)
        return;
    [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidUpdateRoom object:@{kRoomKey: room, kRoomChangedAttributesKey: changedKeys}];
}

#pragma mark - Rooms Server actions
-(Room *) createRoom:(NSString *) name withTopic:(NSString *) topic {
    return [self createRoom:name withTopic:topic error:nil];
}

-(Room *) createRoom:(NSString *) name withTopic:(NSString *) topic error:(NSError **) error {
    if (!name || [name length] == 0) {
        NSLog(@"[RoomsManager] Error: cannot create new room without a name.");
        *error = [NSError errorWithDomain:@"RoomsService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Cannot create room without name"}];
        return nil;
    }
    
    NSAssert(![NSThread isMainThread], @"Create room must not be invoked on mainThread");
    
    // Topic is optional, but app will crash if trying to create dict with nil value
    if (!topic) topic = @"";
    
    // Create the room through REST api.
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRooms];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRooms];
    
    NSDictionary *bodyContent = @{@"name":name, @"visibility":@"private", @"topic":topic, @"history":@"all", @"disableNotifications":@"true"};
    NSData *receivedData = nil;
    NSError *requestError = nil;
    NSURLResponse *response = nil;
    
    BOOL requestSucceed = [_downloadManager postSynchronousRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError shouldSaveRequest:NO];
    
    
    if(!requestSucceed || requestError) {
        NSLog(@"[RoomsManager] Error: Cannot create the room in REST : %@ %@", requestError, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        *error = [NSError errorWithDomain:@"RoomsService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Cannot create room, error in server response"}];
        return nil;
    }
    
    NSDictionary *jsonResponse = [receivedData objectFromJSONData];
    
    if(((NSHTTPURLResponse *)response).statusCode == 403){
        NSLog(@"Error while creating room code 403 %@", jsonResponse);
        NSString *errorDetailsCode = [jsonResponse objectForKey:@"errorDetailsCode"];
        *error = [NSError errorWithDomain:@"RoomsService" code:[errorDetailsCode integerValue] userInfo:@{NSLocalizedDescriptionKey:@"Cannot create room, error in server response"}];
        return nil;
    }
    
    if(!jsonResponse) {
        NSLog(@"[RoomsManager] Error: Cannot create the room in REST due to JSON parsing failure");
        return nil;
    }
    
    NSDictionary *data = [jsonResponse objectForKey:@"data"];
    Room *room = nil;
    if(data){
        // As we are listed in the users, with status = accepted
        // This will automatically join the room.
        room = [self createOrUpdateRoomFromJSON:data];
        // Join/leave room from an xmpp point of view
        // Do this out of the @syncronized
        if(room.myStatusInRoom == ParticipantStatusAccepted) {
            [_xmppService joinRoom:room];
        }
        if(room.myStatusInRoom == ParticipantStatusUnsubscribed) {
            [_xmppService leaveRoom:room];
        }
        NSLog(@"[RoomsManager] Created room %@", room);
    } else {
        NSLog(@"[RoomsManager] Create Room : no data %@", jsonResponse);
    }
    return room;
}

-(void) fetchRoomWithJid:(NSString*) roomJid withCompletionHandler:(RoomsServiceCompletionHandler) completionHandler {
    
    Room *theRoom = [self getRoomByJid:roomJid];
    
    if (theRoom) {
        NSLog(@"Invoke completionHandler %@", theRoom);
        if(completionHandler)
            completionHandler(theRoom, nil);
    } else {
        NSLog(@"[RoomsManager] fetchRoomWithJid : %@", roomJid);
        // Make a REST request to fetch room with jid
        NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesRooms];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/jids/%@/?format=full", url_base, roomJid]];
        NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRooms];
        
        
        NSLog(@"[RoomsManager] Get room");
        [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
            if(error) {
                NSLog(@"[RoomsManager] Error: Cannot get room in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
                if(completionHandler)
                    completionHandler(nil, error);
                return;
            }
            
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(!jsonResponse) {
                NSLog(@"[RoomsManager] Error: Cannot get room in REST due to JSON parsing failure");
                if(completionHandler)
                    completionHandler(nil, nil);
                return;
            }
            
            NSDictionary *data = [jsonResponse objectForKey:@"data"];
            NSLog(@"[RoomsManager] Get room response data ....");
            
            BOOL shoudUpdate = [self shouldLoadUpdateAvatar:data forRoom:[self getRoomByJid:[data objectForKey:@"jid"]]];
            
            Room *newRoom = [self createOrUpdateRoomFromJSON:data];
            
            if(shoudUpdate) {
                [self populateAvatarForRoom:theRoom forceReload:YES atSize:512];
            }
            
            @synchronized (_cacheMutex) {
                NSMutableArray *fullRoomsList = [NSMutableArray arrayWithArray:_rooms];
                [fullRoomsList addObject:newRoom];
                // Check if there is some rooms that must be removed (because loaded from cache)
                [_roomsLoadedFromCache enumerateObjectsUsingBlock:^(Room * aRoom, NSUInteger idx, BOOL * stop) {
                    if(![fullRoomsList containsObject:aRoom]){
                        [self removeRoom:aRoom];
                    }
                }];
            }
            
            // Join/leave room from an xmpp point of view
            // Do this out of the @syncronized
            if(newRoom.myStatusInRoom == ParticipantStatusAccepted) {
                [_xmppService joinRoom:newRoom];
            }
            if(theRoom.myStatusInRoom == ParticipantStatusUnsubscribed) {
                [_xmppService leaveRoom:newRoom];
            }
            // Request all missing vcard
            // Android search by rainbowID
            for (Participant *aParticipant in newRoom.participants) {
                if(!aParticipant.contact.vcardPopulated && aParticipant.contact.jid.length > 0){
                    [_contactsManagerService addJidToFetch:aParticipant.contact.jid];
                }
            }
            
            if(completionHandler)
                completionHandler(newRoom, nil);
        }];
    }
}


/**
 *  Fetch the Room details from the REST server ASYNC.
 *  TODO : Why there is two method to fetch the details !
 *  @param room The Room to fetch
 */
-(void) fetchRoomInformation:(Room *) room {

    if (!room.rainbowID) {
        NSLog(@"[RoomsManager] Error: unable to get Room details from server, room has no rainbowID..");
        return;
    }
    
    NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesRooms];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?format=full", url_base, room.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRooms];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[RoomsManager] Error: Cannot get room %@ details in REST : %@ %@", room.rainbowID, error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }

        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"[RoomsManager] Error: Cannot get room %@ details in REST due to JSON parsing failure", room.rainbowID);
            return;
        }
        
        NSDictionary *roomDict = [jsonResponse objectForKey:@"data"];
        NSLog(@"[RoomsManager] Fetch room details");
        Room *aRoom = [self createOrUpdateRoomFromJSON:roomDict];
        if(aRoom.conference.endpoint){
            // We can ask for conference details
            // TODO : is it needed ? why a snapshot ?
            [_conferenceManagerService fetchConferenceSnapshot:aRoom.conference completionBlock:^(NSError *error) {
                NSLog(@"[RoomsManager] Fetch conference details done %@", error);
            }];
        }
        // Join/leave room from an xmpp point of view
        // Do this out of the @syncronized
        if(aRoom.myStatusInRoom == ParticipantStatusAccepted) {
            [_xmppService joinRoom:aRoom];
        }
        if(aRoom.myStatusInRoom == ParticipantStatusUnsubscribed) {
            [_xmppService leaveRoom:aRoom];
        }
        
        // Request all missing vcard
        for (Participant *aParticipant in aRoom.participants) {
            if(!aParticipant.contact.vcardPopulated)
                [_contactsManagerService addJidToFetch:aParticipant.contact.jid];
        }
    }];
}

-(void) fetchRoomDetails:(Room *) room {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsDetail, [NSString stringWithFormat:@"%@?format=full",room.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsDetail];
    NSLog(@"[RoomsManager] Fetch room details");
    [_downloadManager getRequestWithURL: url requestHeadersParameter:headers  completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[RoomsManager] Error: Cannot get detail of room : %@", error);
            return;
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(jsonResponse){
                Room *theRoom = [self createOrUpdateRoomFromJSON:jsonResponse[@"data"]];
                NSLog(@"[RoomsManager] Fetch room details response %@ theRoom %@", jsonResponse, theRoom);
                Conference *theConference = nil;
                NSString *confId = nil;
                if(jsonResponse[@"data"][@"confEndpoints"] && [(NSArray *)jsonResponse[@"data"][@"confEndpoints"] count] > 0){
                    confId = jsonResponse[@"data"][@"confEndpoints"][0][@"confEndpointId"];
                }
                if(confId){
                    theConference = [_conferenceManagerService getConferenceByRainbowID:confId];
                    theRoom.conference = theConference;
                }
                if(theConference) {
                    if(!theRoom.isMyRoom){
                        [_conferenceManagerService fetchConferenceSnapshot:theConference completionBlock:^(NSError *error) {
                            NSLog(@"[RoomsManager] Fetch conference snapshot %@", error);
                        }];
                    }
                } else {
                    NSLog(@"[RoomsManager] Fetch room details failed to found the conference");
                }
            }
        }
    }];
}

-(void) inviteContact:(Contact *) contact inRoom:(Room *) room error:(NSError **) error{
    // Dict creation will fail if rainbowID is nil.
    if (!room.rainbowID || !contact.rainbowID) {
        NSLog(@"[RoomsManager] Error: Cannot add contact %@ to room %@, because one of the rainbowID is nil.", contact, room);
        *error = [NSError errorWithDomain:@"RoomsService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Cannot create room without room or participant"}];
        return;
    }
    
    if(!room.isAdmin){
        NSLog(@"[RoomsManager] Error: Only room administrator can invite people to join");
        *error = [NSError errorWithDomain:@"RoomsService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Only room administrator can invite people to join"}];
        return;
    }
    
    NSAssert(![NSThread isMainThread], @"Invite contact must not be invoked on mainThread");
    
    BOOL shouldRemove = NO;
    Participant *theParticipant = nil;
    for (Participant *aParticipant in room.participants) {
        if([aParticipant.contact isEqual:contact] && (aParticipant.status == ParticipantStatusRejected || aParticipant.status == ParticipantStatusUnsubscribed)){
            theParticipant = aParticipant;
            shouldRemove = YES;
            break;
        }
    }
    if(shouldRemove){
        // We must delete the already existing participant before inviting it again :/
        [self deleteParticipant:theParticipant fromRoom:room];
    }
    
    // Add the contact to the room in REST api.
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsUsers, room.rainbowID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsUsers];
    
    NSDictionary *bodyContent = @{@"userId":contact.rainbowID, @"privilege":@"user", @"reason":@"invite"};
    NSData *receivedData = nil;
    NSError *requestError = nil;
    NSURLResponse *response = nil;
    
    BOOL requestSucceed = [_downloadManager postSynchronousRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
    
    
    if(!requestSucceed || requestError) {
        NSString *errorString = [NSString stringWithFormat:@"[RoomsManager] Error: Cannot invite user into the room in REST : %@ %@", requestError, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]];
        NSLog(@"[RoomsManager] Error: Cannot invite user into the room in REST : %@ %@", requestError, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        *error = [NSError errorWithDomain:@"RoomsService" code:500 userInfo:@{NSLocalizedDescriptionKey:errorString}];
        return;
    }

    NSDictionary *jsonResponse = [receivedData objectFromJSONData];
    if(!jsonResponse) {
        NSLog(@"[RoomsManager] Error: Cannot read REST answer due to JSON parsing failure");
        *error = [NSError errorWithDomain:@"RoomsService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Cannot read REST answer due to JSON parsing failure"}];
        return;
    }
    NSLog(@"[RoomsManager] Did invite contact");
    if(jsonResponse[@"data"]){
        @synchronized(room){
            NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
            // Update or Add the participant
            Participant *participant = [self createOrUpdateParticipantFromJSONDict:jsonResponse[@"data"] inRoom:room];
            NSLog(@"[RoomsManager] Added participant %@", participant);
            [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
            [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
        }
    }
}

// A simple shortcut to remove a participant from server side and remove it from the room
-(void) cancelInvitationForContact:(Contact *) contact inRoom:(Room *) room {
    NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
    Participant *theParticipant = [room participantFromContact:contact];
    if(theParticipant){
        [self deleteParticipant:theParticipant fromRoom:room];
        [room removeParticipant:theParticipant];
        [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
    }
}

-(void) removeParticipant:(Participant *) participant fromRoom:(Room *) room {
    [self changeParticipant:participant withStatus:ParticipantStatusUnsubscribed inRoom:room];
}

-(void) deleteParticipant:(Participant *) participant fromRoom:(Room *) room {
    if (!room.rainbowID || !participant.contact.rainbowID) {
        NSLog(@"[RoomsManager] Error: Cannot remove contact %@ to room %@, because one of the rainbowID is nil.", participant.contact, room);
        [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidFailRemoveRoom object:room];

        return;
    }
    
    NSAssert(![NSThread isMainThread], @"Remove contact must not be invoked on mainThread");
    NSLog(@"[RoomsManager] Removing contact %@ from room %@", participant.contact, room);
    NSURL *baseUrl = [_apiUrlManagerService getURLForService:ApiServicesRoomsUsers, room.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",baseUrl.absoluteString, participant.contact.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsUsers];
    
    NSData *receivedData = nil;
    NSError *requestError = nil;
    NSURLResponse *response = nil;
    
    NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
    
    BOOL requestSucceed = [_downloadManager deleteSynchronousRequestWithURL:url requestHeadersParameter:headers returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
    
    if(!requestSucceed || requestError) {
        NSLog(@"[RoomsManager] Error: Cannot remove user in the room in REST : %@ %@", requestError, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
       [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidFailRemoveRoom object:room];

        return;
    }
    NSDictionary *jsonResponse = [receivedData objectFromJSONData];
    if(!jsonResponse) {
        NSLog(@"[RoomsManager] Error: Cannot read REST answer due to JSON parsing failure");
        [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidFailRemoveRoom object:room];

        return;
    }
    
    NSLog(@"[RoomsManager] Did remove contact");
    // If there is a data that means the delete works.
    if(jsonResponse[@"status"]){
        [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
        [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
    }
}

-(void) acceptInvitation:(Room *) room completionBlock:(void (^)(NSError *error,BOOL success)) completionHandler {
    if (room.myStatusInRoom != ParticipantStatusInvited) {
        NSLog(@"[RoomsManager] Error: we cannot accept room invitation, because we are not invited (invited state) in the room.");
         NSError *error = [NSError errorWithDomain:@"RoomsService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Cannot accept room!"}];
        if (completionHandler) {
            completionHandler(error,NO);
        }
        return ;
    }
    
    // Accept the invitation in REST
    // PUT on room/myuser with status = accepted.
    NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesRoomsUsers, room.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", url_base, _contactsManagerService.myContact.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsUsers];
    
    NSDictionary *bodyContent = @{@"status":@"accepted"};
    
    [_downloadManager putRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"[RoomsManager] Error: Cannot accept invitation to the room in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
          
            if (completionHandler) {
                completionHandler(error,NO);
            }
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse && error.code != NSURLErrorNotConnectedToInternet){
            NSLog(@"[RoomsManager] Error: Cannot accept invitation due to a JSON parsing error");
            if (completionHandler) {
                completionHandler(error,NO);
            }
            return;
        }
        // Mark as accepted in our data
        for (Participant *participant in room.participants) {
            if ([participant.contact isEqual:_contactsManagerService.myContact]) {
                participant.status = ParticipantStatusAccepted;
                room.myStatusInRoom = participant.status;
                // join the room now.
                [_xmppService joinRoom:room];
                break;
            }
        }
        [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidRoomInvitationStatusChanged object:room];
        [[NSNotificationCenter defaultCenter] postNotificationName:kContactsManagerServiceDidAcceptInvitation object:room];
    
        if (completionHandler) {
            completionHandler(nil,YES);
        }
    }];
}

-(void) declineInvitation:(Room *) room completionBlock:(void (^)(NSError *error,BOOL success)) completionHandler {
    if (room.myStatusInRoom != ParticipantStatusInvited) {
        NSLog(@"[RoomsManager] Error: we cannot decline room invitation, because we are not invited (invited state) in the room.");
        NSError * error = [NSError errorWithDomain:@"RoomsService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Cannot accept room!"}];
        if (completionHandler) {
            completionHandler(error,NO);
        }
        return;
    }
    
    // Decline the invitation in REST
    // PUT on room/myuser with status = unsubscribed.
    NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesRoomsUsers, room.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", url_base, _contactsManagerService.myContact.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsUsers];
    
    NSDictionary *bodyContent = @{@"status":@"rejected"};
    
    [_downloadManager putRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"[RoomsManager] Error: Cannot decline invitation to the room in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if (completionHandler) {
                completionHandler(error,NO);
            }
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse && error.code != NSURLErrorNotConnectedToInternet){
            NSLog(@"[RoomsManager] Error: Cannot decline invitation due to a JSON parsing error");
            if (completionHandler) {
                completionHandler(error,NO);
            }
            return;
        }
        
        // Mark as accepted in our data
        for (Participant *participant in room.participants) {
            if ([participant.contact isEqual:_contactsManagerService.myContact]) {
                participant.status = ParticipantStatusRejected;
                room.myStatusInRoom = participant.status;
                break;
            }
        }
        [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidRoomInvitationStatusChanged object:room];
        if (completionHandler) {
            completionHandler(nil,YES);
        }
    }];
}

-(void) leaveRoom:(Room *) room {
    if (room.myStatusInRoom != ParticipantStatusAccepted) {
        NSLog(@"[RoomsManager] Error: we cannot leave room, because we are not in accepted state in the room.");
        return;
    }
    
    // Mute the room
    // PUT on room/myuser with status = unsubscribed.
    NSURL *url_base = [_apiUrlManagerService getURLForService:ApiServicesRoomsUsers, room.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", url_base, _contactsManagerService.myContact.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsUsers];
    
    NSDictionary *bodyContent = @{@"status":@"unsubscribed"};
    
    NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
    
    [_downloadManager putRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[RoomsManager] Error: Cannot leave the room in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse){
            NSLog(@"[RoomsManager] Error: Cannot leave the room due to a JSON parsing error");
            return;
        }
        // Mark as accepted in our data
        for (Participant *participant in room.participants) {
            if ([participant.contact isEqual:_contactsManagerService.myContact]) {
                participant.status = ParticipantStatusUnsubscribed;
                room.myStatusInRoom = participant.status;
                // leave the room now.
                [_xmppService leaveRoom:room];
                [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
                [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
                break;
            }
        }
    }];
}

-(void) updateRoom:(Room *) room withTopic:(NSString *) topic {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsDetail, room.rainbowID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsDetail];
    NSString *bodyValue = topic;
    if(topic.length == 0)
        bodyValue = @"";
        
    [_downloadManager putRequestWithURL:url body:[@{@"topic":bodyValue} jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers  completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[RoomsManager] Error: Cannot update room topic in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        if(!error){
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(jsonResponse){
                [self createOrUpdateRoomFromJSON:jsonResponse[@"data"]];
            }
        }
    }];
}

-(void) updateRoom:(Room *) room withName:(NSString *) name {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsDetail, room.rainbowID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsDetail];
    NSString *bodyValue = name;
    if(name.length < 3){
        NSLog(@"[RoomsManager] Error: Cannot update room name, it must be at least 3 chars long");
        return;
    }
    
    [_downloadManager putRequestWithURL:url body:[@{@"name":bodyValue} jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers  completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[RoomsManager] Error: Cannot update room topic in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        if(!error){
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(jsonResponse){
                [self createOrUpdateRoomFromJSON:jsonResponse[@"data"]];
            }
        }
    }];
}

-(void) updateParticipant:(Participant *)participant withPrivilege:(ParticipantPrivilege)privilege inRoom:(Room *)room withCompletionBlock:(void (^)(NSError *error))completionBlock {
    if (!room.rainbowID || !participant.contact.rainbowID) {
        NSLog(@"[RoomsManager] Error: Cannot update participant %@ to room %@, because one of the rainbowID is nil.", participant, room);
        return;
    }
    if (participant.status != ParticipantStatusAccepted) {
        NSLog(@"[RoomsManager] Error: Cannot update participant %@ to room with id %@, because his status is not Accepted.", participant, room.rainbowID);
        return;
    }
    if (privilege != ParticipantPrivilegeGuest && privilege != ParticipantPrivilegeUser && privilege != ParticipantPrivilegeModerator) {
        NSLog(@"[RoomsManager] Error: Cannot update participant %@ to room with id %@, privilege must be one of these: guest, user or moderator.", participant, room.rainbowID);
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsChangeUserData, room.rainbowID, participant.contact.rainbowID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsChangeUserData];
    NSString *bodyValue = [Participant stringForPrivilege:privilege];
    
    if(participant.privilege == ParticipantPrivilegeOwner){
        NSLog(@"[RoomsManager] Error: Cannot update participant privilege because it is already the owner of the room");
        return;
    }
    [_downloadManager putRequestWithURL:url body:[@{@"privilege":bodyValue} jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers  completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[RoomsManager] Error: Cannot update room's participant privilege in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(jsonResponse){
                ParticipantPrivilege newPrivilege = [Participant stringToPrivilege:jsonResponse[@"data"][@"privilege"]];
                if(newPrivilege != ParticipantPrivilegeUnknown){
                    [self internalUpdateParticipant:participant.contact.jid withPrivilege:newPrivilege inRoom:room];
                }
            }
        }
        if(completionBlock){
            completionBlock(error);
        }
    }];
}

-(void) updateAvatar:(Room *) room withPhotoData: (NSData *) photoData withCompletionBlock:(void (^)(NSError *error))completionBlock {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsChangeAvatar, room.rainbowID];
    NSMutableDictionary *headers = (NSMutableDictionary*)[_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsChangeAvatar];

    [headers setObject:@"image/png" forKey:@"Content-Type"];
    
    NSData *receivedDataAvatar = nil;
    NSError *requestErrorAvatar = nil;
    NSURLResponse *responseAvatar = nil;
    
    BOOL requestSucceedAvatar = [_downloadManager postSynchronousRequestWithURL:url data:photoData requestHeadersParameter:headers returningResponseData:&receivedDataAvatar returningNSURLResponse:&responseAvatar returningError:&requestErrorAvatar];

    if(requestSucceedAvatar) {
        NSDictionary *response = [receivedDataAvatar objectFromJSONData];
        NSLog(@"[RoomsManager] Avatar update API result  : %@", response);
        
        if(requestErrorAvatar || [response objectForKey:@"errorDetails"]) {
            NSLog(@"[RoomsManager] Could not update room avatar: %ld", (long)requestErrorAvatar.code);
        }
        else {
            NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
            room.photoData = photoData;
            room.lastAvatarUpdateDate = [NSDate date];
            [_roomAvatarCache setObjectAsync:room.photoData forKey:room.rainbowID completion:nil];
            [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
        }
    }
    
    if(completionBlock) {
        if(requestErrorAvatar) {
            completionBlock(requestErrorAvatar);
        } else {
            completionBlock(nil);
        }
    }
}

-(void) populateAvatarForRoom:(Room *) room forceReload:(BOOL) force atSize: (int) size {
    
    // If we already have the photo, don't reload it.
    if (room.photoData && !force)
        return;
    
    if (!room.rainbowID || ![room.rainbowID length]) {
        NSLog(@"[RoomsManager] No rainbowID for room %@, cannot get avatar.", room.jid);
        return;
    }
    
    NSLog(@"[RoomsManager] Fetching Avatar for %@", [room description]);
    
    [self loadRoomPhotoData:room atSize:size completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(!error) {
            NSData *currentPhotoData;
            @synchronized (_roomsMutex) {
                currentPhotoData = room.photoData;
                room.photoData = receivedData;
            }
            
            NSLog(@"[RoomsManager] Saving avatar for room %@", room);
            [_roomAvatarCache setObjectAsync:room.photoData forKey:room.rainbowID completion:^(id<PINCaching>  _Nonnull cache, NSString * _Nonnull key, id  _Nullable object) {
                NSLog(@"[RoomsManager] Saved avatar object in cache for %@", key);
            }];
            
            NSLog(@"[RoomsManager] Update Avatar in cache for %@", [room description]);

            // Handle cases where old or new values can be nil.
            if ((room.photoData || currentPhotoData) && ![room.photoData isEqualToData:currentPhotoData]) {
                [self didUpdateRoom:room withChangedKeys:@[@"photoData"]];
            }
        }
    }];
}

-(void) loadRoomPhotoData:(Room *) room atSize:(int) size completionBlock:(RoomsServiceLoadAvatarInternalCompletionHandler) completionHandler {
    NSString *update = [[NSDate jsonStringFromDate:room.lastAvatarUpdateDate] MD5];
    
    NSMutableString *urlParam = [NSMutableString stringWithFormat:@"%u",size];
    if(update.length > 0)
        [urlParam appendFormat:@"&update=%@",update];
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsAvatar, room.rainbowID, urlParam];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsAvatar];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(completionHandler)
            completionHandler(receivedData, error, response);
    }];
}

-(void) getAvatarInCacheForRoom:(Room *) room {
    NSData *object = [_roomAvatarCache objectForKey:room.rainbowID];
    //        NSLog(@"[RoomsManager] We search a cached avatar for %@ : found it ? %@", key, NSStringFromBOOL(object!=nil));
    NSData *currentPhotoData;
    @synchronized (_roomsMutex) {
        currentPhotoData = room.photoData;
        room.photoData = object;
    }
    // Handle cases where old or new values can be nil.
    if ((room.photoData || currentPhotoData) && ![room.photoData isEqualToData:currentPhotoData]) {
        [self didUpdateRoom:room withChangedKeys:@[@"photoData"]];
    }
}

-(BOOL) shouldLoadUpdateAvatar:(NSDictionary *) roomDic forRoom:(Room*) room {
    BOOL shouldUpdate = YES;
    
    NSDate *newDate = nil;
    NSDate *currentDate = nil;
    
    // Avatar last update date
    if( [roomDic objectForKey:@"lastAvatarUpdateDate"] && [[roomDic objectForKey:@"lastAvatarUpdateDate"] isKindOfClass:[NSString class]] && [[roomDic objectForKey:@"lastAvatarUpdateDate"] length] > 0) {
        newDate = [NSDate dateFromJSONString:roomDic[@"lastAvatarUpdateDate"]];
    } else {
        return NO;
    }
    
    // Avatar current update date
    if( room ) {
        currentDate = [room.lastAvatarUpdateDate copy];
    }
    
    if( currentDate && room.photoData == nil ) {
        return YES;
    }
    
    if(newDate && currentDate && [newDate isEqualToDateAndTime:currentDate] ) {
        return NO;
    }
    
    if(!newDate)
        return NO;
    
    return shouldUpdate;
}

-(void) deleteRoom:(Room *) room {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsDetail, room.rainbowID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsDetail];
    
    NSLog(@"[RoomsManager] Deleting room %@", room);
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"[RoomsManager] Error: Cannot delete the room in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidFailRemoveRoom object:room];

        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(!jsonResponse && error.code != NSURLErrorNotConnectedToInternet) {
                NSLog(@"[RoomsManager] Error: Cannot delete the room in REST due to JSON parsing failure");
                [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidFailRemoveRoom object:room];

            } else {
                NSLog(@"[RoomsManager] Did delete room");
                [_xmppService leaveRoom:room];
                [self removeRoom:room];
            }
        }
    }];
}

-(void) deleteAvatar:(Room *) room  {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsChangeAvatar, room.rainbowID];
    NSMutableDictionary *headers = (NSMutableDictionary*)[_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsChangeAvatar];
    
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[RoomsManager] Error: Cannot delete the room avatar in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(!jsonResponse) {
                NSLog(@"[RoomsManager] Error: Cannot delete the room avatar in REST due to JSON parsing failure");
            } else {
                NSLog(@"[RoomsManager] Did delete room avatar");
                if(jsonResponse[@"status"]){
                    [self removeRoomAvatar: room];
                }
            }
        }
    }];
}

-(void) archiveRoom:(Room *) room {
    NSLog(@"[RoomsManager] Archiving room %@", room);
    if(room.isMyRoom){
        for (Participant *participant in room.participants) {
            [self changeParticipant:participant withStatus:ParticipantStatusUnsubscribed inRoom:room];
        }
    } else {
        [self changeParticipant:[room participantFromContact:_contactsManagerService.myContact] withStatus:ParticipantStatusUnsubscribed inRoom:room];
    }
}

-(void) changeParticipant:(Participant *) participant withStatus:(ParticipantStatus) status inRoom:(Room *) room {
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesRoomsUsers, room.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",serviceUrl, participant.contact.rainbowID]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsUsers];
    
    [_downloadManager  putRequestWithURL:url body:[@{@"status":[Participant stringForStatus:status]} jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error && error.code != NSURLErrorNotConnectedToInternet) {
            NSLog(@"[RoomsManager] Error: Cannot change participant status in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
        } else {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(!jsonResponse && error.code != NSURLErrorNotConnectedToInternet) {
                NSLog(@"[RoomsManager] Error: Cannot delete the room in REST due to JSON parsing failure");
            }
            
            if(jsonResponse[@"data"]) {
                ParticipantStatus new_status = [Participant stringToStatus:jsonResponse[@"data"][@"status"]];
                if ([participant.contact.jid length] && new_status != ParticipantStatusUnknown) {
                    [self updateParticipant:participant.contact.jid withStatus:new_status inRoom:room];
                    NSLog(@"[RoomsManager] Did change participant %@ status to %@", participant.contact.jid, jsonResponse[@"data"][@"status"]);
                } else {
                    NSLog(@"[RoomsManager] Error: Unable to update participant %@ status to %@", participant.contact.jid, jsonResponse[@"data"][@"status"]);
                }
            }
            else if(error.code == NSURLErrorNotConnectedToInternet){
                // offline mode , need to store this action in cache
                if ([participant.contact.jid length]) {
                    [self updateParticipant:participant.contact.jid withStatus:ParticipantStatusUnsubscribed inRoom:room];
                    NSLog(@"[RoomsManager] Did change participant %@ status to %@", participant.contact.jid, jsonResponse[@"data"][@"status"]);
                } else {
                    NSLog(@"[RoomsManager] Error: Unable to update participant %@ status to %@", participant.contact.jid, jsonResponse[@"data"][@"status"]);
                }
            }
        }
    }];
}

-(void) getParticipantRange:(NSRange)inRange inRoom:(Room *)room completionBlock:(RoomsServiceUsersCompletionHandler) completionHandler {
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesRoomsUsers, room.rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?offset=%lu&limit=%lu", serviceUrl, inRange.location, inRange.length]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsUsers];
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *urlResponse) {
        NSDictionary *response = [receivedData objectFromJSONData];
        if(!response) {
            NSLog(@"getParticipantRange:inRoom:completionBlock: JSON parsing failed");
            NSError *parsingError = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorDownloadDecodingFailedToComplete userInfo:nil];
            if(completionHandler)
                completionHandler(nil, parsingError);
            return;
        }
        NSArray *data = [response objectForKey:@"data"];
        NSMutableArray *participants = [[NSMutableArray alloc] init];
        if(data){
            for(NSDictionary *jsonDict in data){
                Participant *theParticipant = [Participant new];
                Contact *contact = [_contactsManagerService createOrUpdateRainbowContactFromJSON:jsonDict];
                theParticipant.contact = contact;
                theParticipant.privilege = [Participant stringToPrivilege:jsonDict[@"privilege"]];
                theParticipant.status = [Participant stringToStatus:jsonDict[@"status"]];
                if([[jsonDict objectForKey:@"additionDate"] length] > 0 && [[jsonDict objectForKey:@"additionDate"] isKindOfClass:[NSString class]])
                    theParticipant.addedDate = [NSDate dateFromJSONString:jsonDict[@"additionDate"]];
                [participants addObject:theParticipant];
            }
        }
        if(completionHandler)
            completionHandler(participants, error);
    }];
}

#pragma mark - XMPP service Rooms delegate
-(void) xmppService:(XMPPService *) service didCreateRoom:(Room *) room {
    [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidRoomInvitationStatusChanged object:room];
}

-(void) xmppService:(XMPPService *) service didJoinRoom:(Room *) room {
    [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidJoinRoom object:room];
}

-(void) xmppService:(XMPPService *) service didLeaveRoom:(Room *) room {
}

-(void) xmppService:(XMPPService *) service didDestroyRoom:(Room *) room {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidRoomInvitationStatusChanged object:room];
    });
}

-(void) xmppService:(XMPPService *) service didFailedToDestroyRoom:(Room *) room withError:(NSError *) error {
    
}
-(void) xmppService:(XMPPService *) service occupant:(Contact *) occupant didJoinRoom:(Room *) room {
}

-(void) xmppService:(XMPPService *) service occupant:(Contact *) occupant didLeaveRoom:(Room *) room {
//    [[NSNotificationCenter defaultCenter] postNotificationName:kRoomsServiceDidUpdateRoom object:room];
}

-(void) xmppService:(XMPPService *)service didChangeTopic:(NSString *) newTopic forRoom:(Room *)room {
    NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
    room.topic = newTopic;
    [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
    [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
}

-(void) xmppService:(XMPPService *)service didChangeName:(NSString *) newName forRoom:(Room *)room {
    NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
    room.displayName = newName;
    [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
    [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
    [self fetchRoomInformation:room];
}

-(void) xmppService:(XMPPService *)service didUpdateAvatarInRoom:(Room *)room withDate:(NSDate *) newUpdateDate {
    NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
    
    // Date has changed - check if we need to update or get the avatar
    if( newUpdateDate ) {
        room.lastAvatarUpdateDate = newUpdateDate;
        BOOL shouldUpdate = [self shouldLoadUpdateAvatar:currentRoomInfo forRoom:room];
        [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
        if(shouldUpdate){
            [self populateAvatarForRoom:room forceReload:YES atSize:512];
            [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
        }
    }
}

-(void) xmppService:(XMPPService *)service didDeleteAvatarInRoom:(Room *)room {
   [self removeRoomAvatar: room];
}

-(void) xmppService:(XMPPService *)service didChangedConferenceScheduleInRoom:(Room *)room startDate:(NSDate *)start endDate:(NSDate *)end {
    NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
    room.conference.start = start;
    room.conference.end = end;
    [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
    [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
}

-(void) xmppService:(XMPPService *)service didChangeCustomData:(NSDictionary *) newData forRoom:(Room *)room {
    NSDictionary *currentRoomInfo = [room dictionaryRepresentation];
    room.customData = newData;
    [_roomCache setObjectAsync:[room dictionaryRepresentation] forKey:room.rainbowID completion:nil];
    [self didUpdateRoom:room withChangedKeys:[currentRoomInfo changedKeysIn:[room dictionaryRepresentation]]];
    [self fetchRoomInformation:room];
}

#pragma mark - search
-(NSArray<Room *> *) searchRoomWithPattern:(NSString *) pattern {
    NSString *trimmedPattern = [pattern stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSMutableArray *subPredicates = [NSMutableArray array];
    NSArray *terms = [trimmedPattern componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    for (NSString *term in terms) {
        if(term.length == 0)
            continue;
        NSPredicate *p = [NSPredicate predicateWithFormat:@"displayName contains[cd] %@ and myStatusInRoom = %d", term, ParticipantStatusAccepted];
        [subPredicates addObject:p];
    }
    
    NSMutableSet<Room *> *result = [NSMutableSet set];
    @synchronized (_roomsMutex) {
        NSPredicate *filter = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
        [result addObjectsFromArray:[_rooms filteredArrayUsingPredicate:filter]];
    }
    
    return [result allObjects];
}

-(NSArray<Room *> *) searchMyRoomBeginWith:(NSString *) str {
    NSString *trimmedPattern = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"displayName beginswith[cd] %@ and isMyRoom == 1", trimmedPattern];
    NSMutableSet<Room *> *result = [NSMutableSet set];
    @synchronized (_roomsMutex) {
        [result addObjectsFromArray:[_rooms filteredArrayUsingPredicate:predicate]];
    }
    
    return [result allObjects];
}

-(NSArray<Room *> *) searchMyRoomMatchName:(NSString *) str {
    NSString *trimmedPattern = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"displayName UTI-EQUALS %@ and isMyRoom == 1", trimmedPattern];
    NSMutableSet<Room *> *result = [NSMutableSet set];
    @synchronized (_roomsMutex) {
        [result addObjectsFromArray:[_rooms filteredArrayUsingPredicate:predicate]];
    }
    
    return [result allObjects];
}

#pragma mark - Conference attachment
-(void) attachConferenceEndpoint:(ConfEndpoint *) confEndpoint inRoom:(Room *) room completionBlock:(RoomsServiceAttachConferenceCompletionHandler) completionHandler {
    if(!confEndpoint.confEndpointId){
        NSLog(@"[RoomsManager] No confEndpoint ID for conf endpoint %@", confEndpoint);
        if(completionHandler)
            completionHandler([NSError errorWithDomain:@"Conference" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Invalid conference ID"}], nil);
        return;
    }
    
    if(confEndpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
        NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsShareConference, room.rainbowID];
        NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsShareConference];
        NSDictionary *bodyContent = @{@"confId":confEndpoint.confEndpointId};
        
        [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers  completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
            if(error) {
                NSLog(@"[RoomsManager] Error: Cannot share conference in room : %@, received data %@", error, [receivedData objectFromJSONData]);
                NSDictionary *response = [receivedData objectFromJSONData];
                if(response){
                    NSInteger errorDetailsCode = [response[@"errorDetailsCode"] integerValue];
                    if(errorDetailsCode == 403620){
                        // The bridge is already attached somewhere :/
                        NSError *theError = [NSError errorWithDomain:ConferenceManagerErrorDomainAttach code:403620 userInfo:@{NSLocalizedDescriptionKey:@"Bridge already attached"}];
                        if(completionHandler)
                            completionHandler(theError,nil);
                        
                        return;
                    }
                }
                
                if(completionHandler)
                    completionHandler(error, nil);
                return;
            } else {
                NSDictionary *jsonResponse = [receivedData objectFromJSONData];
                if(jsonResponse[@"data"]){
                    Conference *conference = [_conferenceManagerService getConferenceByRainbowID:confEndpoint.confEndpointId];
                    if(!conference){
                        conference = [_conferenceManagerService createOrUpdateConferenceFromJson:jsonResponse[@"data"]];
                        conference.endpoint = confEndpoint;
                        conference.confId = confEndpoint.confEndpointId;
                    }
                    room.conference = conference;
                    room.conference.isMyConference = YES;
                    room.conference.owner = room.creator;
                    room.conference.endpoint.attachedRoomID = room.rainbowID;
                    room.conference.status = ConferenceStatusAttached;
                    NSAssert(conference.confId, @"Conference id is mandatory");
                    NSLog(@"[RoomsManager] Attach conference %@ jsonData %@", conference, jsonResponse);
                    [_roomCache setObject:[room dictionaryRepresentation] forKey:room.rainbowID];
                    [self didUpdateRoom:room withChangedKeys:@[@"conference"]];
                    if(completionHandler)
                        completionHandler(nil, conference);
                } else {
                    if(completionHandler)
                        completionHandler(nil, nil);
                }
            }
        }];
        
    } else if(confEndpoint.mediaType == ConferenceEndPointMediaTypeWebRTC){
        NSString *confId = confEndpoint.confEndpointId;
        Conference *conference = [_conferenceManagerService getConferenceByRainbowID: confId];
        if(!conference){
            [_conferenceManagerService createConferenceForRoom:room withConfEndpoint:confEndpoint completionHandler:^(NSError *error) {
                if(!error){
                    Conference *conference = [_conferenceManagerService getConferenceByRainbowID: confId];
                    if(conference){
                        room.conference = conference;
                        room.conference.isMyConference = YES;
                        room.conference.owner = room.creator;
                        room.conference.endpoint.attachedRoomID = room.rainbowID;
                        room.conference.status = ConferenceStatusAttached;
                        [_roomCache setObject:[room dictionaryRepresentation] forKey:room.rainbowID];
                        [self didUpdateRoom:room withChangedKeys:@[@"conference"]];
                        if(completionHandler){
                            completionHandler(nil, conference);
                        }
                    } else {
                        if(completionHandler){
                            NSError *theError = [NSError errorWithDomain:ConferenceManagerErrorDomainAttach code:500 userInfo:@{NSLocalizedDescriptionKey:@"The conference could still not be found"}];
                            completionHandler(theError, nil);
                        }
                    }
                } else {
                    if(completionHandler){
                        NSError *theError = [NSError errorWithDomain:ConferenceManagerErrorDomainAttach code:500 userInfo:@{NSLocalizedDescriptionKey:@"Error in createConferenceForRoom"}];
                        completionHandler(theError, nil);
                    }
                }
            }];
        } else {
            room.conference = conference;
            room.conference.isMyConference = YES;
            room.conference.owner = room.creator;
            room.conference.endpoint.attachedRoomID = room.rainbowID;
            room.conference.status = ConferenceStatusAttached;
            [_roomCache setObject:[room dictionaryRepresentation] forKey:room.rainbowID];
            [self didUpdateRoom:room withChangedKeys:@[@"conference"]];
            if(completionHandler)
                completionHandler(nil, conference);
        }
    }
}

-(void) detachConference:(Conference *) conference fromRoom:(Room *) room completionBlock:(RoomsServiceDetachConferenceCompletionHandler) completionHandler {
    if(conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
        NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsUnshareConference, room.rainbowID, conference.confId];
        NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsUnshareConference];
        
        [_downloadManager deleteRequestWithURL: url requestHeadersParameter:headers  completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
            if(error) {
                NSLog(@"[RoomsManager] Error: Cannot stop sharing conference in room : %@ errorDetails %@", error, [receivedData objectFromJSONData]);
                if(completionHandler)
                    completionHandler(error);
                return;
            }
            
            room.conference.endpoint.attachedRoomID = nil;
            room.conference.status = ConferenceStatusUnknown;
            [_roomCache setObject:[room dictionaryRepresentation] forKey:room.rainbowID];
            if(completionHandler)
                completionHandler(nil);
        }];
    } else if(conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC) {
        room.conference.endpoint.attachedRoomID = nil;
        room.conference.status = ConferenceStatusUnknown;
        [_roomCache setObject:[room dictionaryRepresentation] forKey:room.rainbowID];
        if(completionHandler)
            completionHandler(nil);
    } 
}

-(void) updateRoomInCache:(Room *)room {
    [_roomCache setObject:[room dictionaryRepresentation] forKey:room.rainbowID];
}

-(void) updateRoom:(Room *) room withCustomData:(NSDictionary*)datas completionBlock:(RoomsServiceManageCustomDataCompletionHandler) completionHandler {
    if(datas.count > 0) {
        [datas enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
            if(![object isKindOfClass:[NSString class]] && ![object isKindOfClass:[NSNumber class]]) {
                *stop = YES;
                NSAssert(NO, @"One of value is not a supported type. Only BOOL, NSNumber or NSString are authorized.");
            }
        }];
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesRoomsCustomData, room.rainbowID];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesRoomsCustomData];
    NSDictionary *bodyContent = (datas == nil) ? @{@"customData":@"{}"} : @{@"customData":datas};
    
    [_downloadManager putRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"[RoomsManager] Error: enable update custom data in room : %@", error);
            if(completionHandler)
                completionHandler(error);
            return;
        }
        
        if(completionHandler)
            completionHandler(nil);
    }];
}

@end
