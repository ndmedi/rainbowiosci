/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPWebSocketServer.h"
#import "XMLReader.h"

#import "ObjectAPIStore.h"
#import "RESTAPIServer.h"

@implementation XMPPStubDescriptor
+(XMPPStubDescriptor*)xmppStubPassing:(XMPPTestBlock)testBlock withResponse:(XMPPResponseBlock)responseBlock {
    XMPPStubDescriptor* stub = [XMPPStubDescriptor new];
    stub.testBlock = testBlock;
    stub.responseBlock = responseBlock;
    [[XMPPWebSocketServer stubDescriptors] addObject:stub];
    return stub;
}
@end


@interface XMPPWebSocketServer()
@property (nonatomic, strong) NSString *resource;
@property (nonatomic,       ) BOOL firstFraming;
@end


@implementation XMPPWebSocketServer

+(NSMutableArray<XMPPStubDescriptor *> *) stubDescriptors {
    static NSMutableArray<XMPPStubDescriptor *> *stubDescriptorsVar = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        stubDescriptorsVar = [NSMutableArray new];
    });
    return stubDescriptorsVar;
}

- (void)didOpen {
    [super didOpen];
    NSLog(@"XMPPWebSocketServer didOpen");
    self.firstFraming = YES;
}

-(void)didReceiveData:(NSData *) data {
    [self didReceiveMessage:[[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding]];
}

- (void)didReceiveMessage:(NSString *)msg {
    [super didReceiveMessage:msg];
    NSLog(@"XMPPWebSocketServer didReceive %@", msg);
    
    // Here, we'll respond depending on what the tests told us to do.
    NSError *parseError = nil;
    NSDictionary *message = [XMLReader dictionaryForXMLString:msg error:parseError];
    
    if (parseError) {
        NSLog(@"Error : Unable to parse XMPP message %@", msg);
        return;
    }
    
    // If we have no matching stubs, then use our standard XMPP process.
    
    if ([message[@"open"][@"xmlns"] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-framing"]) {
        if (self.firstFraming) {
            [self sendMessage:@"<open xmlns='urn:ietf:params:xml:ns:xmpp-framing' version='1.0' xml:lang='en' id='11223344' from='openrainbow.com'/>"];
            [self sendMessage:@"<stream:features xmlns:stream='http://etherx.jabber.org/streams'><c xmlns='http://jabber.org/protocol/caps' hash='sha-1' node='http://www.process-one.net/en/ejabberd/' ver='zCuUJRIymQcRFUx/lk/2yoOH4WQ='/><register xmlns='http://jabber.org/features/iq-register'/><mechanisms xmlns='urn:ietf:params:xml:ns:xmpp-sasl'><mechanism>DIGEST-MD5</mechanism><mechanism>SCRAM-SHA-1</mechanism><mechanism>PLAIN</mechanism></mechanisms></stream:features>"];
            self.firstFraming = NO;
        } else {
            [self sendMessage:@"<open xmlns='urn:ietf:params:xml:ns:xmpp-framing' version='1.0' xml:lang='en' id='55667788' from='openrainbow.com'/>"];
            [self sendMessage:@"<stream:features xmlns:stream='http://etherx.jabber.org/streams'><c xmlns='http://jabber.org/protocol/caps' hash='sha-1' node='http://www.process-one.net/en/ejabberd/' ver='zCuUJRIymQcRFUx/lk/2yoOH4WQ='/><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'/><session xmlns='urn:ietf:params:xml:ns:xmpp-session'/><ver xmlns='urn:xmpp:features:rosterver'/><sm xmlns='urn:xmpp:sm:2'/><sm xmlns='urn:xmpp:sm:3'/><csi xmlns='urn:xmpp:csi:0'/></stream:features>"];
        }
        return;
    }
    
    if ([message[@"auth"][@"xmlns"] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-sasl"]) {
        [self sendMessage:@"<challenge xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>cj1DQzAxNjJDMy0yN0Q2LTQ0RkYtOEY5Ni01NTRCMkY0NjIzMkRXY2hzZlJvZkk0UmNMNUN4d2NpNnZnPT0scz1tbmNuVXdtay9kUnkvVWs5VzNnaFhRPT0saT00MDk2</challenge>"];
        return;
    }
    
    if ([message[@"response"][@"xmlns"] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-sasl"]) {
        [self sendMessage:@"<success xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>dj11SVZlaVdMNlJaWHBQSnVDbkZFVlVnWUdvS2s9</success>"];
        return;
    }
    
    if ([message[@"iq"][@"type"] isEqualToString:@"set"] && [message[@"iq"][@"bind"][@"xmlns"] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-bind"]) {
        
        // Save the resource wanted by the client
        self.resource = message[@"iq"][@"bind"][@"resource"][@"text"];
        self.fulljid = [NSString stringWithFormat:@"%@/%@", self.barejid, self.resource];
        
        NSLog(@"FULL JID : %@", self.fulljid);
        
        [self sendMessage:[NSString stringWithFormat:@"<iq xmlns='jabber:client' id='%@' type='result'><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'><jid>%@</jid></bind></iq>", message[@"iq"][@"id"], self.fulljid]];
        return;
    }
    
    if ([message[@"iq"][@"type"] isEqualToString:@"set"] && [message[@"iq"][@"enable"][@"xmlns"] isEqualToString:@"urn:xmpp:carbons:2"]) {
        [self sendMessage:[NSString stringWithFormat:@"<iq type='result' xmlns='jabber:client' id='%@'/>", message[@"iq"][@"id"]]];
        return;
    }
    
    if ([message[@"iq"][@"type"] isEqualToString:@"set"] && [message[@"iq"][@"session"][@"xmlns"] isEqualToString:@"urn:ietf:params:xml:ns:xmpp-session"]) {
        [self sendMessage:[NSString stringWithFormat:@"<iq type='result' xmlns='jabber:client' id='%@'/>", message[@"iq"][@"id"]]];
        return;
    }
    
    if ([message[@"iq"][@"type"] isEqualToString:@"get"] &&
        [message[@"iq"][@"query"][@"xmlns"] isEqualToString:@"http://jabber.org/protocol/disco#items"] &&
        [message[@"iq"][@"to"] isEqualToString:self.barejid])
    {
        // Simulate only one resource connected (itself)
        [self sendMessage:[NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='%@' type='result'>\
                  <query xmlns='http://jabber.org/protocol/disco#items'>\
                  <item jid='%@' name='%@'/>\
                  </query>\
                  </iq>", self.barejid, self.fulljid, message[@"iq"][@"id"], self.fulljid, self.resource]];
        return;
    }
    
    
    // Get roster
    if ([message[@"iq"][@"type"] isEqualToString:@"get"] && [message[@"iq"][@"query"][@"xmlns"] isEqualToString:@"jabber:iq:roster"]) {
        // could be
        // both
        // to
        // ask='subscribe' subscription='none'
        
        
        NSString *rosterStr = [NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='%@' type='result'>\
                               <query xmlns='jabber:iq:roster'>", self.barejid, self.fulljid, message[@"iq"][@"id"]];
        
        for (ContactAPI *contact in [ObjectAPIStore sharedObject].contacts) {
            if (contact.inRoster) {
                if (contact.isPending) {
                    rosterStr = [NSString stringWithFormat:@"%@<item ask='subscribe' subscription='none' jid='%@'/>", rosterStr, contact.jid];
                } else {
                    rosterStr = [NSString stringWithFormat:@"%@<item subscription='both' jid='%@'/>", rosterStr, contact.jid];
                }
            }
        }
        rosterStr = [NSString stringWithFormat:@"%@</query></iq>", rosterStr];
        
        [self sendMessage:rosterStr];
        
        // And then send presence of already connected contacts
        for (ContactAPI *contact in [ObjectAPIStore sharedObject].contacts) {
            if (contact.inRoster && !contact.isPending && [contact.presence length]) {
                
                // <presence xmlns='jabber:client' from='contact.fulljid' to='me.fulljid'><priority>5</priority><show/><status>mode=auto</status></presence>
                
                NSString *presence = nil;
                if ([contact.presence isEqualToString:@"Online"]) {
                    presence = @"<show/><status>mode=auto</status>";
                }
                else if ([contact.presence isEqualToString:@"Away"]) {
                    presence = @"<show>xa</show><status>away</status>";
                }
                else if ([contact.presence isEqualToString:@"Do not disturb"]) {
                    presence = @"<show>dnd</show>";
                }
                else if ([contact.presence isEqualToString:@"Invisible"]) {
                    presence = @"<show>xa</show>";
                }
                if ([presence length]) {
                    [self sendMessage:[NSString stringWithFormat:@"<presence from='%@' to='%@'><priority>5</priority>%@</presence>", contact.jid, self.fulljid, presence]];
                }
            }
            
            if (contact.hasSubscribe) {
                // <presence xmlns='jabber:client' from='contact.fulljid' to='me.fulljid' type='subscribe'></presence>
                [self sendMessage:[NSString stringWithFormat:@"<presence from='%@' to='%@' type='subscribe'></presence>", contact.jid, self.fulljid]];
            }
        }
        return;
    }
    
    
    // Handle presence changes
    //if ([message[@"presence"][@"status"][@"text"] length]) {
    if (message[@"presence"]) {
        
        NSString *status = message[@"presence"][@"status"][@"text"];
        NSString *show = message[@"presence"][@"show"][@"text"];
        
        NSLog(@"GOT PRESENCE status %@ show %@", status, show);
        
        [self sendMessage:[NSString stringWithFormat:@"<presence from='%@' to='%@'><priority>5</priority><show>%@</show>%@</presence>", self.fulljid, self.fulljid, show, [status length] ? [NSString stringWithFormat:@"<status>%@</status>", status] : @""]];
        return;
    }
    
    /* <presence from="caa78385ab0a4053a77233e751fdc118@jerome-all-in-one-dev-1.opentouch.cloud/mobile_ios_FCF1871B-1E64-4659-88EF-7B3C48E58C23" to="caa78385ab0a4053a77233e751fdc118@jerome-all-in-one-dev-1.opentouch.cloud/mobile_ios_FCF1871B-1E64-4659-88EF-7B3C48E58C23"><priority xmlns="jabber:client">5</priority><show xmlns="jabber:client">xa</show><status xmlns="jabber:client">away</status></presence>*/
    
    
    
    // Handle MAM requests
    
    /* <iq xmlns="jabber:client" type="set" id="A68A309B-382A-4845-B68D-4F7C44EB9C32"><query xmlns="urn:xmpp:mam:1:bulk" queryid="A68A309B-382A-4845-B68D-4F7C44EB9C32"><x xmlns="jabber:x:data" type="submit"><field var="FORM_TYPE" type="hidden"><value>urn:xmpp:mam:1</value></field><field var="with"><value>fb43dbd763ef46a9af2f29d25a5f322c@jerome-all-in-one-dev-1.opentouch.cloud</value></field></x><set xmlns="http://jabber.org/protocol/rsm"><max>50</max><before/></set></query></iq>*/
    
    if ([message[@"iq"][@"query"][@"xmlns"] isEqualToString:@"urn:xmpp:mam:1:bulk"]) {
        
        // Extract the interessting parameters from request
        NSString *queryId = message[@"iq"][@"query"][@"queryid"];
        int rsmMax = [message[@"iq"][@"query"][@"set"][@"max"][@"text"] intValue];
        NSString *with;
        for (NSDictionary *field in message[@"iq"][@"query"][@"x"][@"field"]) {
            if ([field[@"var"] isEqualToString:@"with"]) {
                with = field[@"value"][@"text"];
                break;
            }
        }
        
        // Sort all the messages by dates.
        [[ObjectAPIStore sharedObject].messages sortUsingComparator:^NSComparisonResult(MessageAPI *obj1, MessageAPI *obj2) {
            return [obj1.date compare:obj2.date];
        }];
        
        // MAMID = MSGID here.
        NSString *first = nil;
        NSString *last = nil;
        int count = 0;
        NSMutableArray<NSString *> *bulkMsgsToSend = [NSMutableArray new];
        for (MessageAPI *message in [ObjectAPIStore sharedObject].messages) {
            // message with the other dude.
            if ([message.to_jid isEqualToString:with] || [message.from_jid isEqualToString:with]) {
                
                if (!first) {
                    first = message.ID;
                }
                last = message.ID;
                count++;
                
                
                NSString *ack = [NSString stringWithFormat:@"<ack received='%@' %@ read='%@' %@/>", message.received?@"true":@"false", [message.receivedDate length]?[NSString stringWithFormat:@"recv_timestamp='%@'", message.receivedDate]:@"", message.read?@"true":@"false", [message.readDate length]?[NSString stringWithFormat:@"read_timestamp='%@'", message.readDate]:@""];
                
                NSString *msg = [NSString stringWithFormat:@"<message xmlns='jabber:client' from='%@' to='%@' type='chat' id='%@'><body>%@</body><request xmlns='urn:xmpp:receipts'/>%@</message>", message.from_jid, message.to_jid, message.ID, message.body, ack];
                
                NSString *MAM = [NSString stringWithFormat:@"<result xmlns='urn:xmpp:mam:1' id='%@' queryid='%@'><forwarded xmlns='urn:xmpp:forward:0'>%@<delay xmlns='urn:xmpp:delay' from='domain.com' stamp='%@'/></forwarded></result>", message.ID, queryId, msg, message.date];
                [bulkMsgsToSend addObject:MAM];
                
                rsmMax--;
                if (rsmMax < 0) {
                    break;
                }
            }
        }
        
        // Send all messages in bulk
        NSString *msgBulkHeader = [NSString stringWithFormat:@"<message xmlns='jabber:client' from='%@' to='%@'><results xmlns='urn:xmpp:mam:1:bulk'>",self.barejid, self.fulljid];
        
        NSString *msgBulkEnd = @"</results></message>";
        
        NSMutableString *msgBulk = [NSMutableString string];
        [msgBulk appendString:msgBulkHeader];
        for (NSString *msg in bulkMsgsToSend) {
            [msgBulk appendString:msg];
        }
        [msgBulk appendString:msgBulkEnd];
        
        [self sendMessage:msgBulk];
        
        BOOL complete = (rsmMax > 0);
        NSString *firstStr = first ? [NSString stringWithFormat:@"<first>%@</first>", first] : @""; // @"<first>1482157483506024</first>";
        NSString *lastStr = last ? [NSString stringWithFormat:@"<last>%@</last>", last] : @""; // @"<last>1482246386597950</last>";
        
        [self sendMessage:[NSString stringWithFormat:@"<iq from='%@' to='%@' id='%@' type='result'><fin xmlns='urn:xmpp:mam:1' queryid='%@' complete='%@'><set xmlns='http://jabber.org/protocol/rsm'>%@%@<count>%u</count></set></fin></iq>", self.fulljid, self.fulljid, queryId, queryId, complete?@"true":@"false", firstStr, lastStr, count]];
        
        
        // from me
        // <message from="ME" to="ME/rc"><result xmlns="urn:xmpp:mam:1" id="1482157502494398" queryid="AD4E615D-9B12-4A33-977A-6CD7A7B5CB16"><forwarded xmlns="urn:xmpp:forward:0"><message xmlns="jabber:client" from="ME" to="OTHER" type="chat" id="626A2AC7-8D21-4B86-8CDD-3385E524B887"><body>Vacuité du monde la</body><request xmlns="urn:xmpp:receipts"/><ack received="true" recv_timestamp="2016-12-19T14:25:02.648214Z" read="true" read_timestamp="2016-12-19T14:25:02.703911Z"/></message><delay xmlns="urn:xmpp:delay" from="jerome-all-in-one-dev-1.opentouch.cloud" stamp="2016-12-19T14:25:02.494Z"/></forwarded></result></message>
        
        // From other
        // <message from="ME" to="ME/rc"><result xmlns="urn:xmpp:mam:1" id="1482157483506024" queryid="AD4E615D-9B12-4A33-977A-6CD7A7B5CB16"><forwarded xmlns="urn:xmpp:forward:0"><message xmlns="jabber:client" from="OTHER/rc" to="ME" type="chat" id="web_3723aa97-f4b6-4782-8fcc-0f48b0ae2e1824" lang="en"><body lang="en">OUA ?</body><request xmlns="urn:xmpp:receipts"/><active xmlns="http://jabber.org/protocol/chatstates"/><ack received="true" recv_timestamp="2016-12-19T14:24:43.628619Z" read="true" read_timestamp="2016-12-19T14:24:43.651618Z"/></message><delay xmlns="urn:xmpp:delay" from="jerome-all-in-one-dev-1.opentouch.cloud" stamp="2016-12-19T14:24:43.506Z"/></forwarded></result></message>
        
        // End
        // <iq xmlns="jabber:client" from="ME" to="ME/rc" id="AD4E615D-9B12-4A33-977A-6CD7A7B5CB16" type="result"><fin xmlns="urn:xmpp:mam:1" queryid="AD4E615D-9B12-4A33-977A-6CD7A7B5CB16" complete="true"><set xmlns="http://jabber.org/protocol/rsm"><first>1482157483506024</first><last>1482246386597950</last><count>17</count></set></fin></iq>
    }
    
    // Last activity of a contact
    if ([message[@"iq"][@"type"] isEqualToString:@"get"] &&
        [message[@"iq"][@"query"][@"xmlns"] isEqualToString:@"jabber:iq:last"]){
        NSString *msg = [NSString stringWithFormat:@"<iq xmlns='jabber:client' from='%@' to='%@' id='%@' type='result'><query xmlns='jabber:iq:last' seconds='500'/></iq>", message[@"iq"][@"to"], self.fulljid, message[@"iq"][@"id"]];
        [self sendMessage:msg];
    }
    
    // Check all the registred stubs
    for (XMPPStubDescriptor* descriptor in [XMPPWebSocketServer stubDescriptors]) {
        if (descriptor.testBlock(message)) {
            NSArray<NSString*> *responses = descriptor.responseBlock(message);
            for (NSString *response in responses) {
                [self sendMessage:response];
            }
            return;
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [[XMPPWebSocketServer stubDescriptors] removeObject:descriptor];
        });
    }
    
}

- (void)didClose {
    [super didClose];
    NSLog(@"XMPPWebSocketServer didClose");
    [RESTAPIServer clean];
}

@end
