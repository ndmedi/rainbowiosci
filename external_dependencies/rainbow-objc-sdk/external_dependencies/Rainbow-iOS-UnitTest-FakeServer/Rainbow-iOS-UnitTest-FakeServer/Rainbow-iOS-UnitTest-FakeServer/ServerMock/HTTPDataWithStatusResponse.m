/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "HTTPDataWithStatusResponse.h"

@implementation HTTPDataWithStatusResponse

- (id)initWithData:(NSData *)dataParam withStatus:(int)statusParam {
    if ((self = [super initWithData:dataParam])) {
        status = statusParam;
    }
    return self;
}

- (NSInteger) status {
    return status;
}

-(instancetype) initWithFileName:(NSString *) fileName atPath:(NSString *) path status:(int) statusParam {
    NSBundle* bundle = [NSBundle bundleWithPath:path];
    NSString *fullPath = [bundle pathForResource:[fileName stringByDeletingPathExtension]
                                          ofType:[fileName pathExtension]];
    NSData *documentData = [NSData dataWithContentsOfFile:fullPath];
    self = [super initWithData:documentData];
    if(self) {
        status = statusParam;
    }
    return self;
}

NSString* __nullable OHPathForFile(NSString* fileName, Class inBundleForClass)
{
    NSBundle* bundle = [NSBundle bundleForClass:inBundleForClass];
    return OHPathForFileInBundle(fileName, bundle);
}

NSString* __nullable OHPathForFileInBundle(NSString* fileName, NSBundle* bundle)
{
    return [bundle pathForResource:[fileName stringByDeletingPathExtension]
                            ofType:[fileName pathExtension]];
}

@end
