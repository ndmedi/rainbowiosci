/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RESTAPIServer.h"
#import "XMPPWebSocketServer.h"
#import "NSDictionary+JSON.h"

#import "ObjectAPIStore.h"

#import "HTTPServer.h"
#import "HTTPConnection.h"
#import "HTTPMessage.h"
#import "HTTPDataResponse.h"
#import "HTTPDataWithStatusResponse.h"


static XMPPWebSocketServer* websocket = nil;
static NSString *bundlePath = nil;

@implementation HTTPStubDescriptor
+(HTTPStubDescriptor*)httpStubPassing:(HTTPTestBlock)testBlock withResponse:(HTTPResponseBlock)responseBlock {
    HTTPStubDescriptor* stub = [HTTPStubDescriptor new];
    stub.testBlock = testBlock;
    stub.responseBlock = responseBlock;
    [[RESTAPIServer stubDescriptors] addObject:stub];
    return stub;
}
@end



@interface NSString (URLMatcher)
-(BOOL) matchURL:(NSString *) url;
@end

@implementation NSString (URLMatcher)

-(BOOL) matchURL:(NSString *)url {
    // url has form of "/api/rainbow/enduser/v1.0/users/{}/conversations"
    // We check that we match this url.
    // {} allow anything except '/'
    
    url = [url stringByReplacingOccurrencesOfString:@"{}" withString:@"[^/]+"];
    url = [NSString stringWithFormat:@"^%@$", url];
    
    NSError* regexError = nil;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:url
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&regexError];
    
    if (regexError) {
        NSLog(@"Regex creation failed with error: %@", [regexError description]);
        return NO;
    }
    
    NSArray* matches = [regex matchesInString:self
                                      options:NSMatchingWithoutAnchoringBounds
                                        range:NSMakeRange(0, self.length)];
    
    return matches.count > 0;
}

@end

@interface RESTAPIServer ()
@property (nonatomic, strong) NSString *bundlePath;
@end

@implementation RESTAPIServer {
    NSMutableData *body;
}

+(NSMutableArray<HTTPStubDescriptor *> *) stubDescriptors {
    static NSMutableArray<HTTPStubDescriptor *> *stubDescriptorsVar = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        stubDescriptorsVar = [NSMutableArray new];
    });
    return stubDescriptorsVar;
}


#pragma mark - My infos

+(NSString *) myRainbowID {
    return @"myRainbowID";
}

+(NSString *) myJID {
    return @"myjid@domain.com";
}

+(NSString *) myLoginEmail {
    return @"mylogin@domain.com";
}

+(NSString *) myLoginPassword {
    return @"mypassword";
}

+(NSString *) myCompanyID {
    return @"myCompanyID";
}

+(NSString *) myCompanyName {
    return @"test company";
}


#pragma mark - Methods override from HTTPConnection

- (BOOL)supportsMethod:(NSString *)method atPath:(NSString *)path {
    // We support any method (GET, POST, DELETE, ...)
    return YES;
}

- (BOOL)expectsRequestBodyFromMethod:(NSString *)method atPath:(NSString *)path {
    // Inform HTTP server that we expect a body to accompany a POST request
    return [super expectsRequestBodyFromMethod:method atPath:path];
}

// Store the request bodies in RAM.
- (void)prepareForBodyWithSize:(UInt64)contentLength {
    body = nil;
    body = [NSMutableData new];
}

- (void)processBodyData:(NSData *)postDataChunk {
    [body appendData:postDataChunk];
}


- (WebSocket *)webSocketForURI:(NSString *)path {
    // Here the magic happens, we also handle websocket on specific URI ! <3
    if ([path isEqualToString:@"/websocket"]) {
        if (websocket) {
            NSLog(@"2ND WEBSOCKET ? ERROR INCOMING -> YOU SHALL NOT PASS !");
            return nil;
        }
        websocket = [[XMPPWebSocketServer alloc] initWithRequest:request socket:asyncSocket];
        // Set the barejid of the person which will login.
        websocket.barejid = [self.class myJID];
        return websocket;
    }
    return [super webSocketForURI:path];
}

+ (void)closeWebsocket {
    if (websocket) {
        [websocket sendMessage:@"<close xmlns='urn:ietf:params:xml:ns:xmpp-framing'/>"];
    }
}

+ (void)clean {
    websocket = nil;
}

+ (void)flushData {
    [[ObjectAPIStore sharedObject] removeAllObjects];
}

- (NSString *)valueForKey:(NSString *)key fromURL:(NSURL *)url {
    NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:url resolvingAgainstBaseURL:NO];
    NSArray *queryItems = urlComponents.queryItems;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", key];
    NSURLQueryItem *queryItem = [[queryItems
                                  filteredArrayUsingPredicate:predicate]
                                 firstObject];
    return queryItem.value;
}

// This is the heart of the webserver, which handle all the requests.
- (NSObject<HTTPResponse> *)httpResponseForMethod:(NSString *)method URI:(NSString *)path {
    NSLog(@"RESTAPIServer called with %@ %@", method, path);
    
    // First, try to find a matching Stub registred.
    // The stubs are reserved for exception cases we want to test
    // It bypass the standard REST API calls.
    for (HTTPStubDescriptor* descriptor in [RESTAPIServer stubDescriptors]) {
        if (descriptor.testBlock(request)) {
            NSData *data = descriptor.responseBlock(request);
            return [[HTTPDataResponse alloc] initWithData:data];
        }
    }
    
    // If we have no matching stubs,
    // handle all standard REST API calls, with our ObjectAPI data
    NSArray<NSString *> *path_elements = [request.url.relativePath componentsSeparatedByString:@"/"];
    
    
#pragma mark - GET /api/rainbow/authentication/v1.0/login
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:@"/api/rainbow/authentication/v1.0/login"]) {
        
        // header authorization is 'Basic base64(login:password)'
        NSString *authorization = [request headerField:@"authorization"];
        
        if ([authorization hasPrefix:@"Basic "])
            authorization = [authorization substringFromIndex:[@"Basic " length]];
        
        NSData *authorizationData = [[NSData alloc] initWithBase64EncodedString:authorization options:0];
        NSString *authorizationDecoded = [[NSString alloc] initWithData:authorizationData encoding:NSUTF8StringEncoding];
        
        NSArray<NSString *> *LoginPwd = [authorizationDecoded componentsSeparatedByString:@":"];
        
        if ([LoginPwd count] != 2) {
            
            NSDictionary *data = @{
                                   @"errorCode": @(401),
                                   @"errorMsg": @"Unauthorized",
                                   @"errorDetails": @"Request has something wrong with basic authentication header",
                                   @"errorDetailsCode": @(401100)
                                   };
            return [[HTTPDataWithStatusResponse alloc] initWithData:[data jsonRepresentation] withStatus:401];
        }
        
        if (![LoginPwd[0] isEqualToString:[self.class myLoginEmail]] || ![LoginPwd[1] isEqualToString:[self.class myLoginPassword]]) {
            
            NSDictionary *data = @{
                                   @"errorCode": @(401),
                                   @"errorMsg": @"Unauthorized",
                                   @"errorDetails": [NSString stringWithFormat:@"Unknown login or wrong password for login %@", [self.class myLoginEmail]],
                                   @"errorDetailsCode": @(401500)
                                   };
            return [[HTTPDataWithStatusResponse alloc] initWithData:[data jsonRepresentation] withStatus:401];
        }
        
        NSDictionary *data = @{
                               @"token": @"test_token",
                               @"loggedInUser": @{
                                       @"companyId": [self.class myCompanyID],
                                       @"companyName": [self.class myCompanyName],
                                       @"country": @"FRA",
                                       @"displayName": @"iOS Tester",
                                       @"isActivated": @(YES),
                                       @"jid_im": [self.class myJID],
                                       @"jid_password": @"fake_passwd",
                                       @"jid_tel": @"tel_fake_iostester_jid@openrainbow.com",
                                       @"latin_displayName": @"iOS Tester",
                                       @"loginEmail": [self.class myLoginEmail],
                                       @"conversations": @[],
                                       @"sources": @[],
                                       @"loggedSince": @"2016-04-14T12:06:02.083Z",
                                       @"lastLoginDate": @"2016-04-14T12:06:02.083Z",
                                       @"firstLoginDate": @"2016-03-18T15:22:50.510Z",
                                       @"sponsoring": @{
                                               @"invitationsAcceptedByList": @[]
                                               },
                                       @"createdBySelfRegister": @(NO),
                                       @"initializationDate": @"2016-03-14T13:27:16.137Z",
                                       @"isInitialized": @(YES),
                                       @"activationDate": @"2016-03-14T13:27:16.137Z",
                                       @"creationDate": @"2016-01-13T08:39:37.779Z",
                                       @"accountType": @"free",
                                       @"roles": @[@"user"],
                                       @"isActive": @(YES),
                                       @"lastName": @"Tester",
                                       @"firstName": @"iOS",
                                       @"id": [self.class myRainbowID]
                                       }
                               };
        
        return [[HTTPDataResponse alloc] initWithData:[data jsonRepresentation]];
    }
    
    
    //////////////////////
    // CONVERSATIONS
    
#pragma mark - GET /api/rainbow/enduser/v1.0/users/{userId}/conversations
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/conversations", [self.class myRainbowID]]]) {
        NSDictionary *response = @{@"data": [ObjectAPIStore sharedObject].conversationsDictionary};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
#pragma mark - POST /api/rainbow/enduser/v1.0/users/{userId}/conversations
    if ([request.method isEqualToString:@"POST"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/conversations", [self.class myRainbowID]]]) {
        
        NSDictionary *conversationDict = [body objectFromJSONData];
        NSString *peerID = [conversationDict objectForKey:@"peerId"];
        NSString *type = [conversationDict objectForKey:@"type"];
        // handle mute
        
        // Find the contact
        ContactAPI *peer = nil;
        for (ContactAPI *contact in [ObjectAPIStore sharedObject].contacts) {
            if ([contact.ID isEqualToString:peerID]) {
                peer = contact;
            }
        }
        
        // Create conv
        ConversationAPI *conversation = [ConversationAPI new];
        conversation.peerID = peer.ID;
        conversation.type = type;
        conversation.jid = peer.jid;
        conversation.lastMessage = @"";
        conversation.lastMessageDate = @"";
        conversation.unreceived = @(0);
        conversation.unread = @(0);
        conversation.mute = NO;
        [RESTAPIServer addConversation:conversation];
        
        NSDictionary *response = @{@"data": conversation.representation};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
#pragma mark - DELETE /api/rainbow/enduser/v1.0/users/{userId}/conversations/{conversationId}
    if ([request.method isEqualToString:@"DELETE"] && [request.url.relativePath matchURL:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/conversations/{}", [self.class myRainbowID]]]) {
        
        NSString *conversationID = [path_elements objectAtIndex:[path_elements count]-1];
        
        for (ConversationAPI *conversation in [ObjectAPIStore sharedObject].conversations) {
            if ([conversation.ID isEqualToString:conversationID]) {
                [[ObjectAPIStore sharedObject].conversations removeObject:conversation];
                break;
            }
        }
        
        NSDictionary *response = @{@"status": @"Conversation successfully deleted", @"data": @[]};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }

    /////////////////////
    // INVITATION
    
#pragma mark - POST /api/rainbow/enduser/v1.0/users/{userId}/invitations
    if([request.method isEqualToString:@"POST"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/invitations", [self.class myRainbowID]]]) {
        // {"status":"Email successfully sent","data":{"invitedUserEmail":"user-perso@address.com","invitingUserId":"57599db93c919278a8a20b83","invitingUserEmail":"j5@jhe.test.openrainbow.net","hiddenForInvitingUser":false,"hiddenForInvitedUser":false,"type":"registration","status":"pending","requestedNotificationLanguage":"fr","cancelationDate":null,"declinationDate":null,"acceptationDate":null,"lastNotificationDate":"2017-05-16T09:33:46.797Z","invitingDate":"2017-05-16T09:33:46.797Z","id":"591ac77a6a941e1f8ec3c69d"}}
        
        NSDictionary *body_dict = [body objectFromJSONData];
        
        InvitationAPI *invitation = [InvitationAPI new];
        invitation.ID = @"Sent_invitation_123";
        invitation.received = YES;
        invitation.invitingUserId = [self.class myRainbowID];
        invitation.invitedUserEmail = [body_dict valueForKey:@"email"];
        invitation.status = @"pending";
        invitation.invitingDate = @"2017-05-16T12:00:00.881Z";
        NSDictionary *response = @{@"status":@"Email sucessfully sent", @"data": [invitation representation]};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
        
    }
    
#pragma mark - GET /api/rainbow/enduser/v1.0/users/{userId}/invitations/received
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/invitations/received", [self.class myRainbowID]]]) {
        NSDictionary *response = @{@"data": [ObjectAPIStore sharedObject].receivedInvitationsDictionary};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
#pragma mark - GET /api/rainbow/enduser/v1.0/users/{userId}/invitations/sent
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/invitations/sent", [self.class myRainbowID]]]) {
        NSDictionary *response = @{@"data": [ObjectAPIStore sharedObject].sentInvitationsDictionary};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
#pragma mark - GET /api/rainbow/enduser/v1.0/users/{userId}/invitations/{invitationId}
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath matchURL:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/invitations/{}", [self.class myRainbowID]]]) {
        NSString *invitationID = [path_elements objectAtIndex:[path_elements count]-1];
        for (InvitationAPI *invitation in [ObjectAPIStore sharedObject].invitations) {
            if ([invitation.ID isEqualToString:invitationID]) {
                return [[HTTPDataResponse alloc] initWithData:[@{@"data": invitation.representation} jsonRepresentation]];
            }
        }
        return nil;
    }
    
#pragma mark - POST /api/rainbow/enduser/v1.0/users/{userId}/invitations/{invitationId}/accept
    if ([request.method isEqualToString:@"POST"] && [request.url.relativePath matchURL:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/invitations/{}/accept", [self.class myRainbowID]]]) {
        NSString *invitationID = [path_elements objectAtIndex:[path_elements count]-2];
        
        for (InvitationAPI *invitation in [ObjectAPIStore sharedObject].invitations) {
            if ([invitation.ID isEqualToString:invitationID]) {
                // Update the invitation
                invitation.status = @"accepted";
                
                // Search the invitingUserId in our list to get its JID.
                NSString *invitingJID = @"";
                for (ContactAPI *contact in [ObjectAPIStore sharedObject].contacts) {
                    if ([contact.ID isEqualToString:invitation.invitingUserId]) {
                        invitingJID = contact.jid;
                    }
                }
                
                // Send roster-push with new friend.
                NSString *iq = [NSString stringWithFormat:@"<iq to='%@' type='set' id='%u'><query xmlns='jabber:iq:roster'><item jid='%@' subscription='both'></item></query></iq>", websocket.fulljid, arc4random(), invitingJID];
                [websocket sendMessage:iq];
                
                return [[HTTPDataResponse alloc] initWithData:[@{@"data": invitation.representation} jsonRepresentation]];
            }
        }
        return nil;
    }
    
#pragma mark - POST /api/rainbow/enduser/v1.0/users/{userId}/invitations/{invitationId}/decline
    if ([request.method isEqualToString:@"POST"] && [request.url.relativePath matchURL:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/invitations/{}/decline", [self.class myRainbowID]]]) {
        NSString *invitationID = [path_elements objectAtIndex:[path_elements count]-2];
        
        for (InvitationAPI *invitation in [ObjectAPIStore sharedObject].invitations) {
            if ([invitation.ID isEqualToString:invitationID]) {
                // Update the invitation
                invitation.status = @"declined";
                return [[HTTPDataResponse alloc] initWithData:[@{@"data": invitation.representation} jsonRepresentation]];
            }
        }
        return nil;
    }
    
#pragma mark - POST /api/rainbow/enduser/v1.0/users/{userId}/invitations/{invitationId}/cancel
    if ([request.method isEqualToString:@"POST"] && [request.url.relativePath matchURL:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/invitations/{}/cancel", [self.class myRainbowID]]]) {
        NSString *invitationID = [path_elements objectAtIndex:[path_elements count]-2];
        
        for (InvitationAPI *invitation in [ObjectAPIStore sharedObject].invitations) {
            if ([invitation.ID isEqualToString:invitationID]) {
                // Update the invitation
                invitation.status = @"canceled";
                return [[HTTPDataResponse alloc] initWithData:[@{@"data": invitation.representation} jsonRepresentation]];
            }
        }
        return nil;
    }
    
    
    /////////////////////
    // ROOMS
    
#pragma mark - GET /api/rainbow/enduser/v1.0/rooms
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:@"/api/rainbow/enduser/v1.0/rooms"]) {
        NSDictionary *response = @{@"data": [ObjectAPIStore sharedObject].roomsDictionary};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
#pragma mark - PUT /api/rainbow/enduser/v1.0/rooms/{roomId}
    if ([request.method isEqualToString:@"PUT"] && [request.url.relativePath matchURL:@"/api/rainbow/enduser/v1.0/rooms/{}"]) {
        NSString *roomID = [path_elements objectAtIndex:[path_elements count]-1];
        
        NSDictionary *body_dict = [body objectFromJSONData];
        // example of body : { topic = "New topic" }
        
        for (RoomAPI *room in [ObjectAPIStore sharedObject].rooms) {
            if ([room.ID isEqualToString:roomID]) {
                // Update the room
                if ([body_dict objectForKey:@"topic"]) {
                    room.topic = [body_dict objectForKey:@"topic"];
                }
                if ([body_dict objectForKey:@"name"]) {
                    room.name = [body_dict objectForKey:@"name"];
                }
                return [[HTTPDataResponse alloc] initWithData:[@{@"data": room.representation} jsonRepresentation]];
            }
        }
        return nil;
    }
    
#pragma mark - POST /api/rainbow/enduser/v1.0/rooms/{roomId}/users
    if ([request.method isEqualToString:@"POST"] && [request.url.relativePath matchURL:@"/api/rainbow/enduser/v1.0/rooms/{}/users"]) {
        NSString *roomID = [path_elements objectAtIndex:[path_elements count]-2];
        
        NSDictionary *body_dict = [body objectFromJSONData];
        // example of body :
        /*
         {
         "userId": "56e05bb83456faf3384272ee",
         "privilege": "user",
         ? "status": "invited", ?
         "reason": "invitation reason"
         }
         */
        
        for (RoomAPI *room in [ObjectAPIStore sharedObject].rooms) {
            if ([room.ID isEqualToString:roomID]) {
                // Add the user to the room
                NSString *userId = [body_dict objectForKey:@"userId"];
                NSString *privilege = [body_dict objectForKey:@"privilege"];
                NSString *status = @"invited";
                NSDictionary *result = [room addUser:userId privilege:privilege status:status];
                
                return [[HTTPDataResponse alloc] initWithData:[@{@"data": result} jsonRepresentation]];
            }
        }
        return nil;
    }
    
#pragma mark - PUT /api/rainbow/enduser/v1.0/rooms/{roomId}/users/{userId}
    if ([request.method isEqualToString:@"PUT"] && [request.url.relativePath matchURL:@"/api/rainbow/enduser/v1.0/rooms/{}/users/{}"]) {
        NSString *roomID = [path_elements objectAtIndex:[path_elements count]-3];
        NSString *userID = [path_elements objectAtIndex:[path_elements count]-1];
        
        NSDictionary *body_dict = [body objectFromJSONData];
        // example of body :
        /*
         {
         "privilege": "user",
         "status": "invited"
         }
         */
        
        for (RoomAPI *room in [ObjectAPIStore sharedObject].rooms) {
            if ([room.ID isEqualToString:roomID]) {
                // Update the user status/privilege
                for (NSMutableDictionary *user in room.users) {
                    if ([[user objectForKey:@"userId"] isEqualToString:userID]) {
                        
                        NSString *privilege = [body_dict objectForKey:@"privilege"];
                        if ([privilege length]) {
                            [user setObject:privilege forKey:@"privilege"];
                        }
                        NSString *status = [body_dict objectForKey:@"status"];
                        if ([status length]) {
                            [user setObject:status forKey:@"status"];
                        }
                        
                        return [[HTTPDataResponse alloc] initWithData:[@{@"data": user} jsonRepresentation]];
                    }
                }
                return nil;
            }
        }
        return nil;
    }
    
    
    //////////////////////
    // SETTINGS
    
#pragma mark - GET /api/rainbow/enduser/v1.0/users/{userId}/settings
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/settings", [self.class myRainbowID]]]) {
        return [[HTTPDataResponse alloc] initWithData:[@{@"data": [[ObjectAPIStore sharedObject].settings representation]} jsonRepresentation]];
    }
    
#pragma mark - PUT /api/rainbow/enduser/v1.0/users/{userId}/settings
    if ([request.method isEqualToString:@"PUT"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/settings", [self.class myRainbowID]]]) {
        
        NSDictionary *bodyDict = [body objectFromJSONData];
        
        [ObjectAPIStore sharedObject].settings.presence = bodyDict[@"presence"];
        [ObjectAPIStore sharedObject].settings.displayNameOrderFirstNameFirst = bodyDict[@"displayNameOrderFirstNameFirst"];
        
        return [[HTTPDataResponse alloc] initWithData:[@{@"data": [[ObjectAPIStore sharedObject].settings representation]} jsonRepresentation]];
    }
    
    
    /////////////////////
    // USER DETAILS
    
#pragma mark - GET /api/rainbow/enduser/v1.0/users/{userId}
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath matchURL:@"/api/rainbow/enduser/v1.0/users/{}"]) {
        NSString *ID = [[request.url.relativePath componentsSeparatedByString:@"/"] lastObject];
        if(![ID isEqualToString:@"networks"]){
            for (ContactAPI *contact in [ObjectAPIStore sharedObject].contacts) {
                if ([contact.ID isEqualToString:ID]) {
                    NSDictionary *response = @{@"data": contact.representation};
                    return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
                }
            }
            return nil;
        }
    }
    
    
    /////////////////////
    // USER AVATAR
    
#pragma mark - GET /api/avatar/{userId}?size=128
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath matchURL:@"/api/avatar/{}"]) {
        NSString *userID = [path_elements lastObject];
        int side_size = [[self valueForKey:@"size" fromURL:request.url] intValue];
        
        for (ContactAPI *contact in [ObjectAPIStore sharedObject].contacts) {
            if ([contact.ID isEqualToString:userID]) {
                
                if (!contact.avatarColor) {
                    return nil;
                }
                
                // Generate an image of the required color.
                CGSize size = CGSizeMake(side_size, side_size);
                UIGraphicsBeginImageContextWithOptions(size, NO, 1);
                CGContextRef context = UIGraphicsGetCurrentContext();
                
                CGContextSetFillColorWithColor(context, contact.avatarColor.CGColor);
                CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));
                
                UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                return [[HTTPDataResponse alloc] initWithData:UIImageJPEGRepresentation(snapshot, 1.0f)];
            }
        }
        return nil;
    }
    
    
    /////////////////////
    // SELF-REGISTER
    
#pragma mark - POST /api/rainbow/enduser/v1.0/notifications/emails/self-register
    if ([request.method isEqualToString:@"POST"] && [request.url.relativePath isEqualToString:@"/api/rainbow/enduser/v1.0/notifications/emails/self-register"]) {
        // This webservice (on real server) will send an email with a temp code.
        // The user will have to reuse this code in account creation process.
        // Here in fake server, we dont send the email, and the temp code will be 0000.
        
        NSDictionary *bodyDict = [body objectFromJSONData];
        /* example
         {"email": "user@company.com", "lang": "fr"}
         */
        
        return [[HTTPDataResponse alloc] initWithData:[@{@"status": @"Self-register email successfully sent", @"data": bodyDict} jsonRepresentation]];
    }
    
#pragma mark - POST /api/rainbow/enduser/v1.0/users/self-register
    if ([request.method isEqualToString:@"POST"] && [request.url.relativePath isEqualToString:@"/api/rainbow/enduser/v1.0/users/self-register"]) {
        
        NSDictionary *bodyDict = [body objectFromJSONData];
        NSLog(@"self-register body : %@", bodyDict);
        /* many different possibilities for body..
         real self-register :
         {"loginEmail": "user1@company.com", "password": "Password123!","temporaryToken": "5723"}
         */
        
        return [[HTTPDataResponse alloc] initWithData:[@{} jsonRepresentation]];
    }
    
    
    //////////////////////
    // ABOUT
    
#pragma mark - GET /api/rainbow/admin/v1.0/about
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:@"/api/rainbow/admin/v1.0/about"]) {
        // Version is important (> 15) to enable rooms tab in app.
        return [[HTTPDataResponse alloc] initWithData:[@{@"version": @"1.18.0", @"description": @"FAKE Rainbow Admin Portal"} jsonRepresentation]];
    }
    
    // Emily
#pragma mark - GET /api/rainbow/enduser/v1.0/bots
    
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:@"/api/rainbow/enduser/v1.0/bots"]) {
        for (ContactAPI *contact in [ObjectAPIStore sharedObject].contacts) {
            if (contact.isBot) {
                NSDictionary *response = @{@"data": @[contact.representation]};
                return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
            }
        }
        return nil;
    }
    
#pragma mark - GET /api/rainbow/enduser/v1.0/users/myRainbowID/profiles/features
    
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/profiles/features", [self.class myRainbowID]]]) {
        
        return [[HTTPDataWithStatusResponse alloc] initWithFileName:@"getFeatures.json" atPath:bundlePath status:200];
    }
    
#pragma mark - GET /api/rainbow/filestorage/v1.0/users/consumption
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:@"/api/rainbow/filestorage/v1.0/users/consumption"]) {
        
        NSDictionary *response = @{@"data": @{@"feature":@"FILE_SHARING_QUOTA_GB",@"maxValue":@21474836480,@"currentValue":@6923337,@"unit":@"octet"}};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
#pragma mark - GET /api/rainbow/filestorage/v1.0/files
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:@"/api/rainbow/filestorage/v1.0/files"]) {
        NSDictionary *response = @{@"data": @[]};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
#pragma mark - POST /api/rainbow/enduser/v1.0/users/loginemails
    if ([request.method isEqualToString:@"POST"] && [request.url.relativePath isEqualToString:@"/api/rainbow/enduser/v1.0/users/loginemails"]) {
        
        NSDictionary *bodyDict = [body objectFromJSONData];
        NSLog(@"Login Emails : %@", bodyDict);
        
        return [[HTTPDataResponse alloc] initWithData:[@{} jsonRepresentation]];
    }
    
#pragma mark - POST /api/rainbow/enduser/v1.0/users/jids
    if ([request.method isEqualToString:@"POST"] && [request.url.relativePath isEqualToString:@"/api/rainbow/enduser/v1.0/users/jids"]) {
        
        NSDictionary *bodyDict = [body objectFromJSONData];
        
        id jids = [bodyDict objectForKey:@"jid_im"];
        NSArray *allJids = nil;
        if([jids isKindOfClass:[NSArray class]]){
            allJids = jids;
        } else {
            allJids = [jids componentsSeparatedByString:@", "];
        }
        
        NSMutableArray *users_result = [NSMutableArray new];
        NSLog(@"ALL JIDS : %@", allJids);
        
        for (NSString *jid  in allJids) {
            // Search the jid in our ObjectAPIStore
            for (ContactAPI *contact in [ObjectAPIStore sharedObject].contacts) {
                if ([contact.jid isEqualToString:jid]) {
                    [users_result addObject:[contact representation]];
                }
            }
        }
        
        // returns { "users": [ {u1}, {u2} ] }
        return [[HTTPDataResponse alloc] initWithData:[@{@"data": users_result} jsonRepresentation]];
    }
    
#pragma mark - GET /api/rainbow/enduser/v1.0/users/%@/join-companies/requests
    
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/join-companies/requests", [self.class myRainbowID]]]) {
        NSDictionary *response = @{@"data": @[]};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
#pragma mark - GET /api/rainbow/enduser/v1.0/users/myRainbowID/join-companies/invitations
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/join-companies/invitations", [self.class myRainbowID]]]) {
        NSDictionary *response = @{@"data": @[]};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
    
#pragma mark - GET /api/rainbow/enduser/v1.0/settings/iceservers
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:@"/api/rainbow/enduser/v1.0/settings/iceservers"]) {
        NSDictionary *response = @{@"data": @[]};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
#pragma mark - GET /api/rainbow/enduser/v1.0/users/myRainbowID/groups
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/groups", [self.class myRainbowID]]]) {
        NSDictionary *response = @{@"data": @[]};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
#pragma mark - GET /api/rainbow/enduser/v1.0/users/networks
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:@"/api/rainbow/enduser/v1.0/users/networks"]) {
        return [[HTTPDataWithStatusResponse alloc] initWithFileName:@"getUserNetworks.json" atPath:bundlePath status:200];
    }
    
#pragma mark - GET /api/rainbow/authentication/v1.0/logout
    if ([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:@"/api/rainbow/authentication/v1.0/logout"]) {
        NSDictionary *response = @{@"data": @[]};
        return [[HTTPDataResponse alloc] initWithData:[response jsonRepresentation]];
    }
    
#pragma mark - PUT /api/rainbow/enduser/v1.0/users/myRainbowID
    if ([request.method isEqualToString:@"PUT"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@", [self.class myRainbowID]]]) {
        NSDictionary *bodyDict = [body objectFromJSONData];
        return [[HTTPDataResponse alloc] initWithData:[@{@"data": bodyDict} jsonRepresentation]];
    }

    
#pragma mark - POST /api/rainbow/enduser/v1.0/users/myRainbowID/avatar
    if([request.method isEqualToString:@"POST"] && [request.url.relativePath isEqualToString:[NSString stringWithFormat:@"/api/rainbow/enduser/v1.0/users/%@/avatar",[self.class myRainbowID]]]) {
        NSDictionary *bodyDict = @{@"status": [NSString stringWithFormat:@"Avatar successfully set for user %@", [self.class myRainbowID]], @"data":@[]};
        return [[HTTPDataResponse alloc] initWithData:[bodyDict jsonRepresentation]];
    }
    
#pragma mark - GET /api/rainbow/search/v1.0/users?displayName=%@
    if([request.method isEqualToString:@"GET"] && [request.url.relativePath isEqualToString:@"/api/rainbow/search/v1.0/users"]) {
        NSString *pattern = [self valueForKey:@"displayName" fromURL:request.url];
        
        if (pattern && [pattern length]) {
            NSString *trimmedPattern = [pattern stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            NSMutableArray *subPredicates = [NSMutableArray array];
            NSArray *terms = [trimmedPattern componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            for (NSString *term in terms) {
                if(term.length == 0)
                    continue;
                NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                          @"(firstname BEGINSWITH[cd] %@) OR (lastname MATCHES[cd] %@)", term, [NSString stringWithFormat:@"(?w:).*\\b%@.*", term]];
                [subPredicates addObject:predicate];
            }
            
            NSMutableSet<ContactAPI *> *result = [NSMutableSet set];
            NSPredicate *filter = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
            [result addObjectsFromArray:[[ObjectAPIStore sharedObject].contacts filteredArrayUsingPredicate:filter]];
            
            NSMutableArray *users_result = [NSMutableArray array];
            for (ContactAPI *contact in [result allObjects]) {
                [users_result addObject:[contact representation]];
            }
            // returns { "users": [ {u1}, {u2} ] }
            return [[HTTPDataResponse alloc] initWithData:[@{@"users": users_result} jsonRepresentation]];
        }
        return nil;

    }
    
    NSLog(@"UNHANDLED REQUEST %@ %@", request.method, request.url.relativePath);
    return nil;
}



// Store manipulation

+(void) setSettingsPresence:(NSString *) presence {
    [ObjectAPIStore sharedObject].settings.presence = presence;
}

+(void) setSettingsDisplayNameOrderFirstNameFirst:(BOOL) firstNameFirst {
    [ObjectAPIStore sharedObject].settings.displayNameOrderFirstNameFirst = firstNameFirst;
}


+(void) addConversation:(ConversationAPI *) conversation {
    [[ObjectAPIStore sharedObject].conversations addObject:conversation];
}

+(void) deleteConversation:(ConversationAPI *) conversation {
    [[ObjectAPIStore sharedObject].conversations removeObject:conversation];
    
    if (websocket) {
        NSString *iq = [NSString stringWithFormat:@"<message type='management' id='%u' from='server@domain.com' to='%@'><conversation id='%@' action='delete' xmlns='jabber:iq:configuration'/></message>", arc4random(), websocket.fulljid, conversation.ID];
        [websocket sendMessage:iq];
    }
}

+(void) addContact:(ContactAPI *) contact {
    [[ObjectAPIStore sharedObject].contacts addObject:contact];
}

+(void) addInvitation:(InvitationAPI *) invitation {
    [[ObjectAPIStore sharedObject].invitations addObject:invitation];
    
    if (websocket) {
        NSString *iq = [NSString stringWithFormat:@"<message type='management' id='%u' from='server@domain.com' to='%@'><userinvite id='%@' action='create' type='%@' status='%@' xmlns='jabber:iq:configuration'/></message>", arc4random(), websocket.fulljid, invitation.ID, invitation.received ? @"received":@"sent", invitation.status];
        [websocket sendMessage:iq];
    }
}

+(void) updateInvitation:(InvitationAPI *) invitation {
    if (![[ObjectAPIStore sharedObject].invitations containsObject:invitation]) {
        NSAssert(NO, @"Cannot update invitation");
        return;
    }
    
    if (websocket) {
        NSString *iq = [NSString stringWithFormat:@"<message type='management' id='%u' from='server@domain.com' to='%@'><userinvite id='%@' action='update' type='%@' status='%@' xmlns='jabber:iq:configuration'/></message>", arc4random(), websocket.fulljid, invitation.ID, invitation.received ? @"received":@"sent", invitation.status];
        [websocket sendMessage:iq];
    }
}

+(void) deleteInvitation:(InvitationAPI *) invitation {
    [[ObjectAPIStore sharedObject].invitations removeObject:invitation];
    
    if (websocket) {
        NSString *iq = [NSString stringWithFormat:@"<message type='management' id='%u' from='server@domain.com' to='%@'><userinvite id='%@' action='delete' type='%@' status='%@' xmlns='jabber:iq:configuration'/></message>", arc4random(), websocket.fulljid, invitation.ID, invitation.received ? @"received":@"sent", invitation.status];
        [websocket sendMessage:iq];
    }
}

+(void) addMessage:(MessageAPI *) message {
    [[ObjectAPIStore sharedObject].messages addObject:message];
    
    if (websocket) {
        // Message from somebody else to me.
        if ([websocket.barejid isEqualToString:message.to_jid]) {
            NSMutableString *iq = [NSMutableString stringWithFormat:@"<message xmlns='jabber:client' type='%@' id='%@' from='%@' to='%@'>", [MessageAPI stringForMessageType:message.messageType], message.ID, message.from_jid, message.to_jid];
            
            if(message.messageType == ObjectAPIMessageTypeManagement){
                [iq appendString:[NSString stringWithFormat:@"<conversation xmlns='jabber:iq:configuration' id='%@' action='%@/>",message.conversationID, message.action]];
            }
            
            if(message.isArchived)
            [iq appendString:[NSString stringWithFormat:@"<archived xmlns='urn:xmpp:mam:tmp' by='localhost' stamp='%@'/><store xmlns='urn:xmpp:hints'/>", message.archiveDate]];
            
            if(message.needAck)
               [iq appendString:@"<request xmlns='urn:xmpp:receipts'/>"];
            
            if(message.isPush)
                [iq appendString:@"<retransmission xmlns='jabber:iq:notification' source='push'/>"];
            
            if(message.delayDate)
                [iq appendString:[NSString stringWithFormat:@"<delay xmlns='urn:xmpp:delay' from='localhost' stamp='%@'/>",message.delayDate]];
            if(message.body)
                [iq appendString:[NSString stringWithFormat:@"<body>%@</body>",message.body]];
            
            [iq appendString:@"</message>"];
            
            [websocket sendMessage:iq];
        }
        // Message from me to somebody else (send a Carbon to websocket)
        // TODO
    }
}


+(void) addRoom:(RoomAPI *) room {
    [[ObjectAPIStore sharedObject].rooms addObject:room];
    
    /*if (websocket) {
     NSString *iq = [NSString stringWithFormat:@"<message type='management' id='%u' from='server@domain.com' to='%@'><room xmlns='jabber:iq:configuration' roomid='%@' roomjid='%@' userjid='%@' status='accepted'/></message>", arc4random(), websocket.fulljid, room.ID, room.jid, websocket.fulljid];
     [websocket sendMessage:iq];
     }*/
}

+(void) setBundlePathForDocuments:(NSString *) path {
    bundlePath = path;
}

@end
