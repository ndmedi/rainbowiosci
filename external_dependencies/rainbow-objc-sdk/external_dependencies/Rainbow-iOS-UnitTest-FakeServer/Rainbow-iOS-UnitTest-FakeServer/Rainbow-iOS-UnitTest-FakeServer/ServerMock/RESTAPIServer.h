/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


#import "HTTPConnection.h"

// Import all ObjectAPI
#import "ConversationAPI.h"
#import "ContactAPI.h"
#import "InvitationAPI.h"
#import "MessageAPI.h"
#import "RoomAPI.h"


typedef BOOL(^HTTPTestBlock)(HTTPMessage * request);
typedef NSData *(^HTTPResponseBlock)(HTTPMessage * request);

@interface HTTPStubDescriptor : NSObject
@property(atomic, copy) HTTPTestBlock testBlock;
@property(atomic, copy) HTTPResponseBlock responseBlock;

+(HTTPStubDescriptor*)httpStubPassing:(HTTPTestBlock)testBlock withResponse:(HTTPResponseBlock)responseBlock;
@end

/*
 * This will be our REST API server
 * Obviously we'll be able to drive this server
 * from the tests, to make them predictible
 */
@interface RESTAPIServer : HTTPConnection

+(NSMutableArray<HTTPStubDescriptor *> *) stubDescriptors;

+(NSString *) myRainbowID;
+(NSString *) myJID;
+(NSString *) myLoginEmail;
+(NSString *) myLoginPassword;
+(NSString *) myCompanyID;
+(NSString *) myCompanyName;

+(void) closeWebsocket;
+(void) clean;
+(void) flushData;

+(void) setSettingsPresence:(NSString *) presence;
+(void) setSettingsDisplayNameOrderFirstNameFirst:(BOOL) firstNameFirst;

+(void) addConversation:(ConversationAPI *) conversation;
+(void) deleteConversation:(ConversationAPI *) conversation;

+(void) addContact:(ContactAPI *) contact;

+(void) addInvitation:(InvitationAPI *) invitation;
+(void) updateInvitation:(InvitationAPI *) invitation;
+(void) deleteInvitation:(InvitationAPI *) invitation;

+(void) addMessage:(MessageAPI *) message;

+(void) addRoom:(RoomAPI *) room;

+(void) setBundlePathForDocuments:(NSString *) path;

@end
