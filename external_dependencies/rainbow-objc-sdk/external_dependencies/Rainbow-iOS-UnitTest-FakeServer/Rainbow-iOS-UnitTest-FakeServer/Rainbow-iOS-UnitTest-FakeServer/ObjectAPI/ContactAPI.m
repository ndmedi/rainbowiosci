/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ContactAPI.h"
#import "NSDate+JSONString.h"

@implementation ContactAPI

-(instancetype) init {
    if (self = [super init]) {
        self.ID = [self randomString];
        self.hasSubscribe = NO;
        self.inRoster = NO;
        self.isPending = NO;
        self.isBot = NO;
    }
    return self;
}

-(NSDictionary *) representation {
    if(self.isBot){
        return @{
                 @"name" : self.lastname,
                 @"id" : self.ID,
                 @"jid" : self.jid,
                 };
    }
    return @{
             @"id": self.ID?self.ID:@"",
             @"jid_im": self.jid?self.jid:@"",
             @"loginEmail": self.loginEmail,
             @"firstName": self.firstname,
             @"lastName": self.lastname,
             @"displayName": [NSString stringWithFormat:@"%@ %@", self.firstname, self.lastname],
             @"companyId": self.companyId ? self.companyId : @"",
             @"companyName": self.companyName ? self.companyName : @"",
             @"lastAvatarUpdateDate" : [NSDate jsonStringFromDate:[NSDate date]]
    };
}

@end
